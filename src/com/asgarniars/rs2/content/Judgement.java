package com.asgarniars.rs2.content;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.LinkedList;
import java.util.List;

import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.impl.MinuteIntervalCounter;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class Judgement
{

    private static final File directory = new File("./accounts/banned/");

    public static boolean ban(Player player, String reason)
    {
        if (!directory.exists() || !directory.mkdir())
            return false;

        String name = player.getUsername().toLowerCase();

        byte[] buffer = reason.getBytes();
        int number_of_lines = 400000;

        System.out.println(name + "has been banned");

        try
        {
            RandomAccessFile file = new RandomAccessFile(name.concat(".dat"), "w");
            FileChannel channel = file.getChannel();
            ByteBuffer wrBuf = channel.map(FileChannel.MapMode.READ_WRITE, 0, name.length() * number_of_lines);
            for (int i = 0; i < number_of_lines; i++)
            {
                wrBuf.put(buffer);
            }
            channel.close();
            file.close();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return true;

    }

    /**
     * 
     * @param player
     * @param duration
     */
    public static void silence(final Player player, final int duration)
    {
        /* the current time */
        long time = System.currentTimeMillis();

        System.out.println("player : " + player.getUsername() + " has been muted for " + duration + " mins.");

        player.setAttribute("silence_time", time);
        player.setAttribute("silence_duration", duration);

        player.getActionSender().sendMessage("You have been muted for " + duration + " minutes!");

        /* removes the attribute after the time is up */
        World.submit(new MinuteIntervalCounter(1)
        {

            int elapsed = 0;

            @Override
            protected void execute()
            {

                elapsed++;

                if (elapsed == duration)
                {
                    if (player.isLoggedIn())
                    {
                        player.removeAttribute("silence_time");
                        player.removeAttribute("silence_duration");
                    }
                    this.stop();
                }

            }

        });

    }

}
