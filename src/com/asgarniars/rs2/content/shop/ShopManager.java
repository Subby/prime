package com.asgarniars.rs2.content.shop;

import java.io.FileInputStream;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.io.XStreamController;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.model.players.container.Container;
import com.asgarniars.rs2.model.players.container.ContainerType;

/**
 * A class for managing and loading shops.
 * 
 * @author Rait
 * 
 */
public class ShopManager
{

    /**
     * The list of shops.
     */
    private static List<Shop> shops;

    /**
     * The maximum item capacity of a shop.
     */
    public static final int SHOP_CAPACITY = 40;

    /**
     * The interface id for a shop.
     */
    public static final int SHOP_INTERFACE_ID = 3824;

    /**
     * The inventory interface opened when you're shopping.
     */
    public static final int SHOP_INVENTORY_ID = 3822;

    /**
     * The interface id for the shop's name.
     */
    public static final int SHOP_NAME_ID = 3901;

    /**
     * The interface id for the shop's item container.
     */
    public static final int SHOP_CONTAINER_ID = 3900;

    /**
     * The player inventory interface.
     */
    public static final int PLAYER_CONTAINER_ID = 3823;

    /**
     * The logging utility for this class.
     */
    private static final Logger logger = Logger.getLogger(ShopManager.class.getName());

    /**
     * Private constructor to avoid instantiation.
     */
    private ShopManager()
    {
        /* Empty */
    }

    /**
     * Load the shops from an XML file at a given path.
     * 
     * @param path The path to the shops.
     */
    @SuppressWarnings("unchecked")
    public static void load() throws Exception
    {
        List<Shop> list = (List<Shop>) XStreamController.getXStream().fromXML(new FileInputStream(Constants.CONFIG_DIRECTORY + "content/shops.xml"));
        for (Iterator<Shop> it$ = list.iterator(); it$.hasNext();)
        {
            Shop shop = it$.next();
            shop.setOriginalStock(new Container(ContainerType.ALWAYS_STACK, SHOP_CAPACITY));
            shop.setCurrentStock(new Container(ContainerType.ALWAYS_STACK, SHOP_CAPACITY));
            for (Item item : shop.getItems())
            {
                shop.getOriginalStock().add(item);
                shop.getCurrentStock().add(item);
            }
            // shop.setCurrentStock(shop.getOriginalStock());
        }
        logger.info("Loaded " + list.size() + " shops.");
        ShopManager.shops = list;
    }

    /**
     * Opens a shop with a given id.
     * 
     * @param id The id to get a shop by.
     */
    public static void open(Player player, int id)
    {
        Shop shop = getShop(id);
        ShopListener listener = new ShopListener(player);
        if (shop == null)
        {
            throw new NullPointerException("Shop with id " + id + " is broken or doesn't exist.");
        }
        player.getActionSender().sendItfInvOverlay(SHOP_INTERFACE_ID, SHOP_INVENTORY_ID);
        player.getActionSender().sendUpdateItems(SHOP_CONTAINER_ID, shop.getCurrentStock().toArray());
        player.getActionSender().sendUpdateItems(PLAYER_CONTAINER_ID, player.getInventory().getItemContainer().toArray());
        player.getActionSender().sendString(shop.getName(), SHOP_NAME_ID);
        shop.getCurrentStock().addListener(listener);
        player.setAttribute("shop", shop);
        player.setAttribute("shopListener", listener);
    }

    /**
     * Closes the shop for the player.
     * 
     * @param player The player to close the shop for.
     */
    public static void close(Player player)
    {
        Shop shop = player.getAttribute("shop");
        ShopListener listener = player.getAttribute("shopListener");
        player.getActionSender().removeInterfaces();
        if (shop == null || listener == null)
        {
            return;
        }
        shop.getCurrentStock().removeListener(listener);
        player.removeAttribute("shop");
        player.removeAttribute("shopListener");
    }

    /**
     * Gets a shop by a given identifier.
     * 
     * @param id The shop's identifier.
     * @return The shop that has the same identifier.
     */
    public static Shop getShop(int id)
    {
        for (Iterator<Shop> it$ = shops.iterator(); it$.hasNext();)
        {
            Shop shop = it$.next();
            if (shop.getId() == id)
            {
                return shop;
            }
        }
        return null;
    }

    /**
     * Gets the list of the cached shops.
     * 
     * @return The cached shops.
     */
    public static List<Shop> getShops()
    {
        return shops;
    }

}
