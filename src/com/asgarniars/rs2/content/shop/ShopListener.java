package com.asgarniars.rs2.content.shop;

import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.model.players.container.Container;
import com.asgarniars.rs2.model.players.container.ContainerListener;

/**
 * A listener for the shop interface.
 * 
 * @author Rait
 * 
 */
public class ShopListener implements ContainerListener
{

    /**
     * The shop listener attached to the player.
     */
    private Player player;

    /**
     * Construct the shop listener.
     * 
     * @param player The player to attach it to.
     */
    public ShopListener(Player player)
    {
        this.player = player;
    }

    @Override
    public void itemChanged(Container container, int slot)
    {
        player.getActionSender().sendUpdateItem(slot, ShopManager.SHOP_CONTAINER_ID, container.get(slot));
        player.getActionSender().sendUpdateItems(ShopManager.PLAYER_CONTAINER_ID, player.getInventory().getItemContainer().toArray());
    }

    @Override
    public void itemsChanged(Container container, int[] slots)
    {
        player.getActionSender().sendUpdateItems(ShopManager.SHOP_CONTAINER_ID, container.toArray());
        player.getActionSender().sendUpdateItems(ShopManager.PLAYER_CONTAINER_ID, player.getInventory().getItemContainer().toArray());
    }

    @Override
    public void itemsChanged(Container container)
    {
        player.getActionSender().sendUpdateItems(ShopManager.SHOP_CONTAINER_ID, container.toArray());
        player.getActionSender().sendUpdateItems(ShopManager.PLAYER_CONTAINER_ID, player.getInventory().getItemContainer().toArray());
    }

}
