package com.asgarniars.rs2.content.dialogue;

/**
 * The expression/emoticon displayed on a humanoid character's face on a
 * dialogue box.
 * 
 * @author Rait
 * 
 */
public enum ChatExpression
{
    CONTENT(591),

    EVIL(592),

    SAD(596),

    SLEEPY(603),

    LAUGHING(605),

    MOURNING(610),

    MAD(614);

    /**
     * The animation.
     */
    private int animation;

    /**
     * Construct a chat expression.
     * 
     * @param animation The animation id.
     */
    private ChatExpression(int animation)
    {
        this.animation = animation;
    }

    /**
     * Gets the animation.
     * 
     * @return The animation id.
     */
    public int getAnimation()
    {
        return animation;
    }

}
