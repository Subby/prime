package com.asgarniars.rs2.content.dialogue;

import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.npcs.NpcDefinition;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * A chat dialogue, where an NPC or a player say one or more words.
 * 
 * @author Rait
 * 
 */
public class ChatDialogue extends Dialogue
{

    /**
     * The type of this chat dialogue.
     */
    private ChatType type;

    /**
     * If it's an npc chat, hold the npc itself in the dialogue.
     */
    private Npc npc;

    /**
     * Construct a new chat dialogue.
     * 
     * @param player The speaking player.
     * @param text The spoken text.
     */
    public ChatDialogue(Player player, Task task, String... text)
    {
        this.task = task;
        this.type = ChatType.PLAYER_CHAT;
        this.text = text;
    }

    /**
     * Construct a new chat dialogue.
     * 
     * @param npc The speaking NPC.
     * @param text The spoken text.
     */
    public ChatDialogue(Npc npc, Task task, String... text)
    {
        this.task = task;
        this.type = ChatType.NPC_CHAT;
        if (type.equals(ChatType.NPC_CHAT))
        {
            this.npc = npc;
        }
        this.text = text;
    }

    @Override
    public void click(int index, Player player)
    {
        show(player);
    }

    @Override
    public void show(Player player)
    {
        if (task != null)
        {
            World.submit(task);
        }
        switch (type)
        {
        case PLAYER_CHAT:
            switch (text.length)
            {
            case 1:
                player.getActionSender().sendDialogueAnimation(969, ChatExpression.CONTENT.getAnimation());
                player.getActionSender().sendString(player.getUsername(), 970);
                player.getActionSender().sendString(text[0], 971);
                player.getActionSender().sendPlayerDialogueHead(969);
                player.getActionSender().sendChatInterface(968);
                break;
            case 2:
                player.getActionSender().sendDialogueAnimation(974, ChatExpression.CONTENT.getAnimation());
                player.getActionSender().sendString(player.getUsername(), 975);
                player.getActionSender().sendString(text[0], 976);
                player.getActionSender().sendString(text[1], 977);
                player.getActionSender().sendPlayerDialogueHead(974);
                player.getActionSender().sendChatInterface(973);
                break;
            case 3:
                player.getActionSender().sendDialogueAnimation(980, ChatExpression.CONTENT.getAnimation());
                player.getActionSender().sendString(player.getUsername(), 981);
                player.getActionSender().sendString(text[0], 982);
                player.getActionSender().sendString(text[1], 983);
                player.getActionSender().sendString(text[2], 984);
                player.getActionSender().sendPlayerDialogueHead(980);
                player.getActionSender().sendChatInterface(979);
                break;
            case 4:
                player.getActionSender().sendDialogueAnimation(987, ChatExpression.CONTENT.getAnimation());
                player.getActionSender().sendString(player.getUsername(), 988);
                player.getActionSender().sendString(text[0], 989);
                player.getActionSender().sendString(text[1], 990);
                player.getActionSender().sendString(text[2], 991);
                player.getActionSender().sendString(text[3], 992);
                player.getActionSender().sendPlayerDialogueHead(987);
                player.getActionSender().sendChatInterface(986);
                break;
            }
            break;
        case NPC_CHAT:
            if (npc == null)
            {
                player.getActionSender().sendMessage("Dialogue error - NPC is null!");
                return;
            }
            NpcDefinition definition = npc.getDefinition();
            switch (text.length)
            {
            case 1:
                player.getActionSender().sendDialogueAnimation(4883, ChatExpression.LAUGHING.getAnimation());
                player.getActionSender().sendString(definition.getName(), 4884);
                player.getActionSender().sendString(text[0], 4885);
                player.getActionSender().sendNPCDialogueHead(definition.getId(), 4883);
                player.getActionSender().sendChatInterface(4882);
                break;
            case 2:
                player.getActionSender().sendDialogueAnimation(4888, ChatExpression.CONTENT.getAnimation());
                player.getActionSender().sendString(definition.getName(), 4889);
                player.getActionSender().sendString(text[0], 4890);
                player.getActionSender().sendString(text[1], 4891);
                player.getActionSender().sendNPCDialogueHead(definition.getId(), 4888);
                player.getActionSender().sendChatInterface(4887);
                break;
            case 3:
                player.getActionSender().sendDialogueAnimation(4894, ChatExpression.CONTENT.getAnimation());
                player.getActionSender().sendString(definition.getName(), 4895);
                player.getActionSender().sendString(text[0], 4896);
                player.getActionSender().sendString(text[1], 4897);
                player.getActionSender().sendString(text[2], 4898);
                player.getActionSender().sendNPCDialogueHead(definition.getId(), 4894);
                player.getActionSender().sendChatInterface(4893);
                break;
            case 4:
                player.getActionSender().sendDialogueAnimation(4901, ChatExpression.CONTENT.getAnimation());
                player.getActionSender().sendString(definition.getName(), 4902);
                player.getActionSender().sendString(text[0], 4903);
                player.getActionSender().sendString(text[1], 4904);
                player.getActionSender().sendString(text[2], 4905);
                player.getActionSender().sendString(text[3], 4906);
                player.getActionSender().sendNPCDialogueHead(definition.getId(), 4901);
                player.getActionSender().sendChatInterface(4900);
                break;
            }
            break;
        }
    }

    /**
     * Gets the type of this chat dialogue.
     * 
     * @return This chat dialogue's type.
     */
    public ChatType getType()
    {
        return type;
    }

}
