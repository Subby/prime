package com.asgarniars.rs2.content.dialogue.impl;

import com.asgarniars.rs2.content.dialogue.ChatDialogue;
import com.asgarniars.rs2.content.dialogue.Dialogue;
import com.asgarniars.rs2.content.dialogue.DialogueSession;
import com.asgarniars.rs2.content.dialogue.Option;
import com.asgarniars.rs2.content.dialogue.StatementDialogue;
import com.asgarniars.rs2.content.dialogue.StatementType;
import com.asgarniars.rs2.content.quest.Quest;
import com.asgarniars.rs2.content.quest.QuestRepository;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * 
 * @author Conner Kendall <Emnesty>
 * 
 */
public class Cook extends DialogueSession<Npc>
{

    public Cook(Player player, Npc[] actors)
    {
        super(player, actors);
    }

    @Override
    public Dialogue evaluate()
    {
        final Npc npc = actors[0];
        final Quest quest = QuestRepository.get("CooksAssistant");

        if (player.getQuestStorage().hasStarted(quest))
        {
            Dialogue dialogue = new ChatDialogue(npc, null, "How are you getting on with finding the ingredients?");
            if (this.hasRequiredItems(new Item(1927, 1), new Item(1944, 1), new Item(2516, 1)))
            {
                dialogue.add(player, new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        player.getInventory().removeItem(new Item(1927, 1));
                        this.stop();
                    }

                }, "Here's a bucket of milk.");
                dialogue.add(player, new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        player.getInventory().removeItem(new Item(2516, 1));
                        this.stop();
                    }

                }, "Here's a pot of flour.");
                dialogue.add(player, new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        player.getInventory().removeItem(new Item(1944, 1));
                        this.stop();
                    }

                }, "Here's a fresh egg.");
                dialogue.add(npc, null, "You've brought me everything I need! I am saved!", "Thank you!");
                dialogue.add(player, null, "So do I get to go to the Duke's Party?");
                dialogue.add(npc, null, "I'm afraid not, only the big cheeses get to dine with the", "Duke.");
                dialogue.add(player, null, "Well, maybe one day I'll be important enough to sit on", "the Duke's table.");
                dialogue.add(npc, new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        player.setAttribute("currentQuest", quest);
                        player.getQuestStorage().setState(quest, 2);
                        this.stop();
                    }

                }, "Maybe, but I won't be holding my breath.");
            }
            else
            {
                /*
                 * If player doesn't have any of the required items.
                 */
                if (!this.hasRequiredItems(new Item(1927, 1), new Item(1944, 1), new Item(2516, 1)))
                {
                    dialogue.add(player, null, "I haven't got any of them yet, I'm still looking.");
                    Dialogue message = new StatementDialogue(StatementType.NORMAL, "You still need to get:", "A bucket of milk. A pot of flour. An egg.");
                    dialogue.add(message);
                }

                dialogue.add(npc, null, "Please get the ingredients quickly.  I'm running out of", "time! The Duke will throw me into the streets!");
                dialogue.add(player, new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        this.stop();
                    }

                }, "I'll get right on it.");
            }

            player.open(dialogue);
            return dialogue;
        }

        Dialogue dialogue = new ChatDialogue(npc, null, "What am I to do?");

        Option option_1 = new Option("What's wrong?", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(player, null, "What's wrong?");
                dialogue.add(npc, null, "Oh dear, oh dear, oh dear, I'm in a terrible terrible", "mess! It's the Duke's birthday today, and I should be", "making him a lovely big birthday cake.");
                dialogue.add(npc, null, "I've forgotten to buy the ingredients. I'll never get", "them in time now. He'll sack me! What will I do? I have", "four children and a goat to look after. Would you help", "me? Please?");

                Option option_1 = new Option("I'm always happy to help a cook in distress.", new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        Dialogue dialogue = new ChatDialogue(player, new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                player.setAttribute("currentQuest", quest);
                                quest.start(player);
                                player.getQuestStorage().setState(quest, 1);
                                this.stop();
                            }

                        }, "Yes, I'll help you.");
                        dialogue.add(npc, null, "Oh thank you, thank you. I need milk, an egg and", "flour. I'd be very grateful if you can get them for me.");
                        dialogue.add(player, null, "So where do I find these ingredients then?");

                        Option option_1 = new Option("Where do I find some flour?", new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                Dialogue dialogue = new ChatDialogue(npc, null, "There is a Mill fairly close, Go North and then West.", "Mill Lane Mill is just off the road to Draynor. I", "usually get my flour from there.");
                                dialogue.add(npc, null, "Talk to Millie, she'll help, she's a lovely girl and a fine Miller..");

                                player.open(dialogue);
                                this.stop();
                            }

                        });

                        Option option_2 = new Option("How about milk?", new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                Dialogue dialogue = new ChatDialogue(npc, null, "There is a cattle field on the other side of the river,", "just across the road from the Groats' Farm.");
                                dialogue.add(npc, null, "Talk to Gillie Groats, she looks after the Dairy cows -", "she'll tell you everything you need to know about milking cows!");

                                player.open(dialogue);
                                this.stop();
                            }

                        });

                        Option option_3 = new Option("And eggs? Where are they found?", new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                Dialogue dialogue = new ChatDialogue(npc, null, "I normally get my eggs from the Groats' farm, on the", "other side of the river.");
                                dialogue.add(npc, null, "But any chicken should lay eggs.");

                                player.open(dialogue);
                                this.stop();
                            }

                        });

                        Option option_4 = new Option("Actually, I know where to find this stuff.", new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                Dialogue dialogue = new ChatDialogue(player, null, "Actually, I know where to find this stuff.");

                                player.open(dialogue);
                                this.stop();
                            }

                        });

                        dialogue.add("Select an Option", option_1, option_2, option_3, option_4);
                        player.open(dialogue);
                        this.stop();
                    }

                });

                Option option_2 = new Option("I can't right now, Maybe later.", new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        Dialogue dialogue = new ChatDialogue(player, null, "No, I don't feel like it. Maybe later.");
                        dialogue.add(npc, null, "Fine. I always knew you Adventurer types were callous", "beasts. Go on your merry way!");

                        player.open(dialogue);
                        this.stop();
                    }

                });

                dialogue.add("Select an Option", option_1, option_2);
                player.open(dialogue);
                this.stop();
            }

        });

        Option option_2 = new Option("Can you make me a cake?", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(player, null, "You're a cook, why don't you bake me a cake?");
                dialogue.add(npc, null, "*sniff* Don't talk to me about cakes...");

                player.open(dialogue);
                this.stop();
            }

        });

        Option option_3 = new Option("You don't look very happy.", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(player, null, "You don't look very happy.");
                dialogue.add(npc, null, "No, I'm not. The world is caving in around me - I am", "overcome by dark feelings of impending doom.");

                dialogue.add(player, null, "I'd take the rest of the day off if I were you.");
                dialogue.add(npc, null, "No, that's the worst thing I could do. I'd get in terrible", "trouble.");
                dialogue.add(player, null, "Well maybe you need to take a holiday...");
                dialogue.add(npc, null, "That would be nice, but the Duke doesn't allow holidays", "for core staff.");
                dialogue.add(player, null, "Hmm, why not run away to the sea and start a new life", "as a Pirate?");
                dialogue.add(npc, null, "My wife gets sea sick, and I have an irrational fear of", "eyepatches. I don't see it working myself.");
                dialogue.add(player, null, "I'm afraid I've run out of ideas.");
                dialogue.add(npc, null, "I know I'm doomed.");

                player.open(dialogue);
                this.stop();
            }

        });

        Option option_4 = new Option("Nice hat!", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(player, null, "Nice hat!");
                dialogue.add(npc, null, "Err thank you. It's a pretty ordinary cooks hat really.");
                dialogue.add(player, null, "Still, suits you. The trousers are pretty special too.");
                dialogue.add(npc, null, "Its all standard cook's issue uniform...");
                dialogue.add(player, null, "The whole hat, apron, stripey trousers ensemble - it", "works. It make you looks like a real cook.");
                dialogue.add(npc, null, "I am a real cook! I haven't got time to be chatting", "about Culinary Fashion. I am in desperate need of help!");

                player.open(dialogue);
                this.stop();

            }

        });

        dialogue.add("Select an Option", option_1, option_2, option_3, option_4);
        return dialogue;
    }

    private boolean hasRequiredItems(Item... items)
    {
        for (Item item : items)
        {
            if (player.getInventory().getItemContainer().getCount(item.getId()) != item.getCount())
            {
                return false;
            }
        }
        return true;
    }
}
