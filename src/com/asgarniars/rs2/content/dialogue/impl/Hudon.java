package com.asgarniars.rs2.content.dialogue.impl;

import com.asgarniars.rs2.content.dialogue.ChatDialogue;
import com.asgarniars.rs2.content.dialogue.Dialogue;
import com.asgarniars.rs2.content.dialogue.DialogueSession;
import com.asgarniars.rs2.content.quest.Quest;
import com.asgarniars.rs2.content.quest.QuestRepository;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * 
 * @author Conner Kendall <Emnesty>
 * 
 */
public class Hudon extends DialogueSession<Npc>
{

    public Hudon(Player player, Npc[] actors)
    {
        super(player, actors);
    }

    @Override
    public Dialogue evaluate()
    {
        final Npc npc = actors[0];
        final Quest quest = QuestRepository.get("WaterfallQuest");

        Dialogue dialogue = new ChatDialogue(player, null, "Hello son, are you okay? You need help?");
        dialogue.add(npc, null, "It looks like you need the help.");
        dialogue.add(player, null, "Your mum sent me to find you.");
        dialogue.add(npc, null, "Don't play nice with me, I know you're looking for the", "treasure too.");
        dialogue.add(player, null, "Where is this treasure you talk of?");
        dialogue.add(npc, null, "Just because I'm small doesn't mean I'm dumb! If I", "told you, you would take it all for yourself.");
        dialogue.add(player, null, "Maybe I could help.");
        dialogue.add(npc, new Task(1, true)
        {

            @Override
            protected void execute()
            {
                player.getQuestStorage().setState(quest, 2);
                this.stop();
            }

        }, "I'm fine alone.");

        return dialogue;
    }

}
