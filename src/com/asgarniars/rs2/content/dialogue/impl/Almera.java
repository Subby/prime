package com.asgarniars.rs2.content.dialogue.impl;

import com.asgarniars.rs2.content.dialogue.ChatDialogue;
import com.asgarniars.rs2.content.dialogue.Dialogue;
import com.asgarniars.rs2.content.dialogue.DialogueSession;
import com.asgarniars.rs2.content.dialogue.Option;
import com.asgarniars.rs2.content.quest.Quest;
import com.asgarniars.rs2.content.quest.QuestRepository;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * 
 * @author Joshua Barry
 * @author Conner Kendall <Emnesty>
 * 
 */
public class Almera extends DialogueSession<Npc>
{

    public Almera(Player player, Npc[] actors)
    {
        super(player, actors);
    }

    @Override
    public Dialogue evaluate()
    {
        final Npc npc = actors[0];
        final Quest quest = QuestRepository.get("WaterfallQuest");

        Dialogue dialogue = new ChatDialogue(player, null, "Hello.");
        dialogue.add(npc, null, "Ah, hello there. Nice to see an outsider for a change,", "are you busy? I have a problem.");

        Option rush = new Option("I'm afraid I'm in a rush.", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(player, null, "I'm afraid I'm in a rush.");
                player.open(dialogue);
                this.stop();

            }

        });

        Option help = new Option("How can I help?", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(player, null, "How can I help?");
                dialogue.add(npc, null, "It's my son Hudon, he's always getting into trouble, the", "boy's convinced there's hidden treasure in the river and", "I'm a bit worried about his safety, the poor lad can't", "even swim.");
                dialogue.add(player, null, "I could go and take a look for you if you like?");
                dialogue.add(npc, new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        player.setAttribute("currentQuest", quest);
                        quest.start(player);
                        player.getQuestStorage().setState(quest, 1);
                        this.stop();
                    }

                }, "Would you? You are kind. You can use the small raft", "out back if you wish, do be careful, the current down", "stream is very strong.");
                player.open(dialogue);
                this.stop();
            }

        });
        dialogue.add("Select an Option", rush, help);
        return dialogue;
    }

}
