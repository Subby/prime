package com.asgarniars.rs2.content.dialogue.impl;

import com.asgarniars.rs2.content.dialogue.ChatDialogue;
import com.asgarniars.rs2.content.dialogue.Dialogue;
import com.asgarniars.rs2.content.dialogue.DialogueSession;
import com.asgarniars.rs2.content.dialogue.Option;
import com.asgarniars.rs2.content.quest.Quest;
import com.asgarniars.rs2.content.quest.QuestRepository;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class FredTheFarmer extends DialogueSession<Npc>
{

    public FredTheFarmer(Player player, Npc[] actors)
    {

        super(player, actors);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Dialogue evaluate()
    {
        final Npc npc = actors[0];
        final Quest quest = QuestRepository.get("SheepShearer");
        String[] messages = new String[]
        { "What are you doing on my land?" };

        Option opt_kill = new Option("I'm looking for something to kill.", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(player, null, "I'm looking for something to kill.");
                dialogue.add(npc, null, "What, on my land? Leave my livestock alone", "scoundrel!");
                player.open(dialogue);
                this.stop();
            }
        });

        Option opt_lost = new Option("I'm lost.", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(player, null, "I'm lost");
                dialogue.add(npc, null, "How can you be lost? Just follow the road east and", "south. You'll end up in Lumbridge fairly quickly.");
                player.open(dialogue);
                this.stop();
            }
        });

        Option opt_thing = new Option("Fred! Fred! I've seen The Thing!", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(player, null, "Fred! Fred! I've seen The Thing!");
                dialogue.add(npc, null, "You... you actually saw it?");
                dialogue.add(npc, null, "Run for the hills!", player.getUsername(), "grab as many chickens as", "you can! We have to...");
                dialogue.add(player, null, "Fred!");
                dialogue.add(npc, null, "... flee! Oh, woe is me! The shapeshifter is coming!", "We're all ...");
                dialogue.add(player, null, "FRED!");
                dialogue.add(npc, null, "... doomed. What!");
                dialogue.add(player, null, "It's not a shapeshifter or any other kind of monster!");
                dialogue.add(npc, null, "Well then what is it boy?");
                dialogue.add(player, null, "Well ... it's just two Penguins; Penguins disguised as a", "sheep.");
                dialogue.add(npc, null, "...");
                dialogue.add(npc, null, "Have you been out in the sun too long?");
                player.open(dialogue);
                this.stop();

            }

        });

        final Dialogue dialogue = new ChatDialogue(npc, null, messages);

        if (!player.getQuestStorage().hasStarted(quest) && !player.getQuestStorage().hasFinished(quest))
        {
            messages = new String[]
            { "What are you doing on my land? You're not the one", "who keeps leaving all my gates open and letting out all", "my sheep are you?" };

            Option opt_quest = new Option("I'm looking for a quest.", new Task(1, true)
            {

                @Override
                protected void execute()
                {
                    Dialogue dialogue = new ChatDialogue(player, new Task(1, true)
                    {

                        @Override
                        protected void execute()
                        {
                            player.setAttribute("currentQuest", quest);
                            this.stop();

                        }

                    }, "I'm looking for a quest.");
                    dialogue.add(npc, null, "You're after a quest, you say? Actually I could do with", "a bit of help.");
                    dialogue.add(npc, null, "My sheep are getting might woolly. I'd be much", "obliged if you could shear them. And while you're at it", "spin the wool for me too.");
                    dialogue.add(npc, null, "Yes, that's it. Bring me 20 balls of wool and I'm sure", "I could sort out some sort of payment. Of course,", "there's the small matter of The Thing.");
                    Option startQuest = new Option("Yes okay. I can do that", new Task(1, true)
                    {
                        @Override
                        protected void execute()
                        {
                            Dialogue dialogue = new ChatDialogue(player, new Task(1, true)
                            {
                                @Override
                                protected void execute()
                                {
                                    quest.start(player);
                                    player.getQuestStorage().setState(quest, 1);
                                    this.stop();
                                }
                            }, "Yes okay. I can do that.");
                            dialogue.add(npc, null, "Good! Now one more thing, do you actually know how", "to shear a sheep?");
                            Option yes = new Option("Of course!", new Task(1, true)
                            {
                                @Override
                                protected void execute()
                                {
                                    Dialogue dialogue = new ChatDialogue(player, null, "Of course!");
                                    dialogue.add(npc, null, "And you know how to spin wool into balls?");
                                    Option yes = new Option("I'm something of an expert actually!",

                                    new Task(1, true)
                                    {
                                        @Override
                                        protected void execute()
                                        {
                                            Dialogue dialogue = new ChatDialogue(player, null, "I'm something of an expert actually!");
                                            dialogue.add(npc, null, "Well you can stop grinning and get to work then.");
                                            dialogue.add(npc, null, "I'm not paying you by the hour!");
                                            player.open(dialogue);
                                            this.stop();
                                        }
                                    });
                                    Option no = new Option("I don't know how to spin wool, sorry.",

                                    new Task(1, true)
                                    {
                                        @Override
                                        protected void execute()
                                        {
                                            // TODO:
                                            // what
                                            // happens
                                            // here?
                                            this.stop();
                                        }
                                    });
                                    dialogue.add("Select an Option", yes, no);
                                    player.open(dialogue);
                                    this.stop();
                                }
                            });
                            Option no = new Option("Err. No, I don't know actually.", new Task(1, true)
                            {
                                @Override
                                protected void execute()
                                {
                                    this.stop();
                                }
                            });
                            dialogue.add("Select an Option", yes, no);
                            player.open(dialogue);
                            this.stop();
                        }
                    });
                    Option na = new Option("That doesn't sound a very exciting quest.", new Task(1, true)
                    {
                        @Override
                        protected void execute()
                        {
                            // TODO: learn what the fuck happens
                            // here.
                            this.stop();
                        }
                    });
                    Option wut = new Option("What do you mean, The Thing?", new Task(1, true)
                    {
                        @Override
                        protected void execute()
                        {
                            // TODO : wut happens here?
                            this.stop();
                        }
                    });
                    dialogue.add("Select an Option", startQuest, na, wut);
                    player.open(dialogue);
                    this.stop();
                }

            });

            dialogue.add("Select an Option", opt_quest, opt_kill, opt_lost);
        }
        else
        {
            if (!player.getQuestStorage().hasFinished(quest))
            {
                if (player.getInventory().getItemContainer().getCount(1759) < 20)
                {
                    Dialogue unfinDialogue = new ChatDialogue(npc, null, "You do not have enough wool.");
                    return unfinDialogue;
                }
                Dialogue finDialogue = new ChatDialogue(npc, new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        player.setAttribute("currentQuest", quest);
                        this.stop();

                    }

                }, "How are you doing getting those balls of wool?");
                finDialogue.add(player, null, "I have some.");
                finDialogue.add(npc, null, "Give'em here then.");
                finDialogue.add(player, new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        player.getInventory().removeItem(new Item(1759, 20));
                        player.getQuestStorage().setState(quest, 2);
                        this.stop();
                    }
                }, "That's the last of them.");
                finDialogue.add(npc, new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        player.getQuestStorage().setState(quest, 3);
                        this.stop();
                    }
                }, "I guess I'd better pay you then");
                return finDialogue;

            }
        }
        dialogue.add("Select an Option", opt_kill, opt_lost, opt_thing);

        return dialogue;
    }
}
