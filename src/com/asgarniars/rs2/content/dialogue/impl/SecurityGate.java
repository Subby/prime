package com.asgarniars.rs2.content.dialogue.impl;

import java.util.logging.Logger;

import com.asgarniars.rs2.content.dialogue.ChatDialogue;
import com.asgarniars.rs2.content.dialogue.Dialogue;
import com.asgarniars.rs2.content.dialogue.DialogueSession;
import com.asgarniars.rs2.content.dialogue.Option;
import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;
import com.asgarniars.rs2.task.impl.GateOfWar;
import com.asgarniars.rs2.util.Misc;
import com.asgarniars.rs2.util.clip.RSObject;
import com.asgarniars.rs2.util.clip.RegionClipping;

/**
 * Handles the Stronghold of Security doors.
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class SecurityGate extends DialogueSession<Npc>
{

    private Player player;

    /**
     * 
     * @param player
     * @param actors
     */
    public SecurityGate(Player player, Npc[] actors)
    {
        super(player, actors);
        this.player = player;
    }

    @Override
    public Dialogue evaluate()
    {

        final Npc npc = actors[0];

        int idx = Misc.random(Quiz.values().length);

        final Quiz quiz = Quiz.forOridnal(idx);

        Dialogue dialogue = new ChatDialogue(npc, null, quiz.getQuestion());
        Option correct = new Option(quiz.getAnswers()[0], new Task(1, true)
        {

            @Override
            protected void execute()
            {
                proceed();
                this.stop();

            }

        });
        Option incorrect = new Option(quiz.getAnswers()[1], new Task(1, true)
        {

            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(npc, null, quiz.getCorrection());
                player.open(dialogue);
                this.stop();

            }

        });
        dialogue.add("Select an Option", correct, incorrect);
        return dialogue;
    }

    /**
     * Proceed through a door after selecting the correct answer.
     */
    private void proceed()
    {
        final int player_x = player.getPosition().getX();
        final int player_y = player.getPosition().getY();
        final int door_x = player.getClickX();
        final int door_y = player.getClickY();

        final int plane = player.getPosition().getZ();

        RSObject object = RegionClipping.getRSObject(player, door_x, door_y, plane);

        int face = object.getFace();
        int off_x = 0;
        int off_y = 0;

        System.out.println("TACKLING A DOOR FACING : " + face);

        switch (face)
        {
        case 1: // parallel to east - west.
            if (door_y < player_y)
                off_y = -1;
            else if (door_y > player_y || door_y == player_y)
                off_y = 1;
            break;
        case 3:
            if (door_y < player_y || door_y == player_y)
                off_y = -1;
            else if (door_y > player_y)
                off_y = 1;
            break;
        case 0: // parallel to north - south.
            if (door_x > player_x)
                off_x = 1;
            else
                off_x = -1;
            break;
        case 2:
            if (door_x > player_x || door_x == player_x)
                off_x = 1;
            else
                off_x = -1;
            break;
        default:
            Logger.getAnonymousLogger().info("Undefined security door face : " + face);
            break;
        }

        final Position position = new Position((player_x + off_x), (player_y + off_y), plane);
        player.getUpdateFlags().sendAnimation(4282);
        World.submit(new GateOfWar(player, position));

    }

    private enum Quiz
    {

        ADVERTISE(new String[]
        { "Am I allowed to advertise other servers here?" }, new String[]
        { "No, you are not obviously." }, "No That is against the rules!", "Yes, of course I can!"), REPORT(new String[]
        { "If you see other players being flamed or harassed,", "what should you do?" }, new String[]
        { "You bully! Please report all harssasment to a moderator." }, "I should try to report the players to a moderator so they can help!", "I should run away and not care at all, I'm busy!"), EXPLOIT(new String[]
        { "Should I use bots and exploits to,", "give myself a unfair advantage?" }, new String[]
        { "Wrong! We ask that you report any exploits", "or bugs you may find within Asgarnia." }, "No, if I find a bug I should report it on the official website!", "Yes of course, I like causing trouble"), SPAMMING(new String[]
        { "Should i run around spamming nothing but non-sense?" }, new String[]
        { "Spamming is quite a nother to other players and we", "ask that you avoid trying to annoy people." }, "No, the rules state that spamming is wrong", "Yes, I should run around spamming"), SHARING(new String[]
        { "Should I share accounts with another", "player, or a friend for any reason?" }, new String[]
        { "Incorrect! You should never share your account.", "This could lead to you losing your items or even", "your entire account!" }, "No, you should never share accounts with another player", "Yes I share my account all the time!"), PRIVACY(new String[]
        { "Is it safe to submit your login information", "to another website?" }, new String[]
        { "No Way! Never trust third party websites!", "These websites are often phishing sites which will", "steal your password and pin!" }, "No, it's only safe on the official Asgarnia website", "Yes, all websites are safe with my personal information"), SECURITY(new String[]
        { "How should you keep you account safe?" }, new String[]
        { "Wrong! You should never give out your password", "for any reason!" }, "I should set a secure password and never give it out to anyone.", "I should shout out my password to other players!"); // password

        private String[] question;

        private String[] correction;

        private String[] answers;

        private Quiz(String[] question, String[] correction, String... answers)
        {
            this.question = question;
            this.correction = correction;
            this.answers = answers;
        }

        public static Quiz forOridnal(int idx)
        {
            for (Quiz quiz : Quiz.values())
                if (idx == quiz.ordinal())
                    return quiz;
            return null;
        }

        public String[] getQuestion()
        {
            return question;
        }

        public String[] getCorrection()
        {
            return correction;
        }

        public String[] getAnswers()
        {
            return answers;
        }

    }

}
