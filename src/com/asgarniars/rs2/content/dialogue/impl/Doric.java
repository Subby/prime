package com.asgarniars.rs2.content.dialogue.impl;

import com.asgarniars.rs2.content.dialogue.ChatDialogue;
import com.asgarniars.rs2.content.dialogue.Dialogue;
import com.asgarniars.rs2.content.dialogue.DialogueSession;
import com.asgarniars.rs2.content.dialogue.Option;
import com.asgarniars.rs2.content.dialogue.StatementDialogue;
import com.asgarniars.rs2.content.dialogue.StatementType;
import com.asgarniars.rs2.content.quest.Quest;
import com.asgarniars.rs2.content.quest.QuestRepository;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * 
 * @author Conner Kendall <Emnesty>
 * @author Pulse TODO: random dialogue
 */
public class Doric extends DialogueSession<Npc>
{

    public Doric(Player player, Npc[] actors)
    {
        super(player, actors);
    }

    @Override
    public Dialogue evaluate()
    {
        final Npc npc = (Npc) actors[0];
        final Quest quest = QuestRepository.get("DoricsQuest");

        if (player.getQuestStorage().hasStarted(quest))
        {
            Dialogue dialogue = new ChatDialogue(npc, null, "Have you got my materials yet, traveller?");

            if (!this.hasRequiredItems(new Item(434, +6), new Item(436, +4), new Item(440, +2)))
            {
                dialogue.add(player, null, "No, not yet...");
                dialogue.add(npc, null, "Well hurry!");
                return dialogue;
            }

            dialogue.add(player, null, "I have everything you need.");
            dialogue.add(npc, null, "Many thanks. Pass them here, please. I can spare you", "some coins for your trouble, and please use my anvils", "any time you want.");

            Dialogue message = new StatementDialogue(StatementType.NORMAL, new Task(1, true)
            {

                @Override
                protected void execute()
                {
                    /*
                     * for (short id : new short[] { 434, 436, 440 }) {
                     * player.getInventory().removeItem( new Item(id, 1)); }
                     */
                    player.getInventory().removeItem(new Item(434, 6));
                    player.getInventory().removeItem(new Item(436, 4));
                    player.getInventory().removeItem(new Item(440, 2));

                    player.setAttribute("currentQuest", quest);
                    player.getQuestStorage().setState(quest, 2);
                    this.stop();
                }

            }, "You hand the clay, copper, and iron to Doric.");
            dialogue.add(message);

            return dialogue;
        }

        Dialogue dialogue = new ChatDialogue(npc, null, "Hello traveller, what brings you to my humble smithy?");

        Option option_1 = new Option("I wanted to use your anvils", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(player, new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        player.setAttribute("currentQuest", quest);
                        this.stop();
                    }

                }, "I wanted to use your anvils.");

                dialogue.add(npc, null, "My anvils get enough work with my own use. I make", "pickaxes, and it takes a lot of hard work. If you could", "get me some more materials, then I could let you use", "them");

                Option yes = new Option("Yes, I will get you the materials.", new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        Dialogue dialogue = new ChatDialogue(player, new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                quest.start(player);
                                player.getQuestStorage().setState(quest, 1);
                                this.stop();
                            }

                        }, "Yes, I will get you the materials");

                        dialogue.add(npc, new Task(1, true)
                        {
                            @Override
                            protected void execute()
                            {
                                player.getInventory().addItem(new Item(1265, 1));
                                this.stop();
                            }
                        }, "Clay is what I use more than anything, to make casts.", "Could you get me 6 clay, 4 copper ore, and 2 iron ore,", "please? I could pay a little, and let you use my anvils.", "Take this pickaxe with you just in case you need it.");

                        Option one = new Option("Where can I find those?", new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                this.stop();
                            }

                        });

                        Option two = new Option("Certainly, I'll be right back!", new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                Dialogue dialogue = new ChatDialogue(player, null, "Certainly, I'll be right back!");
                                player.open(dialogue);
                                this.stop();
                            }

                        });

                        dialogue.add("Select an Option", one, two);
                        player.open(dialogue);
                        this.stop();
                    }

                });

                Option no = new Option("No, hitting rocks is for boring people, sorry.", new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        // TODO: Get Dialogue
                        this.stop();
                    }

                });

                dialogue.add("Select an Option", yes, no);
                player.open(dialogue);
                this.stop();
            }

        });
        Option option_2 = new Option("I want to use your whitestone.", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                // TODO: Get Dialogue
                this.stop();
            }

        });

        Option option_3 = new Option("Mind your own business, shortstuff!", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                // TODO: Get Dialogue
                this.stop();
            }

        });

        Option option_4 = new Option("I was just checking out the landscape.", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                // TODO: Get Dialogue
                this.stop();
            }

        });

        Option option_5 = new Option("What do you make here?", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                // TODO: Get Dialogue
                this.stop();
            }

        });

        dialogue.add("Select an Option", option_1, option_2, option_3, option_4, option_5);
        return dialogue;
    }

    private boolean hasRequiredItems(Item... items)
    {
        for (Item item : items)
        {
            if (player.getInventory().getItemContainer().getCount(item.getId()) != item.getCount())
            {
                return false;
            }
        }
        return true;
    }
}