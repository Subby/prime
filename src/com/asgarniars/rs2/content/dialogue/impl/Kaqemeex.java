package com.asgarniars.rs2.content.dialogue.impl;

import com.asgarniars.rs2.content.dialogue.ChatDialogue;
import com.asgarniars.rs2.content.dialogue.Dialogue;
import com.asgarniars.rs2.content.dialogue.DialogueSession;
import com.asgarniars.rs2.content.dialogue.Option;
import com.asgarniars.rs2.content.quest.Quest;
import com.asgarniars.rs2.content.quest.QuestRepository;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * 
 * @author Joshua Barry Class containing perfect dialogues, handle with extreme
 *         care xD
 * 
 */
public class Kaqemeex extends DialogueSession<Npc>
{

    public Kaqemeex(Player player, Npc[] actors)
    {
        super(player, actors);
    }

    @Override
    public Dialogue evaluate()
    {
        final Npc npc = actors[0];
        final Quest quest = QuestRepository.get("DruidicRitual");
        Option opt_who = new Option("Who are you?", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(player, null, "Who are you?");
                dialogue.add(npc, null, "We are the druids of Guthix. We worship our god at", "our famous stone circles. You will find them located", "throughout these lands.");
                Option circle = new Option("What about the stone circle full of dark wizards?", new Task(1, true)
                {
                    @Override
                    protected void execute()
                    {
                        Dialogue dialogue = new ChatDialogue(player, null, "What about the stone circle full of dark wizards?");
                        dialogue.add(npc, null, "That used to be OUR stone circle. Unfortunately,", "many many years ago, dark wizards cast a wicked spell", "upon it so that they could corrupt its power for their", "own evil ends.");
                        dialogue.add(npc, null, "When they cursed the rocks for their rituals they made", "them useless to us and our magics. We require a brave", "adventurer to go on a quest for us to help purify the", "circle of Varrock.");
                        final Option opt_ok = new Option("Ok, I will try and help.", new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                Dialogue dialogue = new ChatDialogue(player, new Task(1, true)
                                {
                                    @Override
                                    protected void execute()
                                    {
                                        player.setAttribute("currentQuest", quest);
                                        quest.start(player);
                                        player.getQuestStorage().setState(quest, 1);
                                        this.stop();
                                    }

                                }, "Ok, I will try and help.");
                                dialogue.add(npc, null, "Excellent. Go to the village south of this place and speak", "to my fellow Sanfew who is working on the purification", "ritual. He knows better than I what is required to", "complete it.");
                                dialogue.add(player, null, "Will do.");
                                player.open(dialogue);
                                this.stop();
                            }

                        });
                        final Option opt_no = new Option("No, that doesn't sound very interesting.", new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                Dialogue dialogue = new ChatDialogue(player, null, "No, that doesn't sound very intersting.");
                                dialogue.add(npc, null, "I will not try and change your mind adventurer. Some", "day when you have matured you may reconsider your", "position. We will wait until then.");
                                player.open(dialogue);
                                this.stop();

                            }

                        });
                        Option opt_greed = new Option("So... is there anything in this for me?", new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                Dialogue dialogue = new ChatDialogue(player, null, "So... is there anything in this for me?");
                                dialogue.add(npc, null, "We druids value wisdom over wealth, so if you expect", "material gain, you will be disappointed. We are, however,", "very skilled in the art of Herblore, which we will share", "with you");
                                dialogue.add(npc, null, "if you can assist us in this task. You may find such", "wisdom a greater reward that mere money.");
                                dialogue.add("Select an Option", opt_ok, opt_no);
                                player.open(dialogue);
                                this.stop();

                            }
                        });
                        dialogue.add("Select an Option", opt_ok, opt_no, opt_greed);
                        player.open(dialogue);
                        this.stop();
                    }
                });
                Option guthix = new Option("So what's good about Guthix?", new Task(1, true)
                {
                    @Override
                    protected void execute()
                    {
                        Dialogue dialogue = new ChatDialogue(player, null, "So what's good about Guthix?");
                        dialogue.add(npc, null, "Guthix is the oldest and most powerful god in", "Asgarnia. His existence is vital to this world. He is", "the god of balance, and nature; He is also a very part", "of this world.");
                        dialogue.add(npc, null, "He exists in the trees, and the flowers, the water and", "the rocks. He is everywhere. His purpose is to ensure", "balance in everything in this world, and as such we", "worship him");
                        dialogue.add(player, null, "He sounds kind of boring...");
                        dialogue.add(npc, null, "Some day when your mind achieves enlightenment you", "will see the true beauty of his power.");
                        player.open(dialogue);
                        this.stop();
                    }
                });
                Option disband = new Option("Well, I'll be on my way now.", new Task(1, true)
                {
                    @Override
                    protected void execute()
                    {
                        Dialogue dialogue = new ChatDialogue(player, null, "Well, I'll be on my way now.");
                        dialogue.add(npc, null, "Goodbye adventurer. I feel we shall meet again");
                        player.open(dialogue);
                        this.stop();
                    }
                });
                dialogue.add("Select an Option", circle, guthix, disband);
                player.open(dialogue);
                this.stop();

            }

        });
        final Option opt_quest = new Option("I'm in search of a quest.", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(player, null, "I'm in search of a quest.");
                dialogue.add(npc, null, "Hmm. I think I may have a worthwhile quest for you", "actually. I don't know if you are familiar with the stone", "circle south of Varrock or not, but...");
                dialogue.add(npc, null, "That used to be OUR stone circle. Unfortunately,", "many many years ago, dark wizards cast a wicked spell", "upon it so that they could corrupt its power for their", "own evil ends.");
                dialogue.add(npc, null, "When they cursed the rocks for their rituals they made", "them useless to us and our magics. We require a brave", "adventurer to go on a quest for us to help purify the", "circle of Varrock.");

                final Option opt_ok = new Option("Ok, I will try and help.", new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        Dialogue dialogue = new ChatDialogue(player, new Task(1, true)
                        {
                            @Override
                            protected void execute()
                            {
                                player.setAttribute("currentQuest", quest);
                                quest.start(player);
                                player.getQuestStorage().setState(quest, 1);
                                this.stop();
                            }

                        }, "Ok, I will try and help.");
                        dialogue.add(npc, null, "Excellent. Go to the village south of this place and speak", "to my fellow Sanfew who is working on the purification", "ritual. He knows better than I what is required to", "complete it.");
                        dialogue.add(player, null, "Will do.");
                        player.open(dialogue);
                        this.stop();
                    }

                });
                final Option opt_no = new Option("No, that doesn't sound very interesting.", new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        Dialogue dialogue = new ChatDialogue(player, null, "No, that doesn't sound very intersting.");
                        dialogue.add(npc, null, "I will not try and change your mind adventurer. Some", "day when you have matured you may reconsider your", "position. We will wait until then.");
                        player.open(dialogue);
                        this.stop();

                    }

                });
                Option opt_greed = new Option("So... is there anything in this for me?", new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        Dialogue dialogue = new ChatDialogue(player, null, "So... is there anything in this for me?");
                        dialogue.add(npc, null, "We druids value wisdom over wealth, so if you expect", "material gain, you will be disappointed. We are, however,", "very skilled in the art of Herblore, which we will share", "with you");
                        dialogue.add(npc, null, "if you can assist us in this task. You may find such", "wisdom a greater reward that mere money.");
                        dialogue.add("Select an Option", opt_ok, opt_no);
                        player.open(dialogue);
                        this.stop();

                    }
                });
                dialogue.add("Select an Option", opt_ok, opt_no, opt_greed);
                player.open(dialogue);
                this.stop();

            }

        });
        Option opt_build = new Option("Did you build this?", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(player, null, "Did you build this?");
                dialogue.add(npc, null, "What, personally? No, of course I didn't. However, our", "forefathers did. The first Druids of Guthix built many", "stone circles across these lands over eight hundred", "years ago.");
                dialogue.add(npc, null, "Unfortunately we only know of two remaining, and of", "those only one is usable by us anymore");
                Option circle = new Option("What about the stone circle full of dark wizards?", new Task(1, true)
                {
                    @Override
                    protected void execute()
                    {
                        Dialogue dialogue = new ChatDialogue(player, null, "What about the stone circle full of dark wizards?");
                        dialogue.add(npc, null, "That used to be OUR stone circle. Unfortunately,", "many many years ago, dark wizards cast a wicked spell", "upon it so that they could corrupt its power for their", "own evil ends.");
                        dialogue.add(npc, null, "When they cursed the rocks for their rituals they made", "them useless to us and our magics. We require a brave", "adventurer to go on a quest for us to help purify the", "circle of Varrock.");
                        final Option opt_ok = new Option("Ok, I will try and help.", new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                Dialogue dialogue = new ChatDialogue(player, new Task(1, true)
                                {
                                    @Override
                                    protected void execute()
                                    {
                                        player.setAttribute("currentQuest", quest);
                                        quest.start(player);
                                        player.getQuestStorage().setState(quest, 1);
                                        this.stop();
                                    }

                                }, "Ok, I will try and help.");
                                dialogue.add(npc, null, "Excellent. Go to the village south of this place and speak", "to my fellow Sanfew who is working on the purification", "ritual. He knows better than I what is required to", "complete it.");
                                dialogue.add(player, null, "Will do.");
                                player.open(dialogue);
                                this.stop();
                            }

                        });
                        final Option opt_no = new Option("No, that doesn't sound very interesting.", new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                Dialogue dialogue = new ChatDialogue(player, null, "No, that doesn't sound very intersting.");
                                dialogue.add(npc, null, "I will not try and change your mind adventurer. Some", "day when you have matured you may reconsider your", "position. We will wait until then.");
                                player.open(dialogue);
                                this.stop();

                            }

                        });
                        Option opt_greed = new Option("So... is there anything in this for me?", new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                Dialogue dialogue = new ChatDialogue(player, null, "So... is there anything in this for me?");
                                dialogue.add(npc, null, "We druids value wisdom over wealth, so if you expect", "material gain, you will be disappointed. We are, however,", "very skilled in the art of Herblore, which we will share", "with you");
                                dialogue.add(npc, null, "if you can assist us in this task. You may find such", "wisdom a greater reward that mere money.");
                                dialogue.add("Select an Option", opt_ok, opt_no);
                                player.open(dialogue);
                                this.stop();

                            }
                        });
                        dialogue.add("Select an Option", opt_ok, opt_no, opt_greed);
                        player.open(dialogue);
                        this.stop();
                    }
                });
                Option disband = new Option("Well, I'll be on my way now.", new Task()
                {
                    @Override
                    protected void execute()
                    {
                        Dialogue dialogue = new ChatDialogue(player, null, "Well, I'll be on my way now.");
                        dialogue.add(npc, null, "Goodbye adventurer. I feel we shall meet again.");
                        player.open(dialogue);
                        this.stop();
                    }
                });

                dialogue.add("Select an Option", circle, opt_quest, disband);

                player.open(dialogue);
                this.stop();

            }

        });

        Dialogue dialogue = new ChatDialogue(player, null, "Hello there.");
        if (player.getQuestStorage().hasStarted(quest))
            if (player.getQuestStorage().getState(quest) == 3)
            {
                dialogue.add(npc, new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        player.setAttribute("currentQuest", quest);
                        player.getQuestStorage().setState(quest, 4);
                        this.stop();

                    }

                }, "I have word from Sanfew that you have been very", "helpful in assisting him with his preparations for the", "purification ritual. As promised I will new teach you the", "ancient arts of Herblore.");
                return dialogue;
            }
        dialogue.add(npc, null, "What brings you to our holy monument?");

        // if (!player.getQuestStorage().hasStarted(quest))
        dialogue.add("Select an Option", opt_who, opt_quest, opt_build);
        return dialogue;
    }
}
