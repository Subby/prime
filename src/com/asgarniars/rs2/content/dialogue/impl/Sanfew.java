package com.asgarniars.rs2.content.dialogue.impl;

import com.asgarniars.rs2.content.dialogue.ChatDialogue;
import com.asgarniars.rs2.content.dialogue.Dialogue;
import com.asgarniars.rs2.content.dialogue.DialogueSession;
import com.asgarniars.rs2.content.dialogue.Option;
import com.asgarniars.rs2.content.quest.Quest;
import com.asgarniars.rs2.content.quest.QuestRepository;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class Sanfew extends DialogueSession<Npc>
{

    public Sanfew(Player player, Npc[] actors)
    {
        super(player, actors);
    }

    @Override
    public Dialogue evaluate()
    {
        final Npc npc = actors[0];
        final Quest quest = QuestRepository.get("DruidicRitual");

        if (player.getQuestStorage().hasStarted(quest))
            if (player.getQuestStorage().getState(quest) == 2)
            {
                Dialogue dialogue = new ChatDialogue(npc, null, "Did you bring me the required ingredients for the", "potion?");
                dialogue.add(player, null, "Yes, I have all four now!");
                dialogue.add(npc, null, "Well hand 'em over then lad!");
                dialogue.add(npc, new Task()
                {

                    @Override
                    protected void execute()
                    {
                        for (short id : new short[]
                        { 522, 523, 524, 525 })
                        {
                            player.getInventory().removeItem(new Item(id, 1));

                        }
                        player.getQuestStorage().setState(quest, 3);
                        this.stop();
                    }

                }, "Thank you so much adventurer! These meats will allow", "our potion to honour Guthix to be completed, and bring", "one step closer to reclaiming our stone circle!");
                dialogue.add(npc, null, "Now go and talk to Kaqemeex and he will introduce", "you to the wonderful world of herblore and potion", "making!");// wut
                                                                                                                                                             // do
                                                                                                                                                             // i
                                                                                                                                                             // need
                                                                                                                                                             // to
                                                                                                                                                             // do
                return dialogue;
            }
            else
            {
                Dialogue dialogue = new ChatDialogue(player, null, "No, not yet...");
                dialogue.add(npc, null, "Well let me know when you do young 'un.");
                final Option wut = new Option("What was I meant to be doing again?", new Task()
                {
                    @Override
                    protected void execute()
                    {
                        Dialogue dialogue = new ChatDialogue(player, null, "What was I meant to be doing again?");
                        dialogue.add(npc, null, "Trouble with your memory eh young 'un? I need the", "rat meats of four different animals that have been", "dipped into the Cauldron of Thunder so I can make", "my potion to honour Guthix.");
                        dialogue.add(player, null, "Ooooh yeah, I remember.");
                        final Option where = new Option("Where can I find this cauldron?", new Task()
                        {
                            @Override
                            protected void execute()
                            {
                                Dialogue dialogue = new ChatDialogue(player, null, "Where can I find this cauldron?");
                                dialogue.add(npc, null, "It is located somewhere in the mysterious underground", "halls which are located somewhere in the woods just", "South of here. They are too dangerous for me to go", "myself however.");
                            }
                        });
                        final Option then = new Option("Ok, I'll do that then.", new Task()
                        {
                            @Override
                            protected void execute()
                            {
                                Dialogue dialogue = new ChatDialogue(player, null, "Ok, I'll do that then.");
                                dialogue.add(npc, null, "Well thank you very much!");
                                player.open(dialogue);
                            }
                        });
                        dialogue.add("Select an Option", where, then);
                    }
                });
                final Option get = new Option("I'll get on with it", new Task()
                {
                    @Override
                    protected void execute()
                    {
                        Dialogue dialogue = new ChatDialogue(player, null, "I'll get on with it.");
                        dialogue.add(npc, null, "Good, good.");
                        player.open(dialogue);
                        this.stop();
                    }
                });
                dialogue.add("Select an Option", wut, get);
            }

        Dialogue dialogue = new ChatDialogue(npc, null, "What can I do for you, young 'un?");
        Option opt_1 = null;
        if (player.getQuestStorage().hasStarted(quest))
        {
            opt_1 = new Option("I've been sent to help purify the Varrock stone circle.", new Task(1, true)
            {

                @Override
                protected void execute()
                {
                    player.setAttribute("currentQuest", quest);
                    Dialogue dialogue = new ChatDialogue(player, null, "I've been sent to assist you with the ritual to purify the", "Varrockian stone circle.");
                    dialogue.add(npc, null, "Well, what I'm struggling with right now is the meats", "needed for the potion to honour Guthix. I need the raw", "meat of four different animals for it, but not just any", "old meats will do.");
                    dialogue.add(npc, null, "Each meat has to be dipped individually into the", "Cauldron of Thunder for it to work correctly.");
                    Option opt_1 = new Option("Where can I find this cauldron?", new Task(1, true)
                    {

                        @Override
                        protected void execute()
                        {
                            Dialogue dialogue = new ChatDialogue(player, null, "Where can I find this cauldron?");
                            dialogue.add(npc, new Task()
                            {

                                @Override
                                protected void execute()
                                {
                                    player.getQuestStorage().setState(quest, 2);
                                    this.stop();

                                }

                            }, "It is located somewhere in the mysterious underground", "halls which are located somewhere in the woods just", "South of here. They are too dangerous for me to go", "myself however.");
                            player.open(dialogue);
                            this.stop();
                        }

                    });
                    Option opt_2 = new Option("Ok, I'll do that then.", new Task(1, true)
                    {

                        @Override
                        protected void execute()
                        {
                            Dialogue dialogue = new ChatDialogue(player, null, "Ok, I'll do that then.");
                            player.open(dialogue);
                            this.stop();
                        }

                    });
                    dialogue.add("Select an Option", opt_1, opt_2);
                    player.open(dialogue);
                    this.stop();

                }

            });
        }
        else if (player.getQuestStorage().hasFinished(quest))
        {
            opt_1 = new Option("PLZ FIND MY REAL TXT BRO", new Task(1, true)
            {

                @Override
                protected void execute()
                {
                    this.stop();

                }

            });
        }
        else
            opt_1 = new Option("wut?", new Task(1, true)
            {

                @Override
                protected void execute()
                {
                    this.stop();

                }

            });

        Option opt_2 = new Option("Actually, I don't need to speak to you.", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(player, null, "Actually, I don't need to speak to you.");
                dialogue.add(npc, null, "Well, we all make mistakes sometimes.");
                player.getActionSender().sendMessage("Sanfew grunts");
                player.open(dialogue);
                this.stop();

            }
        });
        dialogue.add("Select an Option", opt_1, opt_2);
        return dialogue;
    }
}
