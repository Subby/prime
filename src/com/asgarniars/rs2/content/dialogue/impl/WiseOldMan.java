package com.asgarniars.rs2.content.dialogue.impl;

import com.asgarniars.rs2.action.impl.TeleportationAction.TeleportType;
import com.asgarniars.rs2.content.dialogue.ChatDialogue;
import com.asgarniars.rs2.content.dialogue.Dialogue;
import com.asgarniars.rs2.content.dialogue.DialogueSession;
import com.asgarniars.rs2.content.dialogue.Option;
import com.asgarniars.rs2.content.skills.magic.Teleport;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class WiseOldMan extends DialogueSession<Npc>
{

    /* The main Npc */
    private Npc npc;

    /* The 'sex' of the player */
    private String sex;

    /**
     * 
     * @param player
     * @param actors
     */
    public WiseOldMan(Player player, Npc[] actors)
    {
        super(player, actors);
        this.npc = actors[0];
        this.sex = (player.getGender() == 0) ? "man" : "lady";
    }

    @Override
    public Dialogue evaluate()
    {
        Dialogue dialogue = new ChatDialogue(npc, null, "Hello there, young " + sex + ". I am the Wise Old Man.", "As a loyal veteran to Prime, I am offering many", "services, as well as my wisdom to all players.");

        /* Teleporation service */
        Option teleportation = new Option("Teleportation", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                /* the teleportation speech */
                Dialogue dialogue = new ChatDialogue(npc, null, "My teleportation services are strictly recreational.", "This means I will only teleport you somewhere that will", "prove useful, to ensure my powers are not being abused.", "Make sure you are well prepared before you proceed.");
                Option a = new Option("Minigames", new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        Dialogue dialogue = new ChatDialogue(npc, null, "Which minigame would you like to attend?");
                        Option a = new Option("Fight Pits", new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                npc.getUpdateFlags().sendFaceToDirection(player.getPosition());
                                Teleport.initiate(player, npc, TeleportType.TELEOTHER, Teleport.FIGHT_PITS, 1);
                                this.stop();

                            }

                        });
                        Option b = new Option("Duel Arena", new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                npc.getUpdateFlags().sendFaceToDirection(player.getPosition());
                                Teleport.initiate(player, npc, TeleportType.TELEOTHER, Teleport.DUEL_ARENA, 4);
                                this.stop();

                            }

                        });
                        dialogue.add("Select an Minigame", a, b);
                        player.open(dialogue);
                        this.stop();

                    }

                });
                Option b = new Option("Skilling Areas", new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        Dialogue dialogue = new ChatDialogue(npc, null, "Which skilling area would you like to visit?");

                        Option a = new Option("Catherby", new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                npc.getUpdateFlags().sendFaceToDirection(player.getPosition());
                                Teleport.initiate(player, npc, TeleportType.TELEOTHER, Teleport.CATHERBY, 2);
                                this.stop();

                            }

                        });
                        Option b = new Option("Gnome Agility Course", new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                npc.getUpdateFlags().sendFaceToDirection(player.getPosition());
                                Teleport.initiate(player, npc, TeleportType.TELEOTHER, Teleport.GNOME_AGILITY_COURSE, 2);
                                this.stop();

                            }

                        });                        
                        dialogue.add("Select an Option", a, b);
                        player.open(dialogue);
                        this.stop();

                    }

                });
                Option c = new Option("Wilderness", new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        Dialogue dialogue = new ChatDialogue(npc, null, "Ahh, so you'd like to enter the wilderness...", "Beware, the wilderness is a very dangerous area full of", "vicious monsters and aggressive players who will try to kill", "you. Be sure you bring lots of replinishment supplies!");

                        Option a = new Option("Edgeville", new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                npc.getUpdateFlags().sendFaceToDirection(player.getPosition());
                                Teleport.initiate(player, npc, TeleportType.TELEOTHER, Teleport.EDGEVILLE_WILDERNESS, 2);
                                this.stop();

                            }

                        });
                        Option b = new Option("Mage Arena", new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                npc.getUpdateFlags().sendFaceToDirection(player.getPosition());
                                Teleport.initiate(player, npc, TeleportType.TELEOTHER, Teleport.EDGEVILLE_WILDERNESS, 2);
                                this.stop();

                            }

                        });                        

                        dialogue.add("Select an Option", a, b);
                        player.open(dialogue);
                        this.stop();

                    }

                });

                dialogue.add("Select an Option", a, b, c);

                player.open(dialogue);

                /* end the main speech */
                this.stop();

            }
        });

        /* Healing service */
        Option healing = new Option("Healing", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(npc, null, "Your mind, soul and body have been restored to a", "state of wellness and clarity.");
                player.open(dialogue);
                player.getSkill().normalize();
                player.setPoisoned(false);
                this.stop();

            }
        });

        /* Wisdom service */
        Option wisdom = new Option("Wisdom", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(npc, null, "What would you like to learn about?");

                Option wealth = new Option("Earning Money", new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        ChatDialogue dialogue = new ChatDialogue(npc, null, "There are many ways for a player to acquire", "wealth. The most reliable way to earn an income", "is to focus on independant sources of profitable", "items. There are three ways I suggest you do this...");
                        dialogue.add(npc, null, "The first way is to become a skiller. Skillers have", "the advantage of creating their own items. As a skiller", "you are an essential asset to maintaining the economy", "by crafting and collecting the items to meet the");
                        dialogue.add(npc, null, "needs of the community. Another way is to battle", "enemies that drop valuable items. Many of which can be", "found in the training areas provided by my teleportation", "services. These items can then be sold to other players");
                        dialogue.add(npc, null, "or converted to gold by the spell of an alchemist.", "There are also several single player minigames as well as", "bosses that can be tackled solo which offer great rewards.", "Some of the finest and most valuable items however,");
                        dialogue.add(npc, null, "are guarded by very powerful enemies that require", "team work to defeat. Please be fair to fellow players", "and share any loots that are earned evenly.");
                        dialogue.add(npc, null, "For further information, please refer to the", "guides section on the forums.");
                        player.open(dialogue);
                        this.stop();

                    }
                });
                Option contribute = new Option("What does Alpha mean?", new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        Dialogue dialogue = new ChatDialogue(npc, null, "Alpha is the first testing period of the game. The", "developers are working hard to find and repair any", "remaining bugs and glitches, as well as add any missing details");
                        dialogue.add(npc, null, "before proceeding to add additional content. It is", "encouraged for all players to report any bugs they may", "discover while playing, by visiting our official website.");
                        dialogue.add(npc, null, "Please do not be discouraged by any lack of content or", "flaws, they are sure to be fixed very soon.");
                        dialogue.add(npc, null, "Please be patient with the the developer.");
                        player.open(dialogue);
                        this.stop();

                    }
                });

                player.open(dialogue);
                dialogue.add("Select an Option", wealth, contribute);
                this.stop();

            }
        });

        dialogue.add(player, null, "What kinds of services are you offering?");
        dialogue.add("Select an Option", teleportation, healing, wisdom);

        return dialogue;
    }

}
