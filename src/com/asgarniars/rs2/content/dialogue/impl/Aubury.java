package com.asgarniars.rs2.content.dialogue.impl;

import com.asgarniars.rs2.content.dialogue.ChatDialogue;
import com.asgarniars.rs2.content.dialogue.Dialogue;
import com.asgarniars.rs2.content.dialogue.DialogueSession;
import com.asgarniars.rs2.content.dialogue.Option;
import com.asgarniars.rs2.content.dialogue.StatementDialogue;
import com.asgarniars.rs2.content.dialogue.StatementType;
import com.asgarniars.rs2.content.quest.Quest;
import com.asgarniars.rs2.content.quest.QuestRepository;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class Aubury extends DialogueSession<Npc>
{

    public Aubury(Player player, Npc[] actors)
    {
        super(player, actors);
    }

    @Override
    public Dialogue evaluate()
    {
        final Npc npc = actors[0];
        final Quest quest = QuestRepository.get("RuneMysteries");

        if (player.getQuestStorage().hasStarted(quest) && !player.getQuestStorage().hasFinished(quest))
            if (player.getQuestStorage().getState(quest) == 3)
            {
                Dialogue dialogue = new ChatDialogue(npc, null, "My gratitude to you adventurer for bringing me these", "research notes. I notice that you brought the head", "wizard a special talisman that was the key to our finally", "unlocking the puzzle.");
                dialogue.add(npc, null, "Combined with the information I had already collated", "regarding the Rune Essence, I think we have finally", "unlocked the power to");
                dialogue.add(npc, new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        player.setAttribute("currentQuest", quest);
                        player.getQuestStorage().setState(quest, 4);

                        this.stop();

                    }

                }, "...no. I am getting ahead of myself. Please take this", "summary of my research back to the head wizard at", "the Wizards' Tower. I trust his judgement on whether", "to let you in on our little secret or not.");
                Dialogue message = new StatementDialogue(StatementType.NORMAL, "Aubury gives you his research notes.");
                dialogue.add(message);
                return dialogue;
            }

        Dialogue dialogue = new ChatDialogue(npc, null, "Do you want to buy some runes?");
        Option opt_1 = new Option("Yes please!", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(player, null, "Wut?");
                player.open(dialogue);
                this.stop();
            }

        });
        Option opt_2 = new Option("Oh, it's a rune shop. No thank you, then.", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(player, null, "Oh, it's a rune shop. No thank you, then.");
                player.open(dialogue);
                this.stop();
            }

        });
        if (player.getQuestStorage().hasStarted(quest))
        {
            if (player.getQuestStorage().getState(quest) == 2)
            {
                Option opt_3 = new Option("I have been sent here with a package for you.", new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        Dialogue dialogue = new ChatDialogue(player, null, "I have been sent here with a package for you. It's from", "the head wizard at the Wizards' Tower.");
                        dialogue.add(npc, null, "Really? But... surely he can't have..? Please, let me,", "have it, it must be extremely important for him yo have", "sent a stranger.");
                        Dialogue message = new StatementDialogue(StatementType.NORMAL, "You hand Aubury the research package.");
                        dialogue.add(message);
                        dialogue.add(npc, new Task(1, true)
                        {

                            @Override
                            protected void execute()
                            {
                                if (player.getInventory().removeItem(new Item(290, 1)))
                                {
                                    player.getQuestStorage().setState(quest, 3);
                                    player.getActionSender().sendMessage("new points : " + player.getQuestStorage().getState(quest));
                                }
                                this.stop();

                            }

                        }, "This... this is incredible. Please, give me a few moments", "to quickly look over this, and then talk to me again.");
                        player.open(dialogue);
                        this.stop();
                    }

                });
                dialogue.add("Select an Option", opt_1, opt_2, opt_3);
            }
        }
        else
        {
            dialogue.add("Select an Option", opt_1, opt_2);
        }

        return dialogue;
    }
}
