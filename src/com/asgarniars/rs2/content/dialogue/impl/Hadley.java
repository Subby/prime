package com.asgarniars.rs2.content.dialogue.impl;

import com.asgarniars.rs2.content.dialogue.ChatDialogue;
import com.asgarniars.rs2.content.dialogue.Dialogue;
import com.asgarniars.rs2.content.dialogue.DialogueSession;
import com.asgarniars.rs2.content.dialogue.Option;
import com.asgarniars.rs2.content.quest.Quest;
import com.asgarniars.rs2.content.quest.QuestRepository;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * @author Joshua Barry <Sneakyhearts>
 * @author Dominick Jones
 * @author Conner Kendall <Emnesty>
 * 
 */
public class Hadley extends DialogueSession<Npc>
{

    public Hadley(Player player, Npc[] actors)
    {
        super(player, actors);
    }

    @Override
    public Dialogue evaluate()
    {
        final Npc npc = actors[0];
        final Quest quest = QuestRepository.get("WaterfallQuest");
        Dialogue dialogue = new ChatDialogue(player, null, "Hello there.");
        dialogue.add(npc, null, "Are you on holiday? If so you've come to the right", "place. I'm Hadley the tourist guide, anything you need", "to know just ask me we have some of the most unspoilt", "wildlife and scenery in Asgarnia.");
        dialogue.add(npc, null, "People come from miles around to fish in the clear lakes", "or to wonder the beautiful hill sides");
        dialogue.add(player, null, "It is quite pretty.");
        dialogue.add(npc, null, "Surely pretty is an understatement kind Sir. Beautiful,", "amazing or possibly life-changing would be more suitable", "wording. Have you seen the Baxtorian waterfall?", "Named after the elf king who was buried beneath.");
        Option elf_king = new Option("Can you tell me what happened to the elf king?", new Task(1, true)
        {
            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(player, null, "Can you tell me what happened to the elf king?");
                dialogue.add(npc, null, "There are many myths about Baxtorian. One popular", "story is this after defending his kingdom against the", "invading dark forces from the west. Baxtorian returned", "to find his wife Glarial had been captured by the");
                dialogue.add(npc, null, "enemy!");
                dialogue.add(npc, null, "This destroyed Baxtorian, after years of searching he", "became a recluse. In the secret home he had made for", "Glarial under the waterfall, he never came out and it is", "told that only Glarial could enter.");
                dialogue.add(player, null, "What happened to him?");
                dialogue.add(npc, null, "Oh, I don't know. I believe we have some pages on him", "upstairs in our archives. If you wish to look at them", "please be careful, they're all pretty delicate.");
                player.open(dialogue);
                this.stop();
            }
        });
        Option worth = new Option("Where else is worth visiting around here?", new Task(1, true)
        {
            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(player, null, "Where else is worth visiting around here?");
                dialogue.add(npc, null, "There is a lovely spot for a picnic on the hill to the", "north east, there lies a monument to the deceased elven", "queen Glarial. It really is quite pretty.");
                dialogue.add(player, null, "Who was queen Glarial?");
                dialogue.add(npc, null, "Baxtorian's wife, the only person who could also enter", "the waterfall. She was queen when this land was", "inhabited by elven kind. Glarial was kidnapped while");
                dialogue.add(npc, null, "Baxtorian was away, but they eventually recovered her", "body and brought her home to rest.");
                dialogue.add(player, null, "That's sad.");
                dialogue.add(npc, new Task(1, true)
                {

                    @Override
                    protected void execute()
                    {
                        player.getQuestStorage().setState(quest, 3);
                        this.stop();
                    }

                }, "True, I believe there's some information about her", "upstairs, if you look at them please be careful");
                player.open(dialogue);
                this.stop();
            }
        });
        Option inquire = new Option("Is there treasure under the waterfall?", new Task(1, true)
        {
            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(player, null, "Is there treasure under the waterfall?");
                dialogue.add(npc, null, "Ha ha... Another treasure hunter. Well if there is no", "one's been able to get it. They've been searching that", "river for decades, all to no avail.");
                player.open(dialogue);
                this.stop();
            }
        });
        Option goodbye = new Option("Thanks then, goodbye.", new Task(1, true)
        {
            @Override
            protected void execute()
            {
                Dialogue dialogue = new ChatDialogue(player, null, "Thanks then, goodbye.");
                dialogue.add(npc, null, "Enjoy your visit.");
                player.open(dialogue);
                this.stop();
            }
        });
        dialogue.add("Select an Option", elf_king, worth, inquire, goodbye);
        return dialogue;
    }
}
