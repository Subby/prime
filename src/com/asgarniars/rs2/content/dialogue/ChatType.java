package com.asgarniars.rs2.content.dialogue;

/**
 * The type of a chat dialogue.
 * 
 * @author Rait
 * 
 */
public enum ChatType
{

    /**
     * An NPC is talking.
     */
    NPC_CHAT,

    /**
     * A player is talking.
     */
    PLAYER_CHAT;

}
