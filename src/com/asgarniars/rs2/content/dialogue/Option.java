package com.asgarniars.rs2.content.dialogue;

import com.asgarniars.rs2.task.Task;

/**
 * A dialogue option.
 * 
 * @author Rait
 * 
 */
public class Option
{

    /**
     * The clickable text;
     */
    private String text;

    /**
     * The task performed when this option is clicked.
     */
    private Task task;

    /**
     * Construct an option.
     * 
     * @param text The text.
     * @param dialogue The following dialogue.
     * @param task The executed task.
     */
    public Option(String text, Task task)
    {
        this.text = text;
        this.task = task;
    }

    /**
     * Get the clickable text.
     * 
     * @return The clickable text.
     */
    public String getText()
    {
        return text;
    }

    /**
     * Gets the executed task.
     * 
     * @return The task to be executed.
     */
    public Task getTask()
    {
        return task;
    }

}
