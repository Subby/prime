package com.asgarniars.rs2.content.dialogue;

import java.util.ArrayDeque;
import java.util.Deque;

import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * Represents a simple dialogue.
 * 
 * @author Rait
 * 
 */
public abstract class Dialogue
{

    /**
     * This is simply the name on the top of the dialogue box, like the player
     * name or on option dialogues, the question-like thingie.
     */
    protected String name;

    /**
     * The task a dialogue performs when is opened.
     */
    protected Task task;

    /**
     * The lines of text rendered on the dialogue box.
     */
    protected String[] text;

    /**
     * The following dialogues.
     */
    private Deque<Dialogue> deque = new ArrayDeque<Dialogue>();

    /**
     * Adds a dialogue to the queue.
     * 
     * @param dialogue The dialogue to queue.
     */
    public void add(Dialogue dialogue)
    {
        deque.add(dialogue);
    }

    /**
     * Adds an option dialogue to the queue.
     * 
     * @param name The name of the dialogue.
     * @param options The options on the dialogue box.
     */
    public void add(String name, Option... options)
    {
        deque.add(new OptionDialogue(name, options));
    }

    /**
     * Adds an NPC chat dialogue to the queue.
     * 
     * @param npc The speaking npc.
     * @param text The spoken text.
     */
    public void add(Npc npc, Task task, String... text)
    {
        deque.add(new ChatDialogue(npc, task, text));
    }

    /**
     * Adds a player chat dialogue to the queue.
     * 
     * @param player The speaking player.
     * @param text The spoken text.
     */
    public void add(Player player, Task task, String... text)
    {
        deque.add(new ChatDialogue(player, task, text));
    }

    /**
     * Adds a statement dialogue to the queue.
     * 
     * @param type The type of the statement.
     * @param text The text displayed by the statement.
     */
    public void add(StatementType type, String... text)
    {
        deque.add(new StatementDialogue(type, text));
    }

    /**
     * Adds a statement dialogue to the queue.
     * 
     * @param type The type of the statement.
     * @param text The text displayed by the statement.
     */
    public void add(StatementType type, Task task, String... text)
    {
        deque.add(new StatementDialogue(type, task, text));
    }

    /**
     * Handle option/continue clicking.
     * 
     * @param index The index of the clicked button.
     * @param player The clicking player.
     */
    public abstract void click(int index, Player player);

    /**
     * Shows this dialogue to the player.
     * 
     * @param player The player to show the dialogue to.
     */
    public abstract void show(Player player);

    /**
     * Gets the name of the dialogue.
     * 
     * @return The name of the dialogue, which is the one displayed on the top
     *         of the dialogue box.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Gets the displayed lines of text on the chatbox.
     * 
     * @return A String array where each String is a separate line on the
     *         dialogue box.
     */
    public String[] getText()
    {
        return text;
    }

    /**
     * Gets the dialogue queue.
     * 
     * @return The queue.
     */
    public Deque<Dialogue> getDeque()
    {
        return deque;
    }

    /**
     * Gets the dialogue that follows this one.
     * 
     * @return The following dialogue, null if this is the last one in the
     *         chain.
     */
    public Dialogue getNextDialogue()
    {
        return deque.poll();
    }

}
