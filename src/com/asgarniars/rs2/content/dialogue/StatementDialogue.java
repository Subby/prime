package com.asgarniars.rs2.content.dialogue;

import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * A statement dialogue. This is just lines of text on a chatbox.
 * 
 * @author Rait
 * 
 */
public class StatementDialogue extends Dialogue
{

    /**
     * The text shown by this dialogue; the statement itself.
     */
    private String[] text;

    /**
     * The type of this statement.
     */
    private StatementType type;

    /**
     * Construct a statement dialogue.
     * 
     * @param type The type.
     * @param text The text.
     */
    public StatementDialogue(StatementType type, String... text)
    {
        this.text = text;
        this.type = type;
    }

    /**
     * Construct a statement dialogue.
     * 
     * @param type The type.
     * @param text The text.
     */
    public StatementDialogue(StatementType type, Task task, String... text)
    {
        this.text = text;
        this.type = type;
        this.task = task;
    }

    @Override
    public void click(int index, Player player)
    {
        if (type.equals(StatementType.TIMED))
        {
            return;
        }
        player.getActionSender().removeInterfaces();
    }

    @Override
    public void show(Player player)
    {
        if (task != null)
        {
            World.submit(task);
        }
        switch (type)
        {
        case NORMAL:
            switch (text.length)
            {
            case 1:
                player.getActionSender().sendString(text[0], 357);
                player.getActionSender().sendChatInterface(356);
                break;
            case 2:
                player.getActionSender().sendString(text[0], 360);
                player.getActionSender().sendString(text[1], 361);
                player.getActionSender().sendChatInterface(359);
                break;
            case 3:
                player.getActionSender().sendString(text[0], 364);
                player.getActionSender().sendString(text[1], 365);
                player.getActionSender().sendString(text[2], 366);
                player.getActionSender().sendChatInterface(363);
                break;
            case 4:
                player.getActionSender().sendString(text[0], 369);
                player.getActionSender().sendString(text[1], 370);
                player.getActionSender().sendString(text[2], 371);
                player.getActionSender().sendString(text[3], 372);
                player.getActionSender().sendChatInterface(368);
                break;
            case 5:
                player.getActionSender().sendString(text[0], 375);
                player.getActionSender().sendString(text[1], 376);
                player.getActionSender().sendString(text[2], 377);
                player.getActionSender().sendString(text[3], 378);
                player.getActionSender().sendString(text[4], 379);
                player.getActionSender().sendChatInterface(374);
                break;
            }
            break;
        case TIMED:
            switch (text.length)
            {
            case 1:
                player.getActionSender().sendString(text[0], 12789);
                player.getActionSender().sendChatInterface(12788);
                break;
            case 2:
                player.getActionSender().sendString(text[0], 12791);
                player.getActionSender().sendString(text[1], 12792);
                player.getActionSender().sendChatInterface(12790);
                break;
            case 3:
                player.getActionSender().sendString(text[0], 12794);
                player.getActionSender().sendString(text[1], 12795);
                player.getActionSender().sendString(text[2], 12796);
                player.getActionSender().sendChatInterface(12793);
                break;
            case 4:
                player.getActionSender().sendString(text[0], 12798);
                player.getActionSender().sendString(text[1], 12799);
                player.getActionSender().sendString(text[2], 12800);
                player.getActionSender().sendString(text[3], 12801);
                player.getActionSender().sendChatInterface(12797);
                break;
            case 5:
                player.getActionSender().sendString(text[0], 12803);
                player.getActionSender().sendString(text[1], 12804);
                player.getActionSender().sendString(text[2], 12805);
                player.getActionSender().sendString(text[3], 12806);
                player.getActionSender().sendString(text[4], 12807);
                player.getActionSender().sendChatInterface(12802);
                break;
            }
            break;
        }
    }

    /**
     * Gets the text.
     */
    public String[] getText()
    {
        return text;
    }

    /**
     * Gets the type of the statement.
     * 
     * @return The statement's type.
     */
    public StatementType getType()
    {
        return type;
    }

}
