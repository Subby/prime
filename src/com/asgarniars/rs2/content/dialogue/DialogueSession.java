package com.asgarniars.rs2.content.dialogue;

import com.asgarniars.rs2.model.players.Player;

/**
 * A chat session.
 * 
 * @author Rait
 * 
 */
@SuppressWarnings("all")
public abstract class DialogueSession<T>
{

    /**
     * The dialogue actors.
     */
    protected T[] actors;

    /**
     * The player.
     */
    protected Player player;

    /**
     * Construct a dialogue session.
     * 
     * @param player The player.
     * @param actors The other actors.
     */
    public DialogueSession(Player player, T... actors)
    {
        this.player = player;
        this.actors = actors;
    }

    /**
     * Initiates the dialogue session.
     */
    public void initiate()
    {
        player.open(evaluate());
    }

    /**
     * Get the dialogue itself.
     * 
     * @return The dialogue.
     */
    public abstract Dialogue evaluate();

    /**
     * Gets the actors of the dialogue.
     * 
     * @return The dialogue actors.
     */
    public T[] getActors()
    {
        return actors;
    }

    /**
     * Gets the player to send the dialogue to. The player can also be used as
     * an actor.
     * 
     * @return The player.
     */
    public Player getPlayer()
    {
        return player;
    }

}
