package com.asgarniars.rs2.content.dialogue;

import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.players.Player;

/**
 * An option dialogue where options are lines of text that you can click.
 * 
 * @author Rait
 * 
 */
public class OptionDialogue extends Dialogue
{

    /**
     * The options. There can only be 5 of those.
     */
    private Option[] options;

    /**
     * Construct an option dialogue.
     * 
     * @param options The options.
     */
    public OptionDialogue(String name, Option... options)
    {
        this.name = name;
        this.options = options;
    }

    @Override
    public void click(int index, Player player)
    {
        Option option = options[index];
        if (option == null)
        {
            player.getActionSender().sendMessage("Error with option dialogue - option at index " + index + "is null.");
            return;
        }
        World.submit(option.getTask());
        player.nextDialogue();
    }

    @Override
    public void show(Player player)
    {
        switch (options.length)
        {
        case 2:
            player.getActionSender().sendString(options[0].getText(), 2461);
            player.getActionSender().sendString(options[1].getText(), 2462);
            player.getActionSender().sendChatInterface(2459);
            break;
        case 3:
            player.getActionSender().sendString(options[0].getText(), 2471);
            player.getActionSender().sendString(options[1].getText(), 2472);
            player.getActionSender().sendString(options[2].getText(), 2473);
            player.getActionSender().sendChatInterface(2469);
            break;
        case 4:
            player.getActionSender().sendString(options[0].getText(), 2482);
            player.getActionSender().sendString(options[1].getText(), 2483);
            player.getActionSender().sendString(options[2].getText(), 2484);
            player.getActionSender().sendString(options[3].getText(), 2485);
            player.getActionSender().sendChatInterface(2480);
            break;
        case 5:
            player.getActionSender().sendString(options[0].getText(), 2494);
            player.getActionSender().sendString(options[1].getText(), 2495);
            player.getActionSender().sendString(options[2].getText(), 2496);
            player.getActionSender().sendString(options[3].getText(), 2497);
            player.getActionSender().sendString(options[4].getText(), 2498);
            player.getActionSender().sendChatInterface(2492);
            break;
        }
    }

    /**
     * Get the options on this dialogue.
     * 
     * @return This dialogue's options.
     */
    public Option[] getOptions()
    {
        return options;
    }

}
