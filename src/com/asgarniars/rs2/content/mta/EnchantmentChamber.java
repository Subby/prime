package com.asgarniars.rs2.content.mta;

import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * 
 * Handles the Enchantment Chambers logic.
 * 
 * @author Joshua Barry
 * 
 */
public class EnchantmentChamber extends EventRoom
{

    private static final short[] BONUS_COMPONENTS =
    { 15922, 15923, 15924, 15925 };
    private static final short[] BONUS_ITEMS =
    { 6899, 6898, 6901, 6900 };
    private static int bonusIndex = 0;
    private static int bonusId = BONUS_ITEMS[0];
    private static int currentComponent = BONUS_COMPONENTS[0];

    @Override
    public Task getEventCycle()
    {
        return new Task(15)
        {

            @Override
            protected void execute()
            {
                if (bonusIndex >= BONUS_COMPONENTS.length)
                {
                    bonusIndex = 0;
                }
                bonusId = BONUS_ITEMS[bonusIndex];
                currentComponent = BONUS_COMPONENTS[bonusIndex];
                for (Player player : Arena.getEnchanters())
                {
                    if (bonusIndex != 0)
                        player.getActionSender().showComponent(currentComponent - 1, false);
                    else
                        player.getActionSender().showComponent(BONUS_COMPONENTS[BONUS_COMPONENTS.length - 1], false);

                    player.getActionSender().showComponent(currentComponent, true);
                }
                bonusIndex++;
            }

        };
    }

    /**
     * 
     * @return
     */
    public static int getBonusId()
    {
        return bonusId;
    }

    /**
     * @return the currentComponent
     */
    public static int getCurrentComponent()
    {
        return currentComponent;
    }

}
