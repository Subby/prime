package com.asgarniars.rs2.content.mta;

import java.util.HashMap;
import java.util.Map;

import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;
import com.asgarniars.rs2.util.Misc;
import com.asgarniars.rs2.util.ScriptManager;

/**
 * 
 * Handles the Alchemist' Playground.
 * 
 * 
 * @author Joshua Barry
 * 
 */
public class AlchemistPlayground extends EventRoom
{

    /*
     * A Map can contain at most one entry per key, so when you call
     * map.put("key","newval"), the old "val" value is removed. To support
     * multiple values per key, you would need to maintain a Map<String,
     * List<String>> or else a multi-map implementation such as Guava's
     * Multimap.
     */

    private static Map<Integer, Integer> cupboardItems = new HashMap<Integer, Integer>();

    private static Map<Integer, Integer> rewardPrices = new HashMap<Integer, Integer>();

    private static final int[] objects =
    { 10783, 10785, 10787, 10789, 10791, 10793, 10795, 10797 };

    private static final int[] rewards =
    { 6893, 6894, 6895, 6896, 6897 };

    private static final short[] components =
    { 15907, 15908, 15909, 15910, 15911 };

    private static int componentIndex = 0;

    private static int bonusId = rewards[0];

    private static int currentComponent = components[0];

    static
    {
        int reward = rewards[0];
        for (int i = 0; i < objects.length; i++)
        {
            if (reward > rewards[rewards.length - 1])
                cupboardItems.put(objects[i], null);
            else
                cupboardItems.put(objects[i], reward);

            reward++;
        }

        for (int i = 0; i < rewards.length; i++)
            rewardPrices.put(rewards[i], Misc.randomMinMax(1, 31));
    }

    @Override
    public Task getEventCycle()
    {
        return new Task(66)
        {

            @Override
            protected void execute()
            {
                // updates the items in the cupboards in clockwise rotation.
                for (int i = 0; i < cupboardItems.size(); i++)
                {
                    if (objects[i] == objects[objects.length - 2])
                        cupboardItems.put(objects[i], cupboardItems.get(objects[0]));
                    else if (objects[i] == objects[objects.length - 1])
                        cupboardItems.put(objects[i], cupboardItems.get(objects[1]));
                    else
                        cupboardItems.put(objects[i], cupboardItems.get(objects[i + 2]));
                }

                for (int i = 0; i < rewardPrices.size(); i++)
                {
                    rewardPrices.put(rewards[i], Misc.randomMinMax(1, 31));
                }

                // update the prices.
                if (componentIndex >= components.length)
                {
                    componentIndex = 0;
                }
                bonusId = rewards[componentIndex];
                currentComponent = components[componentIndex];
                for (Player player : Arena.getAlchemists())
                {
                    if (componentIndex != 0)
                        player.getActionSender().showComponent(currentComponent - 1, false);
                    else
                        player.getActionSender().showComponent(components[components.length - 1], false);

                    ScriptManager.getSingleton().invokeWithFailTest("updateAlchemistInterface", player);
                    Arena.getAlchemyGuardian().getUpdateFlags().sendForceMessage("The costs are changing!");
                }
                componentIndex++;
            }

        };
    }

    /**
     * Mapping of objects and their contents.
     * 
     * @return the cupboard items.
     */
    public static Map<Integer, Integer> getCupboardItems()
    {
        return cupboardItems;
    }

    /**
     * A mapping of rewards and their prices.
     * 
     * @return
     */
    public static Map<Integer, Integer> getRewardPrices()
    {
        return rewardPrices;
    }

    /**
     * ID of the current component ID
     * 
     * @return the currentComponent
     */
    public static int getCurrentComponent()
    {
        return currentComponent;
    }

    /**
     * ID of the current bonus item.
     * 
     * @return
     */
    public static int getBonusId()
    {
        return bonusId;
    }

}
