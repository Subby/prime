package com.asgarniars.rs2.content.mta;

import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class CreatureGraveyard extends EventRoom
{

    @Override
    public Task getEventCycle()
    {
        return new Task(50)
        {

            @Override
            protected void execute()
            {
                for (Player player : Arena.getGraveyardMembers())
                    player.hit(2, 1, false);
            }

        };
    }

}
