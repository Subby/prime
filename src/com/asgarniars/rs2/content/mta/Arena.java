package com.asgarniars.rs2.content.mta;

import java.util.ArrayList;
import java.util.Collection;

import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.area.Area;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * Stores data for the mage training arena and handles it's members.
 * 
 * @author Joshua Barry
 * 
 */
public class Arena
{

    /**
     * A list of players who are enchanting.
     */
    private static Collection<Player> enchanters = new ArrayList<Player>();

    /**
     * A list of players who are in the playground.
     */
    private static Collection<Player> alchemists = new ArrayList<Player>();

    /**
     * A list of players who are in the playground.
     */
    private static Collection<Player> graveyardMembers = new ArrayList<Player>();

    /**
     * The chamber area.
     */
    private static final Area chamberArea = new Area(new Position(3343, 9619, 0), new Position(3384, 9660, 0));

    /**
     * The alchemy area.
     */
    private static final Area alchemyArea = new Area(new Position(3358, 9617, 2), new Position(3378, 9651, 2));

    private static Npc alchemyGuardian;

    /**
     * Starts an cyclic event.
     * 
     * @param tasks
     */
    public static void init(Task... tasks)
    {
        if (tasks == null)
            return;
        for (Task task : tasks)
            World.submit(task);
    }

    /**
     * @return the alchemists
     */
    public static Collection<Player> getAlchemists()
    {
        return alchemists;
    }

    /**
     * 
     * @return
     */
    public static Collection<Player> getEnchanters()
    {
        return enchanters;
    }

    /**
     * 
     * @return
     */
    public static Collection<Player> getGraveyardMembers()
    {
        return graveyardMembers;
    }

    /**
     * @return the chamberarea
     */
    public static Area getChamberArea()
    {
        return chamberArea;
    }

    /**
     * @return the alchemyarea
     */
    public static Area getAlchemyArea()
    {
        return alchemyArea;
    }

    public static Npc getAlchemyGuardian()
    {
        return alchemyGuardian;
    }

    public static void setAlchemyGuardian(Npc npc)
    {
        alchemyGuardian = npc;
    }

}
