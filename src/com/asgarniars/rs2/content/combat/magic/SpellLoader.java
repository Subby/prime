package com.asgarniars.rs2.content.combat.magic;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.io.XStreamController;

public class SpellLoader
{

    /**
     * How many spells are loaded.
     */
    public final static int spellCount = 40;

    /**
     * All the spell definitions.
     */
    private final static Logger logger = Logger.getLogger(SpellLoader.class.getName());
    private final static SpellDefinition[] spellDefinitions = new SpellDefinition[spellCount];

    @SuppressWarnings("unchecked")
    public static void loadSpellDefinitions() throws FileNotFoundException, IOException
    {
        List<SpellDefinition> list = (List<SpellDefinition>) XStreamController.getXStream().fromXML(new FileInputStream(Constants.CONFIG_DIRECTORY + "content/combat/spelldef.xml"));
        int count = 0;
        for (SpellDefinition def : list)
        {
            spellDefinitions[count] = def;
            count++;
        }
        logger.info("Loaded " + list.size() + " spell definitions.");
    }

    public static SpellDefinition[] getSpellDefinitions()
    {
        return spellDefinitions;
    }

}