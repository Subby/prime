package com.asgarniars.rs2.content.combat.magic;

import com.asgarniars.rs2.content.combat.Combat;
import com.asgarniars.rs2.content.combat.util.CombatState.CombatStyle;
import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.Entity.AttackTypes;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.model.players.Player.MagicBookTypes;
import com.asgarniars.rs2.util.Language;

/**
 * Magic class
 * 
 * @author AkZu & Jacob & Mikey (orig.)
 * 
 */
public class Magic
{

    private static Magic instance;

    public static Magic getInstance()
    {
        if (instance == null)
            instance = new Magic();
        return instance;
    }

    /**
     * Staff ids, and the runes they can substitute for.
     */
    private static final int[][] STAFFS =
    {
            // runeId, staffId's
            { 556, 1381, 1397, 1405 },
            { 555, 1383, 1395, 1403, 6562, 6563 },
            { 557, 1385, 1399, 1407, 6562, 6563, 3053, 3054 },
            { 554, 1387, 1393, 1401, 3053, 3054 } };

    /**
     * Check for a staff that corresponds to the rune required.
     */
    public boolean hasStaff(Player player, int runeId)
    {
        Item weapon = player.getEquipment().get(3);
        if (weapon == null)
            return false;
        for (int i = 0; i < STAFFS.length; i++)
        {
            if (STAFFS[i][0] == runeId)
            {
                for (int i2 = 0; i2 < STAFFS[i].length; i2++)
                    if (weapon.getId() == STAFFS[i][i2])
                        return true;
                return false;
            }
        }
        return false;
    }

    public void changeSpellbook(Player player, MagicBookTypes book)
    {
        player.setMagicBookType(book);
        player.getActionSender().sendSidebarInterface(6, (book == MagicBookTypes.ANCIENT ? 12855 : (book == MagicBookTypes.LUNAR ? 22654 : 1151)));
    }

    public boolean hasRequiredLevel(Player player, int lvl)
    {
        if (player.getSkill().getLevel()[Skill.MAGIC] >= lvl)
        {
            return true;
        }
        else
        {
            player.getActionSender().sendMessage("You need a Magic level of atleast " + lvl + " to cast this spell.");
            return false;
        }
    }

    /**
     * Checking for and removing runes before casting the spell.
     */
    public boolean removeRunes(Player player, int[] runes)
    {
        int[] runesRequired = runes;
        int[] runesToRemove = new int[runes.length];
        for (int i = 1; i < runesRequired.length; i += 2)
        {
            if (!hasStaff(player, runesRequired[i]))
            {
                if (player.getInventory().getItemContainer().getCount(runesRequired[i]) < runesRequired[i + 1])
                {
                    String runeName = ItemManager.getInstance().getItemName(runesRequired[i]).toLowerCase();
                    player.getActionSender().sendMessage("You do not have enough " + runeName + "s to cast this spell.");
                    return false;
                }
            }
            if (!hasStaff(player, runesRequired[i]))
            {
                runesToRemove[i] = runesRequired[i];
                runesToRemove[i + 1] = runesRequired[i + 1];
            }
        }
        for (int i = 1; i < runesToRemove.length; i += 2)
            if (runesToRemove[i] > 0)
                player.getInventory().removeItem(new Item(runesToRemove[i], runesToRemove[i + 1]));
        return true;
    }

    /**
     * START OF THE REAL MAGIC
     */

    public boolean attackWithMagic(Entity attacker, Entity victim, int magicId, boolean single_cast)
    {

        if (attacker == null)
            return false;

        if (victim == null && single_cast)
            return false;

        // -- Is attackable check..
        if (victim != null && victim.isNpc())
            if (!((Npc) victim).getDefinition().isAttackable() || ((Npc) victim).getDefinition().getCombatLevel(3) == 1)
            {
                if (attacker.isPlayer())
                    ((Player) attacker).getActionSender().sendMessage("You can't cast magic on that npc.");
                return false;
            }

        if (attacker.isPlayer())
        {

            Player player = (Player) attacker;

            int magicIndex = -1;

            for (int i = 0; i < SpellLoader.spellCount; i++)
            {
                if (magicId == SpellLoader.getSpellDefinitions()[i].getSpellId())
                    magicIndex = i;
            }

            if (magicIndex == -1)
                return false;

            int magicLevelRequired = SpellLoader.getSpellDefinitions()[magicIndex].getRequiredLevel();
            int[] runesRequired = SpellLoader.getSpellDefinitions()[magicIndex].getRunesRequired();

            if (!hasRequiredLevel(player, magicLevelRequired))
                return false;

            if (magicId == 1190 && (player.getEquipment().get(3) == null || player.getEquipment().get(3).getId() != 2415))
            {
                player.getActionSender().sendMessage("You need to be wearing Saradomin staff in order to use this spell.");
                return false;
            }

            if (magicId == 1191 && (player.getEquipment().get(3) == null || player.getEquipment().get(3).getId() != 2416))
            {
                player.getActionSender().sendMessage("You need to be wearing Guthix staff in order to use this spell.");
                return false;
            }

            if (magicId == 1192 && (player.getEquipment().get(3) == null || player.getEquipment().get(3).getId() != 2417))
            {
                player.getActionSender().sendMessage("You need to be wearing Zamorak staff in order to use this spell.");
                return false;
            }

            if (magicId == 12445 && victim.getAttribute("teleBlocked") != null)
            {
                if (System.currentTimeMillis() - (Long) victim.getAttribute("teleBlocked") < 300000)
                {
                    player.getActionSender().sendMessage("Your target is currently immune to that spell.");
                    return false;
                }
                else
                    victim.removeAttribute("teleBlocked");
            }

            if (single_cast && !removeRunes(player, runesRequired))
                return false;

            player.setAttackType(AttackTypes.MAGIC);

            if (single_cast)
            {
                player.setAttribute("SINGLE_CAST_CHANGE", magicIndex);
                player.setTarget(victim);
                player.setFollowingEntity(victim);
            }
            else
            {
                player.setAttribute("AUTO_CAST_CHANGE", magicIndex);
                player.getActionSender().updateAutoCastInterface(magicIndex);
            }

        } else {
        	//attacker is npc
        }
        return true;
    }

    public void changeMagicAttackStatus(Entity entity)
    {

        // -- Players
        if (entity.isPlayer())
        {
            Player player = (Player) entity;

            // -- Next attack will be single time executed
            if (player.getAttribute("SINGLE_CAST_CHANGE") != null)
            {
                player.setAttribute("SINGLE_CAST", player.getAttribute("SINGLE_CAST_CHANGE"));
                player.removeAttribute("SINGLE_CAST_CHANGE");
                player.setInstigatingAttack(true);
            }

            // -- Next attack will be autocasted
            if (player.getAttribute("AUTO_CAST_CHANGE") != null)
            {
                player.setAttribute("AUTO_CAST", player.getAttribute("AUTO_CAST_CHANGE"));
                player.removeAttribute("AUTO_CAST_CHANGE");
            }
        }
    }

    /**
     * Reseting magic
     */
    public void resetMagic(Entity attacker)
    {

        // -- Players
        if (attacker.isPlayer() && attacker.getAttribute("SINGLE_CAST") != null)
        {

            if (attacker.getAttribute("AUTO_CAST") == null)
            {
                Combat.getInstance().resetCombat(attacker);
                Combat.getInstance().fixAttackStyles((Player) attacker, (((Player) attacker).getEquipment().get(3)));
            }

            attacker.removeAttribute("SINGLE_CAST");
        }
    }

    /**
     * The buttons associated with autocasting.
     */
    public void handleAutocastButtons(Player player, int buttonId)
    {

        Item weapon = player.getEquipment().get(3);

        if (weapon == null)
            return;

        // -- Selecting a autocast spell to use and return back to weapon
        // interface..
        if (player.getAttribute("auto_cast_buttonId") != null)
        {
            for (int i = 0; i < SpellLoader.spellCount; i++)
            {

                if (buttonId == SpellLoader.getSpellDefinitions()[i].getAutoCastButton())
                {

                    attackWithMagic(player, null, SpellLoader.getSpellDefinitions()[i].getSpellId(), false);

                    if (player.getCombatState().getCombatStyle() != CombatStyle.AUTOCAST && player.getCombatState().getCombatStyle() != CombatStyle.DEFENSIVE_AUTOCAST)
                        player.setAttribute("oldCombatStyle", player.getCombatState().getCombatStyle().getId());

                    player.getCombatState().setCombatStyle(((Integer) player.getAttribute("auto_cast_buttonId") == 350 ? CombatStyle.AUTOCAST : CombatStyle.DEFENSIVE_AUTOCAST));

                    player.getEquipment().sendWeaponInterface();
                    return;
                }
            }
        }

        switch (buttonId)
        {

        case 350:
        case 1194:

            player.getActionSender().sendConfig((buttonId == 350 ? 950 : 108), 0);
            player.getActionSender().sendConfig((buttonId == 350 ? 108 : 950), 1);

            if (player.getCombatState().getCombatStyle() != CombatStyle.AUTOCAST && player.getCombatState().getCombatStyle() != CombatStyle.DEFENSIVE_AUTOCAST)
                player.setAttribute("oldCombatStyle", player.getCombatState().getCombatStyle().getId());

            if (player.getAttribute("auto_cast_buttonId") != null)
            {
                if ((Integer) player.getAttribute("auto_cast_buttonId") != buttonId)
                {
                    player.setAttribute("auto_cast_buttonId", buttonId);
                    player.getCombatState().setCombatStyle((buttonId == 350 ? CombatStyle.AUTOCAST : CombatStyle.DEFENSIVE_AUTOCAST));
                    return;
                }
                else if ((Integer) player.getAttribute("auto_cast_buttonId") == buttonId)
                {
                    Combat.getInstance().fixAttackStyles(player, player.getEquipment().get(3));
                    resetAll(player, false);
                    return;
                }
            }

            player.setAttribute("auto_cast_buttonId", buttonId);

            if (weapon.getId() == 4675)
            {
                if (player.getMagicBookType() == MagicBookTypes.ANCIENT)
                {
                    player.getActionSender().sendSidebarInterface(0, 1689);
                }
                else
                {
                    player.getActionSender().sendMessage(Language.CANT_AUTOCAST_MODERN);
                    resetAll(player, true);
                }
            }
            else
            {
                if (player.getMagicBookType() == MagicBookTypes.MODERN)
                {
                    player.getActionSender().sendSidebarInterface(0, 1829);
                }
                else
                {
                    player.getActionSender().sendMessage(Language.CANT_AUTOCAST_ANCIENT);
                    resetAll(player, true);
                }
            }
            break;

        case 2004:
        case 6161:
            if (player.getAttribute("AUTO_CAST_CHANGE") == null)
            {
                player.getEquipment().sendWeaponInterface();
                resetAll(player, false);
            }
            break;
        }
    }

    /**
     * Use this to grab the valid magicIndex
     */
    public int getMagicIndex(Entity attacker)
    {

        if (attacker.getAttribute("SINGLE_CAST") != null)
            return attacker.getAttribute("SINGLE_CAST");

        if (attacker.getAttribute("AUTO_CAST") != null)
            return attacker.getAttribute("AUTO_CAST");

        System.err.println(attacker.getAttackType() + " !!!!!!!Magic index faulty!!!!!!!!!");
        return 0;
    }

    public void resetAll(Entity attacker, boolean combat_reset)
    {
        attacker.removeAttribute("AUTO_CAST");
        attacker.removeAttribute("AUTO_CAST_CHANGE");
        attacker.removeAttribute("auto_cast_buttonId");
        if (combat_reset)
            Combat.getInstance().resetCombat(attacker);
        ((Player) attacker).getActionSender().resetAutoCastInterface();
    }
}