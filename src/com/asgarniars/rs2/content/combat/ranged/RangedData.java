package com.asgarniars.rs2.content.combat.ranged;

/**
 * Contains all ranged data.
 * 
 * @author Scu11, AkZu & Jacob
 */
public class RangedData
{

    /**
     * An enum that represents a type of range bow.
     * 
     * @author Michael
     * 
     */
    public static enum BowType
    {
        LONGBOW(new ArrowType[]
        { ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW }, new int[]
        { 9, 9, 10 }),

        SHORTBOW(new ArrowType[]
        { ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW }, new int[]
        { 6, 6, 8 }),

        OAK_SHORTBOW(new ArrowType[]
        { ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW }, new int[]
        { 6, 6, 8 }),

        OAK_LONGBOW(new ArrowType[]
        { ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW }, new int[]
        { 9, 9, 10 }),

        WILLOW_LONGBOW(new ArrowType[]
        { ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW, ArrowType.STEEL_ARROW }, new int[]
        { 9, 9, 10 }),

        WILLOW_SHORTBOW(new ArrowType[]
        { ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW, ArrowType.STEEL_ARROW }, new int[]
        { 6, 6, 8 }),

        MAPLE_LONGBOW(new ArrowType[]
        { ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW, ArrowType.STEEL_ARROW, ArrowType.MITHRIL_ARROW }, new int[]
        { 9, 9, 10 }),

        MAPLE_SHORTBOW(new ArrowType[]
        { ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW, ArrowType.STEEL_ARROW, ArrowType.MITHRIL_ARROW }, new int[]
        { 6, 6, 8 }),

        YEW_LONGBOW(new ArrowType[]
        { ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW, ArrowType.STEEL_ARROW, ArrowType.MITHRIL_ARROW, ArrowType.ADAMANT_ARROW }, new int[]
        { 9, 9, 10 }),

        YEW_SHORTBOW(new ArrowType[]
        { ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW, ArrowType.STEEL_ARROW, ArrowType.MITHRIL_ARROW, ArrowType.ADAMANT_ARROW }, new int[]
        { 6, 6, 8 }),

        MAGIC_LONGBOW(new ArrowType[]
        { ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW, ArrowType.STEEL_ARROW, ArrowType.MITHRIL_ARROW, ArrowType.ADAMANT_ARROW, ArrowType.RUNE_ARROW }, new int[]
        { 9, 9, 10 }),

        MAGIC_SHORTBOW(new ArrowType[]
        { ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW, ArrowType.STEEL_ARROW, ArrowType.MITHRIL_ARROW, ArrowType.ADAMANT_ARROW, ArrowType.RUNE_ARROW }, new int[]
        { 6, 6, 8 }),

        CRYSTAL_BOW(null, new int[]
        { 9, 9, 10 }),

        KARILS_XBOW(new ArrowType[]
        { ArrowType.BOLT_RACK }, new int[]
        { 6, 6, 8 }),

        DARK_BOW(new ArrowType[]
        { ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW, ArrowType.STEEL_ARROW, ArrowType.MITHRIL_ARROW, ArrowType.ADAMANT_ARROW, ArrowType.RUNE_ARROW, ArrowType.DRAGON_ARROW }, new int[]
        { 9, 9, 10 }),

        BLURITE_CBOW(new ArrowType[]
        { ArrowType.BLURITE_BOLT, ArrowType.JADE_BOLT }, new int[]
        { 6, 6, 8 }),

        BRONZE_CBOW(new ArrowType[]
        { ArrowType.BRONZE_BOLT, ArrowType.IRON_BOLT, ArrowType.OPAL_BOLT }, new int[]
        { 6, 6, 8 }),

        IRON_CBOW(new ArrowType[]
        { ArrowType.BRONZE_BOLT, ArrowType.IRON_BOLT, ArrowType.PEARL_BOLT, ArrowType.OPAL_BOLT }, new int[]
        { 6, 6, 8 }),

        STEEL_CBOW(new ArrowType[]
        { ArrowType.BRONZE_BOLT, ArrowType.IRON_BOLT, ArrowType.STEEL_BOLT, ArrowType.TOPAZ_BOLT, ArrowType.PEARL_BOLT, ArrowType.OPAL_BOLT }, new int[]
        { 6, 6, 8 }),

        MITH_CBOW(new ArrowType[]
        { ArrowType.BRONZE_BOLT, ArrowType.IRON_BOLT, ArrowType.STEEL_BOLT, ArrowType.MITHRIL_BOLT, ArrowType.SAPPHIRE_BOLT, ArrowType.EMERALD_BOLT, ArrowType.TOPAZ_BOLT, ArrowType.PEARL_BOLT, ArrowType.OPAL_BOLT }, new int[]
        { 6, 6, 8 }),

        ADAMANT_CBOW(new ArrowType[]
        { ArrowType.BRONZE_BOLT, ArrowType.IRON_BOLT, ArrowType.STEEL_BOLT, ArrowType.MITHRIL_BOLT, ArrowType.ADAMANT_BOLT, ArrowType.RUBY_BOLT, ArrowType.DIAMOND_BOLT, ArrowType.SAPPHIRE_BOLT, ArrowType.EMERALD_BOLT, ArrowType.TOPAZ_BOLT, ArrowType.PEARL_BOLT, ArrowType.OPAL_BOLT }, new int[]
        { 6, 6, 8 }),

        RUNE_CBOW(new ArrowType[]
        { ArrowType.BRONZE_BOLT, ArrowType.IRON_BOLT, ArrowType.STEEL_BOLT, ArrowType.MITHRIL_BOLT, ArrowType.ADAMANT_BOLT, ArrowType.RUNE_BOLT, ArrowType.DRAGONSTONE_BOLT, ArrowType.ONYX_BOLT, ArrowType.RUBY_BOLT, ArrowType.DIAMOND_BOLT, ArrowType.SAPPHIRE_BOLT, ArrowType.EMERALD_BOLT, ArrowType.TOPAZ_BOLT, ArrowType.PEARL_BOLT, ArrowType.OPAL_BOLT }, new int[]
        { 6, 6, 8 });

        /**
         * The arrows this bow can use.
         */
        private ArrowType[] validArrows;

        /**
         * The distances required to be near the victim based on the mob's
         * combat style.
         */
        private int[] distances;

        private BowType(ArrowType[] validArrows, int[] distances)
        {
            this.validArrows = validArrows;
            this.distances = distances;
        }

        /**
         * Gets the valid arrows this bow can use.
         * 
         * @return The valid arrows this bow can use.
         */
        public ArrowType[] getValidArrows()
        {
            return validArrows;
        }

        /**
         * Gets a valid arrow this bow can use by its index.
         * 
         * @param index The arrow index.
         * @return The valid arrow this bow can use by its index.
         */
        public ArrowType getValidArrow(int index)
        {
            return validArrows[index];
        }

        /**
         * Gets a distance required to be near the victim.
         * 
         * @param index The combat style index.
         * @return The distance required to be near the victim
         */
        public int getDistance(int index)
        {
            return distances[index];
        }
    }

    /**
     * An enum for all arrow types, this includes the drop rate percentage of
     * this arrow (the higher quality the less likely it is to disappear).
     * 
     * @author Michael Bull
     * @author Sir Sean
     */
    public static enum ArrowType
    {

        BRONZE_ARROW(0.75, 19, 10, 7),

        IRON_ARROW(0.7, 18, 9, 10),

        STEEL_ARROW(0.65, 20, 11, 16),

        MITHRIL_ARROW(0.6, 21, 12, 22),

        ADAMANT_ARROW(0.5, 22, 13, 31),

        RUNE_ARROW(0.4, 24, 15, 49),

        BOLT_RACK(1.1, -1, 27, 55),

        DRAGON_ARROW(0.3, 1111, 1115, 60),

        BLURITE_BOLT(0.75, -1, 27, 28),

        BRONZE_BOLT(0.75, -1, 27, 10),

        BARBED_BOLT(0.75, -1, 27, 12),

        OPAL_BOLT(0.75, -1, 27, 14),

        JADE_BOLT(0.75, -1, 27, 30),

        IRON_BOLT(0.7, -1, 27, 46),

        PEARL_BOLT(0.75, -1, 27, 48),

        STEEL_BOLT(0.65, -1, 27, 64),

        TOPAZ_BOLT(0.65, -1, 27, 66),

        MITHRIL_BOLT(0.6, -1, 27, 82),

        SAPPHIRE_BOLT(0.6, -1, 27, 83),

        EMERALD_BOLT(0.6, -1, 27, 85),

        ADAMANT_BOLT(0.5, -1, 27, 100),

        RUBY_BOLT(0.5, -1, 27, 103),

        DIAMOND_BOLT(0.5, -1, 27, 105),

        RUNE_BOLT(0.4, -1, 27, 115),

        DRAGONSTONE_BOLT(0.4, -1, 27, 117),

        ONYX_BOLT(0.4, -1, 27, 120);

        /**
         * The percentage chance for the arrow to disappear once fired.
         */
        private double dropRate;

        /**
         * The pullback graphic.
         */
        private int pullback;

        /**
         * The projectile id.
         */
        private int projectile;

        /**
         * The projectile strength (used in ranged maxhit formula).
         */
        private double projectileStrength;

        private ArrowType(double dropRate, int pullback, int projectile, int arrowStrenght)
        {
            this.dropRate = dropRate;
            this.pullback = pullback;
            this.projectile = projectile;
            this.projectileStrength = arrowStrenght;
        }

        /**
         * Gets the arrow's percentage chance to disappear once fired
         * 
         * @return The arrow's percentage chance to disappear once fired.
         */
        public double getDropRate()
        {
            return dropRate;
        }

        /**
         * Gets the arrow's pullback graphic.
         * 
         * @return The arrow's pullback graphic.
         */
        public int getPullbackGraphic()
        {
            return pullback;
        }

        /**
         * Gets the arrow's projectile id.
         * 
         * @return The arrow's projectile id.
         */
        public int getProjectileId()
        {
            return projectile;
        }

        /**
         * Gets the ammo's ranged strength bonus.
         * 
         * @return The ammo's strength bonus.
         */
        public double getProjectileStrength()
        {
            return projectileStrength;
        }
    }

    /**
     * An enum that represents all range weapons, e.g. throwing knives and
     * javelins.
     * 
     * @author Michael
     * 
     */
    public static enum RangeWeaponType
    {

        BRONZE_KNIFE(0.75, 219, 212, new int[]
        { 4, 4, 6 }, 3),

        IRON_KNIFE(0.7, 220, 213, new int[]
        { 4, 4, 6 }, 4),

        STEEL_KNIFE(0.65, 221, 214, new int[]
        { 4, 4, 6 }, 7),

        MITHRIL_KNIFE(0.6, 223, 216, new int[]
        { 4, 4, 6 }, 10),

        ADAMANT_KNIFE(0.5, 224, 217, new int[]
        { 4, 4, 6 }, 14),

        RUNE_KNIFE(0.4, 225, 218, new int[]
        { 4, 4, 6 }, 24),

        BLACK_KNIFE(0.6, 222, 215, new int[]
        { 4, 4, 6 }, 8),

        BRONZE_DART(0.75, 1234, 226, new int[]
        { 3, 3, 5 }, 1),

        IRON_DART(0.7, 1235, 227, new int[]
        { 3, 3, 5 }, 3),

        STEEL_DART(0.65, 1236, 228, new int[]
        { 3, 3, 5 }, 4),

        MITHRIL_DART(0.6, 1237, 229, new int[]
        { 3, 3, 5 }, 7),

        ADAMANT_DART(0.5, 1239, 230, new int[]
        { 3, 3, 5 }, 10),

        RUNE_DART(0.4, 1240, 231, new int[]
        { 3, 3, 5 }, 14),

        BLACK_DART(0.6, 1238, 34, new int[]
        { 3, 3, 5 }, 6),

        DRAGON_DART(0.3, 1123, 1122, new int[]
        { 3, 3, 5 }, 20),

        BRONZE_THROWNAXE(0.75, 42, 36, new int[]
        { 4, 4, 6 }, 5),

        IRON_THROWNAXE(0.7, 43, 35, new int[]
        { 4, 4, 6 }, 7),

        STEEL_THROWNAXE(0.65, 44, 37, new int[]
        { 4, 4, 6 }, 11),

        MITHRIL_THROWNAXE(0.6, 45, 38, new int[]
        { 4, 4, 6 }, 16),

        ADAMANT_THROWNAXE(0.5, 46, 39, new int[]
        { 4, 4, 6 }, 23),

        RUNE_THROWNAXE(0.4, 48, 41, new int[]
        { 4, 4, 6 }, 36),

        BRONZE_JAVELIN(0.75, 206, 200, new int[]
        { 4, 4, 6 }, 6),

        IRON_JAVELIN(0.7, 207, 201, new int[]
        { 4, 4, 6 }, 10),

        STEEL_JAVELIN(0.65, 208, 202, new int[]
        { 4, 4, 6 }, 12),

        MITHRIL_JAVELIN(0.6, 209, 203, new int[]
        { 4, 4, 6 }, 18),

        ADAMANT_JAVELIN(0.5, 210, 204, new int[]
        { 4, 4, 6 }, 28),

        RUNE_JAVELIN(0.4, 211, 205, new int[]
        { 4, 4, 6 }, 42),

        OBSIDIAN_RING(0.45, -1, 442, new int[]
        { 6, 6, 8 }, 49);

        /**
         * The percentage chance for the arrow to disappear once fired.
         */
        private double dropRate;

        /**
         * The pullback graphic.
         */
        private int pullback;

        /**
         * The projectile id.
         */
        private int projectile;

        /**
         * The distances required for each attack type.
         */
        private int[] distances;

        /**
         * The projectile strength (used in ranged maxhit formula).
         */
        private double projectileStrength;

        private RangeWeaponType(double dropRate, int pullback, int projectile, int[] distances, double projectileStrength)
        {
            this.dropRate = dropRate;
            this.pullback = pullback;
            this.projectile = projectile;
            this.distances = distances;
            this.projectileStrength = projectileStrength;
        }

        /**
         * @return the dropRate
         */
        public double getDropRate()
        {
            return dropRate;
        }

        /**
         * @return the pullback
         */
        public int getPullbackGraphic()
        {
            return pullback;
        }

        /**
         * @return the projectile
         */
        public int getProjectileId()
        {
            return projectile;
        }

        /**
         * @return the distances
         */
        public int[] getDistances()
        {
            return distances;
        }

        /**
         * @return the distances
         */
        public int getDistance(int index)
        {
            return distances[index];
        }

        /**
         * @return The ranged_weapon's strength bonus.
         */
        public double getProjectileStrength()
        {
            return projectileStrength;
        }
    }
}