package com.asgarniars.rs2.content.combat.ranged;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.io.XStreamController;

public class RangedDataLoader
{

    private final static Logger logger = Logger.getLogger(RangedDataLoader.class.getName());
    private static HashMap<Integer, RangeProjectiles> projectiles;

    public static RangeProjectiles getProjectileId(int id)
    {
        return projectiles.get(id);
    }

    @SuppressWarnings("unchecked")
    public static void loadRangedDefinitions() throws FileNotFoundException
    {
        projectiles = new HashMap<Integer, RangeProjectiles>();
        XStreamController.getInstance();
        List<RangeProjectiles> loaded = (ArrayList<RangeProjectiles>) XStreamController.getXStream().fromXML(new FileInputStream(Constants.CONFIG_DIRECTORY + "content/combat/rangeddef.xml"));

        for (RangeProjectiles projectile : loaded)
        {
            for (int id : projectile.getID())
            {
                projectiles.put(id, projectile);
            }
        }
        logger.info("Loaded " + projectiles.size() + " ranged definitions.");
    }
}