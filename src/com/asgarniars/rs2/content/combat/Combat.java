package com.asgarniars.rs2.content.combat;

import java.util.Random;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.content.combat.magic.Magic;
import com.asgarniars.rs2.content.combat.magic.SpellEffects;
import com.asgarniars.rs2.content.combat.magic.SpellLoader;
import com.asgarniars.rs2.content.combat.magic.SpellDefinition.MagicTypes;
import com.asgarniars.rs2.content.combat.ranged.RangedDataLoader;
import com.asgarniars.rs2.content.combat.ranged.RangedData.ArrowType;
import com.asgarniars.rs2.content.combat.ranged.RangedData.BowType;
import com.asgarniars.rs2.content.combat.ranged.RangedData.RangeWeaponType;
import com.asgarniars.rs2.content.combat.util.CombatConstants;
import com.asgarniars.rs2.content.combat.util.DetermineHit;
import com.asgarniars.rs2.content.combat.util.Poison;
import com.asgarniars.rs2.content.combat.util.Prayer;
import com.asgarniars.rs2.content.combat.util.Skulling;
import com.asgarniars.rs2.content.combat.util.SpecialAttack;
import com.asgarniars.rs2.content.combat.util.CombatState.CombatStyle;
import com.asgarniars.rs2.content.combat.util.Weapons.WeaponLoader;
import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.Entity.AttackTypes;
import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.area.Areas;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;
import com.asgarniars.rs2.util.Language;
import com.asgarniars.rs2.util.Misc;
import com.asgarniars.rs2.util.clip.RegionClipping;

public class Combat
{

    private static Combat instance;

    public static Combat getInstance()
    {
        if (instance == null)
            instance = new Combat();
        return instance;
    }

    public void start_attack(Entity attacker, Entity victim)
    {
        attacker.setTarget(victim);
        attacker.setInstigatingAttack(true);
        attacker.setFollowingEntity(victim);
    }

    // TODO This handles the delayed hits.
    public void delayHit(final Entity attacker, final Entity victim, final int delay, final boolean immediate, final boolean isSpecial, int hitAmount, final CombatStyle combatStyle, final AttackTypes attackType, final int magicIndex, final Item weapon, final Item arrows)
    {

        int delayForSpells = 0;

        if (attackType == AttackTypes.MAGIC)
        {
            switch (SpellLoader.getSpellDefinitions()[magicIndex].getSpellId())
            {
            case 12901:
            case 12939:
            case 12861:
            case 12987:
            case 12963:
            case 12919:
            case 13011:
            case 12881:
            case 12975:
            case 12951:
            case 12999:
                delayForSpells = 1;
                break;
            case 12911:
                delayForSpells = 2;
                break;
            case 12871:
                delayForSpells = 3;
                break;
            }
        }

        final int spellDelay = delayForSpells;

        ArrowType arrowType = null;

        if (attackType == AttackTypes.RANGED)
        {

            if (arrows != null)
                arrowType = RangedDataLoader.getProjectileId(arrows.getId()).getArrowType();
        }

        // Handle bolt effects that boost damage or do other effects than
        // accuary here.
        /*
         * if (attackType == AttackTypes.RANGED) { if (arrowType != null &&
         * hitAmount > 0 && Misc.random(11) >= 2) { if (arrowType ==
         * ArrowType.OPAL_BOLT) { if (victim.isPlayer()) {
         * victim.getUpdateFlags().sendGraphic(749, 30); hitAmount +=
         * Misc.random(5); } } if (arrowType == ArrowType.JADE_BOLT) {
         * victim.getUpdateFlags().sendGraphic(755); victim.setCanWalk(false);
         * // TODO Not real anim but fits well xD if (victim.isPlayer())
         * victim.getUpdateFlags().sendAnimation(2412); World.submit(new Task(4)
         * {
         * 
         * @Override protected void execute() { victim.setCanWalk(true);
         * this.stop(); } }); } // TODO Pearl bolts, npcIsFieryBeast check.. //
         * if (arrowType == ArrowType.PEARL_BOLT &&
         * !((CombatItemEffects.HAS_CORRECT_EQUIPMENT(victim, 1403) ||
         * CombatItemEffects.HAS_CORRECT_EQUIPMENT(victim, 1383) ||
         * CombatItemEffects.HAS_CORRECT_EQUIPMENT(victim, 1395)))) {
         * victim.getUpdateFlags().sendGraphic(750); hitAmount +=
         * Misc.random(5); if (victim.isPlayer()) { for (byte k = 0; k < 14;
         * k++) { if (((Player) victim).getEquipment().get(k) != null &&
         * ((Player)
         * victim).getEquipment().get(k).getDefinition().getName().toLowerCase
         * ().contains("fire")) { hitAmount += Misc.random(5); } } } } if
         * (arrowType == ArrowType.TOPAZ_BOLT) { if (victim.isPlayer()) {
         * victim.getUpdateFlags().sendGraphic(757);
         * victim.getSkill().decreaseLevel(6, (int)
         * (victim.getSkill().getLevelForXP(victim.getSkill().getExp()[5]) *
         * 0.05)); } } if (arrowType == ArrowType.SAPPHIRE_BOLT) { if
         * (victim.isPlayer()) { victim.getUpdateFlags().sendGraphic(751); int
         * amount = (int)
         * (victim.getSkill().getLevelForXP(victim.getSkill().getExp()[5]) *
         * 0.05); victim.getSkill().decreaseLevel(5, amount);
         * attacker.getSkill().increaseLevel(5, (amount)); } } if (arrowType ==
         * ArrowType.EMERALD_BOLT) { victim.getUpdateFlags().sendGraphic(752);
         * Poison.appendPoison(victim, true, 5); } // TODO Ruby bolts if
         * (arrowType == ArrowType.RUBY_BOLT) {
         * victim.getUpdateFlags().sendGraphic(754);
         * attacker.getSkill().decreaseLevel(3, (int) (attacker.getSkillLevel(3)
         * * 0.1)); hitAmount += (int) (victim.getSkillLevel(3) * 0.2); } //
         * Onyx bolts // TODO When npc support, no undeads. if (arrowType ==
         * ArrowType.ONYX_BOLT) { victim.getUpdateFlags().sendGraphic(753);
         * hitAmount += Misc.random(7); if (attacker.getSkillLevel(3) +
         * (hitAmount / 4) <=
         * attacker.getSkill().getLevelForXP(attacker.getSkill().getExp()[3])) {
         * attacker.getSkill().increaseLevel(3, (hitAmount / 4)); } else {
         * attacker.getSkill().increaseLevel(3,
         * attacker.getSkill().getLevelForXP(attacker.getSkill().getExp()[3]) -
         * attacker.getSkillLevel(3)); } } // Dragon bolts // TODO Antifire
         * protection check, 'fiery beast' (jad, dragons) // check if (arrowType
         * == ArrowType.DRAGONSTONE_BOLT) { if (!((victim.isPlayer() &&
         * CombatItemEffects.HAS_ANTIFIRE_SHIELD(victim)))) {
         * victim.getUpdateFlags().sendGraphic(756, 30); // TODO Isn't there any
         * better way?? if (victim.getSkillLevel(1) < 10) { hitAmount += 20; }
         * else if (victim.getSkillLevel(1) < 20) { hitAmount += 18 +
         * Misc.random(2); } else if (victim.getSkillLevel(1) < 30) { hitAmount
         * += 16 + Misc.random(4); } else if (victim.getSkillLevel(1) < 40) {
         * hitAmount += 14 + Misc.random(6); } else if (victim.getSkillLevel(1)
         * < 50) { hitAmount += 12 + Misc.random(8); } else if
         * (victim.getSkillLevel(1) < 60) { hitAmount += 10 + Misc.random(10); }
         * else if (victim.getSkillLevel(1) < 70) { hitAmount += 8 +
         * Misc.random(12); } else if (victim.getSkillLevel(1) < 80) { hitAmount
         * += 6 + Misc.random(14); } else if (victim.getSkillLevel(1) < 90) {
         * hitAmount += 4 + Misc.random(6); } else { hitAmount += 2 +
         * Misc.random(18); } } } } }
         */

        final int realHit = hitAmount;

        final ArrowType realArrowType = arrowType;

        World.submit(new Task(delay, immediate)
        {

            @Override
            protected void execute()
            {

                if (attacker == null || victim == null)
                {
                    this.stop();
                    return;
                }

                int hit = DetermineHit.determineHit(attacker, victim, realHit, isSpecial);

                if (hit == 0 && attackType == AttackTypes.MAGIC)
                {
                    if (spellDelay >= 2)
                    {
                        World.submit(new Task(1, (spellDelay == 3 ? false : true))
                        {

                            @Override
                            protected void execute()
                            {
                                victim.getUpdateFlags().sendGraphic(85, 0, 100);
                                completeDelayedHit(attacker, victim, 0, combatStyle, attackType, magicIndex, null, null);
                                this.stop();
                            }
                        });
                    }
                    else
                    {
                        victim.getUpdateFlags().sendGraphic(85, 0, 100);
                        completeDelayedHit(attacker, victim, 0, combatStyle, attackType, magicIndex, null, null);
                    }
                    this.stop();
                    return;

                }
                else
                {

                    // Dark bow.
                    if (isSpecial && weapon != null && weapon.getId() == 11235)
                    {

                        hit -= 8;

                        hit = Prayer.getInstance().prayerHitModifiers(attacker, victim, hit, attackType, ((Player) victim).getActivePrayers());

                        if (realArrowType == ArrowType.DRAGON_ARROW)
                        {
                            hit += (((Player) victim).getActivePrayers()[Prayer.PROTECT_FROM_RANGED] ? 4 : 8);
                        }
                        else
                        {
                            hit += (((Player) victim).getActivePrayers()[Prayer.PROTECT_FROM_RANGED] ? 3 : 5);
                        }
                    }
                    else
                    {
                        if (victim.isPlayer())
                        {
                            hit = Prayer.getInstance().prayerHitModifiers(attacker, victim, hit, attackType, ((Player) victim).getActivePrayers());
                        }
                    }

                    if (attackType == AttackTypes.MAGIC && SpellLoader.getSpellDefinitions()[magicIndex].getEndGraphicId() != 0)
                    {

                        if (SpellLoader.getSpellDefinitions()[magicIndex].getMagicType() == MagicTypes.ANCIENT)
                        {

                            switch (SpellLoader.getSpellDefinitions()[magicIndex].getSpellId())
                            {

                            case 12987:
                            case 12901:
                            case 13011:
                            case 12919:
                            case 12881:
                            case 12999:
                            case 12891:
                            case 12929:
                            case 13023:
                                victim.getUpdateFlags().sendGraphic(SpellLoader.getSpellDefinitions()[magicIndex].getEndGraphicId(), 0);
                                break;
                            default:
                                victim.getUpdateFlags().sendGraphic(SpellLoader.getSpellDefinitions()[magicIndex].getEndGraphicId(), 0, 100);
                                break;
                            }
                        }
                        else if (SpellLoader.getSpellDefinitions()[magicIndex].getMagicType() == MagicTypes.MODERN)
                        {
                            if (SpellLoader.getSpellDefinitions()[magicIndex].getSpellId() == 12445 || SpellLoader.getSpellDefinitions()[magicIndex].getSpellId() == 1192)
                            {
                                victim.getUpdateFlags().sendGraphic(SpellLoader.getSpellDefinitions()[magicIndex].getEndGraphicId(), 0, 0);
                            }
                            else
                            {
                                victim.getUpdateFlags().sendGraphic(SpellLoader.getSpellDefinitions()[magicIndex].getEndGraphicId(), 0, 100);
                            }
                        }
                    }

                    // Dark bow.
                    if (isSpecial && weapon != null && weapon.getId() == 11235)
                    {
                        victim.getUpdateFlags().sendGraphic((realArrowType == ArrowType.DRAGON_ARROW ? 1100 : 1103), 0, 100);
                    }

                    if (attackType == AttackTypes.MAGIC)
                    {

                        final int hitDmg = hit;

                        if (attacker.isPlayer())
                        {

                            if (spellDelay > 0)
                            {
                                World.submit(new Task(spellDelay)
                                {

                                    @Override
                                    protected void execute()
                                    {
                                        victim.hit(hitDmg, hitDmg == 0 ? 0 : 1, isSpecial);
                                        SpellEffects.applyMagicEffects(attacker, victim, magicIndex, hitDmg, (victim.isPlayer() && ((Player) victim).getActivePrayers()[Prayer.PROTECT_FROM_MAGIC]));
                                        this.stop();
                                    }
                                });
                            }
                            else
                            {
                                victim.hit(hitDmg, hitDmg == 0 ? 0 : 1, isSpecial);
                                SpellEffects.applyMagicEffects(attacker, victim, magicIndex, hitDmg, (victim.isPlayer() && ((Player) victim).getActivePrayers()[Prayer.PROTECT_FROM_MAGIC]));
                            }
                        } else if(attacker.isNpc()) {
                            if (spellDelay > 0)
                            {
                                World.submit(new Task(spellDelay)
                                {

                                    @Override
                                    protected void execute()
                                    {
                                        victim.hit(hitDmg, hitDmg == 0 ? 0 : 1, isSpecial);
                                        SpellEffects.applyMagicEffects(attacker, victim, magicIndex, hitDmg, (victim.isPlayer() && ((Player) victim).getActivePrayers()[Prayer.PROTECT_FROM_MAGIC]));
                                        this.stop();
                                    }
                                });
                            }
                            else
                            {
                                victim.hit(hitDmg, hitDmg == 0 ? 0 : 1, isSpecial);
                                SpellEffects.applyMagicEffects(attacker, victim, magicIndex, hitDmg, (victim.isPlayer() && ((Player) victim).getActivePrayers()[Prayer.PROTECT_FROM_MAGIC]));
                            }                        	
                        }
                    }
                    else
                    {
                        victim.hit(hit, hit == 0 ? 0 : 1, isSpecial);
                    }

                    // TODO Check these
                    victim.getCombatState().getDamageMap().incrementTotalDamage(attacker, hit);

                    if (attacker.isPlayer() && victim.isPlayer())
                    {
                        Prayer.getInstance().applySmiteEffect((Player) attacker, (Player) victim, hit);
                        Prayer.getInstance().applyRedemptionPrayer((Player) victim);
                    }

                    Poison.applyPoisonFromWeapons(attacker, victim);
                }
                // TODO
                completeDelayedHit(attacker, victim, hit, combatStyle, attackType, magicIndex, weapon, arrows);
                this.stop();
            }
        });
    }

    /**
     * Should work great
     */
    public void fixAttackStyles(Player player, Item item)
    {

        // E.g unarmed
        if (item == null)
        {
            // -- If we have a old combat style to use (i.e we was autocasting),
            // use that instead
            if (player.getAttribute("oldCombatStyle") != null)
            {
                player.getCombatState().setCombatStyle(CombatStyle.forId((Integer) player.getAttribute("oldCombatStyle")));
                player.removeAttribute("oldCombatStyle");
            }
            else
            {
                player.getCombatState().setCombatStyle(CombatStyle.forId(CombatConstants.WEAPON_INTERFACE_DATA[10][8 + player.getCombatState().getCombatStyle().getId()]));
                player.getActionSender().sendConfig(43, CombatConstants.WEAPON_INTERFACE_DATA[10][15 + player.getCombatState().getCombatStyle().getId()]);
            }
            // -- Finally set our attack type as melee.
            player.setAttackType(AttackTypes.MELEE);
            return;
        }

        // -- No weapon definition avaible
        if (WeaponLoader.getWeapon(item.getId()) == null)
        {
            System.err.println("Weapon not defined: " + item.getDefinition().getName() + "[" + item.getId() + "]!");
            player.setAttackType(AttackTypes.MELEE);
            return;
        }

        for (int index = 0; index < CombatConstants.WEAPON_INTERFACE_DATA.length; index++)
        {

            if (CombatConstants.WEAPON_INTERFACE_DATA[index][0] == WeaponLoader.getWeapon(item.getId()).getInterfaceId())
            {

                if (player.getAttribute("oldCombatStyle") != null)
                {
                    player.getCombatState().setCombatStyle(CombatStyle.forId((Integer) player.getAttribute("oldCombatStyle")));
                    player.removeAttribute("oldCombatStyle");
                }
                else
                {
                    player.getCombatState().setCombatStyle(CombatStyle.forId(CombatConstants.WEAPON_INTERFACE_DATA[index][8 + player.getCombatState().getCombatStyle().getId()]));
                    player.getActionSender().sendConfig(43, CombatConstants.WEAPON_INTERFACE_DATA[index][15 + player.getCombatState().getCombatStyle().getId()]);
                }

                if (CombatConstants.WEAPON_INTERFACE_DATA[index][22] == 0)
                    player.setAttackType(AttackTypes.MELEE);
                else if (CombatConstants.WEAPON_INTERFACE_DATA[index][22] == 1)
                    player.setAttackType(AttackTypes.RANGED);
            }
        }
    }

    /**
     * The combat tick for entities. Ticks for all entitys.
     */
    public void combatTick(Entity entity)
    {

        if (entity.isPlayer())
            Magic.getInstance().changeMagicAttackStatus(entity);

        if (entity.getCombatTimer() == 0 && entity.getCombatingEntity() != null)
            entity.setCombatingEntity(null);

        if (entity.getAttackTimer() > 0)
            entity.setAttackTimer(entity.getAttackTimer() - 1);

        if (entity.isInstigatingAttack() && entity.getTarget() != null)
            if (entity.getAttackTimer() == 0)
                attackEntity(entity, entity.getTarget());

        if (entity.getCombatTimer() > 0)
            entity.setCombatTimer(entity.getCombatTimer() - 1);
    }

    /**
     * Make better
     */
    public void attackEntity(final Entity attacker, Entity victim)
    {

        if (victim == null || attacker == null || attacker.getAttribute("isDead") != null || victim.getAttribute("isDead") != null)
            return;

        // First, face the victim.
        attacker.getUpdateFlags().faceEntity((victim.isPlayer() ? victim.getIndex() + 32768 : victim.getIndex()));

        // Check the requirements to attack.
        if (!meetsAttackRequirements(attacker, victim) || !withinRange(attacker, victim))
            return;

        // -- Flag the npc as busy so it doesn't walk for instance.
        if (attacker.isNpc() && attacker.getAttribute("IS_BUSY") == null)
            attacker.setAttribute("IS_BUSY", 0);

        // If at correct stopping distance, reset movement.
        if (withinRange(attacker, victim))
            attacker.getMovementHandler().reset();

        // Stop attacking if under the victim.
        if (Misc.getDistance(attacker.getPosition(), victim.getPosition()) == 0)
            return;

        // If using melee and in diagonal position, stop attacking.
        if (isInDiagonalBlock(attacker, victim) && attacker.getAttackType() == AttackTypes.MELEE)
            return;
        //set the attack style for the NPC
        if (attacker.isNpc()) {
        	attacker.setAttackType(((Npc) attacker).getDefinition().getAttackType());
        }
        if (attacker.isPlayer() && attacker.getAttackType() == AttackTypes.MAGIC)
        {
            switch (SpellLoader.getSpellDefinitions()[Magic.getInstance().getMagicIndex(attacker)].getSpellId())
            {
            case 1572: // Bind
            case 1582: // Snare
            case 1592: // Entangle
                if (victim.getAttribute("isFrozen") != null)
                {
                    ((Player) attacker).getActionSender().sendMessage("Your target is already held by a magical force.");
                    resetCombat(attacker);
                    fixAttackStyles((Player) attacker, (((Player) attacker).getEquipment().get(3)));
                    attacker.removeAttribute("SINGLE_CAST");
                    attacker.removeAttribute("SINGLE_CAST_CHANGE");
                    return;
                }
                break;
            } 
        } else if (attacker.isNpc() && attacker.getAttackType() == AttackTypes.MAGIC) {
        	//victim.getUpdateFlags().sendGraphic(76, 0, 0);
        	
        	attacker.setAttribute("SINGLE_CAST", (((Npc) attacker).getDefinition()).getSpellId());
        	delayHit(attacker, victim, 1, true, false, DetermineHit.calculateHit(attacker, victim, false), attacker.getCombatState().getCombatStyle(), attacker.getAttackType(), (((Npc) attacker).getDefinition()).getSpellId(), null, null);
        	//sendProjectile(attacker, victim);
        	//sending NPC projectile.     	
        }

        // Rune removal for magic if autocasting.
        if (attacker.getAttackType() == AttackTypes.MAGIC)
        {
            if (attacker.isPlayer() && (attacker.getAttribute("AUTO_CAST") != null && attacker.getAttribute("SINGLE_CAST") == null) && !Magic.getInstance().removeRunes((Player) attacker, SpellLoader.getSpellDefinitions()[Magic.getInstance().getMagicIndex(attacker)].getRunesRequired()))
            {
                resetCombat(attacker);
                fixAttackStyles((Player) attacker, (((Player) attacker).getEquipment().get(3)));
                attacker.removeAttribute("AUTO_CAST");
                attacker.removeAttribute("AUTO_CAST_CHANGE");
                attacker.removeAttribute("SINGLE_CAST_CHANGE");
                return;
            } 
            attacker.getUpdateFlags().sendAnimation(attacker.grabAttackAnimation(), 0);
        }

        // Start the animation and show possibly start gfx.
        if (!SpecialAttack.getInstance().specialActivated(attacker))
        {

            if (attacker.getAttackType() != AttackTypes.MAGIC)
                attacker.getUpdateFlags().sendAnimation(attacker.grabAttackAnimation(), 0);

            // Player only
            if (attacker.isPlayer() && attacker.getAttackType() == AttackTypes.RANGED && ((Player) attacker).getEquipment().get(3) != null)
            {

                Item weapon = ((Player) attacker).getEquipment().get(3);
                Item arrows = ((Player) attacker).getEquipment().get(13);

                ArrowType arrowType = null;

                if (arrows != null)
                    arrowType = RangedDataLoader.getProjectileId(arrows.getId()).getArrowType();

                BowType bowType = RangedDataLoader.getProjectileId(weapon.getId()).getBowType();

                RangeWeaponType rangeWeaponType = RangedDataLoader.getProjectileId(weapon.getId()).getRangeWeaponType();

                if (bowType != BowType.CRYSTAL_BOW)
                {
                    ((Player) attacker).getUpdateFlags().sendGraphic((bowType == BowType.DARK_BOW ? CombatConstants.getPullBackDarkBow(arrowType) : (bowType == null ? rangeWeaponType.getPullbackGraphic() : arrowType.getPullbackGraphic())), 0, 100);
                }
                else
                {
                    ((Player) attacker).getUpdateFlags().sendGraphic(250, 0, 100);
                }
            }
        }

        Item weapon = attacker.isNpc() ? null : ((Player) attacker).getEquipment().get(3);

        // Send ranged or magic projectile.
        if ((attacker.getAttackType() == Entity.AttackTypes.MAGIC) || (attacker.getAttackType() == Entity.AttackTypes.RANGED && (weapon.getId() == 11235 || !SpecialAttack.getInstance().specialActivated(((Player) attacker)))))
            sendProjectile(attacker, victim);

        // Perform hit, be carefull not to handle range or mage here...
        else
        {
            if (attacker.isPlayer() && SpecialAttack.getInstance().specialActivated(((Player) attacker)) && attacker.getAttackType() != AttackTypes.MAGIC)
                SpecialAttack.getInstance().performSpecialAttack(((Player) attacker), victim);
            else
                delayHit(attacker, victim, 1, true, false, DetermineHit.calculateHit(attacker, victim, false), attacker.getCombatState().getCombatStyle(), attacker.getAttackType(), 0, (attacker.isPlayer() ? ((Player) attacker).getEquipment().get(3) : null), (attacker.isPlayer() ? ((Player) attacker).getEquipment().get(13) : null));

        }

        // Waste ranged ammunation.
        if (attacker.getAttackType() == AttackTypes.RANGED && ((Player) attacker).getEquipment().get(3) != null)
        {

            final BowType bowType = RangedDataLoader.getProjectileId(((Player) attacker).getEquipment().get(3).getId()).getBowType();
            /**
             * Remove one projectile
             */
            World.submit(new Task(true)
            {

                @Override
                protected void execute()
                {
                    // Degrade crystal bow
                    if (bowType == BowType.CRYSTAL_BOW)
                        degradeCrystalBow(attacker);
                    else
                    {
                        removeAmmo(attacker, bowType == null ? 3 : 13, 1);
                        ((Player) attacker).getEquipment().refresh();
                    }
                    this.stop();
                }
            });
        }

        // Append PJ timers.
        appendCombatTimers(attacker, victim);
        victim.setCombatingEntity(attacker);
    }

    // This is done, what amount of code but works =D
    private void degradeCrystalBow(Entity attacker)
    {

        // Only for players..
        if (!attacker.isPlayer())
            return;

        // The state of the bow e.g 9/10
        int state = 0;

        switch (((Player) attacker).getEquipment().get(3).getId())
        {

        case 4212: // New
            ((Player) attacker).getEquipment().forceEquip(new Item(4214), 3);
            ((Player) attacker).getActionSender().sendMessage("Your Crystal bow has slightly degraded!");
            return;

        case 4214: // Full
        case 4215: // 9/10 and here we go..
        case 4216:
        case 4217:
        case 4218:
        case 4219:
        case 4220:
        case 4221:
        case 4222:
        case 4223:
            state = 4224 - ((Player) attacker).getEquipment().get(3).getId();
            break;
        }

        if (attacker.getAttribute(("crystalBow") + state) == null)
        {
            attacker.setAttribute(("crystalBow") + state, (short) 0);
        }
        else
        {
            // +1 On the current crystal bow charges.
            attacker.setAttribute(("crystalBow") + state, (short) ((Short) attacker.getAttribute(("crystalBow") + state) + 1));

            // If the charge amounts have exceed the limit
            if ((Short) attacker.getAttribute(("crystalBow") + state) >= 249)
            {

                // Remove the uses of the old charge state
                attacker.removeAttribute(("crystalBow" + state));

                if (state != 1)
                {
                    // Degrade the bow
                    ((Player) attacker).getEquipment().forceEquip(new Item(((Player) attacker).getEquipment().get(3).getId() + 1), 3);

                    // Initialize the charges on the new charge state.
                    attacker.setAttribute(("crystalBow") + (state - 1), (short) 0);

                    ((Player) attacker).getActionSender().sendMessage("Your Crystal bow has slightly degraded!");
                }
                else
                {
                    // Remove weapon
                    ((Player) attacker).getEquipment().forceEquip(null, 3);
                    // Update appareance
                    ((Player) attacker).setAppearanceUpdateRequired(true);

                    // TODO Make it drop the item?
                    if (((Player) attacker).getInventory().getItemContainer().freeSlots() == 0)
                    {
                        ((Player) attacker).getActionSender().sendMessage("TODO: No space for Crystal seed!");
                    }
                    else
                        ((Player) attacker).getInventory().addItem(new Item(4207));

                    ((Player) attacker).getActionSender().sendMessage("Your Crystal bow has degraded into a crystal seed!");
                }
            }
        }
        return;
    }

    /**
     * Checks if you can use ranged, to start an attack.
     * 
     * @return <code>True</code> if you have correct ammunition, and stuff
     */
    public boolean canUseRanged(final Player player)
    {

        /**
         * We must have a ranged weapon
         */
        if (player.getEquipment().get(3) == null)
            return false;

        boolean hasCorrectAmmunition = false;

        Item weapon = player.getEquipment().get(3);
        Item arrows = player.getEquipment().get(13);

        BowType bowType = RangedDataLoader.getProjectileId(weapon.getId()).getBowType();

        if (bowType == BowType.CRYSTAL_BOW)
            return true;

        ArrowType arrowType = null;

        if (arrows != null)
            arrowType = RangedDataLoader.getProjectileId(arrows.getId()).getArrowType();

        RangeWeaponType rangeWeaponType = RangedDataLoader.getProjectileId(weapon.getId()).getRangeWeaponType();

        /**
         * Check if using bow
         */
        if (bowType != null && arrowType != null)
        {
            for (ArrowType correctArrowType : bowType.getValidArrows())
            {
                if (correctArrowType == arrowType)
                {
                    hasCorrectAmmunition = true;
                    break;
                }
            }
        }

        /**
         * Check if using any RangeWeapon (wieldable).
         */
        if (rangeWeaponType != null && weapon.getCount() >= 1)
            hasCorrectAmmunition = true;

        /**
         * Check if has correct ammunition required.
         */
        if (!hasCorrectAmmunition && bowType != null && arrows != null)
        {
            player.getActionSender().sendMessage(Language.WRONG_AMMO);
            player.getMovementHandler().reset();
            resetCombat(player);
        }

        /**
         * Check if has any arrows at all required for all kind of bows.
         */
        if (arrows == null && bowType != null)
        {
            player.getActionSender().sendMessage(Language.NO_RANGED_AMMO);
            player.getMovementHandler().reset();
            resetCombat(player);
        }

        /**
         * If has correct things needed, return true
         */
        if (hasCorrectAmmunition)
            return true;

        return false;
    }

    /**
     * Do things after the hit Be sure to not grab any variables as they could
     * have already changed from Attacker or Victim (hence the delay in hit)
     */
    public void completeDelayedHit(final Entity attacker, final Entity victim, final int hit, final CombatStyle combatStyle, final AttackTypes attackType, final int magicIndex, final Item weapon, final Item arrows)
    {
        /*
         * if (attacker.isPlayer()) { //TODO Weapon sound system, maybe later...
         * int weaponId = -1; if (((Player) attacker).getEquipment().get(3) !=
         * null) weaponId = ((Player) attacker).getEquipment().get(3).getId();
         * if (player.getAttackType() != AttackTypes.MAGIC &&
         * WeaponLoader.getWeapon(weaponId) != null &&
         * WeaponLoader.getWeapon(weaponId
         * ).getSound(attacker.getCombatState().getCombatStyle().getId()) != -1)
         * ((Player) attacker).getActionSender().sendSound(
         * WeaponLoader.getWeapon(weaponId).getSound(
         * attacker.getCombatState().getCombatStyle().getId()), 0, 0); }
         */

        if (sendDefenceAnimation(victim))
        {
            victim.getUpdateFlags().sendAnimation(victim.grabDefenceAnimation());
        }

        if (attacker.isPlayer())
        {

            World.submit(new Task(1)
            {

                @Override
                protected void execute()
                {

                    if (hit != 0)
                    {

                        if (attackType == AttackTypes.MELEE)
                        {

                            for (int index = 0; index < combatStyle.getSkills().length; index++)

                                ((Player) attacker).getSkill().addExperience(combatStyle.getSkill(index), (combatStyle.getExperience(index) * hit));

                        }

                        if (attackType == AttackTypes.RANGED)
                        {
                            switch (combatStyle)
                            {
                            case ACCURATE:
                            case AGGRESSIVE:
                                ((Player) attacker).getSkill().addExperience(Skill.RANGED, (4 * hit));
                                break;
                            case DEFENSIVE:
                                ((Player) attacker).getSkill().addExperience(Skill.RANGED, (2 * hit));
                                ((Player) attacker).getSkill().addExperience(Skill.DEFENCE, (2 * hit));
                            }
                        }
                    }
                    if (attackType == AttackTypes.MAGIC)
                    {
                        switch (combatStyle)
                        {
                        case DEFENSIVE_AUTOCAST:
                            ((Player) attacker).getSkill().addExperience(Skill.MAGIC, SpellLoader.getSpellDefinitions()[magicIndex].getBaseExperience() + (1.33 * hit));
                            ((Player) attacker).getSkill().addExperience(Skill.DEFENCE, (1 * hit));
                            break;
                        case AUTOCAST:
                            ((Player) attacker).getSkill().addExperience(Skill.MAGIC, SpellLoader.getSpellDefinitions()[magicIndex].getBaseExperience() + (2 * hit));
                        }
                    }

                    // Regardless of attack type or combat style.
                    ((Player) attacker).getSkill().addExperience(Skill.HITPOINTS, (1.33 * hit));

                    /**
                     * Ranged ammunition "wasting". TODO Redo this you must send
                     * variables from hitdelay class that may have already
                     * changed. Like with anything else!
                     */
                    if (attackType == AttackTypes.RANGED)
                    {

                        if (weapon != null)
                        {

                            ArrowType arrowType = null;

                            if (arrows != null)
                                arrowType = RangedDataLoader.getProjectileId(arrows.getId()).getArrowType();

                            BowType bowType = RangedDataLoader.getProjectileId(weapon.getId()).getBowType();

                            RangeWeaponType rangeWeaponType = RangedDataLoader.getProjectileId(weapon.getId()).getRangeWeaponType();

                            double finalDropRate = 0;

                            if (arrows != null && bowType != null)
                                finalDropRate = arrowType.getDropRate();

                            if (rangeWeaponType != null)
                                finalDropRate = rangeWeaponType.getDropRate();

                            final Item finalAmmo = (bowType == null ? weapon : arrows);

                            if (finalAmmo != null && bowType != BowType.CRYSTAL_BOW)
                            {
                                Random random = new Random();
                                double r = random.nextDouble();
                                // if (0.0-1.0) is BIGGER than chance, 0.4
                                // (rune_arrow) (60% chance) then drop the arrow
                                if (r >= finalDropRate)
                                {
                                    ItemManager.getInstance().createGroundItem((Player) attacker, ((Player) attacker).getUsername(), new Item(finalAmmo.getId(), 1), new Position(victim.getPosition().getX(), victim.getPosition().getY(), victim.getPosition().getZ()), Constants.GROUND_START_TIME_DROP);
                                }
                            }
                        }
                    }
                    this.stop();
                }
            });
        }

        autoRetaliate(victim);

        if (attacker.isPlayer() && ((Player) attacker).getMinigame() == null)
            Skulling.skullEntity(attacker, victim);

        if (attacker.isPlayer() && attacker.getAttackType() == AttackTypes.MAGIC)
            Magic.getInstance().resetMagic(attacker);
    }

    /**
     * Resets anything needed after the end of combat.
     * 
     * @param reset_movement << true reset movement
     */
    public void resetCombat(Entity entity, boolean reset_movement)
    {
        entity.setInstigatingAttack(false);
        entity.setFollowingEntity(null);
        entity.setTarget(null);
        entity.getUpdateFlags().faceEntity(65535);
        if (entity.isNpc() && entity.getAttribute("IS_BUSY") != null)
            entity.removeAttribute("IS_BUSY");
        if (reset_movement)
            entity.getMovementHandler().reset();
    }

    public void resetCombat(Entity entity)
    {
        resetCombat(entity, false);
    }

    /**
     * Updates the Entity's combat timers after an attack.
     */
    public void appendCombatTimers(Entity attacker, Entity victim)
    {
        attacker.setAttackTimer(attacker.grabHitTimer());
        victim.setCombatTimer(16); // 9,6 sec
    }

    public static boolean isInDiagonalBlock(Entity attacker, Entity victim)
    {
        int attackerX = attacker.getPosition().getX();
        int attackerY = attacker.getPosition().getY();
        int victimX = victim.getPosition().getX();
        int victimY = victim.getPosition().getY();
        int deltaX = victimX - attackerX;
        int deltaY = victimY - attackerY;
        return (attacker.getAttackType() == AttackTypes.MELEE) && (Misc.direction(deltaX, deltaY) == 2 || Misc.direction(deltaX, deltaY) == 7 || Misc.direction(deltaX, deltaY) == 5 || Misc.direction(deltaX, deltaY) == 0);
    }

    /**
     * Checking if the attacker meets the requirements to attack the victim.
     */
    public boolean meetsAttackRequirements(final Entity attacker, final Entity victim)
    {

        if (victim.getAttribute("isDead") != null || attacker.getAttribute("isDead") != null || attacker == null || victim == null)
            return false;

        // -- Entity under combat check...
        if (inCombat(attacker, victim))
            return false;

        if (attacker.isPlayer())
        {

            Player player_atk = (Player) attacker;

            if (((Player) attacker).getMinigame() == null && victim.isPlayer())
            {

                if (!Areas.inWilderness(attacker.getPosition()))
                {
                    player_atk.getActionSender().sendMessage(Language.NOT_IN_WILDERNESS);
                    resetCombat(attacker, true);
                    return false;
                }

                if (!Areas.inWilderness(victim.getPosition()))
                {
                    player_atk.getActionSender().sendMessage(Language.VICTIM_NOT_IN_WILDERNESS);
                    resetCombat(attacker, true);
                    return false;
                }

                int variance = ((attacker.getSkill().getCombatLevel() - victim.getSkill().getCombatLevel()) < 0 ? (victim.getSkill().getCombatLevel() - attacker.getSkill().getCombatLevel()) : (attacker.getSkill().getCombatLevel() - victim.getSkill().getCombatLevel()));

                if (variance > Areas.getWildernessLevel(attacker) || variance > Areas.getWildernessLevel(victim))
                {
                    player_atk.getActionSender().sendMessage(Language.LEVEL_DIFFERENCE);
                    player_atk.getActionSender().sendMessage(Language.MOVE_DEEPER_TO_WILDERNESS);
                    resetCombat(attacker, true);
                    return false;
                }
            }

            // Duel arena disabled combat styles
            if (player_atk.getMinigame() != null && player_atk.getMinigame().getGameName() == "Duel Arena")
            {
                if ((Byte) player_atk.getAttribute("duel_button_melee") != null && attacker.getAttackType() == AttackTypes.MELEE)
                {
                    player_atk.getActionSender().sendMessage("You cannot use Melee combat in this duel session.");
                    resetCombat(player_atk);
                    return false;
                }
                if ((Byte) player_atk.getAttribute("duel_button_ranged") != null && attacker.getAttackType() == AttackTypes.RANGED)
                {
                    player_atk.getActionSender().sendMessage("You cannot use Ranged combat in this duel session.");
                    resetCombat(player_atk);
                    return false;
                }
                if ((Byte) player_atk.getAttribute("duel_button_magic") != null && attacker.getAttackType() == AttackTypes.MAGIC)
                {
                    player_atk.getActionSender().sendMessage("You cannot use Magic combat in this duel session.");
                    resetCombat(player_atk);
                    return false;
                }
            }

            if (attacker.isPlayer() && ((Player) attacker).getMinigame() != null)
            {
                if (!((Player) attacker).getMinigame().attackListener((Player) attacker, victim))
                    return false;
            }

            if (attacker.isPlayer() && ((Player) attacker).getMinigame() != null && ((Player) attacker).getMinigame().getGameName().equals("Duel Arena"))
            {
                if (victim.isPlayer() && (Integer) victim.getAttribute("duel_partner") != ((Player) attacker).getIndex())
                    return false;
            }

            if (((Player) attacker).getMinigame() != null && !((Player) attacker).getMinigame().getGameArea().isInArea(attacker.getPosition()))
            {
                player_atk.getActionSender().sendMessage("You must be in the " + ((Player) attacker).getMinigame().getGameName() + " in order to attack players here.");
                resetCombat(attacker);
                ((Player) attacker).getMovementHandler().reset();
                return false;
            }

            if (((Player) attacker).getMinigame() != null && !((Player) attacker).getMinigame().getGameArea().isInArea(victim.getPosition()))
            {
                player_atk.getActionSender().sendMessage("You can't attack players who aren't in the " + ((Player) attacker).getMinigame().getGameName() + ".");
                resetCombat(attacker);
                ((Player) attacker).getMovementHandler().reset();
                return false;
            }

            switch (attacker.getAttackType())
            {
            case RANGED:// TODO RANGE
                if (!canUseRanged((Player) attacker))
                    return false;
                break;
            }
        }
        else
        {
            // -- Aggression
            @SuppressWarnings("unused")
            Npc npc = (Npc) attacker;

        }

        return true;
    }

    /**
     * Checks to see if both entitys can start fighting eachother.
     * 
     * TODO: make it
     */
    public boolean inCombat(Entity attacker, Entity victim)
    {
        if (attacker == null || victim == null)
            return false;

        if (Areas.inMultiArea(attacker.getPosition()) && Areas.inMultiArea(victim.getPosition()))
            return false;

        if (attacker.getCombatingEntity() != null && attacker.getCombatingEntity() != victim)
        {
            if (attacker.isPlayer())
                ((Player) attacker).getActionSender().sendMessage("I'm already under attack.");
            resetCombat(attacker, true);
            return true;
        }

        if (victim.getCombatingEntity() != null && victim.getCombatingEntity() != attacker)
        {
            if (attacker.isPlayer())
                ((Player) attacker).getActionSender().sendMessage("Someone else is already fighting your opponent.");
            resetCombat(attacker, true);
            return true;
        }

        return false;
    }

    public boolean sendDefenceAnimation(Entity victim)
    {
        if (victim == null)
            return false;
        return (victim.getAttackTimer() == 0 && !victim.getUpdateFlags().isAnimationUpdateRequired() && !victim.isInstigatingAttack() || victim.getAttackTimer() > 0 && victim.getAttackTimer() < (victim.grabHitTimer() - 1) && victim.isInstigatingAttack());
    }

    public void autoRetaliate(Entity victim)
    {
        if (victim != null && victim.handleAutoRetaliate())
        {
            victim.setTarget(victim.getCombatingEntity());
            victim.setFollowingEntity(victim.getCombatingEntity());
            victim.setInstigatingAttack(true);
        }
    }

    public void sendProjectile(Entity attacker, final Entity victim)
    {
        int attackerX = attacker.getPosition().getX(), attackerY = attacker.getPosition().getY();
        int victimX = victim.getPosition().getX(), victimY = victim.getPosition().getY();
        int offsetX = (attackerY - victimY) * -1;
        int offsetY = (attackerX - victimX) * -1;
        int projectileId = 0;

        if (attacker.isPlayer() && attacker.getAttackType() == AttackTypes.MAGIC)
        {
            int startHeight = 0, duration = 0, endHeight = 0, curve = 0, hitDelay = 1, delay = 0, radius = 0, angle = 50;

            final int spellId = Magic.getInstance().getMagicIndex(attacker);

            int distance = Misc.getDistance(attacker.getPosition(), victim.getPosition());

            final int startGraphicId = SpellLoader.getSpellDefinitions()[spellId].getGraphicId();

            projectileId = SpellLoader.getSpellDefinitions()[spellId].getProjectileId();

            if (startGraphicId != 0)
                attacker.getUpdateFlags().sendGraphic(startGraphicId, 0, 100);

            switch (SpellLoader.getSpellDefinitions()[spellId].getSpellId())
            {
            case 1152:
            case 1154:
            case 1156:
            case 1158:
            case 1160:
            case 1163:
            case 1166:
            case 1169:
            case 1172:
            case 1175:
            case 1177:
            case 1181:
                startHeight = 41;
                endHeight = 30;
                curve = 16;
                delay = 52;
                hitDelay = (distance >= 3 ? 2 : 1);
                radius = 64;
                duration = 55 + (distance * 3) + (distance == 2 ? -2 : 0) + (distance == 3 ? 5 : 0) + (distance >= 4 ? 2 : 0);
                break;
            case 1572:
            case 1582:
            case 1592:
                startHeight = 41;
                endHeight = 30;
                curve = 16;
                delay = 73;
                hitDelay = (distance >= 8 ? 3 : 2);
                radius = (distance == 1 ? 64 : 145);
                duration = 74 + (distance * 3) + (distance == 4 ? -2 : 0) + (distance == 5 ? -3 : 0) + (distance == 6 ? -5 : 0) + (distance == 7 ? -6 : 0) + (distance >= 8 ? 3 : 0);
                break;
            case 12445:
                startHeight = 42;
                endHeight = 32;
                curve = 16;
                delay = 65;
                hitDelay = (distance >= 8 ? 3 : 2);
                radius = 70;
                duration = 71 + (distance * 3) + (distance >= 6 ? -2 : 0) + (distance >= 8 ? 3 : 0);
                break;
            case 1183:
            case 1185:
            case 1188:
            case 1189:
                startHeight = 38;
                endHeight = 30;
                curve = 16;
                delay = 34;
                hitDelay = (distance >= 9 ? 2 : 1);
                radius = 64;
                duration = 40 + (distance * 3) + (distance == 3 ? 3 : 0) + (distance == 7 ? -4 : 0) + (distance == 8 ? -6 : 0) + (distance >= 9 ? 3 : 0);
                break;
            case 12939:
            case 12951:
                startHeight = 35;
                endHeight = 24;
                curve = 13;
                delay = 58;
                hitDelay = (distance >= 7 ? 3 : 2);
                radius = (distance == 1 ? 70 : 110);
                duration = 66 + (distance * 3) + (distance == 1 ? -2 : 0) + (distance == 7 ? 2 : 0);
                break;
            case 12987:
            case 12999:
                startHeight = 42;
                endHeight = 30;
                curve = 5;
                delay = 58;
                hitDelay = (distance >= 7 ? 3 : 2);
                radius = (distance == 1 ? 70 : 110);
                duration = 66 + (distance * 3) + (distance == 1 ? -2 : 0) + (distance == 7 ? 2 : 0);
                break;
            case 12861:
            case 12911:
                startHeight = 15;
                endHeight = 5;
                curve = 14;
                delay = 58;
                hitDelay = (distance >= 7 ? 3 : 2);
                radius = (distance == 1 ? 70 : 110);
                duration = 66 + (distance * 3) + (distance == 1 ? -2 : 0) + (distance == 7 ? 2 : 0);
                break;
            case 1190:
            case 1191:
            case 1192:
                hitDelay = 2;
                break;
            }

            switch (SpellLoader.getSpellDefinitions()[spellId].getSpellId())
            {
            case 12891:
            case 12929:
            case 13023:
            case 12975:
            case 12881:
            case 12919:
            case 13011:
            case 12963:
                /**
                 * Multi hitting spells 3x3 grid max 8+1 entitys.
                 */
                int found = 0;

                for (Entity entitys : World.getInstance().getRegionManager().getLocalMobs(victim))
                {

                    // -- Check that we aren't looping ourselves..
                    if (entitys != victim && entitys != attacker && entitys != null && (entitys.isNpc() && (!((Npc) entitys).getDefinition().isAttackable() || ((Npc) entitys).getDefinition().getCombatLevel(3) == 1)) && entitys.getAttribute("isDead") == null && Misc.getDistance(victim.getPosition(), entitys.getPosition()) <= 1 && Areas.inMultiArea(entitys.getPosition()))
                    {

                        int differeance = entitys.isNpc() ? 0 : ((attacker.getSkill().getCombatLevel() - entitys.getSkill().getCombatLevel()) < 0 ? (entitys.getSkill().getCombatLevel() - attacker.getSkill().getCombatLevel()) : (attacker.getSkill().getCombatLevel() - entitys.getSkill().getCombatLevel()));

                        if (entitys.isNpc() || (!(Areas.inWilderness(entitys.getPosition()) && differeance > Areas.getWildernessLevel(victim) || differeance > Areas.getWildernessLevel(entitys))))
                        {
                            if (found > 8)
                                break;
                            delayHit(attacker, entitys, hitDelay, false, false, DetermineHit.calculateHit(attacker, entitys, false), ((Player) attacker).getCombatState().getCombatStyle(), attacker.getAttackType(), spellId, null, null);
                            found++;
                        }
                    }
                }

                delayHit(attacker, victim, hitDelay, false, false, DetermineHit.calculateHit(attacker, victim, false), ((Player) attacker).getCombatState().getCombatStyle(), attacker.getAttackType(), spellId, null, null);
                break;
            default:
                delayHit(attacker, victim, hitDelay, false, false, DetermineHit.calculateHit(attacker, victim, false), ((Player) attacker).getCombatState().getCombatStyle(), attacker.getAttackType(), spellId, null, null);
                break;
            }

            if (projectileId != 0)

                World.sendProjectile(attacker.getPosition(), offsetX, offsetY, projectileId, startHeight, endHeight, duration, delay, curve, radius, angle, victim.isNpc() ? victim.getIndex() + 1 : -victim.getIndex() - 1);
        } 
        else if (attacker.isNpc() && attacker.getAttackType() == Entity.AttackTypes.MAGIC)
        {
            int startHeight = 0, duration = 0, endHeight = 0, curve = 0, hitDelay = 1, delay = 0, radius = 0, angle = 50;

            final int spellId = Magic.getInstance().getMagicIndex(attacker);

            int distance = Misc.getDistance(attacker.getPosition(), victim.getPosition());

            final int startGraphicId = SpellLoader.getSpellDefinitions()[spellId].getGraphicId();

            projectileId = SpellLoader.getSpellDefinitions()[spellId].getProjectileId();
            
            if (startGraphicId != 0)
                attacker.getUpdateFlags().sendGraphic(startGraphicId, 0, 100);
            switch (SpellLoader.getSpellDefinitions()[spellId].getSpellId())
            {
            case 1152:
            case 1154:
            case 1156:
            case 1158:
            case 1160:
            case 1163:
            case 1166:
            case 1169:
            case 1172:
            case 1175:
            case 1177:
            case 1181:
                startHeight = 41;
                endHeight = 30;
                curve = 16;
                delay = 52;
                hitDelay = (distance >= 3 ? 2 : 1);
                radius = 64;
                duration = 55 + (distance * 3) + (distance == 2 ? -2 : 0) + (distance == 3 ? 5 : 0) + (distance >= 4 ? 2 : 0);
                break;
            case 1572:
            case 1582:
            case 1592:
                startHeight = 41;
                endHeight = 30;
                curve = 16;
                delay = 73;
                hitDelay = (distance >= 8 ? 3 : 2);
                radius = (distance == 1 ? 64 : 145);
                duration = 74 + (distance * 3) + (distance == 4 ? -2 : 0) + (distance == 5 ? -3 : 0) + (distance == 6 ? -5 : 0) + (distance == 7 ? -6 : 0) + (distance >= 8 ? 3 : 0);
                break;
            case 12445:
                startHeight = 42;
                endHeight = 32;
                curve = 16;
                delay = 65;
                hitDelay = (distance >= 8 ? 3 : 2);
                radius = 70;
                duration = 71 + (distance * 3) + (distance >= 6 ? -2 : 0) + (distance >= 8 ? 3 : 0);
                break;
            case 1183:
            case 1185:
            case 1188:
            case 1189:
                startHeight = 38;
                endHeight = 30;
                curve = 16;
                delay = 34;
                hitDelay = (distance >= 9 ? 2 : 1);
                radius = 64;
                duration = 40 + (distance * 3) + (distance == 3 ? 3 : 0) + (distance == 7 ? -4 : 0) + (distance == 8 ? -6 : 0) + (distance >= 9 ? 3 : 0);
                break;
            case 12939:
            case 12951:
                startHeight = 35;
                endHeight = 24;
                curve = 13;
                delay = 58;
                hitDelay = (distance >= 7 ? 3 : 2);
                radius = (distance == 1 ? 70 : 110);
                duration = 66 + (distance * 3) + (distance == 1 ? -2 : 0) + (distance == 7 ? 2 : 0);
                break;
            case 12987:
            case 12999:
                startHeight = 42;
                endHeight = 30;
                curve = 5;
                delay = 58;
                hitDelay = (distance >= 7 ? 3 : 2);
                radius = (distance == 1 ? 70 : 110);
                duration = 66 + (distance * 3) + (distance == 1 ? -2 : 0) + (distance == 7 ? 2 : 0);
                break;
            case 12861:
            case 12911:
                startHeight = 15;
                endHeight = 5;
                curve = 14;
                delay = 58;
                hitDelay = (distance >= 7 ? 3 : 2);
                radius = (distance == 1 ? 70 : 110);
                duration = 66 + (distance * 3) + (distance == 1 ? -2 : 0) + (distance == 7 ? 2 : 0);
                break;
            case 1190:
            case 1191:
            case 1192:
                hitDelay = 2;
                break;
            default:
                delayHit(attacker, victim, hitDelay, false, false, DetermineHit.calculateHit(attacker, victim, false), ((Npc) attacker).getCombatState().getCombatStyle(), attacker.getAttackType(), spellId, null, null);
                break;                
                
            }
            if (projectileId != 0)
                World.sendProjectile(attacker.getPosition(), offsetX, offsetY, projectileId, startHeight, endHeight, duration, delay, curve, radius, angle, victim.isNpc() ? victim.getIndex() + 1 : -victim.getIndex() - 1);
            
        }

        if (attacker.isPlayer() && attacker.getAttackType() == Entity.AttackTypes.RANGED && ((Player) attacker).getEquipment().get(3) != null)
        {

            int startHeight = 0, duration = 0, endHeight = 0, curve = 0, hitDelay = 2, delay = 0, radius = 0, angle = 50;

            Item weapon = ((Player) attacker).getEquipment().get(3);
            Item arrows = ((Player) attacker).getEquipment().get(13);

            ArrowType arrowType = null;

            if (arrows != null)
                arrowType = RangedDataLoader.getProjectileId(arrows.getId()).getArrowType();

            BowType bowType = RangedDataLoader.getProjectileId(weapon.getId()).getBowType();

            RangeWeaponType rangeWeaponType = RangedDataLoader.getProjectileId(weapon.getId()).getRangeWeaponType();

            if (bowType != BowType.CRYSTAL_BOW)
                projectileId = (bowType == null ? rangeWeaponType.getProjectileId() : arrowType.getProjectileId());

            if (bowType == BowType.CRYSTAL_BOW)
                projectileId = 249;

            int distance = Misc.getDistance(attacker.getPosition(), victim.getPosition());

            if (bowType != null)
            {
                switch (bowType)
                {
                case BRONZE_CBOW:
                case BLURITE_CBOW:
                case IRON_CBOW:
                case STEEL_CBOW:
                case MITH_CBOW:
                case ADAMANT_CBOW:
                case RUNE_CBOW:
                    startHeight = 42;
                    endHeight = 30;
                    curve = 15;
                    delay = 48;
                    hitDelay = (distance >= 4 ? 2 : 1);
                    radius = 52;
                    duration = 53 + (distance * 3) + (distance == 3 ? -2 : 0) + (distance == 4 ? 5 : 0) + (distance >= 5 ? 4 : 0);
                    break;

                case KARILS_XBOW:
                case SHORTBOW:
                case LONGBOW:
                case OAK_LONGBOW:
                case OAK_SHORTBOW:
                case WILLOW_LONGBOW:
                case WILLOW_SHORTBOW:
                case MAPLE_LONGBOW:
                case MAPLE_SHORTBOW:
                case YEW_LONGBOW:
                case YEW_SHORTBOW:
                case MAGIC_LONGBOW:
                case MAGIC_SHORTBOW:
                case CRYSTAL_BOW:
                    startHeight = 45;
                    endHeight = 30;
                    curve = 15;
                    delay = 42;
                    radius = 50;
                    hitDelay = (distance <= 4 ? 1 : 2);
                    duration = 52 + (distance * 3) + (distance == 4 ? -4 : 0) + (distance >= 5 ? 2 : 0);
                    break;
                case DARK_BOW:
                    break;
                }
            }

            if (rangeWeaponType != null)
            {

                switch (rangeWeaponType)
                {

                case OBSIDIAN_RING:
                    startHeight = 40;
                    endHeight = 30;
                    curve = 8;
                    delay = 47;
                    radius = 100;
                    hitDelay = (distance >= 6 ? 2 : 1);
                    duration = 49 + (distance * 3) + (distance >= 6 ? 3 : 0);
                    break;

                case BRONZE_JAVELIN:
                case IRON_JAVELIN:
                case STEEL_JAVELIN:
                case MITHRIL_JAVELIN:
                case ADAMANT_JAVELIN:
                case RUNE_JAVELIN:
                    startHeight = 40;
                    endHeight = 30;
                    curve = 5;
                    delay = 45;
                    hitDelay = (distance >= 3 ? 2 : 1);
                    radius = (distance == 1 ? 100 : 135);
                    duration = 50 + (distance * 3);
                    break;

                case BRONZE_THROWNAXE:
                case IRON_THROWNAXE:
                case STEEL_THROWNAXE:
                case MITHRIL_THROWNAXE:
                case ADAMANT_THROWNAXE:
                case RUNE_THROWNAXE:
                    startHeight = 40;
                    endHeight = 30;
                    curve = 5;
                    delay = 39;
                    radius = 100;
                    hitDelay = 1;
                    duration = 44 + (distance * 3);
                    break;

                case BRONZE_KNIFE:
                case IRON_KNIFE:
                case STEEL_KNIFE:
                case BLACK_KNIFE:
                case MITHRIL_KNIFE:
                case ADAMANT_KNIFE:
                case RUNE_KNIFE:
                    startHeight = 40;
                    endHeight = 30;
                    curve = 6;
                    delay = 45;
                    hitDelay = (distance >= 5 ? 2 : 1);
                    radius = 100;
                    duration = 45 + (distance * 3) + (distance >= 5 ? 4 : 0);
                    break;

                case BRONZE_DART:
                case IRON_DART:
                case STEEL_DART:
                case MITHRIL_DART:
                case ADAMANT_DART:
                case BLACK_DART:
                case RUNE_DART:
                    startHeight = 45;
                    endHeight = 30;
                    curve = 14;
                    delay = 16;
                    radius = 80;
                    hitDelay = 1;
                    duration = 27 + (distance * 3);
                    break;
                case DRAGON_DART:
                    break;
                }
            }

            if (weapon.getId() == 11235)
            {
                // Dark bow
                startHeight = 45;
                endHeight = 28;
                curve = 10;
                delay = 42;
                radius = 50;
                hitDelay = (distance <= 3 ? 1 : 2);
                duration = 46 + (distance * 3) + (distance == 3 ? -4 : 0) + (distance >= 4 ? 2 : 0);

                World.sendProjectile(attacker.getPosition(), offsetX, offsetY, projectileId, startHeight, endHeight, duration, delay, curve, radius, angle, victim.isNpc() ? victim.getIndex() + 1 : -victim.getIndex() - 1);

                startHeight = 42;
                endHeight = 28;
                curve = 15;
                delay = 47;
                duration = 60 + (distance * 3) + (distance == 3 ? -5 : 0) + (distance >= 4 ? 2 : 0);

                if (!SpecialAttack.getInstance().specialActivated(((Player) attacker)))
                {

                    delayHit(attacker, victim, hitDelay, false, false, DetermineHit.calculateHit(attacker, victim, false), ((Player) attacker).getCombatState().getCombatStyle(), attacker.getAttackType(), 0, ((Player) attacker).getEquipment().get(3), ((Player) attacker).getEquipment().get(13));

                    delayHit(attacker, victim, hitDelay, false, false, DetermineHit.calculateHit(attacker, victim, false), ((Player) attacker).getCombatState().getCombatStyle(), attacker.getAttackType(), 0, ((Player) attacker).getEquipment().get(3), ((Player) attacker).getEquipment().get(13));

                }
            }

            if (SpecialAttack.getInstance().specialActivated(((Player) attacker)) && attacker.getAttackType() != AttackTypes.MAGIC)
                SpecialAttack.getInstance().performSpecialAttack(((Player) attacker), victim);
            else if (weapon.getId() != 11235)
            {
                delayHit(attacker, victim, hitDelay, false, false, DetermineHit.calculateHit(attacker, victim, false), ((Player) attacker).getCombatState().getCombatStyle(), attacker.getAttackType(), 0, ((Player) attacker).getEquipment().get(3), ((Player) attacker).getEquipment().get(13));
            }

            if (projectileId != 0)

                World.sendProjectile(attacker.getPosition(), offsetX, offsetY, projectileId, startHeight, endHeight, duration, delay, curve, radius, angle, victim.isNpc() ? victim.getIndex() + 1 : -victim.getIndex() - 1);
        }
    }

    /**
     * Remove item from preferred slot (usage: ranged arrows for example)
     * 
     * @param entity (the entity)
     * @param slot (equipment slot)
     * @param amount (to remove)
     * 
     *            TODO Make better......
     */
    public void removeAmmo(Entity entity, int slot, int amount)
    {
        if (entity.isPlayer())
        {
            Player player = ((Player) entity);
            if (player.getEquipment().get(slot).getCount() <= 1)
            {
                player.getEquipment().getItemContainer().remove(player.getEquipment().get(slot), slot);
                if (slot != 13)
                    player.setAppearanceUpdateRequired(true);
            }
            else
            {
                player.getEquipment().get(slot).setCount(player.getEquipment().get(slot).getCount() - amount);
            }
        }
    }

    /**
     * Checks if the attacker is within distance required for attacking.
     */
    public boolean withinRange(Entity attacker, Entity victim)
    {
        int combatDistance = getDistanceForCombatType(attacker);
        if (!RegionClipping.canMove(attacker, attacker.getPosition().getX(), attacker.getPosition().getY(), victim.getPosition().getX(), victim.getPosition().getY(), attacker.getPosition().getZ(), 1, 1) && Misc.getDistance(attacker.getPosition(), victim.getPosition()) <= combatDistance)
            return false;
        return Misc.getDistance(attacker.getPosition(), victim.getPosition()) <= combatDistance;
    }

    /**
     * Getting the distance needed for combat.
     */
    public int getDistanceForCombatType(Entity entity)
    {
        int combatDistance = 0;
        switch (entity.getAttackType())
        {
        case MELEE:
            combatDistance += 1;
            break;
        case RANGED:
            if (entity.isPlayer())
            {

                if (((Player) entity).getEquipment().get(3) == null)
                    break;

                Item weapon = ((Player) entity).getEquipment().get(3);

                BowType bowType = RangedDataLoader.getProjectileId(weapon.getId()).getBowType();

                RangeWeaponType rangeWeaponType = RangedDataLoader.getProjectileId(weapon.getId()).getRangeWeaponType();

                if (entity.getCombatState().getCombatStyle() != CombatStyle.DEFENSIVE)
                    combatDistance += (bowType == null ? rangeWeaponType.getDistance(entity.getCombatState().getCombatStyle().getId()) : bowType.getDistance(entity.getCombatState().getCombatStyle().getId()));
                else
                    combatDistance += (bowType == null ? rangeWeaponType.getDistance(2) : bowType.getDistance(2));
            }
            break;
        case MAGIC:
            combatDistance += 10;
            break;
        }

        return combatDistance;
    }
}