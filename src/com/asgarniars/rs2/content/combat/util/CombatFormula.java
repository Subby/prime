package com.asgarniars.rs2.content.combat.util;

import com.asgarniars.rs2.content.combat.magic.Magic;
import com.asgarniars.rs2.content.combat.magic.SpellLoader;
import com.asgarniars.rs2.content.combat.ranged.RangedDataLoader;
import com.asgarniars.rs2.content.combat.ranged.RangedData.ArrowType;
import com.asgarniars.rs2.content.combat.ranged.RangedData.BowType;
import com.asgarniars.rs2.content.combat.ranged.RangedData.RangeWeaponType;
import com.asgarniars.rs2.content.combat.util.CombatState.CombatStyle;
import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.players.Player;

/**
 * TODO: Fine tune, take time and listen to feedback what pre-alpha users have
 * said and improve on =)
 * 
 * TODO Needs tweaking, also max hit formulas need special items such as salve
 * ammys etc
 * 
 * @author Jacob & AkZu & Scu11
 * 
 */
public class CombatFormula
{

    /**
     * Used in defence formulae. (How much equipment affects to the accuary)
     */
    public static final double DEFENCE_MODIFIER = 0.895;

    /**
     * Gets the Melee Hit Accuracy.
     */
    public static double getMeleeAccuracy(Entity attacker, Entity victim, boolean isSpecial, boolean defensive_calc)
    {
        if (attacker == null)
            return 0;

        // -- We're calculating offensive..
        if (!defensive_calc)
        {

            double attackBonus = 0;
            int attackLevel = attacker.getSkillLevel(Skill.ATTACK);

            // TODO: Npc's can have bonuses (no offensive) too, like dragons are
            // weak to stab attacks
            if (attacker.isPlayer())
            {
                if (((Player) attacker).getEquipment().get(3) == null)
                    attackBonus = attacker.getBonus(CombatConstants.WEAPON_INTERFACE_DATA[10][attacker.getCombatState().getCombatStyle().getId() + 1]);
                else
                    for (int index = 0; index < CombatConstants.WEAPON_INTERFACE_DATA.length; index++)
                        if (CombatConstants.WEAPON_INTERFACE_DATA[index][0] == Weapons.WeaponLoader.getWeapon(((Player) attacker).getEquipment().get(3).getId()).getInterfaceId())
                            attackBonus = attacker.getBonus(CombatConstants.WEAPON_INTERFACE_DATA[index][attacker.getCombatState().getCombatStyle().getId() + 1]);
            }

            if (attacker.isPlayer())
            {
                if (((Player) attacker).getActivePrayers()[Prayer.CLARITY_OF_THOUGHT])
                    attackLevel *= 1.05;
                if (((Player) attacker).getActivePrayers()[Prayer.IMPROVED_REFLEXES])
                    attackLevel *= 1.1;
                if (((Player) attacker).getActivePrayers()[Prayer.INCREDIBLE_REFLEXES])
                    attackLevel *= 1.15;
            }

            attackLevel += 8;

            switch (attacker.getCombatState().getCombatStyle())
            {

            case ACCURATE:
                attackLevel += 3;
                break;
            case CONTROLLED_1:
            case CONTROLLED_2:
            case CONTROLLED_3:
                attackLevel += 1;
                break;
            }

            // -- Salve ammy has priority over black mask.
            if (victim.isNpc() && victim.getAttribute("UNDEAD") != null)
                attackLevel = (int) (attackLevel * (CombatItemEffects.HAS_SALVE_AMMY(attacker) >= 1.0 ? CombatItemEffects.HAS_SALVE_AMMY(attacker) : CombatItemEffects.HAS_BLACK_MASK(attacker)));

            double hitChance = attackLevel * (1 + attackBonus / 64);

            /**
             * Special attacks
             */
            if (attacker.isPlayer())
            {

                if (((Player) attacker).getEquipment().get(3) != null && isSpecial)
                {

                    int weaponId = ((Player) attacker).getEquipment().get(3).getId();

                    switch (weaponId)
                    {
                    case 3101: // Dragon daggers + d scim
                    case 1215:
                    case 1231:
                    case 5680:
                    case 5698:
                    case 4587:
                        hitChance *= 1.1;
                        break;
                    case 1434: // D mace
                        hitChance *= 0.955;
                        break;
                    }
                }
            }

            /**
             * Void bonus
             */
            hitChance *= CombatItemEffects.HAS_VOID_BONUS(attacker, 2);

            return Math.round(hitChance);

        }
        else
        {

            double defenseBonus = 0;
            int defenseLevel = victim.getSkillLevel(Skill.DEFENCE);

            // -- Calculate bonuses based on attackers combat style
            if (attacker.isPlayer())
            {
                if (((Player) attacker).getEquipment().get(3) == null)
                    defenseBonus = victim.getBonus(CombatConstants.WEAPON_INTERFACE_DATA[10][attacker.getCombatState().getCombatStyle().getId() + 1] + 5);
                else
                    for (int index = 0; index < CombatConstants.WEAPON_INTERFACE_DATA.length; index++)
                        if (CombatConstants.WEAPON_INTERFACE_DATA[index][0] == Weapons.WeaponLoader.getWeapon(((Player) attacker).getEquipment().get(3).getId()).getInterfaceId())
                            defenseBonus = victim.getBonus(CombatConstants.WEAPON_INTERFACE_DATA[index][attacker.getCombatState().getCombatStyle().getId() + 1] + 5);
            }
            else
            {
                // -- Npc's..
                defenseBonus = (victim.getBonus(5) + victim.getBonus(6) + victim.getBonus(7)) / 3;
            }

            /**
             * Gets the bonus for DEFENSIVE prayers.
             */
            if (victim.isPlayer())
            {
                if (((Player) victim).getActivePrayers()[Prayer.THICK_SKIN])
                    defenseLevel *= 1.05;
                if (((Player) victim).getActivePrayers()[Prayer.ROCK_SKIN])
                    defenseLevel *= 1.1;
                if (((Player) victim).getActivePrayers()[Prayer.STEEL_SKIN])
                    defenseLevel *= 1.15;
            }

            defenseLevel += 8;

            switch (victim.getCombatState().getCombatStyle())
            {
            case DEFENSIVE:
                defenseLevel += 3;
                break;
            case CONTROLLED_1:
            case CONTROLLED_2:
            case CONTROLLED_3:
                defenseLevel += 1;
                break;
            }

            double blockChance = (defenseLevel * (1 + defenseBonus / 64)) * DEFENCE_MODIFIER;

            return Math.round(blockChance);
        }
    }

    /**
     * Gets the Magic Hit Accuracy.
     */
    public static double getMagicAccuracy(Entity entity, boolean defensive_calc)
    {

        if (entity == null)
            return 0;

        // -- Calculating attackive hit chance % ...
        if (!defensive_calc)
        {

            int magicLevel = entity.getSkillLevel(Skill.MAGIC);
            //default double attackBonus = (entity.isPlayer() ? entity.getBonus(3) : 0); 
            double attackBonus = (entity.isPlayer() ? entity.getBonus(3) : 250); //let's try setting npc attack bonus to 250
            
            if (entity.isPlayer())
            {
                if (((Player) entity).getActivePrayers()[Prayer.MYSTIC_WILL])
                    magicLevel *= 1.05;
                if (((Player) entity).getActivePrayers()[Prayer.MYSTIC_LORE])
                    magicLevel *= 1.1;
                if (((Player) entity).getActivePrayers()[Prayer.MYSTIC_MIGHT])
                    magicLevel *= 1.15;
            }        

            // Spell accuary == Spell requiring lower magic level will have
            // small
            // chance hitting more accurate than spell that has high req.
            if (entity.isPlayer())
            {
                int reqLvl = SpellLoader.getSpellDefinitions()[Magic.getInstance().getMagicIndex(entity)].getRequiredLevel();
                int deltaDiffereance = (entity.getSkillLevel(Skill.MAGIC) - reqLvl);
                magicLevel += (deltaDiffereance * 0.325);
            }

            magicLevel += 8;
            double hitChance = 1.3 + (magicLevel / 10) + (attackBonus / 64) + ((magicLevel * (attackBonus <= 0.0 ? attackBonus * 0.9 : attackBonus)) / 640);

            /**
             * Void bonus
             */
            hitChance *= CombatItemEffects.HAS_VOID_BONUS(entity, 0);
            System.out.println("NPC Magic hitChance" + Math.round(hitChance));
            return Math.round(hitChance);

        }
        else
        {

            int victim_defenseLevel = entity.getSkillLevel(Skill.DEFENCE);
            int victimMagicLevel = entity.getSkillLevel(Skill.MAGIC);
            double defenseBonus = entity.getBonus(8);

            /**
             * Gets the bonus for DEFENSIVE prayers.
             */
            if (entity.isPlayer())
            {
                if (((Player) entity).getActivePrayers()[Prayer.THICK_SKIN])
                    victim_defenseLevel *= 1.05;
                if (((Player) entity).getActivePrayers()[Prayer.ROCK_SKIN])
                    victim_defenseLevel *= 1.1;
                if (((Player) entity).getActivePrayers()[Prayer.STEEL_SKIN])
                    victim_defenseLevel *= 1.15;
            }

            victim_defenseLevel += 8;

            switch (entity.getCombatState().getCombatStyle())
            {
            case DEFENSIVE:
                victim_defenseLevel += 3;
                break;
            case CONTROLLED_1:
            case CONTROLLED_2:
            case CONTROLLED_3:
                victim_defenseLevel += 1;
                break;
            }

            /**
             * Gets the bonus for DEFENSIVE prayers.
             */
            if (entity.isPlayer())
            {
                if (((Player) entity).getActivePrayers()[Prayer.MYSTIC_WILL])
                    victimMagicLevel *= 1.05;
                if (((Player) entity).getActivePrayers()[Prayer.MYSTIC_LORE])
                    victimMagicLevel *= 1.1;
                if (((Player) entity).getActivePrayers()[Prayer.MYSTIC_MIGHT])
                    victimMagicLevel *= 1.15;
            }

            victimMagicLevel += 8;

            double effectiveDefense = (victim_defenseLevel * 0.3) + (victimMagicLevel * 0.7);

            double blockChance = 1.3 + ((effectiveDefense / 10) + (defenseBonus / 64) + ((effectiveDefense * (defenseBonus <= 0.0 ? defenseBonus * 0.9 : defenseBonus)) / 640));

            return Math.round(blockChance);
        }
    }

    /**
     * Gets the RANGED accuary.
     */
    public static double getRangedAccuracy(Entity entity, boolean isSpecial, boolean defensive_calc)
    {

        if (entity == null)
            return 0;

        // -- We're calculating offensive...
        if (!defensive_calc)
        {

            double attackBonus = (entity.isPlayer() ? entity.getBonus(4) : 0);
            int rangedLevel = entity.getSkillLevel(Skill.RANGED);

            /**
             * Gets prayer effect for RANGED prayers.
             */
            if (entity.isPlayer())
            {
                if (((Player) entity).getActivePrayers()[Prayer.SHARP_EYE])
                    rangedLevel *= 1.05;
                if (((Player) entity).getActivePrayers()[Prayer.HAWK_EYE])
                    rangedLevel *= 1.1;
                if (((Player) entity).getActivePrayers()[Prayer.EAGLE_EYE])
                    rangedLevel *= 1.15;
            }

            rangedLevel += 8;

            if (entity.getCombatState().getCombatStyle() == CombatStyle.ACCURATE)
                rangedLevel += 3;

            double hitChance = rangedLevel * (1 + attackBonus / 64);

            /**
             * Void bonus
             */
            hitChance *= CombatItemEffects.HAS_VOID_BONUS(entity, 1);

            /**
             * Special attacks
             */
            if (entity.isPlayer())
            {

                if (((Player) entity).getEquipment().get(3) != null && isSpecial)
                {

                    int weaponId = ((Player) entity).getEquipment().get(3).getId();

                    switch (weaponId)
                    {
                    case 861:
                        hitChance *= 0.925;
                        break;
                    }
                }
            }
            return Math.round(hitChance);

        }
        else
        {

            double defenseBonus = entity.getBonus(9);

            int defenseLevel = entity.getSkillLevel(Skill.DEFENCE);

            /**
             * Gets the bonus for DEFENSIVE prayers.
             */
            if (entity.isPlayer())
            {
                if (((Player) entity).getActivePrayers()[Prayer.THICK_SKIN])
                {
                    defenseLevel *= 1.05;
                }
                if (((Player) entity).getActivePrayers()[Prayer.ROCK_SKIN])
                {
                    defenseLevel *= 1.1;
                }
                if (((Player) entity).getActivePrayers()[Prayer.STEEL_SKIN])
                {
                    defenseLevel *= 1.15;
                }
            }

            defenseLevel += 8;

            switch (entity.getCombatState().getCombatStyle())
            {
            case DEFENSIVE:
                defenseLevel += 3;
                break;
            case CONTROLLED_1:
            case CONTROLLED_2:
            case CONTROLLED_3:
                defenseLevel += 1;
                break;
            }

            double blockChance = (defenseLevel * (1 + defenseBonus / 64)) * DEFENCE_MODIFIER;

            return Math.round(blockChance);
        }
    }

    /**
     * Gets the RANGED max hit.
     * 
     * @param entity
     * @return
     */
    public static double getRangedMaxHit(Entity entity, boolean special)
    {

        int rangedLevel = entity.getSkillLevel(Skill.RANGED);

        /**
         * Gets bonus for RANGED prayers.
         */
        if (entity.isPlayer())
        {
            if (((Player) entity).getActivePrayers()[Prayer.SHARP_EYE])
                rangedLevel *= 1.05;
            if (((Player) entity).getActivePrayers()[Prayer.HAWK_EYE])
                rangedLevel *= 1.1;
            if (((Player) entity).getActivePrayers()[Prayer.EAGLE_EYE])
                rangedLevel *= 1.15;
        }

        if (entity.getCombatState().getCombatStyle() == CombatStyle.ACCURATE)
            rangedLevel += 3;

        double rangeBonus = 0.0;

        if (entity.isPlayer() && ((Player) entity).getEquipment().get(3) != null)
        {

            Item weapon = ((Player) entity).getEquipment().get(3);
            Item arrows = ((Player) entity).getEquipment().get(13);

            BowType bowType = RangedDataLoader.getProjectileId(weapon.getId()).getBowType();

            ArrowType arrowType = null;

            if (arrows != null)
                arrowType = RangedDataLoader.getProjectileId(arrows.getId()).getArrowType();

            RangeWeaponType rangeWeaponType = RangedDataLoader.getProjectileId(weapon.getId()).getRangeWeaponType();

            if (bowType != BowType.CRYSTAL_BOW)
                rangeBonus = (bowType == null ? (rangeWeaponType == null ? 0 : rangeWeaponType.getProjectileStrength()) : (arrowType == null ? 0 : arrowType.getProjectileStrength()));
            else
                rangeBonus = 52 + ((4223 - (weapon.getId() == 4212 ? 4214 : weapon.getId())) * 2);

            rangeBonus *= (bowType == null || arrowType == null || !special ? 1.0 : (arrowType == ArrowType.DRAGON_ARROW ? 1.5 : 1.3));

        }

        double maxHit = (5 + ((rangedLevel + 8) * (rangeBonus + 64) / 64)) / 10;

        maxHit *= CombatItemEffects.HAS_VOID_BONUS(entity, 1);

        return Math.round(maxHit);
    }

    /**
     * Gets the MELEE max hit.
     */
    public static double getMeleeMaxHit(Entity entity, Entity victim, boolean isSpecial)
    {

        double strengthLevel = entity.getSkillLevel(Skill.STRENGTH);
        double currentHpLevel = entity.getSkillLevel(Skill.HITPOINTS); // Dharoks

        double maxHpLevel = entity.getSkill().getLevelForXP(entity.getSkill().getExp()[Skill.HITPOINTS]); // Dharoks

        /**
         * Gets bonus for MELEE prayers.
         */
        if (entity.isPlayer())
        {
            if (((Player) entity).getActivePrayers()[Prayer.BURST_OF_STRENGTH])
                strengthLevel *= 1.05;
            if (((Player) entity).getActivePrayers()[Prayer.SUPERHUMAN_STRENGTH])
                strengthLevel *= 1.1;
            if (((Player) entity).getActivePrayers()[Prayer.ULTIMATE_STRENGTH])
                strengthLevel *= 1.15;
        }

        switch (entity.getCombatState().getCombatStyle())
        {

        case AGGRESSIVE:
        case AGGRESSIVE_1:
            strengthLevel += 3;
            break;
        case CONTROLLED_1:
        case CONTROLLED_2:
        case CONTROLLED_3:
            strengthLevel += 1;
            break;
        }

        /**
         * Combat style multipliers here
         */
        double strengthBonus = entity.getBonus(10);

        // -- Salve ammy has priority over black mask.
        if (victim.isNpc() && victim.getAttribute("UNDEAD") != null)
            strengthLevel = strengthLevel * (CombatItemEffects.HAS_SALVE_AMMY(entity) >= 1.0 ? CombatItemEffects.HAS_SALVE_AMMY(entity) : CombatItemEffects.HAS_BLACK_MASK(entity));

        double maxHit = ((13 + strengthLevel + (strengthBonus / 8) + ((strengthLevel * strengthBonus) / 64)) / 10);

        /**
         * Obby weapons + berserker necklace (obsidian)
         */
        maxHit *= CombatItemEffects.HAS_OBSIDIAN_BONUS(entity);

        /**
         * Void bonus
         */
        maxHit *= CombatItemEffects.HAS_VOID_BONUS(entity, 2);

        /**
         * Dharok's bonus
         */
        if (CombatItemEffects.DHAROK_SET(entity))
            maxHit *= (2 - (currentHpLevel / maxHpLevel));

        /**
         * Special attacks TODO: Modify values
         */
        if (entity.isPlayer())
        {

            if (((Player) entity).getEquipment().get(3) != null && isSpecial)
            {

                int weaponId = ((Player) entity).getEquipment().get(3).getId();

                switch (weaponId)
                {
                case 11694: // Ags
                    maxHit *= 1.25;
                    break;
                case 11696: // Bgs
                    maxHit *= 1.15;
                    break;
                case 1215: // Drag daggers
                case 1231:
                case 5680:
                case 5698:
                    maxHit *= 1.1;
                    break;
                case 1434: // D mace
                    maxHit *= 1.45;
                    break;
                }
            }
        }
        return Math.round(maxHit);
    }
}
