package com.asgarniars.rs2.content.combat.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.io.XStreamController;

public class Bonuses
{

    private final static Logger logger = Logger.getLogger(Bonuses.class.getName());

    private int itemId;
    private int[] bonuses;

    public int getItemId()
    {
        return itemId;
    }

    public int getBonus(int i)
    {
        return bonuses[i];
    }

    public static class BonusDefinition
    {

        private static Map<Integer, Bonuses> bonuses;

        public static Bonuses getBonus(int id)
        {
            return bonuses.get(id);
        }

        @SuppressWarnings("unchecked")
        public static void loadBonusDefinitions() throws FileNotFoundException
        {
            bonuses = new HashMap<Integer, Bonuses>();
            XStreamController.getInstance();
            List<Bonuses> loaded = (ArrayList<Bonuses>) XStreamController.getXStream().fromXML(new FileInputStream(Constants.CONFIG_DIRECTORY + "content/combat/bonuses.xml"));
            for (Bonuses bonus : loaded)
            {
                bonuses.put(bonus.getItemId(), bonus);
            }
            logger.info("Loaded " + bonuses.size() + " bonus definitions.");
        }
    }
}
