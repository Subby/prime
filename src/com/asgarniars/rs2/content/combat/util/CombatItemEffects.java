package com.asgarniars.rs2.content.combat.util;

import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.players.Player;

public class CombatItemEffects
{

    /**
     * Grabs the void set.
     * 
     * @param entity
     * @return
     */
    public static boolean VOID_SET(Entity entity)
    {
        return HAS_CORRECT_EQUIPMENT(entity, CombatConstants.VOID_CHEST) && HAS_CORRECT_EQUIPMENT(entity, CombatConstants.VOID_LEGS) && HAS_CORRECT_EQUIPMENT(entity, CombatConstants.VOID_GLOVES);
    }

    public static boolean HAS_ANTIFIRE_SHIELD(Entity entity)
    {
        return HAS_CORRECT_EQUIPMENT(entity, 1540) || HAS_CORRECT_EQUIPMENT(entity, 11283) || HAS_CORRECT_EQUIPMENT(entity, 11284);
    }

    /**
     * Has obsidian bonus Obsidian neck & Tzhaar weapon
     */
    public static double HAS_OBSIDIAN_BONUS(Entity entity)
    {
        if (entity.isPlayer())
            if (HAS_CORRECT_EQUIPMENT(entity, CombatConstants.OBSIDIAN_NECKLACE))
                for (int index = 0; index < 5; index++)
                    if (HAS_CORRECT_EQUIPMENT(entity, CombatConstants.OBSIDIAN_WEAPONS[index]))
                        return 1.2;
        return 1.0;
    }

    public static double HAS_BLACK_MASK(Entity entity)
    {
        if (entity.isPlayer())
            for (int index = 0; index < CombatConstants.BLACK_MASK.length; index++)
                if (HAS_CORRECT_EQUIPMENT(entity, CombatConstants.BLACK_MASK[index]))
                    return 1.125;
        return 1.0;
    }

    public static double HAS_SALVE_AMMY(Entity entity)
    {
        if (entity.isPlayer())
        {
            if (HAS_CORRECT_EQUIPMENT(entity, CombatConstants.SALVE_AMMY_E))
                return 1.2;
            if (HAS_CORRECT_EQUIPMENT(entity, CombatConstants.SALVE_AMMY))
                return 1.15;
        }
        return 1.0;
    }

    /**
     * Get's the void bonus set to grab the correct helm.
     * 
     * @param helm MAGE_VOID_HELM 0 RANGE_VOID_HELM 1 MELEE_VOID_HELM 2
     */
    public static double HAS_VOID_BONUS(Entity entity, int helm)
    {
        if (VOID_SET(entity))
        {
            if (HAS_CORRECT_EQUIPMENT(entity, CombatConstants.MAGE_VOID_HELM) && helm == 0)
                return 1.30;
            if (HAS_CORRECT_EQUIPMENT(entity, CombatConstants.RANGE_VOID_HELM) && helm == 1)
                return 1.10;
            if (HAS_CORRECT_EQUIPMENT(entity, CombatConstants.MELEE_VOID_HELM) && helm == 2)
                return 1.10;
        }
        return 1.0;
    }

    /**
     * Checks for the correct items.
     * 
     * @param entity
     * @param id
     * @return
     */
    public static boolean HAS_CORRECT_EQUIPMENT(Entity entity, int id)
    {
        if (entity.isPlayer())
            return ((Player) entity).getEquipment().getItemContainer().contains(id);
        return false;
    }

    /**
     * The Dharok's set making the max hit.
     * 
     * @param entity
     * @return
     */
    public static boolean DHAROK_SET(Entity entity)
    {

        if (!entity.isPlayer())
            return false;

        boolean hasHelm = false;
        boolean hasChest = false;
        boolean hasLegs = false;
        boolean hasAxe = false;

        for (int index = 0; index < 4; index++)
            if (HAS_CORRECT_EQUIPMENT(entity, CombatConstants.DHAROK_HELM[index]))
                hasHelm = true;

        for (int index = 0; index < 4; index++)
            if (HAS_CORRECT_EQUIPMENT(entity, CombatConstants.DHAROK_CHEST[index]))
                hasChest = true;

        for (int index = 0; index < 4; index++)
            if (HAS_CORRECT_EQUIPMENT(entity, CombatConstants.DHAROK_LEGS[index]))
                hasLegs = true;

        for (int index = 0; index < 4; index++)
            if (HAS_CORRECT_EQUIPMENT(entity, CombatConstants.DHAROK_AXE[index]))
                hasAxe = true;

        if (hasHelm && hasChest && hasLegs && hasAxe)
            return true;

        return false;
    }
}
