package com.asgarniars.rs2.content.combat.util;

import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.players.Player;

// TODO need to know how skulling actually works.
public class Skulling
{

    public static void skullEntity(Entity attacker, Entity victim)
    {

        if (attacker.isPlayer() && victim.isPlayer())
        {

            Player player = (Player) attacker;

            if (!player.isSkulled() && !attacker.getCombatState().getDamageMap().getTotalDamages().containsKey(victim))
            {
                player.setSkulled(true);
            }
        }
    }
}
