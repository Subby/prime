package com.asgarniars.rs2.content.combat.util;

import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.util.Language;
import com.asgarniars.rs2.util.Misc;

//TODO look into this..
public class Poison
{

    public static final int POISON_HIT_TIMER = 30;

    public static void appendPoison(Entity entity, boolean addingPoison, int poisonDamage)
    {

        if (entity.getPoisonImmunityTimer() > 0 && addingPoison)
            return;

        if (addingPoison && entity.getPoisonDamage() < poisonDamage)
        {
            entity.setPoisonDamage(poisonDamage);
            entity.setPoisonHitTimer(POISON_HIT_TIMER);
            entity.setPoisonedTimer(0);
            entity.setPoisoned(true);
            if (entity.isPlayer())
                ((Player) entity).getActionSender().sendMessage(Language.POISONED);
        }
        else if (!addingPoison)
            entity.setPoisoned(false);
    }

    public static void applyPoisonFromWeapons(Entity attacker, Entity victim)
    {
        if (attacker.isPlayer())
        {

            if (Misc.random(10) != 0)
                return;

            Player player = (Player) attacker;

            if (player.getEquipment().get(3) == null)
                return;

            int weaponId = player.getEquipment().getItemContainer().get(3).getId();

            String weaponName = ItemManager.getInstance().getItemName(weaponId).toLowerCase();

            if (weaponName.contains("dart") || weaponName.contains("javelin") || weaponName.contains("knife") || weaponName.contains("bolt") || weaponName.contains("arrow") || weaponName.contains("jav'n"))
            {
                if (weaponName.contains("(p)"))
                {
                    appendPoison(victim, true, 2);
                }
                else if (weaponName.contains("(p+)"))
                {
                    appendPoison(victim, true, 3);
                }
                else if (weaponName.contains("(p++)"))
                {
                    appendPoison(victim, true, 4);
                }
                else if (weaponName.contains("emerald bolts (e)"))
                {
                    appendPoison(victim, true, 5);
                }
            }
            if (weaponName.contains("dagger") || weaponName.contains("spear") || weaponName.contains("hasta"))
            {
                if (weaponName.contains("(p)"))
                {
                    appendPoison(victim, true, 4);
                }
                else if (weaponName.contains("(p+)"))
                {
                    appendPoison(victim, true, 5);
                }
                else if (weaponName.contains("(p++)"))
                {
                    appendPoison(victim, true, 6);
                }
            }
        }
    }
}
