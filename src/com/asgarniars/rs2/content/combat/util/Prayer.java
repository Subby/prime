package com.asgarniars.rs2.content.combat.util;

import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.Entity.AttackTypes;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.impl.PrayerDrainTask;
import com.asgarniars.rs2.util.Language;
import com.asgarniars.rs2.util.Misc;

/**
 * @author AkZu
 * @author Joshua Barry
 * 
 */
public class Prayer
{

    private static Prayer instance;

    public static Prayer getInstance()
    {
        if (instance == null)
            instance = new Prayer();
        return instance;
    }

    public static final int THICK_SKIN = 0, BURST_OF_STRENGTH = 1, CLARITY_OF_THOUGHT = 2, SHARP_EYE = 3, MYSTIC_WILL = 4, ROCK_SKIN = 5, SUPERHUMAN_STRENGTH = 6, IMPROVED_REFLEXES = 7, RAPID_RESTORE = 8, RAPID_HEAL = 9, PROTECT_ITEM = 10, HAWK_EYE = 11, MYSTIC_LORE = 12, STEEL_SKIN = 13, ULTIMATE_STRENGTH = 14, INCREDIBLE_REFLEXES = 15, PROTECT_FROM_MAGIC = 16, PROTECT_FROM_RANGED = 17, PROTECT_FROM_MELEE = 18, EAGLE_EYE = 19, MYSTIC_MIGHT = 20, RETRIBUTION = 21, REDEMPTION = 22, SMITE = 23;

    public final static Object[][] PRAYER_DATA =
    {
    { THICK_SKIN, 83, "Thick Skin", 1, 12.0 },
    { BURST_OF_STRENGTH, 84, "Burst of Strength", 4, 12.0 },
    { CLARITY_OF_THOUGHT, 85, "Clarity of Thought", 7, 12.0 },
    { SHARP_EYE, 700, "Sharp Eye", 8, 12.0 },
    { MYSTIC_WILL, 701, "Mystic Will", 9, 12.0 },
    { ROCK_SKIN, 86, "Rock Skin", 10, 6.0 },
    { SUPERHUMAN_STRENGTH, 87, "Superhuman Strength", 13, 6.0 },
    { IMPROVED_REFLEXES, 88, "Improved Reflexes", 16, 6.0 },
    { RAPID_RESTORE, 89, "Rapid Restore", 19, 36.0 },
    { RAPID_HEAL, 90, "Rapid Heal", 22, 18.0 },
    { PROTECT_ITEM, 91, "Protect Item", 25, 18.0 },
    { HAWK_EYE, 702, "Hawk Eye", 26, 6.0 },
    { MYSTIC_LORE, 703, "Mystic Lore", 27, 6.0 },
    { STEEL_SKIN, 92, "Steel Skin", 28, 3.0 },
    { ULTIMATE_STRENGTH, 93, "Ultimate Strength", 31, 3.0 },
    { INCREDIBLE_REFLEXES, 94, "Incredible Reflexes", 34, 3.0 },
    { PROTECT_FROM_MAGIC, 95, "Protect from Magic", 37, 3.0 },
    { PROTECT_FROM_RANGED, 96, "Protect from Range", 40, 3.0 },
    { PROTECT_FROM_MELEE, 97, "Protect from Melee", 43, 3.0 },
    { EAGLE_EYE, 704, "Eagle Eye", 44, 3.0 },
    { MYSTIC_MIGHT, 705, "Mystic Might", 45, 3.0 },
    { RETRIBUTION, 98, "Retribution", 46, 12.0 },
    { REDEMPTION, 99, "Redemption", 49, 6.0 },
    { SMITE, 100, "Smite", 52, 1.8 }, };

    public void activatePrayer(Player player, int id, boolean deactivate)
    {

        int config = (Integer) PRAYER_DATA[id][1];
        String name = (String) PRAYER_DATA[id][2];
        int level = (Integer) PRAYER_DATA[id][3];

        if (player.getAttribute("duel_button_prayer") != null)
        {
            player.getActionSender().sendMessage("You're not allowed to use prayers in this duel session.");
            player.getActionSender().sendConfig(config, 0);
            return;
        }

        if (deactivate)
            player.getActivePrayers()[id] = false;

        if (player.getAttribute("dScimEffect") != null && (id == 16 || id == 17 || id == 18))
        {
            player.getActionSender().sendConfig(config, 0);
            return;
        }

        if (player.getSkill().getLevelForXP(player.getSkill().getExp()[5]) < level)
        {
            player.getActionSender().sendString("You need a @blu@prayer level of at least " + level + " to use " + name + ".", 357);
            player.getActionSender().sendChatInterface(356);
            player.getActionSender().sendConfig(config, 0);
            return;
        }

        if (player.getSkill().getPrayerPoints() <= 0.0)
        {
            player.getActionSender().sendConfig(config, 0);
            player.getActionSender().sendMessage(Language.OUT_OF_PRAYER);
            return;
        }

        int headIcon = -1;
        boolean hasHeadIcon = false;

        switch (id)
        {
        case PROTECT_FROM_MAGIC:
            headIcon = 2;
            hasHeadIcon = true;
            break;
        case PROTECT_FROM_RANGED:
            headIcon = 1;
            hasHeadIcon = true;
            break;
        case PROTECT_FROM_MELEE:
            headIcon = 0;
            hasHeadIcon = true;
            break;
        case RETRIBUTION:
            headIcon = 3;
            hasHeadIcon = true;
            break;
        case REDEMPTION:
            headIcon = 5;
            hasHeadIcon = true;
            break;
        case SMITE:
            headIcon = 4;
            hasHeadIcon = true;
            break;
        }

        if (hasHeadIcon)
            player.setPrayerIcon(!player.getActivePrayers()[id] ? headIcon : -1);

        if (!deactivate)
            player.getActivePrayers()[id] = !player.getActivePrayers()[id];

        player.getActionSender().sendConfig(config, player.getActivePrayers()[id] ? 1 : 0);

        switchPrayers(player, id);

        if (player.getPrayerDrainTask() == null)
        {
            player.setPrayerDrainTask(new PrayerDrainTask(player));
            World.submit(player.getPrayerDrainTask());
        }

        boolean prayersFound = false;

        for (int i = 0; i < player.getActivePrayers().length; i++)
        {
            if (player.getActivePrayers()[i])
            {
                prayersFound = true;
                break;
            }
        }

        if (!prayersFound)
        {
            if (player.getPrayerDrainTask() != null)
            {
                player.getPrayerDrainTask().stop();
                player.setPrayerDrainTask(null);
            }
        }

        player.setAppearanceUpdateRequired(true);
    }

    private void switchPrayers(Player player, int id)
    {
        int[] turnOff = new int[0];
        switch (id)
        {
        case THICK_SKIN:
            turnOff = new int[]
            { ROCK_SKIN, STEEL_SKIN };
            break;
        case ROCK_SKIN:
            turnOff = new int[]
            { THICK_SKIN, STEEL_SKIN };
            break;
        case STEEL_SKIN:
            turnOff = new int[]
            { THICK_SKIN, ROCK_SKIN };
            break;
        case CLARITY_OF_THOUGHT:
            turnOff = new int[]
            { IMPROVED_REFLEXES, INCREDIBLE_REFLEXES, SHARP_EYE, MYSTIC_WILL, HAWK_EYE, MYSTIC_LORE, EAGLE_EYE, MYSTIC_MIGHT };
            break;
        case IMPROVED_REFLEXES:
            turnOff = new int[]
            { CLARITY_OF_THOUGHT, INCREDIBLE_REFLEXES, SHARP_EYE, MYSTIC_WILL, HAWK_EYE, MYSTIC_LORE, EAGLE_EYE, MYSTIC_MIGHT };
            break;
        case INCREDIBLE_REFLEXES:
            turnOff = new int[]
            { IMPROVED_REFLEXES, CLARITY_OF_THOUGHT, SHARP_EYE, MYSTIC_WILL, HAWK_EYE, MYSTIC_LORE, EAGLE_EYE, MYSTIC_MIGHT };
            break;
        case BURST_OF_STRENGTH:
            turnOff = new int[]
            { SUPERHUMAN_STRENGTH, ULTIMATE_STRENGTH, SHARP_EYE, MYSTIC_WILL, HAWK_EYE, MYSTIC_LORE, EAGLE_EYE, MYSTIC_MIGHT };
            break;
        case SUPERHUMAN_STRENGTH:
            turnOff = new int[]
            { BURST_OF_STRENGTH, ULTIMATE_STRENGTH, SHARP_EYE, MYSTIC_WILL, HAWK_EYE, MYSTIC_LORE, EAGLE_EYE, MYSTIC_MIGHT };
            break;
        case ULTIMATE_STRENGTH:
            turnOff = new int[]
            { SUPERHUMAN_STRENGTH, BURST_OF_STRENGTH, SHARP_EYE, MYSTIC_WILL, HAWK_EYE, MYSTIC_LORE, EAGLE_EYE, MYSTIC_MIGHT };
            break;
        case SHARP_EYE:
            turnOff = new int[]
            { MYSTIC_WILL, HAWK_EYE, MYSTIC_LORE, EAGLE_EYE, MYSTIC_MIGHT, BURST_OF_STRENGTH, SUPERHUMAN_STRENGTH, ULTIMATE_STRENGTH, CLARITY_OF_THOUGHT, IMPROVED_REFLEXES, INCREDIBLE_REFLEXES };
            break;
        case HAWK_EYE:
            turnOff = new int[]
            { MYSTIC_WILL, SHARP_EYE, MYSTIC_LORE, EAGLE_EYE, MYSTIC_MIGHT, BURST_OF_STRENGTH, SUPERHUMAN_STRENGTH, ULTIMATE_STRENGTH, CLARITY_OF_THOUGHT, IMPROVED_REFLEXES, INCREDIBLE_REFLEXES };
            break;
        case EAGLE_EYE:
            turnOff = new int[]
            { MYSTIC_WILL, HAWK_EYE, MYSTIC_LORE, SHARP_EYE, MYSTIC_MIGHT, BURST_OF_STRENGTH, SUPERHUMAN_STRENGTH, ULTIMATE_STRENGTH, CLARITY_OF_THOUGHT, IMPROVED_REFLEXES, INCREDIBLE_REFLEXES };
            break;
        case MYSTIC_WILL:
            turnOff = new int[]
            { SHARP_EYE, HAWK_EYE, MYSTIC_LORE, EAGLE_EYE, MYSTIC_MIGHT, BURST_OF_STRENGTH, SUPERHUMAN_STRENGTH, ULTIMATE_STRENGTH, CLARITY_OF_THOUGHT, IMPROVED_REFLEXES, INCREDIBLE_REFLEXES };
            break;
        case MYSTIC_LORE:
            turnOff = new int[]
            { MYSTIC_WILL, HAWK_EYE, SHARP_EYE, EAGLE_EYE, MYSTIC_MIGHT, BURST_OF_STRENGTH, SUPERHUMAN_STRENGTH, ULTIMATE_STRENGTH, CLARITY_OF_THOUGHT, IMPROVED_REFLEXES, INCREDIBLE_REFLEXES };
            break;
        case MYSTIC_MIGHT:
            turnOff = new int[]
            { MYSTIC_WILL, HAWK_EYE, MYSTIC_LORE, EAGLE_EYE, SHARP_EYE, BURST_OF_STRENGTH, SUPERHUMAN_STRENGTH, ULTIMATE_STRENGTH, CLARITY_OF_THOUGHT, IMPROVED_REFLEXES, INCREDIBLE_REFLEXES };
            break;
        case PROTECT_FROM_MAGIC:
            turnOff = new int[]
            { REDEMPTION, SMITE, RETRIBUTION, PROTECT_FROM_RANGED, PROTECT_FROM_MELEE };
            break;
        case PROTECT_FROM_RANGED:
            turnOff = new int[]
            { REDEMPTION, SMITE, RETRIBUTION, PROTECT_FROM_MAGIC, PROTECT_FROM_MELEE };
            break;
        case PROTECT_FROM_MELEE:
            turnOff = new int[]
            { REDEMPTION, SMITE, RETRIBUTION, PROTECT_FROM_RANGED, PROTECT_FROM_MAGIC };
            break;
        case RETRIBUTION:
            turnOff = new int[]
            { REDEMPTION, SMITE, PROTECT_FROM_MELEE, PROTECT_FROM_RANGED, PROTECT_FROM_MAGIC };
            break;
        case REDEMPTION:
            turnOff = new int[]
            { RETRIBUTION, SMITE, PROTECT_FROM_MELEE, PROTECT_FROM_RANGED, PROTECT_FROM_MAGIC };
            break;
        case SMITE:
            turnOff = new int[]
            { REDEMPTION, RETRIBUTION, PROTECT_FROM_MELEE, PROTECT_FROM_RANGED, PROTECT_FROM_MAGIC };
            break;
        }

        for (int i : turnOff)
        {
            if (i != id)
            {
                player.getActivePrayers()[i] = false;
                player.getActionSender().sendConfig((Integer) PRAYER_DATA[i][1], 0);
            }
        }

    }

    public void drainPrayer(Player player, double drainAmount)
    {
        if (player.getSkill().getPrayerPoints() <= 0.0 || (player.getSkill().getPrayerPoints() - drainAmount) <= 0.0)
        {
            player.getSkill().setPrayerPoints(0);
            player.getSkill().refresh(Skill.PRAYER);
            player.getActionSender().sendMessage(Language.PRAYER_DROP);
            resetAll(player);
            return;
        }
        player.getSkill().setPrayerPoints(player.getSkill().getPrayerPoints() - drainAmount);
        player.getSkill().refresh(Skill.PRAYER);
    }

    // TODO
    public void applySmiteEffect(Player player, Player victim, int hit)
    {
        if (player.getActivePrayers()[SMITE])
        {
            if (victim.getSkill().getPrayerPoints() <= 0.0)
            {
                return;
            }
            if (victim.getSkill().getPrayerPoints() >= (victim.getSkill().getPrayerPoints() - (hit / 4)))
            {
                victim.getSkill().setPrayerPoints(victim.getSkill().getPrayerPoints() - (hit / 4));
                victim.getSkill().refresh(Skill.PRAYER);
            }
        }
    }

    public void applyRedemptionPrayer(Player player)
    {
        if (player.getActivePrayers()[REDEMPTION])
        {
            if (player.getSkill().getLevel()[Skill.HITPOINTS] <= (int) (player.getSkill().getLevelForXP(player.getSkill().getExp()[Skill.HITPOINTS]) * 0.1))
            {
                player.getSkill().getLevel()[Skill.HITPOINTS] += (int) (player.getSkill().getLevelForXP(player.getSkill().getExp()[Skill.PRAYER]) * 0.25);
                player.getSkill().setPrayerPoints(0);
                player.getUpdateFlags().sendGraphic(436, 0);
                player.getSkill().refresh(Skill.PRAYER);
                player.getSkill().refresh(Skill.HITPOINTS);
            }
        }
    }

    // TODO make it send entity Victim as args too.
    public void applyRetributionPrayer(Player player)
    {
        if (player.getActivePrayers()[RETRIBUTION])
        {
            if (player.getAttribute("isDead") != null)
            {
                // TODO Check for multi-area
                // implement that
                if (player.getCombatingEntity() != null)
                {
                    if (Misc.getDistance(player.getPosition(), player.getCombatingEntity().getPosition()) < 4)
                    {
                        Player other = (Player) player.getCombatingEntity();
                        if (other.getMinigame() == null || (other.getMinigame() != null && other.getMinigame().getGameName() != "Duel Arena"))
                            player.getCombatingEntity().hit((int) (player.getSkill().getLevelForXP(player.getSkill().getExp()[5]) * 0.25), 1, false);
                    }
                    if (player.getCombatingEntity().isPlayer())
                    {
                        Player otherPlayer = (Player) player.getCombatingEntity();
                        otherPlayer.getSkill().refresh(Skill.HITPOINTS);
                    }
                }
                player.getUpdateFlags().sendGraphic(437, 0);
            }
        }
    }

    public int prayerHitModifiers(Entity attacker, Entity victim, int hit, final AttackTypes attackType, boolean[] prayers)
    {
        if (attacker.isPlayer())
        {
        	if (victim.isPlayer()) {
        		if (prayers[PROTECT_FROM_MELEE] && attackType == AttackTypes.MELEE)
        		{
        			hit = (int) (attacker.isPlayer() ? (hit * 0.6) : 0);
        		}
        		else if (prayers[PROTECT_FROM_RANGED] && attackType == AttackTypes.RANGED)
        		{
        			hit = (int) (attacker.isPlayer() ? (hit * 0.6) : 0);
        		}
        		else if (prayers[PROTECT_FROM_MAGIC] && attackType == AttackTypes.MAGIC)
        		{
        			hit = (int) (attacker.isPlayer() ? (hit * 0.6) : 0);
        		}
        	}
        } else if(attacker.isNpc()) {
        	if (victim.isPlayer()) {
        		if (prayers[PROTECT_FROM_MELEE] && attackType == AttackTypes.MELEE)
        		{
        			hit = 0;
        		}
        		else if (prayers[PROTECT_FROM_RANGED] && attackType == AttackTypes.RANGED)
        		{
        			hit = 0;
        		}
        		else if (prayers[PROTECT_FROM_MAGIC] && attackType == AttackTypes.MAGIC)
        		{
        			hit = 0;
        		}
        	}       	
        }
        return hit;
    }

    public void resetAll(Player player)
    {
        for (int i = 0; i < 24; i++)
        {
            if (player.getActivePrayers()[i])
            {
                player.getActivePrayers()[i] = false;
                player.getActionSender().sendConfig((Integer) PRAYER_DATA[i][1], 0);
            }
        }

        if (player.getPrayerDrainTask() != null)
        {
            player.getPrayerDrainTask().stop();
            player.setPrayerDrainTask(null);
        }

        player.setPrayerIcon(-1);
        player.setAppearanceUpdateRequired(true);
    }

    public void refreshOnLogin(Player player)
    {
        for (int i = 0; i < 24; i++)
            player.getActionSender().sendConfig((Integer) PRAYER_DATA[i][1], 0);
    }

    public void setPrayers(Player player, int buttonId)
    {
        switch (buttonId)
        {
        case 5609:
            activatePrayer(player, THICK_SKIN, false);
            break;
        case 5610:
            activatePrayer(player, BURST_OF_STRENGTH, false);
            break;
        case 5611:
            activatePrayer(player, CLARITY_OF_THOUGHT, false);
            break;
        case 5607:
            activatePrayer(player, SHARP_EYE, false);
            break;
        case 6030:
            activatePrayer(player, MYSTIC_WILL, false);
            break;
        case 5612:
            activatePrayer(player, ROCK_SKIN, false);
            break;
        case 5613:
            activatePrayer(player, SUPERHUMAN_STRENGTH, false);
            break;
        case 5614:
            activatePrayer(player, IMPROVED_REFLEXES, false);
            break;
        case 5615:
            activatePrayer(player, RAPID_RESTORE, false);
            break;
        case 5616:
            activatePrayer(player, RAPID_HEAL, false);
            break;
        case 5617:
            activatePrayer(player, PROTECT_ITEM, false);
            break;
        case 6401:
            activatePrayer(player, HAWK_EYE, false);
            break;
        case 6834:
            activatePrayer(player, MYSTIC_LORE, false);
            break;
        case 5618:
            activatePrayer(player, STEEL_SKIN, false);
            break;
        case 5619:
            activatePrayer(player, ULTIMATE_STRENGTH, false);
            break;
        case 5620:
            activatePrayer(player, INCREDIBLE_REFLEXES, false);
            break;
        case 5621:
            activatePrayer(player, PROTECT_FROM_MAGIC, false);
            break;
        case 5622:
            activatePrayer(player, PROTECT_FROM_RANGED, false);
            break;
        case 5623:
            activatePrayer(player, PROTECT_FROM_MELEE, false);
            break;
        case 6967:
            activatePrayer(player, EAGLE_EYE, false);
            break;
        case 6990:
            activatePrayer(player, MYSTIC_MIGHT, false);
            break;
        case 683:
            activatePrayer(player, RETRIBUTION, false);
            break;
        case 684:
            activatePrayer(player, REDEMPTION, false);
            break;
        case 685:
            activatePrayer(player, SMITE, false);
            break;
        }
    }

}
