package com.asgarniars.rs2.content.combat.util;

import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;
import com.asgarniars.rs2.util.Language;

public class FreezeEntity
{

    public static void freezeEntity(final Entity entity, final int freezeTimer, final int immunityTimer, boolean flag)
    {

        if (entity != null && (entity.getAttribute("isFrozen") != null || entity.getAttribute("cantBeFrozen") != null))
            return;

        entity.getMovementHandler().reset();

        if (entity.isPlayer())
        {
            if (flag)
                ((Player) entity).getActionSender().sendMessage(Language.FROZEN);
            ((Player) entity).getActionSender().removeMapFlag();
        }

        entity.setAttribute("isFrozen", (byte) 0);

        World.submit(new Task(freezeTimer)
        {
            @Override
            protected void execute()
            {
                if (entity == null || entity.getAttribute("isDead") != null)
                {
                    this.stop();
                    return;
                }
                entity.removeAttribute("isFrozen");
                entity.setAttribute("cantBeFrozen", 0);
                World.submit(new Task(immunityTimer)
                {
                    @Override
                    protected void execute()
                    {
                        if (entity == null || entity.getAttribute("isDead") != null)
                        {
                            this.stop();
                            return;
                        }
                        entity.removeAttribute("cantBeFrozen");
                        this.stop();
                    }
                });
                this.stop();
            }
        });
    }
}
