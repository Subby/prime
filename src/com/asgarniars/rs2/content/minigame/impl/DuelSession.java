package com.asgarniars.rs2.content.minigame.impl;

import com.asgarniars.rs2.content.duel.Course;
import com.asgarniars.rs2.content.duel.DuelArena;
import com.asgarniars.rs2.content.minigame.ItemSafety;
import com.asgarniars.rs2.content.minigame.Minigame;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.area.Area;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * @author Joshua Barry
 */
public class DuelSession implements Minigame<Entity>
{

    private Area area;

    private Course course;

    private Player player;

    private Player opponent;

    private Object[] endPosition;

    private boolean started;

    /**
     * 
     * @param player
     * @param opponent
     * @param course
     */
    public DuelSession(Player player, Player opponent, Course course)
    {
        this.course = course;
        this.player = player;
        this.opponent = opponent;
        area = new Area(course.getMinimumPosition(), course.getMaximumPosition());
        endPosition = new Object[]
        { new Position(3355, 3267, 0), 0, 24, 13 };
    }

    @Override
    public Area getGameArea()
    {
        return area;
    }

    @Override
    public Object[] getEndPosition()
    {
        return endPosition;
    }

    @Override
    public Object[] getStartPosition()
    {
        return null;
    }

    @Override
    public ItemSafety getItemSafety()
    {
        return ItemSafety.SAFE;
    }

    @Override
    public void start()
    {

        player.setMinigame(this);
        opponent.setMinigame(this);

        if (player.getAttribute("duel_button_movement") != null)
        {
            player.setCanWalk(false);
            opponent.setCanWalk(false);
            player.clipTeleport(new Position(course.getPlayerSpawnPosition().getX(), course.getPlayerSpawnPosition().getY() - 5, course.getPlayerSpawnPosition().getZ()), 0, 18, 9);
            opponent.teleport(new Position(player.getPosition().getX(), player.getPosition().getY() + 1, player.getPosition().getZ()));
        }
        else
        {
            player.clipTeleport(course.getPlayerSpawnPosition(), 2);
            opponent.clipTeleport(course.getOpponentSpawnPosition(), 2);
        }

        player.getActionSender().createPlayerHints(opponent.getIndex());
        opponent.getActionSender().createPlayerHints(player.getIndex());

        // Shout 3 2 1 FIGHT!
        player.setAttribute("duel_countdown", 0);
        opponent.setAttribute("duel_countdown", 0);
        World.submit(new Task(2, true)
        {
            byte shouts = 3;

            @Override
            protected void execute()
            {
                if (shouts == 0)
                {
                    opponent.getUpdateFlags().sendForceMessage("FIGHT!");
                    player.getUpdateFlags().sendForceMessage("FIGHT!");
                    player.removeAttribute("duel_countdown");
                    opponent.removeAttribute("duel_countdown");
                    started = true;
                    this.stop();
                    return;
                }
                opponent.getUpdateFlags().sendForceMessage("" + shouts);
                player.getUpdateFlags().sendForceMessage("" + shouts);
                shouts--;
            }
        });
    }

    @Override
    public void end()
    {
    }

    @Override
    public void quit(Player player)
    {
    }

    @Override
    public void movementListener(Player player)
    {
    }

    @Override
    public boolean attackListener(Player player, Entity victim)
    {
        if (!started)
            player.getActionSender().sendMessage("The duel has not started yet!");
        return started;
    }

    @Override
    public boolean defeatListener(Player player, Entity victim)
    {
        DuelArena.getInstance().send_victory((Player) victim, false);
        return true;
    }

    @Override
    public boolean sessionListener(Player player)
    {
        return true;
    }

    @Override
    public String getGameName()
    {
        return "Duel Arena";
    }

    @Override
    public int getGameInterfaceId()
    {
        return -1;
    }

    public Course getCourse()
    {
        return course;
    }

    @Override
    public String getDeadMessage()
    {
        return "Oh dear, you have lost the duel!";
    }

    /**
     * @return if the match of duel has started, i.e after 3 2 1 FIGHT!
     */
    public boolean isStarted()
    {
        return started;
    }

    @Override
    public Task getGameCycle()
    {
        return null;
    }

    @Override
    public void init()
    {
    }

}
