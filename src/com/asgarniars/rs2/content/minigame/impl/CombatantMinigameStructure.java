package com.asgarniars.rs2.content.minigame.impl;

import java.util.logging.Logger;

import com.asgarniars.rs2.content.minigame.Minigame;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * 
 * @author Joshua Barry
 * 
 */
public abstract class CombatantMinigameStructure implements Minigame<Entity>
{

    private final Logger logger = Logger.getAnonymousLogger();

    private Player player;

    public CombatantMinigameStructure(Player player, Entity opponent)
    {
        this.player = player;
    }

    @Override
    public Task getGameCycle()
    {
        return null;
    }

    @Override
    public void init()
    {
        if (getGameCycle() != null)
        {
            World.submit(getGameCycle());
            logger.info(getGameName().concat(" Minigame Rolling..."));
        }

    }

    @Override
    public void start()
    {
        this.player.clipTeleport((Position) getStartPosition()[0], (Integer) getStartPosition()[1], (Integer) getStartPosition()[2], (Integer) getStartPosition()[3]);
    }

    @Override
    public void end()
    {
        this.player.clipTeleport((Position) getEndPosition()[0], (Integer) getEndPosition()[1], (Integer) getEndPosition()[2], (Integer) getEndPosition()[3]);
    }

    @Override
    public void movementListener(Player player)
    {
        if (!getGameArea().isInArea(player.getPosition()))
            quit(player);
    }

}
