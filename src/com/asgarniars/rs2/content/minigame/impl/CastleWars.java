package com.asgarniars.rs2.content.minigame.impl;

import java.util.Collection;
import java.util.LinkedList;
import java.util.logging.Logger;

import com.asgarniars.rs2.content.minigame.ItemSafety;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.area.Area;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/*
 * [1:12:04 PM NDT] AkZu: 11479
 * [1:13:51 PM NDT] AkZu: sendConfig(380, 0-amount of seconds?)
 */

/**
 * Castle Wars is a minigame that involves two teams playing capture the flag in
 * a safe combat area.
 * 
 * @author Joshua Barry
 * 
 */
public class CastleWars extends MultiMinigameStructure
{

    private static final Logger logger = Logger.getAnonymousLogger();

    private Area gameArea;

    private Collection<Player> participants;

    private Collection<Player> waitingPlayers;

    private Collection<Player> saradominPlayers;

    private Collection<Player> zamorakPlayers;

    private Collection<Player> saradominWaiters;

    private Collection<Player> zamorakWaiters;

    private Item reward;

    private Position saradominBasePosition;

    private Position zamorakBasePosition;

    private Object[] endPosition;

    private String gameName;

    private boolean gameStarted;

    private byte minimumPlayers;

    private byte time;

    private byte zamorakPoints;

    private byte saradominPoints;

    private int interfaceId;

    /**
     * Construct our data members and set the game rollin'
     */
    public CastleWars()
    {
        this.gameArea = new Area(new Position(2369, 3079, 0), new Position(2430, 3135, 0));
        this.participants = new LinkedList<Player>();
        this.waitingPlayers = new LinkedList<Player>();
        this.saradominPlayers = new LinkedList<Player>();
        this.zamorakPlayers = new LinkedList<Player>();
        this.saradominWaiters = new LinkedList<Player>();
        this.zamorakWaiters = new LinkedList<Player>();
        this.reward = new Item(4067, 2);
        this.saradominBasePosition = new Position(2427, 3076, 1);
        this.zamorakBasePosition = new Position(2372, 3131, 1);
        this.endPosition = new Object[]
        { new Position(2438, 3087, 0), 0, 5, 5 };
        this.gameName = "Castle Wars";
        this.gameStarted = false;
        this.minimumPlayers = 2;
        this.time = 20;
        this.interfaceId = 11146;
        init();
    }

    /**
     * 
     * @param player
     * @param id
     */
    public void transform(Player player, int id)
    {
        player.setHumanToNpc(id);
        player.setAppearanceUpdateRequired(true);
    }

    /**
     * Appends a cloak and hood to the players inventory when they join a team.
     * 
     * @param player The player who receives the cloak.
     * @param team The team the player has joined.
     */
    public void appendCloak(Player player, Team team)
    {
        if (team == null)
            return;
        Item hood = new Item(team.getHoodId());
        Item cloak = new Item(team.getCloakId());
        // Add it
        player.getEquipment().getItemContainer().set(0, hood);
        player.getEquipment().getItemContainer().set(1, cloak);
        // Refresh it
        player.getEquipment().refresh(0, hood);
        player.getEquipment().refresh(1, cloak);
        player.setAppearanceUpdateRequired(true);
        // Correct bonuses
        player.getEquipment().setBonus();
    }

    /**
     * Removed a cloak set from a player who is done with the castle wars game.
     * 
     * @param player The player to remove the cloak from.
     * @param team The team they were on.
     */
    public void removeCloak(Player player, Team team)
    {
        if (team == null)
            return;
        Item hood = new Item(team.getHoodId());
        Item cloak = new Item(team.getCloakId());

        player.getEquipment().getItemContainer().remove(hood, 0);
        player.getEquipment().getItemContainer().remove(cloak, 1);
        // Refresh it
        player.getEquipment().refresh(0, null);
        player.getEquipment().refresh(1, null);
        player.setAppearanceUpdateRequired(true);
        // Correct bonuses
        player.getEquipment().setBonus();
    }

    /**
     * Determines whether a player could join a team or not.
     * 
     * @param player The player attempting the join a team.
     * @param team The team they the player is attempting to join.
     */
    public boolean join(Player player, Team team)
    {

        player.getActionSender().sendWalkableInterface(11479);
        appendCloak(player, team);
        this.waitingPlayers.add(player);
        player.setAttribute("team", team);
        switch (team)
        {
        case SARADOMIN:
            player.clipTeleport(2382, 9489, 0, 3);
            this.saradominWaiters.add(player);
            return true;
        case ZAMORAK:
            player.clipTeleport(2421, 9524, 0, 3);
            this.zamorakWaiters.add(player);
            return true;
        default:
            logger.info("Cannot determine which team to join!");
            return false;
        }
    }

    /**
     * 
     * @param player
     * @param team
     */
    public boolean leave(Player player, Team team)
    {

        player.getActionSender().removeInterfaces();
        player.clipTeleport((Position) getEndPosition()[0], (Integer) getEndPosition()[1], (Integer) getEndPosition()[2], (Integer) getEndPosition()[3]);
        player.removeAttribute("team");
        removeCloak(player, team);
        if (this.waitingPlayers.contains(player))
            this.waitingPlayers.remove(player);
        else if (this.participants.contains(player))
            this.participants.remove(player);

        if (this.participants.size() <= 1)
            end();

        switch (team)
        {
        case SARADOMIN:
            if (this.saradominPlayers.contains(player))
                this.saradominPlayers.remove(player);
            else if (this.saradominWaiters.contains(player))
                this.saradominWaiters.remove(player);
            return true;
        case ZAMORAK:
            if (this.zamorakPlayers.contains(player))
                this.zamorakPlayers.remove(player);
            else if (this.zamorakWaiters.contains(player))
                this.zamorakWaiters.remove(player);
            return true;
        default:
            player.getActionSender().sendMessage("Could not determine your team. Please report this to administration.");
            return false;
        }
    }

    /**
     * Awards the winning team a prize.
     * 
     * @param team The winning team.
     */
    public void award(Team team)
    {

        switch (team)
        {
        case SARADOMIN:
            for (Player player : this.saradominPlayers)
                player.getInventory().addItem(this.reward);
            break;
        case ZAMORAK:
            for (Player player : this.saradominPlayers)
                player.getInventory().addItem(this.reward);
            break;
        }

    }

    public void update()
    {
        for (Player player : this.participants)
        {
            player.getActionSender().sendString("Zamorak = " + this.zamorakPoints, 11147).sendString(this.saradominPoints + " = Saradomin", 11148);

        }
    }

    public void increment(Team team)
    {
        switch (team)
        {
        case SARADOMIN:
            this.saradominPoints += 1;
            return;
        case ZAMORAK:
            this.zamorakPoints += 1;
            return;

        }
    }

    @Override
    public void start()
    {

        this.gameStarted = true;

        for (Player player : this.waitingPlayers)
        {
            // send the players to their bases.
            if (this.saradominWaiters.contains(player))
                player.clipTeleport(saradominBasePosition, 4);
            else if (this.zamorakWaiters.contains(player))
                player.clipTeleport(zamorakBasePosition, 4);

            // interface configuration.
            player.getActionSender().sendWalkableInterface(this.interfaceId);
            player.getActionSender().sendConfig(380, this.time);

            // normalize players in the pnpc state
            if (player.getHumanToNpc() > 0)
                transform(player, -1);
        }

        // add the waiters to the players list
        this.saradominPlayers.addAll(this.saradominWaiters);
        this.zamorakPlayers.addAll(this.zamorakWaiters);
        this.participants.addAll(this.waitingPlayers);

        // clear the waiting lists.
        this.saradominWaiters.clear();
        this.zamorakWaiters.clear();
        this.waitingPlayers.clear();

        /* submit a new world event to tick the game away */

        World.submit(new Task(100)
        {

            private int mins = time;

            @Override
            protected void execute()
            {

                if (this.mins <= 0)
                {
                    end(); // stop the minigame
                    this.stop();
                }
                else
                {

                    /* reduce the amount of minutes by 1. */
                    this.mins--;

                    /* display the remaining minutes to the player. */
                    for (Player player : participants)
                        player.getActionSender().sendConfig(380, this.mins);
                }

            }
        });
    }

    @Override
    public void end()
    {
        this.gameStarted = false;
        this.award(Team.ZAMORAK); // TODO: make this a winning team.
        for (Player player : this.participants)
        {
            Team team = (Team) player.getAttribute("team");
            player.removeAttribute("team");
            this.removeCloak(player, team);
        }
        this.zamorakPlayers.clear();
        this.saradominPlayers.clear();
        super.end();

    }

    @Override
    public Collection<Player> getParticipants()
    {
        return this.participants;
    }

    @Override
    public Collection<Player> getWaitingPlayers()
    {
        return this.waitingPlayers;
    }

    @Override
    public int getMinimumGameSize()
    {
        return this.minimumPlayers;
    }

    @Override
    public Area getGameArea()
    {
        return this.gameArea;
    }

    @Override
    public Object[] getEndPosition()
    {
        return this.endPosition;
    }

    @Override
    public Object[] getStartPosition()
    {
        return null;
    }

    @Override
    public ItemSafety getItemSafety()
    {
        return ItemSafety.SAFE;
    }

    @Override
    public Task getGameCycle()
    {
        return new Task(25)
        {
            @Override
            protected void execute()
            {
                if (!gameStarted && (waitingPlayers.size() + participants.size()) >= minimumPlayers && participants.size() <= 1)
                    start();
            }
        };
    }

    @Override
    public boolean attackListener(Player player, Entity victim)
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean defeatListener(Player player, Entity victim)
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean sessionListener(Player player)
    {
        Team team = (Team) player.getAttribute("team");
        leave(player, team);
        player.removeAttribute("team");
        return true;
    }

    @Override
    public String getGameName()
    {
        return gameName;
    }

    @Override
    public int getGameInterfaceId()
    {
        return interfaceId;
    }

    public Collection<Player> getSaradominPlayers()
    {
        return saradominPlayers;
    }

    public Collection<Player> getZamorakPlayers()
    {
        return zamorakPlayers;
    }

    public Collection<Player> getSaradominWaiters()
    {
        return saradominWaiters;
    }

    public Collection<Player> getZamorakWaiters()
    {
        return zamorakWaiters;
    }

    public byte getZamorakPoints()
    {
        return zamorakPoints;
    }

    public byte getSaradominPoints()
    {
        return saradominPoints;
    }

    /**
     * Represents the team the player joins.
     * 
     * @author Joshua Barry
     * 
     */
    public enum Team
    {
        SARADOMIN(4513, 4514), ZAMORAK(4515, 4516);

        private int hoodId;

        private int cloakId;

        /**
         * 
         * @param hoodId The ID of the hood the player receives.
         * @param cloakId The ID of the cloak the player receives.
         */
        private Team(int hoodId, int cloakId)
        {
            this.hoodId = hoodId;
            this.cloakId = cloakId;
        }

        public int getHoodId()
        {
            return hoodId;
        }

        public int getCloakId()
        {
            return cloakId;
        }

    }

    @Override
    public String getDeadMessage()
    {
        return null;
    }

}
