package com.asgarniars.rs2.content.minigame.barrows;

import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.util.Misc;

/**
 * Encapsulates each barrows brother and their respective properties.
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public enum Brother
{
    AHRIM(2025, 6821, new Position(3556, 9696, 3)), // Ahrim the Blighted
    DHAROK(2026, 6771, new Position(3551, 9714, 3)), // Dharok the Wretched
    GUTHAN(2027, 6773, new Position(3539, 9701, 3)), // Guthan the Infested
    KARIL(2028, 6822, new Position(3554, 9683, 3)), // Karil the Tainted
    TORAG(2029, 6772, new Position(3572, 9685, 3)), // Torag the Corrupted
    VERAC(2030, 6823, new Position(3570, 9706, 3)); // Verac the Defiled

    /* The ID of the Npc */
    private int npcId;

    /* The ID of the sarcophagus object */
    private int sarcophagusId;

    /* The relative postition the Npc should spawn at */
    private Position position;

    /**
     * 
     * @param npcId The NPC ID.
     * @param sarcophagusId The object ID.
     * @param position The spawn position.
     */
    private Brother(int npcId, int sarcophagusId, Position position)
    {
        this.npcId = npcId;
        this.sarcophagusId = sarcophagusId;
        this.position = position;

    }

    public int getNpcId()
    {
        return npcId;
    }
    
    public int getSarcophagusId()
    {
        return sarcophagusId;
    }

    public Position getPosition()
    {
        return position;
    }

    /**
     * Global access to a Barrows Brother
     * 
     * @param id The NPC id you want to match.
     * @return The brother corresponding to the NPC id.
     */
    public static Brother forId(int id)
    {
        for (Brother brother : Brother.values())
            if (id == brother.getNpcId())
                return brother;
        return null;

    }

    /**
     * Gets a brother based on the ID of the sarcophagus they are associated
     * with.
     * 
     * @param id The ID of the sarcophagus object.
     * @return
     */
    public static Brother forTomb(int id)
    {
        for (Brother brother : Brother.values())
            if (id == brother.getSarcophagusId())
                return brother;
        return null;

    }

    /**
     * Returns a randomised brother which we will use to enter a hidden tomb.
     * 
     * @return
     */
    public static Brother randomise()
    {
        Brother[] values = Brother.values();
        int ordinal = Misc.random(values.length);
        for (Brother brother : values)
            if (ordinal == brother.ordinal())
                return brother;
        return null;
    }

}
