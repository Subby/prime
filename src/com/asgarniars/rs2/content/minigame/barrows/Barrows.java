package com.asgarniars.rs2.content.minigame.barrows;

import com.asgarniars.rs2.content.combat.Combat;
import com.asgarniars.rs2.content.dialogue.Dialogue;
import com.asgarniars.rs2.content.dialogue.Option;
import com.asgarniars.rs2.content.dialogue.StatementDialogue;
import com.asgarniars.rs2.content.dialogue.StatementType;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.npcs.Npc.WalkType;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class Barrows
{

    /**
     * Summons a brother from their tomb.
     * 
     * @param brother
     * @param player
     */
    public static void awaken(Brother brother, Player player)
    {

        Npc npc = new Npc(brother.getNpcId());
        npc.setSpawnPosition(brother.getPosition());
        npc.setWalkType(WalkType.STAND);
        npc.setFaceType(1);
        npc.setAttribute("owner", player);
        
        World.register(npc);

        npc.getUpdateFlags().sendForceMessage("You dare disturb my rest!");

        player.getActionSender().createNpcHints(npc.getIndex());
        player.setAttribute("brother", npc);
        
        npc.setFollowingEntity(player);
        npc.setTarget(player);
        npc.setInstigatingAttack(true);
        npc.setCombatingEntity(player);
        

    }

    /**
     * 
     */
    public static void requestHiddenTomb(final Player player)
    {
        Dialogue dialogue = new StatementDialogue(StatementType.NORMAL, "You've found a hidden tunnel, do you want to enter?");
        Option yes = new Option("Yeah I'm fearless!", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                player.clipTeleport(new Position(3535, 9712, 0), 3);
                this.stop();

            }

        });
        Option no = new Option("No way, that looks scary!", new Task(1, true)
        {

            @Override
            protected void execute()
            {
                player.getActionSender().sendMessage("You chose NOT to enter!");
                this.stop();

            }

        });
        dialogue.add("Select an Option", yes, no);
        player.open(dialogue);
    }

    public static void openChest(Player player)
    {

        Brother brother = player.getAttribute("hidden_tomb");
        Npc npc = new Npc(brother.getNpcId());
        npc.setSpawnPosition(new Position(3551, 9691, 0));
        npc.setWalkType(WalkType.STAND);
        npc.setFaceType(1);
        npc.setAttribute("owner", player);

        World.register(npc);

        npc.getUpdateFlags().sendForceMessage("You dare steal from us!");

        player.getActionSender().createNpcHints(npc.getIndex());
        player.setAttribute("brother", npc);

    }

    /**
     * Called when a player defeats a brother. Increments the killcount and
     * proceeds.
     * 
     * @param brother The brother who has been slain.
     * @param player The player who defeated the brother.
     */
    public static void defeat(Player player)
    {
    	
    	incrementKillcount(player);
    	player.getActionSender().sendString("Killcount: " + player.getAttribute("barrows_killcount"), 4536);
    	player.removeAttribute("brother");
    	
    }

    /**
     * Increments the players kill count for each brother they slay.
     * 
     * @param player
     */
    public static void incrementKillcount(Player player)
    {
        /* define the key we're going to use to identify this attribute */
        String key = "barrows_killcount";

        /* check if the attribute exists, if not (first kill) we set it to 1 */
        if (player.getAttribute(key) == null)
        {
            player.setAttribute(key, 1);
            return;
        }

        /* create an integer type specific data type of the attribute */
        int killcount = (Integer) player.getAttribute(key);

        /* If you have killed all the brother's, your kill count is reset */
        if (killcount >= 6)
        {
            player.setAttribute(key, 0);
            return;
        }

        /* increment too kill count at this point */
        player.setAttribute(key, ++killcount);

    }

}
