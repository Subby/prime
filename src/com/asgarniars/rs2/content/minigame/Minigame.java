package com.asgarniars.rs2.content.minigame;

import com.asgarniars.rs2.model.area.Area;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * 
 * @author Joshua Barry
 * 
 */
public interface Minigame<T>
{

    /**
     * The boundary area of the minigame.
     * 
     * @return
     */
    public abstract Area getGameArea();

    /**
     * The clipTeleport(pos, size, xlength, ylength) arguments the player is
     * teleported to when they quit or lose the game.
     * 
     * @return
     */
    public abstract Object[] getEndPosition();

    /**
     * The clipTeleport(pos, size, xlength, ylength) arguments the player is
     * teleported to when they start the game.
     * 
     * @return
     */
    public abstract Object[] getStartPosition();

    /**
     * Determine whether the players items are safe in combat or not.
     * 
     * @return
     */
    public abstract ItemSafety getItemSafety();

    /**
     * The dead message wich will be sent to player who died.
     */
    public abstract String getDeadMessage();

    /**
     * The game cycle which calculates how often the minigame is started.
     * 
     * @return
     */
    public abstract Task getGameCycle();

    /**
     * Initialize the minigame.
     */
    public abstract void init();

    /**
     * Start the minigame.
     */
    public abstract void start();

    /**
     * End the minigame.
     */
    public abstract void end();

    /**
     * End the minigame for a particular player.
     */
    public abstract void quit(Player player);

    /**
     * A hook that checks when the player moves.
     * 
     * @param player
     */
    public abstract void movementListener(Player player);

    /**
     * 
     * @param player
     * @param victim
     * @return
     */
    public abstract boolean attackListener(Player player, T victim);

    /**
     * A hook that is called when a player kills a victim.
     * 
     * @param player
     * @param victim
     * @return
     */
    public abstract boolean defeatListener(Player player, T victim);

    /**
     * A hook that is called when a player's session state is updated (i.e :
     * login/logout)
     * 
     * @param player
     * @return
     */
    public abstract boolean sessionListener(Player player);

    /**
     * The name of the minigame.
     * 
     * @return
     */
    public abstract String getGameName();

    /**
     * The client interface ID for the minigame if applicable.
     * 
     * @return
     */
    public abstract int getGameInterfaceId();

}
