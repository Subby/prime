package com.asgarniars.rs2.content.minigame;

import java.util.Collection;

import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.area.Area;
import com.asgarniars.rs2.model.players.Player;

/**
 * A minigame for multiple players.
 * 
 * @author Joshua Barry
 * 
 */
public interface MultiMinigame extends Minigame<Entity>
{

    /**
     * The boundary of the waiting area.
     * 
     * @return
     */
    public abstract Area getWaitingArea();

    /**
     * The players participating in this game.
     * 
     * @return
     */
    public abstract Collection<Player> getParticipants();

    /**
     * The players waiting to play the next round of the game.
     * 
     * @return
     */
    public abstract Collection<Player> getWaitingPlayers();

    /**
     * The minimum amount of players required to start the game.
     * 
     * @return
     */
    public abstract int getMinimumGameSize();

}
