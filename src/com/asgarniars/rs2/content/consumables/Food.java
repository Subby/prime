package com.asgarniars.rs2.content.consumables;

import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;
import com.asgarniars.rs2.util.Misc;

/**
 * Food class. EDITED: [1/31/2012]
 * 
 * @author Jacob & AkZu
 * 
 *         TODO Thin, lean snails, beer keg, guthix tea rest, few cocktails
 */
public class Food
{

    public static void eatFood(Player player, Item item, int slot)
    {

        if (player.getAttribute("cantEat") != null)
            return;

        if (player.getAttribute("duel_button_food") != null)
        {
            player.getActionSender().sendMessage("You're not to eat in this duel session.");
            return;
        }

        player.setAttribute("cantEat", (byte) 0);

        String name = ItemManager.getInstance().getItemName(item.getId());

        String message = "You eat the " + name.toLowerCase() + ".";

        String message2 = "";

        int healAmount = FoodLoader.getFood(item.getId()).getHealAmount();

        int finalHitpoints = 1;

        boolean removeItem = true;

        switch (item.getId())
        {

        case 3146: // Poisoned karambwan.
            finalHitpoints = 0;
            message = "";
            player.getActionSender().sendMessage("You eat the poisoned karambwan..");
            player.getActionSender().sendMessage(".. and it damages you!");
            player.getInventory().removeItemSlot(slot);
            player.hit(5, 2, false);
            break;

        case 1891: // Cake
        case 1897: // Chocolate cake
            healAmount /= 3;
            message = "You eat part of the " + (item.getId() == 1891 ? "cake." : "chocolate cake.");
            player.getInventory().getItemContainer().set(slot, new Item(item.getId() == 1891 ? 1893 : 1899));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            removeItem = false;
            break;

        case 1893: // 2/3 Cake
        case 1899: // 2/3 Chocolate cake
            healAmount /= 3;
            message = "You eat some more of the " + (item.getId() == 1893 ? "cake." : "chocolate cake.");
            player.getInventory().getItemContainer().set(slot, new Item(item.getId() == 1893 ? 1895 : 1901));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            removeItem = false;
            break;

        case 1895: // 1/3 Cake
        case 1901: // 1/3 Chocolate cake
            healAmount /= 3;
            message = "You eat the slice of " + (item.getId() == 1895 ? "cake." : "chocolate cake.");
            break;

        case 2289: // Pizzas
        case 2293:
        case 2297:
        case 2301:
            healAmount /= 2;
            message = "You eat half of the " + (item.getId() == 2289 ? "pizza" : (item.getId() == 2293 ? "meat pizza" : item.getId() == 2297 ? "anchovy pizza" : "pineapple pizza")) + ".";
            player.getInventory().getItemContainer().set(slot, new Item(item.getId() + 2));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            removeItem = false;
            break;

        case 2291: // 1/2 Pizzas
        case 2295:
        case 2299:
        case 2303:
            healAmount /= 2;
            message = "You eat remaining of the " + (item.getId() == 2291 ? "pizza" : (item.getId() == 2295 ? "meat pizza" : item.getId() == 2299 ? "anchovy pizza" : "pineapple pizza")) + ".";
            break;

        case 2325: // Normal pies
        case 2327:
        case 2323:
            healAmount /= 2;
            message = "You eat half of the " + (item.getId() == 2325 ? "redberry" : item.getId() == 2327 ? "meat" : "apple") + " pie.";
            player.getInventory().getItemContainer().set(slot, new Item(item.getId() == 2325 ? 2333 : (item.getId() == 2327 ? 2331 : 2335)));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            removeItem = false;
            break;

        case 2333: // Normal pies (half's)
        case 2331:
        case 2335:
            healAmount /= 2;
            message = "You eat the remaining of the " + (item.getId() == 2333 ? "redberry" : item.getId() == 2331 ? "meat" : "apple") + " pie.";
            player.getInventory().getItemContainer().set(slot, new Item(2313));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            removeItem = false;
            break;

        case 7178: // Garden pie
            healAmount /= 2;
            message = "You eat half of the garden pie.";
            player.getInventory().getItemContainer().set(slot, new Item(7180));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().increaseLevelOnce(19, 3);
            removeItem = false;
            break;

        case 7180: // Garden pie half
            healAmount /= 2;
            message = "You eat the remaining of the garden pie.";
            player.getInventory().getItemContainer().set(slot, new Item(2313));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().increaseLevelOnce(19, 3);
            removeItem = false;
            break;

        case 7188: // Fish pie
            healAmount /= 2;
            message = "You eat half of the fish pie.";
            player.getInventory().getItemContainer().set(slot, new Item(7190));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().increaseLevelOnce(10, 3);
            removeItem = false;
            break;

        case 7190: // Fish pie half
            healAmount /= 2;
            message = "You eat the remaining of the fish pie.";
            player.getInventory().getItemContainer().set(slot, new Item(2313));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().increaseLevelOnce(10, 3);
            removeItem = false;
            break;

        case 7198: // Admiral pie
            healAmount /= 2;
            message = "You eat half of the admiral pie.";
            player.getInventory().getItemContainer().set(slot, new Item(7200));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().increaseLevelOnce(10, 5);
            removeItem = false;
            break;

        case 7200: // Admiral pie half
            healAmount /= 2;
            message = "You eat the remaining of the admiral pie.";
            player.getInventory().getItemContainer().set(slot, new Item(2313));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().increaseLevelOnce(10, 5);
            removeItem = false;
            break;

        case 2003: // Stew's
        case 7479:
        case 2011:
            message = "You eat the " + (item.getId() == 2003 ? "stew" : item.getId() == 7479 ? "spicy stew" : "curry stew") + ".";
            player.getInventory().getItemContainer().set(slot, new Item(1923));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            removeItem = false;
            break;

        case 1993: // Jug of wine
        case 1991:
            message = "You drink the wine.";
            player.getInventory().getItemContainer().set(slot, new Item(1935));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().decreaseLevel(0, 2);
            removeItem = false;
            break;

        case 1917: // Beer
            message = "You drink the beer.";
            player.getInventory().getItemContainer().set(slot, new Item(1919));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().increaseLevelOnce(2, (int) (player.getSkill().getLevelForXP(player.getSkill().getExp()[2]) * 0.04));
            player.getSkill().decreaseLevel(0, (int) (player.getSkill().getLevelForXP(player.getSkill().getExp()[0]) * 0.03));
            removeItem = false;
            break;

        case 3803: // Beer
            message = "You quaff the beer..";
            message2 = "You feel slightly reinvigorated... but very dizzy too!";
            player.getInventory().getItemContainer().set(slot, new Item(3805));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().increaseLevelOnce(2, 4);
            player.getSkill().decreaseLevel(0, 9);
            removeItem = false;
            break;

        case 1905: // Asgarnian ale
            message = "You drink the asgarnian ale.";
            player.getInventory().getItemContainer().set(slot, new Item(1919));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().increaseLevelOnce(2, 2);
            player.getSkill().decreaseLevel(0, 4);
            removeItem = false;
            break;

        case 5751: // Axeman's folly
            message = "You drink the axeman's folly.";
            player.getInventory().getItemContainer().set(slot, new Item(1919));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().decreaseLevel(0, 3);
            player.getSkill().decreaseLevel(2, 3);
            player.getSkill().increaseLevelOnce(8, 1);
            removeItem = false;
            break;

        case 5755: // Chef's delight
            message = "You drink the chef's delight.";
            player.getInventory().getItemContainer().set(slot, new Item(1919));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().getLevel()[0] -= 2;
            player.getSkill().refresh(0);
            player.getSkill().getLevel()[2] -= 2;
            player.getSkill().refresh(2);
            player.getSkill().decreaseLevel(0, 2);
            player.getSkill().decreaseLevel(2, 3);
            player.getSkill().increaseLevelOnce(7, (int) (player.getSkill().getLevel()[7] * 0.05));
            removeItem = false;
            break;

        case 5763: // Cider
            message = "You drink the cider.";
            player.getInventory().getItemContainer().set(slot, new Item(1919));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().decreaseLevel(0, 2);
            player.getSkill().decreaseLevel(2, 2);
            player.getSkill().increaseLevelOnce(19, 1);
            removeItem = false;
            break;

        case 4627: // Bandit's brew
            message = "You drink the bandit's brew.";
            player.getInventory().getItemContainer().set(slot, new Item(1919));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().increaseLevelOnce(17, 1);
            removeItem = false;
            break;

        case 1911: // Dragon bitter
            message = "You drink the dragon bitter.";
            player.getInventory().getItemContainer().set(slot, new Item(1919));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().increaseLevelOnce(2, 2);
            player.getSkill().decreaseLevel(0, 4);
            removeItem = false;
            break;

        case 1909: // Greenman's ale
            message = "You drink the greenman's ale.";
            player.getInventory().getItemContainer().set(slot, new Item(1919));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().increaseLevelOnce(15, 1);
            player.getSkill().decreaseLevel(0, 3);
            player.getSkill().decreaseLevel(1, 3);
            player.getSkill().decreaseLevel(2, 3);
            removeItem = false;
            break;

        case 1915: // Grog
            message = "You drink the grog.";
            player.getInventory().getItemContainer().set(slot, new Item(1919));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().increaseLevelOnce(2, 3);
            player.getSkill().decreaseLevel(0, 6);
            removeItem = false;
            break;

        case 431: // Karamjan rum
            message = "You drink the karamjan rum.";
            player.getSkill().increaseLevelOnce(2, 3);
            player.getSkill().decreaseLevel(0, 4);
            break;

        case 5759: // Slayer's respite
            message = "You drink the slayer's respite.";
            player.getInventory().getItemContainer().set(slot, new Item(1919));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().increaseLevelOnce(18, 1);
            player.getSkill().decreaseLevel(0, 2);
            player.getSkill().decreaseLevel(2, 2);
            removeItem = false;
            break;

        case 1907: // Wizard's mind bomb
            message = "You drink the wizard's mind bomb.";
            player.getInventory().getItemContainer().set(slot, new Item(1919));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().increaseLevelOnce(6, (player.getSkill().getLevelForXP(player.getSkill().getExp()[6]) < 50 ? 2 : 3));
            removeItem = false;
            break;

        case 712: // Cup of tea
            player.getInventory().getItemContainer().set(slot, new Item(1980));
            message = "You drink the tea.";
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getUpdateFlags().sendForceMessage("Aaah, nothing like a nice cuppa tea!");
            player.getSkill().increaseLevelOnce(0, 3);
            removeItem = false;
            break;

        case 2955: // Moonlight mead
            message = "You drink the moonlight mead..";
            message2 = "..It tastes like something died in your mouth.";
            player.getInventory().getItemContainer().set(slot, new Item(1919));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            removeItem = false;
            break;

        case 1971: // Kebab, TODO doesn't work like it should, keeping like this
            // for now.
            message = "You eat the kebab.";
            if (Misc.random(5) > 1)
            {
                finalHitpoints = (int) (0.1 * player.getSkill().getLevelForXP(player.getSkill().getExp()[3]));
            }
            else
            {
                int random = Misc.randomMinMax(10, 20);
                finalHitpoints = (player.getSkill().getLevel()[3] + random) <= player.getSkill().getLevelForXP(player.getSkill().getExp()[3]) ? player.getSkill().getLevel()[3] + random : player.getSkill().getLevelForXP(player.getSkill().getExp()[3]);

                if (finalHitpoints != player.getSkill().getLevel()[3])
                    player.getActionSender().sendMessage("That was a good kebab. You feel a lot better.");

                player.getSkill().getLevel()[3] = (byte) finalHitpoints;
                player.getSkill().refresh(3);

                finalHitpoints = 0;
            }
            break;

        case 1913: // Dwarven stout
            message = "You drink the dwarven stout.";
            player.getInventory().getItemContainer().set(slot, new Item(1919));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().increaseLevelOnce(13, 1);
            player.getSkill().increaseLevelOnce(14, 1);
            player.getSkill().decreaseLevel(0, 5);
            player.getSkill().decreaseLevel(1, 5);
            player.getSkill().decreaseLevel(2, 5);
            removeItem = false;
            break;

        case 7208: // Wild pie
            healAmount /= 2;
            message = "You eat half of the wild pie.";
            player.getInventory().getItemContainer().set(slot, new Item(7210));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().increaseLevelOnce(18, 5);
            player.getSkill().increaseLevelOnce(4, 4);
            removeItem = false;
            break;

        case 7210: // Wild pie half
            healAmount /= 2;
            message = "You eat the remaining of the wild pie.";
            player.getInventory().getItemContainer().set(slot, new Item(2313));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().increaseLevelOnce(18, 5);
            player.getSkill().increaseLevelOnce(4, 4);
            removeItem = false;
            break;

        case 7218: // Summer pie
            healAmount /= 2;
            message = "You eat half of the summer pie.";
            player.getInventory().getItemContainer().set(slot, new Item(7220));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().increaseLevelOnce(16, 5);
            player.setEnergy((int) (player.getEnergy() * 1.1));
            removeItem = false;
            break;

        case 7220: // Summer pie half
            healAmount /= 2;
            message = "You eat the remaining of the summer pie.";
            player.getInventory().getItemContainer().set(slot, new Item(2313));
            player.getInventory().refresh(slot, player.getInventory().get(slot));
            player.getSkill().increaseLevelOnce(16, 5);
            player.setEnergy((int) (player.getEnergy() * 1.1));
            removeItem = false;
            break;

        case 7060:
            message = "You eat a baked potato with tuna and sweetcorn.";
            break;
        case 7054:
            message = "You eat a baked potato with chilli.";
            break;
        case 7056:
            message = "You eat a baked potato with egg.";
            break;
        case 7058:
            message = "You eat a baked mushroom potato with onion.";
            break;
        case 1977:
        case 2084:
        case 2048:
        case 2040:
        case 2080:
        case 2074:
            message = "You drink the " + name.toLowerCase() + ".";
            break;
        }

        player.getUpdateFlags().sendAnimation(829, 0);
        player.getActionSender().sendSound((message.contains("drink") ? 334 : 317), 100, 0);

        if (removeItem)
            player.getInventory().removeItemSlot(slot);

        if (!message.isEmpty())
            player.getActionSender().sendMessage(message);

        if (!message2.isEmpty())
            player.getActionSender().sendMessage(message2);

        if (finalHitpoints != 0)
        {

            finalHitpoints = (player.getSkill().getLevel()[3] + healAmount <= player.getSkill().getLevelForXP(player.getSkill().getExp()[3]) ? player.getSkill().getLevel()[3] + healAmount : player.getSkill().getLevelForXP(player.getSkill().getExp()[3]));

            if (finalHitpoints != player.getSkill().getLevel()[3])
                player.getActionSender().sendMessage("It heals some health.");

            player.getSkill().getLevel()[3] = (byte) finalHitpoints;
            player.getSkill().refresh(3);
        }

        player.setAttackTimer(player.getAttackTimer() + 3);
        player.stopAllActions(false);
        tickAfterEating(player);
    }

    private static void tickAfterEating(final Player player)
    {
        World.submit(new Task(2)
        {
            @Override
            protected void execute()
            {
                player.removeAttribute("cantEat");
                this.stop();
            }
        });
    }
}