package com.asgarniars.rs2.content.consumables;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.io.XStreamController;

/**
 * By Mikey` of Rune-Server
 */
public class PotionLoader
{

    private final static Logger logger = Logger.getLogger(PotionLoader.class.getName());
    private static Map<Integer, PotionDefinition> potion;

    public static PotionDefinition getPotion(int id)
    {
        return potion.get(id);
    }

    @SuppressWarnings("unchecked")
    public static void loadPotionDefinitions() throws FileNotFoundException, IOException
    {

        potion = new HashMap<Integer, PotionDefinition>();
        XStreamController.getInstance();

        List<PotionDefinition> loaded = (ArrayList<PotionDefinition>) XStreamController.getXStream().fromXML(new FileInputStream(Constants.CONFIG_DIRECTORY + "content/combat/potiondef.xml"));
        for (PotionDefinition item : loaded)
        {
            for (int id : item.getPotionIds())
            {
                potion.put(id, item);
            }
        }
        logger.info("Loaded " + loaded.size() + " potion definitions.");
    }

    public static class PotionDefinition
    {

        private String potionName;
        private PotionTypes potionType;
        private int[] potionIds;
        private int[] affectedStats;
        private int[] statAddons;
        private double[] statModifiers;

        public String getPotionName()
        {
            return potionName;
        }

        public int[] getPotionIds()
        {
            return potionIds;
        }

        public int[] getAffectedStats()
        {
            return affectedStats;
        }

        public int[] getStatAddons()
        {
            return statAddons;
        }

        public double[] getStatModifiers()
        {
            return statModifiers;
        }

        public PotionTypes getPotionType()
        {
            return potionType;
        }

        enum PotionTypes
        {
            BOOST, RESTORE
        }

    }
}
