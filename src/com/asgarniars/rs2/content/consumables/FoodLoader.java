package com.asgarniars.rs2.content.consumables;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.io.XStreamController;

/**
 * 
 * @author AkZu
 * 
 */
public class FoodLoader
{

    private final static Logger logger = Logger.getLogger(FoodLoader.class.getName());
    private static Map<Integer, FoodDefinition> food;

    public static FoodDefinition getFood(int id)
    {
        return food.get(id);
    }

    @SuppressWarnings("unchecked")
    public static void loadFoodDefinitions() throws FileNotFoundException
    {
        food = new HashMap<Integer, FoodDefinition>();
        XStreamController.getInstance();

        List<FoodDefinition> loaded = (ArrayList<FoodDefinition>) XStreamController.getXStream().fromXML(new FileInputStream(Constants.CONFIG_DIRECTORY + "content/fooddef.xml"));
        for (FoodDefinition item : loaded)
        {
            for (int id : item.getFoodId())
            {
                food.put(id, item);
            }
        }
        logger.info("Loaded " + food.size() + " food definitions.");
    }

    public static class FoodDefinition
    {

        private short[] foodIds;
        private byte healAmount;

        public short[] getFoodId()
        {
            return foodIds;
        }

        public int getHealAmount()
        {
            return healAmount;
        }
    }
}