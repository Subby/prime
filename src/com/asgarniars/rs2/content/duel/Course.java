package com.asgarniars.rs2.content.duel;

import com.asgarniars.rs2.model.Position;

/**
 * Represents the different duel courses.
 * 
 * @author Joshua Barry
 * 
 */
public enum Course
{

    // Bottom left course
    EMPTY_C(new Position(3332, 3206, 0), new Position(3358, 3220, 0), new Position[]
    { new Position(3337, 3213, 0), new Position(3353, 3213, 0) }),

    // Bottom right course
    OBSTACLE_C(new Position(3363, 3206, 0), new Position(3389, 3220, 0), new Position[]
    { new Position(3367, 3213, 0), new Position(3386, 3213, 0) }),

    // Right middle course
    EMPTY_B(new Position(3363, 3225, 0), new Position(3389, 3239, 0), new Position[]
    { new Position(3367, 3232, 0), new Position(3384, 3232, 0) }),

    // Top right course
    OBSTACLE_B(new Position(3363, 3244, 0), new Position(3389, 3258, 0), new Position[]
    { new Position(3367, 3251, 0), new Position(3385, 3251, 0) }),

    // Top left course
    EMPTY_A(new Position(3332, 3244, 0), new Position(3358, 3258, 0), new Position[]
    { new Position(3336, 3251, 0), new Position(3354, 3251, 0) }),

    // Left middle course
    OBSTACLE_A(new Position(3332, 3225, 0), new Position(3358, 3239, 0), new Position[]
    { new Position(3336, 3232, 0), new Position(3354, 3232, 0) });

    private Position bottomLeft;

    private Position topRight;

    private Position[] spawnPositions;

    /**
     * 
     * @param minimumPosition The minimum position
     * @param maximumPosition The maximum position
     * @param spawnPositions The center position
     */
    private Course(Position minimumPosition, Position maximumPosition, Position[] spawnPositions)
    {

        this.bottomLeft = minimumPosition;
        this.topRight = maximumPosition;
        this.spawnPositions = spawnPositions;
    }

    public Position getMinimumPosition()
    {
        return bottomLeft;
    }

    public Position getMaximumPosition()
    {
        return topRight;
    }

    public Position getPlayerSpawnPosition()
    {
        return spawnPositions[0];
    }

    public Position getOpponentSpawnPosition()
    {
        return spawnPositions[1];
    }

    public Position[] getSpawnPositions()
    {
        return spawnPositions;
    }

}
