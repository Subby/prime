package com.asgarniars.rs2.content;

import com.asgarniars.rs2.model.players.Player;

/**
 * 8/6/2012 11:25 AM Handles everything related to skillcapes. (Emotes,
 * trimming, reqs <- soon)
 * 
 * @author AkZu
 */
public class Skillcapes
{

    public static void doSkillcape(Player player)
    {
        Skillcape.ATTACK.doEmote(player);
        Skillcape.STRENGTH.doEmote(player);
        Skillcape.DEFENCE.doEmote(player);
        Skillcape.RANGED.doEmote(player);
        Skillcape.PRAYER.doEmote(player);
        Skillcape.MAGIC.doEmote(player);
        Skillcape.RUNECRAFTING.doEmote(player);
        Skillcape.HITPOINTS.doEmote(player);
        Skillcape.AGILITY.doEmote(player);
        Skillcape.HERBLORE.doEmote(player);
        Skillcape.THIEVING.doEmote(player);
        Skillcape.CRAFTING.doEmote(player);
        Skillcape.FLETCHING.doEmote(player);
        Skillcape.SLAYER.doEmote(player);
        Skillcape.MINING.doEmote(player);
        Skillcape.SMITHING.doEmote(player);
        Skillcape.FISHING.doEmote(player);
        Skillcape.COOKING.doEmote(player);
        Skillcape.FIREMAKING.doEmote(player);
        Skillcape.WOODCUTTING.doEmote(player);
        Skillcape.FARMING.doEmote(player);
        Skillcape.QUESTPOINT.doEmote(player);
    }

    public static enum Skillcape
    {

        ATTACK(4959, 823, 9747), STRENGTH(4981, 828, 9750), DEFENCE(4961, 824, 9753), RANGED(4973, 832, 9756), PRAYER(4979, 829, 9759), MAGIC(4939, 813, 9762), RUNECRAFTING(4947, 817, 9765), HITPOINTS(4971, 833, 9768), AGILITY(4977, 830, 9771), HERBLORE(4969, 835, 9774), THIEVING(4965, 826, 9777), CRAFTING(4949, 818, 9780), FLETCHING(4937, 812, 9783), SLAYER(4967, 827, 9786), MINING(4941, 814, 9792), SMITHING(4943, 815, 9795), FISHING(4951, 819, 9798), COOKING(4955, 821, 9801), FIREMAKING(4975, 831, 9804), WOODCUTTING(4957, 822, 9807), FARMING(4963, 825, 9810), QUESTPOINT(4945, 816, 9813);

        private int animation;
        private int gfx;
        private int item;

        Skillcape(int anim, int gfx, int item)
        {
            this.animation = anim;
            this.gfx = gfx;
            this.item = item;
        }

        public final void doEmote(Player player)
        {
            if (player.getEquipment().getItemContainer().getById(this.getItem()) == null && player.getEquipment().getItemContainer().getById(this.getItem() + 1) == null)
                return;
            player.getUpdateFlags().sendAnimation(this.getAnim(), 0);
            if (player.getGender() == 1 && this.equals(HITPOINTS))
                player.getUpdateFlags().sendGraphic(this.getGfx() + 1, 0);
            else
                player.getUpdateFlags().sendGraphic(this.getGfx(), 0);
        }

        public final String getData()
        {
            return toString() + " (Anim: " + animation + ", GFX:" + gfx + ", ITEM: " + item + ")";
        }

        public final int getAnim()
        {
            return animation;
        }

        public final int getGfx()
        {
            return gfx;
        }

        public final int getItem()
        {
            return item;
        }
    }
}
