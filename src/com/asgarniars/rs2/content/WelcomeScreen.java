package com.asgarniars.rs2.content;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.model.players.Player;

/**
 * 8/6/2012 9:17PM Handles all things to do with welcome screen
 * 
 * @author AkZu
 * 
 *         TODO: Add support if player has a membership and recovery questions
 */
public class WelcomeScreen
{

    private static WelcomeScreen instance = null;

    public static WelcomeScreen getInstance()
    {
        if (instance == null)
        {
            instance = new WelcomeScreen();
        }
        return instance;
    }

    public void showWelcomeScreen(Player player)
    {
        player.getActionSender().sendString("You have @yel@0 unread messages\\nin your message centre.", 15261);
        /*
         * player.getActionSender().sendString( "You have " +
         * (player.getUnreadMessages() == 0 ? "@yel@" : "@gre@") +
         * player.getUnreadMessages() + " unread message" +
         * (player.getUnreadMessages() > 1 ? "s" : "") +
         * "@yel@\\nin your message centre.", 15261);
         */

        player.getActionSender().sendString("You last logged in @red@" + player.getLastLoginDay() + " @bla@from: @red@" + player.getLastIp(), 15258);

        player.getActionSender().sendString("You have not yet set any recovery questions.\\nIt is @or2@strongly @yel@recommended that you do so.\\n\\nIf you don't you will be @or2@unable to recover your\\npassword @yel@if you forget it, or it is stolen.", 15259);

        /*
         * player.getActionSender() .sendString( player.getRecoverys() ?
         * "\\n\\nRecovery Questions Last Set:\\n@gre@" +
         * player.getRecoveryDate() :
         * "\\n\\nYou haven't yet \\nset any recovery questions",
         */
        // 15259);

        player.getActionSender().sendString(player.getBankPin().hasBankPin() ? "\\nYou have a Bank PIN!" : "\\nYou do not have a Bank PIN. Please visit a bank\\nif you would like one", 15270);

        player.getActionSender().sendString("\\n\\nMembership is not implemented.", 15262);

        player.getActionSender().sendString("Welcome to " + Constants.SERVER_NAME, 15257);

        player.getActionSender().sendString(Constants.WELCOME_SCREEN_MESSAGES[0], Constants.WELCOME_SCREEN_TEXTS[Constants.WELCOME_SCREEN_ITF]);
        player.getActionSender().sendString(Constants.WELCOME_SCREEN_MESSAGES[1], Constants.WELCOME_SCREEN_TEXTS[Constants.WELCOME_SCREEN_ITF] + 1);

        player.getActionSender().sendString(Constants.SERVER_NAME + " -", 15260);

        player.getActionSender().sendWelcomeScreen(Constants.WELCOME_SCREEN_ITF);
    }
}
