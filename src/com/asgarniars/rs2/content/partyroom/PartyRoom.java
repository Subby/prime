package com.asgarniars.rs2.content.partyroom;

import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.players.Inventory;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.model.players.container.Container;
import com.asgarniars.rs2.model.players.container.ContainerType;

public class PartyRoom {
	
    private static PartyRoom instance;

    public static PartyRoom getInstance()
    {
        if (instance == null)
            instance = new PartyRoom();
        return instance;
    }
    
    /**
     * The items of the chest.
     */
    private Item[] items;
	
	/**
	 * Container for items in chest
	 */
	private Container currentlyStored;
	
	/**
	 * Puts a item into the chest
	 */
    public void sell(Player player, int slot, int amount)
    {
    	Item item = player.getInventory().get(slot);
        if (item == null)
        {
            player.getActionSender().sendMessage("Error storing that item - it is null.");
            return;
        }    	
        int transferAmount = player.getInventory().getItemContainer().getCount(item.getId());
        if (amount >= transferAmount)
        {
            amount = transferAmount;
        }
        else if (transferAmount == 0)
        {
            return;
        }
        if (!player.getInventory().getItemContainer().contains(item.getId()))
        {
            player.getActionSender().sendMessage("Error selling item - player's inventory doesn't contain item.");
            return;
        }
        Container temporaryInventory = new Container(ContainerType.STANDARD, Inventory.SIZE);
        for (Item invItem : player.getInventory().getItemContainer().toArray())
        {
            temporaryInventory.add(invItem);
        }
        temporaryInventory.remove(new Item(item.getId(), amount));

        player.getInventory().removeItem(new Item(item.getId(), amount));
        getCurrentStock().add(new Item(item.getId(), amount));
      
    }

    /**
     * Gets the list of items to create a container off of.
     * 
     * @return The item array.
     */
    public Item[] getItems()
    {
        return items;
    }	
	
    /**
     * Sets the current stored items of the chest.
     * 
     * @param stored The store to set to.
     */
    public void setCurrentStock(Container stored)
    {
        this.currentlyStored = stored;
    }

    /**
     * Gets the current item stock of this shop.
     * 
     * @return The current item stock.
     */
    public Container getCurrentStock()
    {
        return currentlyStored;
    }

}
