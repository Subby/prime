
package com.asgarniars.rs2.content.partyroom;

import com.asgarniars.rs2.content.partyroom.PartyRoomManager;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.model.players.container.Container;
import com.asgarniars.rs2.model.players.container.ContainerListener;

public class PartyRoomListener implements ContainerListener {
	
    /**
     * The shop listener attached to the player.
     */
    private Player player;
    
    public PartyRoomListener(Player player) {
    	this.player = player;
    }
    
	@Override
	public void itemChanged(Container container, int slot) {
		player.getActionSender().sendUpdateItem(slot, 2273, container.get(slot));
        //player.getActionSender().sendUpdateItems(2273, player.getInventory().getItemContainer().toArray());
		
	}

	@Override
	public void itemsChanged(Container container, int[] slots) {
        player.getActionSender().sendUpdateItems(2273, container.toArray());
        //player.getActionSender().sendUpdateItems(PartyRoomManager.PLAYER_CONTAINER_ID, player.getInventory().getItemContainer().toArray());
		
	}

	@Override
	public void itemsChanged(Container container) {
        player.getActionSender().sendUpdateItems(2273, container.toArray());
        //player.getActionSender().sendUpdateItems(PartyRoomManager.PLAYER_CONTAINER_ID, player.getInventory().getItemContainer().toArray());
		
	}

}
