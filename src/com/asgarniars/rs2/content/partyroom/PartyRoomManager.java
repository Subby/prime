package com.asgarniars.rs2.content.partyroom;

import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.model.players.container.Container;
import com.asgarniars.rs2.model.players.container.ContainerType;

public class PartyRoomManager {
	
	private static PartyRoomManager instance;
	
	public static PartyRoomManager getInstance() 
	{
		if (instance == null)
			instance = new PartyRoomManager();
		return instance;
	}
    /**
     * The maximum item capacity of the chest.
     */
    public static final int CHEST_CAPACITY = 10;
    
    /**
     * The interface id for the chest.
     */
    public static final int CHEST_CONTAINER_ID = 2156;
    
    /**
     * The sidebar interface id for the chest.
     */
    public static final int PLAYER_CONTAINER_ID = 5063;
    
    /**
     * The sidebar interface id for the chest.
     */
    public static final int CHEST_INTERFACE_OVERLAY_ID = 5064;    
    
    /**
     * Opens a shop with a given id.
     * TODO: Change interface IDs to immutable variables
     * @param id The id to get a shop by.
     */
    @SuppressWarnings("unused")
	public static void open(Player player)
    {
    	PartyRoomListener listener = new PartyRoomListener(player);
    	
    	if(listener == null) {
    		System.out.println("Party room was fucking null man :(");
    		return;
    	}
    	PartyRoom.getInstance().setCurrentStock(new Container(ContainerType.ALWAYS_STACK, CHEST_CAPACITY));
    	Item coins = new Item(995, 100);
    	PartyRoom.getInstance().getCurrentStock().add(coins);
    	PartyRoom.getInstance().getCurrentStock().addListener(listener);
        player.getActionSender().sendItfInvOverlay(2156, 5063);
        player.getActionSender().sendUpdateItems(2273, PartyRoom.getInstance().getCurrentStock().toArray());
        player.getActionSender().sendUpdateItems(5064, player.getInventory().getItemContainer().toArray());
        
    }
}
