package com.asgarniars.rs2.content;

import com.asgarniars.rs2.model.players.Player;

/**
 * 
 * @author Joshua Barry
 * 
 */
public interface Shearer<T>
{

    /**
     * Shears an actor to a naked state.
     * 
     * @param player The player shearing the Npc
     * @param actor The unknown sheered.
     */
    public void shear(Player player, T actor);

}
