package com.asgarniars.rs2.content;

import java.util.logging.Logger;

import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class Climbable
{

    private static final Logger logger = Logger.getAnonymousLogger();

    public enum Direction
    {
        DOWN(-1), UP(1);

        private int heightMod;

        /**
         * 
         * @param heightMod
         */
        private Direction(int heightMod)
        {
            this.heightMod = heightMod;
        }

        public int getHeightMod()
        {
            return heightMod;
        }
    }

    /**
     * 
     * @param player
     * @param position
     */
    public static void climb(final Player player, final Position position, final int size)
    {
        int objectId = player.getClickId();
        int anim = 0x33c;
        switch (objectId)
        {
        default:
            player.getUpdateFlags().sendAnimation(anim);
            World.submit(new Task(3)
            {

                @Override
                protected void execute()
                {
                    if (size > 0)
                        player.clipTeleport(position, size);
                    else
                        player.teleport(position);
                    this.stop();

                }

            });

            break;
        }

    }

    /**
     * 
     * @param player
     * @param direction
     */
    public static void climb(final Player player, Direction direction)
    {
        int objectId = player.getClickId();
        int x = player.getPosition().getX();
        int y = player.getPosition().getY();
        int z = player.getPosition().getZ();
        int anim = 0x33c;

        switch (objectId)
        {
        /* related to barrows */
        case 6702:
            x = 3565;
            y = 3289;
            z = 0;
            break;

        /* related to stronghold of security */
        case 16080:
            x = 1904;
            y = 5222;
            z = 0;
            break;
        case 16081:
            x = 2121;
            y = 5255;
            z = 0;
            break;
        case 16115:
            x = 2296;
            y = 5212;
            z = 0;
            break;
        case 16114:
            x = 2026;
            y = 5216;
            z = 0;
            break;
        case 16148:
            x = 3081;
            y = 3421;
            z = 0;
            break;
        case 16149:
            x = 2041;
            y = 5244;
            z = 0;
            break;
        default:
            switch (direction)
            {
            case DOWN:
                if (z == 0)
                    y += 6400;
                else
                    z -= 1;
                break;
            case UP:
                // determine if you're in a dungeon or not.
                if (y > 6400)
                    y -= 6400;
                else
                    z += 1;
                break;
            default:
                logger.severe("Unhandled Direction sent!");
                break;

            }
        }

        final int newX = x;
        final int newY = y;
        final int height = z;

        player.getUpdateFlags().sendAnimation(anim);

        World.submit(new Task(3)
        {

            @Override
            public void execute()
            {
                player.teleport(new Position(newX, newY, height));
                this.stop();

            }

        });
    }

}
