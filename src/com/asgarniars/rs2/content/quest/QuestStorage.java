package com.asgarniars.rs2.content.quest;

import java.util.HashMap;
import java.util.Map;

/**
 * Quest storage, a repository to hold the state's and access to quests.
 * 
 * @author Joshua Barry
 * 
 */
public class QuestStorage
{

    /**
     * The current quest states.
     */
    private final Map<Quest, Integer> states = new HashMap<Quest, Integer>();

    /**
     * Gets the quest state.
     * 
     * @param questId The quest id to check.
     * @return The quest state (0 at default).
     */
    public int getState(int questId)
    {
        return states.get(QuestRepository.get(questId));
    }

    /**
     * Gets the quest state by quest.
     * 
     * @param quest The quest to check.
     * @return The quest state (0 at default).
     */
    public int getState(Quest quest)
    {
        return states.get(quest);
    }

    /**
     * Checks if the player has finished a certain quest.
     * 
     * @param quest The quest id.
     * @return {@code True} if so, {@code false} if not.
     */
    public boolean hasFinished(Quest quest)
    {
        if (states.containsKey(quest))
        {
            return quest != null && quest.getFinalStage() <= states.get(quest);
        }
        return false;
    }

    /**
     * Checks if the player has started a certain quest.
     * 
     * @param quest The quest's id.
     * @return {@code true} if the player has started the quest, {@code false}
     *         if not.
     */
    public boolean hasStarted(Quest quest)
    {
        if (states.containsKey(quest))
        {
            return states.get(quest) > 0 && states.get(quest) < quest.getFinalStage();
        }
        return false;
    }

    /**
     * 
     * @param questId The quest to set the stage for.
     * @param state the state to set.
     */
    public void setState(Quest quest, int state)
    {
        states.put(quest, state);
    }
}
