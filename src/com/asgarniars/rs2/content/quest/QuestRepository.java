package com.asgarniars.rs2.content.quest;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.asgarniars.rs2.content.quest.impl.CooksAssistant;
import com.asgarniars.rs2.content.quest.impl.DoricsQuest;
import com.asgarniars.rs2.content.quest.impl.DruidicRitual;
import com.asgarniars.rs2.content.quest.impl.SheepShearer;
import com.asgarniars.rs2.content.quest.impl.TheRestlessGhost;
import com.asgarniars.rs2.content.quest.impl.WaterfallQuest;
import com.asgarniars.rs2.content.quest.impl.WitchsPotion;
import com.asgarniars.rs2.model.players.Player;

/**
 * 
 * @author Joshua Barry <Ares>
 * @author Rait
 * 
 */
public class QuestRepository
{

    /**
     * The amount of quests available.
     */
    public static final int AMOUNT_OF_QUESTS = 103;
    /**
     * The mapping holding all quest data.
     */
    public static final Map<QuestEntry, Quest> QUEST_DATABASE = new HashMap<QuestEntry, Quest>();

    /**
     * p.s. static initializers are ugly
     */
    static
    {
        QUEST_DATABASE.put(new QuestEntry("SheepShearer", 7344), new SheepShearer());
        QUEST_DATABASE.put(new QuestEntry("DoricsQuest", 7336), new DoricsQuest());
        QUEST_DATABASE.put(new QuestEntry("TheRestlessGhost", 7337), new TheRestlessGhost());
        QUEST_DATABASE.put(new QuestEntry("DruidicRitual", 7355), new DruidicRitual());
        QUEST_DATABASE.put(new QuestEntry("WitchsPotion", 7348), new WitchsPotion());
        QUEST_DATABASE.put(new QuestEntry("WaterfallQuest", 7350), new WaterfallQuest());
        QUEST_DATABASE.put(new QuestEntry("CooksAssistant", 1), new CooksAssistant());
    }

    /**
     * Grabs a quest from the mapping with the given button index.
     * 
     * @param button The quest button id.
     * @return The {@code Quest} which id matches the id given, or {@code null}
     *         if the quest didn't exist.
     */
    public static Quest get(int button)
    {
        Set<QuestEntry> entries = QUEST_DATABASE.keySet();
        for (Iterator<QuestEntry> it$ = entries.iterator(); it$.hasNext();)
        {
            QuestEntry entry = it$.next();
            if (entry.getValue() == button)
            {
                return QUEST_DATABASE.get(entry);
            }
        }
        return null;
    }

    /**
     * Grab a quest from the mapping with the given name.
     * 
     * @param name The quest's name.
     * @return The {@code Quest} whose name matches the one provided, or
     *         {@code null} if the quest with the given name couldn't be found.
     */
    public static Quest get(String name)
    {
        Set<QuestEntry> entries = QUEST_DATABASE.keySet();
        for (Iterator<QuestEntry> it$ = entries.iterator(); it$.hasNext();)
        {
            QuestEntry entry = it$.next();
            if (entry.getKey().equalsIgnoreCase(name))
            {
                return QUEST_DATABASE.get(entry);
            }
        }
        return null;
    }

    /**
     * Handles the quest side bar interfaces and name
     * 
     * @param player The player.
     */
    public static void handle(Player player)
    {
        for (Quest quest : QUEST_DATABASE.values())
        {
            if (player.getQuestStorage().hasStarted(quest))
            {
                player.getActionSender().changeInterfaceComponentColor(quest.getQuestButton(), 0x33FF66);
            }
            if (player.getQuestStorage().hasFinished(quest))
            {
                player.getActionSender().changeInterfaceComponentColor(quest.getQuestButton(), 0x3366);
            }

            player.getActionSender().sendString(quest.getQuestName(), quest.getQuestButton());
        }
    }
}
