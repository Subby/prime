package com.asgarniars.rs2.content.quest.impl;

import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.players.Player;

/**
 * 
 * @author Conner Kendall <Emnesty>
 * 
 */
public class WitchsPotion extends QuestStructure
{

    @Override
    public boolean hasRequirements(Player player)
    {
        return player.isLoggedIn();
    }

    @Override
    public boolean deathListener(Player player)
    {
        return false;
    }

    @Override
    public boolean defeatListener(Player player)
    {
        return false;
    }

    @Override
    public boolean dialougeListener(Player player)
    {
        if (player.getQuestStorage().hasFinished(this))
            end(player);
        return true;
    }

    @Override
    public Item[] getRewards()
    {
        return null;
    }

    @Override
    public String getAttributeName()
    {
        return "WitchsPotion";
    }

    @Override
    public String getQuestName()
    {
        return "Witch's Potion";
    }

    @Override
    public short getQuestPointAward()
    {
        return 1;
    }

    @Override
    public double[][] getExperienceAward()
    {
        return new double[][]
        {
        { Skill.MAGIC, 325 } };
    }

    @Override
    public String getAdditionalMessage()
    {
        return null;
    }

    @Override
    public short getFinalStage()
    {
        return 4;
    }

    @Override
    public short getQuestButton()
    {
        return 7348;
    }

}
