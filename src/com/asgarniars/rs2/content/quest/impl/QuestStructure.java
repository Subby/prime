package com.asgarniars.rs2.content.quest.impl;

import com.asgarniars.rs2.content.dialogue.Dialogue;
import com.asgarniars.rs2.content.dialogue.StatementDialogue;
import com.asgarniars.rs2.content.dialogue.StatementType;
import com.asgarniars.rs2.content.quest.Quest;
import com.asgarniars.rs2.content.quest.QuestRepository;
import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.model.players.Player;

/**
 * The foundation of a quest upon which all other quests should stand.
 * 
 * @author Joshua Barry
 * 
 */
public abstract class QuestStructure implements Quest
{

    @Override
    public void start(Player player)
    {
        if (!this.hasRequirements(player))
        {
            Dialogue dialogue = new StatementDialogue(StatementType.NORMAL, new String[]
            { "You do not meet the requirements for this quest." });
            player.open(dialogue);
            return;
        }
        player.getQuestStorage().setState(this, 1);
        QuestRepository.handle(player);
        player.setAttribute(getAttributeName(), (byte) -1);

    }

    @Override
    public void end(Player player)
    {
        player.setQuestPoints(player.getQuestPoints() + getQuestPointAward());
        giveRewards(player);
        QuestRepository.handle(player);
        player.getActionSender().sendMessage("Congratulations! Quest complete!");
        player.removeAttribute(getAttributeName());
        player.removeAttribute("currentQuest");
    }

    @Override
    public void giveRewards(Player player)
    {
        StringBuilder rewardNames = new StringBuilder();
        if (getRewards() != null)
            for (Item item : getRewards())
            {
                if (player.getInventory().hasRoomFor(item))
                {
                    player.getInventory().addItem(item);
                    rewardNames.append(ItemManager.getInstance().getItemName(item.getId()).concat("\\n"));
                }
            }
        StringBuilder experienceGains = new StringBuilder();
        for (int x = 0; x < this.getExperienceAward().length; x++)
        {
            short skill = (short) getExperienceAward()[x][0];
            double exp = getExperienceAward()[x][1];
            experienceGains.append((int) exp + " " + Skill.getSkillName(skill) + " XP.\\n");
            player.getSkill().addExperience(skill, exp);
        }
        StringBuilder additionalMessage = new StringBuilder();
        if (getAdditionalMessage() != null)
            additionalMessage.append(getAdditionalMessage().concat("\\n"));
        player.getActionSender().sendString(getQuestPointAward() + " Quest Points\\n".concat(experienceGains.toString().concat(additionalMessage.toString())).concat(rewardNames.toString()), 4444);
        player.getActionSender().sendString("You have completed the " + getQuestName() + " Quest!", 301);
        player.getActionSender().sendConfig(101, player.getQuestPoints());
        player.getActionSender().sendInterface(297);

    }
}
