package com.asgarniars.rs2.content.quest.impl;

import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.players.Player;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class SheepShearer extends QuestStructure
{

    @Override
    public boolean hasRequirements(Player player)
    {
        return player.isLoggedIn();
    }

    @Override
    public boolean deathListener(Player player)
    {
        player.getActionSender().sendMessage("NOOO YOU LOST MAHFUCKA.");
        return false;
    }

    @Override
    public boolean defeatListener(Player player)
    {
        player.getActionSender().sendMessage(this.getQuestName() + " killHook activated.");
        return true;
    }

    @Override
    public boolean dialougeListener(Player player)
    {

        if (player.getQuestStorage().hasFinished(this))
            end(player);

        return true;
    }

    @Override
    public Item[] getRewards()
    {
        return new Item[]
        { new Item(995, 60) };
    }

    @Override
    public String getAttributeName()
    {
        return "SheepShearer";
    }

    @Override
    public String getQuestName()
    {
        return "Sheep Shearer";
    }

    @Override
    public short getQuestPointAward()
    {
        return 1;
    }

    @Override
    public double[][] getExperienceAward()
    {
        return new double[][]
        {
        { Skill.CRAFTING, 150 } };
    }

    @Override
    public short getFinalStage()
    {
        return 3;
    }

    @Override
    public short getQuestButton()
    {
        return 7344;
    }

    @Override
    public String getAdditionalMessage()
    {
        // TODO Auto-generated method stub
        return null;
    }

}
