package com.asgarniars.rs2.content.quest.impl;

import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.players.Player;

/**
 * 
 * @author Conner Kendall <Emnesty>
 * @author Pulse
 * 
 */
public class DoricsQuest extends QuestStructure
{

    @Override
    public boolean hasRequirements(Player player)
    {
        return player.isLoggedIn();
    }

    @Override
    public boolean deathListener(Player player)
    {
        return false;
    }

    @Override
    public boolean defeatListener(Player player)
    {
        return false;
    }

    @Override
    public boolean dialougeListener(Player player)
    {
        if (player.getQuestStorage().hasFinished(this))
            end(player);
        return true;
    }

    @Override
    public Item[] getRewards()
    {
        return new Item[]
        { new Item(995, 180) };
    }

    @Override
    public String getAttributeName()
    {
        return "DoricsQuest";
    }

    @Override
    public String getQuestName()
    {
        return "Dorics Quest";
    }

    @Override
    public short getQuestPointAward()
    {
        return 1;
    }

    @Override
    public double[][] getExperienceAward()
    {
        return new double[][]
        {
        { Skill.MINING, 1300 } };
    }

    @Override
    public short getFinalStage()
    {
        return 2;
    }

    @Override
    public short getQuestButton()
    {
        return 7336;
    }

    @Override
    public String getAdditionalMessage()
    {
        // TODO Auto-generated method stub
        return null;
    }

}