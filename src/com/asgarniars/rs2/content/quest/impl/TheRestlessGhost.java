package com.asgarniars.rs2.content.quest.impl;

import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.players.Player;

/**
 * 
 * @author Conner Kendall <Emnesty>
 * 
 */
public class TheRestlessGhost extends QuestStructure
{

    @Override
    public boolean hasRequirements(Player player)
    {
        return player.isLoggedIn();
    }

    @Override
    public boolean deathListener(Player player)
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean defeatListener(Player player)
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean dialougeListener(Player player)
    {
        if (player.getQuestStorage().hasFinished(this))
            end(player);

        return true;
    }

    @Override
    public Item[] getRewards()
    {
        return null;
    }

    @Override
    public String getAttributeName()
    {
        return "TheRestlessGhost";
    }

    @Override
    public String getQuestName()
    {
        return "The Restless Ghost";
    }

    @Override
    public short getQuestPointAward()
    {
        return 1;
    }

    @Override
    public double[][] getExperienceAward()
    {
        return new double[][]
        {
        { Skill.PRAYER, 1125 } };
    }

    @Override
    public short getFinalStage()
    {
        return 99;
    }

    @Override
    public short getQuestButton()
    {
        return 7337;
    }

    @Override
    public String getAdditionalMessage()
    {
        // TODO Auto-generated method stub
        return null;
    }

}
