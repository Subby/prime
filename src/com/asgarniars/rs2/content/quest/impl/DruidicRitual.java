package com.asgarniars.rs2.content.quest.impl;

import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.players.Player;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class DruidicRitual extends QuestStructure
{

    @Override
    public boolean hasRequirements(Player player)
    {
        return player.isLoggedIn();
    }

    @Override
    public boolean deathListener(Player player)
    {
        return false;
    }

    @Override
    public boolean defeatListener(Player player)
    {
        return false;
    }

    @Override
    public boolean dialougeListener(Player player)
    {
        if (player.getQuestStorage().hasFinished(this))
            end(player);
        return true;
    }

    @Override
    public Item[] getRewards()
    {
        return null;
    }

    @Override
    public String getAttributeName()
    {
        return "DruidicRitual";
    }

    @Override
    public String getQuestName()
    {
        return "Druidic Ritual";
    }

    @Override
    public short getQuestPointAward()
    {
        return 4;
    }

    @Override
    public double[][] getExperienceAward()
    {
        return new double[][]
        {
        { Skill.HERBLORE, 250 } };
    }

    @Override
    public String getAdditionalMessage()
    {
        return "Access to the Herblore skill";
    }

    @Override
    public short getFinalStage()
    {
        return 4;
    }

    @Override
    public short getQuestButton()
    {
        return 7355;
    }

}
