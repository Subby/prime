package com.asgarniars.rs2.content.quest;

import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.players.Player;

/**
 * 
 * @author Joshua Barry
 * 
 */
public interface Quest
{

    /**
     * Determines if the player has the requirements to complete this quest.
     * 
     * @param player The player attempting the quest.
     * @return {@code true} if the player can complete the quest.
     */
    public abstract boolean hasRequirements(Player player);

    /**
     * Determine what happens when the questing player dies.
     * 
     * @param player The player who has died.
     * @return {@code true} if the player has died.
     */
    public abstract boolean deathListener(Player player);

    /**
     * Determines what happens when the questing player kills another entity.
     * 
     * @param player The player who has killed the victim entity.
     * @return
     */
    public abstract boolean defeatListener(Player player);

    /**
     * Determines whether a player has clicked a specific button or not, and
     * what the outcome will be.
     * 
     * @param player
     * @return
     */
    public abstract boolean dialougeListener(Player player);

    /**
     * Start the quest.
     */
    public abstract void start(Player player);

    /**
     * End the quest.
     */
    public abstract void end(Player player);

    /**
     * Give the player the reward items.
     */
    public abstract void giveRewards(Player player);

    /**
     * The list of rewards the player will receive upon quest completion.
     * 
     * @return
     */
    public abstract Item[] getRewards();

    /**
     * Gets the quest attribute name.
     * 
     * @return
     */
    public abstract String getAttributeName();

    /**
     * Gets the quest name.
     * 
     * @return
     */
    public abstract String getQuestName();

    /**
     * Gets the amount of quest points to award the player.
     * 
     * @return
     */
    public abstract short getQuestPointAward();

    /**
     * Gets the amount of experience gained.
     * 
     * @return
     */
    public abstract double[][] getExperienceAward();

    /**
     * Displayed on reward interface.
     * 
     * @return
     */
    public abstract String getAdditionalMessage();

    /**
     * Gets the value of the final quest.
     * 
     * @return
     */
    public abstract short getFinalStage();

    /**
     * Gets the quest button id.
     * 
     * @return
     */
    public abstract short getQuestButton();

}
