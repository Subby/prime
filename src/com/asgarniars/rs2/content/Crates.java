package com.asgarniars.rs2.content;

import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;
import com.asgarniars.rs2.util.Misc;

/**
 * Saturday January 19th 2013 Used to search crates. Crates have a "search"
 * option. When searched, crates used to give random items, such as damaged
 * armour, broken arrows, broken staffs, broken glass, leather boots, leather
 * gloves, and small amounts of coins.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public class Crates
{

    /**
     * Random items a player gets if not in treasure trials.
     */
    public static int[] crateRewards =
    { 687, 689, 995, 1059, 1061, 1469 };

    /**
     * TODO: implement treasure trials.
     * 
     * @param player
     */
    public static void search(final Player player)
    {
        if (player.getAttribute("canSearchCrate") == null)
        {
            player.setAttribute("canSearchCrate", (byte) -1);
            short reward = (short) Misc.generatedValue(crateRewards);
            Item item = null;
            if (reward == 995)
            {
                item = new Item(reward, Misc.randomMinMax(10, 55));
            }
            else
            {
                item = new Item(reward, 1);
            }
            String name = item.getDefinition().getName().toLowerCase();
            String prefix = Misc.getArticle(name);
            if (name.contains("leather"))
            {
                prefix = " a pair of ";
            }
            else if (name.contains("coin") || name.contains("glass") || item.getCount() > 1)
            {
                prefix = " some ";
            }
            player.getActionSender().sendMessage("You search the crate...");
            if (player.getInventory().addItem(item))
            {
                player.getActionSender().sendMessage("... and find " + prefix + name + ".");
            }
            World.submit(new Task(10)
            {
                @Override
                public void execute()
                {
                    player.removeAttribute("canSearchCrate");
                    this.stop();
                }
            });
        }
        else
        {
            player.getActionSender().sendMessage("You search the crate...");
            player.getActionSender().sendMessage("... and find nothing.");
        }
    }
}