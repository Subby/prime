package com.asgarniars.rs2.content.skills.woodcut;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public enum Axe
{

    BRONZE_AXE(1351, 1, 0, 879, 508, 492, 494, 0), IRON_AXE(1349, 1, 15, 877, 510, 492, 496, 0), STEEL_AXE(1353, 6, 30, 875, 512, 492, 498, 0), BLACK_AXE(1361, 6, 45, 873, 514, 492, 500, 10), MITHRIL_AXE(1355, 21, 60, 871, 516, 492, 502, 18), ADAMANT_AXE(1357, 31, 75, 869, 518, 492, 504, 43), RUNE_AXE(1359, 41, 90, 867, 520, 492, 506, 427), DRAGON_AXE(6739, 61, 90, 2846, 6743, 492, 6741, 1800);

    /**
     * The item id.
     */
    private int itemId;

    /**
     * The level req.
     */
    private int levelRequirement;

    /**
     * The item animationId.
     */
    private int animationId;

    /**
     * The item bonus.
     */
    private int bonus;

    /**
     * The axe head.
     */
    private int axeHead;

    /**
     * The axe handle.
     */
    private int axeHandle;

    /**
     * The broken axe.
     */
    private int brokenAxe;

    /**
     * The cost to repair.
     */
    private int repairCost;

    private Axe(int itemId, int levelReq, int bonus, int animationId, int axeHead, int axeHandle, int brokenAxe, int repairCost)
    {
        this.itemId = itemId;
        this.levelRequirement = levelReq;
        this.bonus = bonus;
        this.animationId = animationId;
        this.axeHead = axeHead;
        this.brokenAxe = brokenAxe;
        this.repairCost = repairCost;
    }

    /**
     * Gets the item id.
     * 
     * @return the item
     */
    public int getItemId()
    {
        return itemId;
    }

    /**
     * Gets the level req.
     * 
     * @return the level req
     */
    public int getLevelRequirement()
    {
        return levelRequirement;
    }

    /**
     * Gets the axe head.
     * 
     * @return the axe head
     */
    public int getAxeHead()
    {
        return axeHead;
    }

    /**
     * Gets the item animationId.
     * 
     * @return the animationId
     */
    public int getAnimationId()
    {
        return animationId;
    }

    /**
     * Gets the item bonus.
     * 
     * @return the item bonus
     */
    public int getBonus()
    {
        return bonus;
    }

    /**
     * Gets the broken axe.
     * 
     * @return the broken axe
     */
    public int getBroken()
    {
        return brokenAxe;
    }

    /**
     * Gets the axe handle.
     * 
     * @return the axe handle.
     */
    public int getAxeHandle()
    {
        return axeHandle;
    }

    /**
     * Gets the repair cost.
     * 
     * @return the repair cost.
     */
    public int getRepairCost()
    {
        return repairCost;
    }
}
