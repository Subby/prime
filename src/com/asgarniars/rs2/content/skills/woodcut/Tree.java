package com.asgarniars.rs2.content.skills.woodcut;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public enum Tree
{

    ACHEY_TREE(new int[]
    { 2023 }, 1, 25, 2862, 3371, 75, 100, 8), NORMAL_TREE(new int[]
    { 1276, 1277, 1278, 1279, 1280, 1282, 1283, 1284, 1285, 1286, 1289, 1290, 1291, 1315, 1316, 1318, 1319, 1330, 1331, 1332, 1333, 1365, 1383, 1384, 2409, 3033, 3034, 3035, 3036, 3881, 3882, 3883, 5902, 5903, 5904 }, 1, 25, 1511, 1342, 75, 100, 1), OAK_TREE(new int[]
    { 1281, 2037 }, 15, 37.5, 1521, 1356, 14, 25, 4), WILLOW_TREE(new int[]
    { 1308, 5551, 5552, 5553 }, 30, 67.5, 1519, 7399, 14, 15, 8), TEAK_TREE(new int[]
    { 9036 }, 35, 85, 6333, 9037, 14, 20, 8), MAPLE_TREE(new int[]
    { 1307, 4677 }, 45, 100, 1517, 1343, 59, 15, 10), HOLLOW_TREE(new int[]
    { 2289, 4060 }, 45, 83, 3239, 2310, 59, 15, 1), MAHOGANY_TREE(new int[]
    { 9034 }, 50, 125, 6332, 9035, 80, 10, 8), YEW_TREE(new int[]
    { 1309 }, 60, 175, 1515, 7402, 100, 5, 8), MAGIC_TREE(new int[]
    { 1306 }, 75, 250, 1513, 7401, 200, 5, 13), DRAMEN_TREE(new int[]
    { 1292 }, 36, 0, 771, 1513, 59, 100, 1), VINES(new int[]
    { 5103, 5104, 5105, 5106, 5107 }, 34, 0, -1, 1513, 2, 100, 15);

    /**
     * The tree id.
     */
    private int[] treeId;

    /**
     * The level req.
     */
    private int levelRequired;

    /**
     * The exp gain.
     */
    private double experienceGain;

    /**
     * The log id.
     */
    private int logId;

    /**
     * The stump id.
     */
    private int stumpId;

    /**
     * The time used to respawn a tree.
     */
    private int respawnTime;

    /**
     * The chance of a tree decaying.
     */
    private int decayChance;

    /**
     * The log count
     */
    private int logCount;

    /**
     * A map of object ids to trees.
     */
    private static Map<Integer, Tree> trees = new HashMap<Integer, Tree>();

    public static Tree forId(int object)
    {
        return trees.get(object);
    }

    static
    {
        for (Tree tree : Tree.values())
        {
            for (int object : tree.getTreeId())
            {
                trees.put(object, tree);
            }
        }
    }

    /**
     * 
     * @param treeId The tree.
     * @param levelReq The level req.
     * @param expGain The exp gained.
     * @param logId The log Isd.
     * @param stumpId The stump Id.
     * @param respawnTime The respawn time.
     * @param decayChance The decay chance.
     */
    private Tree(int[] treeId, int levelReq, double expGain, int logId, int stumpId, int respawnTime, int decayChance, int logCount)
    {
        this.treeId = treeId;
        this.levelRequired = levelReq;
        this.experienceGain = expGain;
        this.logId = logId;
        this.stumpId = stumpId;
        this.respawnTime = respawnTime;
        this.decayChance = decayChance;
        this.logCount = logCount;
    }

    /**
     * Gets the tree id.
     * 
     * @retun the tree id
     */
    public int[] getTreeId()
    {
        return treeId;
    }

    /**
     * Gets the level requirement.
     * 
     * @return the level
     */
    public int getLevelRequired()
    {
        return levelRequired;
    }

    /**
     * Gets the experience gained.
     * 
     * @return the experience gain.
     */
    public double getExperienceGain()
    {
        return experienceGain;
    }

    /**
     * Gets the log id.
     * 
     * @return the log
     */
    public int getLogId()
    {
        return logId;
    }

    /**
     * Gets the stump id.
     * 
     * @return the stunp
     */
    public int getStumpId()
    {
        return stumpId;
    }

    /**
     * Gets the respawn time.
     * 
     * @return the respawn time
     */
    public int getRespawnTime()
    {
        return respawnTime;
    }

    /**
     * Gets the chance of decaying.
     * 
     * @return the decay chance
     */
    public int getDecayChance()
    {
        return decayChance;
    }

    public int getLogCount()
    {
        return logCount;
    }

}