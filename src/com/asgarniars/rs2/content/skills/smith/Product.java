package com.asgarniars.rs2.content.skills.smith;

import com.asgarniars.rs2.model.items.Item;

/**
 * Resembles the product of an item that has been created through smithery.
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public enum Product
{

    /*
     * Bronze Forging
     */
    BRONZE_DAGGER(new Item(1205), 1, 12.5, new Item(2349, 1)), BRONZE_SWORD(new Item(1277), 4, 12.5, new Item(2349, 1)), BRONZE_SCIMITAR(new Item(1321), 5, 25, new Item(2349, 2)), BRONZE_LONGSWORD(new Item(1291), 6, 25, new Item(2349, 2)), BRONZE_2HAND_SWORD(new Item(1307), 14, 37.5, new Item(2349, 3)), BRONZE_AXE(new Item(1351), 1, 12.5, new Item(2349, 1)), BRONZE_MACE(new Item(1422), 2, 12.5, new Item(2349, 1)), BRONZE_WARHAMMER(new Item(1337), 9, 37.5, new Item(2349, 3)), BRONZE_BATTLEAXE(new Item(1375), 10, 37.5, new Item(2349, 3)), BRONZE_CLAWS(new Item(3095), 13, 25, new Item(2349, 2)), BRONZE_CHAINBODY(new Item(1103), 11, 37.5, new Item(2349, 3)), BRONZE_PLATELEGS(new Item(1075), 16, 37.5, new Item(2349, 3)), BRONZE_PLATESKIRT(new Item(1087), 16, 37.5, new Item(2349, 3)), BRONZE_PLATEBODY(new Item(1117), 18, 62.5, new Item(2349, 5)), BRONZE_MED_HELM(new Item(1139), 1, 12.5, new Item(2349, 1)), BRONZE_FULL_HELM(new Item(1155), 7, 25, new Item(2349, 2)), BRONZE_SQ_SHIELD(new Item(1173), 8, 25, new Item(2349, 2)), BRONZE_KITE_SHIELD(new Item(1189), 12, 37.5, new Item(2349, 3)), BRONZE_NAILS(new Item(4819, 15), 4, 12.5, new Item(2349, 1)), BRONZE_DART_TIPS(new Item(819, 10), 4, 12.5, new Item(2349, 1)), BRONZE_ARROW_TIPS(new Item(39, 15), 5, 12.5, new Item(2349, 1)), BRONZE_THROWING_KNIVES(new Item(864, 5), 7, 12.5, new Item(2349, 1)),

    /*
     * Iron Forging
     */
    IRON_DAGGER(new Item(1203), 15, 25, new Item(2351, 1)), IRON_SWORD(new Item(1279), 19, 25, new Item(2351, 1)), IRON_SCIMITAR(new Item(1323), 20, 50, new Item(2351, 2)), IRON_LONGSWORD(new Item(1293), 21, 50, new Item(2351, 2)), IRON_2HAND_SWORD(new Item(1309), 29, 75, new Item(2351, 3)), IRON_AXE(new Item(1349), 16, 25, new Item(2349, 1)), IRON_MACE(new Item(1420), 17, 25, new Item(2349, 1)), IRON_WARHAMMER(new Item(1335), 24, 75, new Item(2349, 3)), IRON_BATTLEAXE(new Item(1363), 25, 75, new Item(2349, 3)), IRON_CLAWS(new Item(3096), 28, 50, new Item(2349, 2)), IRON_CHAINBODY(new Item(1101), 26, 75, new Item(2349, 3)), IRON_PLATELEGS(new Item(1067), 31, 75, new Item(2349, 3)), IRON_PLATESKIRT(new Item(1081), 31, 75, new Item(2349, 3)), IRON_PLATEBODY(new Item(1115), 33, 125, new Item(2349, 5)), IRON_MED_HELM(new Item(1137), 18, 25, new Item(2349, 1)), IRON_FULL_HELM(new Item(1153), 22, 50, new Item(2349, 2)), IRON_SQ_SHIELD(new Item(1175), 23, 50, new Item(2349, 2)), IRON_KITE_SHIELD(new Item(1191), 27, 75, new Item(2349, 3)), IRON_NAILS(new Item(4820, 15), 19, 25, new Item(2349, 1)), IRON_DART_TIPS(new Item(820, 10), 19, 25, new Item(2349, 1)), IRON_ARROW_TIPS(new Item(40, 15), 20, 25, new Item(2349, 1)), IRON_THROWING_KNIVES(new Item(863, 5), 22, 25, new Item(2349, 1)),

    /*
     * Steel Forging
     */
    STEEL_DAGGER(new Item(1207), 30, 37.5, new Item(2353, 1)), STEEL_SWORD(new Item(1281), 34, 37.5, new Item(2353, 1)), STEEL_SCIMITAR(new Item(1325), 35, 75, new Item(2353, 2)), STEEL_LONGSWORD(new Item(1295), 36, 75, new Item(2535, 2)), STEEL_2HAND_SWORD(new Item(1311), 44, 112.5, new Item(2353, 3)), STEEL_AXE(new Item(1351), 31, 37.5, new Item(2353, 1)), STEEL_MACE(new Item(1424), 32, 37.5, new Item(2353, 1)), STEEL_WARHAMMER(new Item(1339), 39, 112.5, new Item(2353, 3)), STEEL_BATTLEAXE(new Item(1365), 40, 112.5, new Item(2353, 3)), STEEL_CLAWS(new Item(3097), 43, 75, new Item(2353, 2)), STEEL_CHAINBODY(new Item(1105), 41, 112.5, new Item(2353, 3)), STEEL_PLATELEGS(new Item(1069), 46, 112.5, new Item(2353, 3)), STEEL_PLATESKIRT(new Item(1083), 46, 112.5, new Item(2353, 3)), STEEL_PLATEBODY(new Item(1119), 48, 187.5, new Item(2353, 5)), STEEL_MED_HELM(new Item(1141), 33, 37.5, new Item(2353, 1)), STEEL_FULL_HELM(new Item(1157), 37, 75, new Item(2353, 2)), STEEL_SQ_SHIELD(new Item(1177), 38, 75, new Item(2353, 2)), STEEL_KITE_SHIELD(new Item(1193), 42, 112.5, new Item(2353, 3)), STEEL_NAILS(new Item(1539, 15), 34, 37.5, new Item(2353, 1)), STEEL_DART_TIPS(new Item(821, 10), 34, 37.5, new Item(2353, 1)), STEEL_ARROW_TIPS(new Item(41, 15), 35, 37.5, new Item(2353, 1)), STEEL_THROWING_KNIVES(new Item(865, 5), 37, 37.5, new Item(2353, 1)),

    /*
     * Mithril Forging
     */
    MITHRIL_DAGGER(new Item(1209), 50, 50, new Item(2359, 1)), MITHRIL_SWORD(new Item(1285), 54, 50, new Item(2359, 1)), MITHRIL_SCIMITAR(new Item(1329), 55, 100, new Item(2359, 2)), MITHRIL_LONGSWORD(new Item(1299), 56, 100, new Item(2359, 2)), MITHRIL_2HAND_SWORD(new Item(1315), 64, 150, new Item(2359, 3)), MITHRIL_AXE(new Item(1355), 51, 50, new Item(2359, 1)), MITHRIL_MACE(new Item(1428), 52, 50, new Item(2359, 1)), MITHRIL_WARHAMMER(new Item(1343), 59, 150, new Item(2359, 3)), MITHRIL_BATTLEAXE(new Item(1369), 60, 150, new Item(2359, 3)), MITHRIL_CLAWS(new Item(3099), 63, 100, new Item(2359, 2)), MITHRIL_CHAINBODY(new Item(1109), 61, 100, new Item(2359, 3)), MITHRIL_PLATELEGS(new Item(1071), 66, 150, new Item(2359, 3)), MITHRIL_PLATESKIRT(new Item(1085), 66, 150, new Item(2359, 3)), MITHRIL_PLATEBODY(new Item(1121), 68, 150, new Item(2359, 5)), MITHRIL_MED_HELM(new Item(1143), 53, 50, new Item(2359, 1)), MITHRIL_FULL_HELM(new Item(1159), 57, 100, new Item(2359, 2)), MITHRIL_SQ_SHIELD(new Item(1181), 58, 100, new Item(2359, 1)), MITHRIL_KITE_SHIELD(new Item(1197), 62, 150, new Item(2359, 3)), MITHRIL_NAILS(new Item(4822, 15), 54, 50, new Item(2359, 1)), MITHRIL_DART_TIPS(new Item(822, 10), 54, 50, new Item(2359, 1)), MITHRIL_ARROW_TIPS(new Item(42, 15), 55, 50, new Item(2359, 1)), MITHRIL_THROWING_KNIVES(new Item(866, 5), 57, 50, new Item(2359, 1)),

    /*
     * Adamant Forging
     */
    ADAMANT_DAGGER(new Item(1211), 70, 62.5, new Item(2361, 1)), ADAMANT_SWORD(new Item(1287), 74, 62.5, new Item(2361, 1)), ADAMANT_SCIMITAR(new Item(1331), 75, 125, new Item(2361, 2)), ADAMANT_LONGSWORD(new Item(1301), 76, 125, new Item(2361, 2)), ADAMANT_2HAND_SWORD(new Item(1317), 84, 187.5, new Item(2361, 3)), ADAMANT_AXE(new Item(1357), 71, 62.5, new Item(2361, 1)), ADAMANT_MACE(new Item(1430), 72, 62.5, new Item(2361, 1)), ADAMANT_WARHAMMER(new Item(1345), 79, 187.5, new Item(2361, 3)), ADAMANT_BATTLEAXE(new Item(1371), 80, 187.5, new Item(2361, 3)), ADAMANT_CLAWS(new Item(3100), 83, 125, new Item(2361, 2)), ADAMANT_CHAINBODY(new Item(1111), 81, 187.5, new Item(2361, 3)), ADAMANT_PLATELEGS(new Item(1073), 86, 187.5, new Item(2361, 3)), ADAMANT_PLATESKIRT(new Item(1091), 86, 187.5, new Item(2361, 3)), ADAMANT_PLATEBODY(new Item(1123), 88, 312.5, new Item(2361, 5)), ADAMANT_MED_HELM(new Item(1145), 73, 62.5, new Item(2361, 1)), ADAMANT_FULL_HELM(new Item(1161), 77, 125, new Item(2361, 2)), ADAMANT_SQ_SHIELD(new Item(1183), 78, 125, new Item(2361, 2)), ADAMANT_KITE_SHIELD(new Item(1199), 82, 187.5, new Item(2361, 3)), ADAMANT_NAILS(new Item(4823, 15), 74, 62.5, new Item(2361, 1)), ADAMANT_DART_TIPS(new Item(823, 10), 74, 62.5, new Item(2361, 1)), ADAMANT_ARROW_TIPS(new Item(43, 15), 75, 62.5, new Item(2361, 1)), ADAMANT_THROWING_KNIVES(new Item(867, 5), 77, 62.5, new Item(2361, 1)),

    /*
     * Rune Forging
     */
    RUNE_DAGGER(new Item(1213), 85, 75, new Item(2363, 1)), RUNE_SWORD(new Item(1289), 89, 75, new Item(2363, 1)), RUNE_SCIMITAR(new Item(1333), 90, 150, new Item(2363, 2)), RUNE_LONGSWORD(new Item(1303), 91, 150, new Item(2363, 2)), RUNE_2HAND_SWORD(new Item(1319), 99, 225, new Item(2363, 3)), RUNE_AXE(new Item(1359), 86, 75, new Item(2363, 1)), RUNE_MACE(new Item(1432), 87, 75, new Item(2363, 1)), RUNE_WARHAMMER(new Item(1347), 94, 225, new Item(2363, 3)), RUNE_BATTLEAXE(new Item(1373), 95, 225, new Item(2363, 3)), RUNE_CLAWS(new Item(3101), 98, 150, new Item(2363, 2)), RUNE_CHAINBODY(new Item(1113), 96, 225, new Item(2363, 3)), RUNE_PLATELEGS(new Item(1079), 99, 225, new Item(2363, 3)), RUNE_PLATESKIRT(new Item(1093), 99, 225, new Item(2363, 3)), RUNE_PLATEBODY(new Item(1127), 99, 375, new Item(2363, 5)), RUNE_MED_HELM(new Item(1147), 88, 75, new Item(2363, 1)), RUNE_FULL_HELM(new Item(1163), 92, 150, new Item(2363, 2)), RUNE_SQ_SHIELD(new Item(1185), 93, 150, new Item(2363, 2)), RUNE_KITE_SHIELD(new Item(1201), 97, 225, new Item(2363, 3)), RUNE_DART_TIPS(new Item(824, 10), 89, 75, new Item(2363, 1)), RUNE_ARROW_TIPS(new Item(44, 15), 90, 75, new Item(2363, 1)), RUNE_THROWING_KNIVES(new Item(868, 5), 92, 75, new Item(2363, 1)),

    /*
     * Forging Blurite (The Knight's Sword must be completed)
     */
    BLURITE_BOLTS(new Item(9139, 10), 8, 17.5, new Item(9467, 1)), BLURITE_LIMB(new Item(9422, 1), 13, 17.5, new Item(9467, 1));

    private Item product;

    private int levelRequirement;

    private double experience;

    private Item[] materials;

    /**
     * 
     * @param product The ID of the product item.
     * @param levelRequirement The level requirement.
     * @param experience The amount of experience gained.
     * @param materials The item ID's of the materials consumed in production.
     */
    private Product(Item product, int levelRequirement, double experience, Item... materials)
    {
        this.product = product;
        this.levelRequirement = levelRequirement;
        this.experience = experience;
        this.materials = materials;

    }

    /**
     * @return the productId
     */
    public Item getProduct()
    {
        return product;
    }

    /**
     * @return the levelRequirement
     */
    public int getLevelRequirement()
    {
        return levelRequirement;
    }

    /**
     * @return the experience
     */
    public double getExperience()
    {
        return experience;
    }

    /**
     * @return the materials
     */
    public Item[] getMaterials()
    {
        return materials;
    }

    /**
     * Retrieves a product by it's product id.
     * 
     * @param id The id of the product item.
     * @return
     */
    static Product forId(int id)
    {
        for (Product product : Product.values())
            if (id == product.getProduct().getId())
                return product;
        return null;

    }

}
