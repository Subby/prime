package com.asgarniars.rs2.content.skills.smith;

import com.asgarniars.rs2.action.impl.ProductionAction;
import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.util.Misc;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class Forging extends ProductionAction
{

    Player player;

    Product product;

    public Forging(Player player, Product product)
    {
        super(player);
        this.player = player;
        this.product = product;
    }

    @Override
    public boolean canProduce()
    {
        if (player.getInventory().getItemContainer().freeSlots() < 1)
            return false;
        return player.getInventory().getItemContainer().contains(2347);
    }

    @Override
    public int getCycleCount()
    {
        return 5;
    }

    @Override
    public short getGraphic()
    {
        return 0;
    }

    @Override
    public short getAnimation()
    {
        return 898;
    }

    @Override
    public double getExperience()
    {
        return product.getExperience();
    }

    @Override
    public Item[] getConsumedItems()
    {
        return product.getMaterials();
    }

    @Override
    public String getInsufficentLevelMessage()
    {
        return "You must have a " + Skill.getSkillName(Skill.SMITHING) + " level of " + product.getLevelRequirement() + " to smith this item.";
    }

    @Override
    public int getProductionCount()
    {
        return product.getProduct().getCount();
    }

    @Override
    public int getRequiredLevel()
    {
        return product.getLevelRequirement();
    }

    @Override
    public Item[] getRewards()
    {
        return new Item[]
        { product.getProduct() };
    }

    @Override
    public int getSkill()
    {
        return Skill.SMITHING;
    }

    @Override
    public String getSuccessfulProductionMessage()
    {
        String name = ItemManager.getInstance().getItemName(product.getProduct().getId()).toLowerCase();
        String prefix = product.getProduct().getCount() > 1 ? Integer.toString(product.getProduct().getCount()) : Misc.getArticle(name);
        return "You hammer the " + name.split(" ")[0] + " and make " + prefix + " " + name + ".";
    }

}
