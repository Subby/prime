package com.asgarniars.rs2.content.skills.smith;

import com.asgarniars.rs2.model.items.Item;

/**
 * 
 * @author Joshua Barry
 * 
 */
public enum Bar
{
    BRONZE(2349, 12.5, new Item[]
    { new Item(438), new Item(436) }, 1), IRON(2351, 15, new Item[]
    { new Item(440) }, 15), STEEL(2353, 20, new Item[]
    { new Item(440), new Item(453, 2) }, 30), MITHRIL(2359, 25, new Item[]
    { new Item(447), new Item(453, 4) }, 50), ADAMANT(2361, 30, new Item[]
    { new Item(449), new Item(453, 6) }, 70), RUNITE(2363, 35, new Item[]
    { new Item(451), new Item(453, 8) }, 85);
    /**
     * The item id.
     */
    private int barId;
    /**
     * The experience granted per bar consumed.
     */
    private double experience;
    /**
     * The array of items this bar can make.
     */
    private Item[] ores;
    /**
     * The level required to make the item.
     */
    private int levelRequired;

    /**
     * 
     * @param ores
     * @return
     */
    public static Bar forBarId(int id)
    {
        for (Bar bar : Bar.values())
            if (id == bar.getId())
                return bar;
        return null;
    }

    /**
     * 
     * @param barId The ID of the bar we are producing.
     * @param experience The experience gained from production.
     * @param ores The ores smelted in production.
     * @param levelRequired The level required to smelt a bar.
     */
    private Bar(int barId, double experience, Item[] ores, int levelRequired)
    {
        this.barId = barId;
        this.ores = ores;
        this.experience = experience;
        this.levelRequired = levelRequired;
    }

    /**
     * @return the experience
     */
    public double getExperience()
    {
        return experience;
    }

    /**
     * @return the id
     */
    public int getId()
    {
        return barId;
    }

    /**
     * @return the levelRequired
     */
    public int getLevelRequired()
    {
        return levelRequired;
    }

    /**
     * @return the items
     */
    public Item getOre(int i)
    {
        return ores[i];
    }

    /**
     * @return the items
     */
    public Item[] getOres()
    {
        return ores;
    }
}
