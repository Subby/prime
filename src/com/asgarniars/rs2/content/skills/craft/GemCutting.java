package com.asgarniars.rs2.content.skills.craft;

import com.asgarniars.rs2.action.impl.ProductionAction;
import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.items.ItemManager;

/**
 * Gem crafting is an action in which a player cuts an uncut gem into a precious
 * gem which can further be crafted in jewelry and from there, Enchanted.
 * 
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public class GemCutting extends ProductionAction
{

    /**
     * How many gems we will be crafting.
     */
    private short productionCount;

    /**
     * The gem we will be crafting.
     */
    private Gem gem;

    /**
     * Initialize our data.
     * 
     * @param entity The entity who is cutting gems.
     * @param productionCount
     * @param gem
     */
    public GemCutting(Entity entity, short productionCount, Gem gem)
    {
        super(entity);
        this.productionCount = productionCount;
        this.gem = gem;
    }

    @Override
    public boolean canProduce()
    {
        if (getEntity().isPlayer())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public int getCycleCount()
    {
        return 4;
    }

    @Override
    public short getGraphic()
    {
        return -1;
    }

    @Override
    public short getAnimation()
    {
        return gem.getAnimation();
    }

    @Override
    public double getExperience()
    {
        return gem.getExperience();
    }

    @Override
    public Item[] getConsumedItems()
    {
        return new Item[]
        { new Item(gem.getUncutGem(), 1) };
    }

    @Override
    public String getInsufficentLevelMessage()
    {
        return "You need a " + Skill.SKILL_NAME[getSkill()] + " level of " + getRequiredLevel() + " to cut that gem.";
    }

    @Override
    public int getProductionCount()
    {
        return productionCount;
    }

    @Override
    public int getRequiredLevel()
    {
        return gem.getRequiredLevel();
    }

    @Override
    public Item[] getRewards()
    {
        return new Item[]
        { new Item(gem.getCutGem(), 1) };
    }

    @Override
    public int getSkill()
    {
        return Skill.CRAFTING;
    }

    @Override
    public String getSuccessfulProductionMessage()
    {
        return "You cut the " + ItemManager.getInstance().getItemName(getRewards()[0].getId()).toLowerCase() + ".";
    }

}
