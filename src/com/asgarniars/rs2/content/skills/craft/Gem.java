package com.asgarniars.rs2.content.skills.craft;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains all data relevant to gem's that can be cut.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public enum Gem
{

    OPAL(1625, 1609, 891, 1, 15, true), JADE(1627, 1611, 891, 13, 20, true), REDTOPAZ(1629, 1613, 892, 16, 25, true), SAPPHIRE(1623, 1607, 888, 1, 50, false), EMERALD(1621, 1605, 889, 27, 68, false), RUBY(1619, 1603, 887, 34, 85, false), DIAMOND(1617, 1601, 890, 43, 108, false), DRAGONSTONE(1631, 1615, 890, 55, 138, false), ONYX(6571, 6573, 2717, 67, 168, false);

    private int uncutGem, cutGem, animation, requiredLevel, experience;
    private boolean isSemiPrecious;

    private Gem(int uncutID, int cutID, int animation, int levelReq, int XP, boolean semiPrecious)
    {
        this.uncutGem = uncutID;
        this.cutGem = cutID;
        this.animation = animation;
        this.requiredLevel = levelReq;
        this.experience = XP;
        this.isSemiPrecious = semiPrecious;
    }

    public short getAnimation()
    {
        return (short) animation;
    }

    public int getCutGem()
    {
        return cutGem;
    }

    public int getRequiredLevel()
    {
        return requiredLevel;
    }

    public int getUncutGem()
    {
        return uncutGem;
    }

    public int getExperience()
    {
        return experience;
    }

    public boolean isSemiPrecious()
    {
        return isSemiPrecious;
    }

    private static Map<Integer, Gem> gems = new HashMap<Integer, Gem>();

    static
    {
        for (Gem gem : Gem.values())
        {
            gems.put(gem.uncutGem, gem);
        }
    }

    public static Gem forId(int gemId)
    {
        return gems.get(gemId);
    }

}
