package com.asgarniars.rs2.content.skills.craft;

import java.util.HashMap;
import java.util.Map;

import com.asgarniars.rs2.model.items.Item;

/**
 * Contains all relevant data for spinnable items. Special thanks to Pulse for
 * adding the data.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public enum Spinnable
{

    BOWSTRING(new Item(1779), new Item(1777), 15, 10), WOOL(new Item(1737), new Item(1759), 2.5, 1), ROPE(new Item(10814), new Item(954), 25, 30), MAGIC_STRING(new Item(6051), new Item(6038), 30, 19), YEW_STRING(new Item(6049), new Item(9438), 15, 10), SINEW_STRING(new Item(9436), new Item(9438), 15, 10);

    private Item item, outcome;
    private double experience;
    private int requiredLevel;

    private Spinnable(Item item, Item outcome, double experience, int requiredLevel)
    {
        this.item = item;
        this.outcome = outcome;
        this.experience = experience;
        this.requiredLevel = requiredLevel;
    }

    public double getExperience()
    {
        return experience;
    }

    public Item getItem()
    {
        return item;
    }

    public Item getOutcome()
    {
        return outcome;
    }

    public int getRequiredLevel()
    {
        return requiredLevel;
    }

    public static Map<Short, Spinnable> spins = new HashMap<Short, Spinnable>();

    static
    {
        for (Spinnable spinnable : Spinnable.values())
        {
            spins.put((short) spinnable.getItem().getId(), spinnable);
        }
    }

    public static Spinnable forId(short id)
    {
        return spins.get(id);
    }
}
