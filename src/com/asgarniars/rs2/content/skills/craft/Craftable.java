package com.asgarniars.rs2.content.skills.craft;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains all data relevant to craftable goods.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public enum Craftable
{

    /*
     * SNAKESKIN_BODY(6289, new int[] {0, 0}, 6322, 53), SNAKESKIN_CHAPS(6289,
     * new int[] {0, 0}, 6324, 51), SNAKESKIN_VAMB(6289, new int[] {0, 0}, 6330,
     * 47), SNAKESKIN_BANDANA(6289, new int[] {0, 0}, 6326, 48),
     * SNAKESKIN_BOOTS(6289, new int[] {0, 0}, 6328, 45),
     */
    LEATHERGLOVES(1741, 1059, 1, 13.8, 1), LEATHERBOOTS(1741, 1061, 7, 16.25, 1), LEATHERCOWL(1741, 1167, 9, 18.5, 1), LEATHERVAMBS(1741, 1063, 11, 22, 1), LEATHERBODY(1741, 1129, 14, 25, 1), LEATHERCHAPS(1741, 1095, 18, 27, 1), COIF(1741, 1169, 38, 37, 1), GREENVAMBS(1745, 1065, 57, 62, 1), GREENCHAPS(1745, 1099, 60, 124, 2), GREENBODY(1745, 1135, 63, 186, 3), BLUEVAMBS(2505, 2487, 66, 70, 1), BLUECHAPS(2505, 2493, 68, 140, 2), BLUEBODY(2505, 2499, 71, 210, 3), REDVAMBS(2507, 2489, 73, 78, 1), REDCHAPS(2507, 2495, 75, 156, 2), REDBODY(2507, 2501, 77, 234, 3), BLACKVAMBS(2509, 2491, 79, 86, 1), BLACKCHAPS(2509, 2497, 82, 172, 2), BLACKBODY(2509, 2503, 84, 258, 3);

    private int itemId, outcome, requiredLevel, requiredAmount;
    private double experience;

    /**
     * 
     * @param itemId
     * @param outcome
     * @param requiredLevel
     * @param experience
     * @param requiredAmount
     */
    private Craftable(int itemId, int outcome, int requiredLevel, double experience, int requiredAmount)
    {
        this.itemId = itemId;
        this.outcome = outcome;
        this.requiredLevel = requiredLevel;
        this.experience = experience;
        this.requiredAmount = requiredAmount;
    }

    public int getItemId()
    {
        return itemId;
    }

    public int getOutcome()
    {
        return outcome;
    }

    public int getRequiredAmount()
    {
        return requiredAmount;
    }

    public int getRequiredLevel()
    {
        return requiredLevel;
    }

    public double getExperience()
    {
        return experience;
    }

    private static Map<Integer, Craftable> craftables = new HashMap<Integer, Craftable>();

    static
    {
        for (Craftable craftable : Craftable.values())
        {
            craftables.put(craftable.getItemId(), craftable);
        }
    }

    public static Craftable forId(int id)
    {
        return craftables.get(id);
    }

    private static Map<Integer, Craftable> craftableRewards = new HashMap<Integer, Craftable>();

    static
    {
        for (Craftable craftable : Craftable.values())
        {
            craftableRewards.put(craftable.getOutcome(), craftable);
        }
    }

    public static Craftable forReward(int id)
    {
        return craftableRewards.get(id);
    }

}
