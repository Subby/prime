package com.asgarniars.rs2.content.skills.craft;

import com.asgarniars.rs2.action.impl.EntityInteraction;
import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.players.Player;

/**
 * Leather tanning happens when a player invokes the second option with the Npc
 * 'Ellis' who will display an interface allowing you to choose what to tan.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public class LeatherTanning extends EntityInteraction
{

    /**
     * The amount of hides to tan.
     */
    short creationAmount;

    /**
     * The type of hide to tan.
     */
    private Hide hide;

    Npc tanner;

    /**
     * 
     * @param entity
     * @param productionCount
     * @param hide
     */
    public LeatherTanning(Entity entity, short creationAmount, Hide hide)
    {
        super(entity, 0);
        this.creationAmount = creationAmount;
        this.hide = hide;
        this.tanner = entity.getInteractingEntity().isNpc() ? (Npc) entity.getInteractingEntity() : null;
    }

    public short getCreationAmount()
    {
        return creationAmount;
    }

    @Override
    public String getInsufficentLevelMessage()
    {
        return "You need a " + Skill.SKILL_NAME[Skill.CRAFTING] + " level of " + getRequiredLevel() + " to tan these hides.";
    }

    @Override
    public String getInteractionMessage()
    {
        return "The tanner tans " + getCreationAmount() + " " + ItemManager.getInstance().getItemName(hide.getItemId()).toLowerCase() + " for you.";
    }

    @Override
    public String getSuccessfulInteractionMessage()
    {
        return null;
    }

    @Override
    public String getUnsuccessfulInteractionMessage()
    {
        return "You do not have any " + ItemManager.getInstance().getItemName(getConsumedItems()[0].getId()).toLowerCase() + " to tan.";
    }

    @Override
    public Item[] getConsumedItems()
    {
        short index = 0;
        if (getInteractingNpc().getDefinition().getName().equalsIgnoreCase("Sbott"))
        {
            index = 1;
        }
        return new Item[]
        { new Item(hide.getItemId(), getCreationAmount()), new Item(995, (hide.getCoins()[index] * getCreationAmount())) };
    }

    @Override
    public Npc getInteractingNpc()
    {
        return tanner;
    }

    @Override
    public Item[] getRewards()
    {
        return new Item[]
        { new Item(hide.getOutcome(), getCreationAmount()) };
    }

    @Override
    public short getRequiredLevel()
    {
        return hide.getRequiredLevel();
    }

    @Override
    public CancelPolicy getCancelPolicy()
    {
        return CancelPolicy.ONLY_ON_WALK;
    }

    @Override
    public StackPolicy getStackPolicy()
    {
        return StackPolicy.NEVER;
    }

    @Override
    public AnimationPolicy getAnimationPolicy()
    {
        return AnimationPolicy.RESET_ALL;
    }

    @Override
    public void execute()
    {
        Player player = (Player) getEntity();

        if (player.getInventory().getItemContainer().getCount(getConsumedItems()[0].getId()) < getConsumedItems()[0].getCount() || getConsumedItems()[0].getCount() == 0)
        {
            player.getActionSender().sendMessage(getUnsuccessfulInteractionMessage());
            player.getActionSender().removeInterfaces();
            player.setInteractingEntity(null);
            this.stop();
            return;

        }
        if (player.getInventory().getItemContainer().getCount(getConsumedItems()[1].getId()) < getConsumedItems()[1].getCount())
        {
            player.getActionSender().sendMessage("You do not have enough coins.");
            player.getActionSender().removeInterfaces();
            player.setInteractingEntity(null);
            this.stop();
            return;
        }

        player.getActionSender().sendMessage(getInteractionMessage());
        player.getActionSender().removeInterfaces();

        if (player.getInventory().addItem(getRewards()[0]))
        {
            player.getInventory().removeItem(getConsumedItems()[0]);
            player.getInventory().removeItem(getConsumedItems()[1]);
        }

        player.setInteractingEntity(null);
        this.stop();
        return;
    }
}
