package com.asgarniars.rs2.content.skills.craft;

import com.asgarniars.rs2.action.impl.ProductionAction;
import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.util.Misc;

/**
 * Glass Blowing takes place when a player uses a glassblowing pipe on molten
 * glass to blow a new glass item.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public class GlassBlowing extends ProductionAction
{

    private short productionCount;

    private Glass glass;

    public GlassBlowing(Entity entity, short productionCount, Glass glass)
    {
        super(entity);
        this.productionCount = productionCount;
        this.glass = glass;
    }

    @Override
    public boolean canProduce()
    {
        return getEntity().isPlayer();
    }

    @Override
    public int getCycleCount()
    {
        return 7;
    }

    @Override
    public short getGraphic()
    {
        return -1;
    }

    @Override
    public short getAnimation()
    {
        return 884;
    }

    @Override
    public double getExperience()
    {
        return glass.getExperience();
    }

    @Override
    public Item[] getConsumedItems()
    {
        return new Item[]
        { new Item(glass.getMaterialId()) };
    }

    @Override
    public String getInsufficentLevelMessage()
    {
        return "You need a " + Skill.SKILL_NAME[getSkill()] + " level of " + getRequiredLevel() + " to blow this glass.";
    }

    @Override
    public int getProductionCount()
    {
        return productionCount;
    }

    @Override
    public int getRequiredLevel()
    {
        return glass.getRequiredLevel();
    }

    @Override
    public Item[] getRewards()
    {
        return new Item[]
        { new Item(glass.getRewardId()) };
    }

    @Override
    public int getSkill()
    {
        return Skill.CRAFTING;
    }

    @Override
    public String getSuccessfulProductionMessage()
    {
        String itemName = ItemManager.getInstance().getItemName(getRewards()[0].getId()).toLowerCase();

        return "you make " + Misc.getArticle(itemName) + " " + itemName + ".";
    }
}
