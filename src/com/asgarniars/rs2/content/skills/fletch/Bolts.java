package com.asgarniars.rs2.content.skills.fletch;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains all data relevant to bolts.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public enum Bolts
{
    BRONZE(9375, 877, 9, 0.5), IRON(9377, 9140, 39, 1.5), STEEL(9378, 9141, 46, 3.5), MITHRIL(9379, 9142, 54, 5), ADAMANT(9380, 9143, 61, 7), RUNE(9381, 9144, 69, 10);
    /**
     * The id
     */
    private int id;
    /**
     * The reward;
     */
    private int reward;
    /**
     * The level required.
     */
    private int levelRequired;
    /**
     * The experience granted.
     */
    private double experience;
    /**
     * A map of item ids to arrow tips.
     */
    private static final Map<Integer, Bolts> bolts = new HashMap<Integer, Bolts>();

    /**
     * Populates the log map.
     */
    static
    {
        for (Bolts bolt : Bolts.values())
        {
            bolts.put(bolt.id, bolt);
        }
    }

    /**
     * Gets an arrow tip by an item id.
     * 
     * @param item The item id.
     * @return The ArrowTip, or <code>null</code> if the object is not a arrow
     *         tip.
     */
    public static Bolts forId(int item)
    {
        return bolts.get(item);
    }

    private Bolts(int id, int reward, int levelRequired, double experience)
    {
        this.id = id;
        this.reward = reward;
        this.levelRequired = levelRequired;
        this.experience = experience;
    }

    /**
     * @return the experience
     */
    public double getExperience()
    {
        return experience;
    }

    /**
     * @return the id
     */
    public int getId()
    {
        return id;
    }

    /**
     * @return the levelRequired
     */
    public int getLevelRequired()
    {
        return levelRequired;
    }

    /**
     * @return the reward
     */
    public int getReward()
    {
        return reward;
    }
}
