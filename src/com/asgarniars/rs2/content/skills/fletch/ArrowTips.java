package com.asgarniars.rs2.content.skills.fletch;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains all data relevant to arrow tips.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public enum ArrowTips
{
    /**
     * Bronze arrow tips.
     */
    BRONZE(39, 882, 1, 2.6),
    /**
     * Iron arrow tips.
     */
    IRON(40, 884, 15, 3.8),
    /**
     * Steel arrow tips.
     */
    STEEL(41, 886, 30, 6.3),
    /**
     * Mithril arrow tips.
     */
    MITHRIL(42, 888, 45, 8.8),
    /**
     * Adamant arrow tips
     */
    ADAMANT(43, 890, 60, 11.3),
    /**
     * Rune arrow tips.
     */
    RUNE(44, 892, 75, 13.8);
    /**
     * The id
     */
    private int id;
    /**
     * The reward;
     */
    private int reward;
    /**
     * The level required.
     */
    private int levelRequired;
    /**
     * The experience granted.
     */
    private double experience;
    /**
     * A map of item ids to arrow tips.
     */
    private static Map<Integer, ArrowTips> arrowtips = new HashMap<Integer, ArrowTips>();

    /**
     * Populates the log map.
     */
    static
    {
        for (ArrowTips arrowtip : ArrowTips.values())
        {
            arrowtips.put(arrowtip.id, arrowtip);
        }
    }

    /**
     * Gets an arrow tip by an item id.
     * 
     * @param item The item id.
     * @return The ArrowTip, or <code>null</code> if the object is not a arrow
     *         tip.
     */
    public static ArrowTips forId(int item)
    {
        return arrowtips.get(item);
    }

    /**
     * 
     * @param id
     * @param reward
     * @param levelRequired
     * @param experience
     */
    private ArrowTips(int id, int reward, int levelRequired, double experience)
    {
        this.id = id;
        this.reward = reward;
        this.levelRequired = levelRequired;
        this.experience = experience;
    }

    /**
     * @return the experience
     */
    public double getExperience()
    {
        return experience;
    }

    /**
     * @return the id
     */
    public int getId()
    {
        return id;
    }

    /**
     * @return the levelRequired
     */
    public int getLevelRequired()
    {
        return levelRequired;
    }

    /**
     * @return the reward
     */
    public int getReward()
    {
        return reward;
    }

}
