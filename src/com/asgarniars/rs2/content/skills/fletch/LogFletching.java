package com.asgarniars.rs2.content.skills.fletch;

import com.asgarniars.rs2.action.impl.ProductionAction;
import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.items.ItemManager;

/**
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public class LogFletching extends ProductionAction
{

    /**
     * The amount of items to produce.
     */
    private int productionCount;

    /**
     * The log index.
     */
    private int logIndex;

    /**
     * The log we are going to fletch
     */
    private FletchableLogs log;

    public LogFletching(Entity entity, int productionCount, int logIndex, FletchableLogs log)
    {
        super(entity);
        this.productionCount = productionCount;
        this.logIndex = logIndex;
        this.log = log;
    }

    @Override
    public boolean canProduce()
    {
        if (getEntity().isPlayer())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public int getCycleCount()
    {
        return 3;
    }

    @Override
    public short getGraphic()
    {
        return -1;
    }

    @Override
    public short getAnimation()
    {
        return 1248;
    }

    @Override
    public double getExperience()
    {
        return log.getExperience()[logIndex];
    }

    @Override
    public Item[] getConsumedItems()
    {
        return new Item[]
        { new Item(log.getLogId()) };
    }

    @Override
    public String getInsufficentLevelMessage()
    {
        return "You need a Fletching level of " + getRequiredLevel() + " to fletch this.";
    }

    @Override
    public int getProductionCount()
    {
        return productionCount;
    }

    @Override
    public int getRequiredLevel()
    {
        return log.getRequiredLevel()[logIndex];
    }

    @Override
    public Item[] getRewards()
    {
        return new Item[]
        { new Item(log.getRewards()[logIndex], log.getRewards()[logIndex] == 52 ? 15 : 1) };
    }

    @Override
    public int getSkill()
    {
        return Skill.FLETCHING;
    }

    @Override
    public String getSuccessfulProductionMessage()
    {
        String prefix = "a";
        String suffix = "";
        String result = ItemManager.getInstance().getItemName(log.getRewards()[logIndex]).toLowerCase();
        char first = result.charAt(0);
        char[] vowels =
        { 'a', 'e', 'i', 'o', 'u' };
        for (char vowel : vowels)
        {
            if (vowel == first)
            {
                prefix = "an";
                break;
            }
        }
        if (log.getRewards()[logIndex] == 52)
        {
            prefix = "some";
            suffix = "s";
        }

        if (result.contains("shortbow"))
        {
            result = "shortbow";
        }
        else if (result.contains("longbow"))
        {
            result = "longbow";
        }
        else if (result.contains("stock"))
        {
            result = "crossbow stock";
        }
        return "You carefully cut the logs " + "into " + prefix + " " + result + suffix + ".";
    }

}
