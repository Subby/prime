package com.asgarniars.rs2.content.skills.fletch;

import com.asgarniars.rs2.action.impl.ProductionAction;
import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.players.Player;

/**
 * 
 * 
 * @author Joshua Barry
 * 
 */
public class ArrowCreation extends ProductionAction
{

    short creationAmount;

    private ArrowTips arrowtip;

    public ArrowCreation(Entity entity, short creationAmount, ArrowTips arrowtip)
    {
        super(entity);
        this.creationAmount = creationAmount;
        this.arrowtip = arrowtip;
    }

    /**
     * Get the creation amount.
     * 
     * @return the creation amount.
     */
    public int getCreationAmount()
    {
        return creationAmount;
    }

    @Override
    public boolean canProduce()
    {
        Player player = (Player) getEntity();

        if (player.getInventory().getItemContainer().getCount(53) <= 0)
        {
            player.getActionSender().sendMessage("You do not have any headless arrows!");
            return false;
        }
        else if (player.getInventory().getItemContainer().getCount(arrowtip.getId()) <= 0)
        {
            player.getActionSender().sendMessage("You do not have any arrowtips!");
            return false;
        }
        else
        {
            return true;
        }
    }

    @Override
    public int getCycleCount()
    {
        return 0;
    }

    @Override
    public short getGraphic()
    {
        return -1;
    }

    @Override
    public short getAnimation()
    {
        return -1;
    }

    @Override
    public double getExperience()
    {
        return arrowtip.getExperience();
    }

    @Override
    public Item[] getConsumedItems()
    {
        return new Item[]
        { new Item(53, getCreationAmount()), new Item(arrowtip.getId(), getCreationAmount()) };
    }

    @Override
    public String getInsufficentLevelMessage()
    {
        return "You need a Fletching level of " + arrowtip.getLevelRequired() + " to make these arrows.";
    }

    @Override
    public int getProductionCount()
    {
        return 1;
    }

    @Override
    public int getRequiredLevel()
    {
        return arrowtip.getLevelRequired();
    }

    @Override
    public Item[] getRewards()
    {
        System.out.println("Reward ID: " + arrowtip.getReward() + " lowest amt :  " + getCreationAmount());
        return new Item[]
        { new Item(arrowtip.getReward(), getCreationAmount()) };
    }

    @Override
    public int getSkill()
    {
        return Skill.FLETCHING;
    }

    @Override
    public String getSuccessfulProductionMessage()
    {
        return "You attach the arrow tips to the headless arrows.";
    }

}
