package com.asgarniars.rs2.content.skills.fletch;

/**
 * 
 * @author josh
 * 
 */
public enum CreationType
{

    ARROW_CREATION, BOW_CREATION, CROSSBOW_CREATION, HEADLESS_ARROW_CREATION, UNSTRUNG_BOW_CREATION

}
