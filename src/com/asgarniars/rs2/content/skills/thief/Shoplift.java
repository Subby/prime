package com.asgarniars.rs2.content.skills.thief;

import com.asgarniars.rs2.action.Action;
import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.model.players.GlobalObject;
import com.asgarniars.rs2.model.players.GlobalObjectHandler;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;
import com.asgarniars.rs2.util.Language;
import com.asgarniars.rs2.util.Misc;
import com.asgarniars.rs2.util.clip.ObjectDefinition;
import com.asgarniars.rs2.util.clip.RSObject;

/**
 * The event in which an entity steals from a stall.
 * 
 * @author Joshua Barry
 * 
 */
public class Shoplift extends Action
{

    /**
     * The player who is stealing.
     */
    private Player player;

    /**
     * The dummy object that represents an 'empty' version of the stall after we
     * swipe the items.
     */
    private GlobalObject dummy;

    /**
     * The object in it's restored state.
     */
    private GlobalObject restored;

    /**
     * The definions of the object we are stealing from.
     */
    private ObjectDefinition definition;

    /**
     * The type of stall we are stealing from.
     */
    private StallType stall;

    /**
     * 
     * @param player The acting player.
     * @param object The object associated with the clean stall.
     * @param ticks The amount of ticks before executing the service.
     */
    public Shoplift(Player player, RSObject object, int ticks)
    {
        super(player, ticks);
        this.player = player;
        this.dummy = new GlobalObject(634, object.getPosition(), object.getFace(), object.getType());
        this.restored = new GlobalObject(object.getId(), object.getPosition(), object.getFace(), object.getType());
        this.stall = StallType.forId(object.getId());
        this.definition = ObjectDefinition.forId(object.getId());
    }

    @Override
    public CancelPolicy getCancelPolicy()
    {
        return CancelPolicy.ALWAYS;
    }

    @Override
    public StackPolicy getStackPolicy()
    {
        return StackPolicy.NEVER;
    }

    @Override
    public AnimationPolicy getAnimationPolicy()
    {
        return AnimationPolicy.RESET_ALL;
    }

    @Override
    public void execute()
    {

        if (!allow())
        {
            this.stop();
            return;
        }

        final int randomId = Misc.generatedValue(stall.getRewards());
        final String itemName = ItemManager.getInstance().getItemName(randomId);
        final String prefix = Misc.getArticle(itemName);
        final String stallName = definition.getName().toLowerCase();
        final Item item = new Item(randomId, 1);

        player.getUpdateFlags().sendAnimation(0x340);
        player.getActionSender().sendMessage("You attempt to steal from the " + stallName + "...");

        World.submit(new Task(2)
        {

            @Override
            protected void execute()
            {

                GlobalObjectHandler.createGlobalObject(player, dummy);

                if (player.getInventory().addItem(item))
                {
                    player.getActionSender().sendMessage("You steal " + prefix + itemName + " from the " + stallName);
                    player.getSkill().addExperience(Skill.THIEVING, stall.getExperience());
                    World.submit(new Task(8)
                    {

                        @Override
                        protected void execute()
                        {
                            GlobalObjectHandler.createGlobalObject(player, restored);
                            this.stop();

                        }

                    });
                }
                this.stop();

            }

        });

        this.stop();

    }

    /**
     * Determines whether the player is allowed to steal or not.
     * 
     * @return
     */
    private boolean allow()
    {
        if (player.getSkill().getLevel()[Skill.THIEVING] < stall.getLevelRequirement())
        {
            player.getActionSender().sendMessage("You need a " + Skill.getSkillName(Skill.THIEVING) + " level of " + stall.getLevelRequirement() + " to steal from this stall.");
            return false;
        }
        else if (player.getInventory().getItemContainer().freeSlots() < 1)
        {
            player.getActionSender().sendMessage(Language.NO_SPACE);
            return false;
        }
        else if (player.getAttribute("isStunned") != null)
        {
            player.getActionSender().sendMessage("You're stunned!");
            return false;
        }
        return true;

    }

}
