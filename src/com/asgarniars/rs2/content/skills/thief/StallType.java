package com.asgarniars.rs2.content.skills.thief;

import java.util.HashMap;
import java.util.Map;

/**
 * An enumeration of stalls that the player can steal from.
 * 
 * @author Joshua Barry
 * 
 */
public enum StallType
{

    VEGETABLE(4706, 2, 2, 10, 1942, 1965, 1957, 1982, 1550), BAKERY_STALL(2561, 5, 2.5, 16, 2309, 1891, 1901), GENERAL_STALL(4876, 5, 7, 10, 2347, 1931, 590), TEA_STALL(635, 5, 7, 16, 712), CRAFTING_STALL(4874, 5, 7, 16, 1755, 1592, 1597), MONKEY_FOOD_STALL(4875, 5, 7, 16, 1963), ROCK_CAKES_STALL(2793, 15, 5, 10, 2379), SILK_STALL(2560, 20, 8, 24, 950), WINE_STALL(14011, 22, 16, 27, 1935, 1937, 1993, 7919, 1987), SEED_STALL(7053, 27, 11, 10, 5307, 5318, 5096, 5319, 5305, 5308, 5324, 5309, 5306, 5322, 5320, 5097, 5098, 5310, 5311, 5321, 5323, 5103), // http://runescape.wikia.com/wiki/Seed_stall
    // scroll to
    // bottom.
    FUR_STALL(2563, 35, 15, 36, 958), FISH_STALL(4705, 42, 16, 42, 331, 359, 377), SILVER_STALL(2565, 50, 30, 54, 442), MAGIC_STALL(4877, 65, 80, 100, 554, 555, 557, 556), SCIMITAR_STALL(4878, 65, 80, 160, 1323, 1325), SPICE_STALL(2564, 65, 80, 81, 2007), GEM_STALL(2562, 75, 180, 10, 1617, 1619, 1621, 1623);

    private int objectId;

    private int levelRequirement;

    private double respawnTime;

    private double experience;

    private Integer[] rewards;

    private static Map<Integer, StallType> stalls = new HashMap<Integer, StallType>();

    /**
     * 
     * @param objectId
     * @param levelRequired
     * @param respawnTime
     * @param experience
     * @param rewards
     */
    private StallType(int objectId, int levelRequired, double respawnTime, double experience, Integer... rewards)
    {
        this.objectId = objectId;
        this.levelRequirement = levelRequired;
        this.respawnTime = respawnTime;
        this.experience = experience;
        this.rewards = rewards;
    }

    public int getObjectId()
    {
        return objectId;
    }

    public int getLevelRequirement()
    {
        return levelRequirement;
    }

    public double getRespawnTime()
    {
        return respawnTime;
    }

    public double getExperience()
    {
        return experience;
    }

    public Integer[] getRewards()
    {
        return rewards;
    }

    static
    {
        for (StallType stall : StallType.values())
            stalls.put(stall.getObjectId(), stall);
    }

    public static StallType forId(int objectId)
    {
        return stalls.get(objectId);
    }

}
