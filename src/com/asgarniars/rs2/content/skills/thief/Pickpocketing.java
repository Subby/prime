package com.asgarniars.rs2.content.skills.thief;

import com.asgarniars.rs2.action.impl.EntityInteraction;
import com.asgarniars.rs2.content.dialogue.Dialogue;
import com.asgarniars.rs2.content.dialogue.StatementDialogue;
import com.asgarniars.rs2.content.dialogue.StatementType;
import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;
import com.asgarniars.rs2.util.Misc;

/**
 * Pickpocketing is an extension of an EntityInteraction. This handles the
 * action in which a player reaches for the pocket of another entity (Npc) which
 * is our @victim in this case. The player has a chance of failure in which the
 * victim catches the player attempting to pick their pocket and hits the @thief
 * so damn hard that the @thief is stunned temporarily. Upon success the player
 * received a small loot dependent on the victim at hand.
 * 
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public class Pickpocketing extends EntityInteraction
{

    private Player thief;

    private Npc victim;

    private ThieveableNpcs victimData;

    /**
     * 
     * @param thief The entity who's urging to reach for the pocket.
     * @param victim the ever-so-helpless victim.
     * @param ticks The amount of cycles to skip before execution.
     */
    public Pickpocketing(Player thief, Npc victim, int ticks)
    {
        super(thief, ticks);
        this.thief = thief;
        this.victim = victim;
        this.victimData = ThieveableNpcs.forId(victim.getNpcId());
    }

    /**
     * Thieving animation.
     */
    protected static final short THIEVING_ANIMATION = 881;

    /**
     * 'Stunned' Graphics.
     */
    private static final short STUNNED_GFX = 254;

    /**
     * Npc attacking on fail animation.
     */
    private static final short NPC_ATTACK_ANIMATION = 401;

    /**
     * Player blocking on fail animation.
     */
    private static final short PLAYER_BLOCK_ANIMATION = 404;

    /**
     * Perform checks to determine whether this player can pick-pocket a
     * particular @Npc Victim.
     * 
     * @param thief The entity who's urging to reach for the pocket.
     * @param victim the ever-so-helpless victim.
     * @return
     */
    private boolean canPickPocket(Player thief, Npc victim)
    {
        if (thief.getSkillLevel(Skill.THIEVING) < getRequiredLevel())
        {
            Dialogue dialogue = new StatementDialogue(StatementType.NORMAL, getInsufficentLevelMessage());
            thief.open(dialogue);
            return false;
        }
        else if (thief.isInstigatingAttack() || thief.isInCombat())
        {
            thief.getActionSender().sendMessage("You cannot pickpocket during combat!");
            return false;
        }
        else if (victim.isInstigatingAttack() || victim.isInCombat())
        {
            thief.getActionSender().sendMessage("You cannot pickpocket this " + victim.getDefinition().getName().toLowerCase() + " while they're in combat.");
            return false;
        }
        else if (thief.getAttribute("isStunned") != null)
        {
            return !((Boolean) thief.getAttribute("isStunned"));
        }
        else
        {
            return (Boolean) (thief.getAttribute("isSkilling") == null);
        }
    }

    /**
     * Determines the chance of failure. TODO : make this a more elaborate
     * method.
     * 
     * @param thief The entity who is urging to reach for the pocket.
     * @return the result of chance.
     */
    public boolean isSuccessful(Entity thief)
    {
        Player player = (Player) thief;
        short factor = (short) Misc.random(thief.getSkillLevel(Skill.THIEVING));
        short fluke = (short) Misc.random(getRequiredLevel() == 1 ? 3 : getRequiredLevel());
        if (thief.getAttribute("isDebugging") != null)
        {
            player.getActionSender().sendMessage("Factor / Fluke    >>    " + factor + " / " + fluke);
        }
        return factor > fluke;
    }

    /**
     * Checks the Npc's name appends the appropriate string.
     * 
     * @return
     */
    public String getVictimForceMessage()
    {
        return getInteractingEntityName().contains("farmer") ? "Cor blimey, mate! What are ye doing in me pockets?" : "What do you think you're doing?";

    }

    public String getInteractingEntityName()
    {
        return getInteractingNpc().getDefinition().getName().toLowerCase();
    }

    @Override
    public CancelPolicy getCancelPolicy()
    {
        return CancelPolicy.ONLY_ON_WALK;
    }

    @Override
    public StackPolicy getStackPolicy()
    {
        return StackPolicy.ALWAYS;
    }

    @Override
    public AnimationPolicy getAnimationPolicy()
    {
        return AnimationPolicy.RESET_ALL;
    }

    @Override
    public String getInsufficentLevelMessage()
    {
        return "You need a " + Skill.SKILL_NAME[Skill.THIEVING] + " level of " + getRequiredLevel() + " to pick the " + getInteractingEntityName() + "'s pocket.";
    }

    @Override
    public String getInteractionMessage()
    {
        return "You attempt to pick the " + getInteractingEntityName() + "'s pocket.";
    }

    @Override
    public String getSuccessfulInteractionMessage()
    {
        return "You pick the " + getInteractingEntityName() + "'s pocket.";
    }

    @Override
    public String getUnsuccessfulInteractionMessage()
    {
        return "You fail to pick the " + (getInteractingEntityName().equals("man") ? "fellow" : getInteractingEntityName()) + "'s pocket.";
    }

    @Override
    public Npc getInteractingNpc()
    {
        return victim;
    }

    @Override
    public Item[] getConsumedItems()
    {
        return null;
    }

    @Override
    public Item[] getRewards()
    {
        final short slot = (short) Misc.random(victimData.getNormalLoot().length - 1);
        return new Item[]
        { victimData.getNormalLoot()[slot] };
    }

    @Override
    public short getRequiredLevel()
    {
        return victimData.getLevelRequirement();
    }

    @Override
    public void execute()
    {
        if (!canPickPocket(thief, victim))
        {
            this.stop();
            return;
        }
        thief.setAttribute("isSkilling", (byte) 1);
        thief.getUpdateFlags().sendAnimation(THIEVING_ANIMATION);
        thief.getActionSender().sendMessage(getInteractionMessage());

        World.submit(new Task(3)
        {
            @Override
            protected void execute()
            {
                if (!isSuccessful(thief))
                {
                    victim.getUpdateFlags().sendForceMessage(getVictimForceMessage());
                    victim.getUpdateFlags().sendAnimation(NPC_ATTACK_ANIMATION);
                    thief.setCanWalk(false);
                    thief.setAttribute("isStunned", true);
                    thief.getUpdateFlags().sendAnimation(PLAYER_BLOCK_ANIMATION);
                    thief.getUpdateFlags().sendGraphic(STUNNED_GFX, 100 << 16);
                    thief.getActionSender().sendMessage(getUnsuccessfulInteractionMessage());
                    thief.hit(victimData.stunDamage(), 1, false);
                    World.submit(new Task(victimData.getStunTimer())
                    {
                        @Override
                        protected void execute()
                        {
                            thief.removeAttribute("isStunned");
                            thief.setCanWalk(true);
                            this.stop();
                        }
                    });
                    this.stop();
                }
                else
                {
                    thief.getActionSender().sendMessage(getSuccessfulInteractionMessage());
                    thief.getInventory().addItem(getRewards()[0]);
                    thief.getSkill().addExperience(Skill.THIEVING, victimData.getExpGain());
                    this.stop();
                }
            }

            @Override
            public void stop()
            {
                thief.removeAttribute("isSkilling");
                thief.setInteractingEntity(null);
                victim.setInteractingEntity(null);
                super.stop();
            }
        });
        this.stop();
    }

}