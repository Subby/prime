package com.asgarniars.rs2.content.skills.herbalism;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Contains all data relevant to a herb
 * </p>
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public enum HerbType
{
    /**
     * Guam
     */
    GUAM(199, 249, 1, 2.5),
    /**
     * Marrentill
     */
    MARRENTILL(201, 251, 5, 3.8),
    /**
     * Tarromin
     */
    TARROMIN(203, 253, 11, 5),
    /**
     * Harralander
     */
    HARRALANDER(205, 255, 20, 6.3),
    /**
     * Ranaar
     */
    RANARR(207, 257, 25, 7.5),
    /**
     * Irit
     */
    IRIT(209, 259, 40, 8.8),
    /**
     * Avantoe
     */
    AVANTOE(211, 261, 48, 10),
    /**
     * Kuarm
     */
    KWUARM(213, 263, 54, 11.3),
    /**
     * Cadantine
     */
    CADANTINE(215, 265, 65, 12.5),
    /**
     * Dwarf Weed
     */
    DWARF_WEED(217, 267, 70, 13.8),
    /**
     * Torstol
     */
    TORSTOL(219, 269, 75, 15);
    /**
     * The id of the herb
     */
    private int id;
    /**
     * The reward for identifying the herb.
     */
    private int reward;
    /**
     * The level required to identify this herb.
     */
    private int level;
    /**
     * The experience granted for identifying the herb.
     */
    private double experience;
    /**
     * A map of item ids to herbs.
     */
    private static Map<Integer, HerbType> herbs = new HashMap<Integer, HerbType>();

    /**
     * Populates the herb map.
     */
    static
    {
        for (HerbType herb : HerbType.values())
        {
            herbs.put(herb.id, herb);
        }
    }

    /**
     * Gets a herb by an item id.
     * 
     * @param item The item id.
     * @return The <code>Herb</code> or <code>null</code> if the item is not a
     *         herb.
     */
    public static HerbType forId(int item)
    {
        return herbs.get(item);
    }

    private HerbType(int id, int reward, int level, double experience)
    {
        this.id = id;
        this.reward = reward;
        this.level = level;
        this.experience = experience;
    }

    /**
     * @return the experience
     */
    public double getExperience()
    {
        return experience;
    }

    /**
     * @return the id
     */
    public int getId()
    {
        return id;
    }

    /**
     * @return the level
     */
    public int getRequiredLevel()
    {
        return level;
    }

    /**
     * @return the reward
     */
    public int getReward()
    {
        return reward;
    }
}