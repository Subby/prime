package com.asgarniars.rs2.content.skills.herbalism;

/**
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public enum IngredientType
{
    PRIMARY_INGREDIENT, SECONDARY_INGREDIENT
}
