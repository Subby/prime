package com.asgarniars.rs2.content.skills.herbalism;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public enum PrimaryIngredient
{

    /**
     * Guam leaf.
     */
    GUAM(249, 91, 3),
    /**
     * Marrentill leaf.
     */
    MARRENTILL(251, 93, 5),
    /**
     * Tarromin leaf.
     */
    TARROMIN(253, 95, 12),
    /**
     * Harralander leaf.
     */
    HARRALANDER(255, 97, 22),
    /**
     * Ranarr leaf.
     */
    RANARR(257, 99, 30),
    /**
     * Irit leaf.
     */
    IRIT(259, 101, 45),
    /**
     * Avantoe leaf.
     */
    AVANTOE(261, 103, 50),
    /**
     * Kwuarm leaf.
     */
    KWUARM(263, 105, 55),
    /**
     * Cadantine leaf.
     */
    CADANTINE(265, 107, 66),
    /**
     * Dwarf Weed leaf.
     */
    DWARF_WEED(267, 109, 72),
    /**
     * Torstol leaf.
     */
    TORSTOL(269, 111, 78),
    /**
     * Toadflax leaf.
     */
    TOADFLAX(2998, 3002, 34),
    /**
     * Snapdragon leaf.
     */
    SNAPDRAGON(3000, 3004, 63);
    /**
     * The id.
     */
    private int id;
    /**
     * The reward.
     */
    private int reward;
    /**
     * The level.
     */
    private int level;
    /**
     * A map of object ids to primary ingredients.
     */
    private static Map<Integer, PrimaryIngredient> ingredients = new HashMap<Integer, PrimaryIngredient>();

    /**
     * Populates the log map.
     */
    static
    {
        for (PrimaryIngredient ingredient : PrimaryIngredient.values())
        {
            ingredients.put(ingredient.id, ingredient);
        }
    }

    /**
     * Gets a log by an item id.
     * 
     * @param item The item id.
     * @return The PrimaryIngredient, or <code>null</code> if the object is not
     *         a PrimaryIngredient.
     */
    public static PrimaryIngredient forId(int item)
    {
        return ingredients.get(item);
    }

    private PrimaryIngredient(int id, int reward, int level)
    {
        this.id = id;
        this.level = level;
        this.reward = reward;
    }

    /**
     * Gets the id.
     * 
     * @return The id.
     */
    public int getId()
    {
        return id;
    }

    /**
     * Gets the required level.
     * 
     * @return The required level.
     */
    public int getRequiredLevel()
    {
        return level;
    }

    /**
     * Gets the reward.
     * 
     * @return The reward.
     */
    public int getReward()
    {
        return reward;
    }

}
