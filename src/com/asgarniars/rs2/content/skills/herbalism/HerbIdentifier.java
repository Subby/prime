package com.asgarniars.rs2.content.skills.herbalism;

import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.model.players.Player;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class HerbIdentifier
{

    private Player player;

    private HerbType herb;

    public HerbIdentifier(Player player, HerbType herb)
    {
        this.herb = herb;
        this.player = player;

    }

    private boolean canIdentify()
    {
        if (player.getSkillLevel(Skill.HERBLORE) < herb.getRequiredLevel())
        {
            player.getActionSender().sendMessage("You need a herblore level of " + herb.getRequiredLevel() + " to clean and identify this herb.");
            return false;
        }
        return true;
    }

    public void identifyHerb()
    {
        if (canIdentify())
        {
            player.getActionSender().sendMessage("You clean the dirt from the " + ItemManager.getInstance().getItemName(herb.getId()).toLowerCase() + ".");
            player.getInventory().removeItem(new Item(herb.getId()));
            if (player.getInventory().addItem(new Item(herb.getReward())))
            {
                player.getSkill().addExperience(Skill.HERBLORE, herb.getExperience());
            }
        }
    }

}
