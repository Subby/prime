package com.asgarniars.rs2.content.skills.herbalism;

import com.asgarniars.rs2.action.impl.ProductionAction;
import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.items.ItemManager;

/**
 * Leave these action base skills for me to set the bases up for please.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public class Herbalism extends ProductionAction
{

    /**
     * The ingredient type.
     */
    private IngredientType type;
    /**
     * The primary ingredient of the mixture.
     */
    private PrimaryIngredient primary;
    /**
     * The secondary ingredient of the mixture.
     */
    private SecondaryIngredient secondary;

    private short productionCount;

    /**
     * Starts the Herblore event!
     * 
     * @param entity
     * @param productionCount
     * @param herbType
     * @param ingredientType
     * @param primaryIngredient
     * @param secondaryIngredient
     */
    public Herbalism(Entity entity, short productionCount, IngredientType ingredientType, PrimaryIngredient primaryIngredient, SecondaryIngredient secondaryIngredient)
    {
        super(entity);
        this.productionCount = productionCount;
        this.type = ingredientType;
        this.primary = primaryIngredient;
        this.secondary = secondaryIngredient;
    }

    @Override
    public boolean canProduce()
    {
        if (getEntity().isPlayer())
        {
            return true;
        }
        return false;
    }

    @Override
    public int getCycleCount()
    {
        return 4;
    }

    @Override
    public double getExperience()
    {
        switch (type)
        {
        case PRIMARY_INGREDIENT:
            return 0;
        case SECONDARY_INGREDIENT:
            return secondary.getExperience();
        }
        return 0;
    }

    @Override
    public Item[] getConsumedItems()
    {
        switch (type)
        {
        case PRIMARY_INGREDIENT:
            return new Item[]
            { new Item(primary.getId()), new Item(227) };
        case SECONDARY_INGREDIENT:
            return new Item[]
            { new Item(secondary.getId()), secondary.getRequiredItem() };
        }
        return null;
    }

    @Override
    public String getInsufficentLevelMessage()
    {
        return "You need a " + Skill.SKILL_NAME[getSkill()] + " level of " + getRequiredLevel() + " to combine these ingredients.";
    }

    @Override
    public int getProductionCount()
    {
        return productionCount;
    }

    @Override
    public int getRequiredLevel()
    {
        return type == IngredientType.PRIMARY_INGREDIENT ? primary.getRequiredLevel() : secondary.getRequiredLevel();
    }

    @Override
    public Item[] getRewards()
    {
        switch (type)
        {
        case PRIMARY_INGREDIENT:
            return new Item[]
            { new Item(primary.getReward()) };
        case SECONDARY_INGREDIENT:
            return new Item[]
            { new Item(secondary.getReward()) };
        }
        return null;
    }

    @Override
    public int getSkill()
    {
        return Skill.HERBLORE;
    }

    @Override
    public String getSuccessfulProductionMessage()
    {
        switch (type)
        {
        case PRIMARY_INGREDIENT:
            return "You put the " + ItemManager.getInstance().getItemName(primary.getId()).replaceAll(" clean", "").replaceAll(" leaf", "").toLowerCase() + " into the vial of water.";
        case SECONDARY_INGREDIENT:
            return "You mix the " + ItemManager.getInstance().getItemName(secondary.getId()).toLowerCase() + " into your potion.";
        }
        return "";
    }

    @Override
    public short getGraphic()
    {
        return -1;
    }

    @Override
    public short getAnimation()
    {
        return 363;
    }

}
