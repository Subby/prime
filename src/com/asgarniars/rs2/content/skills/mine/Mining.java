package com.asgarniars.rs2.content.skills.mine;

import com.asgarniars.rs2.action.impl.HarvestingAction;
import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.util.Language;
import com.asgarniars.rs2.util.Misc;
import com.asgarniars.rs2.util.clip.RSObject;

/**
 * Handles ore mining. A player hack's at the rock with their pickaxe in attempt
 * to collect a bundle of ore's from the rock. There is a chance to find random
 * gem's inside ores, as well, TODO: gem's can be mined in Shilo Village.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public class Mining extends HarvestingAction
{

    /**
     * The rock we are trying to mine from.
     */
    private RSObject rock = null;
    /**
     * The ore we are trying to collect.
     */
    private Ore ore = null;
    /**
     * The pickaxe we should be carrying.
     */
    private Pickaxe pick = null;

    /**
     * Initialise our data.
     * 
     * @param entity The player who is Mining.
     * @param object The rock that we are trying to Mine.
     */
    public Mining(Entity entity, RSObject object)
    {
        super(entity);
        this.rock = object;
        this.ore = Ore.forId(object.getId());
    }

    @Override
    public short getCycleCount()
    {// does it work properly too?
     // it works great as your get better at mining
     // its bettter
        short level = (short) ore.getRequiredLevel();
        if (this.getGameObjectMaxHealth() == -1)
        {
            level = 25;
        }
        short modifier = (short) pick.getRequiredLevel();
        if (modifier == 1)
        {
            modifier = 5;
        }
        double cycleCount = 1;
        cycleCount = Math.ceil((level * 50 - getEntity().getSkillLevel(getSkill()) * 10) / modifier * 0.0625 * 3);
        if (cycleCount < 1)
        {
            cycleCount = 1;
        }
        return (short) cycleCount;
    }

    @Override
    public RSObject getGlobalObject()
    {
        return rock;
    }

    @Override
    public RSObject getReplacementObject()
    {
        int index = 0;
        for (int i = 0; i < ore.getObjectIds().length; i++)
        {
            if (ore.getObjectIds()[i] == getGlobalObject().getId())
            {
                index = i;
                break;
            }

        }
        return new RSObject(ore.getReplacementRocks()[index], getGlobalObject().getX(), getGlobalObject().getY(), getEntity().getPosition().getZ(), getGlobalObject().getType(), getGlobalObject().getFace());
    }

    @Override
    public int getObjectRespawnTimer()
    {
        return ore.getRespawnTimer();
    }

    @Override
    public Item getReward()
    {
        switch (ore)
        {
        case SHILO_GEM_STONE:
            getEntity().setAttribute("shiloGem", (short) Misc.generatedValue(new short[]
            { 1629, 1627, 1625, 1623, 1621, 1619, 1617 }));

            return new Item((Short) (getEntity().getAttribute("shiloGem")));
        default:
            return new Item(ore.getOreId());
        }
    }

    @Override
    public int getSkill()
    {
        return Skill.MINING;
    }

    @Override
    public int getRequiredLevel()
    {
        return ore.getRequiredLevel();
    }

    @Override
    public double getExperience()
    {
        return ore.getExperience();
    }

    @Override
    public String getInsufficentLevelMessage()
    {
        return "You need a " + Skill.SKILL_NAME[getSkill()] + " level of " + getRequiredLevel() + " to mine this ore.";
    }

    @Override
    public String getHarvestStartedMessage()
    {
        return "You swing your pick at the rock.";
    }

    @Override
    public String getSuccessfulHarvestMessage()
    {
        switch (ore)
        {
        case SHILO_GEM_STONE:
            String item = ItemManager.getInstance().getItemName((Short) getEntity().getAttribute("shiloGem")).toLowerCase().replace("uncut ", "");
            return "You just mined " + Misc.getArticle(item).concat(" ") + Misc.capitalizeFirst(item) + "!";
        default:
            return "You manage to mine some " + getReward().getDefinition().getName().toLowerCase() + ".";
        }
    }

    @Override
    public String getInventoryFullMessage()
    {
        return Language.NO_SPACE;
    }

    @Override
    public short getAnimationId()
    {
        return pick.getAnimation();
    }

    @Override
    public boolean canHarvest()
    {
        Player player = (Player) getEntity();

        if (getGlobalObject() == null)
        {
            player.getActionSender().sendMessage("Oops, this ore needs a minor configuration!");
            player.getActionSender().sendMessage("Please type ::pos and file a bug report online with your position.");
            return false;
        }

        for (Pickaxe pickaxe : Pickaxe.values())
        {
            if (player.getInventory().getItemContainer().contains(pickaxe.getId()) || player.getEquipment().getItemContainer().contains(pickaxe.getId()) && player.getSkillLevel(getSkill()) > pickaxe.getRequiredLevel())
            {
                this.pick = pickaxe;
                break;
            }
        }

        if (pick == null)
        {
            player.getActionSender().sendMessage("You do not have a pickaxe that you can use.");
            return false;
        }

        if (!player.getInventory().hasRoomFor(getReward()))
        {
            player.getActionSender().sendMessage(getInventoryFullMessage());
            return false;
        }

        return true;
    }

    @Override
    public int getGameObjectMaxHealth()
    {
        return ore.getOreCount();
    }

    @Override
    public byte getRespawnRate()
    {
        // TODO Auto-generated method stub
        return 3;
    }

}
