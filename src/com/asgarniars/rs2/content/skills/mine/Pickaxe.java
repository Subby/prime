package com.asgarniars.rs2.content.skills.mine;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains the data for the pickaxe.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public enum Pickaxe
{
    /**
     * Rune pickaxe.
     */
    RUNE(1275, 41, 624),
    /**
     * Adamant pickaxe.
     */
    ADAMANT(1271, 31, 628),
    /**
     * Mithril pickaxe.
     */
    MITHRIL(1273, 21, 629),
    /**
     * Steel pickaxe.
     */
    STEEL(1269, 11, 627),
    /**
     * Iron pickaxe.
     */
    IRON(1267, 5, 626),
    /**
     * Bronze pickaxe.
     */
    BRONZE(1265, 1, 625);
    /**
     * The item id of this pick axe.
     */
    private short id;
    /**
     * The level required to use this pick axe.
     */
    private short level;

    /**
     * The 'id' value of the animation to cast.
     */
    private short animation;
    /**
     * A list of pick axes.
     */
    private static List<Pickaxe> pickaxes = new ArrayList<Pickaxe>();

    /**
     * Populates the pick axe map.
     */
    static
    {
        for (Pickaxe pickaxe : Pickaxe.values())
        {
            pickaxes.add(pickaxe);
        }
    }

    /**
     * Gets the list of pick axes.
     * 
     * @return The list of pick axes.
     */
    public static List<Pickaxe> getPickaxes()
    {
        return pickaxes;
    }

    private Pickaxe(int id, int level, int animation)
    {
        this.id = (short) id;
        this.level = (short) level;
        this.animation = (short) animation;
    }

    /**
     * @return the animation
     */
    public short getAnimation()
    {
        return animation;
    }

    /**
     * @return the id
     */
    public int getId()
    {
        return id;
    }

    /**
     * @return the level
     */
    public int getRequiredLevel()
    {
        return level;
    }
}
