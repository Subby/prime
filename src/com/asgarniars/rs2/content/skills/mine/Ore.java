package com.asgarniars.rs2.content.skills.mine;

import java.util.HashMap;
import java.util.Map;

import com.asgarniars.rs2.util.Misc;

public enum Ore
{
    /**
     * Clay ore.
     */
    ESS(7936, 1, 5, 1000, -1, new int[]
    { 2491 }, new int[]
    { -1 }), CLAY(434, 1, 5, 2, 1, new int[]
    { 2108, 2109 }, new int[]
    { 450, 451 }),
    /**
     * Copper ore.
     */
    COPPER(436, 1, 17.5, 6, 1, new int[]
    { 11959, 11960, 11961, 11962, 2090, 2091 }, new int[]
    { 9725, 9725, 9725, 9725, 450, 451 }),
    /**
     * Tin ore.
     */
    TIN(438, 1, 17.5, 6, 1, new int[]
    { 2094, 2095, 11957, 11958, 11959 }, new int[]
    { 450, 451, 9725, 9725, 9725 }),
    /**
     * Iron ore.
     */
    IRON(440, 15, 35, 15, 1, new int[]
    { 11936, 11937, 11938, 11954, 11955, 11956, 2092, 2093 }, new int[]
    { 9725, 9725, 9725, 9725, 9725, 9725, 450, 451 }),
    /**
     * Silver ore.
     */
    SILVER(442, 20, 40, 100, 1, new int[]
    { 2100, 2101 }, new int[]
    { 450, 451 }),
    /**
     * Gold ore.
     */
    GOLD(444, 40, 65, 100, 1, new int[]
    { 2098, 2099 }, new int[]
    { 450, 451 }),
    /**
     * Coal ore.
     */
    COAL(453, 30, 50, 50, 3, new int[]
    { 11932, 11930, 2096, 2097 }, new int[]
    { 9725, 9725, 450, 451 }),
    /**
     * Mithril ore.
     */
    MITHRIL(447, 55, 80, 200, 4, new int[]
    { 11942, 11944, 2102, 2103 }, new int[]
    { 9725, 9725, 450, 451 }),
    /**
     * Adamantite ore.
     */
    ADAMANTITE(449, 70, 95, 400, 6, new int[]
    { 11941, 11939, 2104, 2105 }, new int[]
    { 9725, 9725, 450, 451 }),
    /**
     * Rune ore.
     */
    RUNE(451, 85, 125, 1000, 8, new int[]
    { 2106, 2107, 14859, 14860 }, new int[]
    { 450, 451, 9725, 9725 }),

    SHILO_GEM_STONE(1627, 1, 0, 1000, 1, new int[]
    { 2111 }, new int[]
    { -1 /* todo complete */});

    /**
     * 
     * @param ore
     * @param level
     * @param experience
     * @param respawnTimer
     * @param oreCount
     * @param objects
     * @param replacementRocks
     */
    /**
     * The object ids of this rock.
     */
    private int[] objects;
    /**
     * The level required to mine this rock.
     */
    private int level;
    /**
     * The ore rewarded for each cut of the tree.
     */
    private int log;
    /**
     * The time it takes for this rock to respawn.
     */
    private int respawnTimer;
    /**
     * The amount of ores this rock contains.
     */
    private int oreCount;
    /**
     * The experience granted for mining this rock.
     */
    private double experience;
    /**
     * The rocks to replace.
     */
    private int[] replacementRocks;
    /**
     * A map of object ids to rocks.
     */
    private static Map<Integer, Ore> rocks = new HashMap<Integer, Ore>();

    static
    {
        for (Ore rock : Ore.values())
        {
            for (int object : rock.objects)
            {
                rocks.put(object, rock);
            }
        }
    }

    /**
     * Gets a rock by an object id.
     * 
     * @param object The object id.
     * @return The rock, or <code>null</code> if the object is not a rock.
     */
    public static Ore forId(int object)
    {
        return rocks.get(object);
    }

    /**
     * 
     * @param ore
     * @param level
     * @param experience
     * @param respawnTimer
     * @param oreCount
     * @param objects
     * @param replacementRocks
     */
    private Ore(int ore, int level, double experience, int respawnTimer, int oreCount, int[] objects, int[] replacementRocks)
    {
        this.objects = objects;
        this.level = level;
        this.experience = experience;
        this.respawnTimer = respawnTimer;
        this.oreCount = oreCount;
        this.log = ore;
        this.replacementRocks = replacementRocks;
    }

    /**
     * Gets the experience.
     * 
     * @return The experience.
     */
    public double getExperience()
    {
        return experience;
    }

    /**
     * Gets the object ids.
     * 
     * @return The object ids.
     */
    public int[] getObjectIds()
    {
        return objects;
    }

    /**
     * @return the oreCount
     */
    public int getOreCount()
    {
        return oreCount;
    }

    /**
     * Gets the log id.
     * 
     * @return The log id.
     */
    public int getOreId()
    {
        return log;
    }

    /**
     * @return the replacementRocks
     */
    public int[] getReplacementRocks()
    {
        return replacementRocks;
    }

    /**
     * Gets the required level.
     * 
     * @return The required level.
     */
    public int getRequiredLevel()
    {
        return level;
    }

    /**
     * @return the respawnTimer
     */
    public int getRespawnTimer()
    {
        return respawnTimer;
    }

    public static int getRandomGem(short[] gems)
    {
        return Misc.generatedValue(gems);
    }
}
