package com.asgarniars.rs2.content.skills.runecraft;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Joshua Barry <Ares>
 * @author Dominick Jones <Jones>
 * 
 */
public enum Rune
{

    AIR(556, 1, 5, new short[]
    { 1438, 5527 }), MIND(558, 2, 5.5, new short[]
    { 1448, 5529 }), WATER(555, 5, 6, new short[]
    { 1444, 5531 }), EARTH(557, 9, 6.5, new short[]
    { 1440, 5535 }), FIRE(554, 14, 7, new short[]
    { 1442, 5537 }), BODY(559, 20, 7.5, new short[]
    { 1446, 5533 }), COSMIC(564, 27, 10, new short[]
    { 1454, 5539 }), CHAOS(562, 35, 10, new short[]
    { 1452, 5543 }), ASTRAL(9075, 40, 10, new short[]
    { 9106 }), NATURE(561, 44, 11, new short[]
    { 1462, 5541 }), LAW(563, 54, 12, new short[]
    { 1458, 5545 }), DEATH(560, 65, 13, new short[]
    { 1456, 5547 }), BLOOD(565, 77, 14, new short[]
    { 1450, 5549 }), SOUL(566, 85, 15, new short[]
    { 1460, 5551 });

    private int reward;
    private int levelRequired;
    private double experience;
    private short[] requiredItems;

    /**
     * 
     * @param reward The rune to reward the player.
     * @param essence The essence type the player is crafting with.
     * @param levelRequired The level required to craft this rune type.
     * @param experience The experience gain from crafting this rune type.
     */
    private Rune(int reward, int levelRequired, double experience, short[] requiredItems)
    {
        this.reward = reward;
        this.levelRequired = levelRequired;
        this.experience = experience;
        this.requiredItems = requiredItems;
    }

    public short[] getRequiredItems()
    {
        return requiredItems;
    }

    public double getExperience()
    {
        return experience;
    }

    public int getLevelRequired()
    {
        return levelRequired;
    }

    public int getReward()
    {
        return reward;
    }

    public static Rune forId(int id)
    {
        return runes.get(id);
    }

    public static Map<Integer, Rune> runes = new HashMap<Integer, Rune>();

    static
    {
        for (Rune rune : Rune.values())
        {
            runes.put(rune.getReward(), rune);
        }
    }

}
