package com.asgarniars.rs2.content.skills.agility;

/**
 * Represents the type of action performed.
 * 
 * @author Joshua Barry
 * 
 */
public enum ObstacleAction
{

    CLIMB, MOVE, WALK

}
