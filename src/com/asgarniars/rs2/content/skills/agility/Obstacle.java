package com.asgarniars.rs2.content.skills.agility;

import java.util.HashMap;
import java.util.Map;

import com.asgarniars.rs2.model.Position;

/**
 * Represents an Agility obstacle that must be tackled to gain experience.
 * 
 * TODO: Replace skeletal element values to appropriate figures for :
 * levelRequired, experience.
 * 
 * @author Joshua Barry
 * 
 */
public enum Obstacle
{

    GNOME_LEFT_PIPE_ENTRANCE(154, 1, 746, new Position(0, 2, 0), 7.5),

    GNOME_RIGHT_PIPE_ENTRANCE(4058, 1, 746, new Position(0, 2, 0), 7.5),

    GNOME_BALANCING_ROPE(2312, 1, 762, new Position(2483, 3420, 2), 7.5),

    GNOME_LOG_BALANCE(2295, 1, 762, new Position(2474, 3429, 0), 7.5),

    GNOME_OBSTACLE_NET(2285, 1, 828, new Position(2473, 3424, 1), 7.5),

    GNOME_OBSTACLE_CLIMBING_NET(2286, 1, 828, new Position(2485, 3427, 0), 7.5),

    GNOME_TREE_BRANCH(2313, 1, 828, new Position(2473, 3420, 2), 5),

    GNOME_TOP_TREE_BRANCH(2314, 1, 828, new Position(2487, 3422, 0), 5),

    BARBARIAN_ROPE_SWING(2282, 1, 0, new Position(2551, 3549, 0), 22),

    BARBARIAN_LOG_BALANCE(2294, 1, 762, new Position(2541, 3546, 0), 13.7),

    BARBARIAN_OBSTACLE_NET(20211, 1, 828, new Position(2537, 3546, 1), 8.2),

    BARBARIAN_BALANCING_LEDGE(2302, 1, 756, new Position(2532, 3547, 1), 22),

    BARBARIAN_LADDER(3205, 1, 827, new Position(2532, 3546, 0), 0),

    BARBARIAN_WALL(1948, 1, 839, new Position(2537, 3553), 13.7),

    BARBARIAN_WALL2(1948, 1, 839, new Position(2540, 3553), 13.7),

    BARBARIAN_WALL3(1948, 1, 839, new Position(2543, 3553), 13.7);

    /**
     * The object ID of the 'obstacle'.
     */
    private int objectId;

    /**
     * The level required to tackle the 'obstacle'.
     */
    private int levelRequired;

    /**
     * The animation ID of the 'obstacle'.
     */
    private int animId;

    /**
     * The position we are moving to.
     */
    private Position position;

    /**
     * The experience gained from the obstacle.
     */
    private double experience;

    /**
     * Creates a mapping of 'obstacles' based on their object ID.
     */
    private static Map<Integer, Obstacle> obstacles = new HashMap<Integer, Obstacle>();

    static
    {
        for (Obstacle obstacle : Obstacle.values())
        {
            obstacles.put(obstacle.getObjectId(), obstacle);
        }
    }

    /**
     * 
     * @param objectId The object ID of the 'obstacle'.
     * @param levelRequired The level required to tackle the 'obstacle'.
     * @param animationId The animation ID of the 'obstacle'.
     * @param position The position we are moving to.
     * @param experience The experience gained from the obstacle.
     */
    private Obstacle(int objectId, int levelRequired, int animationId, Position position, double experience)
    {
        this.objectId = objectId;
        this.animId = animationId;
        this.position = position;
        this.experience = experience;

    }

    /**
     * 
     * @return The object ID variable.
     */
    public int getObjectId()
    {
        return objectId;
    }

    /**
     * 
     * @return The level required variable.
     */
    public int getLevelRequired()
    {
        return levelRequired;
    }

    /**
     * 
     * @return The animation variable.
     */
    public int getAnimId()
    {
        return animId;
    }

    /**
     * 
     * @return The position variable.
     */
    public Position getPosition()
    {
        return position;
    }

    /**
     * 
     * @return The experience variable.
     */
    public double getExp()
    {
        return experience;
    }

    /**
     * 
     * @param id The ID of the object we are attempting to tackle.
     * @return The obstacle with the corresponding object ID.
     */
    public static Obstacle forObjectId(int id)
    {
        return obstacles.get(id);
    }

}
