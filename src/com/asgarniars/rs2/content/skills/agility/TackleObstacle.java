package com.asgarniars.rs2.content.skills.agility;

import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;
import com.asgarniars.rs2.util.Misc;

/**
 * 
 * @author Joshua Barry
 * @author AkZu
 * 
 */
public class TackleObstacle
{

    /**
     * 
     * @param player The player who is interacting with the obstacle.
     * @param obstacle The obstacle interacted with.
     * @param ticks The ticks prior to the result.
     * @param messages The messages sent during the process.
     */
    public static void obstacleForceWalk(final Player player, final Obstacle obstacle, int ticks, final String... messages)
    {
        player.setAttribute("tempStandAnim", obstacle.getAnimId());
        player.setAttribute("tempWalkAnim", obstacle.getAnimId());
        player.setAttribute("tempRun", player.getAttribute("isRunning"));
        player.removeAttribute("isRunning");
        player.setAppearanceUpdateRequired(true);
        player.getMovementHandler().setRunPath(false);
        player.getMovementHandler().walk(obstacle.getPosition());
        player.getActionSender().sendMessage(messages[0]);
        World.submit(new Task(ticks)
        {
            @Override
            protected void execute()
            {
                player.setAttribute("isRunning", player.getAttribute("tempRun"));
                player.removeAttribute("tempRun");
                player.removeAttribute("tempStandAnim");
                player.removeAttribute("tempWalkAnim");
                player.setAppearanceUpdateRequired(true);
                if (messages.length > 1)
                {
                    player.getActionSender().sendMessage(messages[1]);
                }
                player.getSkill().addExperience(Skill.AGILITY, obstacle.getExp());
                this.stop();
            }
        });

    }

    /**
     * 
     * @param player
     * @param obstacle
     * @param ticks
     * @param xOffset
     * @param yOffset
     * @param speed
     * @param altspeed
     * @param direction
     */
    public static void obstacleForceMovement(final Player player, final Obstacle obstacle, int ticks, int speed, int altspeed, int direction, final String... messages)
    {

        if (!player.canWalk())
        {
            return;
        }

        player.setCanWalk(false);

        player.getActionSender().sendMessage(messages[0]);

        player.getUpdateFlags().sendFaceToDirection(new Position(player.getClickX(), player.getClickY(), player.getPosition().getZ()));
        player.getUpdateFlags().sendAnimation(obstacle.getAnimId());
        player.getUpdateFlags().sendForceMovement(obstacle.getPosition().getX(), obstacle.getPosition().getY(), speed, altspeed, direction);
        World.submit(new Task(ticks)
        {

            @Override
            protected void execute()
            {
                player.teleport(new Position(player.getPosition().getX(), player.getPosition().getY() + obstacle.getPosition().getY()));
                player.getSkill().addExperience(Skill.AGILITY, obstacle.getExp());
                World.submit(new Task(1)
                {
                    @Override
                    protected void execute()
                    {
                        if (messages.length > 1)
                            player.getActionSender().sendMessage(messages[1]);
                        player.setCanWalk(true);
                        this.stop();
                    }
                });
                this.stop();

            }

        });
    }

    /**
     * 
     * @param player The player who is interacting with the obstacle.
     * @param obstacle The obstacle interacted with.
     * @param ticks The ticks prior to the result.
     * @param altPos An alternate position to teleport to.
     * @param messages The messages sent during the process.
     */
    public static void obstacleTeleport(final Player player, final Obstacle obstacle, int ticks, final Position altPos, final String... messages)
    {

        if (!player.canWalk())
        {
            return;
        }

        player.setCanWalk(false);

        player.getActionSender().sendMessage(messages[0]);
        player.getUpdateFlags().sendAnimation(obstacle.getAnimId());

        World.submit(new Task(ticks)
        {
            @Override
            protected void execute()
            {
                Position pos = altPos == null ? obstacle.getPosition() : altPos;

                player.clipTeleport(pos, 1);
                player.getSkill().addExperience(Skill.AGILITY, obstacle.getExp());
                World.submit(new Task(1)
                {
                    @Override
                    protected void execute()
                    {
                        if (messages.length > 1)
                            player.getActionSender().sendMessage(messages[1]);
                        player.setCanWalk(true);
                        this.stop();
                    }
                });
                this.stop();
            }
        });

    }

    /**
     * 
     * @param player The player in the middle of the obstacle pipe.
     * @param ticks The ticks before exiting the obstacle pipe.
     */
    public static void obstaclePipeExit(final Player player, int ticks, final byte xOffset, final byte yOffset)
    {

        // first we move out of the pipe.
        player.getUpdateFlags().sendForceMovement(xOffset, yOffset, 20, 40, Misc.EAST);

        World.submit(new Task(ticks)
        {

            @Override
            protected void execute()
            {
                // next we 'teleport' the player to set the new world position.
                player.teleport(new Position(player.getPosition().getX(), player.getPosition().getY() + yOffset, player.getPosition().getZ()));
                // last, we play the exiting animation.
                player.getUpdateFlags().sendAnimation(748);
                this.stop();

            }

        });
    }

    /**
     * 
     * @param player
     * @param obstacle
     */
    public static void updateCourseState(Player player, Obstacle obstacle, String courseNameState)
    {
        if (player.getAttribute(courseNameState) == null)
            player.setAttribute(courseNameState, 1);
        else
            player.setAttribute(courseNameState, ((Integer) player.getAttribute(courseNameState)) + 1);
        if ((Integer) player.getAttribute(courseNameState) > 7)
            player.removeAttribute(courseNameState);

    }
}
