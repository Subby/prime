package com.asgarniars.rs2.content.skills.magic;

import com.asgarniars.rs2.action.Action;
import com.asgarniars.rs2.action.impl.TeleportationAction;
import com.asgarniars.rs2.action.impl.TeleportationAction.TeleportType;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.Position;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class Teleport
{

    /* Kingdoms */
    public static final Position FIGHT_PITS = new Position(2399, 5178, 0);
    public static final Position DUEL_ARENA = new Position(3367, 3268, 0);

    /* Skilling Areas */
    public static final Position CATHERBY = new Position(2804, 3434, 0);
    public static final Position GNOME_AGILITY_COURSE = new Position(2480, 3424, 0);

    /* Wilderness Areas */
    public static final Position EDGEVILLE_WILDERNESS = new Position(3087, 3516, 0);
    public static final Position MAGE_ARENA = new Position(2545, 4715 , 0);

    /**
     * 
     * @param entity
     * @param type
     * @param position
     * @param offset
     */
    public static void initiate(Entity entity, TeleportType type, Position position, int offset)
    {
        System.out.println("initiated");
        Action action = new TeleportationAction(entity, type, position, offset);
        entity.getActionDeque().addAction(action);
    }

    public static void initiate(Entity entity, Entity caster, TeleportType type, Position position, int offset)
    {
        System.out.println("initiated");
        Action action = new TeleportationAction(entity, caster, type, position, offset);
        entity.getActionDeque().addAction(action);
    }

}
