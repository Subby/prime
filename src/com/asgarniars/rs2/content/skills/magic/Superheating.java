package com.asgarniars.rs2.content.skills.magic;

import java.util.HashMap;
import java.util.Map;

import com.asgarniars.rs2.content.combat.magic.Magic;
import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;
import com.asgarniars.rs2.util.Misc;

/**
 * 
 * @author AkZu & Jacob
 * 
 */
public class Superheating
{

    private final static int BRONZE = 2349, IRON = 2351, STEEL = 2353, SILVER = 2355, GOLD = 2357, MITHRIL = 2359, ADAMANTITE = 2361, RUNITE = 2363;

    private enum BarsAndOres
    {
        // barId, required items/amounts, smithing lvl req, xp rewarded
        BRONZE(2349, new int[]
        { 436, 1, 438, 1 }, 1, 6.2), IRON(2351, new int[]
        { 440, 1 }, 15, 12.5), SILVER(2355, new int[]
        { 442, 1 }, 20, 13.7), STEEL(2353, new int[]
        { 440, 1, 453, 2 }, 30, 17.5), GOLD(2357, new int[]
        { 444, 1 }, 40, 22.5), // gold
        // smith
        // gaunts =
        // 56.2xp
        MITHRIL(2359, new int[]
        { 447, 1, 453, 4 }, 50, 30.0), ADAMANTITE(2361, new int[]
        { 449, 1, 453, 6 }, 70, 37.5), RUNITE(2363, new int[]
        { 451, 1, 453, 8 }, 85, 50.0);

        int barId;
        int[] reqItems;
        int lvlReq;
        double xpReward;

        private BarsAndOres(int barId, int[] reqItems, int lvlReq, double xpReward)
        {
            this.barId = barId;
            this.reqItems = reqItems;
            this.lvlReq = lvlReq;
            this.xpReward = xpReward;
        }

        public int getBarId()
        {
            return barId;
        }

        public int[] getReqItems()
        {
            return reqItems;
        }

        public int getLvlReq()
        {
            return lvlReq;
        }

        public double getXp()
        {
            return xpReward;
        }

        public static final Map<Integer, BarsAndOres> map = new HashMap<Integer, BarsAndOres>();

        public static BarsAndOres forId(int id)
        {
            return map.get(id);
        }

        static
        {
            for (BarsAndOres bar : BarsAndOres.values())
            {
                map.put(bar.getBarId(), bar);
            }
        }
    }

    public static void handleSuperheating(final Player player, int spellId, int slot)
    {

        final Item item = player.getInventory().get(slot);

        if (item == null)
            return;

        if (!(Boolean) player.getAttribute("canCastMagicOnItems"))
            return;

        if (player.getSkill().getLevel()[Skill.MAGIC] < 43)
        {
            player.getActionSender().sendMessage("You need a magic level of atleast 43 to cast this spell.");
            return;
        }

        if (player.getInventory().getItemContainer().getById(561) == null)
        {
            player.getActionSender().sendMessage("You do not have enough nature runes to cast this spell.");
            return;
        }

        if (!Magic.getInstance().hasStaff(player, 554))
            if (player.getInventory().getItemContainer().getById(554) == null || player.getInventory().getItemContainer().getById(554) != null && player.getInventory().getItemContainer().getById(554).getCount() < 4)
            {
                player.getActionSender().sendMessage("You do not have enough fire runes to cast this spell.");
                return;
            }

        int barType = 0;

        switch (item.getId())
        {
        case 436:
        case 438:
            barType = BRONZE;
            if (player.getSkill().getLevel()[Skill.SMITHING] < BarsAndOres.forId(barType).getLvlReq())
            {
                player.getActionSender().sendMessage("You need a smithing level of atleast " + BarsAndOres.forId(barType).getLvlReq() + " to superheat " + ItemManager.getInstance().getItemName(barType).toLowerCase() + "s.");
                return;
            }
            if (!player.getInventory().removeItems(BarsAndOres.forId(barType).getReqItems(), true))
                return;
            break;
        case 440:
            if (player.getInventory().getItemContainer().contains(453))
            {
                barType = STEEL;
                if (player.getSkill().getLevel()[Skill.SMITHING] < BarsAndOres.forId(barType).getLvlReq())
                {
                    player.getActionSender().sendMessage("You need a smithing level of atleast " + BarsAndOres.forId(barType).getLvlReq() + " to superheat " + ItemManager.getInstance().getItemName(barType).toLowerCase() + "s.");
                    return;
                }
                if (!player.getInventory().removeItems(BarsAndOres.forId(barType).getReqItems(), true))
                    return;
            }
            else
            {
                barType = IRON;
                if (player.getSkill().getLevel()[Skill.SMITHING] < BarsAndOres.forId(barType).getLvlReq())
                {
                    player.getActionSender().sendMessage("You need a smithing level of atleast " + BarsAndOres.forId(barType).getLvlReq() + " to superheat " + ItemManager.getInstance().getItemName(barType).toLowerCase() + "s.");
                    return;
                }
                if (!player.getInventory().removeItems(BarsAndOres.forId(barType).getReqItems(), true))
                    return;
            }
            break;
        case 442:
            barType = SILVER;
            if (player.getSkill().getLevel()[Skill.SMITHING] < BarsAndOres.forId(barType).getLvlReq())
            {
                player.getActionSender().sendMessage("You need a smithing level of atleast " + BarsAndOres.forId(barType).getLvlReq() + " to superheat " + ItemManager.getInstance().getItemName(barType).toLowerCase() + "s.");
                return;
            }
            if (!player.getInventory().removeItems(BarsAndOres.forId(barType).getReqItems(), true))
                return;
            break;
        case 444:
            barType = GOLD;
            if (player.getSkill().getLevel()[Skill.SMITHING] < BarsAndOres.forId(barType).getLvlReq())
            {
                player.getActionSender().sendMessage("You need a smithing level of atleast " + BarsAndOres.forId(barType).getLvlReq() + " to superheat " + ItemManager.getInstance().getItemName(barType).toLowerCase() + "s.");
                return;
            }
            if (!player.getInventory().removeItems(BarsAndOres.forId(barType).getReqItems(), true))
                return;
            break;
        case 447:
            barType = MITHRIL;
            if (player.getSkill().getLevel()[Skill.SMITHING] < BarsAndOres.forId(barType).getLvlReq())
            {
                player.getActionSender().sendMessage("You need a smithing level of atleast " + BarsAndOres.forId(barType).getLvlReq() + " to superheat " + ItemManager.getInstance().getItemName(barType).toLowerCase() + "s.");
                return;
            }
            if (!player.getInventory().removeItems(BarsAndOres.forId(barType).getReqItems(), true))
                return;
            break;
        case 449:
            barType = ADAMANTITE;
            if (player.getSkill().getLevel()[Skill.SMITHING] < BarsAndOres.forId(barType).getLvlReq())
            {
                player.getActionSender().sendMessage("You need a smithing level of atleast " + BarsAndOres.forId(barType).getLvlReq() + " to superheat " + ItemManager.getInstance().getItemName(barType).toLowerCase() + "s.");
                return;
            }
            if (!player.getInventory().removeItems(BarsAndOres.forId(barType).getReqItems(), true))
                return;
            break;
        case 451:
            barType = RUNITE;
            if (player.getSkill().getLevel()[Skill.SMITHING] < BarsAndOres.forId(barType).getLvlReq())
            {
                player.getActionSender().sendMessage("You need a smithing level of atleast " + BarsAndOres.forId(barType).getLvlReq() + " to superheat " + ItemManager.getInstance().getItemName(barType).toLowerCase() + "s.");
                return;
            }
            if (!player.getInventory().removeItems(BarsAndOres.forId(barType).getReqItems(), true))
                return;
            break;
        default:
            return;
        }

        player.setAttribute("canCastMagicOnItems", false);

        player.getUpdateFlags().sendAnimation(722);
        player.getUpdateFlags().sendGraphic(148, 0, 0);
        player.getActionSender().sendSound(204, 100, 0);
        Magic.getInstance().removeRunes(player, new int[]
        { -1, 554, 4, 561, 1 });
        player.getInventory().addItem(new Item(BarsAndOres.forId(barType).getBarId()));
        player.getSkill().addExperience(Skill.MAGIC, 53);
        player.getSkill().addExperience(Skill.SMITHING, BarsAndOres.forId(barType).getXp());
        player.getActionSender().sendMessage("You magically smelt your ore and receive " + Misc.getArticle(ItemManager.getInstance().getItemName(barType)) + " " + ItemManager.getInstance().getItemName(barType).toLowerCase() + ".");

        World.submit(new Task(2)
        {
            @Override
            protected void execute()
            {
                player.setAttribute("canCastMagicOnItems", true);
                stop();
            }
        });
    }
}
