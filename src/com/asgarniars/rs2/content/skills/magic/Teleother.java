package com.asgarniars.rs2.content.skills.magic;

import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.model.players.Player.TeleotherStage;
import com.asgarniars.rs2.task.Task;
import com.asgarniars.rs2.util.Language;
import com.asgarniars.rs2.util.Misc;

/**
 * 
 * @author AkZu
 * 
 *         TODO Accept aid. CHECK BUGS!!
 * 
 */
public class Teleother
{

    private static final String forCityName(int spellId)
    {
        if (spellId == 12425)
            return "Lumbridge";
        else if (spellId == 12435)
            return "Falador";
        else if (spellId == 12455)
            return "Camelot";
        else
            return "";
    }

    public static void handleDecline(final Player player)
    {

        int playerId = (Integer) player.getAttribute("teleotherPartnerId");

        Player caster = World.getPlayer(playerId);
        if (caster != null)
        {
            caster.getActionSender().removeInterfaces();
            caster.getActionSender().sendMessage(player.getUsername() + " has declined your teleother request.");
        }

        player.getActionSender().sendMessage("You cancel the teleother request.");

        player.setTeleotherStage(TeleotherStage.WAITING);
        player.removeAttribute("teleotherPartnerId");
        player.removeAttribute("teleotherSpellId");

        player.getActionSender().removeInterfaces();
    }

    public static void handleAccept(final Player player, final Player victim, final int spellId)
    {

        // not sure if needed
        if (victim.getTeleotherStage() != TeleotherStage.RECEIVED_REQUEST)
            return;

        switch (spellId)
        {
        case 12425:
            player.getSkill().addExperience(Skill.MAGIC, 84);
            player.getInventory().removeItems(new int[]
            { 566, 1, 563, 1, 557, 1 }, true);
            victim.getActionSender().removeInterfaces();
            victim.getUpdateFlags().sendGraphic(342, 0, 100);
            victim.getUpdateFlags().sendAnimation(1816);
            World.submit(new Task(3)
            {
                @Override
                protected void execute()
                {
                    victim.clipTeleport(3223, 3219, 0, 3);
                    victim.getActionSender().sendMessage(player.getUsername() + " teleports you to Lumbridge.");
                    victim.getUpdateFlags().sendAnimation(-1);
                    this.stop();
                }
            });
            break;
        case 12435:
            player.getSkill().addExperience(Skill.MAGIC, 92);
            player.getInventory().removeItems(new int[]
            { 566, 1, 555, 1, 563, 1 }, true);
            victim.getActionSender().removeInterfaces();
            victim.getUpdateFlags().sendGraphic(342, 0, 100);
            victim.getUpdateFlags().sendAnimation(1816);
            World.submit(new Task(3)
            {
                @Override
                protected void execute()
                {
                    victim.clipTeleport(2965, 3381, 0, 4);
                    victim.getActionSender().sendMessage(player.getUsername() + " teleports you to Falador.");
                    victim.getUpdateFlags().sendAnimation(-1);
                    this.stop();
                }
            });
            break;
        case 12455:
            player.getSkill().addExperience(Skill.MAGIC, 100);
            player.getInventory().removeItems(new int[]
            { 566, 2, 563, 1 }, true);
            victim.getActionSender().removeInterfaces();
            victim.getUpdateFlags().sendGraphic(342, 0, 100);
            victim.getUpdateFlags().sendAnimation(1816);
            World.submit(new Task(3)
            {
                @Override
                protected void execute()
                {
                    victim.clipTeleport(2757, 3478, 0, 3);
                    victim.getActionSender().sendMessage(player.getUsername() + " teleports you to Camelot.");
                    victim.getUpdateFlags().sendAnimation(-1);
                    this.stop();
                }
            });
            break;

        default:
            break;
        }
    }

    public static void handleFunctions(final Player player, final Player victim, final int spellId)
    {

        player.getMovementHandler().reset();

        player.getActionSender().removeMapFlag();

        // cant cast spell over 11 tiles
        if (Misc.getDistance(player.getPosition(), victim.getPosition()) > 10)
            return;

        switch (spellId)
        {
        case 12425:
            if (player.getSkill().getLevel()[Skill.MAGIC] < 74)
            {
                player.getActionSender().sendMessage("You need a magic level of atleast 74 to cast this spell.");
                return;
            }
            if (!player.getInventory().removeItems(new int[]
            { 566, 1, 563, 1, 557, 1 }, false))
                return;
            break;
        case 12435:
            if (player.getSkill().getLevel()[Skill.MAGIC] < 82)
            {
                player.getActionSender().sendMessage("You need a magic level of atleast 82 to cast this spell.");
                return;
            }
            if (!player.getInventory().removeItems(new int[]
            { 566, 1, 555, 1, 563, 1 }, false))
                return;
            break;
        case 12455:
            if (player.getSkill().getLevel()[Skill.MAGIC] < 90)
            {
                player.getActionSender().sendMessage("You need a magic level of atleast 90 to cast this spell.");
                return;
            }
            if (!player.getInventory().removeItems(new int[]
            { 566, 2, 563, 1 }, false))
                return;
            break;
        }

        player.getUpdateFlags().sendFaceToDirection(victim.getPosition());

        if (victim.getMinigame() != null)
        {
            player.getActionSender().sendMessage("You can't teleother other player who's playing a minigame.");
            return;
        }

        if (victim.isBusy())
        {
            player.getActionSender().sendMessage(Language.IS_BUSY);
            return;
        }

        if (victim.getAttribute("cant_teleport") != null)
            return;

        player.getUpdateFlags().sendAnimation(1818);
        player.getUpdateFlags().sendGraphic(343, 0, 100);
        player.getActionSender().sendMessage("Sending teleother request to " + victim.getUsername() + ".");

        World.submit(new Task(3)
        {
            @Override
            protected void execute()
            {
                // for doing actions for the caster such as rewarding
                // with xp
                player.setAttribute("teleotherPartnerId", victim.getIndex());
                victim.setAttribute("teleotherPartnerId", player.getIndex());
                victim.setAttribute("teleotherSpellId", spellId);

                player.setTeleotherStage(TeleotherStage.SEND_REQUEST);
                victim.setTeleotherStage(TeleotherStage.RECEIVED_REQUEST);
                victim.getActionSender().sendString(player.getUsername(), 12558);
                victim.getActionSender().sendString(forCityName(spellId), 12560);
                victim.getActionSender().sendInterface(12468);
                this.stop();
            }
        });
    }
}
