package com.asgarniars.rs2.content.skills.magic;

import com.asgarniars.rs2.content.combat.magic.Magic;
import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.area.Areas;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.model.players.Player.MagicBookTypes;
import com.asgarniars.rs2.task.Task;
import com.asgarniars.rs2.util.Language;
import com.asgarniars.rs2.util.Misc;

/**
 * Handles everything related to teleporting [11/10/2012] - DD/MM/YYYY
 * 
 * TODO: Later on........ combination runes
 * 
 * ARGENT: Home teleport - deny the teleport if walked/performed any actions.
 * 
 * @author AkZu & Jacob
 * 
 */

public class Teleportation
{

    private static Teleportation instance = null;

    public static Teleportation getInstance()
    {
        if (instance == null)
        {
            instance = new Teleportation();
        }
        return instance;
    }

    // buttonId, x, y, z,
    // "size" = (Distance from coords, how big rectangle u can tele to)
    // if x and y is both 0 and size is 3, you can be teled to
    // -3x, -3y to +3x, +3y :P
    // spellbook required to use
    // lvl required to use
    // xp rewarded
    private static final Object[][] TELEPORTS =
    {
    { 1164, 3213, 3429, 0, 6, MagicBookTypes.MODERN, 25, 35.0 }, // Varrock
            { 1167, 3223, 3219, 0, 3, MagicBookTypes.MODERN, 31, 41.0 }, // Lumby
            { 1170, 2965, 3381, 0, 4, MagicBookTypes.MODERN, 37, 48.0 }, // Falador
            { 1174, 2757, 3478, 0, 3, MagicBookTypes.MODERN, 45, 55.5 }, // Camelot
            { 1540, 2662, 3306, 0, 4, MagicBookTypes.MODERN, 51, 61.0 }, // Ardougne
            // -quest
            { 1541, 2547, 3114, 2, 3, MagicBookTypes.MODERN, 58, 68.0 }, // Watchtower
            // -quest
            { 7455, 2892, 3678, 0, 3, MagicBookTypes.MODERN, 61, 68.0 }, // Trollheim
            // -quest
            { 18470, 2797, 2798, 1, 2, MagicBookTypes.MODERN, 64, 76.0 }, // Ape
            // Atoll
            // -quest

            { 13035, 3098, 9883, 0, 3, MagicBookTypes.ANCIENT, 54, 64.0 }, // Paddewwa
            { 13045, 3319, 3336, 0, 3, MagicBookTypes.ANCIENT, 60, 70.0 }, // Senntisten
            { 13053, 3495, 3474, 0, 3, MagicBookTypes.ANCIENT, 66, 76.0 }, // Kharyrll
            { 13061, 3001, 3470, 0, 2, MagicBookTypes.ANCIENT, 72, 82.0 }, // Lassar
            { 13069, 3035, 3701, 0, 2, MagicBookTypes.ANCIENT, 78, 88.0 }, // Dareyaak
            { 13079, 3161, 3673, 0, 3, MagicBookTypes.ANCIENT, 84, 94.0 }, // Carrallangar
            { 13087, 3288, 3886, 0, 3, MagicBookTypes.ANCIENT, 90, 100.0 }, // Annakarl
            { 13095, 2971, 3876, 0, 5, MagicBookTypes.ANCIENT, 96, 106.0 } // Ghorrock
    };

    // supports for 4 items (runes) as for teleports only,
    // combat spells will do xml perhaps
    private static int[][] RUNES_REQUIRED =
    {
    { 1164, 554, 1, 556, 3, 563, 1 },
    { 1167, 557, 1, 556, 3, 563, 1 },
    { 1170, 555, 1, 556, 3, 563, 1 },
    { 1174, 556, 5, 563, 1 },
    { 1540, 555, 2, 563, 2 },
    { 1541, 557, 2, 563, 2 },
    { 7455, 554, 2, 563, 2 },
    { 18470, 554, 2, 555, 2, 563, 2, 1963, 1 },

    { 13035, 563, 2, 554, 1, 556, 1 },
    { 13045, 563, 2, 566, 1 },
    { 13053, 563, 2, 565, 1 },
    { 13061, 563, 2, 555, 4 },
    { 13069, 563, 2, 554, 3, 556, 2 },
    { 13079, 563, 2, 566, 2 },
    { 13087, 563, 2, 565, 2 },
    { 13095, 563, 2, 555, 8 } };

    public void activateTeleportButton(final Player player, int buttonId)
    {
        // check for mass-clicking
        if (player.getAttribute("cant_teleport") != null)
            return;

        if (player.getMinigame() != null)
        {
            player.getActionSender().sendMessage("You can't teleport whilst playing a minigame.");
            return;
        }

        if (player.getAttribute("teleBlocked") != null)
        {
            if (System.currentTimeMillis() - (Long) player.getAttribute("teleBlocked") < 300000)
            {
                player.getActionSender().sendMessage("You are teleport blocked and can't teleport!");
                return;
            }
            else
            {
                player.removeAttribute("teleBlocked");
            }
        }

        if (buttonId == 12856)
        {
            if (Areas.getWildernessLevel(player) > 20)
            {
                player.getActionSender().sendMessage(Language.CANT_TELE_20);
                return;
            }
            if (player.getAttribute("homeTeleTimer") != null && System.currentTimeMillis() - (Long) player.getAttribute("homeTeleTimer") < 1800000)
            {
                int elapsedTime = (int) ((1800 - ((System.currentTimeMillis() - (Long) player.getAttribute("homeTeleTimer")) / 1000)) / 60);
                player.getActionSender().sendMessage("You need to wait atleast " + elapsedTime + " " + Misc.trimMinutes(elapsedTime) + " before casting this spell again.");
                return;
            }
            player.getUpdateFlags().sendGraphic(392, 0, 0);
            player.getUpdateFlags().sendAnimation(1979, 0);
            player.setAttribute("cant_teleport", 0);
            World.submit(new Task(4)
            {

                @Override
                protected void execute()
                {
                    player.clipTeleport(3086, 3496, 0, 3);
                    this.stop();
                }
            });
            World.submit(new Task(4)
            {

                @Override
                protected void execute()
                {
                    player.removeAttribute("cant_teleport");
                    player.setAttribute("homeTeleTimer", System.currentTimeMillis());
                    this.stop();
                }
            });
        }

        if (buttonId == 9845)
        {
            if (Areas.getWildernessLevel(player) > 20)
            {
                player.getActionSender().sendMessage(Language.CANT_TELE_20);
                return;
            }
            if (player.getAttribute("homeTeleTimer") != null && System.currentTimeMillis() - (Long) player.getAttribute("homeTeleTimer") < 1800000)
            {
                int elapsedTime = (int) ((1800 - ((System.currentTimeMillis() - (Long) player.getAttribute("homeTeleTimer")) / 1000)) / 60);
                player.getActionSender().sendMessage("You need to wait atleast " + elapsedTime + " " + Misc.trimMinutes(elapsedTime) + " before casting this spell again.");
                return;
            }
            player.setAttribute("cant_teleport", 0);
            player.getUpdateFlags().sendGraphic(800, 0, 0);
            player.getUpdateFlags().sendAnimation(4847, 0);
            World.submit(new Task(4)
            {

                @Override
                protected void execute()
                {
                    player.getUpdateFlags().sendAnimation(4850, 0);
                    this.stop();
                    World.submit(new Task(2)
                    {

                        @Override
                        protected void execute()
                        {
                            player.getUpdateFlags().sendGraphic(801, 0, 0);
                            World.submit(new Task(1)
                            {

                                @Override
                                protected void execute()
                                {
                                    player.getUpdateFlags().sendGraphic(802, 30, 0);
                                    World.submit(new Task(1)
                                    {

                                        @Override
                                        protected void execute()
                                        {
                                            player.getUpdateFlags().sendAnimation(4853, 0);
                                            World.submit(new Task(2)
                                            {

                                                @Override
                                                protected void execute()
                                                {
                                                    player.getUpdateFlags().sendGraphic(803, 0, 0);
                                                    player.getUpdateFlags().sendAnimation(4855, 0);
                                                    World.submit(new Task(4)
                                                    {

                                                        @Override
                                                        protected void execute()
                                                        {
                                                            player.getUpdateFlags().sendGraphic(804, 0, 0);
                                                            player.getUpdateFlags().sendAnimation(4857, 0);
                                                            World.submit(new Task(3)
                                                            {

                                                                @Override
                                                                protected void execute()
                                                                {
                                                                    player.clipTeleport(3223, 3219, 0, 3);
                                                                    World.submit(new Task(2)
                                                                    {

                                                                        @Override
                                                                        protected void execute()
                                                                        {
                                                                            player.removeAttribute("cant_teleport");

                                                                            player.setAttribute("homeTeleTimer", System.currentTimeMillis());
                                                                            this.stop();
                                                                        }
                                                                    });
                                                                    this.stop();
                                                                }
                                                            });
                                                            this.stop();
                                                        }
                                                    });
                                                    this.stop();
                                                }
                                            });
                                            this.stop();
                                        }
                                    });
                                    this.stop();
                                }
                            });
                            this.stop();
                        }
                    });
                }
            });
        }

        for (int i = 0; i < TELEPORTS.length; i++)
        {
            if (buttonId == (Integer) TELEPORTS[i][0] && player.getMagicBookType() == TELEPORTS[i][5])
            {
                final int index = i;
                if (Areas.getWildernessLevel(player) > 20)
                {
                    player.getActionSender().sendMessage(Language.CANT_TELE_20);
                    return;
                }
                if (!Magic.getInstance().hasRequiredLevel(player, (Integer) TELEPORTS[index][6]))
                    return;
                if (!Magic.getInstance().removeRunes(player, RUNES_REQUIRED[i]))
                    return;
                if (player.getMagicBookType() == MagicBookTypes.MODERN)
                {
                    player.getUpdateFlags().sendGraphic(111, 0, 100);
                    player.getUpdateFlags().sendAnimation(714, 7);
                    player.getActionSender().sendSound(202, 100, 10);
                }
                else if (player.getMagicBookType() == MagicBookTypes.ANCIENT)
                {
                    player.getUpdateFlags().sendGraphic(392, 0, 0);
                    player.getUpdateFlags().sendAnimation(1979, 0);
                    player.getActionSender().sendSound(1048, 100, 0);
                }

                player.setAttribute("cant_teleport", 0);
                World.submit(new Task((player.getMagicBookType() == MagicBookTypes.MODERN ? 3 : 4))
                {

                    @Override
                    protected void execute()
                    {
                        player.clipTeleport((Integer) TELEPORTS[index][1], (Integer) TELEPORTS[index][2], (Integer) TELEPORTS[index][3], (Integer) TELEPORTS[index][4]);
                        if (player.getMagicBookType() == MagicBookTypes.MODERN)
                            player.getUpdateFlags().sendAnimation(715, 1);
                        this.stop();
                    }
                });

                World.submit(new Task((player.getMagicBookType() == MagicBookTypes.MODERN ? 6 : 5))
                {
                    @Override
                    protected void execute()
                    {
                        player.getSkill().addExperience(Skill.MAGIC, (Double) TELEPORTS[index][7]);
                        player.removeAttribute("cant_teleport");
                        this.stop();
                    }
                });
                break;
            }
        }
    }
}
