package com.asgarniars.rs2.content.skills.cook;

import java.util.HashMap;
import java.util.Map;

import com.asgarniars.rs2.model.items.Item;

/**
 * 
 * @author Joshua Barry
 * @author Dominick Jones <DJ>
 * 
 */
public enum Cookable
{

    /**
     * Raw, Cooked, Level, XP, ExtraIng? (Guide) Read about cooking.txt for
     * guidance on these foods.
     */

    /**
     * Meat
     */
    BEEF(new Item(2132, 1), new Item(2142, 1), 1, 30, new Item[]
    {}), CHICKEN(new Item(2138, 1), new Item(2140, 1), 1, 30, new Item[]
    {}), UGTHANKI(new Item(1859, 1), new Item(1861, 1), 1, 40, new Item[]
    {}), RABBIT(new Item(3226, 1), new Item(3228, 1), 1, 30, new Item[]
    {}), ROASTED_BIRD_MEAT(new Item(9978, 1), new Item(9980, 1), 11, 62, new Item[]
    {}), /*
          * ROASTED_RABBIT ( new Item ( 3226 , 1), new Item ( 7223 , 1), 16, 72,
          * new Item [] { }),
          */SPIDER_ON_STICK(new Item(6291, 1), new Item(6293, 1), 16, 80, new Item[]
    { new Item(6305) }), SPIDER_ON_SHAFT(new Item(6291, 1), new Item(6295, 1), 16, 80, new Item[]
    { new Item(52, 1) }), CRAB_MEAT(new Item(7518, 1), new Item(7521, 1), 21, 100, new Item[]
    {}), ROASTED_BEAST_MEAT(new Item(9986, 1), new Item(9988, 1), 21, 82, new Item[]
    {}), CHOMPY(new Item(2876, 1), new Item(2878, 1), 21, 82, new Item[]
    {}), JUBBLY(new Item(7566, 1), new Item(7568, 1), 41, 160, new Item[]
    {}), OOMLIE(new Item(2337, 1), new Item(2343, 1), 41, 160, new Item(2339, 1)
    {
    }),

    /**
     * Fish
     */
    SHRIMP(new Item(317, 1), new Item(315, 1), 1, 30, new Item[]
    {}), KARAMBWANJI(new Item(3150, 1), new Item(3151, 1), 1, 10, new Item[]
    {}), SARDINE(new Item(327, 1), new Item(325, 1), 1, 40, new Item[]
    {}), ANCHOVIES(new Item(321, 1), new Item(319, 1), 1, 30, new Item[]
    {}),
    /*
     * KARAMBWAN(new Item(3142, 1), new Item(3144, 1), 1, 80, new Item[] {}),
     * This is the Poison Karambwan which can only be made before completing Tai
     * Bwo Wannai Trio, once this quest is completed you can cook a Karambwan
     * properly.
     */
    HERRING(new Item(345, 1), new Item(347, 1), 5, 50, new Item[]
    {}), MACKEREL(new Item(353, 1), new Item(355, 1), 10, 60, new Item[]
    {}), TROUT(new Item(335, 1), new Item(333, 1), 15, 70, new Item[]
    {}), COD(new Item(341, 1), new Item(339, 1), 18, 75, new Item[]
    {}), PIKE(new Item(349, 1), new Item(351, 1), 20, 80, new Item[]
    {}), SALMON(new Item(331, 1), new Item(329, 1), 25, 90, new Item[]
    {}), SLIMY_EEL(new Item(3379, 1), new Item(3381, 1), 28, 95, new Item[]
    {}), TUNA(new Item(359, 1), new Item(361, 1), 30, 100, new Item[]
    {}),
    /*
     * KARAMBWAN(new Item(3142, 1), new Item(3144, 1), 30, 190, new Item[] {}),
     * This is the cooked Karambwan which can only be made after completing Tai
     * Bwo Wannai Trio, if the quest isn't completed the outcome is a posion
     * Karambwan.
     */
    RAINBOW_FISH(new Item(10138, 1), new Item(10136, 1), 35, 110, new Item[]
    {}), CAVE_EEL(new Item(5001, 1), new Item(5003, 1), 38, 115, new Item[]
    {}), LOBSTER(new Item(377, 1), new Item(379, 1), 40, 120, new Item[]
    {}), BASS(new Item(363, 1), new Item(365, 1), 43, 130, new Item[]
    {}), SWORDFISH(new Item(371, 1), new Item(373, 1), 45, 140, new Item[]
    {}), LAVA_EEL(new Item(2148, 1), new Item(2149, 1), 53, 30, new Item[]
    {}), MONKFISH(new Item(7944, 1), new Item(7946, 1), 62, 150, new Item[]
    {}), SHARK(new Item(383, 1), new Item(385, 1), 80, 210, new Item[]
    {}), SEA_TURTLE(new Item(395, 1), new Item(397, 1), 82, 212, new Item[]
    {}), MANTA_RAY(new Item(389, 1), new Item(391, 1), 91, 216, new Item[]
    {}),

    /**
     * Snails
     */
    THIN_SNAIL_MEAT(new Item(3363, 1), new Item(3369, 1), 12, 70, new Item[]
    {}), LEAN_SNAIL_MEAT(new Item(3365, 1), new Item(3371, 1), 17, 80, new Item[]
    {}), FAT_SNAIL_MEAT(new Item(3367, 1), new Item(3373, 1), 22, 95, new Item[]
    {}),

    /**
     * Bread
     */
    BREAD(new Item(2307, 1), new Item(2309, 1), 1, 40, new Item[]
    {}), PITTA_BREAD(new Item(1863, 1), new Item(1865, 1), 58, 40, new Item[]
    {}),

    /**
     * Pies
     */
    REDBERRY_PIE(new Item(2321, 1), new Item(2325, 1), 10, 78, new Item[]
    {}), MEAT_PIE(new Item(2319, 1), new Item(2327, 1), 20, 110, new Item[]
    {}), MUD_PIE(new Item(7168, 1), new Item(7170, 1), 29, 128, new Item[]
    {}), APPLE_PIE(new Item(2317, 1), new Item(2323, 1), 30, 130, new Item[]
    {}), GARDEN_PIE(new Item(7176, 1), new Item(7178, 1), 34, 138, new Item[]
    {}), FISH_PIE(new Item(7186, 1), new Item(7188, 1), 47, 164, new Item[]
    {}), ADMIRAL_PIE(new Item(7196, 1), new Item(7198, 1), 70, 210, new Item[]
    {}), WILD_PIE(new Item(7206, 1), new Item(7208, 1), 85, 240, new Item[]
    {}), SUMMER_PIE(new Item(7216, 1), new Item(7218, 1), 95, 260, new Item[]
    {}),

    /**
     * Stews
     */
    STEW(new Item(2001, 1), new Item(2003, 1), 25, 117, new Item[]
    {}),
    /* SPICY_STEW(new Item(2001, 1), new Item(7479, 1), 25, 117, new Item[] {}), */
    CURRY(new Item(2009, 1), new Item(2011, 1), 60, 280, new Item[]
    {}),

    /**
     * Pizza
     */
    PLAIN_PIZZA(new Item(2287, 1), new Item(2289, 1), 35, 143, new Item[]
    {});

    /**
     * Potato Toppings
     */
    // SPICY_SAUCE

    /**
     * The main ingredient, which is used on the fire/range.
     */
    private Item mainIngredient;

    /**
     * Any extra raw materials used to produce a 'cooked' product.
     */
    private Item[] extraMaterials;

    /**
     * The result, a 'cooked' product.
     */
    private Item cooked;

    /**
     * The level required to preform the 'cooking' task.
     */
    private int levelRequried;

    /**
     * The amount of experience gained from the 'cooking' task.
     */
    private double experience;

    /**
     * 
     * @param rawId The ID of the 'uncooked' item.
     * @param cookedId The ID of the 'cooked' item.
     * @param levelRequired The level required to 'cook' this item.
     * @param experience The experienced gained.
     */
    private Cookable(Item mainIngredient, Item cooked, int levelRequired, double experience, Item... rawMaterials)
    {
        this.mainIngredient = mainIngredient;
        this.cooked = cooked;
        this.levelRequried = levelRequired;
        this.experience = experience;
        this.extraMaterials = rawMaterials;

    }

    public Item getMainIngredient()
    {
        return mainIngredient;
    }

    public Item getCookedItem()
    {
        return cooked;
    }

    public int getLevelRequried()
    {
        return levelRequried;
    }

    public double getExperience()
    {
        return experience;
    }

    public Item[] getRawItems()
    {
        return extraMaterials;
    }

    /**
     * A mapping of the cookable items, called by the key of the main
     * ingredient.
     */
    private static Map<Integer, Cookable> cookableItems = new HashMap<Integer, Cookable>();

    static
    {
        for (Cookable cookable : Cookable.values())
        {
            cookableItems.put(cookable.getMainIngredient().getId(), cookable);
        }
    }

    /**
     * 
     * @param id The ID of the rewarded item.
     * @return
     */
    public static Cookable forUncooked(int id)
    {
        return cookableItems.get(id);
    }

}
