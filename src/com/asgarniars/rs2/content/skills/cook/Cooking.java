package com.asgarniars.rs2.content.skills.cook;

import com.asgarniars.rs2.action.impl.ProductionAction;
import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.util.Misc;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class Cooking extends ProductionAction
{

    private Cookable cookable;

    public Cooking(Entity entity, Cookable cookable)
    {
        super(entity);
        this.cookable = cookable;
    }

    @Override
    public boolean canProduce()
    {
        return true;
    }

    @Override
    public int getCycleCount()
    {
        return 3;
    }

    @Override
    public short getGraphic()
    {
        return -1;
    }

    @Override
    public short getAnimation()
    {
        // TODO: switch fire/range
        return 883;
    }

    @Override
    public double getExperience()
    {
        return cookable.getExperience();
    }

    @Override
    public Item[] getConsumedItems()
    {
        return new Item[]
        { cookable.getMainIngredient() };
    }

    @Override
    public String getInsufficentLevelMessage()
    {
        return "You do not have a high enough " + Skill.getSkillName(Skill.COOKING) + " level to cook " + ItemManager.getInstance().getItemName(cookable.getCookedItem().getId()) + ".";
    }

    @Override
    public int getProductionCount()
    {
        return cookable.getCookedItem().getCount();
    }

    @Override
    public int getRequiredLevel()
    {
        return cookable.getLevelRequried();
    }

    @Override
    public Item[] getRewards()
    {
        return new Item[]
        { cookable.getCookedItem() };
    }

    @Override
    public int getSkill()
    {
        return Skill.COOKING;
    }

    @Override
    public String getSuccessfulProductionMessage()
    {
        String reward = ItemManager.getInstance().getItemName(cookable.getCookedItem().getId()).toLowerCase();
        return "You successfully cook " + Misc.getArticle(reward) + reward + ".";
    }

}
