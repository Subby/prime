package com.asgarniars.rs2.content.skills.firecraft;

import com.asgarniars.rs2.action.Action;
import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.model.players.GlobalObjectHandler;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;
import com.asgarniars.rs2.util.Misc;
import com.asgarniars.rs2.util.clip.RSObject;
import com.asgarniars.rs2.util.clip.RegionClipping;

/**
 * The event in which a player lights a fire.
 * 
 * @author Joshua Barry
 * 
 */
public class FireCraft extends Action
{

    private Player player;

    private Position position;

    private LogType log;

    private final Item ASHES = new Item(592, 1);

    /**
     * 
     * @param player The player who is crafting a fire.
     * @param log The log they are attempting to burn.
     * @param ticks The ticks prior to execution.
     */
    public FireCraft(Player player, LogType log, int ticks)
    {
        super(player, ticks);
        this.player = player;
        this.position = player.getPosition();
        this.log = log;
    }

    @Override
    public CancelPolicy getCancelPolicy()
    {
        return CancelPolicy.ALWAYS;
    }

    @Override
    public StackPolicy getStackPolicy()
    {
        return StackPolicy.NEVER;
    }

    @Override
    public AnimationPolicy getAnimationPolicy()
    {
        return AnimationPolicy.RESET_ALL;
    }

    @Override
    public void execute()
    {

        player.setCanWalk(false);

        if (RegionClipping.blockedWest(player, position))

            if (!RegionClipping.blockedEast(player, position))
                player.setAttribute("nextX", (position.getX() + 1));

            else
            {
                player.getActionSender().sendMessage("You cannot light a fire here.");
                player.setCanWalk(true);
                this.stop();
                return;
            }

        else
            player.setAttribute("nextX", (position.getX() - 1));

        player.getUpdateFlags().sendAnimation(0x2DD);

        if (player.getAttribute("litFire") == null)
            player.getActionSender().sendMessage("You attempt to light the logs.");

        World.submit(new Task(4)
        {

            @Override
            protected void execute()
            {

                final int x = (Integer) player.getAttribute("nextX");
                final int y = position.getY();

                Item burn = new Item(log.getId());

                if (player.getInventory().removeItem(burn))
                {

                    switch (log.getId())
                    {
                    case 7404:
                        player.setAttribute("fireId", 11404);
                        break;
                    case 7405:
                        player.setAttribute("fireId", 11405);
                        break;
                    case 7406:
                        player.setAttribute("fireId", 11406);
                        break;
                    case 10328:
                        player.setAttribute("fireId", 20000);
                        break;
                    case 10329:
                        player.setAttribute("fireId", 20001);
                        break;
                    default:
                        player.setAttribute("fireId", 2732);
                        break;
                    }

                    int fireId = (Integer) player.getAttribute("fireId");

                    final RSObject fire = new RSObject(fireId, player.getPosition(), 0xA, 0);

                    player.getMovementHandler().walk(new Position(x, y));

                    player.getUpdateFlags().sendFaceToDirection(fire.getPosition());

                    player.getActionSender().sendMessage("The fire catches and the logs begin to burn.");


                    GlobalObjectHandler.createGlobalObject(player, fire);

                    int duration = Misc.randomMinMax(60, 120);

                    World.submit(new Task(duration)
                    {

                        @Override
                        protected void execute()
                        {
                            player.getActionSender().removeObject(fire.getPosition());
                            ItemManager.getInstance().createGroundItem(player, player.getUsername(), ASHES, fire.getPosition(), 60);
                            this.stop();

                        }

                    });

                    player.getSkill().addExperience(Skill.FIREMAKING, log.getExperience());

                    player.setCanWalk(true);

                    player.setAttribute("litFire", (byte) 1);
                }
                player.removeAttribute("fireId");
                player.removeAttribute("nextX");
                this.stop();

            }

        });

        this.stop();
    }
}
