package com.asgarniars.rs2.content.skills.fish;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Joshua Barry <Ares>
 * @author Dominick Jones <Jones>
 * 
 */
public enum Tools
{

    /**
     * Small Net
     */
    SMALL_NET(303, 621, new short[]
    { 317, 3150, 321, 5004, 7994 }),
    /**
     * Big Net
     */
    BIG_NET(305, 621, new short[]
    { 353, 341, 363 }),
    /**
     * Crayfish Cage [needs proper anim]
     */
    CRAYFISH_CAGE(13431, 619, new short[]
    { 13435 }),
    /**
     * Fishing Rod
     */
    FISHING_ROD(307, 622, new short[]
    { 327, 345, 349, 3379, 5001, 2148 }),
    /**
     * Fly Fishing Rod
     */
    FLYFISHING_ROD(309, 622, new short[]
    { 335, 331 }),
    /**
     * Karambwan Pot (need to find animation)
     */
    KARAMBWAN_POT(3157, -1, new short[]
    { 3142 }),
    /**
     * Harpoon
     */
    HARPOON(311, 618, new short[]
    { 359, 371 }),
    /**
     * Lobster Pot
     */
    LOBSTER_POT(301, 619, new short[]
    { 377 });

    private int toolId;

    private int animation;

    private short[] outcomes;

    /**
     * 
     * @param toolId The item id of the tool we will be using to catch fish.
     * @param outcomes An array of item id's of possible reward outcomes.
     */
    private Tools(int toolId, int animation, short[] outcomes)
    {
        this.toolId = toolId;
        this.outcomes = outcomes;
        this.animation = animation;
    }

    public int getToolId()
    {
        return toolId;
    }

    public int getAnimationId()
    {
        return animation;
    }

    public short[] getOutcomes()
    {
        return outcomes;
    }

    public static Tools forId(int id)
    {
        return tools.get(id);
    }

    private static Map<Integer, Tools> tools = new HashMap<Integer, Tools>();

    static
    {
        for (Tools tool : Tools.values())
        {
            tools.put(tool.getToolId(), tool);
        }
    }

}
