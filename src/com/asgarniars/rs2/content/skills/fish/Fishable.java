package com.asgarniars.rs2.content.skills.fish;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains all data relevant to fishing tools.
 * 
 * @author Joshua Barry <Ares>
 * @author Dominick Jones <Jones>
 * 
 */
public enum Fishable
{

    /*
     * Shrimp
     */
    SHRIMP(317, 303, 1, 10, -1),
    /*
     * Crayfish
     */
    CRAYFISH(13435, 13431, 1, 10, -1),
    /*
     * Karambwanji
     */
    KARAMBWANJI(3150, 303, 5, 5, -1),
    /*
     * Sardine
     */
    SARDINE(327, 307, 5, 20, 313),
    /*
     * Herring
     */
    HERRING(345, 307, 10, 30, 313),
    /*
     * Anchovies
     */
    ANCHOVIES(321, 303, 15, 40, -1),
    /*
     * Mackerel
     */
    MACKEREL(353, 305, 16, 20, -1),
    /*
     * Trout
     */
    TROUT(335, 309, 20, 50, 314),
    /*
     * Cod
     */
    COD(341, 305, 23, 45, -1),

    /*
     * Pike
     */
    PIKE(349, 307, 25, 60, 313),

    /*
     * Slimy Eel
     */
    SLIMY_EEL(3379, 307, 28, 65, 313),
    /*
     * Salmon
     */
    SALMON(331, 309, 30, 70, 314),
    /*
     * Frog Spawn
     */
    FROG_SPAWN(5004, 303, 33, 75, -1),
    /*
     * Tuna
     */
    TUNA(359, 311, 35, 80, -1),
    /*
     * Cave Eel
     */
    CAVE_EEL(5001, 307, 38, 80, 313),
    /*
     * Lobster
     */
    LOBSTER(377, 301, 40, 90, -1),
    /*
     * Bass
     */
    BASS(363, 305, 46, 100, -1),
    /*
     * Sword Fish
     */
    SWORD_FISH(371, 311, 50, 100, -1),
    /*
     * Lava Eel
     */
    LAVA_EEL(2148, 307, 53, 30, 313),
    /*
     * Monk Fish
     */
    MONK_FISH(7944, 303, 62, 120, -1),
    /*
     * Karambwan
     */
    KARAMBWAN(3142, 3157, 65, 100, -1),
    /*
     * Shark
     */
    SHARK(383, 311, 76, 110, -1),
    /*
     * Sea Turtle
     */
    SEA_TURTLE(395, -1, 79, 38, -1),
    /*
     * Manta Ray
     */
    MANTA_RAY(390, -1, 81, 46, -1);

    private short rawFishId;

    private short toolId;

    private short levelRequired;

    private short baitRequired;

    private double experienceGain;

    /**
     * 
     * @param rawFishId The id of the fish you will catch
     * @param toolId The id of the tool needed to catch the fish
     * @param levelRequired The level required to catch the fish
     * @param experienceGain The experience gained from catching the fish.
     */
    private Fishable(int rawFishId, int toolId, int levelRequired, double experienceGain, int baitRequired)
    {
        this.rawFishId = (short) rawFishId;
        this.toolId = (short) toolId;
        this.levelRequired = (short) levelRequired;
        this.experienceGain = experienceGain;
        this.baitRequired = (short) baitRequired;

    }

    public short getRawFishId()
    {
        return rawFishId;
    }

    public short getToolId()
    {
        return toolId;
    }

    public short getRequiredLevel()
    {
        return levelRequired;
    }

    public double getExperience()
    {
        return experienceGain;
    }

    public short getBaitRequired()
    {
        return baitRequired;
    }

    private static Map<Integer, Fishable> fish = new HashMap<Integer, Fishable>();

    static
    {
        for (Fishable fishes : Fishable.values())
        {
            fish.put((int) fishes.getRawFishId(), fishes);
        }
    }

    public static Fishable forId(int rawFishId)
    {
        return fish.get(rawFishId);
    }

}
