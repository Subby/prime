package com.asgarniars.rs2.network.command.commands;

import java.io.FileNotFoundException;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.content.shop.ShopManager;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.npcs.NpcLoader;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.model.players.Rights;
import com.asgarniars.rs2.network.command.Command;
import com.asgarniars.rs2.util.ScriptManager;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class ReloadScripts implements Command
{

    @Override
    public void invoke(Player player, String command)
    {

        if (player.getRights() < Rights.ADMINISTRATOR.ordinal())
            return;

        ScriptManager.getSingleton().loadScripts(Constants.SCRIPT_DIRECTORY);

        // -- First unregistering all npcs from world so they wont get doubled.
        for (Npc npc : World.getNpcs())
        {
            if (npc != null)
            {
                npc.setAttribute("inVisible", 0);
                World.unregister(npc);
            }
        }

        // -- Then reloading the data.
        try
        {
            NpcLoader.loadDefinitions();
            NpcLoader.loadSpawns();
            
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        
        // -- Load the shops
        try {
			ShopManager.load();
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        player.getActionSender().sendMessage("Scripts reloaded successfully.");

    }

}
