package com.asgarniars.rs2.network.command.commands;

import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.network.command.Command;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class JoshTestCommand implements Command
{

    @Override
    public void invoke(final Player player, String command)
    {

        player.getActionSender().sendMessage(">> print 'Hello, World!'");
    }

}
