package com.asgarniars.rs2.network.command.commands;

import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.npcs.Npc.WalkType;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.model.players.Rights;
import com.asgarniars.rs2.network.command.Command;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class NpcDecoy implements Command
{

    @Override
    public void invoke(Player player, String command)
    {

        if (player.getRights() < Rights.ADMINISTRATOR.ordinal())
            return;

        String[] parts = command.split(" ");
        int id = Integer.parseInt(parts[1]);
        WalkType mode = WalkType.WALK;
        Npc npc = new Npc(id);
        npc.setSpawnPosition(player.getPosition());
        npc.setAttribute("Denver", 0);
        npc.setMaxWalkingArea(new Position(1, 0, 0));
        npc.setWalkType(mode);
        npc.setFaceType(1);
        World.register(npc);
    }
}
