package com.asgarniars.rs2.network.command.commands;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.model.players.Rights;
import com.asgarniars.rs2.network.command.Command;
import com.asgarniars.rs2.task.Task;

/**
 * 
 * @author AkZu
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class Update implements Command
{

    @Override
    public void invoke(final Player player, String command)
    {
        // -- Only manager of this server is allowed to do this
        if (player.getRights() >= Rights.ADMINISTRATOR.ordinal())
        {

            String[] parts = command.split(" ");

            // -- Base setting is 1 minute, using this multiplier you can
            // multiply it
            int time = Integer.parseInt(parts[1]);

            if (!Constants.ACCEPT_CONNECTIONS)
            {
                player.getActionSender().sendMessage("Update is already announced!");
                return;
            }

            // -- Block all incoming connections right away
            Constants.ACCEPT_CONNECTIONS = false;

            // -- Inform players that update is happening in time
            for (Player players : World.getPlayers())
            {
                if (players == null)
                    continue;
                players.getActionSender().sendServerUpdate(60 * time);
            }

            // -- Log the players out
            World.submit(new Task(100 * time)
            {

                @Override
                protected void execute()
                {
                    for (final Player players : World.getPlayers())
                    {
                        if (players == null)
                            continue;
                        players.logout();
                    }

                    // -- Give it 6 sec cooldown time and kill the server
                    World.submit(new Task(10)
                    {

                        @Override
                        protected void execute()
                        {
                            System.exit(0);
                            this.stop();
                        }
                    });
                    this.stop();
                }
            });
        }
    }
}
