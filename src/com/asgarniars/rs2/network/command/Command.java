package com.asgarniars.rs2.network.command;

import com.asgarniars.rs2.model.players.Player;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public interface Command
{

    public void invoke(Player player, String command);

}