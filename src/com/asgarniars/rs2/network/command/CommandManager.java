package com.asgarniars.rs2.network.command;

import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.network.command.commands.*;
import com.asgarniars.rs2.util.ScriptManager;

/**
 * Allows the user to execute commands that were developed in either Java
 * {@code Command} objects, or Python scripts.
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class CommandManager
{

    /**
     * Represents an invokable command.
     * 
     * @author Joshua Barry <Sneakyhearts>
     * 
     */
    private enum Invokable
    {

        AKZU("akzu", new AkzuTestCommand()), // Aki
        JOSH("barry", new JoshTestCommand()), // Josh
        NPC("npc", new NpcDecoy()), // spawn an npc
        RELOAD("xreload", new ReloadScripts()), // reload all scripts /
                                                // reloadable content
        UPDATE("update", new Update()); // update the game

        /**
         * The string used to identify the command.
         */
        private String identifier;

        /**
         * The command to execute.
         */
        private Command command;

        /**
         * 
         * @param identifier A string used to identify the command.
         * @param command The command that will be invoked.
         */
        private Invokable(String identifier, Command command)
        {

            this.identifier = identifier;
            this.command = command;
        }

        public String getIdentifier()
        {
            return identifier;
        }

        public Command getCommand()
        {
            return command;
        }

        /**
         * 
         * @param name The name of the command
         * @return invokable The invokable command, if one is found.
         */
        public static Invokable forIdentifier(String name)
        {
            for (Invokable invokable : Invokable.values())
                if (name.equalsIgnoreCase(invokable.getIdentifier()))
                    return invokable;
            return null;
        }

    }

    /**
     * Executes the command.
     * 
     * @param player
     * @param command
     */
    public static void invoke(Player player, String command)
    {
        int rights = player.getRights();
        String name = " ";
        String[] args =
        {};
        if (command.indexOf(' ') > -1)
        {
            command.replaceAll(":", "");
            args = command.split(" ");
            name = args[0].toLowerCase();
        }
        else
        {
            name = command.toLowerCase();
        }
        if (name.length() == 0)
        {
            player.getActionSender().sendMessage("The command you have entered does not exist.");
            return;
        }
        else
        {
            Invokable invokable = Invokable.forIdentifier(name);
            if (invokable != null)
            {
                invokable.getCommand().invoke(player, command);
                return;
            }

            String script = "command_".concat(name);

            try
            {
                ScriptManager manager = ScriptManager.getSingleton();

                if (!manager.invokeWithFailTest(script, player, args))
                    if (rights >= 1)
                        if (!manager.invokeWithFailTest("mod_".concat(script), player, args))
                            if (rights >= 2)
                                if (!manager.invokeWithFailTest("admin_".concat(script), player, args))
                                    if (rights >= 3)
                                        if (!manager.invokeWithFailTest("owner_".concat(script), player, args))
                                            player.getActionSender().sendMessage("Sorry bro, no can do.");

            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                player.getActionSender().sendMessage("Error while processing command.");
            }
        }

    }
}
