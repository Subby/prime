package com.asgarniars.rs2.network;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.content.BankPin.AtInterfaceStatus;
import com.asgarniars.rs2.content.combat.Combat;
import com.asgarniars.rs2.content.combat.magic.SpellLoader;
import com.asgarniars.rs2.content.combat.util.SpecialAttack;
import com.asgarniars.rs2.content.duel.DuelArena;
import com.asgarniars.rs2.model.Palette;
import com.asgarniars.rs2.model.Palette.PaletteTile;
import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.players.GlobalObject;
import com.asgarniars.rs2.model.players.GroundItem;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.model.players.Player.MagicBookTypes;
import com.asgarniars.rs2.model.players.Player.TeleotherStage;
import com.asgarniars.rs2.model.region.Region;
import com.asgarniars.rs2.network.StreamBuffer.AccessType;
import com.asgarniars.rs2.network.StreamBuffer.ByteOrder;
import com.asgarniars.rs2.network.StreamBuffer.OutBuffer;
import com.asgarniars.rs2.network.StreamBuffer.ValueType;
import com.asgarniars.rs2.util.clip.RSObject;
import com.asgarniars.rs2.util.clip.RegionClipping;

public class ActionSender
{

    private Player player;

    public ActionSender(Player player)
    {
        this.player = player;
    }

    public ActionSender sendMinimapBlackout(int val)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(2);
        out.writeHeader(player.getEncryptor(), 99);
        out.writeByte(val);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendServerUpdate(int time)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(4);
        out.writeHeader(player.getEncryptor(), 114);
        out.writeShort((time * 50 / 30), StreamBuffer.ByteOrder.LITTLE);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendLogin()
    {
        sendDetails();
        sendResetCamera();
        sendMapRegion();
        int[] sidebars =
        { 2423, 3917, 638, 3213, 1644, 5608, 0, 18128, 5065, 5715, 2449, 904, 147, 962 };
        for (int i = 0; i < sidebars.length; i++)
        {
            sendSidebarInterface(i, sidebars[i]);
        }
        if (player.getMagicBookType() == MagicBookTypes.MODERN)
        {
            sendSidebarInterface(6, 1151);
        }
        else if (player.getMagicBookType() == MagicBookTypes.ANCIENT)
        {
            sendSidebarInterface(6, 12855);
        }
        sendEnergy();
        sendWeight();
        SpecialAttack.getInstance().addSpecialBar(player);
        updateSpecialBar();
        sendMessage("Welcome to " + Constants.SERVER_NAME + ".");

        for (int i = 0; i < Constants.MESSAGES_ON_LOGIN.length; i++)
        {
            player.getActionSender().sendMessage(Constants.MESSAGES_ON_LOGIN[i]);
        }

        player.getPrivateMessaging().sendPMOnLogin();
        player.setSkulled(player.isSkulled());
        player.setPoisoned(player.isPoisoned());
        return this;
    }

    public ActionSender sendConfigsOnLogin()
    {
        resetAutoCastInterface();
        Combat.getInstance().fixAttackStyles(player, player.getEquipment().get(3));
        sendPlayerOption("Follow", 3);
        sendPlayerOption("Trade with", 4);
        sendConfig(172, player.shouldAutoRetaliate() ? 1 : 0);

        sendConfig(170, (Byte) player.getAttribute("mouseButtons"));
        sendConfig(171, (Byte) player.getAttribute("chatEffects"));
        sendConfig(287, (Byte) player.getAttribute("splitPrivate"));
        sendConfig(427, (Byte) player.getAttribute("acceptAid"));
        sendConfig(304, 0); // Bank modes
        sendConfig(115, 0); // Bank modes
        sendConfig(173, 0); // Walk mode
        return this;
    }

    public ActionSender sendCameraUpdate(int x, int y, int height, int speed, int angle)
    {
        OutBuffer out = StreamBuffer.newOutBuffer(7);
        out.writeHeader(player.getEncryptor(), 177);
        out.writeByte(x / 64);
        out.writeByte(y / 64);
        out.writeShort(height);
        out.writeByte(speed);
        out.writeByte(angle);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendCameraSpin(int x, int y, int height, int speed, int angle)
    {
        OutBuffer out = StreamBuffer.newOutBuffer(7);
        out.writeHeader(player.getEncryptor(), 166);
        out.writeByte(x / 64);
        out.writeByte(y / 64);
        out.writeShort(height);
        out.writeByte(speed);
        out.writeByte(angle);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendCameraReset()
    {
        OutBuffer out = StreamBuffer.newOutBuffer(1);
        out.writeHeader(player.getEncryptor(), 107);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendSkill(int skillID, int level, double exp)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(7);
        out.writeHeader(player.getEncryptor(), 134);
        out.writeByte(skillID);
        out.writeInt((int) exp, StreamBuffer.ByteOrder.MIDDLE);
        out.writeByte(level);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendEnterX()
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(1);
        out.writeHeader(player.getEncryptor(), 27);
        player.send(out.getBuffer());
        return this;
    }

    /**
     * Sends the welcome screen interface
     * 
     * @param id 0-7 ( The id's of the bottom part of the interface )
     */
    public ActionSender sendWelcomeScreen(int id)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(3);
        out.writeHeader(player.getEncryptor(), 176);
        out.writeByte(id);
        player.send(out.getBuffer());
        return this;
    }

    /**
     * Removes map flag
     */
    public ActionSender removeMapFlag()
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(1);
        out.writeHeader(player.getEncryptor(), 78);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender updateFlashingSideIcon(int tabId)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(2);
        out.writeHeader(player.getEncryptor(), 24);
        out.writeByte(tabId);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendUpdateItem(int slot, int inventoryId, Item item)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(32);
        out.writeVariableShortPacketHeader(player.getEncryptor(), 34);
        out.writeShort(inventoryId);
        out.writeByte(slot);
        out.writeShort((item == null ? 0 : item.getId() + 1));
        if (item != null)
        {
            if (item.getCount() > 254)
            {
                out.writeByte(255);
                out.writeInt(item.getCount());
            }
            else
            {
                out.writeByte(item.getCount());
            }
        }
        else
            out.writeByte(0);
        out.finishVariableShortPacketHeader();
        player.send(out.getBuffer());
        return this;
    }

    /**
     * 
     * @param interfaceId
     * @param items
     * @param flag If you wanna limit the size
     * @return
     */
    public ActionSender sendUpdateItems(int interfaceId, Item[] items)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(5 + (items.length * 7));
        out.writeVariableShortPacketHeader(player.getEncryptor(), 53);
        out.writeShort(interfaceId);
        out.writeShort(items.length);
        for (Item item : items)
        {
            if (item != null)
            {
                if (item.getCount() > 254)
                {
                    out.writeByte(255);
                    out.writeInt(item.getCount(), ByteOrder.INVERSE_MIDDLE);
                }
                else
                {
                    out.writeByte(item.getCount());
                }
                out.writeShort(item.getId() + 1, ValueType.A, ByteOrder.LITTLE);
            }
            else
            {
                out.writeByte(0);
                out.writeShort(0, ValueType.A, ByteOrder.LITTLE);
            }
        }
        out.finishVariableShortPacketHeader();
        player.send(out.getBuffer());
        return this;
    }

    /**
     * 
     * @param position
     * @return
     */
    public ActionSender removeObject(Position position)
    {
        // Coded by Sneakyheartz <3
        int blank = 0x1B27;
        int type = 0xA;
        sendCoords(position);
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(5);
        out.writeHeader(player.getEncryptor(), 151);
        out.writeByte(0, StreamBuffer.ValueType.S);
        out.writeShort(blank, ByteOrder.LITTLE);
        out.writeByte(((type << 2) + (0 & 3)), StreamBuffer.ValueType.S);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendObject(GlobalObject object)
    {
        // c.getOutStream().createFrame(85);
        // c.getOutStream().writeByteC(objectY - (c.getMapRegionY() * 8));
        // c.getOutStream().writeByteC(objectX - (c.getMapRegionX() * 8));
        // c.getOutStream().createFrame(101);
        // c.getOutStream().writeByteC((objectType<<2) + (face&3));
        // c.getOutStream().writeByte(0);
        //
        // if (objectId != -1) { // removing
        // c.getOutStream().createFrame(151);
        // c.getOutStream().writeByteS(0);
        // c.getOutStream().writeWordBigEndian(objectId);
        // c.getOutStream().writeByteS((objectType<<2) + (face&3));
        // }

        sendCoords(object.getPosition());
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(5);
        out.writeHeader(player.getEncryptor(), 151);
        out.writeByte(0, StreamBuffer.ValueType.S);
        out.writeShort(object.getId(), StreamBuffer.ByteOrder.LITTLE);
        out.writeByte(((object.getType() << 2) + (object.getFace() & 3)), StreamBuffer.ValueType.S);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendObject(RSObject object)
    {
        sendCoords(object.getPosition());
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(5);
        out.writeHeader(player.getEncryptor(), 151);
        out.writeByte(0, StreamBuffer.ValueType.S);
        out.writeShort(object.getId(), ByteOrder.LITTLE);
        out.writeByte(((object.getType() << 2) + (object.getFace() & 3)), StreamBuffer.ValueType.S);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendMessage(String message)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(message.length() + 3);
        out.writeVariablePacketHeader(player.getEncryptor(), 253);
        out.writeString(message);
        out.finishVariablePacketHeader();
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendSidebarInterface(int menuId, int form)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(4);
        out.writeHeader(player.getEncryptor(), 71);
        out.writeShort(form);
        out.writeByte(menuId, StreamBuffer.ValueType.A);
        player.send(out.getBuffer());
        return this;
    }

    /**
     * This creates a projectile packet for a specified player.
     * 
     * @param position
     * @param offsetX
     * @param offsetY
     * @param id
     * @param startHeight
     * @param endHeight
     * @param speed
     * @param lockon
     * @return
     */
    public ActionSender sendProjectile(Position position, int offsetX, int offsetY, int projectileId, int startHeight, int endHeight, int speed, int lockon, int delay, int curve, int radius, int angle)
    {
        sendCoordinates2(position);
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(16);
        out.writeHeader(player.getEncryptor(), 117);
        out.writeByte(angle);// angle
        out.writeByte(offsetY);
        out.writeByte(offsetX);
        out.writeShort(lockon);
        out.writeShort(projectileId);
        out.writeByte(startHeight);
        out.writeByte(endHeight);
        out.writeShort(delay);
        out.writeShort(speed);
        out.writeByte(curve);
        out.writeByte(radius);// radius?
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendCoordinates2(Position position)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(3);
        out.writeHeader(player.getEncryptor(), 85);
        int y = position.getY() - player.getLastKnownRegion().getRegionY() * 8 - 2;
        int x = position.getX() - player.getLastKnownRegion().getRegionX() * 8 - 3;
        out.writeByte(y, StreamBuffer.ValueType.C);
        out.writeByte(x, StreamBuffer.ValueType.C);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendMapRegion()
    {
        player.getLastKnownRegion().setAs(player.getPosition());
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(5);
        out.writeHeader(player.getEncryptor(), 73);
        out.writeShort(player.getPosition().getRegionX() + 6, StreamBuffer.ValueType.A);
        out.writeShort(player.getPosition().getRegionY() + 6);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendLogout()
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(1);
        out.writeHeader(player.getEncryptor(), 109);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendInterface(int id)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(3);
        out.writeHeader(player.getEncryptor(), 97);
        out.writeShort(id);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendWalkableInterface(int id)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(3);
        out.writeHeader(player.getEncryptor(), 208);
        out.writeShort(id, StreamBuffer.ByteOrder.LITTLE);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendMultiInterface(boolean inMulti)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(3);
        out.writeHeader(player.getEncryptor(), 61);
        out.writeByte(inMulti ? 1 : 0);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendItfInvOverlay(int interfaceId, int inventoryId)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(5);
        out.writeHeader(player.getEncryptor(), 248);
        out.writeShort(interfaceId, StreamBuffer.ValueType.A);
        out.writeShort(inventoryId);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender removeInterfaces()
    {

        if (player.getAttribute("teleotherPartnerId") != null)
        {
            player.setTeleotherStage(TeleotherStage.WAITING);
            player.removeAttribute("teleotherPartnerId");
            player.removeAttribute("teleotherSpellId");
        }

        if ((String) player.getAttribute("duel_stage") == "VICTORY")
            DuelArena.getInstance().give_won_items(player);

        player.removeAttribute("DIALOG_CLICK_ID");
        player.setInteractingEntity(null);

        if (player.getBankPin().getInterfaceStatus() != AtInterfaceStatus.NONE)
            player.getBankPin().setAtInterfaceStatus(AtInterfaceStatus.NONE);

        if (player.getAttribute("isBanking") != null)
        {
            player.getBank().shift();
            player.removeAttribute("BANK_MODE_NOTE");
            player.removeAttribute("BANK_MODE_INSERT");
            player.getActionSender().sendConfig(304, 0);
            player.getActionSender().sendConfig(115, 0);
            player.removeAttribute("isBanking");
        }

        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(1);
        out.writeHeader(player.getEncryptor(), 219);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender setScrollPosition(int itfId, int pos)
    {
        // TODO: Lower buffer
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(8);
        out.writeHeader(player.getEncryptor(), 79);
        out.writeShort(itfId, ByteOrder.LITTLE);
        out.writeShort(pos, ValueType.A);
        player.send(out.getBuffer());
        return this;
    }

    // Custom packet
    public ActionSender setScrollMax(int itfId, int max)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(5);
        out.writeHeader(player.getEncryptor(), 238);
        out.writeShort(itfId);
        out.writeShort(max);
        player.send(out.getBuffer());
        return this;
    }

    /**
     * Sends all ground items ( and future, objects etc ) on region load for 8x8
     * region blocks.. Need to send exactly 8 times this packet.
     * 
     * TODO: Unfinished........
     * 
     * Need to split 32x32 region into 16 8x8 chunks
     */
    public ActionSender sendEntitiesOnRegionLoad(Region region)
    {

        // -- Total grounditems in this region (used for determining the packet
        // buffer etc)...

        int total_items = 0;
        for (GroundItem g : region.getGroundItems())
        {
            if (g == null)
                continue;
            total_items++;
        }

        // -- First split all the ground items in current 32x32 region and that
        // into 16 smaller 8x8 chunks for client to easily read.
        GroundItem[][] ground_items = new GroundItem[16][1 + total_items];

        int[] ground_items_index_sub = new int[16];

        // -- Converting 32x32 region into 16 8x8 chunks..
        int[][] offset_table = new int[][]
        {
        { 0, 0 },
        { 8, 0 },
        { 16, 0 },
        { 24, 0 },
        { 0, 8 },
        { 8, 8 },
        { 16, 8 },
        { 24, 8 },
        { 0, 16 },
        { 8, 16 },
        { 16, 16 },
        { 24, 16 },
        { 0, 24 },
        { 8, 24 },
        { 16, 24 },
        { 24, 24 }, };

        for (GroundItem g : region.getGroundItems())
        {

            if (g == null)
                continue;

            // -- If the grounditem is at same height as we, and the item is NOT
            // global and we are not the owner of the
            // item, continue..
            if (g.getPos().getZ() != player.getPosition().getZ() || (!g.isGlobal() && !g.getOwner().equals(player.getUsername())))
                continue;

            // X < 8
            if (g.getPos().getX() - region.getCoordinates().getX() * 32 < 8)
            {
                if (g.getPos().getY() - region.getCoordinates().getY() * 32 < 8)
                    ground_items[0][ground_items_index_sub[0]++] = g;
                else if (g.getPos().getY() - region.getCoordinates().getY() * 32 < 16)
                    ground_items[4][ground_items_index_sub[4]++] = g;
                else if (g.getPos().getY() - region.getCoordinates().getY() * 32 < 24)
                    ground_items[8][ground_items_index_sub[8]++] = g;
                else
                    ground_items[12][ground_items_index_sub[12]++] = g;
                continue;
            }

            // X < 16
            if (g.getPos().getX() - region.getCoordinates().getX() * 32 < 16)
            {
                if (g.getPos().getY() - region.getCoordinates().getY() * 32 < 8)
                    ground_items[1][ground_items_index_sub[1]++] = g;
                else if (g.getPos().getY() - region.getCoordinates().getY() * 32 < 16)
                    ground_items[5][ground_items_index_sub[5]++] = g;
                else if (g.getPos().getY() - region.getCoordinates().getY() * 32 < 24)
                    ground_items[9][ground_items_index_sub[9]++] = g;
                else
                    ground_items[13][ground_items_index_sub[13]++] = g;
                continue;
            }

            // X < 24
            if (g.getPos().getX() - region.getCoordinates().getX() * 32 < 24)
            {
                if (g.getPos().getY() - region.getCoordinates().getY() * 32 < 8)
                    ground_items[2][ground_items_index_sub[2]++] = g;
                else if (g.getPos().getY() - region.getCoordinates().getY() * 32 < 16)
                    ground_items[6][ground_items_index_sub[6]++] = g;
                else if (g.getPos().getY() - region.getCoordinates().getY() * 32 < 24)
                    ground_items[10][ground_items_index_sub[10]++] = g;
                else
                    ground_items[14][ground_items_index_sub[14]++] = g;
                continue;
            }

            // X < 32
            if (g.getPos().getX() - region.getCoordinates().getX() * 32 < 32)
            {
                if (g.getPos().getY() - region.getCoordinates().getY() * 32 < 8)
                    ground_items[3][ground_items_index_sub[3]++] = g;
                else if (g.getPos().getY() - region.getCoordinates().getY() * 32 < 16)
                    ground_items[7][ground_items_index_sub[7]++] = g;
                else if (g.getPos().getY() - region.getCoordinates().getY() * 32 < 24)
                    ground_items[11][ground_items_index_sub[11]++] = g;
                else
                    ground_items[15][ground_items_index_sub[15]++] = g;
                continue;
            }
        }

        for (int index = 0; index < 16; index++)
        {
            if (ground_items[index][0] != null)
            {

                OutBuffer out = StreamBuffer.newOutBuffer(2450);

                out.writeVariableShortPacketHeader(player.getEncryptor(), 60);
                out.writeByte(((region.getCoordinates().getY() * 32) + offset_table[index][1]) - 8 * (player.getLastKnownRegion().getRegionY()));
                out.writeByte(((region.getCoordinates().getX() * 32) + offset_table[index][0]) - 8 * (player.getLastKnownRegion().getRegionX()), StreamBuffer.ValueType.C);

                for (GroundItem g : ground_items[index])
                {
                    if (g != null)
                    {
                        System.err.println(g);
                        out.writeByte(44);
                        out.writeShort(g.getItem().getId(), StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE);
                        out.writeShort(g.getItem().getCount());
                        out.writeByte(((g.getPos().getX() - (region.getCoordinates().getX() * 32) - offset_table[index][0]) * 16) + (g.getPos().getY() - (region.getCoordinates().getY() * 32) - offset_table[index][1]));
                    }
                }
                out.finishVariableShortPacketHeader();
                player.send(out.getBuffer());
            }
        }

        // -- Null our big array as it's not used anymore..
        ground_items = null;
        return this;
    }

    /**
     * Clears the region out of all ground items, objects etc
     */
    public ActionSender clear_region()
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(1);
        out.writeHeader(player.getEncryptor(), 64);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendCoords(Position position)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(3);
        out.writeHeader(player.getEncryptor(), 85);
        out.writeByte(position.getY() - 8 * player.getLastKnownRegion().getRegionY(), StreamBuffer.ValueType.C);
        out.writeByte(position.getX() - 8 * player.getLastKnownRegion().getRegionX(), StreamBuffer.ValueType.C);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender editGroundItemAmount(GroundItem groundItem)
    {
        sendCoords(groundItem.getPos());
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(6);
        out.writeHeader(player.getEncryptor(), 84);
        out.writeByte(0);
        out.writeShort(groundItem.getItem().getId());
        out.writeShort(groundItem.getItem().getCount());
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendGroundItem(GroundItem groundItem)
    {
        sendCoords(groundItem.getPos());
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(6);
        out.writeHeader(player.getEncryptor(), 44);
        out.writeShort(groundItem.getItem().getId(), StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE);
        out.writeShort(groundItem.getItem().getCount());
        out.writeByte(0);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender removeGroundItem(GroundItem groundItem)
    {
        sendCoords(groundItem.getPos());
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(4);
        out.writeHeader(player.getEncryptor(), 156);
        out.writeByte(0, StreamBuffer.ValueType.S);
        out.writeShort(groundItem.getItem().getId());
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender removeObject(RSObject object)
    {
        if (object.getId() == -1)
        {
            return removeObject(object);
        }
        if (player.getPosition().getZ() != object.getPosition().getZ())
        {
            return this;
        }
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(4);
        out.writeHeader(player.getEncryptor(), 188);
        // int ot = ((object.getType() << 2) + (object.getFace() & 3));
        out.writeByte(0, StreamBuffer.ValueType.S);
        out.writeShort(object.getId());
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendConfig(int id, int value)
    {
        if (value >= -128 && value <= 127)
        {
            StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(4);
            out.writeHeader(player.getEncryptor(), 36);
            out.writeShort(id, StreamBuffer.ByteOrder.LITTLE);
            out.writeByte(value);
            player.send(out.getBuffer());
        }
        else
        {
            StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(7);
            out.writeHeader(player.getEncryptor(), 87);
            out.writeShort(id, StreamBuffer.ByteOrder.LITTLE);
            out.writeInt(value, StreamBuffer.ByteOrder.MIDDLE);
            player.send(out.getBuffer());
        }
        return this;
    }

    public ActionSender sendString(String message, int interfaceId)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(message.length() + 6);
        out.writeVariableShortPacketHeader(player.getEncryptor(), 126);
        out.writeString(message);
        out.writeShort(interfaceId, StreamBuffer.ValueType.A);
        out.finishVariableShortPacketHeader();
        player.send(out.getBuffer());
        return this;
    }

    /**
     * Change e.g text color Green 0x3366 Yellow 0x33FF66 Red 0x6000
     * 
     * @param id
     * @param color
     */
    public ActionSender changeInterfaceComponentColor(int id, int color)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(6);
        out.writeHeader(player.getEncryptor(), 122);
        out.writeShort(id, StreamBuffer.ValueType.A, ByteOrder.LITTLE);
        out.writeShort(color, StreamBuffer.ValueType.A, ByteOrder.LITTLE);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendFriendList(long name, int world)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(10);
        out.writeHeader(player.getEncryptor(), 50);
        if (world != 0)
        {
            world += 9;
        }
        out.writeLong(name);
        out.writeByte(world);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendPMServer(int state)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(2);
        out.writeHeader(player.getEncryptor(), 221);
        out.writeByte(state);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendItemOnInterface(int id, int zoom, int model)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(7);
        out.writeHeader(player.getEncryptor(), 246);
        out.writeShort(id, StreamBuffer.ByteOrder.LITTLE);
        out.writeShort(zoom);
        out.writeShort(model);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendStillGraphic(int graphicId, Position pos, int delay)
    {
        sendCoords(pos);
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(7);
        out.writeHeader(player.getEncryptor(), 4);
        out.writeByte(0);
        out.writeShort(graphicId);
        out.writeByte(pos.getZ());
        out.writeShort(delay);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendPrivateMessage(long name, int rights, byte[] message, int messageSize)
    {
        // TODO: FIXME
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(2048);
        out.writeVariablePacketHeader(player.getEncryptor(), 196);
        out.writeLong(name);
        out.writeInt(player.getPrivateMessaging().getLastPrivateMessageId());
        out.writeByte(rights);
        out.writeBytes(message, messageSize);
        out.finishVariablePacketHeader();
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendChatInterface(int id)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(3);
        out.writeHeader(player.getEncryptor(), 164);
        out.writeShort(id, StreamBuffer.ByteOrder.LITTLE);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendDialogueAnimation(int animId, int interfaceId)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(5);
        out.writeHeader(player.getEncryptor(), 200);
        out.writeShort(animId);
        out.writeShort(interfaceId);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendPlayerDialogueHead(int interfaceId)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(5);
        out.writeHeader(player.getEncryptor(), 185);
        out.writeShort(interfaceId, StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendNPCDialogueHead(int npcId, int interfaceId)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(5);
        out.writeHeader(player.getEncryptor(), 75);
        out.writeShort(npcId, StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE);
        out.writeShort(interfaceId, StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendSound(int id, int tone, int delay)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(6);
        out.writeHeader(player.getEncryptor(), 174);
        out.writeShort(id);
        out.writeByte(tone);
        out.writeShort(delay);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendSong(int id)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(3);
        out.writeHeader(player.getEncryptor(), 74);
        out.writeShort(id, StreamBuffer.ByteOrder.LITTLE);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendPlayerOption(String option, int slot)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(option.length() + 6);
        out.writeVariablePacketHeader(player.getEncryptor(), 104);
        out.writeByte(slot, StreamBuffer.ValueType.C);
        out.writeByte(0, StreamBuffer.ValueType.A);
        out.writeString(option);
        out.finishVariablePacketHeader();
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendDetails()
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(4);
        out.writeHeader(player.getEncryptor(), 249);
        out.writeByte(1, StreamBuffer.ValueType.A);
        out.writeShort(player.getIndex(), StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendEnergy()
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(2);
        out.writeHeader(player.getEncryptor(), 110);
        out.writeByte((int) player.getEnergy());
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendWeight()
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(3);
        out.writeHeader(player.getEncryptor(), 240);
        out.writeShort((int) player.getWeight());
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender sendResetCamera()
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(4);
        out.writeHeader(player.getEncryptor(), 107);
        player.send(out.getBuffer());
        return this;
    }

    public void updateAutoCastInterface(int spellIndex)
    {
        for (int i = 0; i < Constants.MAGIC_SPELL_NAMES.length; i++)
        {
            if (Constants.MAGIC_SPELL_NAMES[i].toLowerCase().equals(SpellLoader.getSpellDefinitions()[spellIndex].getSpellName().toLowerCase()))
            {
                sendConfig(960, i);
                return;
            }
            else
            {
                sendConfig(960, 1);
            }
        }
    }

    /**
     * Sends the packet to construct a map region.
     * 
     * @param palette The palette of map regions.
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendConstructMapRegion(Palette palette)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(2048);
        out.writeVariableShortPacketHeader(player.getEncryptor(), 241);
        out.writeShort(player.getPosition().getRegionY() + 6, ValueType.A);
        out.setAccessType(AccessType.BIT_ACCESS);
        for (int z = 0; z < 4; z++)
        {
            for (int x = 0; x < 13; x++)
            {
                for (int y = 0; y < 13; y++)
                {
                    PaletteTile tile = palette.getTile(x, y, z);
                    out.writeBits(1, tile != null ? 1 : 0);
                    if (tile != null)
                    {
                        out.writeBits(26, tile.getX() << 14 | tile.getY() << 3 | tile.getZ() << 24 | tile.getRotation() << 1);
                    }
                }
            }
        }
        out.setAccessType(AccessType.BYTE_ACCESS);
        out.writeShort(player.getPosition().getRegionX() + 6);
        out.finishVariableShortPacketHeader();
        player.send(out.getBuffer());
        return this;
    }

    public void resetAutoCastInterface()
    {
        sendConfig(960, 1);
        sendConfig(950, 0);
        sendConfig(108, 0);
    }

    public ActionSender showComponent(int mainFrame, boolean show)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(4);
        out.writeHeader(player.getEncryptor(), 171);
        out.writeByte(show ? 0 : 1);
        out.writeShort(mainFrame);
        player.send(out.getBuffer());
        return this;
    }

    /**
     * 
     * @param player_index Players index number
     */
    public ActionSender createPlayerHints(int player_index)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(7);
        out.writeHeader(player.getEncryptor(), 254);
        out.writeByte(10);
        out.writeShort(player_index);
        out.writeByte(0 >> 16);
        out.writeByte(0 >> 8);
        out.writeByte(0);
        player.send(out.getBuffer());
        return this;
    }

    /**
     * 
     * @param player_index Players index number
     */
    public ActionSender createNpcHints(int npc_index)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(7);
        out.writeHeader(player.getEncryptor(), 254);
        out.writeByte(1);
        out.writeShort(npc_index);
        out.writeByte(0 >> 16);
        out.writeByte(0 >> 8);
        out.writeByte(0);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender animateObject(int x, int y, int z, int animationID)
    {
        RSObject object = RegionClipping.getRSObject(player, x, y, z);
        if (object == null)
            return this;
        sendCoords(new Position(x, y, z));
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(5);
        out.writeHeader(player.getEncryptor(), 160);
        out.writeByte(0, StreamBuffer.ValueType.S);
        out.writeByte((object.getType() << 2) + (object.getFace() & 3), StreamBuffer.ValueType.S);
        out.writeShort(animationID, StreamBuffer.ValueType.A);
        player.send(out.getBuffer());
        return this;
    }

    /**
     * Orient : 2 Middle ? 3 West ? 4 East ? 5 South ? 6 North.
     */
    public ActionSender createObjectHints(int x, int y, int height, int pos)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(7);
        out.writeHeader(player.getEncryptor(), 254);
        out.writeByte(pos);
        out.writeShort(x);
        out.writeShort(y);
        out.writeByte(height);
        player.send(out.getBuffer());
        return this;
    }

    public ActionSender moveComponent(int x, int y, int componentId)
    {
        StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(7);
        out.writeHeader(player.getEncryptor(), 70);
        out.writeShort(x);
        out.writeShort(y);
        out.writeShort(componentId, StreamBuffer.ByteOrder.LITTLE);
        player.send(out.getBuffer());
        return this;
    }

    public void updateSpecialBar()
    {
        sendConfig(300, (int) (player.getSpecialAmount() * 100));
        sendConfig(301, player.isSpecialAttackActive() ? 1 : 0);
    }
}