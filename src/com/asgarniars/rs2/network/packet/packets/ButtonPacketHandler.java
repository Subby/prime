package com.asgarniars.rs2.network.packet.packets;

import java.util.logging.Logger;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.content.EmoteActions;
import com.asgarniars.rs2.content.Skillcapes;
import com.asgarniars.rs2.content.combat.magic.Magic;
import com.asgarniars.rs2.content.combat.ranged.RangedDataLoader;
import com.asgarniars.rs2.content.combat.util.Prayer;
import com.asgarniars.rs2.content.combat.util.SpecialAttack;
import com.asgarniars.rs2.content.combat.util.CombatState.CombatStyle;
import com.asgarniars.rs2.content.duel.DuelArena;
import com.asgarniars.rs2.content.skills.craft.ArmourCreation;
import com.asgarniars.rs2.content.skills.craft.Craftable;
import com.asgarniars.rs2.content.skills.craft.CraftingType;
import com.asgarniars.rs2.content.skills.craft.Gem;
import com.asgarniars.rs2.content.skills.craft.GemCutting;
import com.asgarniars.rs2.content.skills.craft.Hide;
import com.asgarniars.rs2.content.skills.fletch.ArrowCreation;
import com.asgarniars.rs2.content.skills.fletch.ArrowTips;
import com.asgarniars.rs2.content.skills.fletch.BowCreation;
import com.asgarniars.rs2.content.skills.fletch.CreationType;
import com.asgarniars.rs2.content.skills.fletch.FletchableLogs;
import com.asgarniars.rs2.content.skills.fletch.HeadlessArrowCreation;
import com.asgarniars.rs2.content.skills.fletch.LogFletching;
import com.asgarniars.rs2.content.skills.fletch.UnstrungBows;
import com.asgarniars.rs2.content.skills.herbalism.Herbalism;
import com.asgarniars.rs2.content.skills.herbalism.IngredientType;
import com.asgarniars.rs2.content.skills.herbalism.PrimaryIngredient;
import com.asgarniars.rs2.content.skills.herbalism.SecondaryIngredient;
import com.asgarniars.rs2.content.skills.magic.BoneConversion;
import com.asgarniars.rs2.content.skills.magic.Charge;
import com.asgarniars.rs2.content.skills.magic.Teleother;
import com.asgarniars.rs2.content.skills.magic.Teleportation;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.Entity.AttackTypes;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.model.players.Player.TeleotherStage;
import com.asgarniars.rs2.network.packet.Packet;
import com.asgarniars.rs2.network.packet.PacketManager.PacketHandler;
import com.asgarniars.rs2.util.Misc;
import com.asgarniars.rs2.util.ScriptManager;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class ButtonPacketHandler implements PacketHandler
{

    public static final int BUTTON = 185;

    private static final Logger logger = Logger.getLogger(ButtonPacketHandler.class.getName());

    @Override
    public void handlePacket(Player player, Packet packet)
    {
        handleButton(player, packet.getIn().readShort());
    }

    private void handleButton(final Player player, int buttonId)
    {
        if (player.isDebugging())
        {
            player.getActionSender().sendMessage("button : " + buttonId);
        }

        Player otherPlayer;

        switch (buttonId)
        {

        case 6674:
            DuelArena.getInstance().accept_stage_1(player);
            break;

        case 6520:
            DuelArena.getInstance().accept_stage_2(player);
            break;

        /**
         * Start of duel configuring... duel_button_ranged = null=OFF, 0=ON
         * 
         * TODO: Confirm that everything applies how No Ranged rule is done.
         */
        case 6698: // -- No ranged
        case 6725:

            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            if (player.getAttribute("duel_button_melee") != null && player.getAttribute("duel_button_magic") != null && player.getAttribute("duel_button_ranged") == null)
            {
                player.getActionSender().sendMessage("You must have atleast one combat style to use.");
                return;
            }

            // -- Forfeiting fix
            if (player.getAttribute("duel_button_forfeit") != null && player.getAttribute("duel_button_melee") != null)
            {

                player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") - Constants.duel_rule_configs[0]));
                player.toggleAttribute("duel_button_forfeit");
                player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

                otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
                if (otherPlayer != null)
                {
                    otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                    otherPlayer.toggleAttribute("duel_button_forfeit");
                    otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
                }
            }

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_ranged") == null ? Constants.duel_rule_configs[2] : -Constants.duel_rule_configs[2])));
            player.toggleAttribute("duel_button_ranged");
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_ranged");
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }

            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT"))
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;

        case 6699: // -- No melee
        case 6726:
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            if (player.getAttribute("duel_button_melee") == null && player.getAttribute("duel_button_magic") != null && player.getAttribute("duel_button_ranged") != null)
            {
                player.getActionSender().sendMessage("You must have atleast one combat style to use.");
                return;
            }

            // -- Forfeiting fix
            if (player.getAttribute("duel_button_forfeit") != null && player.getAttribute("duel_button_melee") == null)
            {

                player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") - Constants.duel_rule_configs[0]));
                player.toggleAttribute("duel_button_forfeit");
                player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

                otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
                if (otherPlayer != null)
                {
                    otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                    otherPlayer.toggleAttribute("duel_button_forfeit");
                    otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
                }
            }

            // -- Ranged + weapon disabled fix
            if (player.getAttribute("duel_button_weapon") != null && player.getAttribute("duel_button_ranged") == null && player.getAttribute("duel_button_magic") != null)
            {

                player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") - Constants.duel_rule_configs[14]));
                player.toggleAttribute("duel_button_weapon");
                player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

                otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
                if (otherPlayer != null)
                {
                    otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                    otherPlayer.toggleAttribute("duel_button_weapon");
                    otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
                }
            }

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_melee") == null ? Constants.duel_rule_configs[3] : -Constants.duel_rule_configs[3])));
            player.toggleAttribute("duel_button_melee");
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_melee");
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }
            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;

        case 6697: // -- No magic
        case 6727:
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            if (player.getAttribute("duel_button_melee") != null && player.getAttribute("duel_button_magic") == null && player.getAttribute("duel_button_ranged") != null)
            {
                player.getActionSender().sendMessage("You must have atleast one combat style to use.");
                return;
            }

            // -- Forfeiting fix
            if (player.getAttribute("duel_button_forfeit") != null && player.getAttribute("duel_button_melee") != null)
            {

                player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") - Constants.duel_rule_configs[0]));
                player.toggleAttribute("duel_button_forfeit");
                player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

                otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
                if (otherPlayer != null)
                {
                    otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                    otherPlayer.toggleAttribute("duel_button_forfeit");
                    otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
                }
            }

            // -- Ranged + weapon disabled fix
            if (player.getAttribute("duel_button_weapon") != null && player.getAttribute("duel_button_ranged") == null && player.getAttribute("duel_button_melee") != null)
            {

                player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") - Constants.duel_rule_configs[14]));
                player.toggleAttribute("duel_button_weapon");
                player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

                otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
                if (otherPlayer != null)
                {
                    otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                    otherPlayer.toggleAttribute("duel_button_weapon");
                    otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
                }
            }

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_magic") == null ? Constants.duel_rule_configs[4] : -Constants.duel_rule_configs[4])));
            player.toggleAttribute("duel_button_magic");
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_magic");
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }
            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;

        case 7817: // -- No sp atk
        case 7816:
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_sp_atk") == null ? Constants.duel_rule_configs[10] : -Constants.duel_rule_configs[10])));
            player.toggleAttribute("duel_button_sp_atk");
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_sp_atk");
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }
            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;

        case 669: // -- Fun weapons
        case 670:
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            if (player.getAttribute("duel_button_weapon") != null)
            {
                player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") - Constants.duel_rule_configs[14]));
                player.toggleAttribute("duel_button_weapon");
                player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

                otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
                if (otherPlayer != null)
                {
                    otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                    otherPlayer.toggleAttribute("duel_button_weapon");
                    otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
                }
            }

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_fun_wep") == null ? Constants.duel_rule_configs[9] : -Constants.duel_rule_configs[9])));
            player.toggleAttribute("duel_button_fun_wep");
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_fun_wep");
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }
            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;

        case 6696: // -- No forfeit
        case 6721:
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            if (player.getAttribute("duel_button_movement") != null)
            {
                player.getActionSender().sendMessage("You can't toggle forfeiting off with no movement.");
                return;
            }

            if (player.getAttribute("duel_button_melee") != null && (player.getAttribute("duel_button_magic") == null || player.getAttribute("duel_button_ranged") == null))
            {
                player.getActionSender().sendMessage("You can't have no forfeit and no melee - you could run out of ammo.");
                return;
            }

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_forfeit") == null ? Constants.duel_rule_configs[0] : -Constants.duel_rule_configs[0])));
            player.toggleAttribute("duel_button_forfeit");
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_forfeit");
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }
            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;

        case 6701: // -- No drinks
        case 6728:
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_drinks") == null ? Constants.duel_rule_configs[5] : -Constants.duel_rule_configs[5])));
            player.toggleAttribute("duel_button_drinks");
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_drinks");
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }
            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;

        case 6702: // -- No food
        case 6729:
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_food") == null ? Constants.duel_rule_configs[6] : -Constants.duel_rule_configs[6])));
            player.toggleAttribute("duel_button_food");
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_food");
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }
            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;

        case 6703: // -- No prayer
        case 6730:
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_prayer") == null ? Constants.duel_rule_configs[7] : -Constants.duel_rule_configs[7])));
            player.toggleAttribute("duel_button_prayer");
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_prayer");
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }
            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;

        case 6704: // -- No movement
        case 6722:
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            // -- Forfeit fix
            if (player.getAttribute("duel_button_forfeit") != null)
            {
                player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") - Constants.duel_rule_configs[0]));
                player.toggleAttribute("duel_button_forfeit");
                player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

                otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
                if (otherPlayer != null)
                {
                    otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                    otherPlayer.toggleAttribute("duel_button_forfeit");
                    otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
                }
            }

            // -- Forfeit fix
            if (player.getAttribute("duel_button_obstacles") != null)
            {
                player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") - Constants.duel_rule_configs[8]));
                player.toggleAttribute("duel_button_obstacles");
                player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

                otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
                if (otherPlayer != null)
                {
                    otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                    otherPlayer.toggleAttribute("duel_button_obstacles");
                    otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
                }
            }

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_movement") == null ? Constants.duel_rule_configs[1] : -Constants.duel_rule_configs[1])));
            player.toggleAttribute("duel_button_movement");
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_movement");
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }
            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;

        case 6731: // -- Obstacles
        case 6732:
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            if (player.getAttribute("duel_button_movement") != null)
            {
                player.getActionSender().sendMessage("You can't go into the obstacle arena with no movement.");
                return;
            }

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_obstacles") == null ? Constants.duel_rule_configs[8] : -Constants.duel_rule_configs[8])));
            player.toggleAttribute("duel_button_obstacles");
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_obstacles");
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }
            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;

        case 13813: // -- Helmet
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));

            // Our player have a equiped item? Have space?
            if (player.getEquipment().get(0) != null)
            {

                int free_inv_slots = player.getInventory().getItemContainer().freeSlots();

                int weapon_count = player.getEquipment().get(3) == null ? 0 : player.getEquipment().get(3).getCount();
                int arrows_count = player.getEquipment().get(3) == null ? 0 : player.getEquipment().get(3).getCount();

                int weapon_count_inv = player.getEquipment().get(3) == null ? 0 : player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()) != null ? player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()).getCount() : 0;
                int arrows_count_inv = player.getEquipment().get(13) == null ? 0 : player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()).getCount();

                int weapon_count_duel_inv = player.getEquipment().get(3) == null ? 0 : player.getDuelInventory().contains(player.getEquipment().get(3).getId()) ? player.getDuelInventory().getById(player.getEquipment().get(3).getId()).getCount() : 0;
                int arrows_count_duel_inv = player.getEquipment().get(13) == null ? 0 : player.getDuelInventory().contains(player.getEquipment().get(13).getId()) ? player.getDuelInventory().getById(player.getEquipment().get(13).getId()).getCount() : 0;

                int weapon_count_other_duel_inv = otherPlayer.getEquipment().get(3) == null ? 0 : otherPlayer.getDuelInventory().contains(player.getEquipment().get(3).getId()) ? otherPlayer.getDuelInventory().getById(player.getEquipment().get(3).getId()).getCount() : 0;
                int arrows_count_other_duel_inv = otherPlayer.getEquipment().get(13) == null ? 0 : otherPlayer.getDuelInventory().contains(player.getEquipment().get(13).getId()) ? otherPlayer.getDuelInventory().getById(player.getEquipment().get(13).getId()).getCount() : 0;

                int required_space = (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_helmet") != null ? -1 : 0);

                if (weapon_count != 0)
                {
                    if (weapon_count_inv + weapon_count <= 0)
                        required_space += 1;
                    else
                        required_space -= 1;
                }

                if (free_inv_slots <= required_space)
                {
                    player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
                    return;
                }
            }

            // Opponent have a equiped item? Have space?
            if (otherPlayer.getEquipment().get(0) != null)
            {
                if (otherPlayer.getInventory().getItemContainer().freeSlots() <= ((Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_helmet") != null ? -1 : 0) + (otherPlayer.getEquipment().get(13) != null && otherPlayer.getAttribute("duel_button_arrows") != null && otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()) != null && otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount() + (otherPlayer.getDuelInventory().contains(otherPlayer.getEquipment().get(13).getId()) ? otherPlayer.getDuelInventory().getById(otherPlayer.getEquipment().get(13).getId()).getCount() : 0) + (player.getDuelInventory().contains(otherPlayer.getEquipment().get(13).getId()) ? player.getDuelInventory().getById(otherPlayer.getEquipment().get(13).getId()).getCount() : 0) <= 0 ? 0 : -1) + (otherPlayer.getEquipment().get(3) != null && otherPlayer.getAttribute("duel_button_weapon") != null && otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()) != null && otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount() + (otherPlayer.getDuelInventory().contains(otherPlayer.getEquipment().get(3).getId()) ? otherPlayer.getDuelInventory().getById(otherPlayer.getEquipment().get(3).getId()).getCount() : 0) + (player.getDuelInventory().contains(otherPlayer.getEquipment().get(3).getId()) ? player.getDuelInventory().getById(otherPlayer.getEquipment().get(3).getId()).getCount() : 0) <= 0 ? 0 : -1)))
                {
                    player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
                    return;
                }
            }

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_helmet") == null ? Constants.duel_rule_configs[11] : -Constants.duel_rule_configs[11])));
            player.toggleAttribute("duel_button_helmet");
            if (player.getEquipment().get(0) != null)
                player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_helmet") != null ? 1 : -1));
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                if (otherPlayer.getEquipment().get(0) != null)
                    otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_helmet") != null ? 1 : -1));
                otherPlayer.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space"));
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }
            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;
        case 13814: // -- Cape
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));

            // Our player have a equiped item? Have space?
            if (player.getEquipment().get(1) != null)
            {
                if (player.getInventory().getItemContainer().freeSlots() <= ((Integer) player.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (player.getAttribute("duel_button_cape") != null ? -1 : 0) + (player.getEquipment().get(13) != null && player.getAttribute("duel_button_arrows") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()).getCount() + player.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (player.getEquipment().get(3) != null && player.getAttribute("duel_button_weapon") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()).getCount() + player.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0)))
                {
                    player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
                    return;
                }
            }

            // Opponent have a equiped item? Have space?
            if (otherPlayer.getEquipment().get(1) != null)
            {
                if (otherPlayer.getInventory().getItemContainer().freeSlots() <= ((Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (otherPlayer.getAttribute("duel_button_cape") != null ? -1 : 0) + (otherPlayer.getEquipment().get(13) != null && otherPlayer.getAttribute("duel_button_arrows") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (otherPlayer.getEquipment().get(3) != null && otherPlayer.getAttribute("duel_button_weapon") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0)))
                {
                    player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
                    return;
                }
            }

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_cape") == null ? Constants.duel_rule_configs[12] : -Constants.duel_rule_configs[12])));
            player.toggleAttribute("duel_button_cape");
            if (player.getEquipment().get(1) != null)
                player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_cape") != null ? 1 : -1));
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_cape");
                if (otherPlayer.getEquipment().get(1) != null)
                    otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_cape") != null ? 1 : -1));
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }
            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;
        case 13815: // -- Amulet
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));

            // Our player have a equiped item? Have space?
            if (player.getEquipment().get(2) != null)
            {
                if (player.getInventory().getItemContainer().freeSlots() <= ((Integer) player.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (player.getAttribute("duel_button_amulet") != null ? -1 : 0) + (player.getEquipment().get(13) != null && player.getAttribute("duel_button_arrows") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()).getCount() + player.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (player.getEquipment().get(3) != null && player.getAttribute("duel_button_weapon") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()).getCount() + player.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0)))
                {
                    player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
                    return;
                }
            }

            // Opponent have a equiped item? Have space?
            if (otherPlayer.getEquipment().get(2) != null)
            {
                if (otherPlayer.getInventory().getItemContainer().freeSlots() <= ((Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (otherPlayer.getAttribute("duel_button_amulet") != null ? -1 : 0) + (otherPlayer.getEquipment().get(13) != null && otherPlayer.getAttribute("duel_button_arrows") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (otherPlayer.getEquipment().get(3) != null && otherPlayer.getAttribute("duel_button_weapon") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0)))
                {
                    player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
                    return;
                }
            }

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_amulet") == null ? Constants.duel_rule_configs[13] : -Constants.duel_rule_configs[13])));
            player.toggleAttribute("duel_button_amulet");
            if (player.getEquipment().get(2) != null)
                player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_amulet") != null ? 1 : -1));
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_amulet");
                if (otherPlayer.getEquipment().get(2) != null)
                    otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_amulet") != null ? 1 : -1));
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }
            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;
        case 13816: // -- Arrows
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));

            // Our player have a equiped item? Have space?
            if (player.getEquipment().get(13) != null)
            {
                if (player.getInventory().getItemContainer().freeSlots() + ((player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()) != null && !(player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()).getCount() + player.getEquipment().get(13).getCount() < 1)) ? 1 : 0) < (Integer) player.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (player.getAttribute("duel_button_arrows") != null ? -1 : 1) || !player.getInventory().hasRoomFor(player.getEquipment().get(13)) || (player.getDuelInventory().getById(player.getEquipment().get(13).getId()) != null && Integer.MAX_VALUE - (player.getDuelInventory().getById(player.getEquipment().get(13).getId()).getCount() + player.getEquipment().get(13).getCount()) <= 0))
                {
                    player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
                    return;
                }
            }

            // Opponent have a equiped item? Have space?
            if (otherPlayer.getEquipment().get(13) != null)
            {
                if (otherPlayer.getInventory().getItemContainer().freeSlots() + ((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()) != null && !(otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount() < 1)) ? 1 : 0) < (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (otherPlayer.getAttribute("duel_button_arrows") != null ? -1 : 1) || !otherPlayer.getInventory().hasRoomFor(otherPlayer.getEquipment().get(13)) || (otherPlayer.getDuelInventory().getById(otherPlayer.getEquipment().get(13).getId()) != null && Integer.MAX_VALUE - (otherPlayer.getDuelInventory().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount()) <= 0))
                {
                    player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
                    return;
                }
            }

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_arrows") == null ? Constants.duel_rule_configs[21] : -Constants.duel_rule_configs[21])));
            player.toggleAttribute("duel_button_arrows");
            if (player.getEquipment().get(13) != null)
                player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_arrows") != null ? 1 : -1));
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_arrows");
                if (otherPlayer.getEquipment().get(13) != null)
                    otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_arrows") != null ? 1 : -1));
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }
            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;
        case 13817: // -- Weapon
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            if (player.getAttribute("duel_button_magic") != null && player.getAttribute("duel_button_melee") != null)
            {
                player.getActionSender().sendMessage("You can't disable weapon slot if you're using ranged combat.");
                return;
            }

            if (player.getAttribute("duel_button_fun_wep") != null)
            {
                player.getActionSender().sendMessage("You can't disable weapon slot if you're going to use fun weapons.");
                return;
            }

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
            // Our player have a equiped item? Have space?
            if (player.getEquipment().get(3) != null)
            {
                if (player.getInventory().getItemContainer().freeSlots() + (player.getEquipment().get(3).getDefinition().isStackable() && player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()) != null && !(player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()).getCount() + player.getEquipment().get(3).getCount() < 1) ? 1 : 0) < (Integer) player.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (player.getAttribute("duel_button_weapon") != null ? -1 : 1) || (player.getEquipment().get(3).getDefinition().isStackable() && !player.getInventory().hasRoomFor(player.getEquipment().get(3)) || (player.getDuelInventory().getById(player.getEquipment().get(3).getId()) != null && Integer.MAX_VALUE - (player.getDuelInventory().getById(player.getEquipment().get(3).getId()).getCount() + player.getEquipment().get(3).getCount()) <= 0)))
                {
                    player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
                    return;
                }
            }

            // Opponent have a equiped item? Have space?
            if (otherPlayer.getEquipment().get(3) != null)
            {
                if (otherPlayer.getInventory().getItemContainer().freeSlots() + (otherPlayer.getEquipment().get(3).getDefinition().isStackable() && otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()) != null && !(otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount() < 1) ? 1 : 0) < (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (otherPlayer.getAttribute("duel_button_weapon") != null ? -1 : 1) || (otherPlayer.getEquipment().get(3).getDefinition().isStackable() && !otherPlayer.getInventory().hasRoomFor(otherPlayer.getEquipment().get(3)) || (otherPlayer.getDuelInventory().getById(otherPlayer.getEquipment().get(3).getId()) != null && Integer.MAX_VALUE - (otherPlayer.getDuelInventory().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount()) <= 0)))
                {
                    player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
                    return;
                }
            }

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_weapon") == null ? Constants.duel_rule_configs[14] : -Constants.duel_rule_configs[14])));
            player.toggleAttribute("duel_button_weapon");
            if (player.getEquipment().get(3) != null)
                player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_weapon") != null ? 1 : -1));
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_weapon");
                if (otherPlayer.getEquipment().get(3) != null)
                    otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_weapon") != null ? 1 : -1));
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }
            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;
        case 13818: // -- Chest
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));

            // Our player have a equiped item? Have space?
            if (player.getEquipment().get(4) != null)
            {
                if (player.getInventory().getItemContainer().freeSlots() <= ((Integer) player.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (player.getAttribute("duel_button_chest") != null ? -1 : 0) + (player.getEquipment().get(13) != null && player.getAttribute("duel_button_arrows") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()).getCount() + player.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (player.getEquipment().get(3) != null && player.getAttribute("duel_button_weapon") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()).getCount() + player.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0)))
                {
                    player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
                    return;
                }
            }

            // Opponent have a equiped item? Have space?
            if (otherPlayer.getEquipment().get(4) != null)
            {
                if (otherPlayer.getInventory().getItemContainer().freeSlots() <= ((Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (otherPlayer.getAttribute("duel_button_chest") != null ? -1 : 0) + (otherPlayer.getEquipment().get(13) != null && otherPlayer.getAttribute("duel_button_arrows") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (otherPlayer.getEquipment().get(3) != null && otherPlayer.getAttribute("duel_button_weapon") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0)))
                {
                    player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
                    return;
                }
            }

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_chest") == null ? Constants.duel_rule_configs[15] : -Constants.duel_rule_configs[15])));
            player.toggleAttribute("duel_button_chest");
            if (player.getEquipment().get(4) != null)
                player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_chest") != null ? 1 : -1));
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_chest");
                if (otherPlayer.getEquipment().get(4) != null)
                    otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_chest") != null ? 1 : -1));
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }
            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;
        case 13819: // -- Shield
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));

            // Our player have a equiped item? Have space?
            if (player.getEquipment().get(5) != null)
            {
                if (player.getInventory().getItemContainer().freeSlots() <= ((Integer) player.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (player.getAttribute("duel_button_shield") != null ? -1 : 0) + (player.getEquipment().get(13) != null && player.getAttribute("duel_button_arrows") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()).getCount() + player.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (player.getEquipment().get(3) != null && player.getAttribute("duel_button_weapon") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()).getCount() + player.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0)))
                {
                    player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
                    return;
                }
            }

            // Opponent have a equiped item? Have space?
            if (otherPlayer.getEquipment().get(5) != null)
            {
                if (otherPlayer.getInventory().getItemContainer().freeSlots() <= ((Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (otherPlayer.getAttribute("duel_button_shield") != null ? -1 : 0) + (otherPlayer.getEquipment().get(13) != null && otherPlayer.getAttribute("duel_button_arrows") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (otherPlayer.getEquipment().get(3) != null && otherPlayer.getAttribute("duel_button_weapon") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0)))
                {
                    player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
                    return;
                }
            }

            if (player.getAttribute("duel_button_shield") == null)
                player.getActionSender().sendMessage("Beware: You won't be able to use two-handed weapons such as bows.");

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_shield") == null ? Constants.duel_rule_configs[16] : -Constants.duel_rule_configs[16])));
            player.toggleAttribute("duel_button_shield");
            if (player.getEquipment().get(5) != null)
                player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_shield") != null ? 1 : -1));
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_shield");
                if (otherPlayer.getEquipment().get(5) != null)
                    otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_shield") != null ? 1 : -1));
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }

            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;
        case 13820: // -- Legs
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));

            // Our player have a equiped item? Have space?
            if (player.getEquipment().get(7) != null)
            {
                if (player.getInventory().getItemContainer().freeSlots() <= ((Integer) player.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (player.getAttribute("duel_button_legs") != null ? -1 : 0) + (player.getEquipment().get(13) != null && player.getAttribute("duel_button_arrows") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()).getCount() + player.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (player.getEquipment().get(3) != null && player.getAttribute("duel_button_weapon") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()).getCount() + player.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0)))
                {
                    player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
                    return;
                }
            }

            // Opponent have a equiped item? Have space?
            if (otherPlayer.getEquipment().get(7) != null)
            {
                if (otherPlayer.getInventory().getItemContainer().freeSlots() <= ((Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (otherPlayer.getAttribute("duel_button_legs") != null ? -1 : 0) + (otherPlayer.getEquipment().get(13) != null && otherPlayer.getAttribute("duel_button_arrows") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (otherPlayer.getEquipment().get(3) != null && otherPlayer.getAttribute("duel_button_weapon") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0)))
                {
                    player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
                    return;
                }
            }

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_legs") == null ? Constants.duel_rule_configs[17] : -Constants.duel_rule_configs[17])));
            player.toggleAttribute("duel_button_legs");
            if (player.getEquipment().get(7) != null)
                player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_legs") != null ? 1 : -1));
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_legs");
                if (otherPlayer.getEquipment().get(7) != null)
                    otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_legs") != null ? 1 : -1));
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }
            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;
        case 13823: // -- Gloves
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));

            // Our player have a equiped item? Have space?
            if (player.getEquipment().get(9) != null)
            {
                if (player.getInventory().getItemContainer().freeSlots() <= ((Integer) player.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (player.getAttribute("duel_button_gloves") != null ? -1 : 0) + (player.getEquipment().get(13) != null && player.getAttribute("duel_button_arrows") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()).getCount() + player.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (player.getEquipment().get(3) != null && player.getAttribute("duel_button_weapon") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()).getCount() + player.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0)))
                {
                    player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
                    return;
                }
            }

            // Opponent have a equiped item? Have space?
            if (otherPlayer.getEquipment().get(9) != null)
            {
                if (otherPlayer.getInventory().getItemContainer().freeSlots() <= ((Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (otherPlayer.getAttribute("duel_button_gloves") != null ? -1 : 0) + (otherPlayer.getEquipment().get(13) != null && otherPlayer.getAttribute("duel_button_arrows") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (otherPlayer.getEquipment().get(3) != null && otherPlayer.getAttribute("duel_button_weapon") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0)))
                {
                    player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
                    return;
                }
            }

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_gloves") == null ? Constants.duel_rule_configs[18] : -Constants.duel_rule_configs[18])));
            player.toggleAttribute("duel_button_gloves");
            if (player.getEquipment().get(9) != null)
                player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_gloves") != null ? 1 : -1));
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_gloves");
                if (otherPlayer.getEquipment().get(9) != null)
                    otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_gloves") != null ? 1 : -1));
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }
            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;
        case 13822: // -- Boots
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));

            // Our player have a equiped item? Have space?
            if (player.getEquipment().get(10) != null)
            {
                if (player.getInventory().getItemContainer().freeSlots() <= ((Integer) player.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (player.getAttribute("duel_button_boots") != null ? -1 : 0) + (player.getEquipment().get(13) != null && player.getAttribute("duel_button_arrows") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()).getCount() + player.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (player.getEquipment().get(3) != null && player.getAttribute("duel_button_weapon") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()).getCount() + player.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0)))
                {
                    player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
                    return;
                }
            }

            // Opponent have a equiped item? Have space?
            if (otherPlayer.getEquipment().get(10) != null)
            {
                if (otherPlayer.getInventory().getItemContainer().freeSlots() <= ((Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (otherPlayer.getAttribute("duel_button_boots") != null ? -1 : 0) + (otherPlayer.getEquipment().get(13) != null && otherPlayer.getAttribute("duel_button_arrows") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (otherPlayer.getEquipment().get(3) != null && otherPlayer.getAttribute("duel_button_weapon") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0)))
                {
                    player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
                    return;
                }
            }

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_boots") == null ? Constants.duel_rule_configs[19] : -Constants.duel_rule_configs[19])));
            player.toggleAttribute("duel_button_boots");
            if (player.getEquipment().get(10) != null)
                player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_boots") != null ? 1 : -1));
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_boots");
                if (otherPlayer.getEquipment().get(10) != null)
                    otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_boots") != null ? 1 : -1));
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }
            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;
        case 13821: // -- Ring
            if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT")
                return;

            otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));

            // Our player have a equiped item? Have space?
            if (player.getEquipment().get(12) != null)
            {
                if (player.getInventory().getItemContainer().freeSlots() <= ((Integer) player.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (player.getAttribute("duel_button_ring") != null ? -1 : 0) + (player.getEquipment().get(13) != null && player.getAttribute("duel_button_arrows") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()).getCount() + player.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (player.getEquipment().get(3) != null && player.getAttribute("duel_button_weapon") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()).getCount() + player.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0)))
                {
                    player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
                    return;
                }
            }

            // Opponent have a equiped item? Have space?
            if (otherPlayer.getEquipment().get(12) != null)
            {
                if (otherPlayer.getInventory().getItemContainer().freeSlots() <= ((Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (otherPlayer.getAttribute("duel_button_ring") != null ? -1 : 0) + (otherPlayer.getEquipment().get(13) != null && otherPlayer.getAttribute("duel_button_arrows") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (otherPlayer.getEquipment().get(3) != null && otherPlayer.getAttribute("duel_button_weapon") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0)))
                {
                    player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
                    return;
                }
            }

            player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_ring") == null ? Constants.duel_rule_configs[20] : -Constants.duel_rule_configs[20])));
            player.toggleAttribute("duel_button_ring");
            if (player.getEquipment().get(12) != null)
                player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_ring") != null ? 1 : -1));
            player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));

            if (otherPlayer != null)
            {
                otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
                otherPlayer.toggleAttribute("duel_button_ring");
                if (otherPlayer.getEquipment().get(12) != null)
                    otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_ring") != null ? 1 : -1));
                otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
            }
            if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null)
            {
                player.setAttribute("duel_stage", "FIRST_WINDOW");
                otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
                player.getActionSender().sendString("", 6684);
                otherPlayer.getActionSender().sendString("", 6684);
            }
            break;

        case 2461: // 2 options, option 1
        case 2471: // 3 options, option 1
        case 2482: // 4 options, option 1
        case 2494: // 5 options, option 1
            player.getCurrentDialogue().click(0, player);
            break;
        case 2462: // 2 options, option 2
        case 2472: // 3 options, option 2
        case 2483: // 4 options, option 2
        case 2495: // 5 options, option 2
            player.getCurrentDialogue().click(1, player);
            break;
        case 2473: // 3 options, option 3
        case 2484: // 4 options, option 3
        case 2496: // 5 options, option 3
            player.getCurrentDialogue().click(2, player);
            break;
        case 2485: // 4 options, option 4
        case 2497: // 5 options, option 4
            player.getCurrentDialogue().click(3, player);
            break;
        case 2498: // 5 options, option 5
            player.getCurrentDialogue().click(4, player);
            break;

        // TODO Temporary skill leveling.
        case 8654:
            player.setEnterXId(0);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;
        case 8655:
            player.setEnterXId(3);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;
        case 8657:
            player.setEnterXId(2);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;
        case 8660:
            player.setEnterXId(1);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;
        case 8663:
            player.setEnterXId(4);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;
        case 8666:
            player.setEnterXId(5);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;
        case 8669:
            player.setEnterXId(6);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;
        case 8672:
            player.setEnterXId(20);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;
        case 6996:
            player.setEnterXId(21);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;

        case 8658:
            player.setEnterXId(16);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;
        case 8661:
            player.setEnterXId(15);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;
        case 8664:
            player.setEnterXId(17);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;
        case 8667:
            player.setEnterXId(12);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;
        case 8670:
            player.setEnterXId(9);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;
        case 12162:
            player.setEnterXId(18);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;
        case 8656:
            player.setEnterXId(14);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;
        case 8659:
            player.setEnterXId(13);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;
        case 8662:
            player.setEnterXId(10);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;
        case 8665:
            player.setEnterXId(7);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;
        case 8668:
            player.setEnterXId(11);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;
        case 8671:
            player.setEnterXId(8);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;
        case 13928:
            player.setEnterXId(19);
            player.setEnterXInterfaceId(1337);
            player.getActionSender().sendEnterX();
            break;

        case 12856:
        case 1164:
        case 9845:
        case 1167:
        case 1170:
        case 1174:
        case 1540:
        case 1541:
        case 7455:
        case 18470:
        case 13035:
        case 13045:
        case 13053:
        case 13061:
        case 13069:
        case 13079:
        case 13087:
        case 13095:
            Teleportation.getInstance().activateTeleportButton(player, buttonId);
            break;
        case 1193: // charge
            Charge.handleCharge(player);
            break;
        case 175:
            Skillcapes.doSkillcape(player);
            break;
        case 1159:
        case 15877:
            BoneConversion.convert(player, buttonId);
            break;
        case 12566: // Teleother accept
            if (player.getTeleotherStage() == TeleotherStage.RECEIVED_REQUEST)
            {
                int playerId = (Integer) player.getAttribute("teleotherPartnerId");
                Player caster = World.getPlayer(playerId);
                if (caster != null)
                {
                    if (Misc.getDistance(player.getPosition(), caster.getPosition()) > 10)
                    {
                        Teleother.handleDecline(player);
                        return;
                    }
                    Teleother.handleAccept(caster, player, (Integer) player.getAttribute("teleotherSpellId"));
                }
            }
            break;
        case 12568: // Teleother decline
            Teleother.handleDecline(player);
            break;
        case 1757:
        case 1772:
        case 4454:
            if (player.getEquipment().get(3) != null && RangedDataLoader.getProjectileId(player.getEquipment().get(3).getId()) == null)
                return;
            player.setAttackType(AttackTypes.RANGED);
            player.getCombatState().setCombatStyle(CombatStyle.ACCURATE);
            break;
        case 1756:
        case 1771:
        case 4453:
            if (player.getEquipment().get(3) != null && RangedDataLoader.getProjectileId(player.getEquipment().get(3).getId()) == null)
                return;
            player.setAttackType(AttackTypes.RANGED);
            player.getCombatState().setCombatStyle(CombatStyle.AGGRESSIVE);
            break;
        case 1755:
        case 1770:
        case 4452:
            if (player.getEquipment().get(3) != null && RangedDataLoader.getProjectileId(player.getEquipment().get(3).getId()) == null)
                return;
            player.setAttackType(AttackTypes.RANGED);
            player.getCombatState().setCombatStyle(CombatStyle.DEFENSIVE);
            break;
        case 335:
        case 433:
        case 785:
        case 1707:
        case 2285:
        case 2432:
        case 3805:
        case 4714:
        case 5579:
        case 6137:
        case 7771:
        case 12298:
        case 5862:
            if (buttonId == 335)
                Magic.getInstance().resetAll(player, false);

            if (player.getEquipment().get(3) != null && RangedDataLoader.getProjectileId(player.getEquipment().get(3).getId()) != null)
                return;
            player.setAttackType(AttackTypes.MELEE);
            player.getCombatState().setCombatStyle(CombatStyle.ACCURATE);
            break;
        case 334:
        case 432:
        case 784:
        case 1706:
        case 2284:
        case 2431:
        case 3804:
        case 4713:
        case 5578:
        case 5861:
        case 6136:
        case 7770:
            if (buttonId == 334)
                Magic.getInstance().resetAll(player, false);

            if (player.getEquipment().get(3) != null && RangedDataLoader.getProjectileId(player.getEquipment().get(3).getId()) != null)
                return;
            player.setAttackType(AttackTypes.MELEE);
            player.getCombatState().setCombatStyle(CombatStyle.AGGRESSIVE);
            break;
        case 1704:
        case 4711:
        case 5576:
        case 782:
        case 2282:
            if (player.getEquipment().get(3) != null && RangedDataLoader.getProjectileId(player.getEquipment().get(3).getId()) != null)
                return;
            player.setAttackType(AttackTypes.MELEE);
            player.getCombatState().setCombatStyle(CombatStyle.AGGRESSIVE_1);
            break;
        case 336:
        case 431:
        case 783:
        case 1705:
        case 2283:
        case 2430:
        case 3803:
        case 4686:
        case 4712:
        case 1080:
        case 5577:
        case 5860:
        case 6135:
        case 7769:
        case 12296:
            if (buttonId == 336)
                Magic.getInstance().resetAll(player, false);

            if (player.getEquipment().get(3) != null && RangedDataLoader.getProjectileId(player.getEquipment().get(3).getId()) != null)
                return;
            player.setAttackType(AttackTypes.MELEE);
            player.getCombatState().setCombatStyle(CombatStyle.DEFENSIVE);
            break;
        case 4688:
        case 8468:
            if (player.getEquipment().get(3) != null && RangedDataLoader.getProjectileId(player.getEquipment().get(3).getId()) != null)
                return;
            player.setAttackType(AttackTypes.MELEE);
            player.getCombatState().setCombatStyle(CombatStyle.CONTROLLED_1);
            break;
        case 4687:
        case 12297:
            if (player.getEquipment().get(3) != null && RangedDataLoader.getProjectileId(player.getEquipment().get(3).getId()) != null)
                return;
            player.setAttackType(AttackTypes.MELEE);
            player.getCombatState().setCombatStyle(CombatStyle.CONTROLLED_2);
            break;
        case 4685:
        case 2429:
        case 3802:
        case 7768:
            if (player.getEquipment().get(3) != null && RangedDataLoader.getProjectileId(player.getEquipment().get(3).getId()) != null)
                return;
            player.setAttackType(AttackTypes.MELEE);
            player.getCombatState().setCombatStyle(CombatStyle.CONTROLLED_3);
            break;
        case 150:
            player.setAutoRetaliate((!player.shouldAutoRetaliate()));
            break;
        case 14922:
            player.getActionSender().removeInterfaces();
            break;
        case 3546:
            player.getTrading().acceptStageTwo(player);
            break;
        case 3420:
            player.getTrading().acceptStageOne(player);
            break;
        case 3651:
            player.setAppearanceUpdateRequired(true);
            player.getUpdateFlags().setUpdateRequired(true);
            player.setAttribute("hasDesigned", (byte) 0);
            player.getActionSender().removeInterfaces();
            break;
        case 13030:
            if (player.getCombatTimer() > 0)
            {
                player.getActionSender().sendMessage("You can't logout until 10 seconds after the end of combat.");
                return;
            }
            player.getActionSender().sendLogout();
            break;
        case 1747: // Single model - Make all
            if (player.getAttribute("craftingType") != null)
            {
                switch ((CraftingType) player.getAttribute("craftingType"))
                {
                case GEM_CRAFTING:
                    player.getActionDeque().addAction(new GemCutting(player, (short) player.getInventory().getItemContainer().getCount(((Gem) player.getAttribute("craftingGem")).getUncutGem()), (Gem) player.getAttribute("craftingGem")));
                    player.getActionSender().removeInterfaces();
                    player.removeAttribute("craftingType");
                    player.removeAttribute("craftingGem");
                    player.getActionSender().moveComponent(0, 0, 1746);
                    break;
                default:
                    logger.severe("Unhandled Crafting Type : " + player.getAttribute("craftingType"));
                    break;

                }
            }

            else if (player.getAttribute("herbloreType") != null)
            {
                switch ((IngredientType) player.getAttribute("herbloreType"))
                {
                case PRIMARY_INGREDIENT:
                    player.getActionDeque().addAction(new Herbalism(player, (short) player.getInventory().getItemContainer().getCount(((PrimaryIngredient) player.getAttribute("herbloreIngredient")).getId()), (IngredientType) player.getAttribute("herbloreType"), (PrimaryIngredient) player.getAttribute("herbloreIngredient"), null));
                    player.getActionSender().removeInterfaces();
                    player.removeAttribute("herbloreIngredient");
                    player.removeAttribute("herbloreType");
                    break;
                case SECONDARY_INGREDIENT:
                    player.getActionDeque().addAction(new Herbalism(player, (short) player.getInventory().getItemContainer().getCount(((SecondaryIngredient) player.getAttribute("herbloreIngredient")).getId()), IngredientType.SECONDARY_INGREDIENT, null, (SecondaryIngredient) player.getAttribute("herbloreIngredient")));
                    player.getActionSender().removeInterfaces();
                    player.removeAttribute("herbloreIngredient");
                    player.removeAttribute("herbloreType");
                    break;
                default:
                    logger.severe("Unhandled Ingredient Type : " + player.getAttribute("herbloreType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            else if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case BOW_CREATION:
                    player.getActionDeque().addAction(new BowCreation(player, (short) player.getInventory().getItemContainer().getCount(((UnstrungBows) player.getAttribute("fletchingItem")).getItem()), (UnstrungBows) player.getAttribute("fletchingItem")));
                    player.getActionSender().removeInterfaces();
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingItem");
                    player.getActionSender().moveComponent(0, 0, 1746);
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            break;
        case 1748: // Single model interface Enter 'X'
            player.setEnterXInterfaceId(1748);
            player.getActionSender().sendEnterX();
            break;
        case 2798: // Single model interface - make 5
            if (player.getAttribute("craftingType") != null)
            {
                switch ((CraftingType) player.getAttribute("craftingType"))
                {
                case GEM_CRAFTING:
                    player.getActionDeque().addAction(new GemCutting(player, (short) 5, (Gem) player.getAttribute("craftingGem")));
                    player.getActionSender().removeInterfaces();
                    player.removeAttribute("craftingType");
                    player.removeAttribute("craftingGem");
                    player.getActionSender().moveComponent(0, 0, 1746);
                    break;
                default:
                    logger.severe("Unhandled Crafting Type : " + player.getAttribute("craftingType"));
                    break;

                }
            }
            else if (player.getAttribute("herbloreType") != null)
            {
                switch ((IngredientType) player.getAttribute("herbloreType"))
                {
                case PRIMARY_INGREDIENT:
                    player.getActionDeque().addAction(new Herbalism(player, (short) 5, (IngredientType) player.getAttribute("herbloreType"), (PrimaryIngredient) player.getAttribute("herbloreIngredient"), null));
                    player.getActionSender().removeInterfaces();
                    break;
                case SECONDARY_INGREDIENT:
                    player.getActionDeque().addAction(new Herbalism(player, (short) 5, IngredientType.SECONDARY_INGREDIENT, null, (SecondaryIngredient) player.getAttribute("herbloreIngredient")));
                    player.getActionSender().removeInterfaces();
                    player.removeAttribute("herbloreIngredient");
                    player.removeAttribute("herbloreType");
                    break;
                default:
                    logger.severe("Unhandled Ingredient Type : " + player.getAttribute("herbloreType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            else if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case BOW_CREATION:
                    player.getActionDeque().addAction(new BowCreation(player, (short) 5, (UnstrungBows) player.getAttribute("fletchingItem")));
                    player.getActionSender().removeInterfaces();
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingItem");
                    player.getActionSender().moveComponent(0, 0, 1746);
                    break;
                default:
                    logger.severe("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            break;
        case 2799:// single interface make 1 - josh
            if (player.getAttribute("herbloreType") != null)
            {
                switch ((IngredientType) player.getAttribute("herbloreType"))
                {
                case PRIMARY_INGREDIENT:
                    player.getActionDeque().addAction(new Herbalism(player, (short) 1, (IngredientType) player.getAttribute("herbloreType"), (PrimaryIngredient) player.getAttribute("herbloreIngredient"), null));
                    player.getActionSender().removeInterfaces();
                    player.removeAttribute("herbloreIngredient");
                    player.removeAttribute("herbloreType");
                    break;
                case SECONDARY_INGREDIENT:
                    player.getActionDeque().addAction(new Herbalism(player, (short) 1, IngredientType.SECONDARY_INGREDIENT, null, (SecondaryIngredient) player.getAttribute("herbloreIngredient")));
                    player.getActionSender().removeInterfaces();
                    player.removeAttribute("herbloreIngredient");
                    player.removeAttribute("herbloreType");
                    break;
                default:
                    logger.severe("Unhandled Ingredient Type : " + player.getAttribute("herbloreType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            else if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case ARROW_CREATION:
                    player.getActionDeque().addAction(new ArrowCreation(player, (Short) player.getAttribute("creationAmount"), (ArrowTips) player.getAttribute("arrowTip")));
                    player.getActionSender().removeInterfaces();
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("arrowTip");
                    player.removeAttribute("creationAmount");
                    player.getActionSender().moveComponent(0, 0, 1746);
                    break;
                case BOW_CREATION:
                    player.getActionDeque().addAction(new BowCreation(player, (short) 1, (UnstrungBows) player.getAttribute("fletchingItem")));
                    player.getActionSender().removeInterfaces();
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingItem");
                    player.getActionSender().moveComponent(0, 0, 1746);
                    break;
                case HEADLESS_ARROW_CREATION:
                    player.getActionDeque().addAction(new HeadlessArrowCreation(player, (Short) player.getAttribute("creationAmount")));
                    player.getActionSender().removeInterfaces();
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("creationAmount");
                    player.getActionSender().moveComponent(0, 0, 1746);
                    break;
                default:
                    logger.severe("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    player.getActionSender().moveComponent(0, 0, 1746);
                    break;

                }
            }
            else if (player.getAttribute("craftingType") != null)
            {
                switch ((CraftingType) player.getAttribute("craftingType"))
                {
                case GEM_CRAFTING:
                    player.getActionDeque().addAction(new GemCutting(player, (short) 1, (Gem) player.getAttribute("craftingGem")));
                    player.getActionSender().removeInterfaces();
                    player.removeAttribute("craftingType");
                    player.removeAttribute("craftingGem");
                    player.getActionSender().moveComponent(0, 0, 1746);
                    break;
                default:
                    logger.severe("Unhandled Crafting Type : " + player.getAttribute("craftingType"));
                    break;
                }
            }
            else
            {
                logger.severe("Cannot find handler for button " + buttonId);
            }
            break;
        case 5386:
            player.setAttribute("BANK_MODE_NOTE", (byte) 0);
            break;
        case 5387:
            player.removeAttribute("BANK_MODE_NOTE");
            break;
        case 8130:
            player.removeAttribute("BANK_MODE_INSERT");
            break;
        case 8131:
            player.setAttribute("BANK_MODE_INSERT", (byte) 0);
            break;
        case 8886:
            // Tri-model interface - make x (first model)
            player.setEnterXInterfaceId(8886);
            player.getActionSender().sendEnterX();
            break;
        case 8887: // Tri-model interface - make 10 (first model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 10, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8883);
                    player.getActionSender().moveComponent(0, 0, 8884);
                    player.getActionSender().moveComponent(0, 0, 8885);
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            else if (player.getAttribute("craftingType") != null)
            {
                switch ((CraftingType) player.getAttribute("craftingType"))
                {
                case ARMOUR_CREATION:
                    if (player.getAttribute("craftingHide") == null)
                    {
                        return;
                    }
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new ArmourCreation(player, (short) 10, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[0])));
                    player.removeAttribute("craftingHide");
                    player.removeAttribute("craftingType");
                    break;
                default:
                    if (player.isDebugging())
                    {
                        player.getActionSender().sendMessage("Unhandled crafting type : " + player.getAttribute("craftingType"));
                    }
                    logger.severe("Unhandled crafting type : " + player.getAttribute("craftingType"));
                }
            }
            break;
        case 8888: // Tri-model interface - make 5 (first model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 5, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8883);
                    player.getActionSender().moveComponent(0, 0, 8884);
                    player.getActionSender().moveComponent(0, 0, 8885);
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            else if (player.getAttribute("craftingType") != null)
            {
                switch ((CraftingType) player.getAttribute("craftingType"))
                {
                case ARMOUR_CREATION:
                    if (player.getAttribute("craftingHide") == null)
                    {
                        return;
                    }
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new ArmourCreation(player, (short) 5, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[0])));
                    player.removeAttribute("craftingHide");
                    player.removeAttribute("craftingType");
                    break;
                default:
                    if (player.isDebugging())
                    {
                        player.getActionSender().sendMessage("Unhandled crafting type : " + player.getAttribute("craftingType"));
                    }
                    logger.severe("Unhandled crafting type : " + player.getAttribute("craftingType"));
                }
            }
            break;
        case 8890:
            // Tri-model interface - make x (second model)
            player.setEnterXInterfaceId(8890);
            player.getActionSender().sendEnterX();
            break;
        case 8891:// Tri-model interface - make 10 (second model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 10, 2, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8883);
                    player.getActionSender().moveComponent(0, 0, 8884);
                    player.getActionSender().moveComponent(0, 0, 8885);
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            else if (player.getAttribute("craftingType") != null)
            {
                switch ((CraftingType) player.getAttribute("craftingType"))
                {
                case ARMOUR_CREATION:
                    if (player.getAttribute("craftingHide") == null)
                    {
                        return;
                    }
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new ArmourCreation(player, (short) 10, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[1])));
                    player.removeAttribute("craftingHide");
                    player.removeAttribute("craftingType");
                    break;
                default:
                    if (player.isDebugging())
                    {
                        player.getActionSender().sendMessage("Unhandled crafting type : " + player.getAttribute("craftingType"));
                    }
                    logger.severe("Unhandled crafting type : " + player.getAttribute("craftingType"));
                }
            }
            break;
        case 8892:// Tri-model interface - make 5 (second model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 5, 2, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8883);
                    player.getActionSender().moveComponent(0, 0, 8884);
                    player.getActionSender().moveComponent(0, 0, 8885);
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            else if (player.getAttribute("craftingType") != null)
            {
                switch ((CraftingType) player.getAttribute("craftingType"))
                {
                case ARMOUR_CREATION:
                    if (player.getAttribute("craftingHide") == null)
                    {
                        return;
                    }
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new ArmourCreation(player, (short) 5, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[1])));
                    player.removeAttribute("craftingHide");
                    player.removeAttribute("craftingType");
                    break;
                default:
                    if (player.isDebugging())
                    {
                        player.getActionSender().sendMessage("Unhandled crafting type : " + player.getAttribute("craftingType"));
                    }
                    logger.severe("Unhandled crafting type : " + player.getAttribute("craftingType"));
                }
            }
            break;
        case 8893:// Tri-model interface - make 1 (second model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 1, 2, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8883);
                    player.getActionSender().moveComponent(0, 0, 8884);
                    player.getActionSender().moveComponent(0, 0, 8885);
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            else if (player.getAttribute("craftingType") != null)
            {
                switch ((CraftingType) player.getAttribute("craftingType"))
                {
                case ARMOUR_CREATION:
                    if (player.getAttribute("craftingHide") == null)
                    {
                        return;
                    }
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new ArmourCreation(player, (short) 1, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[1])));
                    player.removeAttribute("craftingHide");
                    player.removeAttribute("craftingType");
                    break;
                default:
                    if (player.isDebugging())
                    {
                        player.getActionSender().sendMessage("Unhandled crafting type : " + player.getAttribute("craftingType"));
                    }
                    logger.severe("Unhandled crafting type : " + player.getAttribute("craftingType"));
                }
            }
            break;
        case 8894:
            // Tri-model interface - make x (first model)
            player.setEnterXInterfaceId(8894);
            player.getActionSender().sendEnterX();
            break;
        case 8895:// Tri-model interface - make 10 (third model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 10, 1, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            else if (player.getAttribute("craftingType") != null)
            {
                switch ((CraftingType) player.getAttribute("craftingType"))
                {
                case ARMOUR_CREATION:
                    if (player.getAttribute("craftingHide") == null)
                    {
                        return;
                    }
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new ArmourCreation(player, (short) 10, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[2])));
                    player.removeAttribute("craftingHide");
                    player.removeAttribute("craftingType");
                    break;
                default:
                    if (player.isDebugging())
                    {
                        player.getActionSender().sendMessage("Unhandled crafting type : " + player.getAttribute("craftingType"));
                    }
                    logger.severe("Unhandled crafting type : " + player.getAttribute("craftingType"));
                }
            }
            break;
        case 8896:// Tri-model interface - make 5 (third model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 5, 1, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            else if (player.getAttribute("craftingType") != null)
            {
                switch ((CraftingType) player.getAttribute("craftingType"))
                {
                case ARMOUR_CREATION:
                    if (player.getAttribute("craftingHide") == null)
                    {
                        return;
                    }
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new ArmourCreation(player, (short) 5, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[2])));
                    player.removeAttribute("craftingHide");
                    player.removeAttribute("craftingType");
                    break;
                default:
                    if (player.isDebugging())
                    {
                        player.getActionSender().sendMessage("Unhandled crafting type : " + player.getAttribute("craftingType"));
                    }
                    logger.severe("Unhandled crafting type : " + player.getAttribute("craftingType"));
                }
            }
            break;
        case 8897:// Tri-model interface - make 1 (third model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 1, 1, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8883);
                    player.getActionSender().moveComponent(0, 0, 8884);
                    player.getActionSender().moveComponent(0, 0, 8885);
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            else if (player.getAttribute("craftingType") != null)
            {
                switch ((CraftingType) player.getAttribute("craftingType"))
                {
                case ARMOUR_CREATION:
                    if (player.getAttribute("craftingHide") == null)
                    {
                        return;
                    }
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new ArmourCreation(player, (short) 1, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[2])));
                    player.removeAttribute("craftingHide");
                    player.removeAttribute("craftingType");
                    break;
                default:
                    if (player.isDebugging())
                    {
                        player.getActionSender().sendMessage("Unhandled crafting type : " + player.getAttribute("craftingType"));
                    }
                    logger.severe("Unhandled crafting type : " + player.getAttribute("craftingType"));
                }
            }
            break;
        case 8889: // Tri-model interface - make 1 (first model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 1, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8883);
                    player.getActionSender().moveComponent(0, 0, 8884);
                    player.getActionSender().moveComponent(0, 0, 8885);
                    System.out.println("Cleared all fletching");
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    player.getActionSender().moveComponent(0, 0, 8883);
                    player.getActionSender().moveComponent(0, 0, 8884);
                    player.getActionSender().moveComponent(0, 0, 8885);
                    break;

                }
            }
            else if (player.getAttribute("craftingType") != null)
            {
                switch ((CraftingType) player.getAttribute("craftingType"))
                {
                case ARMOUR_CREATION:
                    if (player.getAttribute("craftingHide") == null)
                    {
                        return;
                    }
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new ArmourCreation(player, (short) 1, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[0])));
                    player.removeAttribute("craftingHide");
                    player.removeAttribute("craftingType");
                    break;
                default:
                    if (player.isDebugging())
                    {
                        player.getActionSender().sendMessage("Unhandled crafting type : " + player.getAttribute("craftingType"));
                    }
                    logger.severe("Unhandled crafting type : " + player.getAttribute("craftingType"));
                }
            }
            break;
        case 8906:// quad-model interface - make 'x' (first model)
            player.setEnterXInterfaceId(8906);
            player.getActionSender().sendEnterX();
            break;
        case 8907:// quad-model interface - make 10 (first model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 10, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8902);
                    player.getActionSender().moveComponent(0, 0, 8903);
                    player.getActionSender().moveComponent(0, 0, 8904);
                    player.getActionSender().moveComponent(0, 0, 8905);
                    System.out.println("Cleared all fletching");
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;
                }
            }
            break;
        case 8908:// quad-model interface - make 5 (first model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 5, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8902);
                    player.getActionSender().moveComponent(0, 0, 8903);
                    player.getActionSender().moveComponent(0, 0, 8904);
                    player.getActionSender().moveComponent(0, 0, 8905);
                    System.out.println("Cleared all fletching");
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;
                }
            }
            break;
        case 8909: // quad-model interface - make 1 (first model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 1, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8902);
                    player.getActionSender().moveComponent(0, 0, 8903);
                    player.getActionSender().moveComponent(0, 0, 8904);
                    player.getActionSender().moveComponent(0, 0, 8905);
                    System.out.println("Cleared all fletching");
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;
                }
            }
            break;
        case 8910:// quad-model interface - make 'x' (second model)
            player.setEnterXInterfaceId(8910);
            player.getActionSender().sendEnterX();
            break;
        case 8911:// quad-model interface - make 10 (second model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 10, 1, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8902);
                    player.getActionSender().moveComponent(0, 0, 8903);
                    player.getActionSender().moveComponent(0, 0, 8904);
                    player.getActionSender().moveComponent(0, 0, 8905);
                    System.out.println("Cleared all fletching");
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            break;
        case 8912:// quad-model interface - make 5 (second model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 5, 1, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8902);
                    player.getActionSender().moveComponent(0, 0, 8903);
                    player.getActionSender().moveComponent(0, 0, 8904);
                    player.getActionSender().moveComponent(0, 0, 8905);
                    System.out.println("Cleared all fletching");
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            break;
        case 8913: // quad-model interface - make 1 (second model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 1, 1, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8902);
                    player.getActionSender().moveComponent(0, 0, 8903);
                    player.getActionSender().moveComponent(0, 0, 8904);
                    player.getActionSender().moveComponent(0, 0, 8905);
                    System.out.println("Cleared all fletching");
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            break;
        case 8914:// quad-model interface - make 'x' (third model)
            player.setEnterXInterfaceId(8914);
            player.getActionSender().sendEnterX();
            break;
        case 8915:// quad-model interface - make 10 (third model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 10, 2, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8902);
                    player.getActionSender().moveComponent(0, 0, 8903);
                    player.getActionSender().moveComponent(0, 0, 8904);
                    player.getActionSender().moveComponent(0, 0, 8905);
                    System.out.println("Cleared all fletching");
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            break;
        case 8916:// quad-model interface - make 5 (third model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 5, 2, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8902);
                    player.getActionSender().moveComponent(0, 0, 8903);
                    player.getActionSender().moveComponent(0, 0, 8904);
                    player.getActionSender().moveComponent(0, 0, 8905);
                    System.out.println("Cleared all fletching");
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            break;
        case 8917: // quad-model interface - make 1 (third model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 1, 2, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8902);
                    player.getActionSender().moveComponent(0, 0, 8903);
                    player.getActionSender().moveComponent(0, 0, 8904);
                    player.getActionSender().moveComponent(0, 0, 8905);
                    System.out.println("Cleared all fletching");
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            break;
        case 8918:// quad-model interface - make 'x' (fourth model)
            player.setEnterXInterfaceId(8918);
            player.getActionSender().sendEnterX();
            break;
        case 8919:// quad-model interface - make 10 (fourth model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 10, 3, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8902);
                    player.getActionSender().moveComponent(0, 0, 8903);
                    player.getActionSender().moveComponent(0, 0, 8904);
                    player.getActionSender().moveComponent(0, 0, 8905);
                    System.out.println("Cleared all fletching");
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;
                }
            }
            break;
        case 8920:// quad-model interface - make 5 (fourth model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 5, 3, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8902);
                    player.getActionSender().moveComponent(0, 0, 8903);
                    player.getActionSender().moveComponent(0, 0, 8904);
                    player.getActionSender().moveComponent(0, 0, 8905);
                    System.out.println("Cleared all fletching");
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            break;
        case 8921: // quad-model interface - make 1 (fourth model)
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, 1, 3, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8902);
                    player.getActionSender().moveComponent(0, 0, 8903);
                    player.getActionSender().moveComponent(0, 0, 8904);
                    player.getActionSender().moveComponent(0, 0, 8905);
                    System.out.println("Cleared all fletching");
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            break;

        case 12858: // Mouse buttons
            player.setAttribute("mouseButtons", (byte) ((Byte) player.getAttribute("mouseButtons") == 1 ? 0 : 1));
            break;
        case 12859: // Chat effects
            player.setAttribute("chatEffects", (byte) ((Byte) player.getAttribute("chatEffects") == 1 ? 0 : 1));
            break;
        case 1195: // Split private chat
            player.setAttribute("splitPrivate", (byte) ((Byte) player.getAttribute("splitPrivate") == 1 ? 0 : 1));
            break;
        case 1196: // Accept aid'
            player.setAttribute("acceptAid", (byte) ((Byte) player.getAttribute("acceptAid") == 1 ? 0 : 1));
            break;
        case 1197: // Run toggle
            if (player.getAttribute("runLocked") == null)
            {
                if (player.getAttribute("isRunning") == null)
                {
                    player.toggleRunMode(true);
                }
                else
                {
                    player.toggleRunMode(false);
                }
            }
            break;
        default:
            String scriptName = "actionButton" + buttonId;
            if (!ScriptManager.getSingleton().invokeWithFailTest(scriptName, player))
            {
                logger.severe("Unhandled action button: " + buttonId);
            }
            break;
        }
        /*
         * Below looks absolutely horrible to me - josh Agreed to fully extend
         * -akzu, gonna clean this soon.
         */
        Prayer.getInstance().setPrayers(player, buttonId);
        switch (buttonId)
        {
        case 7462:
        case 7487:
        case 7587:
        case 7537:
        case 7562:// 2276
        case 7612:// 3796
        case 7662:// 4679
        case 7687:// 4705
        case 7788:// 7762
        case 8481:// 8460
        case 12311:// 12290
            SpecialAttack.getInstance().clickSpecialBar(player, buttonId);
            break;
        }
        Magic.getInstance().handleAutocastButtons(player, buttonId);
        player.getBankPin().clickPinButton(buttonId);
        player.getBankPin().handleGeneralButtons(buttonId);
        EmoteActions.getInstance().activateEmoteButton(player, buttonId);
    }
}
