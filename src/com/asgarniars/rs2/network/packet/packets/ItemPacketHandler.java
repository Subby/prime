package com.asgarniars.rs2.network.packet.packets;

import java.util.logging.Logger;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.content.Explosive;
import com.asgarniars.rs2.content.TeleportTabs;
import com.asgarniars.rs2.content.consumables.Food;
import com.asgarniars.rs2.content.consumables.FoodLoader;
import com.asgarniars.rs2.content.consumables.Potion;
import com.asgarniars.rs2.content.consumables.PotionLoader;
import com.asgarniars.rs2.content.duel.DuelArena;
import com.asgarniars.rs2.content.shop.Shop;
import com.asgarniars.rs2.content.skills.craft.CraftingType;
import com.asgarniars.rs2.content.skills.craft.Gem;
import com.asgarniars.rs2.content.skills.craft.GemCutting;
import com.asgarniars.rs2.content.skills.craft.Hide;
import com.asgarniars.rs2.content.skills.firecraft.FireCraft;
import com.asgarniars.rs2.content.skills.firecraft.LogType;
import com.asgarniars.rs2.content.skills.fletch.ArrowCreation;
import com.asgarniars.rs2.content.skills.fletch.ArrowTips;
import com.asgarniars.rs2.content.skills.fletch.BowCreation;
import com.asgarniars.rs2.content.skills.fletch.CreationType;
import com.asgarniars.rs2.content.skills.fletch.FletchableLogs;
import com.asgarniars.rs2.content.skills.fletch.HeadlessArrowCreation;
import com.asgarniars.rs2.content.skills.fletch.UnstrungBows;
import com.asgarniars.rs2.content.skills.herbalism.HerbIdentifier;
import com.asgarniars.rs2.content.skills.herbalism.HerbType;
import com.asgarniars.rs2.content.skills.herbalism.Herbalism;
import com.asgarniars.rs2.content.skills.herbalism.IngredientType;
import com.asgarniars.rs2.content.skills.herbalism.PrimaryIngredient;
import com.asgarniars.rs2.content.skills.herbalism.SecondaryIngredient;
import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.model.players.BankManager;
import com.asgarniars.rs2.model.players.GroundItem;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.model.players.Player.TradeStage;
import com.asgarniars.rs2.network.StreamBuffer;
import com.asgarniars.rs2.network.StreamBuffer.ValueType;
import com.asgarniars.rs2.network.packet.Packet;
import com.asgarniars.rs2.network.packet.PacketManager.PacketHandler;
import com.asgarniars.rs2.task.Task;
import com.asgarniars.rs2.util.Language;
import com.asgarniars.rs2.util.Misc;
import com.asgarniars.rs2.util.ScriptManager;

/**
 * Refactoring and security checks by Ares & AkZu.
 * 
 * @author Joshua Barry <Ares>
 * @author AkZu
 * 
 */
public class ItemPacketHandler implements PacketHandler
{

    public static final int ITEM_OPERATE = 75;
    public static final int DROP_ITEM = 87;
    public static final int PICKUP_ITEM = 236;
    public static final int HANDLE_OPTIONS = 214;
    public static final int PACKET_145 = 145;
    public static final int PACKET_117 = 117;
    public static final int PACKET_43 = 43;
    public static final int PACKET_129 = 129;
    public static final int EQUIP_ITEM = 41;
    public static final int USE_ITEM_ON_ITEM = 53;
    public static final int FIRST_CLICK_ITEM = 122;
    public static final int THIRD_CLICK_ITEM = 16;
    private static final Logger logger = Logger.getLogger(ItemPacketHandler.class.getName());

    @Override
    public void handlePacket(Player player, Packet packet)
    {
        if (player.getTradeStage().equals(TradeStage.SECOND_TRADE_WINDOW) || player.getTradeStage().equals(TradeStage.ACCEPT))
        {
            player.getActionSender().sendMessage(Language.CANT_DO);
            return;
        }
        if (player.isDebugging())
        {
            player.getActionSender().sendMessage("Item packet : <col=ff0000>" + packet + "</col>");
            player.getActionSender().sendMessage("packet opcode : <col=ff0000>" + packet.getOpcode() + "</col> packet length : <col=ff0000>" + packet.getPacketLength() + "</col>");
        }
        switch (packet.getOpcode())
        {
        case USE_ITEM_ON_ITEM:
            useItemOnItem(player, packet);
            break;
        case ITEM_OPERATE:
            handleAlternativeItemOption(player, packet);
            break;
        case DROP_ITEM:
            handleDropItem(player, packet);
            break;
        case PICKUP_ITEM:
            handlePickupItem(player, packet);
            break;
        case HANDLE_OPTIONS:
            handleOptions(player, packet);
            break;
        case PACKET_145:
            handleInterfaceAction1(player, packet);
            break;
        case PACKET_117:
            handleInterfaceAction2(player, packet);
            break;
        case PACKET_43:
            handleInterfaceAction3(player, packet);
            break;
        case PACKET_129:
            handleInterfaceAction4(player, packet);
            break;
        case EQUIP_ITEM:
            handleEquipItem(player, packet);
            break;
        case FIRST_CLICK_ITEM:
            handleFirstClickItem(player, packet);
            break;
        case THIRD_CLICK_ITEM:
            handleThirdClickItem(player, packet);
            break;
        }
    }

    /**
     * TODO read in vals
     * 
     * THIS IS NOT ITEM OPERATE -Aki (Item operate is interfaceActionMenuSlot3)
     * 
     * @param player
     * @param packet
     */
    private void handleAlternativeItemOption(Player player, Packet packet)
    {
        /*
         * int interfaceId = packet.getIn().readShort(true, ValueType.A,
         * ByteOrder.LITTLE); int unknown = packet.getIn().readShort(true,
         * ValueType.A); // Must be // slot packet.getIn().readShort(true,
         * ValueType.A); // Item Id, only use slot.
         */
    }

    /**
     * Working Clienth4x fix - <Ares>
     * 
     * @param player
     * @param packet
     */
    private void handleDropItem(Player player, Packet packet)
    {
        packet.getIn().readShort(ValueType.A);
        packet.getIn().readShort();

        int itemSlot = packet.getIn().readShort(ValueType.A);

        Item item = player.getInventory().get(itemSlot);

        if (item == null)
            return;

        // -- Check for pre-existing stackable items on current tile (check only
        // for visible items(?)..
        if (item.getDefinition().isStackable())
        {
            for (GroundItem other : player.getRegion().getTile(player.getPosition()).getGroundItems())
            {
                if (other.getPos().getX() == player.getPosition().getX() && other.getPos().getY() == player.getPosition().getY() && other.getItem().getId() == item.getId())
                {
                    if (!other.isRespawn() && (other.isGlobal() || (other.getOwner() != null && other.getOwner().equals(player.getUsername()))))
                    {

                        int allowed_amt = Integer.MAX_VALUE - other.getItem().getCount();

                        if (allowed_amt <= 0)
                        {
                            player.stopAllActions(true);
                            player.getActionSender().sendMessage("The floor doesn't have enough space to hold that item.");
                            return;
                        }

                        if (allowed_amt > item.getCount())
                            allowed_amt = item.getCount();

                        other.getItem().setCount(other.getItem().getCount() + allowed_amt);
                        player.stopAllActions(true);
                        player.getInventory().removeItem(new Item(item.getId(), allowed_amt));
                        return;
                    }
                }
            }
        }

        switch (item.getId())
        {
        case 4045:
            Explosive.explode(player);
            break;
        default:
            String itemName = ItemManager.getInstance().getItemName(item.getId()).toLowerCase().replace(" ", "_");
            String scriptName = "dropItem_".concat(itemName);
            if (!ScriptManager.getSingleton().invokeWithFailTest(scriptName, player, item))
                ItemManager.getInstance().createGroundItem(player, player.getUsername(), new Item(item.getId(), item.getCount()), new Position(player.getPosition().getX(), player.getPosition().getY(), player.getPosition().getZ()), ItemManager.getInstance().isUntradeable(item.getId()) ? Constants.GROUND_START_TIME_UNTRADEABLE : Constants.GROUND_START_TIME);
            break;
        }

        player.stopAllActions(true);
        player.getInventory().removeItemSlot(itemSlot);
    }

    /**
     * Working Clienth4x fix.
     * 
     * @author Ares
     * @param player
     * @param packet
     */
    private void useItemOnItem(Player player, Packet packet)
    {
        int firstSlot = packet.getIn().readShort();
        int secondSlot = packet.getIn().readShort(StreamBuffer.ValueType.A);

        Item usedWith = player.getInventory().get(firstSlot);
        Item itemUsed = player.getInventory().get(secondSlot);

        if (usedWith == null || itemUsed == null)
        {
            return;
        }

        /**
         * Cooking
         */
        /*
         * Pineapple Pizza
         */
        if (usedWith.getId() == 2118 || itemUsed.getId() == 2118)
        {
            if (itemUsed.getId() == 2118 && usedWith.getId() == 2289)
            {
                player.getInventory().removeItem(new Item(2118));
                player.getInventory().removeItem(new Item(2289));
                player.getInventory().addItem(new Item(2301));
            }
            else if (itemUsed.getId() == 2289 && usedWith.getId() == 2118)
            {
                player.getInventory().removeItem(new Item(2118));
                player.getInventory().removeItem(new Item(2289));
                player.getInventory().addItem(new Item(2301));
            }
            else
            {
                Logger.getAnonymousLogger().info("unhandled cooking technique.");
            }
        }

        if (usedWith.getId() == 590 || itemUsed.getId() == 590)
        {
            // sneakyhearts
            int logId = usedWith.getId() == 590 ? itemUsed.getId() : usedWith.getId();

            LogType logType = LogType.forId(logId);

            if (logType == null)
                return;

            player.getActionDeque().addAction(new FireCraft(player, logType, 0));

        }

        /*
         * Meat Pizza
         */
        if (usedWith.getId() == 2142 || itemUsed.getId() == 2142)
        {
            if (itemUsed.getId() == 2142 && usedWith.getId() == 2289)
            {
                player.getInventory().removeItem(new Item(2142));
                player.getInventory().removeItem(new Item(2289));
                player.getInventory().addItem(new Item(2293));
            }
            else if (itemUsed.getId() == 2289 && usedWith.getId() == 2142)
            {
                player.getInventory().removeItem(new Item(2142));
                player.getInventory().removeItem(new Item(2289));
                player.getInventory().addItem(new Item(2293));
            }
            else
            {
                Logger.getAnonymousLogger().info("unhandled cooking technique.");
            }
        }
        /*
         * Anchovy Pizza
         */
        if (usedWith.getId() == 319 || itemUsed.getId() == 319)
        {
            if (itemUsed.getId() == 319 && usedWith.getId() == 2289)
            {
                player.getInventory().removeItem(new Item(319));
                player.getInventory().removeItem(new Item(2289));
                player.getInventory().addItem(new Item(2297));
            }
            else if (itemUsed.getId() == 2289 && usedWith.getId() == 319)
            {
                player.getInventory().removeItem(new Item(319));
                player.getInventory().removeItem(new Item(2289));
                player.getInventory().addItem(new Item(2297));
            }
            else
            {
                Logger.getAnonymousLogger().info("unhandled cooking technique.");
            }
        }

        /*
         * Molten glass on glass blowing pipe
         */

        if (usedWith.getId() == 1785 || itemUsed.getId() == 1785)
        {
            if (usedWith.getId() == 1785 && itemUsed.getId() == 1775)
            {
                player.getActionSender().sendInterface(11462);
            }
            else if (itemUsed.getId() == 1785 && usedWith.getId() == 1775)
            {
                player.getActionSender().sendInterface(11462);
            }
            else
            {
                Logger.getAnonymousLogger().info("unhandled glass blowing technique.");
            }
        }

        /*
         * Needle on leather
         */
        if (usedWith.getId() == 1733 || itemUsed.getId() == 1733)
        {
            Hide hide;
            if (usedWith.getId() == 1733)
            {
                hide = Hide.forReward((short) itemUsed.getId());
            }
            else
            {
                hide = Hide.forReward((short) usedWith.getId());
            }
            System.out.println("WORKING WITH HIDE : " + hide);
            if (hide != null)
            {
                player.setAttribute("craftingType", CraftingType.ARMOUR_CREATION);
                player.setAttribute("craftingHide", hide);
                if (hide.getItemId() == 1739)
                {
                    player.getActionSender().sendInterface(2311);
                }
                else if (ItemManager.getInstance().getItemName(hide.getOutcome()).toLowerCase().contains("snake"))
                {
                    // TODO: snakeskin;
                }
                else
                {
                    String prefix = ItemManager.getInstance().getItemName(hide.getOutcome()).split(" ")[0];
                    player.getActionSender().sendItemOnInterface(8883, 170, hide.getCraftableOutcomes()[0]);
                    player.getActionSender().sendItemOnInterface(8884, 170, hide.getCraftableOutcomes()[1]);
                    player.getActionSender().sendItemOnInterface(8885, 170, hide.getCraftableOutcomes()[2]);
                    player.getActionSender().moveComponent(0, -10, 8883);
                    player.getActionSender().moveComponent(0, -10, 8884);
                    player.getActionSender().moveComponent(0, -10, 8885);
                    player.getActionSender().sendString("\\n \\n \\n \\n".concat(prefix + " body"), 8889);
                    player.getActionSender().sendString("\\n \\n \\n \\n".concat(prefix + " vambraces"), 8893);
                    player.getActionSender().sendString("\\n \\n \\n \\n".concat(prefix + " chaps"), 8897);
                    player.getActionSender().sendChatInterface(8880);
                }
            }
        }

        if (usedWith.getId() == 1755 || itemUsed.getId() == 1755)
        {
            Gem gem;
            if (usedWith.getId() == 1755)
            {
                gem = Gem.forId(itemUsed.getId());
            }
            else
            {
                gem = Gem.forId(usedWith.getId());
            }
            if (gem != null)
            {
                if (player.getInventory().getItemContainer().getCount(gem.getUncutGem()) > 1)
                {
                    player.getActionSender().sendString("\\n \\n \\n \\n" + ItemManager.getInstance().getItemName(gem.getCutGem()), 2799);
                    player.getActionSender().moveComponent(0, -5, 1746);
                    player.getActionSender().sendItemOnInterface(1746, 175, gem.getCutGem());
                    player.getActionSender().sendChatInterface(4429);
                    player.setAttribute("craftingType", CraftingType.GEM_CRAFTING);
                    player.setAttribute("craftingGem", gem);
                }
                else
                {
                    player.getActionDeque().addAction(new GemCutting(player, (short) 1, gem));
                }
            }
        }

        if (usedWith.getId() == 52 || itemUsed.getId() == 52)
        {
            int lowestAmount = player.getInventory().getItemContainer().getCount(52) < player.getInventory().getItemContainer().getCount(314) ? player.getInventory().getItemContainer().getCount(52) : player.getInventory().getItemContainer().getCount(314);
            if (player.getInventory().getItemContainer().getCount(52) > 4 && player.getInventory().getItemContainer().getCount(314) > 4)
            {
                player.getActionSender().sendString("\\n \\n \\n \\n" + ItemManager.getInstance().getItemName(53), 2799);
                player.getActionSender().moveComponent(0, -15, 1746);
                player.getActionSender().sendItemOnInterface(1746, 175, 53);
                player.getActionSender().sendChatInterface(4429);
                player.setAttribute("creationAmount", (short) lowestAmount);
                player.setAttribute("fletchingType", CreationType.HEADLESS_ARROW_CREATION);
            }
            else
            {
                player.getActionDeque().addAction(new HeadlessArrowCreation(player, (short) lowestAmount));
            }
        }
        if (usedWith.getId() == 53 || itemUsed.getId() == 53)
        {
            ArrowTips arrowtip = null;
            if (itemUsed.getId() == 53)
            {
                arrowtip = ArrowTips.forId(usedWith.getId());
            }
            else
            {
                arrowtip = ArrowTips.forId(itemUsed.getId());
            }
            if (arrowtip != null)
            {
                int lowestAmount = player.getInventory().getItemContainer().getCount(53) < player.getInventory().getItemContainer().getCount(arrowtip.getId()) ? player.getInventory().getItemContainer().getCount(53) : player.getInventory().getItemContainer().getCount(arrowtip.getId());
                if (player.getInventory().getItemContainer().getCount(53) > 4 && player.getInventory().getItemContainer().getCount(arrowtip.getId()) > 4)
                {
                    player.getActionSender().sendString("\\n \\n \\n \\n" + ItemManager.getInstance().getItemName(arrowtip.getReward()), 2799);
                    player.getActionSender().moveComponent(0, -15, 1746);
                    player.getActionSender().sendItemOnInterface(1746, 175, arrowtip.getReward());
                    player.getActionSender().sendChatInterface(4429);
                    player.setAttribute("arrowTip", arrowtip);
                    player.setAttribute("creationAmount", (short) lowestAmount);
                    player.setAttribute("fletchingType", CreationType.ARROW_CREATION);
                }
                else
                {
                    player.getActionDeque().addAction(new ArrowCreation(player, (short) lowestAmount, arrowtip));
                }
            }
        }
        else if (usedWith.getId() == 946 || itemUsed.getId() == 946)
        {
            if (itemUsed.getId() == 946)
            {
                player.setAttribute("fletchingLog", usedWith.getId());
            }
            else
            {
                player.setAttribute("fletchingLog", itemUsed.getId());
            }
            FletchableLogs log;
            log = FletchableLogs.forId((Integer) player.getAttribute("fletchingLog"));
            if (log != null)
            {
                if (log.getLogId() == 1511)
                {
                    player.getActionSender().sendItemOnInterface(8902, 170, log.getRewards()[0]);
                    player.getActionSender().sendItemOnInterface(8903, 170, log.getRewards()[1]);
                    player.getActionSender().sendItemOnInterface(8904, 170, log.getRewards()[2]);
                    player.getActionSender().sendItemOnInterface(8905, 170, log.getRewards()[3]);
                    player.getActionSender().moveComponent(0, -10, 8902);
                    player.getActionSender().moveComponent(0, -10, 8903);
                    player.getActionSender().moveComponent(0, -10, 8904);
                    player.getActionSender().moveComponent(0, -10, 8905);
                    player.getActionSender().sendString("\\n \\n \\n \\n".concat("15 Arrow Shafts"), 8909);
                    player.getActionSender().sendString("\\n \\n \\n \\n".concat("Short Bow"), 8913);
                    player.getActionSender().sendString("\\n \\n \\n \\n".concat("Long Bow"), 8917);
                    player.getActionSender().sendString("\\n \\n \\n \\n".concat("Crossbow Stock"), 8921);
                    player.getActionSender().sendChatInterface(8899);
                }
                else
                {
                    String prefix = ItemManager.getInstance().getItemName(log.getRewards()[0]).split(" ")[0];
                    player.getActionSender().sendItemOnInterface(8883, 170, log.getRewards()[0]);
                    player.getActionSender().sendItemOnInterface(8884, 170, log.getRewards()[2]);
                    player.getActionSender().sendItemOnInterface(8885, 170, log.getRewards()[1]);
                    player.getActionSender().moveComponent(0, -10, 8883);
                    player.getActionSender().moveComponent(0, -10, 8884);
                    player.getActionSender().moveComponent(0, -10, 8885);
                    player.getActionSender().sendString("\\n \\n \\n \\n".concat(prefix + " Short Bow"), 8889);
                    player.getActionSender().sendString("\\n \\n \\n \\n".concat(prefix + " Long Bow"), 8893);
                    player.getActionSender().sendString("\\n \\n \\n \\n".concat("Crossbow Stock"), 8897);
                    player.getActionSender().sendChatInterface(8880);
                }
                player.setAttribute("fletchingType", CreationType.UNSTRUNG_BOW_CREATION);
                player.setAttribute("fletchingLog", log);
            }
        }
        else if (usedWith.getId() == 1777 || itemUsed.getId() == 1777)
        {
            Item unstrungBow = null;
            if (usedWith.getId() == 1777)
            {
                unstrungBow = itemUsed;
            }
            else
            {
                unstrungBow = usedWith;
            }
            if (unstrungBow != null)
            {
                if (UnstrungBows.forId(unstrungBow.getId()) != null)
                {
                    UnstrungBows bow = UnstrungBows.forId(unstrungBow.getId());
                    if (player.getInventory().getItemContainer().getCount(1777) > 1 && player.getInventory().getItemContainer().getCount(bow.getItem()) > 1)
                    {
                        player.getActionSender().sendString("\\n \\n \\n \\n" + ItemManager.getInstance().getItemName(bow.getProduct()), 2799);
                        player.getActionSender().moveComponent(0, -18, 1746);
                        player.getActionSender().sendItemOnInterface(1746, 175, bow.getProduct());
                        player.getActionSender().sendChatInterface(4429);
                        player.setAttribute("fletchingType", CreationType.BOW_CREATION);
                        player.setAttribute("fletchingItem", bow);
                    }
                    else
                    {
                        player.getActionDeque().addAction(new BowCreation(player, (short) 1, bow));
                    }
                }
            }
        }
        else if (usedWith.getId() == 227 || itemUsed.getId() == 227)
        {
            Item primeIngredient = null;
            if (usedWith.getId() == 227)
            {
                primeIngredient = itemUsed;
            }
            else
            {
                primeIngredient = usedWith;
            }
            PrimaryIngredient ingredient = PrimaryIngredient.forId(primeIngredient.getId());
            if (ingredient != null)
            {
                if (player.getInventory().getItemContainer().getCount(227) > 1 && player.getInventory().getItemContainer().getCount(primeIngredient.getId()) > 1)
                {
                    player.getActionSender().sendString("\\n \\n \\n \\n" + ItemManager.getInstance().getItemName(ingredient.getReward()), 2799);
                    player.getActionSender().sendItemOnInterface(1746, 150, ingredient.getReward());
                    player.getActionSender().sendChatInterface(4429);
                    player.setAttribute("herbloreType", IngredientType.PRIMARY_INGREDIENT);
                    player.setAttribute("herbloreIngredient", ingredient);
                }
                else
                {
                    player.getActionDeque().addAction(new Herbalism(player, (short) 1, IngredientType.PRIMARY_INGREDIENT, ingredient, null));
                }
            }
        }

        SecondaryIngredient ingredient = null;
        for (SecondaryIngredient secondaryIngredient : SecondaryIngredient.values())
        {
            if (secondaryIngredient.getId() == itemUsed.getId() && secondaryIngredient.getRequiredItem().getId() == usedWith.getId() || secondaryIngredient.getId() == usedWith.getId() && secondaryIngredient.getRequiredItem().getId() == itemUsed.getId())
            {
                ingredient = secondaryIngredient;
            }
        }
        if (ingredient != null)
        {
            if (player.getInventory().getItemContainer().getCount(ingredient.getId()) > 1 && player.getInventory().getItemContainer().getCount(ingredient.getRequiredItem().getId()) > 1)
            {
                player.getActionSender().sendString("\\n \\n \\n \\n" + ItemManager.getInstance().getItemName(ingredient.getReward()), 2799);
                player.getActionSender().sendItemOnInterface(1746, 150, ingredient.getReward());
                player.getActionSender().sendChatInterface(4429);
                player.setAttribute("herbloreType", IngredientType.SECONDARY_INGREDIENT);
                player.setAttribute("herbloreIngredient", ingredient);
            }
            else
            {
                player.getActionDeque().addAction(new Herbalism(player, (short) 1, IngredientType.SECONDARY_INGREDIENT, null, ingredient));
            }
            return;
        }
    }

    /**
     * NEEDS ALOT OF WORK - stop tick if no space - check if has space Write
     * shorter & more efficient code
     * 
     * @param player
     * @param packet
     */
    private void handlePickupItem(final Player player, Packet packet)
    {
        player.setClickY(packet.getIn().readShort(StreamBuffer.ByteOrder.LITTLE));
        player.setClickId(packet.getIn().readShort());
        player.setClickX(packet.getIn().readShort(StreamBuffer.ByteOrder.LITTLE));

        if ((Boolean) player.getAttribute("disableItemPickup") != null)
            return;

        if (Misc.getDistance(new Position(player.getClickX(), player.getClickY()), player.getPosition()) >= 20)
        {
            player.getMovementHandler().reset();
            return;
        }

        // -- If we're top of the item..
        if (player.getPosition().getX() == player.getClickX() && player.getPosition().getY() == player.getClickY())
        {
            ItemManager.getInstance().pickupItem(player, player.getClickId(), new Position(player.getClickX(), player.getClickY(), player.getPosition().getZ()));
        }
        else
        {
            player.setWalkToAction(new Task(1, true)
            {
                @Override
                protected void execute()
                {

                    // -- Item has vanished, reset movement & stop.
                    if (!ItemManager.getInstance().itemExists(player.getClickId(), new Position(player.getClickX(), player.getClickY(), player.getPosition().getZ())))
                    {
                        player.getMovementHandler().reset();
                        this.stop();
                        return;
                    }

                    // -- Wait till we're next to the item...
                    if (!player.getMovementHandler().walkToAction(new Position(player.getClickX(), player.getClickY(), player.getPosition().getZ()), 1))
                    {
                        return;
                    }

                    World.submit(new Task(1)
                    {

                        @Override
                        protected void execute()
                        {

                            // -- Finally pick up the item...
                            if (Misc.getDistance(player.getPosition(), new Position(player.getClickX(), player.getClickY(), player.getPosition().getZ())) < 1)
                            {
                                ItemManager.getInstance().pickupItem(player, player.getClickId(), new Position(player.getClickX(), player.getClickY(), player.getPosition().getZ()));
                            }

                            this.stop();
                        }
                    });

                    if (player.getAttribute("pickedUpItem") != null)
                    {
                        player.removeAttribute("pickedUpItem");
                        this.stop();
                        return;
                    }

                }
            });
        }
    }

    /**
     * Should be working.
     * 
     * @param player
     * @param packet
     */
    private void handleOptions(Player player, Packet packet)
    {
        int interfaceId = packet.getIn().readShort(StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE);
        packet.getIn().readByte(StreamBuffer.ValueType.C);
        int fromSlot = packet.getIn().readShort(StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE);
        int toSlot = packet.getIn().readShort(StreamBuffer.ByteOrder.LITTLE);
        switch (interfaceId)
        {
        case 5382:
            BankManager.handleBankOptions(player, fromSlot, toSlot);
            break;
        case 3214:
            player.getInventory().swap(fromSlot, toSlot);
            break;
        default:
            logger.severe("SWAP ITEMS: " + interfaceId);
            break;
        }
    }

    /**
     * withdraw/deposit 1/x TODO: implement a method to determine the current
     * container.
     * 
     * TODO: Item item = null You need to specify from wich container you get
     * the item e.g getTrade().get(slot) e.g getInventory().get(slot)
     * 
     * @param player
     * @param packet
     */
    private void handleInterfaceAction1(Player player, Packet packet)
    {
        int interfaceID = packet.getIn().readShort(StreamBuffer.ValueType.A);
        int slot = packet.getIn().readShort(StreamBuffer.ValueType.A);
        packet.getIn().readShort(StreamBuffer.ValueType.A);

        Item item;

        if (player.isDebugging())
        {
            player.getActionSender().sendMessage("Packet145 InterfaceID >> " + interfaceID);
        }

        Shop shop = player.getAttribute("shop");

        switch (interfaceID)
        {
        case 6574:
            DuelArena.getInstance().offerItem(player, slot, 1);
            break;
        case 6669:
            DuelArena.getInstance().removeItem(player, slot, 1);
            break;
        case 1688:
            player.getEquipment().unequip(slot);
            break;
        case 3322:
            item = player.getInventory().get(slot);
            if (item != null)
                player.getTrading().offerItem(player, slot, item.getId(), 1);
            break;
        case 3415:
            item = player.getTrade().get(slot);
            if (item != null)
                player.getTrading().removeTradeItem(player, slot, item.getId(), 1);
            break;
        case 3823:
            // ShopManager.getSellValue(player, item.getId());
            if (shop == null)
            {
                player.getActionSender().sendMessage("Shop is null?");
                return;
            }
            Item s_item = player.getInventory().get(slot);
            shop.value(player, s_item, true, true);
            break;
        case 3900:
            // ShopManager.getBuyValue(player, item.getId());
            // item = player.getInventory().get(slot);
            if (shop == null)
            {
                player.getActionSender().sendMessage("Shop is null?");
                return;
            }
            Item b_item = shop.getCurrentStock().get(slot);
            shop.value(player, b_item, true, false);
            break;
        case 5064:
            item = player.getInventory().get(slot);
            if (item != null)
                BankManager.bankItem(player, slot, item.getId(), 1);
            break;
        case 5382:
            item = player.getBank().get(slot);
            if (item != null)
                BankManager.withdrawItem(player, slot, item.getId(), 1);
            break;
        default:
            logger.severe("Unhandled InterfaceAction #1 interfaceID : " + interfaceID);
        }
    }

    /**
     * withdraw/deposit 5 TODO: implement a method to determine the current
     * container.
     * 
     * @param player
     * @param packet
     */
    private void handleInterfaceAction2(Player player, Packet packet)
    {
        int interfaceID = packet.getIn().readShort(true, StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE);
        packet.getIn().readShort(true, StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE);
        int slot = packet.getIn().readShort(true, StreamBuffer.ByteOrder.LITTLE);

        Item item;
        Shop shop = player.getAttribute("shop");

        switch (interfaceID)
        {
        case 6574:
            item = player.getInventory().get(slot);
            if (item != null)
                DuelArena.getInstance().offerItem(player, slot, 5);
            break;
        case 6669:
            DuelArena.getInstance().removeItem(player, slot, 5);
            break;
        case 3322:
            item = player.getInventory().get(slot);
            if (item != null)
                player.getTrading().offerItem(player, slot, item.getId(), 5);
            break;
        case 3415:
            item = player.getTrade().get(slot);
            if (item != null)
                player.getTrading().removeTradeItem(player, slot, item.getId(), 5);
            break;
        case 3823:
            // ShopManager.sellItem(player, slot, item.getId(), 1);
            if (shop == null)
            {
                player.getActionSender().sendMessage("Error, nulled shop?");
                return;
            }
            shop.sell(player, slot, 1);
            break;
        case 3900:
            // ShopManager.buyItem(player, slot, item.getId(), 1);
            if (shop == null)
            {
                player.getActionSender().sendMessage("Error, nulled shop?");
                return;
            }
            shop.buy(player, slot, 1);
            break;
        case 5064:
            item = player.getInventory().get(slot);
            if (item != null)
                BankManager.bankItem(player, slot, item.getId(), 5);
            break;
        case 5382:
            item = player.getBank().get(slot);
            if (item != null)
                BankManager.withdrawItem(player, slot, item.getId(), 5);
            break;
        default:
            logger.severe("Unhandled packet InterfaceAction #2 interfaceID : " + interfaceID);
        }
    }

    /**
     * withdraw/deposit 10 also seems to be operate for dragon fire shield once
     * equipped. TODO: implement a method to determine the current container.
     * 
     * @param player
     * @param packet
     */
    private void handleInterfaceAction3(Player player, Packet packet)
    {
        int interfaceID = packet.getIn().readShort(StreamBuffer.ByteOrder.LITTLE);
        packet.getIn().readShort(StreamBuffer.ValueType.A);
        int slot = packet.getIn().readShort(StreamBuffer.ValueType.A);

        Item item;
        Shop shop = player.getAttribute("shop");

        switch (interfaceID)
        {
        case 6574:
            DuelArena.getInstance().offerItem(player, slot, 10);
            break;
        case 6669:
            DuelArena.getInstance().removeItem(player, slot, 10);
            break;
        case 1688:
            item = player.getEquipment().get(slot);
            if (item == null)
                return;
            if (player.isDebugging())
            {
                player.getActionSender().sendMessage("<col=ff0000>[DEBUG]<col=255> Item operate >> " + item);
            }

            String scriptName = "operateItem_" + item.getId();
            if (player.isDebugging())
            {
                System.out.println("attempted to call script " + scriptName + ".");

            }
            if (!ScriptManager.getSingleton().invokeWithFailTest(scriptName, player, item))
            {
                player.getActionSender().sendMessage("This item cannot be operated.");
            }
            break;
        case 3322:
            item = player.getInventory().get(slot);
            if (item != null)
                player.getTrading().offerItem(player, slot, item.getId(), 10);
            break;
        case 3415:
            item = player.getTrade().get(slot);
            if (item != null)
                player.getTrading().removeTradeItem(player, slot, item.getId(), 10);
            break;
        case 3823:
            // ShopManager.sellItem(player, slot, item.getId(), 5);
            if (shop == null)
            {
                player.getActionSender().sendMessage("Error, nulled shop?");
                return;
            }
            shop.sell(player, slot, 5);
            break;
        case 3900:
            // ShopManager.buyItem(player, slot, item.getId(), 5);
            if (shop == null)
            {
                player.getActionSender().sendMessage("Error, nulled shop?");
                return;
            }
            shop.buy(player, slot, 5);
            break;
        case 5064:
            item = player.getInventory().get(slot);
            if (item != null)
                BankManager.bankItem(player, slot, item.getId(), 10);
            break;
        case 5382:
            item = player.getBank().get(slot);
            if (item != null)
                BankManager.withdrawItem(player, slot, item.getId(), 10);
            break;
        default:
            logger.severe("Unhandled packet InterfaceAction #3 interfaceID :" + interfaceID);
        }
    }

    /**
     * 'Withdraw'/'Deposit All' TODO: implement a method to determine the
     * current container.
     * 
     * @param player
     * @param packet
     */
    private void handleInterfaceAction4(Player player, Packet packet)
    {
        int slot = packet.getIn().readShort(StreamBuffer.ValueType.A);
        int interfaceID = packet.getIn().readShort();
        packet.getIn().readShort(StreamBuffer.ValueType.A);

        Item item;
        Shop shop = player.getAttribute("shop");

        switch (interfaceID)
        {
        case 6574:
            item = player.getInventory().get(slot);
            if (item != null)
                DuelArena.getInstance().offerItem(player, slot, item.getDefinition().isStackable() ? item.getCount() : 28);
            break;
        case 6669:
            item = player.getDuelInventory().get(slot);
            if (item != null)
                DuelArena.getInstance().removeItem(player, slot, item.getDefinition().isStackable() ? item.getCount() : 28);
            break;
        case 3322:
            item = player.getInventory().get(slot);
            if (item != null)
                player.getTrading().offerItem(player, slot, item.getId(), item.getDefinition().isStackable() ? item.getCount() : 28);
            break;
        case 3415:
            item = player.getTrade().get(slot);
            if (item == null)
                return;
            int amount = item.getDefinition().isStackable() ? item.getCount() : 28;
            player.getTrading().removeTradeItem(player, slot, item.getId(), amount);
            break;
        case 3823:
            // ShopManager.sellItem(player, slot, itemId, 10);
            if (shop == null)
            {
                player.getActionSender().sendMessage("Error, nulled shop?");
                return;
            }
            shop.sell(player, slot, 10);
            break;
        case 3900:
            // ShopManager.buyItem(player, slot, itemId, 10);
            if (shop == null)
            {
                player.getActionSender().sendMessage("Error, nulled shop?");
                return;
            }
            shop.buy(player, slot, 10);
            break;
        case 5064:
            item = player.getInventory().get(slot);
            if (item == null)
                return;
            int amount2 = item.getDefinition().isStackable() ? item.getCount() : 28;
            BankManager.bankItem(player, slot, item.getId(), amount2);
            break;
        case 5382:
            item = player.getBank().get(slot);
            if (item == null)
                return;
            int amount1 = 0;
            if (player.getAttribute("BANK_MODE_NOTE") != null)
            {
                amount1 = item.getCount();
            }
            else
            {
                amount1 = item.getDefinition().isStackable() ? item.getCount() : 28;
            }
            BankManager.withdrawItem(player, slot, item.getId(), amount1);
            break;
        default:
            player.getActionSender().sendMessage("Unhandled packet InterfaceAction #4 interfaceID : " + interfaceID);
        }
    }

    /**
     * @param player The player involved in the interaction.
     * @param packet The packet representation.
     * 
     *            TODO BY AKZU: Should probably have check for ids for the
     *            actions like I do with food items, if its food then do only
     *            food actions. Like in the buttons class we should not have
     *            getClass().performButtons() but a list of cases what buttons
     *            performs those actions (annoying.), other way around would be
     *            making sure only the certain buttons are used and we use
     *            default:return;
     */
    private void handleFirstClickItem(Player player, Packet packet)
    {
        packet.getIn().readShort(StreamBuffer.ValueType.A);
        int slot = packet.getIn().readShort(StreamBuffer.ValueType.A);
        packet.getIn().readShort(StreamBuffer.ByteOrder.LITTLE);
        Item item = player.getInventory().get(slot);
        if (item == null)
            return;

        switch (item.getId())
        {

        default:
            String scriptName = "itemInteractOptionOne_" + (item.getDefinition() != null ? item.getDefinition().getName().toLowerCase().replaceAll(" ", "_") : item.getId());
            if (!ScriptManager.getSingleton().invokeWithFailTest(scriptName, player, item))
            {
                logger.severe("Could not find item interaction script :" + scriptName);
                if (player.isDebugging())
                {
                    player.getActionSender().sendMessage("Unhandled first click item : <col=ff0000>" + item + "</col>.");
                }
                else
                {
                    logger.severe("Unhandled first click item : " + item);
                }
            }
        }

        if (FoodLoader.getFood(item.getId()) != null)
        {
            Food.eatFood(player, item, slot);
            return;
        }

        if (PotionLoader.getPotion(item.getId()) != null)
        {
            Potion.drinkPotion(player, item, slot);
            return;
        }

        TeleportTabs.getInstance().activateTeleportTab(player, item.getId(), slot);

        HerbType herb = HerbType.forId(item.getId());

        if (herb != null)
        {
            HerbIdentifier identifier = new HerbIdentifier(player, herb);
            identifier.identifyHerb();

        }
    }

    /**
     * Workling Clienth4x fix <Ares>
     * 
     * @param player
     * @param packet
     */
    private void handleThirdClickItem(Player player, Packet packet)
    {
        packet.getIn().readShort(StreamBuffer.ValueType.A);
        int slot = packet.getIn().readShort(true, StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE);
        packet.getIn().readShort();

        Item item = player.getInventory().get(slot);

        if (item == null)
            return;

        switch (item.getId())
        {
        default:
            if (player.isDebugging())
            {
                player.getActionSender().sendMessage("Unhandled third click item :  <col=ff0000>[" + item.getId() + ": " + item.getDefinition() != null ? item.getDefinition().getName() : "nullitem" + " - amt :" + item.getCount() + "]" + "</col>.");
            }
            else
            {
                logger.severe("Unhandled third click item : " + item);
            }
        }

    }

    /**
     * Working Clienth4x fix <Ares>
     * 
     * @param player
     * @param packet
     */
    private void handleEquipItem(Player player, Packet packet)
    {
        packet.getIn().readShort(); // do not use - clienth4x <Ares>
        int slot = packet.getIn().readShort(StreamBuffer.ValueType.A);
        packet.getIn().readShort(); // Interface ID.
        player.getEquipment().equip(slot);
    }
}
