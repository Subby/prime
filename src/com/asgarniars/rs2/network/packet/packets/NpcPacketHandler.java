package com.asgarniars.rs2.network.packet.packets;

import com.asgarniars.rs2.content.combat.Combat;
import com.asgarniars.rs2.content.combat.magic.Magic;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.model.players.WalkToActions;
import com.asgarniars.rs2.model.players.WalkToActions.Actions;
import com.asgarniars.rs2.network.StreamBuffer;
import com.asgarniars.rs2.network.packet.Packet;
import com.asgarniars.rs2.network.packet.PacketManager.PacketHandler;

@SuppressWarnings("unused")
public class NpcPacketHandler implements PacketHandler
{

    public static final int FIRST_CLICK = 155;
    public static final int SECOND_CLICK = 17;
    public static final int ATTACK = 72;
    public static final int ITEM_ON_NPC = 57;

    @Override
    public void handlePacket(Player player, Packet packet)
    {
        switch (packet.getOpcode())
        {
        case FIRST_CLICK:
            handleFirstClick(player, packet);
            break;
        case SECOND_CLICK:
            handleSecondClick(player, packet);
            break;
        case ATTACK:
            handleAttack(player, packet);
            break;
        case ITEM_ON_NPC:
            handleItemOnNpc(player, packet);
            break;
        }
    }

    private void handleFirstClick(Player player, Packet packet)
    {
        int npcSlot = packet.getIn().readShort(true, StreamBuffer.ByteOrder.LITTLE);
        Npc npc = World.getNpc(npcSlot);

        player.setClickId(npc.getNpcId());
        player.setClickX(npc.getPosition().getX());
        player.setClickY(npc.getPosition().getY());
        player.setNpcClickIndex(npcSlot);
        player.getUpdateFlags().faceEntity(npcSlot);
        WalkToActions.setActions(Actions.NPC_FIRST_CLICK);
        WalkToActions.doActions(player);
    }

    private void handleSecondClick(Player player, Packet packet)
    {
        int npcSlot = packet.getIn().readShort(StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE) & 0xFFFF;
        Npc npc = World.getNpc(npcSlot);
        player.setClickId(npc.getNpcId());
        player.setClickX(npc.getPosition().getX());
        player.setClickY(npc.getPosition().getY());
        player.setNpcClickIndex(npcSlot);
        player.getUpdateFlags().faceEntity(npcSlot);
        WalkToActions.setActions(Actions.NPC_SECOND_CLICK);
        WalkToActions.doActions(player);
    }

    private void handleAttack(final Player player, Packet packet)
    {
        int npcSlot = packet.getIn().readShort(StreamBuffer.ValueType.A);
        final Npc npc = World.getNpc(npcSlot);
        if(npc != null) 
        {
        	player.getUpdateFlags().sendFaceToDirection(npc.getCentreLocation());
        }

        // -- Is attackable check..
        if (!npc.getDefinition().isAttackable() || npc.getDefinition().getCombatLevel(3) == 1)
        {
            player.getActionSender().sendMessage("Npc doesn't have definitions!");
            player.getMovementHandler().reset();
            Combat.getInstance().resetCombat(player);
            player.getActionSender().removeMapFlag();
            return;
        }

        if (!Combat.getInstance().meetsAttackRequirements(player, npc))
        {
            player.getMovementHandler().reset();
            Combat.getInstance().resetCombat(player);
            player.getActionSender().removeMapFlag();
            return;
        }

        if (Combat.getInstance().withinRange(player, npc))
            player.getMovementHandler().reset();

        player.setClickId(npc.getNpcId());
        player.setClickX(npc.getPosition().getX());
        player.setClickY(npc.getPosition().getY());
        player.setTarget(npc);
        player.setInstigatingAttack(true);
        player.setFollowingEntity(npc);
    }

    // TODO:
    private void handleItemOnNpc(final Player player, Packet packet)
    {

        packet.getIn().readShort(StreamBuffer.ValueType.A); // item id
        int npc_slot = packet.getIn().readShort(StreamBuffer.ValueType.A); // npc
        // index
        int slot = packet.getIn().readShort(StreamBuffer.ByteOrder.LITTLE);
        packet.getIn().readShort(StreamBuffer.ValueType.A); // itf id

    }

}
