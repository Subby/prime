package com.asgarniars.rs2.network.packet.packets;

import java.util.logging.Logger;

import com.asgarniars.rs2.content.duel.DuelArena;
import com.asgarniars.rs2.content.skills.craft.ArmourCreation;
import com.asgarniars.rs2.content.skills.craft.Craftable;
import com.asgarniars.rs2.content.skills.craft.CraftingType;
import com.asgarniars.rs2.content.skills.craft.Gem;
import com.asgarniars.rs2.content.skills.craft.GemCutting;
import com.asgarniars.rs2.content.skills.craft.Hide;
import com.asgarniars.rs2.content.skills.craft.LeatherTanning;
import com.asgarniars.rs2.content.skills.fletch.BowCreation;
import com.asgarniars.rs2.content.skills.fletch.CreationType;
import com.asgarniars.rs2.content.skills.fletch.FletchableLogs;
import com.asgarniars.rs2.content.skills.fletch.LogFletching;
import com.asgarniars.rs2.content.skills.fletch.UnstrungBows;
import com.asgarniars.rs2.content.skills.herbalism.Herbalism;
import com.asgarniars.rs2.content.skills.herbalism.IngredientType;
import com.asgarniars.rs2.content.skills.herbalism.PrimaryIngredient;
import com.asgarniars.rs2.content.skills.herbalism.SecondaryIngredient;
import com.asgarniars.rs2.model.players.BankManager;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.network.StreamBuffer;
import com.asgarniars.rs2.network.packet.Packet;
import com.asgarniars.rs2.network.packet.PacketManager.PacketHandler;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class ChatInterfacePacketHandler implements PacketHandler
{

    private static final Logger logger = Logger.getLogger(ChatInterfacePacketHandler.class.getName());

    int enterAmountId;
    public static final int DIALOGUE = 40;
    public static final int SHOW_ENTER_X = 135;
    public static final int ENTER_X = 208;

    @Override
    public void handlePacket(Player player, Packet packet)
    {
        switch (packet.getOpcode())
        {
        case DIALOGUE:
            handleDialogue(player, packet);
            break;
        case SHOW_ENTER_X:
            showEnterX(player, packet);
            break;
        case ENTER_X:
            handleEnterX(player, packet);
            break;
        }
    }

    /**
     * 
     * @param player
     * @param packet
     */
    private void handleDialogue(Player player, Packet packet)
    {
        player.nextDialogue();
    }

    /**
     * 
     * @param player
     * @param packet
     */
    private void showEnterX(Player player, Packet packet)
    {
        player.setEnterXSlot(packet.getIn().readShort(StreamBuffer.ByteOrder.LITTLE));
        player.setEnterXInterfaceId(packet.getIn().readShort(StreamBuffer.ValueType.A));
        player.setEnterXId(packet.getIn().readShort(StreamBuffer.ByteOrder.LITTLE));

        player.getActionSender().sendEnterX();
    }

    /**
     * Handle Enter X / Handle InterfaceAction #5
     */
    private void handleEnterX(Player player, Packet packet)
    {
        int amount = packet.getIn().readInt();
        logger.info("Enter X Interface : " + player.getEnterXInterfaceId());
        if (player.getEnterXInterfaceId() == 1748)
        {
            if (player.getAttribute("craftingType") != null)
            {
                switch ((CraftingType) player.getAttribute("craftingType"))
                {
                case GEM_CRAFTING:
                    player.getActionDeque().addAction(new GemCutting(player, (short) amount, (Gem) player.getAttribute("craftingGem")));
                    player.getActionSender().removeInterfaces();
                    player.removeAttribute("craftingType");
                    player.removeAttribute("craftingGem");
                    player.getActionSender().moveComponent(0, 0, 1746);
                    break;
                default:
                    logger.severe("Unhandled Crafting Type : " + player.getAttribute("craftingType"));
                    break;

                }
            }
            else if (player.getAttribute("herbloreType") != null)
            {
                switch ((IngredientType) player.getAttribute("herbloreType"))
                {
                case PRIMARY_INGREDIENT:
                    player.getActionDeque().addAction(new Herbalism(player, (short) amount, (IngredientType) player.getAttribute("herbloreType"), (PrimaryIngredient) player.getAttribute("herbloreIngredient"), null));
                    player.getActionSender().removeInterfaces();
                    break;
                case SECONDARY_INGREDIENT:
                    player.getActionDeque().addAction(new Herbalism(player, (short) amount, IngredientType.SECONDARY_INGREDIENT, null, (SecondaryIngredient) player.getAttribute("herbloreIngredient")));
                    player.getActionSender().removeInterfaces();
                    player.removeAttribute("herbloreIngredient");
                    player.removeAttribute("herbloreType");
                    break;
                default:
                    if (player.getAttribute("herbloreIngredient") != null)
                    {
                        player.removeAttribute("herbloreIngredient");
                    }
                    if (player.getAttribute("herbloreType") != null)
                    {
                        player.removeAttribute("herbloreType");
                    }
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            else if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case ARROW_CREATION:
                    // TODO : write and implement.
                    player.getActionSender().removeInterfaces();
                    break;
                case BOW_CREATION:
                    player.getActionDeque().addAction(new BowCreation(player, (short) amount, (UnstrungBows) player.getAttribute("fletchingItem")));
                    player.getActionSender().removeInterfaces();
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingItem");
                    break;
                case CROSSBOW_CREATION:
                    // TODO : write and implement.
                    player.getActionSender().removeInterfaces();
                    break;
                default:
                    break;

                }
            }
            // TODO Working on-akzu
        }
        else if (player.getEnterXInterfaceId() == 5064)
        {
            BankManager.bankItem(player, player.getEnterXSlot(), player.getEnterXId(), amount);
        }
        else if (player.getEnterXInterfaceId() == 5382)
        {
            BankManager.withdrawItem(player, player.getEnterXSlot(), player.getEnterXId(), amount);
        }
        else if (player.getEnterXInterfaceId() == 3322)
        {
            player.getTrading().offerItem(player, player.getEnterXSlot(), player.getEnterXId(), amount);
        }
        else if (player.getEnterXInterfaceId() == 6669)
        {
            DuelArena.getInstance().removeItem(player, player.getEnterXSlot(), amount);
        }
        else if (player.getEnterXInterfaceId() == 6574)
        {
            DuelArena.getInstance().offerItem(player, player.getEnterXSlot(), amount);
        }
        else if (player.getEnterXInterfaceId() == 3415)
        {
            player.getTrading().removeTradeItem(player, player.getEnterXSlot(), player.getEnterXId(), amount);
        }
        else if (player.getEnterXInterfaceId() == 8886)
        {
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, amount, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8883);
                    player.getActionSender().moveComponent(0, 0, 8884);
                    player.getActionSender().moveComponent(0, 0, 8885);
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            else if (player.getAttribute("craftingType") != null)
            {
                switch ((CraftingType) player.getAttribute("craftingType"))
                {
                case ARMOUR_CREATION:
                    if (player.getAttribute("craftingHide") == null)
                    {
                        return;
                    }
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new ArmourCreation(player, (short) amount, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[0])));
                    player.removeAttribute("craftingHide");
                    player.removeAttribute("craftingType");
                    break;
                default:
                    if (player.isDebugging())
                    {
                        player.getActionSender().sendMessage("Unhandled crafting type : " + player.getAttribute("craftingType"));
                    }
                    logger.severe("Unhandled crafting type : " + player.getAttribute("craftingType"));
                }
            }
        }
        else if (player.getEnterXInterfaceId() == 8890)
        {
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, amount, 2, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8883);
                    player.getActionSender().moveComponent(0, 0, 8884);
                    player.getActionSender().moveComponent(0, 0, 8885);
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            else if (player.getAttribute("craftingType") != null)
            {
                switch ((CraftingType) player.getAttribute("craftingType"))
                {
                case ARMOUR_CREATION:
                    if (player.getAttribute("craftingHide") == null)
                    {
                        return;
                    }
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new ArmourCreation(player, (short) amount, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[1])));
                    player.removeAttribute("craftingHide");
                    player.removeAttribute("craftingType");
                    break;
                default:
                    if (player.isDebugging())
                    {
                        player.getActionSender().sendMessage("Unhandled crafting type : " + player.getAttribute("craftingType"));
                    }
                    logger.severe("Unhandled crafting type : " + player.getAttribute("craftingType"));
                }
            }
        }
        else if (player.getEnterXInterfaceId() == 8894)
        {
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, amount, 1, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8883);
                    player.getActionSender().moveComponent(0, 0, 8884);
                    player.getActionSender().moveComponent(0, 0, 8885);
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
            else if (player.getAttribute("craftingType") != null)
            {
                switch ((CraftingType) player.getAttribute("craftingType"))
                {
                case ARMOUR_CREATION:
                    if (player.getAttribute("craftingHide") == null)
                    {
                        return;
                    }
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new ArmourCreation(player, (short) amount, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[2])));
                    player.removeAttribute("craftingHide");
                    player.removeAttribute("craftingType");
                    break;
                default:
                    if (player.isDebugging())
                    {
                        player.getActionSender().sendMessage("Unhandled crafting type : " + player.getAttribute("craftingType"));
                    }
                    logger.severe("Unhandled crafting type : " + player.getAttribute("craftingType"));
                }
            }
        }
        else if (player.getEnterXInterfaceId() == 8906)
        {
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, amount, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8902);
                    player.getActionSender().moveComponent(0, 0, 8903);
                    player.getActionSender().moveComponent(0, 0, 8904);
                    player.getActionSender().moveComponent(0, 0, 8905);
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
        }
        else if (player.getEnterXInterfaceId() == 8910)
        {
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, amount, 1, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8902);
                    player.getActionSender().moveComponent(0, 0, 8903);
                    player.getActionSender().moveComponent(0, 0, 8904);
                    player.getActionSender().moveComponent(0, 0, 8905);
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
        }
        else if (player.getEnterXInterfaceId() == 8914)
        {
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, amount, 2, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8902);
                    player.getActionSender().moveComponent(0, 0, 8903);
                    player.getActionSender().moveComponent(0, 0, 8904);
                    player.getActionSender().moveComponent(0, 0, 8905);
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
        }
        else if (player.getEnterXInterfaceId() == 8918)
        {
            if (player.getAttribute("fletchingType") != null)
            {
                switch ((CreationType) player.getAttribute("fletchingType"))
                {
                case UNSTRUNG_BOW_CREATION:
                    player.getActionSender().removeInterfaces();
                    player.getActionDeque().addAction(new LogFletching(player, amount, 3, (FletchableLogs) player.getAttribute("fletchingLog")));
                    player.removeAttribute("fletchingType");
                    player.removeAttribute("fletchingLog");
                    player.getActionSender().moveComponent(0, 0, 8902);
                    player.getActionSender().moveComponent(0, 0, 8903);
                    player.getActionSender().moveComponent(0, 0, 8904);
                    player.getActionSender().moveComponent(0, 0, 8905);
                    break;
                default:
                    System.out.println("Unhandled Creation Type : " + player.getAttribute("fletchingType"));
                    player.getActionSender().removeInterfaces();
                    break;

                }
            }
        }
        else if (player.getEnterXInterfaceId() == 14801)
        {
            player.getActionDeque().addAction(new LeatherTanning(player, (short) amount, Hide.forReward((short) 1741)));
        }
        else if (player.getEnterXInterfaceId() == 14802)
        {
            player.getActionDeque().addAction(new LeatherTanning(player, (short) amount, Hide.forReward((short) 1743)));
        }
        else if (player.getEnterXInterfaceId() == 14803)
        {
            player.getActionDeque().addAction(new LeatherTanning(player, (short) amount, Hide.forId((short) 6287)));
        }
        else if (player.getEnterXInterfaceId() == 14804)
        {
            player.getActionDeque().addAction(new LeatherTanning(player, (short) amount, Hide.forId((short) 7801)));
        }
        else if (player.getEnterXInterfaceId() == 14805)
        {
            player.getActionDeque().addAction(new LeatherTanning(player, (short) amount, Hide.forId((short) 1753)));
        }
        else if (player.getEnterXInterfaceId() == 14806)
        {
            player.getActionDeque().addAction(new LeatherTanning(player, (short) amount, Hide.forId((short) 1751)));
        }
        else if (player.getEnterXInterfaceId() == 14807)
        {
            player.getActionDeque().addAction(new LeatherTanning(player, (short) amount, Hide.forId((short) 1749)));
        }
        else if (player.getEnterXInterfaceId() == 14808)
        {
            player.getActionDeque().addAction(new LeatherTanning(player, (short) amount, Hide.forId((short) 1747)));
        }

        else if (player.getEnterXInterfaceId() == 1337)
        {
            if (amount < 1)
                amount = 1;
            if (player.getEnterXId() != 5)
                player.getSkill().getLevel()[player.getEnterXId()] = (byte) amount;
            else
                player.getSkill().setPrayerPoints(amount);
            player.getSkill().getExp()[player.getEnterXId()] = player.getSkill().getXPForLevel(amount);
            player.getSkill().refresh(player.getEnterXId());
            player.setAppearanceUpdateRequired(true);
        }
    }
}
