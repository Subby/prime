package com.asgarniars.rs2.network.packet.packets;

import com.asgarniars.rs2.content.combat.Combat;
import com.asgarniars.rs2.content.duel.DuelArena;
import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.network.StreamBuffer;
import com.asgarniars.rs2.network.StreamBuffer.ByteOrder;
import com.asgarniars.rs2.network.packet.Packet;
import com.asgarniars.rs2.network.packet.PacketManager.PacketHandler;
import com.asgarniars.rs2.task.Task;
import com.asgarniars.rs2.util.Language;

public class PlayerOptionPacketHandler implements PacketHandler
{

    public static final int SLOT_1 = 128;
    public static final int SLOT_2 = 153;
    public static final int SLOT_3 = 73;
    public static final int SLOT_4 = 139;
    public static final int SLOT_5 = 39;
    public static final int USE_ITEM_ON_PLAYER = 14;

    @Override
    public void handlePacket(Player player, Packet packet)
    {

        /*
         * TODO Probably have to make like player.getMenuOptions() to determine
         * wich options are wich if we have to change one e.g in castle wars
         * "Take Flag" or.. Whack -option on holiday item.
         */
        switch (packet.getOpcode())
        {
        case USE_ITEM_ON_PLAYER:
            useItemOnPlayer(player, packet);
            break;
        case SLOT_1:
            handleSlot1(player, packet);
            // usually attacking
            break;
        case SLOT_2:
            handleSlot2(player, packet);
            // challenge requests
            break;
        case SLOT_3:
            handleSlot3(player, packet);
            // following
            break;
        case SLOT_4:
            handleSlot4(player, packet);
            // trade
            break;
        case SLOT_5:
            handleSlot5(player, packet);
            // whack (Rubber chicken), etc
            break;
        }
    }

    private void useItemOnPlayer(final Player player, Packet packet)
    {

        final int otherPlayerId = packet.getIn().readShort();

        int itemSlot = packet.getIn().readShort(true, StreamBuffer.ByteOrder.LITTLE);

        final Player otherPlayer = World.getPlayer(otherPlayerId);

        if (otherPlayer == null)
            return;

        player.getUpdateFlags().sendFaceToDirection(otherPlayer.getPosition());
        player.getActionSender().removeMapFlag();

        if (player.isDebugging())
            player.getActionSender().sendMessage("used item from slot " + itemSlot + " on player " + otherPlayer.getUsername());

    }

    private void handleSlot1(final Player player, Packet packet)
    {
        final int otherPlayerId = packet.getIn().readShort();

        final Player otherPlayer = World.getPlayer(otherPlayerId);

        if (otherPlayer == null)
            return;

        // TODO Attack player
        player.getUpdateFlags().sendFaceToDirection(otherPlayer.getPosition());

        if (!Combat.getInstance().meetsAttackRequirements(player, otherPlayer))
        {
            player.getMovementHandler().reset();
            Combat.getInstance().resetCombat(player);
            player.getActionSender().removeMapFlag();
            return;
        }

        if (Combat.getInstance().withinRange(player, otherPlayer))
            player.getMovementHandler().reset();

        player.getActionSender().removeMapFlag();
        player.setFollowingEntity(otherPlayer);
        player.setTarget(otherPlayer);
        player.setInstigatingAttack(true);
    }

    private void handleSlot2(final Player player, Packet packet)
    {
        final int otherPlayerId = packet.getIn().readShort(ByteOrder.LITTLE);

        final Player otherPlayer = World.getPlayer(otherPlayerId);

        if (otherPlayer == null)
            return;

        // Are we actually at the duel arena so we can legimately use the
        // Challenge option
        if (!(player.getAttribute("duel_stage") != null && (String) player.getAttribute("duel_stage") == "SENT_REQUEST" || (String) player.getAttribute("duel_stage") == "WAITING" && (player.getPosition().getX() >= 3328 && player.getPosition().getX() <= 3395 && player.getPosition().getY() >= 3200 && player.getPosition().getY() <= 3287)))
            return;

        // TODO Handle here: Challenging, duel arena

        player.getActionSender().removeMapFlag();
        player.setClickId(otherPlayerId);
        player.getUpdateFlags().sendFaceToDirection(otherPlayer.getPosition());

        final Position otherPos = otherPlayer.getPosition();

        player.setWalkToAction(new Task(1, true)
        {

            @Override
            protected void execute()
            {
                if (otherPos.getX() != otherPlayer.getPosition().getX() && otherPos.getY() != otherPlayer.getPosition().getY())
                {
                    this.stop();
                    return;
                }
                if (!player.getMovementHandler().walkToAction(otherPlayer.getPosition(), 1))
                {
                    return;
                }
                if (otherPlayer.isBusy())
                {
                    player.getActionSender().sendMessage(Language.IS_BUSY);
                    this.stop();
                    return;
                }
                DuelArena.getInstance().handleChallengeRequest(player, otherPlayer);
                this.stop();
            }
        });

    }

    private void handleSlot3(Player player, Packet packet)
    {
        final int otherPlayerId = packet.getIn().readShort(true, ByteOrder.LITTLE);

        final Player otherPlayer = World.getPlayer(otherPlayerId);

        if (otherPlayer == null)
            return;

        player.getActionSender().removeMapFlag();
        player.setFollowingEntity(otherPlayer);
    }

    private void handleSlot4(final Player player, Packet packet)
    {

        final int otherPlayerId = packet.getIn().readShort(true, ByteOrder.LITTLE);

        final Player otherPlayer = World.getPlayer(otherPlayerId);

        if (otherPlayer == null)
            return;

        player.getActionSender().removeMapFlag();
        player.setClickId(otherPlayerId);
        player.getUpdateFlags().sendFaceToDirection(otherPlayer.getPosition());

        final Position otherPos = otherPlayer.getPosition();

        player.setWalkToAction(new Task(1, true)
        {

            @Override
            protected void execute()
            {
                if (otherPos.getX() != otherPlayer.getPosition().getX() && otherPos.getY() != otherPlayer.getPosition().getY())
                {
                    this.stop();
                    return;
                }
                if (otherPlayer.isBusy())
                {
                    player.getActionSender().sendMessage(Language.IS_BUSY);
                    this.stop();
                    return;
                }
                player.getTrading().handleTradeRequest(player, otherPlayer);
                this.stop();
            }
        });
    }

    private void handleSlot5(final Player player, Packet packet)
    {
        final int otherPlayerId = packet.getIn().readShort(true, ByteOrder.LITTLE);

        Player otherPlayer = World.getPlayer(otherPlayerId);

        if (otherPlayer == null)
            return;
    }
}
