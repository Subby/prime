package com.asgarniars.rs2.network.packet.packets;

import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.network.command.CommandManager;
import com.asgarniars.rs2.network.packet.Packet;
import com.asgarniars.rs2.network.packet.PacketManager.PacketHandler;

/**
 * Handles the incoming command packet.
 * 
 * @author Joshua Barry
 * 
 */
public class CommandPacketHandler implements PacketHandler
{

    public static final int COMMAND = 103;

    @Override
    public void handlePacket(final Player player, Packet packet)
    {

        int opcode = packet.getOpcode();

        switch (opcode)
        {
        case COMMAND:
            String playerCommand = packet.getIn().readString();
            CommandManager.invoke(player, playerCommand);
            break;
        }
    }
}
