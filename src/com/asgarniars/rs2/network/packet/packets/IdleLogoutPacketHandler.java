package com.asgarniars.rs2.network.packet.packets;

import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.network.packet.Packet;
import com.asgarniars.rs2.network.packet.PacketManager.PacketHandler;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class IdleLogoutPacketHandler implements PacketHandler
{

    public static final int IDLELOGOUT = 202;

    @Override
    public void handlePacket(Player player, Packet packet)
    {
    	if(player.isInCombat()) {
    		return;
    	}
    	player.getActionSender().sendLogout();
    	player.disconnect();
    }
}