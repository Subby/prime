package com.asgarniars.rs2.io;

import com.asgarniars.rs2.content.combat.magic.SpellLoader;
import com.asgarniars.rs2.content.combat.ranged.RangedDataLoader;
import com.asgarniars.rs2.content.combat.util.Bonuses.BonusDefinition;
import com.asgarniars.rs2.content.combat.util.LevelRequirements.LevelRequirementLoader;
import com.asgarniars.rs2.content.combat.util.Weapons.WeaponLoader;
import com.asgarniars.rs2.content.consumables.FoodLoader;
import com.asgarniars.rs2.content.consumables.PotionLoader;
import com.asgarniars.rs2.content.shop.ShopManager;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.model.npcs.NpcLoader;
import com.asgarniars.rs2.model.npcs.NpcLoot;
import com.asgarniars.rs2.model.players.GlobalObjectHandler;
import com.asgarniars.rs2.model.players.GlobalItemSpawns.GlobalItemLoader;
import com.asgarniars.rs2.music.MusicData;
import com.thoughtworks.xstream.XStream;

/**
 * Class handling all XStream
 * 
 * @author BFMV
 * 
 */
public class XStreamController
{

    private static XStreamController instance = new XStreamController();
    private static XStream xStream = new XStream();

    public static XStreamController getInstance()
    {
        return instance;
    }

    public static XStream getXStream()
    {
        return xStream;
    }

    static
    {
        xStream.alias("npcloot", NpcLoot.class);
        xStream.alias("spawn", com.asgarniars.rs2.model.players.GlobalItemSpawns.class);
        xStream.alias("npc", com.asgarniars.rs2.model.npcs.Npc.class);
        xStream.alias("item", com.asgarniars.rs2.model.items.Item.class);
        xStream.alias("itemDef", com.asgarniars.rs2.model.items.ItemManager.ItemDefinition.class);
        xStream.alias("npcdef", com.asgarniars.rs2.model.npcs.NpcDefinition.class);
        xStream.alias("object", com.asgarniars.rs2.model.players.GlobalObject.class);
        xStream.alias("spelldef", com.asgarniars.rs2.content.combat.magic.SpellDefinition.class);
        xStream.alias("bowdef", com.asgarniars.rs2.content.combat.ranged.RangeProjectiles.class);
        xStream.alias("fooddef", com.asgarniars.rs2.content.consumables.FoodLoader.FoodDefinition.class);
        xStream.alias("potiondef", com.asgarniars.rs2.content.consumables.PotionLoader.PotionDefinition.class);
        xStream.alias("bonus", com.asgarniars.rs2.content.combat.util.Bonuses.class);
        xStream.alias("weapon", com.asgarniars.rs2.content.combat.util.Weapons.class);
        xStream.alias("musicArea", com.asgarniars.rs2.music.MusicArea.class);
        xStream.alias("req", com.asgarniars.rs2.content.combat.util.LevelRequirements.class);
        xStream.alias("shop", com.asgarniars.rs2.content.shop.Shop.class);
    }

    public static void loadAllFiles() throws Exception
    {
        GlobalItemLoader.loadItemSpawns();
        ItemManager.getInstance().loadItemDefinitions();
        NpcLoader.loadDefinitions();
        NpcLoader.loadSpawns();
        GlobalObjectHandler.loadObjects();
        SpellLoader.loadSpellDefinitions();
        RangedDataLoader.loadRangedDefinitions();
        FoodLoader.loadFoodDefinitions();
        PotionLoader.loadPotionDefinitions();
        BonusDefinition.loadBonusDefinitions();
        WeaponLoader.loadWeapons();
        MusicData.loadMusic();
        LevelRequirementLoader.loadRequirements();
        ShopManager.load();
    }
}
