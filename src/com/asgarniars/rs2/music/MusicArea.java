package com.asgarniars.rs2.music;

import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.area.Area;

public class MusicArea extends Area
{
    private int id;

    /**
     * Gets the music id.
     * 
     * @return The id.
     */
    public int getId()
    {
        return id;
    }

    /**
     * Constructs a new MusicArea.
     * 
     * @param id The music id.
     * @param minimumPosition The minimum area position.
     * @param maximumPosition The maximum area position.
     */
    public MusicArea(int id, Position minimumPosition, Position maximumPosition)
    {
        super(minimumPosition, maximumPosition);
        this.id = id;
    }
}
