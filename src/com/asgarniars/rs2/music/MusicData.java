package com.asgarniars.rs2.music;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.io.XStreamController;
import com.asgarniars.rs2.model.area.Area;

/**
 * Loads and contains the music data.
 * 
 * @author PrimaDude.
 */
public class MusicData
{

    private final static Logger logger = Logger.getLogger(MusicData.class.getName());

    /**
     * The maximum amount of songs in the 317 revision.
     */
    public static final int MAX_SONGS = 647;

    /**
     * The areas that have a music song to play.
     */
    private static Map<Integer, Area> areas = new HashMap<Integer, Area>();

    /**
     * Gets the areas.
     * 
     * @return The areas.
     */
    public static Map<Integer, Area> getAreas()
    {
        return areas;
    }

    /**
     * Loads the music data.
     * 
     * @throws FileNotFoundException If the file cannot be found.
     */
    @SuppressWarnings("unchecked")
    public static void loadMusic() throws FileNotFoundException
    {
        List<MusicArea> list = (List<MusicArea>) XStreamController.getXStream().fromXML(new FileInputStream(Constants.CONFIG_DIRECTORY + "content/music.xml"));
        for (MusicArea area : list)
        {
            areas.put(area.getId(), new Area(area.getMinimumPosition(), area.getMaximumPosition()));
        }

        logger.info("loaded a total of " + areas.size() + " music areas.");
    }

}
