package com.asgarniars.rs2.music;

import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.area.Area;
import com.asgarniars.rs2.model.players.Player;

/**
 * Music playing.
 * 
 * @author PrimaDude.
 */
public class Music
{

    /**
     * The time since a 'short song' was played. A short song is, for example,
     * skill level-up music or duelling winning/losing music.
     * 
     * The use of this, is to revert back to the normal area-music after the
     * 'short music' has finished playing.
     */
    private long timeSinceLastShort;
    private boolean musicAllowed;
    private boolean[] inArea = new boolean[MusicData.MAX_SONGS];

    /**
     * If music is allowed.
     * 
     * @return True or false.
     */
    public boolean isMusicAllowed()
    {
        return musicAllowed;
    }

    /**
     * If the player is in a new area to play music.
     * 
     * @param index The index in the <code>inArea</code> array.
     * @return True or false.
     */
    public boolean isInArea(int index)
    {
        return inArea[index];
    }

    /**
     * Sets the last 'short music' time.
     * 
     * @param timeSinceLastShort The time since the last 'short music'.
     */
    public void setTimeSinceLastShort(long timeSinceLastShort)
    {
        this.timeSinceLastShort = timeSinceLastShort;
    }

    /**
     * Sets the status of music being allowed.
     * 
     * @param musicAllowed True or false.
     */
    public void setMusicAllowed(boolean musicAllowed)
    {
        this.musicAllowed = musicAllowed;
    }

    /**
     * Sets the area checker to true or false.
     * 
     * @param inArea True or false.
     * @param index The index in the <code>inArea</code> array.
     */
    public void setInArea(boolean inArea, int index)
    {
        this.inArea[index] = inArea;
    }

    /**
     * Checks if the player is in a new music playing area.
     * 
     * @param player The player that is being checked.
     * @return True or false.
     */
    public boolean checkIfInNewArea(Player player)
    {
        Area area = null;
        Position position = player.getPosition();
        for (int songId = 0; songId < MusicData.MAX_SONGS; songId++)
        {
            if ((area = MusicData.getAreas().get(songId)) == null)
            {
                continue;
            }
            else if ((position.getX() >= area.getMinimumPosition().getX()) && (position.getX() < area.getMaximumPosition().getX()) && (position.getY() >= area.getMinimumPosition().getY()) && (position.getY() < area.getMaximumPosition().getY()))
            {
                if (timeSinceLastShort > 0 && System.currentTimeMillis() - timeSinceLastShort > 7000)
                {
                    player.getActionSender().sendSong(songId);
                    setTimeSinceLastShort(0);
                }
                if (!isInArea(songId))
                {
                    if (System.currentTimeMillis() - timeSinceLastShort < 7000)
                    {
                        continue;
                    }
                    inArea[songId] = true;
                    setInArea(true, songId);
                    player.getActionSender().sendSong(songId);
                    for (int i = 0; i < inArea.length; i++)
                    {
                        if (songId != i)
                        {
                            inArea[i] = false;
                        }
                    }
                    return true;
                }
            }
        }
        return false;
    }
}