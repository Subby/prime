package com.asgarniars.rs2.action.impl;

import java.util.logging.Logger;

import com.asgarniars.rs2.action.Action;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;
import com.asgarniars.rs2.util.ScriptManager;

/**
 * Used for items with an interactive function such as a Toy Horsey.
 * 
 * @author Joshua Barry
 * 
 */
public class ItemInteraction extends Action
{

    private static final Logger logger = Logger.getLogger(ItemInteraction.class.getName());

    private Item item;

    public ItemInteraction(Entity entity, Item item, int ticks)
    {
        super(entity, ticks);
        entity.setAttribute("interactiveItem", item);
        this.item = item;
    }

    @Override
    public CancelPolicy getCancelPolicy()
    {
        return CancelPolicy.ALWAYS;
    }

    @Override
    public StackPolicy getStackPolicy()
    {

        return StackPolicy.NEVER;
    }

    @Override
    public AnimationPolicy getAnimationPolicy()
    {
        return AnimationPolicy.RESET_ALL;
    }

    @Override
    public void execute()
    {
        Player player = (Player) getEntity();

        if (getEntity().getAttribute("itemInteraction") != null)
        {
            return;
        }
        getEntity().setAttribute("itemInteraction", (byte) -1);
        String itemName = item.getDefinition().getName();
        String scriptName = itemName.toLowerCase().replaceAll(" ", "_");
        if (!ScriptManager.getSingleton().invokeWithFailTest(scriptName, getEntity(), getEntity().getAttribute("interactiveItem")))
        {
            if (player.getActionSender() != null)
            {
                player.getActionSender().sendMessage("Nothing interesting happens.");
                logger.severe("Could not find specialty item script : " + scriptName);
            }
        }
        /**
         * Submit a task to remove the interaction after the event has been
         * completed.
         */
        World.submit(new Task(2, true)
        {
            @Override
            protected void execute()
            {
                getEntity().removeAttribute("itemInteraction");
                getEntity().removeAttribute("interactiveItem");
                this.stop();
            }
        });

        this.stop();

    }
}
