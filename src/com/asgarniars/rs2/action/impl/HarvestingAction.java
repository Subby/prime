package com.asgarniars.rs2.action.impl;

import com.asgarniars.rs2.action.Action;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.players.GlobalObjectHandler;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;
import com.asgarniars.rs2.util.clip.RSObject;

/**
 * <p>
 * A harvesting action is a resource-gathering action, which includes, but is
 * not limited to, woodcutting and mining.
 * </p>
 * 
 * <p>
 * This class implements code related to all harvesting-type skills, such as
 * dealing with the action itself, looping, expiring the object (i.e. changing
 * rocks to the gray rock and trees to the stump), checking requirements and
 * giving out the harvested resources.
 * </p>
 * 
 * <p>
 * The individual woodcutting and mining classes implement things specific to
 * these individual skills such as random events.
 * </p>
 * 
 * @author Michael Bull <Scu11>
 * @author Joshua Barry <Ares>
 * 
 */
public abstract class HarvestingAction extends Action
{

    /**
     * Creates the harvesting action for the specified entity.
     * 
     * @param entity
     */
    public HarvestingAction(Entity entity)
    {
        super(entity, 0);
    }

    /**
     * Gets the amount of cycles before the object is interacted with.
     * 
     * @return The amount of cycles before the object is interacted with.
     */
    public abstract short getCycleCount();

    /**
     * Gets the game object we are harvesting.
     * 
     * @return The game object we are harvesting.
     */
    public abstract RSObject getGlobalObject();

    /**
     * Gets the game object that replaces the object we harvest.
     * 
     * @return The game object that replaces the object we harvest.
     */
    public abstract RSObject getReplacementObject();

    /**
     * Gets the amount of cycles it takes for the object to respawn.
     * 
     * @return The amount of cycles it takes for the object to respawn.
     */
    public abstract int getObjectRespawnTimer();

    /**
     * Gets the reward from harvesting the object.
     * 
     * @return The reward from harvesting the object.
     */
    public abstract Item getReward();

    /**
     * Gets the skill we are using to harvest.
     * 
     * @return The skill we are using to harvest.
     */
    public abstract int getSkill();

    /**
     * Gets the required level to harvest this object.
     * 
     * @return The required level to harvest this object.
     */
    public abstract int getRequiredLevel();

    /**
     * Gets the experience granted for each item that is successfully harvested.
     * 
     * @return The experience granted for each item that is successfully
     *         harvested.
     */
    public abstract double getExperience();

    /**
     * Gets the message sent when the mob's level is too low to harvest this
     * object.
     * 
     * @return The message sent when the mob's level is too low to harvest this
     *         object.
     */
    public abstract String getInsufficentLevelMessage();

    /**
     * Gets the message sent when the harvest successfully begins.
     * 
     * @return The message sent when the harvest successfully begins.
     */
    public abstract String getHarvestStartedMessage();

    /**
     * Gets the message sent when the mob successfully harvests from the object.
     * 
     * @return The message sent when the mob successfully harvests from the
     *         object.
     */
    public abstract String getSuccessfulHarvestMessage();

    /**
     * Gets the message sent when the mob has a full inventory.
     * 
     * @return The message sent when the mob has a full inventory.
     */
    public abstract String getInventoryFullMessage();

    /**
     * Gets the animation played whilst harvesting the object.
     * 
     * @return The animation played whilst harvesting the object.
     */
    public abstract short getAnimationId();

    /**
     * Performs extra checks that a specific harvest event independently uses,
     * e.g. checking for a pickaxe in mining.
     */
    public abstract boolean canHarvest();

    /**
     * This starts the actions animation and requirement checks, but prevents
     * the harvest from immediately executing.
     */
    private boolean started = false;

    /**
     * The current cycle time.
     */
    private int currentCycles = 0;

    /**
     * The amount of cycles before an animation.
     */
    private int lastAnimation = 0;

    /**
     * Gets the game objects maximum health.
     * 
     * @return The game objects maximum health.
     */
    public abstract int getGameObjectMaxHealth();

    /**
     * 
     * @return
     */
    public abstract byte getRespawnRate();

    @Override
    public CancelPolicy getCancelPolicy()
    {
        return CancelPolicy.ALWAYS;
    }

    @Override
    public StackPolicy getStackPolicy()
    {
        return StackPolicy.NEVER;
    }

    @Override
    public AnimationPolicy getAnimationPolicy()
    {
        return AnimationPolicy.RESET_ALL;
    }

    @Override
    public void execute()
    {

        final Player player = (Player) getEntity();

        if (!canHarvest())
        {
            this.stop();
            return;
        }

        if (!started)
        {
            started = true;
            player.getUpdateFlags().sendAnimation(getAnimationId());
            player.getActionSender().sendMessage(getHarvestStartedMessage());
            if (getGlobalObject().getMaxHealth() <= 0)
            {
                if (getGameObjectMaxHealth() == -1)
                {
                    getGlobalObject().setMaxHealth(1);
                }
                else
                {
                    getGlobalObject().setMaxHealth(getGameObjectMaxHealth());
                }
            }
            currentCycles = getCycleCount();
            return;
        }

        if (getGlobalObject().getCurrentHealth() < 1)
        {
            this.stop();
            return;
        }

        if (lastAnimation > 3)
        {
            player.getUpdateFlags().sendAnimation(getAnimationId());
            lastAnimation = 0;
        }
        lastAnimation++;

        if (currentCycles > 0)
        {
            currentCycles--;
            return;
        }

        currentCycles = getCycleCount();

        if (getGameObjectMaxHealth() != -1)
        {
            getGlobalObject().decreaseCurrentHealth(1);
        }

        if (player.getInventory().addItem(getReward()))
        {
            player.getActionSender().sendMessage(getSuccessfulHarvestMessage());
        }

        player.getSkill().addExperience(getSkill(), getExperience());

        if (getGlobalObject().getCurrentHealth() < 1)
        {
            getGlobalObject().setCurrentHealth(getGameObjectMaxHealth());

            GlobalObjectHandler.createGlobalObject(player, getReplacementObject());
            World.submit(new Task(getRespawnRate())
            {

                @Override
                protected void execute()
                {
                    GlobalObjectHandler.createGlobalObject(player, getGlobalObject());
                    this.stop();

                }

            });
            player.getUpdateFlags().sendAnimation(-1);
            this.stop();
            return;
        }
    }
}