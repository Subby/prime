package com.asgarniars.rs2.action.impl;

import com.asgarniars.rs2.action.Action;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.model.items.Item;

/**
 * TODO replace dialogue
 * <p>
 * A producing action is an action where on item is transformed into another,
 * typically this is in skills such as smithing and crafting.
 * </p>
 * 
 * <p>
 * This class implements code related to all production-type skills, such as
 * dealing with the action itself, replacing the items and checking levels.
 * </p>
 * 
 * <p>
 * The individual crafting, smithing, and other skills implement functionality
 * specific to them such as random events.
 * </p>
 * 
 * @author Graham Edgecombe
 * @author Michael <Scu11>
 * @author Joshua Barry <Ares>
 */
public abstract class ProductionAction extends Action
{

    /**
     * This starts the actions animation and requirement checks, but prevents
     * the production from immediately executing.
     */
    private boolean started = false;

    /**
     * The cycle count.
     */
    private int cycleCount = 0;

    /**
     * The amount of items to produce.
     */
    private int productionCount = 0;

    /**
     * Performs extra checks that a specific production event independently
     * uses, e.g. checking for ingredients in herblore.
     */
    public abstract boolean canProduce();

    /**
     * Creates the production action for the specified mob.
     * 
     * @param mob The mob to create the action for.
     */
    public ProductionAction(Entity entity)
    {
        super(entity, 0);
    }

    @Override
    public void execute()
    {

        Player player = (Player) getEntity();

        if (player.getSkillLevel(getSkill()) < getRequiredLevel())
        {
            player.getUpdateFlags().sendAnimation(-1);
            this.stop();
            return;
        }

        for (Item productionItem : getConsumedItems())
        {
            if (player.getInventory().getItemContainer().getCount(productionItem.getId()) < productionItem.getCount())
            {
                player.getUpdateFlags().sendAnimation(-1);
                this.stop();
                return;
            }
        }

        if (!canProduce())
        {
            this.stop();
            return;
        }

        if (!started)
        {
            started = true;

            if (getAnimation() != -1)
            {
                player.getUpdateFlags().sendAnimation(getAnimation());
            }
            if (getGraphic() != -1)
            {
                player.getUpdateFlags().sendGraphic(getGraphic());
            }

            productionCount = getProductionCount();
            cycleCount = getCycleCount();
            return;
        }

        if (cycleCount > 1)
        {
            cycleCount--;
            return;
        }

        if (getAnimation() > 0)
        {
            player.getUpdateFlags().sendAnimation(getAnimation());
        }
        if (getGraphic() > 0)
        {
            player.getUpdateFlags().sendGraphic(getGraphic());
        }

        cycleCount = getCycleCount();

        productionCount--;

        for (Item item : getConsumedItems())
        {
            player.getInventory().removeItem(item);
        }
        for (Item item : getRewards())
        {
            player.getInventory().addItem(item);
        }

        player.getActionSender().sendMessage(getSuccessfulProductionMessage());
        player.getSkill().addExperience(getSkill(), getExperience());

        if (productionCount < 1)
        {
            player.getUpdateFlags().sendAnimation(-1);
            this.stop();
            return;
        }

        for (Item item : getConsumedItems())
        {
            if (player.getInventory().getItemContainer().getCount(item.getId()) < item.getCount())
            {
                player.getUpdateFlags().sendAnimation(-1);
                this.stop();
                return;
            }
        }
    }

    /**
     * Gets the amount of cycles before the item is produced.
     * 
     * @return The amount of cycles before the item is produced.
     */
    public abstract int getCycleCount();

    /**
     * Gets the graphic played whilst producing the item.
     * 
     * @return The graphic played whilst producing the item.
     */
    public abstract short getGraphic();

    /**
     * Gets the animation played whilst producing the item.
     * 
     * @return The animation played whilst producing the item.
     */
    public abstract short getAnimation();

    /**
     * Gets the experience granted for each item that is successfully produced.
     * 
     * @return The experience granted for each item that is successfully
     *         produced.
     */
    public abstract double getExperience();

    @Override
    public Action.AnimationPolicy getAnimationPolicy()
    {
        return Action.AnimationPolicy.RESET_ALL;
    }

    @Override
    public Action.CancelPolicy getCancelPolicy()
    {
        return Action.CancelPolicy.ALWAYS;
    }

    /**
     * Gets the consumed item in the production of this item.
     * 
     * @return The consumed item in the production of this item.
     */
    public abstract Item[] getConsumedItems();

    /**
     * Gets the message sent when the Entity's level is too low to produce this
     * item.
     * 
     * @return The message sent when the Entity's level is too low to produce
     *         this item.
     */
    public abstract String getInsufficentLevelMessage();

    /**
     * Gets the amount of times an item is produced.
     * 
     * @return The amount of times an item is produced.
     */
    public abstract int getProductionCount();

    /**
     * Gets the required level to produce this item.
     * 
     * @return The required level to produce this item.
     */
    public abstract int getRequiredLevel();

    /**
     * Gets the rewarded items from production.
     * 
     * @return The rewarded items from production.
     */
    public abstract Item[] getRewards();

    /**
     * Gets the skill we are using to produce.
     * 
     * @return The skill we are using to produce.
     */
    public abstract int getSkill();

    @Override
    public Action.StackPolicy getStackPolicy()
    {
        return Action.StackPolicy.NEVER;
    }

    /**
     * Gets the message sent when the Entity successfully produces an item.
     * 
     * @return The message sent when the Entity successfully produce an item.
     */
    public abstract String getSuccessfulProductionMessage();

    public boolean isStarted()
    {
        return started;
    }

    public void setCycleCount(int cycleCount)
    {
        this.cycleCount = cycleCount;
    }

    public void setProductionCount(int productionCount)
    {
        this.productionCount = productionCount;
    }

    public void setStarted(boolean started)
    {
        this.started = started;
    }
}
