package com.asgarniars.rs2.action.impl;

import com.asgarniars.rs2.action.Action;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.npcs.Npc;

/**
 * Used to handle npc interactivity such as pickpocketing.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public abstract class EntityInteraction extends Action
{

    public EntityInteraction(Entity entity, int ticks)
    {
        super(entity, ticks);
    }

    /**
     * The message when you do not have the level required to interact with the
     * entity.
     * 
     * @return the message to display.
     */
    public abstract String getInsufficentLevelMessage();

    /**
     * The message when you begin to interact with the entity.
     * 
     * @return the message to display.
     */
    public abstract String getInteractionMessage();

    /**
     * The message when you succeed to interact as planned with the entity.
     * 
     * @return the message to display.
     */
    public abstract String getSuccessfulInteractionMessage();

    /**
     * The message when you fail to interact as planned with the entity.
     * 
     * @return the message to display.
     */
    public abstract String getUnsuccessfulInteractionMessage();

    /**
     * 
     * @return
     */
    public abstract Npc getInteractingNpc();

    /**
     * 
     * @return
     */
    public abstract Item[] getConsumedItems();

    /**
     * 
     * @return
     */
    public abstract Item[] getRewards();

    /**
     * 
     * @return
     */
    public abstract short getRequiredLevel();

}
