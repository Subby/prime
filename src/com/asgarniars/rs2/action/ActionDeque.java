package com.asgarniars.rs2.action;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.players.Inventory;
import com.asgarniars.rs2.task.Task;

/**
 * Stores a deque of pending actions.
 * 
 * @author Graham Edgecombe
 * @author Joshua Barry
 * 
 */
public class ActionDeque
{

    /**
     * The entity who's action queue this is.
     */
    private Entity entity;

    /**
     * The tickable.
     */
    private Task cycleTick;

    /**
     * The maximum number of actions allowed to be queued at once, deliberately
     * set to the size of the player's inventory.
     */
    public static final int MAXIMUM_SIZE = Inventory.SIZE;

    /**
     * A deque of <code>Action</code> objects.
     */
    private Deque<Action> actions = new ArrayDeque<Action>();

    public Deque<Action> getActions()
    {
        return actions;
    }

    /**
     * The default constructor.
     * 
     * @param entity The entity.
     */
    public ActionDeque(Entity entity)
    {
        this.entity = entity;
    }

    /**
     * Adds an <code>Action</code> to the queue.
     * 
     * @param action The action.
     */
    public void addAction(Action action)
    {
        if (actions.size() >= MAXIMUM_SIZE)
        {
            return;
        }
        switch (action.getStackPolicy())
        {
        case ALWAYS:
            break;
        case NEVER:
            clearAllActions();
            break;
        }
        switch (action.getAnimationPolicy())
        {
        case RESET_NONE:
            break;
        case RESET_ALL:
            entity.getUpdateFlags().sendAnimation(-1);
            entity.getUpdateFlags().sendGraphic(-1);
            break;
        }
        if (actions.size() > 0)
        {
            Iterator<Action> it = actions.iterator();
            while (it.hasNext())
            {
                Action actionTickable = it.next();
                switch (actionTickable.getStackPolicy())
                {
                case ALWAYS:
                    break;
                case NEVER:
                    actionTickable.stop();
                    it.remove();
                    break;
                }
            }
        }
        actions.add(action);
        /*
         * This is now converted as a tickable. Why? Because before, the system
         * would register a tickable that would execute a certain amount of
         * ticks later. What did this mean? E.g.
         * 
         * Home teleport started. First emote begins, and the second emote is
         * added as an Action, however it is already submitted as a tickable,
         * but you should be able to cancel out of it before the emote begins.
         * This system fixes that. - Joshua Barry.
         */
        if (cycleTick == null)
        {
            cycleTick = new Task(1)
            {
                @Override
                public void execute()
                {
                    try
                    {
                        if (actions.size() > 0)
                        {
                            Iterator<Action> it = actions.iterator();
                            while (it.hasNext())
                            {
                                Action actionTickable = it.next();
                                if (actionTickable.getCurrentTicks() > 0)
                                {
                                    actionTickable.decreaseTicks(1);
                                }
                                if (actionTickable.getCurrentTicks() == 0)
                                {
                                    actionTickable.execute();
                                    if (!actionTickable.isRunning())
                                    {
                                        it.remove();
                                    }
                                    else
                                    {
                                        actionTickable.setCurrentTicks(actionTickable.getTicks());
                                    }
                                }
                            }
                        }
                        else
                        {
                            cycleTick = null;
                            this.stop();
                        }
                    }
                    catch (Exception e)
                    {
                    }
                }
            };
            World.submit(cycleTick);
        }
    }

    /**
     * Purges actions in the queue with a <code>WalkablePolicy</code> of
     * <code>NON_WALKABLE</code>.
     */
    public void clearRemovableActions()
    {
        boolean resetAnimations = false;
        if (actions.size() > 0)
        {
            Iterator<Action> it = actions.iterator();
            while (it.hasNext())
            {
                Action actionTickable = it.next();
                switch (actionTickable.getCancelPolicy())
                {
                case ONLY_ON_WALK:
                    break;
                case ALWAYS:
                    actionTickable.stop();
                    it.remove();
                    break;
                }
                switch (actionTickable.getAnimationPolicy())
                {
                case RESET_NONE:
                    break;
                case RESET_ALL:
                    resetAnimations = true;
                    break;
                }
            }
        }
        if (actions.size() < 0)
        {
            if (cycleTick != null)
            {
                cycleTick.stop();
            }
            cycleTick = null;
        }
        if (resetAnimations)
        {
            entity.getUpdateFlags().sendAnimation(-1);
            entity.getUpdateFlags().sendGraphic(-1);
        }
    }

    /**
     * Clears the action queue and current action.
     */
    public void clearAllActions()
    {
        if (cycleTick != null)
        {
            cycleTick.stop();
        }
        cycleTick = null;
        boolean resetAnimations = false;
        if (actions.size() > 0)
        {
            for (Action action : actions)
            {
                switch (action.getAnimationPolicy())
                {
                case RESET_NONE:
                    break;
                case RESET_ALL:
                    resetAnimations = true;
                    break;
                }
                action.stop();
            }
            actions = new ArrayDeque<Action>();
        }
        if (resetAnimations)
        {
            entity.getUpdateFlags().sendAnimation(-1);
            entity.getUpdateFlags().sendGraphic(-1);
        }
    }
}