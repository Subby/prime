package com.asgarniars.rs2.task.impl;

import com.asgarniars.rs2.content.combat.util.Prayer;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * The prayer draining task
 * 
 * @author AkZu & Jacob
 * 
 */

public class PrayerDrainTask extends Task
{

    /**
     * The mob for who we are draining the prayer.
     */
    public Player player;

    /**
     * Creates the event to cycle every cycle.
     */
    public PrayerDrainTask(Player player)
    {
        super(1);
        this.player = player;
    }

    @Override
    protected void execute()
    {
        if (!player.isLoggedIn() || player.getAttribute("isDead") != null)
        {
            this.stop();
            return;
        }
        double amountToDrain = 0;
        for (int i = 0; i < player.getActivePrayers().length; i++)
        {
            if (player.getActivePrayers()[i])
            {
                double drain = (Double) Prayer.PRAYER_DATA[i][4];
                double bonus = 0.035 * player.getBonus(11);
                drain = drain * (1 + bonus);
                drain = 0.6 / drain;
                amountToDrain += drain;
            }
        }
        Prayer.getInstance().drainPrayer(player, amountToDrain);
    }
}
