package com.asgarniars.rs2.task.impl;

import com.asgarniars.rs2.task.Task;

/**
 * Creates a new task which ticks for a specified duration in the unit of
 * minutes.
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public abstract class MinuteIntervalCounter extends Task
{

    /**
     * Constructs a new counter.
     * 
     * @param duration The duration in minutes.
     * @param immediate Whether or not to submit the task immediately.
     */
    public MinuteIntervalCounter(int duration, boolean immediate)
    {
        super(100 * duration, immediate);
    }

    /**
     * Constructs a new counter.
     * 
     * @param duration The duration in minutes.
     */
    public MinuteIntervalCounter(int duration)
    {
        super(100 * duration);
    }

}
