package com.asgarniars.rs2.task.impl;

import com.asgarniars.rs2.content.combat.util.Prayer;
import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * Restores players (when npc support, Entitys) skill levels to normal.
 * 
 * @author AkZu
 * 
 */
public class SkillEquilizer extends Task
{

    /**
     * The player who's stats we're normalizing.
     */
    private Player player;

    /**
     * Creates the task to run every 30 seconds. If player doesn't have Rapid
     * heal/restore prayers activated the code just ticks a new cycle (waits 30
     * sec more).
     */
    public SkillEquilizer(Player player)
    {
        super(50);
        this.player = player;
    }

    int skipCycle = 0;
    int skipCycle2 = 0;

    @Override
    protected void execute()
    {
        if (player == null || !player.isLoggedIn())
        {
            this.stop();
            return;
        }
        // Don't kill the task when dead, just return from it.
        if (player.getAttribute("isDead") != null)
            return;

        if (player.getActivePrayers()[Prayer.RAPID_HEAL] || skipCycle == 1)
        {
            if (player.getSkill().getLevel()[3] > player.getSkill().getLevelForXP(player.getSkill().getExp()[3]))
            {
                player.getSkill().getLevel()[3] -= 1;
                player.getSkill().refresh(3);
            }
            else if (player.getSkill().getLevel()[3] != player.getSkill().getLevelForXP(player.getSkill().getExp()[3]))
            {
                player.getSkill().getLevel()[3] += 1;
                player.getSkill().refresh(3);
            }
            skipCycle = 0;
        }
        else
        {
            skipCycle++;
        }

        if (player.getActivePrayers()[Prayer.RAPID_RESTORE] || skipCycle2 == 1)
        {
            for (int i = 0; i < Skill.SKILL_COUNT; i++)
            {
                if (i == 5 || i == 3)
                    continue;
                if (player.getSkill().getLevel()[i] > player.getSkill().getLevelForXP(player.getSkill().getExp()[i]))
                {
                    player.getSkill().getLevel()[i] -= 1;
                    player.getSkill().refresh(i);
                }
                else if (player.getSkill().getLevel()[i] != player.getSkill().getLevelForXP(player.getSkill().getExp()[i]))
                {
                    player.getSkill().getLevel()[i] += 1;
                    player.getSkill().refresh(i);
                }
            }
            skipCycle2 = 0;
        }
        else
        {
            skipCycle2++;
        }
    }
}
