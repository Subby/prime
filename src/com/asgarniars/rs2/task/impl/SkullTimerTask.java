package com.asgarniars.rs2.task.impl;

import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * The skulltimer task.
 * 
 * @author AkZu & Jacob
 */

public class SkullTimerTask extends Task
{

    /**
     * The mob for we're counting skullticks.
     */
    public Player player;

    /**
     * Creates the event to cycle every cycle.
     */
    public SkullTimerTask(Player player)
    {
        super(1);
        this.player = player;
    }

    @Override
    protected void execute()
    {
        if (!player.isSkulled() || !player.isLoggedIn())
        {
            stop();
            return;
        }
        if (player.getSkullTimer() > 1)
        {
            player.setSkullTimer(player.getSkullTimer() - 1);
            return;
        }
        player.setSkulled(false);
    }
}
