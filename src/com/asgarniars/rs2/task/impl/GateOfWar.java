package com.asgarniars.rs2.task.impl;

import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;

/**
 * Handles the event of a player passing through the Gate of War.
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class GateOfWar extends Task
{
    private Player player;

    private Position position;

    /**
     * 
     * @param player The player to pass through the door.
     * @param position The position they teleport to.
     */
    public GateOfWar(Player player, Position position)
    {
        super(1, true);
        this.player = player;
        this.position = position;
    }

    @Override
    protected void execute()
    {
        player.teleport(position);
        player.getUpdateFlags().sendAnimation(4283);
        this.stop();

    }

}
