package com.asgarniars.rs2.util;

import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.players.Player;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class ShutDownListener extends Thread
{

    @Override
    public void run()
    {
        synchronized (World.getPlayers())
        {
            final Player[] players = World.getPlayers();
            for (Player p : players)
            {
                if (p != null && p.getIndex() != -1)
                {
                    try
                    {
                        p.logout();
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}