package com.asgarniars.rs2.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.asgarniars.rs2.model.players.Player;

/**
 * 8/6/2012 9:20PM TimeUtility - Methods for keeping track of time, specially
 * for the welcome screen.
 * 
 * @author AkZu
 */
public class TimeUtility
{

    public static final String DATE_FORMAT_NOW = "dd-MM-yyyy";

    private static TimeUtility instance = null;

    public static TimeUtility getInstance()
    {
        if (instance == null)
        {
            instance = new TimeUtility();
        }
        return instance;
    }

    public String loginDate()
    {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat date = new SimpleDateFormat(DATE_FORMAT_NOW);
        return date.format(cal.getTime());
    }

    public void runDateIPCheck(Player player)
    {
        if (player.getLastIp().startsWith((player.getHost())))
        {
        }
        else if (player.getLastIp().startsWith("0.0.0.0"))
        {
            player.setLastIp(player.getHost());
        }
        if (player.getLastLoginDay().startsWith(loginDate()))
        {
        }
        else if (player.getLastLoginDay() == "")
        {
            player.setLastLoginDay(loginDate());
        }
    }
}
