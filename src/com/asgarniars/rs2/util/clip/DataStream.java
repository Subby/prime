package com.asgarniars.rs2.util.clip;

/**
 * 
 * @author Joshua Barry
 * 
 */
public final class DataStream
{

    public int pointer;
    public byte[] buffer;

    public void skip(int length)
    {
        pointer += length;
    }

    public DataStream(byte[] buffer)
    {
        this.buffer = buffer;
        this.pointer = 0;
    }

    public int readUByte()
    {
        return buffer[pointer++] & 0xff;
    }

    public byte readByte()
    {
        return buffer[pointer++];
    }

    public int readUShort()
    {
        pointer += 2;
        return ((buffer[pointer - 2] & 0xff) << 8) + (buffer[pointer - 1] & 0xff);
    }

    public int readShort()
    {
        pointer += 2;
        int i = ((buffer[pointer - 2] & 0xff) << 8) + (buffer[pointer - 1] & 0xff);
        if (i > 32767)
        {
            i -= 0x10000;
        }
        return i;
    }

    public int read24Int()
    {
        pointer += 3;
        return ((buffer[pointer - 3] & 0xff) << 16) + ((buffer[pointer - 2] & 0xff) << 8) + (buffer[pointer - 1] & 0xff);
    }

    public int readInverse24Int()
    {
        pointer += 3;
        return ((buffer[pointer - 1] & 0xff) << 16) + ((buffer[pointer - 2] & 0xff) << 8) + (buffer[pointer - 3] & 0xff);
    }

    public int readInt()
    {
        pointer += 4;
        return ((buffer[pointer - 4] & 0xff) << 24) + ((buffer[pointer - 3] & 0xff) << 16) + ((buffer[pointer - 2] & 0xff) << 8) + (buffer[pointer - 1] & 0xff);
    }

    public long readLong()
    {
        long l = readInt() & 0xffffffffL;
        long l1 = readInt() & 0xffffffffL;
        return (l << 32) + l1;
    }

    public String readString()
    {
        int offset = pointer;
        while (buffer[pointer++] != 10)
        {
            /* Empty */
        }
        return new String(buffer, offset, pointer - offset - 1);
    }

    public String readNewString()
    {
        int offset = pointer;
        while (buffer[pointer++] != 0)
        {
            /* Empty */
        }
        return new String(buffer, offset, pointer - offset - 1);
    }

    public byte[] readBytes()
    {
        int offset = pointer;
        while (buffer[pointer++] != 10)
        {
            /* Empty */
        }
        byte[] data = new byte[pointer - offset - 1];
        System.arraycopy(buffer, offset, data, offset - offset, pointer - 1 - offset);
        return data;
    }

    public void readBytes(int length, int offset, byte[] dest)
    {
        for (int ptr = offset; ptr < offset + length; ptr++)
        {
            dest[ptr] = buffer[pointer++];
        }
    }

}
