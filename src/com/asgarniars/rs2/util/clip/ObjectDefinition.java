package com.asgarniars.rs2.util.clip;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.logging.Logger;

import com.asgarniars.rs2.Constants;

/**
 * 
 * @author Joshua Barry
 * 
 */
public final class ObjectDefinition
{

    public static ObjectDefinition forId(int id)
    {
        for (int index = 0; index < 20; index++)
        {
            if (cache[index].type == id)
            {
                return cache[index];
            }
        }

        ObjectDefinition.cachePointer = (ObjectDefinition.cachePointer + 1) % 20;
        ObjectDefinition definition = ObjectDefinition.cache[ObjectDefinition.cachePointer];
        definition.type = id;
        definition.reset();
        byte[] buffer = ObjectDefinition.archive.get(id);
        if (buffer != null && buffer.length > 0)
        {
            definition.read(new DataStream(buffer));
        }
        return definition;
    }

    private void reset()
    {
        anIntArray773 = null;
        anIntArray776 = null;
        name = null;
        description = null;
        modifiedModelColors = null;
        originalModelColors = null;
        width = 1;
        height = 1;
        isSolid = true;
        canShoot = true;
        hasActions = false;
        aBoolean762 = false;
        aBoolean764 = false;
        anInt781 = -1;
        anInt775 = 16;
        actions = null;
        anInt746 = -1;
        anInt758 = -1;
        aBoolean779 = true;
        anInt768 = 0;
        aBoolean736 = false;
        anInt774 = -1;
        anInt749 = -1;
        childrenIDs = null;
    }

    public static void load()
    {
        archive = new MemoryArchive(new ByteStream(getBuffer("loc.dat")), new ByteStream(getBuffer("loc.idx")));
        cache = new ObjectDefinition[20];
        for (int k = 0; k < 20; k++)
        {
            cache[k] = new ObjectDefinition();
        }
        Logger.getAnonymousLogger().info("Loaded object definitions.");
    }

    public static byte[] getBuffer(String s)
    {
        try
        {
            File f = new File(Constants.CONFIG_DIRECTORY + "world/object/" + s);
            if (!f.exists())
            {
                return null;
            }
            byte[] buffer = new byte[(int) f.length()];
            DataInputStream dis = new DataInputStream(new FileInputStream(f));
            dis.readFully(buffer);
            dis.close();
            return buffer;
        }
        catch (Exception exception)
        {
            /* Empty */
        }
        return null;
    }

    private void read(DataStream stream)
    {
        int flag = -1;
        while (true)
        {
            int type = stream.readUByte();
            if (type == 0)
            {
                break;
            }
            if (type == 1)
            {
                int len = stream.readUByte();
                if (len > 0)
                {
                    if (anIntArray773 == null || lowMem)
                    {
                        anIntArray776 = new int[len];
                        anIntArray773 = new int[len];
                        for (int k1 = 0; k1 < len; k1++)
                        {
                            anIntArray773[k1] = stream.readUShort();
                            anIntArray776[k1] = stream.readUByte();
                        }
                    }
                    else
                    {
                        stream.pointer += len * 3;
                    }
                }
            }
            else if (type == 2)
            {
                name = stream.readNewString();
            }
            else if (type == 5)
            {
                int len = stream.readUByte();
                if (len > 0)
                {
                    if (anIntArray773 == null || lowMem)
                    {
                        anIntArray776 = null;
                        anIntArray773 = new int[len];
                        for (int l1 = 0; l1 < len; l1++)
                            anIntArray773[l1] = stream.readUShort();
                    }
                    else
                    {
                        stream.pointer += len * 2;
                    }
                }
            }
            else if (type == 14)
            {
                width = stream.readUByte();
            }
            else if (type == 15)
            {
                height = stream.readUByte();
            }
            else if (type == 17)
            {
                isSolid = false;
            }
            else if (type == 18)
            {
                canShoot = false;
            }
            else if (type == 19)
            {
                hasActions = (stream.readUByte() == 1);
            }
            else if (type == 21)
            {
                aBoolean762 = true;
            }
            else if (type == 22)
            {
                /* Empty */
            }
            else if (type == 23)
            {
                aBoolean764 = true;
            }
            else if (type == 24)
            {
                anInt781 = stream.readUShort();
                if (anInt781 == 65535)
                {
                    anInt781 = -1;
                }
            }
            else if (type == 27)
            {
                continue;
            }
            else if (type == 28)
            {
                anInt775 = stream.readUByte();
            }
            else if (type == 29)
            {
                stream.readByte();
            }
            else if (type == 39)
            {
                stream.readByte();
            }
            else if (type >= 30 && type < 39)
            {
                if (actions == null)
                {
                    actions = new String[5];
                }
                actions[type - 30] = stream.readNewString();
                hasActions = true;
                if (actions[type - 30].equalsIgnoreCase("hidden"))
                {
                    actions[type - 30] = null;
                }
            }
            else if (type == 40)
            {
                int i1 = stream.readUByte();
                modifiedModelColors = new int[i1];
                originalModelColors = new int[i1];
                for (int i2 = 0; i2 < i1; i2++)
                {
                    modifiedModelColors[i2] = stream.readUShort();
                    originalModelColors[i2] = stream.readUShort();
                }
            }
            else if (type == 41)
            {
                int l = stream.readUByte();
                stream.skip(l * 4);
            }
            else if (type == 42)
            {
                int l = stream.readUByte();
                stream.skip(l);
            }
            else if (type == 60)
            {
                anInt746 = stream.readUShort();
            }
            else if (type == 62)
            {
                /* Empty */
            }
            else if (type == 64)
            {
                aBoolean779 = false;
            }
            else if (type == 65)
            {
                stream.readUShort();
            }
            else if (type == 66)
            {
                stream.readUShort();
            }
            else if (type == 67)
            {
                stream.readUShort();
            }
            else if (type == 68)
            {
                anInt758 = stream.readUShort();
            }
            else if (type == 69)
            {
                anInt768 = stream.readUByte();
            }
            else if (type == 70)
            {
                stream.readShort();
            }
            else if (type == 71)
            {
                stream.readShort();
            }
            else if (type == 72)
            {
                stream.readShort();
            }
            else if (type == 73)
            {
                aBoolean736 = true;
            }
            else if (type == 74)
            {
                /* Empty */
            }
            else if (type == 75)
            {
                stream.readUByte();
            }
            else if (type == 77 || type == 92)
            {
                anInt774 = stream.readUShort();
                if (anInt774 == 65535)
                {
                    anInt774 = -1;
                }
                anInt749 = stream.readUShort();
                if (anInt749 == 65535)
                {
                    anInt749 = -1;
                }
                int endChild = -1;
                if (type == 92)
                {
                    endChild = stream.readUShort();
                    if (endChild == 65535)
                    {
                        endChild = -1;
                    }
                }
                int j1 = stream.readUByte();
                childrenIDs = new int[j1 + 2];
                for (int j2 = 0; j2 <= j1; j2++)
                {
                    childrenIDs[j2] = stream.readUShort();
                    if (childrenIDs[j2] == 65535)
                    {
                        childrenIDs[j2] = -1;
                    }
                }
                childrenIDs[j1 + 1] = endChild;
            }
            else if (type == 78)
                stream.skip(3);
            else if (type == 79)
            {
                stream.skip(5);
                int l = stream.readUByte();
                stream.skip(l * 2);
            }
            else if (type == 81)
            {
                stream.skip(1);
            }
            else if (type == 82 || type == 88 || type == 89 || type == 90 || type == 91 || type == 94 || type == 95 || type == 96 || type == 97)
            {
                continue;
            }
            else if (type == 93)
            {
                stream.skip(2);
            }
            else if (type == 249)
            {
                int l = stream.readUByte();
                for (int ii = 0; ii < l; ii++)
                {
                    boolean b = (stream.readUByte() == 1);
                    stream.skip(3);
                    if (b)
                    {
                        stream.readNewString();
                    }
                    else
                    {
                        stream.skip(4);
                    }
                }
            }
            else
            {
                System.out.println("Unknown config: " + type);
            }
        }
        if (flag == -1)
        {
            hasActions = anIntArray773 != null && (anIntArray776 == null || anIntArray776[0] == 10);
            if (actions != null)
            {
                hasActions = true;
            }
        }
    }

    private ObjectDefinition()
    {
        type = -1;
    }

    public boolean hasActions()
    {
        return hasActions;
    }

    public boolean hasName()
    {
        return name != null && name.length() > 1;
    }

    public boolean isShootable()
    {
        return canShoot;
    }

    public int xLength()
    {
        return width;
    }

    public int yLength()
    {
        return height;
    }

    public boolean isSolid()
    {
        return isSolid;
    }

    public String getName()
    {
        return name;
    }

    public boolean aBoolean736;
    private String name;
    public int width;
    public int anInt746;
    private int[] originalModelColors;
    public int anInt749;
    public static boolean lowMem;
    public int type;
    public boolean canShoot;
    public int anInt758;
    public int childrenIDs[];
    public int height;
    public boolean aBoolean762;
    public boolean aBoolean764;
    public boolean isSolid;
    public int anInt768;
    private static int cachePointer;
    private int[] anIntArray773;
    public int anInt774;
    public int anInt775;
    private int[] anIntArray776;
    public byte description[];
    public boolean hasActions;
    public boolean aBoolean779;
    public int anInt781;
    private static ObjectDefinition[] cache;
    private int[] modifiedModelColors;
    public String actions[];
    private static MemoryArchive archive;

}
