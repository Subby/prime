package com.asgarniars.rs2.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.util.clip.RegionClipping;

/**
 * A collection of miscellaneous utility methods and constants.
 * 
 * @author blakeman8192
 */
public class Misc
{

    public static final Random random = new Random();

    public static final int NORTH = 1, SOUTH = 6, EAST = 4, WEST = 3, NORTH_EAST = 2, SOUTH_EAST = 7, NORTH_WEST = 0, SOUTH_WEST = 5;

    public static final byte[] DIRECTION_DELTA_X = new byte[]
    { -1, 0, 1, -1, 1, -1, 0, 1 };

    public static final byte[] DIRECTION_DELTA_Y = new byte[]
    { 1, 1, 1, 0, 0, -1, -1, -1 };

    private static char X_LATE_TABLE[] =
    { ' ', 'e', 't', 'a', 'o', 'i', 'h', 'n', 's', 'r', 'd', 'l', 'u', 'm', 'w', 'c', 'y', 'f', 'g', 'p', 'b', 'v', 'k', 'x', 'j', 'q', 'z', 228, 229, 246, '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '!', '?', '.', ',', ':', ';', '(', ')', '-', '&', '*', '\'', '@', '#', '+', '=', '$', 163, '%', '"', '[', ']', '>', '<', '^', '/', '_', '|', 189, 180, '`', 167, 8364, '}', 168, '~', 164, '{', 'E', 'T', 'A', 'O', 'I', 'H', 'N', 'S', 'R', 'D', 'L', 'U', 'M', 'W', 'C', 'Y', 'F', 'G', 'P', 'B', 'V', 'K', 'X', 'J', 'Q', 'Z', 196, 197, 214 };

    /**
     * Unpacks text.
     * 
     * @param packedData The packet text.
     * @param size The length.
     * @return The string.
     */
    public static String textUnpack(byte packedData[], int size)
    {
        byte[] decodeBuf = new byte[4096];
        int idx = 0;
        for (int i = 0; i < size; i++)
        {
            int val = packedData[i];
            decodeBuf[idx++] = (byte) X_LATE_TABLE[val];
        }
        return new String(decodeBuf, 0, idx);
    }

    /**
     * Client code, <AkZu> I use this for duel arena interface haha this
     * basically colors the username based on combat level variance - sneaky.
     * 
     * @param my_combat_level
     * @param other_combat_level
     * @return
     */
    public static String getHighightingColor(int my_combat_level, int other_combat_level)
    {
        int k = my_combat_level - other_combat_level;
        if (k < -9)
            return "@red@";
        if (k < -6)
            return "@or3@";
        if (k < -3)
            return "@or2@";
        if (k < 0)
            return "@or1@";
        if (k > 9)
            return "@gre@";
        if (k > 6)
            return "@gr3@";
        if (k > 3)
            return "@gr2@";
        if (k > 0)
            return "@gr1@";
        else
            return "@yel@";
    }

    /**
     * Optimises text.
     * 
     * @param text The text to optimise.
     * @return The text.
     */
    public static String optimizeText(String text)
    {
        int skip = 0;
        for (int i1 = 0; i1 < text.length(); i1++)
        {
            if (skip == 2)
            {
                skip = 0;
                text = String.format("%s%s%s", text.subSequence(0, i1), Character.toUpperCase(text.charAt(i1)), text.substring(i1 + 1));
                continue;
            }
            if (skip == 1)
            {
                skip = 0;
                if (Character.isUpperCase(text.charAt(i1)))
                {
                    continue;
                }
            }
            if (Character.isUpperCase(text.charAt(i1)))
            {
                text = String.format("%s%s%s", text.subSequence(0, i1), Character.toLowerCase(text.charAt(i1)), text.substring(i1 + 1));
            }
            if (text.charAt(i1) == '.' || text.charAt(i1) == '?' || text.charAt(i1) == '!')
            {
                skip = 2;
                continue;
            }
            if (Character.isWhitespace(text.charAt(i1)) || !Character.isLetter(text.charAt(i1)))
                skip = 1;
            if (i1 == 0)
            {
                text = String.format("%s%s", Character.toUpperCase(text.charAt(0)), text.substring(1));
            }
        }
        return text;
    }

    /**
     * Packs text.
     * 
     * @param packedData The destination of the packed text.
     * @param text The unpacked text.
     */
    public static void textPack(byte packedData[], String text)
    {
        if (text.length() > 80)
            text = text.substring(0, 80);

        int ofs = 0;
        for (int idx = 0; idx < text.length(); idx++)
        {
            char c = text.charAt(idx);
            int tableIdx = 0;
            for (int i = 0; i < X_LATE_TABLE.length; i++)
            {
                if (c == X_LATE_TABLE[i])
                {
                    tableIdx = i;
                    break;
                }
            }
            packedData[ofs++] = (byte) (tableIdx);
        }
    }

    /**
     * Filters invalid characters out of a string.
     * 
     * @param s The string.
     * @return The filtered string.
     */
    public static String filterText(String s)
    {
        StringBuilder bldr = new StringBuilder();
        for (char c : s.toCharArray())
        {
            boolean valid = false;
            for (char validChar : X_LATE_TABLE)
            {
                if (validChar == c)
                {
                    valid = true;
                }
            }
            if (valid)
            {
                bldr.append(c);
            }
        }
        return bldr.toString();
    }

    /**
     * Returns the delta coordinates. Note that the returned Position is not an
     * actual position, instead it's values represent the delta values between
     * the two arguments.
     * 
     * @param a the first position
     * @param b the second position
     * @return the delta coordinates contained within a position
     */
    public static Position delta(Position a, Position b)
    {
        return new Position(b.getX() - a.getX(), b.getY() - a.getY(), a.getZ());
    }

    public static int getDistance(Position a, Position b)
    {
        int deltaX = b.getX() - a.getX();
        int deltaY = b.getY() - a.getY();
        return (int) Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
    }

    /**
     * Calculates the direction between the two coordinates.
     * 
     * @param dx the first coordinate
     * @param dy the second coordinate
     * @return the direction
     */
    public static int direction(int dx, int dy)
    {
        if (dx < 0)
        {
            if (dy < 0)
            {
                return SOUTH_WEST;
            }
            else if (dy > 0)
            {
                return NORTH_WEST;
            }
            else
            {
                return WEST;
            }
        }
        else if (dx > 0)
        {
            if (dy < 0)
            {
                return SOUTH_EAST;
            }
            else if (dy > 0)
            {
                return NORTH_EAST;
            }
            else
            {
                return EAST;
            }
        }
        else
        {
            if (dy < 0)
            {
                return SOUTH;
            }
            else if (dy > 0)
            {
                return NORTH;
            }
            else
            {
                return -1;
            }
        }
    }

    public static String formatNumber(double number)
    {
        NumberFormat format = NumberFormat.getIntegerInstance(Locale.US);
        return format.format(number);
    }

    /**
     * A simple logging utility that prefixes all messages with a timestamp.
     * 
     * @author blakeman8192
     */
    public static class TimestampLogger extends PrintStream
    {

        /**
         * The OutputStream to log to.
         * 
         * @param out
         */
        public TimestampLogger(OutputStream out, String file) throws IOException
        {
            super(out);
            BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));
            writer.write("Illuminati want my mind, soul and body.");
            writer.close();
        }

        public TimestampLogger(OutputStream out)
        {
            super(out);
        }

    }

    /**
     * A simple timing utility.
     * 
     * @author blakeman8192
     */
    public static class Stopwatch
    {

        /** The cached time. */
        private long time = System.currentTimeMillis();

        /**
         * Resets this stopwatch.
         */
        public void reset()
        {
            time = System.currentTimeMillis();
        }

        /**
         * Returns the amount of time elapsed (in milliseconds) since this
         * object was initialized, or since the last call to the "reset()"
         * method.
         * 
         * @return the elapsed time (in milliseconds)
         */
        public long elapsed()
        {
            return System.currentTimeMillis() - time;
        }
    }

    public static String intToString(int intToChange)
    {
        if (intToChange == 1)
            return "first";
        else if (intToChange == 2)
            return "second";
        else if (intToChange == 3)
            return "third";
        else if (intToChange == 4)
            return "fourth";
        return "first";
    }

    public static String trimMinutes(int amount)
    {
        if (amount < 2)
            return "minute";
        return "minutes";
    }

    /**
     * Checks if object exists
     */
    public static boolean objectExists(Player player, int id, int x, int y, int height)
    {
        if (RegionClipping.getRSObject(player, x, y, height) == null)
            return false;

        return RegionClipping.getRSObject(player, x, y, height).getId() == id;
    }

    /**
     * Capitalize txt utils
     */
    public static String capitalize(String s)
    {
        for (int i = 0; i < s.length(); i++)
        {
            if (i == 0)
            {
                s = String.format("%s%s", Character.toUpperCase(s.charAt(0)), s.substring(1));
            }
            if (!Character.isLetterOrDigit(s.charAt(i)))
            {
                if (i + 1 < s.length())
                {
                    s = String.format("%s%s%s", s.subSequence(0, i + 1), Character.toUpperCase(s.charAt(i + 1)), s.substring(i + 2));
                }
            }
        }
        return s;
    }

    /**
     * Capitalize the first letter of a string.
     * 
     * @param s
     * @return
     */
    public static String capitalizeFirst(String s)
    {
        return s.replaceFirst(String.valueOf(s.charAt(0)), String.valueOf(s.charAt(0)).toUpperCase());
    }

    /**
     * Generates a random number between 0 and the range
     */
    public static int random(int range)
    {
        return (int) (Math.random() * (range));
    }

    public static int randomMinMax(int min, int max)
    {
        return random.nextInt(max - min) + min;
    }

    /**
     * 
     * @param arrayAn array of integer values.
     * @return A random number from the array.
     */
    public static int generatedValue(int[] array)
    {
        int rnd = random.nextInt(array.length);
        return array[rnd];
    }

    public static int generatedValue(Integer[] values)
    {
        int idx = random.nextInt(values.length);
        return values[idx];
    }

    public static int generatedValue(List<Integer> values)
    {
        int idx = random.nextInt(values.size());
        int value = values.get(idx);
        values.remove(idx);
        return value;
    }

    /**
     * 
     * @param array An array of short values.
     * @return A random number from the array.
     */
    public static int generatedValue(short[] array)
    {

        int rnd = random.nextInt(array.length);
        return array[rnd];
    }

    /**
     * @author Joshua Barry <Ares>
     * @param list
     * @return
     */
    public static String generatedString(String[] list)
    {
        String randomString = "";
        try
        {
            int randomIndex = random.nextInt(list.length);
            randomString = list[randomIndex];
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            e.printStackTrace();
        }
        return randomString;
    }

    public static int getDayOfYear()
    {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int days = 0;
        int[] daysOfTheMonth =
        { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0))
        {
            daysOfTheMonth[1] = 29;
        }
        days += c.get(Calendar.DAY_OF_MONTH);
        for (int i = 0; i < daysOfTheMonth.length; i++)
        {
            if (i < month)
                days += daysOfTheMonth[i];
        }
        return days;
    }

    public static int getYear()
    {
        Calendar calender = Calendar.getInstance();
        return calender.get(Calendar.YEAR);
    }

    public static String getArticle(String text)
    {
        char first = text.toLowerCase().toCharArray()[0];
        char[] vowels =
        { 'a', 'e', 'i', 'o', 'u' };
        for (char c : vowels)
        {
            if (c == first)
            {
                return "an";
            }
        }
        return "a";
    }

}
