package com.asgarniars.rs2.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * @author Joshua Barry
 * 
 */
public class ScriptManager
{

    /**
     * The global access point instance.
     */
    protected static ScriptManager instance;

    /**
     * The script engine manager.
     */
    protected static ScriptEngineManager manager;

    /**
     * The script engine.
     */
    protected static ScriptEngine engine;

    /**
     * The logger.
     */
    private final Logger logger = Logger.getAnonymousLogger();

    /**
     * Access to the script manager.
     * 
     * @return
     */
    public static ScriptManager getSingleton()
    {
        if (instance == null)
        {
            instance = new ScriptManager();
        }
        return instance;
    }

    /**
     * Initialize the class variables.
     */
    private ScriptManager()
    {
        manager = new ScriptEngineManager();
        engine = manager.getEngineByName("python");
    }

    /**
     * Invokes a Python function.
     * 
     * @param identifier The identifier of the function.
     * @param args The function arguments.
     * @return if the script was found
     */
    public boolean invokeWithFailTest(String identifier, Object... args)
    {
        Invocable invEngine = (Invocable) engine;
        try
        {
            invEngine.invokeFunction(identifier, args);
            return true;
        }
        catch (NoSuchMethodException ex)
        {
            return false;
        }
        catch (ScriptException ex)
        {
            logger.log(Level.WARNING, "ScriptException thrown!", ex);
            return false;
        }
    }

    /**
     * Invokes a Python function.
     * 
     * @param identifier The identifier of the function.
     * @param args The function arguments.
     */
    public void invoke(String identifier, Object... args)
    {
        Invocable invEngine = (Invocable) engine;
        try
        {
            invEngine.invokeFunction(identifier, args);
        }
        catch (NoSuchMethodException ex)
        {
            logger.warning("Attempted to call non-existant function: " + identifier);
        }
        catch (ScriptException ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * Load all scripts in a given directory.
     * 
     * @param directory
     */
    public void loadScripts(String directory)
    {
        File dir = new File(directory);
        if (dir.exists() && dir.isDirectory())
        {
            short parsed = 0;
            File[] children = dir.listFiles();
            logger.info("Loading python scripts...");
            for (File child : children)
            {
                if (child.isFile() && child.getName().endsWith(".py"))
                    try
                    {
                        final long start = System.nanoTime();
                        engine.eval(new InputStreamReader(new FileInputStream(child)));
                        final long elapsed = System.nanoTime() - start;
                        logger.info("Loaded script: " + child.getName() + " in " + elapsed / 1000000D + " milliseconds");
                        parsed++;
                    }
                    catch (ScriptException ex)
                    {
                        logger.severe("Unable to load script: " + child + "!");
                        logger.severe(ex.getMessage());
                    }
                    catch (FileNotFoundException ex)
                    {
                        logger.severe("Unable to find script: " + child + "!");
                        logger.severe(ex.toString());
                    }
                else if (child.isDirectory())
                {
                    loadScripts(child.getPath());
                }
            }

            logger.info("Parsed a total of " + parsed + " scripts");
        }
    }

}
