package com.asgarniars.rs2.model.items;

import com.asgarniars.rs2.model.items.ItemManager.ItemDefinition;

/**
 * Represents a single item.
 * 
 * @author Graham Edgecombe
 * 
 */
public class Item
{

    /**
     * The id.
     */
    private short id;

    /**
     * The number of items.
     */
    private int count;

    /**
     * Creates a single item.
     * 
     * @param id The id.
     */
    public Item(int id)
    {
        this(id, 1);
    }

    /**
     * Sets the count of the item.
     * 
     * @param count The count to set to.
     */
    public void setCount(int count)
    {
        this.count = count;
    }

    /**
     * Creates a stacked item.
     * 
     * @param id The id.
     * @param count The number of items.
     * @throws IllegalArgumentException if count is negative.
     */
    public Item(int id, int count)
    {
        if (count < 0)
        {
            throw new IllegalArgumentException("Count cannot be negative.");
        }
        this.id = (short) id;
        this.count = count;
    }

    /**
     * Gets the item id.
     * 
     * @return The item id.
     */
    public int getId()
    {
        return id;
    }

    /**
     * Gets the count.
     * 
     * @return The count.
     */
    public int getCount()
    {
        return count;
    }

    /**
     * Gets the item's definition.
     * 
     * @return The definition of this item.
     */
    public ItemDefinition getDefinition()
    {
        return ItemManager.getInstance().getItemDefinitions()[id];
    }

    @Override
    public String toString()
    {
        return Item.class.getName() + " [id=" + id + ", count=" + count + "]";
    }

}
