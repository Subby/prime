package com.asgarniars.rs2.model.items;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.io.XStreamController;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.players.Equipment;
import com.asgarniars.rs2.model.players.GroundItem;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.model.region.Region;
import com.asgarniars.rs2.util.Language;
import com.asgarniars.rs2.util.Misc;

/**
 * In conjuction with the regional system!
 * 
 * @author AkZu
 * 
 */
public class ItemManager
{

    private static ItemManager instance = new ItemManager();

    private static final Logger logger = Logger.getLogger(ItemManager.class.getName());

    private Collection<GroundItem> groundItems = new ArrayList<GroundItem>();
    private ItemDefinition[] definitions = new ItemDefinition[Constants.MAX_ITEMS];

    public void addGroundItem(GroundItem g)
    {
        synchronized (groundItems)
        {
            groundItems.add(g);
        }
        synchronized (g.getRegion().getTile(g.getPos()).getGroundItems())
        {
            g.getRegion().getTile(g.getPos()).getGroundItems().add(g);
        }
    }

    public void removeGroundItem(GroundItem g)
    {
        synchronized (groundItems)
        {
            groundItems.remove(g);
        }
        synchronized (g.getRegion().getTile(g.getPos()).getGroundItems())
        {
            g.getRegion().getTile(g.getPos()).getGroundItems().remove(g);
        }
    }

    // TODO:
    public void spawnGroundItems()
    {
        /*
         * for (int index = 0; index <
         * GlobalItemSpawns.getInstance().getTotalSpawns(); index++) {
         * tickRespawnableItems(new GroundItem(null, new
         * Item(GlobalItemLoader.getItemSpawn(index).getId(), GlobalItemLoader
         * .getItemSpawn(index).getAmount()),
         * GlobalItemLoader.getItemSpawn(index).getPosition(), true),
         * GlobalItemLoader .getItemSpawn(index).getTime(), true); }
         */
    }

    /**
     * Ticks all available ground items in this server and processes them.
     */
    public void tick()
    {

        // TODO Synchronizing issues
        // Copy the map OR face concurrent modification exceptions ;)
        Collection<GroundItem> groundItems_copy = new ArrayList<GroundItem>();
        groundItems_copy.addAll(groundItems);

        for (GroundItem g : groundItems_copy)
        {

            if (g == null)
                continue;

            // -- Reduce the ground item's timer, if it's not respawnable type.
            if (g.getTime() > 0 && !g.isRespawn())
                g.setTime(g.getTime() - 1);

            // -- If ground item has been active for 1 full minute, and is not
            // respawnable, it will be set as global.
            if (!isUntradeable(g.getItem().getId()) && g.getTime() == Constants.SHOW_ALL_GROUND_ITEMS && !g.isRespawn())
            {
                g.setGlobal(true);
                showGlobalItem(g);
            }

            // -- If ground item's timer hit's 0 and isn't respawnable, it will
            // be removed of the server.
            if (g.getTime() == 0 && !g.isRespawn())
            {
                removeGroundItem(g);
                removeGlobalItem(g);
            }
        }
    }

    // -- Check if ground item exist's at given pos and ID..
    // -- Loop through ONLY our current region ground items..
    public boolean itemExists(int itemId, Position pos)
    {
        for (GroundItem g : World.getInstance().getRegionManager().getRegionByLocation(pos).getTile(pos).getGroundItems())
            if (g != null && g.getItem().getId() == itemId && g.getPos().getX() == pos.getX() && g.getPos().getZ() == pos.getZ() && g.getPos().getY() == pos.getY())
                return true;
        return false;
    }

    // -- Pick up the ground item on given position and given item Id..
    public void pickupItem(final Player p, final int itemId, final Position pos)
    {
        for (GroundItem g : World.getInstance().getRegionManager().getRegionByLocation(pos).getTile(pos).getGroundItems())
        {

            if (g == null || (!g.isGlobal() && g.getOwner() != null && !g.getOwner().equals(p.getUsername())))
                continue;

            if (g.getItem().getId() == itemId && g.getPos().getX() == p.getPosition().getX() && g.getPos().getY() == p.getPosition().getY() && g.getPos().getZ() == p.getPosition().getZ() && p.getMovementHandler().isEmpty() && Misc.getDistance(p.getPosition(), g.getPos()) <= 1)
            {

                // -- Ground item timed out..
                if (g.getTime() <= 0)
                    return;

                // -- Stackable count fix..
                if (g.getItem().getDefinition().isStackable() && p.getInventory().getItemContainer().contains(g.getItem().getId()))
                {
                    int allowed_amt = Integer.MAX_VALUE - p.getInventory().getItemContainer().getById(g.getItem().getId()).getCount();

                    if (allowed_amt > g.getItem().getCount())
                        allowed_amt = g.getItem().getCount();

                    if (allowed_amt <= 0)
                    {
                        p.getActionSender().sendMessage(Language.NO_SPACE);
                        p.stopAllActions(true);
                        p.setAttribute("pickedUpItem", (byte) 1);
                        return;
                    }

                    if (g.getItem().getCount() - allowed_amt > 0)
                    {
                        g.getItem().setCount(g.getItem().getCount() - allowed_amt);
                        if (g.isGlobal())
                            editGlobalGroundItemCount(g);
                        else
                            p.getActionSender().editGroundItemAmount(g);
                    }
                    else
                    {
                        removeGroundItem(g);

                        if (g.isGlobal())
                            removeGlobalItem(g);
                        else
                            p.getActionSender().removeGroundItem(g);
                    }

                    p.getInventory().addItem(new Item(g.getItem().getId(), allowed_amt));
                }
                else
                {

                    p.getInventory().addItem(new Item(g.getItem().getId(), g.getItem().getCount()));

                    // -- If it's respawnable type of item, let it spawn again
                    if (g.isRespawn())
                        tickRespawnableItems(g, g.getTime(), false);

                    removeGroundItem(g);

                    if (g.isGlobal())
                        removeGlobalItem(g);
                    else
                        p.getActionSender().removeGroundItem(g);
                }
                p.stopAllActions(true);
                p.setAttribute("pickedUpItem", (byte) 1);
                return;
            }
        }
    }

    public void showGlobalItem(GroundItem g)
    {

        // -- Looping through the nearest regions of the ground item
        for (Region region : World.getInstance().getRegionManager().getSurroundingRegions(g.getPos()))
        {
            // -- Looping through all the players near the ground item
            for (Entity entity : region.getMobs())
            {

                if (entity == null || entity.isNpc())
                    continue;

                // Showing the ground item for other players near the ground
                // item (not for our player)
                if (!((Player) entity).getUsername().equals(g.getOwner()))
                    if (Misc.getDistance(g.getPos(), entity.getPosition()) <= 60 && entity.getPosition().getZ() == g.getPos().getZ())
                        ((Player) entity).getActionSender().sendGroundItem(g);
            }
        }
    }

    public void removeGlobalItem(GroundItem g)
    {
        // -- Looping through the nearest regions of the ground item
        for (Region region : World.getInstance().getRegionManager().getSurroundingRegions(g.getPos()))
        {
            // -- Looping through all the players near the ground item
            for (Entity entity : region.getMobs())
            {

                if (entity == null || entity.isNpc())
                    continue;

                // Sending single packet to remove single ground item, if player
                // is within 60 tiles of the item and on same height.
                if (Misc.getDistance(g.getPos(), entity.getPosition()) <= 60 && entity.getPosition().getZ() == g.getPos().getZ())
                    ((Player) entity).getActionSender().removeGroundItem(g);
            }
        }
    }

    public void editGlobalGroundItemCount(GroundItem g)
    {
        // -- Looping through the nearest regions of the ground item
        for (Region region : World.getInstance().getRegionManager().getSurroundingRegions(g.getPos()))
        {
            // -- Looping through all the players near the ground item
            for (Entity entity : region.getMobs())
            {

                if (entity == null || entity.isNpc())
                    continue;

                // Sending single packet to remove single ground item, if player
                // is within 60 tiles of the item and on same height.
                if (Misc.getDistance(g.getPos(), entity.getPosition()) <= 60 && entity.getPosition().getZ() == g.getPos().getZ())
                    ((Player) entity).getActionSender().editGroundItemAmount(g);
            }
        }
    }

    /**
     * Loading ground items in 64x64 region.. when region has finished loading.
     */
    public void loadOnRegion(Player player)
    {

        // -- First reset client's whole loaded map (104x104) out of all ground
        // entities
        player.getActionSender().clear_region();

        for (Region region : World.getInstance().getRegionManager().getSurroundingRegions(player.getPosition()))
            player.getActionSender().sendEntitiesOnRegionLoad(region);
    }

    public void createGroundItem(Player player, String owner, Item item, Position pos, int time)
    {
        GroundItem g = new GroundItem(owner, item, new Position(pos.getX(), pos.getY(), pos.getZ()), false);
        g.setGlobal(player == null ? true : false);
        g.setTime(time);
        addGroundItem(g);
        if (player == null)
            showGlobalItem(g);
        else
            player.getActionSender().sendGroundItem(g);
    }

    public void tickRespawnableItems(GroundItem item, int time, boolean regionSpawn)
    {
        /*
         * item.setTime(time); if (item != null) { final GroundItem g = new
         * GroundItem(null, item.getItem(), item.getPos(), true);
         * g.setTime(item.getTime()); g.setGlobal(true); if (regionSpawn) {
         * addGroundItem(g); showGlobalItem(g); return; } World.submit(new
         * Task(item.getTime()) {
         * 
         * @Override protected void execute() { addGroundItem(g);
         * showGlobalItem(g); stop(); } }); }
         */
    }

    public void handle_rares(GroundItem item)
    {
        /*
         * item.setTime(150 + Misc.random(50)); if (item != null) { final
         * GroundItem g = new GroundItem(null, item.getItem(), item.getPos(),
         * true); g.setTime(item.getTime()); g.setGlobal(true);
         * addGroundItem(g); showGlobalItem(g); }
         */
    }

    @SuppressWarnings("unchecked")
    public void loadItemDefinitions() throws FileNotFoundException, IOException
    {
        List<ItemDefinition> list = (List<ItemDefinition>) XStreamController.getXStream().fromXML(new FileInputStream(Constants.CONFIG_DIRECTORY + "content/itemdefs.xml"));
        for (ItemDefinition definition : list)
        {
            definitions[definition.getId()] = definition;
        }
        logger.info("Loaded " + list.size() + " item definitions.");
    }

    public int getItemValue(int itemId, String type)
    {
        int value = 0;
        ItemDefinition definition = definitions[itemId];
        if (type.equals("lowalch"))
        {
            value = definition.getLowAlchValue();
        }
        else if (type.equals("highalch"))
        {
            value = definition.getHighAlchValue();
        }
        else if (type.equals("shop"))
        {
            value = definition.getShopValue();
        }
        return value;
    }

    public boolean isUntradeable(int id)
    {
        return Arrays.binarySearch(Equipment.UNTRADEABLE_ITEMS, id) > -1 || Arrays.binarySearch(Equipment.UNDROPPABLE_ITEMS, id) > -1;
    }

    public boolean getIsStackable(int itemId)
    {
        return definitions[itemId].isStackable();
    }

    public String getItemName(int itemId)
    {
        return definitions[itemId].getName();
    }

    public Collection<GroundItem> getGroundItems()
    {
        return groundItems;
    }

    public ItemDefinition[] getItemDefinitions()
    {
        return definitions;
    }

    public static ItemManager getInstance()
    {
        return instance;
    }

    // TODO BY AKZU: Have to look at this and usages.
    public class ItemDefinition
    {

        private short id;
        private String name;
        private int lowAlchValue;
        private int highAlchValue;
        private int shopValue;
        private boolean stackable;
        private boolean note;
        private double weight;
        private short noteId;
        private short parentId;

        public int getId()
        {
            return id;
        }

        public String getName()
        {
            return name;
        }

        public int getLowAlchValue()
        {
            if (lowAlchValue == 0)
            {
                lowAlchValue = 1;
            }
            return lowAlchValue;
        }

        public int getHighAlchValue()
        {
            if (highAlchValue == 0)
            {
                highAlchValue = 1;
            }
            return highAlchValue;
        }

        public int getShopValue()
        {
            if (shopValue == 0)
            {
                shopValue = 1;
            }
            return shopValue;
        }

        public boolean isStackable()
        {
            return stackable;
        }

        public boolean isNote()
        {
            return note;
        }

        public double getWeight()
        {
            return weight;
        }

        public int getNoteId()
        {
            return noteId;
        }

        public int getParentId()
        {
            return parentId;
        }
    }

}