package com.asgarniars.rs2.model;

import com.asgarniars.rs2.model.region.Region;
import com.asgarniars.rs2.util.Misc;

/**
 * Represents the position in x,y,z
 * 
 * @author blakeman8192
 */
public class Position
{

    private short x;
    private short y;
    private byte z;
    private Entity entity; // Only used for region system (npcs and players).

    private short[] lastStep;

    public short[] getLastStep()
    {
        return lastStep;
    }

    public void setLastStep(int x, int y, int z)
    {

        if (entity == null)
            return;

        if (lastStep == null)
            lastStep = new short[]
            { 0, 0, 0 };

        this.lastStep[0] = (short) x;
        this.lastStep[1] = (short) y;
        this.lastStep[2] = (short) z;
    }

    /**
     * Creates a new Position with the specified coordinates.
     * 
     * @param x the X coordinate
     * @param y the Y coordinate
     * @param z the Z coordinate
     */
    public Position(int x, int y, int z)
    {
        this.setX(x);
        this.setY(y);
        this.setZ(z);
        setLastStep(getX(), getY(), getZ());
    }

    /**
     * Creates a new Position with the specified coordinates for set entity.
     * 
     * @param x
     * @param y
     * @param z
     */
    public Position(int x, int y, int z, Entity e)
    {
        this.setX(x);
        this.setY(y);
        this.setZ(z);
        this.entity = e;
        setLastStep(getX(), getY(), getZ());
    }

    /**
     * Creates a new Position with the specified coordinates.
     * 
     * @param x the X coordinate
     * @param y the Y coordinate
     */
    public Position(int x, int y)
    {
        this.setX(x);
        this.setY(y);
        setLastStep(getX(), getY(), getZ());
    }

    @Override
    public String toString()
    {
        return "Position(" + x + ", " + y + ", " + z + ")";
    }

    @Override
    public boolean equals(Object other)
    {
        if (other instanceof Position)
        {
            Position p = (Position) other;
            return x == p.x && y == p.y && z == p.z;
        }
        return false;
    }

    /**
     * Sets this position as the other position. <b>Please use this method
     * instead of player.setPosition(other)</b> because of reference conflicts
     * (if the other position gets modified, so will the players).
     * 
     * @param other the other position
     */
    public void setAs(Position other)
    {
        this.x = other.x;
        this.y = other.y;
        this.z = other.z;
        setLastStep(getX(), getY(), getZ());
        addToRegion();
    }

    /**
     * Moves the position.
     * 
     * @param amountX the amount of X coordinates
     * @param amountY the amount of Y coordinates
     */
    public void move(int amountX, int amountY)
    {
        if (lastStep != null)
            setLastStep(getX(), getY(), getZ());
        setX(getX() + amountX);
        setY(getY() + amountY);
        addToRegion();
    }

    public boolean isPerpendicularTo(Position other)
    {
        Position delta = Misc.delta(this, other);
        return delta.getX() != delta.getY() && delta.getX() == 0 || delta.getY() == 0;
    }

    /**
     * Sets the X coordinate.
     * 
     * @param x the X coordinate
     */
    public void setX(int x)
    {
        this.x = (short) x;
        addToRegion();
    }

    /**
     * Gets the X coordinate.
     * 
     * @return the X coordinate
     */
    public int getX()
    {
        return x;
    }

    /**
     * Sets the Y coordinate.
     * 
     * @param y the Y coordinate
     */
    public void setY(int y)
    {
        this.y = (short) y;
        addToRegion();
    }

    /**
     * Gets the Y coordinate.
     * 
     * @return the Y coordinate
     */
    public int getY()
    {
        return y;
    }

    /**
     * Sets the Z coordinate.
     * 
     * @param z the Z coordinate
     */
    public void setZ(int z)
    {
        this.z = (byte) z;
        addToRegion();
    }

    /**
     * Gets the Z coordinate.
     * 
     * @return the Z coordinate.
     */
    public int getZ()
    {
        return z;
    }

    /**
     * Gets the X coordinate of the region containing this Position.
     * 
     * @return the region X coordinate
     */
    public int getRegionX()
    {
        return (x >> 3) - 6;
    }

    /**
     * Gets the Y coordinate of the region containing this Position.
     * 
     * @return the region Y coordinate
     */
    public int getRegionY()
    {
        return (y >> 3) - 6;
    }

    /**
     * Gets the local X coordinate relative to the base Position.
     * 
     * @param base the base Position
     * @return the local X coordinate
     */
    public int getLocalX(Position base)
    {
        return x - 8 * base.getRegionX();
    }

    /**
     * Gets the local Y coordinate relative to the base Position.
     * 
     * @param base the base Position.
     * @return the local Y coordinate.
     */
    public int getLocalY(Position base)
    {
        return y - 8 * base.getRegionY();
    }

    /**
     * Gets the local X coordinate relative to this Position.
     * 
     * @return the local X coordinate
     */
    public int getLocalX()
    {
        return getLocalX(this);
    }

    /**
     * Gets the local Y coordinate relative to this Position.
     * 
     * @return the local Y coordinate.=
     */
    public int getLocalY()
    {
        return getLocalY(this);
    }

    /**
     * Checks if this location is within range of another.
     * 
     * @param other The other location.
     * @return <code>true</code> if the location is in range, <code>false</code>
     *         if not.
     */
    public boolean isWithinDistance(Position other)
    {
        Position p = Misc.delta(this, other);
        return p.x <= 14 && p.x >= -15 && p.y <= 14 && p.y >= -15 && this.getZ() == other.getZ();
    }

    /**
     * This is used for region system. Don't modify.
     */
    public void addToRegion()
    {
        if (entity == null)
            return;

        Region newRegion = World.getInstance().getRegionManager().getRegionByLocation(this);
        if (newRegion != entity.getRegion())
        {
            if (entity.getRegion() != null)
            {
                entity.removeFromRegion(entity.getRegion());
            }
            entity.setRegion(newRegion);
            entity.addToRegion(entity.getRegion());
        }
    }
}
