package com.asgarniars.rs2.model.area;

import com.asgarniars.rs2.model.Position;

/**
 * Represents an area.
 * 
 * @author Joshua Barry
 */
public class Area
{

    private Position minimumPosition;
    private Position maximumPosition;

    /**
     * Gets the minimum position.
     * 
     * @return The minimum position.
     */
    public Position getMinimumPosition()
    {
        return minimumPosition;
    }

    /**
     * Gets the maximum position.
     * 
     * @return The maximum position.
     */
    public Position getMaximumPosition()
    {
        return maximumPosition;
    }

    /**
     * Checks if the specified position is within this area.
     * 
     * @param position The position.
     * @return True or false.
     */
    public boolean isInArea(Position position)
    {
        return position.getX() >= getMinimumPosition().getX() && position.getX() <= getMaximumPosition().getX() && position.getY() >= getMinimumPosition().getY() && position.getY() <= getMaximumPosition().getY() && position.getZ() == getMaximumPosition().getZ();
    }

    /**
     * Constructs a new area.
     * 
     * @param minimumPosition The minimum position.
     * @param maximumPosition The maximum position.
     */
    public Area(Position minimumPosition, Position maximumPosition)
    {
        this.minimumPosition = minimumPosition;
        this.maximumPosition = maximumPosition;
    }
}
