package com.asgarniars.rs2.model;

import com.asgarniars.rs2.model.players.Player;

/**
 * A camera used for cutscenes.
 * 
 * @author Rait
 * 
 */
public class Camera
{

    /**
     * The x-position of this camera.
     */
    private int x;

    /**
     * The y-position of this camera.
     */
    private int y;

    /**
     * The height of this camera.
     */
    private int height;

    /**
     * The velocity to move the camera at.
     */
    private int velocity;

    /**
     * The angle of this camera.
     */
    private int angle;

    /**
     * The player who this camera is shown to.
     */
    private Player player;

    /**
     * Construct a camera.
     * 
     * @param player The player.
     */
    public Camera(Player player)
    {
        this.player = player;
        this.x = player.getPosition().getX();
        this.y = player.getPosition().getY();
        this.height = 100;
    }

    /**
     * Set the rotation (the angle) of the camera.
     * 
     * @param angle The angle to set to.
     */
    public void rotate(int angle)
    {
        this.angle = angle;
    }

    /**
     * Moves the camera.
     * 
     * @param x The target x-position.
     * @param y The target y-position.
     * @param velocity The velocity to move at.
     */
    public void move(int x, int y, int velocity)
    {
        this.x = x;
        this.y = y;
        this.velocity = velocity;
    }

    /**
     * Ascend in height; increase the height level.
     * 
     * @param height The amount of levels to increase by.
     */
    public void ascend(int height)
    {
        this.height += height;
    }

    /**
     * Descend in height; decrease the height level.
     * 
     * @param height THe amount of levels to decrease by.
     */
    public void descend(int height)
    {
        this.height -= height;
    }

    /**
     * Updates the camera itself.
     */
    public void update()
    {
        player.getActionSender().sendCameraUpdate(x, y, height, velocity, angle);
    }

    /**
     * Gets the x-position of this camera.
     * 
     * @return The camera's x-position.
     */
    public int getX()
    {
        return x;
    }

    /**
     * Gets the y-position of this camera.
     * 
     * @return The camera's y-position.
     */
    public int getY()
    {
        return y;
    }

    /**
     * Gets the height of this camera.
     * 
     * @return The camera's height.
     */
    public int getZ()
    {
        return height;
    }

    /**
     * Gets the angle of this camera.
     * 
     * @return The camera's angle.
     */
    public int getAngle()
    {
        return angle;
    }

    /**
     * Gets the player whose this camera belongs to.
     * 
     * @return The owning player.
     */
    public Player getPlayer()
    {
        return player;
    }

}
