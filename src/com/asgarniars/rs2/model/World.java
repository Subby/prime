package com.asgarniars.rs2.model;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.Server;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.npcs.NpcUpdating;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.model.players.PlayerUpdating;
import com.asgarniars.rs2.model.region.RegionManager;
import com.asgarniars.rs2.task.Task;

/**
 * Handles all logged in players.
 * 
 * @author blakeman8192
 */
public class World
{

    /**
     * World instance.
     */
    private static final World world = new World();

    /** All registered players. */
    private static Player[] players = new Player[Constants.MAX_PLAYERS];

    /** All registered NPCs. */
    private static Npc[] npcs = new Npc[Constants.MAX_NPCS];

    /**
     * The region manager.
     */
    private RegionManager regionManager = new RegionManager();

    /**
     * The permanent attributes map. Items set here are only removed when told
     * to.
     */
    private Map<String, Object> attributes = new HashMap<String, Object>();

    public static void submit(final Task task)
    {
        Server.getScheduler().submit(task);
    }

    private static ExecutorService threadPool;

    static
    {
        int cpus = Runtime.getRuntime().availableProcessors();
        threadPool = Executors.newFixedThreadPool(cpus);
    }

    /**
     * Performs the processing of all players.
     * 
     * @throws Exception
     */
    public static void process() throws Exception
    {
        // Perform any logic processing for players.
        for (int i = 0; i < players.length; i++)
        {
            Player player = players[i];
            if (player == null)
            {
                continue;
            }
            try
            {
                player.process();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                player.disconnect();
            }
        }

        // Perform any logic processing for NPCs.
        for (int i = 0; i < npcs.length; i++)
        {
            Npc npc = npcs[i];
            if (npc == null)
            {
                continue;
            }
            try
            {
                npc.process();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                unregister(npc);
            }
        }

        // Update all players.
        final CountDownLatch latch = new CountDownLatch(playerAmount());
        for (int i = 0; i < players.length; i++)
        {
            final Player player = players[i];
            if (player == null)
            {
                continue;
            }
            threadPool.execute(new Runnable()
            {

                @Override
                public void run()
                {
                    synchronized (player)
                    {
                        try
                        {
                            PlayerUpdating.update(player);
                            NpcUpdating.update(player);
                        }
                        catch (Exception ex)
                        {
                            ex.printStackTrace();
                            player.disconnect();
                        }
                        finally
                        {
                            latch.countDown();
                        }
                    }
                }
            });
        }
        try
        {
            latch.await();
        }
        catch (InterruptedException ex)
        {
            ex.printStackTrace();
        }

        // Reset all players after cycle.
        for (int i = 0; i < players.length; i++)
        {
            Player player = players[i];
            if (player == null)
            {
                continue;
            }
            try
            {
                player.reset();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                player.disconnect();
            }
        }

        // Reset all npcs after cycle.
        for (int i = 0; i < npcs.length; i++)
        {
            Npc npc = npcs[i];
            if (npc == null)
            {
                continue;
            }
            try
            {
                npc.reset();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                unregister(npc);
            }
        }
        ItemManager.getInstance().tick();
    }

    /**
     * Registers a player for processing.
     * 
     * @param player the player
     */
    public static void register(Player player)
    {
        for (int i = 1; i < players.length; i++)
        {
            if (players[i] == null)
            {
                players[i] = player;
                player.setIndex(i);
                return;
            }
        }
        throw new IllegalStateException("Server is full!");
    }

    /**
     * Registers an NPC for processing.
     * 
     * @param npc the npc
     */
    public static void register(Npc npc)
    {
        for (int i = 1; i < npcs.length; i++)
        {
            if (npcs[i] == null)
            {
                npcs[i] = npc;
                npc.setIndex(i);
                npc.actions_after_spawn();
                return;
            }
        }
        throw new IllegalStateException("Server is full!");
    }

    /**
     * Unregisters a player from processing.
     * 
     * @param player the player
     */
    public static void unregister(Player player)
    {
        if (player.getIndex() == -1 || (player.getIndex() != -1 && players[player.getIndex()] == null))
            return;

        players[player.getIndex()].removeFromRegion(players[player.getIndex()].getRegion());
        players[player.getIndex()] = null;
    }

    /**
     * Unregisters an NPC from processing.
     * 
     * @param npc the npc
     */
    public static void unregister(Npc npc)
    {
        if (npc.getIndex() == -1 || (npc.getIndex() != -1 && npcs[npc.getIndex()] == null))
            return;

        npcs[npc.getIndex()].removeFromRegion(npcs[npc.getIndex()].getRegion());
        npcs[npc.getIndex()] = null;
    }

    /**
     * Gets the amount of players that are online.
     * 
     * @return the amount of online players
     */
    public static int playerAmount()
    {
        int amount = 0;
        for (int i = 1; i < players.length; i++)
        {
            if (players[i] != null)
            {
                amount++;
            }
        }
        return amount;
    }

    /**
     * Gets the amount of NPCs that are online.
     * 
     * @return the amount of online NPCs
     */
    public static int npcAmount()
    {
        int amount = 0;
        for (int i = 1; i < npcs.length; i++)
        {
            if (npcs[i] != null)
            {
                amount++;
            }
        }
        return amount;
    }

    public static Player getPlayerByName(String name)
    {
        for (Player player : players)
        {
            if (player == null)
                continue;
            if (player.getUsername().toLowerCase().equalsIgnoreCase(name))
            {
                return player;
            }
        }
        return null;
    }

    /**
     * 
     * @param position
     * @param offsetX
     * @param offsetY
     * @param projectileId
     * @param startHeight
     * @param endHeight
     * @param speed
     * @param delay
     * @param curve
     * @param radius
     * @param angle
     * @param lockon
     */
    public static void sendProjectile(Position position, int offsetX, int offsetY, int projectileId, int startHeight, int endHeight, int speed, int delay, int curve, int radius, int angle, int lockon)
    {
        for (Player player : players)
        {
            if (player == null)
                continue;
            if (position.isWithinDistance(player.getPosition()))
            {
                player.getActionSender().sendProjectile(position, offsetX, offsetY, projectileId, startHeight, endHeight, speed, lockon, delay, curve, radius, angle);
            }
        }
    }

    /**
     * Gets all registered players.
     * 
     * @return the players
     */
    public static Player[] getPlayers()
    {
        return players;
    }

    /**
     * Use this rather than getPlayers() for searching single player so no need
     * for many checks if client sends wrong index number for player so it wont
     * crash server
     * 
     * @param index_number The player's getIndex()
     * @return
     */
    public static Player getPlayer(int index_number)
    {
        if (index_number > 0 && index_number <= Constants.MAX_PLAYERS)
            return players[index_number];
        return null;
    }

    /**
     * Gets all registered NPCs.
     * 
     * @return the npcs
     */
    public static Npc[] getNpcs()
    {
        return npcs;
    }

    /**
     * Use this rather than getNpcs() for searching single npc so no need for
     * many checks if client sends wrong index number for npc so it wont crash
     * server
     * 
     * @param index_number The npc's getIndex()
     * @return
     */
    public static Npc getNpc(int index_number)
    {
        if (index_number > 0 && index_number <= Constants.MAX_NPCS)
            return npcs[index_number];
        return null;
    }

    /**
     * Gets the world instance.
     * 
     * @return The world instance.
     */
    public static World getInstance()
    {
        return world;
    }

    public Map<String, Object> getAttributes()
    {
        return attributes;
    }

    /**
     * Gets an attribute.<br />
     * WARNING: unchecked cast, be careful!
     * 
     * @param <T> The type of the value.
     * @param key The key.
     * @return The value.
     */
    @SuppressWarnings("unchecked")
    public <T> T getAttribute(String key)
    {
        return (T) attributes.get(key);
    }

    /**
     * Sets an attribute.<br />
     * WARNING: unchecked cast, be careful!
     * 
     * @param <T> The type of the value.
     * @param key The key.
     * @param value The value.
     * @return The old value.
     */
    @SuppressWarnings("unchecked")
    public <T> T setAttribute(String key, T value)
    {
        return (T) attributes.put(key, value);
    }

    /**
     * Removes an attribute.<br />
     * WARNING: unchecked cast, be careful!
     * 
     * @param <T> The type of the value.
     * @param key The key.
     * @return The old value.
     */
    @SuppressWarnings("unchecked")
    public <T> T removeAttribute(String key)
    {
        return (T) attributes.remove(key);
    }

    public void initAttributes()
    {
        // -- Init the duel arena players count
        setAttribute("duel_empty_a", 0);
        setAttribute("duel_empty_b", 0);
        setAttribute("duel_empty_c", 0);
        setAttribute("duel_obstacle_a", 0);
        setAttribute("duel_obstacle_b", 0);
        setAttribute("duel_obstacle_c", 0);

        Deque<String> last50_duels = new ArrayDeque<String>(50);

        for (int k = 0; k < 50; k++)
            last50_duels.add("");

        setAttribute("last50_duels", last50_duels);
    }

    /**
     * Gets the region manager.
     * 
     * @return The region manager.
     */
    public RegionManager getRegionManager()
    {
        return regionManager;
    }
}