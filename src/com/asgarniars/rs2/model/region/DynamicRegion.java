package com.asgarniars.rs2.model.region;

import com.asgarniars.rs2.model.Palette;
import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.Palette.PaletteTile;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.util.clip.RSObject;
import com.asgarniars.rs2.util.clip.RegionClipping;

/**
 * Represents a dynamic region, though it's always created at 4000, 4000 central
 * coords to make life easy (Except poh for say 5000, 5000).
 * 
 * 13 by 13 by 4 maximum allowed size
 * 
 * 1 Chunk is 8x8 tiles, chunk is always centered at 6,6
 * 
 * Chunk positions: 0,0 is at most bottom left, 13,13 is most top right
 * 
 * Warning: only use chunks from 2,2 towards till 11,11 as client doesn't
 * support more
 * 
 * @author AkZu
 * 
 */
public class DynamicRegion
{

    private static DynamicRegion instance;

    public static DynamicRegion getInstance()
    {
        if (instance == null)
            instance = new DynamicRegion();
        return instance;
    }

    /**
     * Creates normal instanced area that reflects back to normal game maps on
     * pos 4000, 4000 (NO POH HERE)
     * 
     * @param player
     * @param palette
     */
    public void create(Player player, Palette palette)
    {
        // -- Initializing the clips for player
        int[][][] clips = new int[4][64][64];
        player.setAttribute("2nd_map_region_clip", clips);

        // -- Initializing the objects for player
        RSObject[][][] rsObjects = new RSObject[4][64][64];
        player.setAttribute("2nd_map_region_objects", rsObjects);

        for (int z = 0; z < 4; z++)
        {
            for (int x = 0; x < 13; x++)
            {
                for (int y = 0; y < 13; y++)
                {
                    PaletteTile tile = palette.getTile(x, y, z);
                    for (int tileX = 0; tileX < 8; tileX++)
                    {
                        for (int tileY = 0; tileY < 8; tileY++)
                        {

                            if (tile != null)
                            {

                                int xxx = (3952 + (x * 8) + rotateTerrainBlockX(tileX, tileY, 1, 1, tile.getRotation()));
                                int yyy = (3952 + (y * 8) + rotateTerrainBlockY(tileX, tileY, 1, 1, tile.getRotation()));

                                int xxx2 = (tile.getX() * 8) + tileX;
                                int yyy2 = (tile.getY() * 8) + tileY;

                                // -- Populate the players clipping data with
                                // floor clipping data (rotated correctly)
                                // RegionClipping.addClipping(player, xxx, yyy,
                                // tile.getZ(),
                                // RegionClipping.getClipping(null, xxx2, yyy2,
                                // tile.getZ(), true));

                                // RSObject object_found =
                                // RegionClipping.getRSObject(null, xxx2, yyy2,
                                // tile.getZ());

                                // -- If object on tile is null, don't add
                                // it and don't add clipping data for that
                                // object, just continue with the loop
                                // if (object_found == null)
                                // continue;

                                xxx = (3952 + (x * 8) + rotateTerrainBlockX(tileX, tileY, 1, 1, tile.getRotation()));
                                yyy = (3952 + (y * 8) + rotateTerrainBlockY(tileX, tileY, 1, 1, tile.getRotation()));

                                RegionClipping.addClipping(player, xxx, yyy, tile.getZ(), RegionClipping.getClipping(null, xxx2, yyy2, tile.getZ()));

                                // -- Populate the players objects/clipping
                                // data
                                // RegionClipping.addObject(player,
                                // object_found.getId(), xxx, yyy, tile.getZ(),
                                // object_found.getType(),
                                // object_found.getFace());

                            }
                            else
                            {
                                // Populate the surroundings (todo: only
                                // surroundings.) of the map to be 100%
                                // NON_WALKABLE
                                RegionClipping.addClipping(player, (3952 + (x * 8) + tileX), (3952 + (y * 8) + tileY), z, 256);
                            }
                        }
                    }
                }
            }
        }
        player.teleport(new Position(4000, 4000, 0));
        player.setAttribute("2nd_map_region_mode", 0);
        player.getActionSender().sendConstructMapRegion(palette);
    }

    /**
     * 
     * @param x
     * @param y
     * @param width
     * @param height
     * @param rot
     * @return
     */
    public static int rotateTerrainBlockX(int x, int y, int width, int height, int rot)
    {
        if (rot == 0)
            return x;
        if (rot == 1)
            return y;
        if (rot == 2)
            return 7 - x - (height - 1);
        else
            return 7 - y - (width - 1);
    }

    /**
     * 
     * @param x
     * @param y
     * @param width
     * @param height
     * @param rot
     * @return
     */
    public static int rotateTerrainBlockY(int x, int y, int width, int height, int rot)
    {
        if (rot == 0)
            return y;
        if (rot == 1)
            return 7 - x - (height - 1);
        if (rot == 2)
            return 7 - y - (width - 1);
        else
            return x;
    }
}
