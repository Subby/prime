package com.asgarniars.rs2.model.region;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.players.GroundItem;

/**
 * Represents a single region.
 * 
 * @author Graham Edgecombe
 * 
 */
public class Region
{

    /**
     * The region coordinates.
     */
    private RegionCoordinates coordinate;

    /**
     * A list of mobs in this region.
     */
    private List<Entity> mobs = new LinkedList<Entity>();

    /**
     * A list of objects in this region.
     */
    // private List<GameObject> objects = new LinkedList<GameObject>();

    /**
     * A list of ground items in this region.
     */
    // private List<GroundItem> groundItems = new LinkedList<GroundItem>();

    private Tile[][][] tiles = new Tile[4][32][32];

    /**
     * A list of doors in this region.
     */
    // private List<Door> doors = new ArrayList<Door>();

    /**
     * Creates a region.
     * 
     * @param coordinate The coordinate.
     */
    public Region(RegionCoordinates coordinate)
    {
        this.coordinate = coordinate;
    }

    /**
     * Adds a new door.
     * 
     * @param door The door to add.
     */
    /*
     * public void addDoor(Door door) { doors.add(door); }
     */

    /**
     * Adds a new mob.
     * 
     * @param npc The mob to add.
     */
    public void addMob(Entity mob)
    {
        synchronized (this)
        {
            mobs.add(mob);
        }
    }

    /**
     * Adds a new obj.
     * 
     * @param obj The obj to add.
     */
    /*
     * public void addObject(GameObject obj) { objects.add(obj); }
     */

    /*
     * public Door doorForLocation(Location location, int id) { for (Door door :
     * doors) { if (door.getObject().getLocation().equals(location) &&
     * door.getObject().getId() == id ||
     * door.getReplacement().getLocation().equals(location) &&
     * door.getReplacement().getId() == id) { return door; } } return null; }
     */

    /**
     * Gets the region coordinates.
     * 
     * @return The region coordinates.
     */
    public RegionCoordinates getCoordinates()
    {
        return coordinate;
    }

    /*
     * public GameObject getGameObject(Location location, int id) { for (Region
     * r : getSurroundingRegions()) { for (GameObject obj : r.getGameObjects())
     * { if (obj.getLocation().equals(location) && obj.getId() == id) { return
     * obj; } } } return null; }
     */

    /**
     * Gets the list of objects.
     * 
     * @return The list of objects.
     */
    /*
     * public Collection<GameObject> getGameObjects() { return objects; }
     */

    /**
     * @return the groundItems
     */

    public List<GroundItem> getGroundItems()
    {
        List<GroundItem> groundItems = new LinkedList<GroundItem>();
        for (int z = 0; z < 4; z++)
            for (int x = 0; x < 32; x++)
                for (int y = 0; y < 32; y++)
                    if (tiles[z][x][y] != null && tiles[z][x][y].getGroundItems().size() > 0)
                        groundItems.addAll(tiles[z][x][y].getGroundItems());
        return groundItems;
    }

    /**
     * Gets the list of mobs.
     * 
     * @return The list of mobs.
     */
    public Collection<Entity> getMobs()
    {
        synchronized (this)
        {
            return Collections.unmodifiableCollection(new LinkedList<Entity>(mobs));
        }
    }

    /**
     * Gets the regions surrounding a location.
     * 
     * @return The regions surrounding the location.
     */
    public Region[] getSurroundingRegions()
    {
        Region[] surrounding = new Region[9];
        surrounding[0] = this;
        surrounding[1] = World.getInstance().getRegionManager().getRegion(this.getCoordinates().getX() - 1, this.getCoordinates().getY() - 1);
        surrounding[2] = World.getInstance().getRegionManager().getRegion(this.getCoordinates().getX() + 1, this.getCoordinates().getY() + 1);
        surrounding[3] = World.getInstance().getRegionManager().getRegion(this.getCoordinates().getX() - 1, this.getCoordinates().getY());
        surrounding[4] = World.getInstance().getRegionManager().getRegion(this.getCoordinates().getX(), this.getCoordinates().getY() - 1);
        surrounding[5] = World.getInstance().getRegionManager().getRegion(this.getCoordinates().getX() + 1, this.getCoordinates().getY());
        surrounding[6] = World.getInstance().getRegionManager().getRegion(this.getCoordinates().getX(), this.getCoordinates().getY() + 1);
        surrounding[7] = World.getInstance().getRegionManager().getRegion(this.getCoordinates().getX() - 1, this.getCoordinates().getY() + 1);
        surrounding[8] = World.getInstance().getRegionManager().getRegion(this.getCoordinates().getX() + 1, this.getCoordinates().getY() - 1);
        return surrounding;
    }

    /**
     * Removes an old mob.
     * 
     * @param mob The mob to remove.
     */
    public void removeMob(Entity mob)
    {
        synchronized (this)
        {
            mobs.remove(mob);
        }
    }

    /**
     * Removes an old obj.
     * 
     * @param obj The obj to remove.
     */
    /*
     * public void removeObject(GameObject obj) { objects.remove(obj); }
     */

    @Override
    public String toString()
    {
        return "[Region] [" + coordinate.getX() + ":" + coordinate.getY() + "]";
    }

    public Tile[][][] getTiles()
    {
        return tiles;
    }

    public Tile getTile(Position pos)
    {
        // -- If the tile doesn't exist, create it!
        if (tiles[pos.getZ()][pos.getX() - (coordinate.getX() * 32)][pos.getY() - (coordinate.getY() * 32)] == null)
            tiles[pos.getZ()][pos.getX() - (coordinate.getX() * 32)][pos.getY() - (coordinate.getY() * 32)] = new Tile();

        return tiles[pos.getZ()][pos.getX() - (coordinate.getX() * 32)][pos.getY() - (coordinate.getY() * 32)];
    }

}
