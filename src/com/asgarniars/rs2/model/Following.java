package com.asgarniars.rs2.model;

import com.asgarniars.rs2.content.combat.Combat;
import com.asgarniars.rs2.model.Entity.AttackTypes;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.util.Misc;
import com.asgarniars.rs2.util.clip.PathFinder;

/**
 * By Mikey` of Rune-Server and modified by AkZu & Jacob Scu11
 * 
 * ### NEEDS LITTLE TWEAKING ###
 */
public class Following
{

    private static final int NORTH = 0, EAST = 1, SOUTH = 2, WEST = 3;

    private static Following instance = null;

    public static Following getInstance()
    {
        if (instance == null)
        {
            instance = new Following();
        }
        return instance;
    }

    /**
     * An entity following another entity.
     */
    public void followEntity(Entity follower)
    {

        Entity leader = follower.getFollowingEntity();

        @SuppressWarnings("unused")
		int srcX = follower.getPosition().getX();
        @SuppressWarnings("unused")
		int srcY = follower.getPosition().getY();
        int dstX = leader.getPosition().getX();
        int dstY = leader.getPosition().getY();
        @SuppressWarnings("unused")
		int z = follower.getPosition().getZ();

        // -- If the leader (we're following) is player AND has logged out,
        // reset following.
        if (leader.isPlayer())
        {
            if (!((Player) leader).isLoggedIn())
            {
                Combat.getInstance().resetCombat(follower, true);
                return;
            }
        }

        if ((!leader.getPosition().isWithinDistance(follower.getPosition()) && !Combat.isInDiagonalBlock(follower, leader)) || leader.getAttribute("isDead") != null)
        {
            Combat.getInstance().resetCombat(follower, true);
            return;
        }

        // -- Face the entity
        follower.getUpdateFlags().faceEntity(leader.isPlayer() ? leader.getIndex() + 32768 : leader.getIndex());

        // -- If we're frozen, just return.
        if (follower.getAttribute("isFrozen") != null || follower.getAttribute("isStunned") != null)
            return;

        if (follower.isInstigatingAttack())
        {
            if (Misc.getDistance(follower.getPosition(), leader.getPosition()) == 0)
            {
                PathFinder.getSingleton().generateRandomMovement(follower);
                return;
            }
        }

        if (follower.getTarget() != null && Combat.getInstance().withinRange(follower, leader) && !Combat.isInDiagonalBlock(follower, leader))
        {
            follower.getMovementHandler().reset();
            return;
        }

        if (follower.isPlayer())
        {

            if (follower.isInstigatingAttack() && follower.getAttackType() == AttackTypes.MELEE)
            {
                switch (Misc.direction(dstX - leader.getPosition().getLastStep()[0], dstY - leader.getPosition().getLastStep()[1]))
                {
                case Misc.NORTH_WEST:
                    PathFinder.getSingleton().findRoute(follower, dstX, dstY - 1, true, 1, 1);
                    break;
                case Misc.NORTH:
                    PathFinder.getSingleton().findRoute(follower, dstX, dstY - 1, true, 1, 1);
                    break;
                case Misc.NORTH_EAST:
                    PathFinder.getSingleton().findRoute(follower, dstX, dstY - 1, true, 1, 1);
                    break;
                case Misc.WEST:
                    PathFinder.getSingleton().findRoute(follower, dstX + 1, dstY, true, 1, 1);
                    break;
                case Misc.EAST:
                    PathFinder.getSingleton().findRoute(follower, dstX - 1, dstY, true, 1, 1);
                    break;
                case Misc.SOUTH_WEST:
                    PathFinder.getSingleton().findRoute(follower, dstX, dstY + 1, true, 1, 1);
                    break;
                case Misc.SOUTH:
                    PathFinder.getSingleton().findRoute(follower, dstX, dstY + 1, true, 1, 1);
                    break;
                case Misc.SOUTH_EAST:
                    PathFinder.getSingleton().findRoute(follower, dstX, dstY + 1, true, 1, 1);
                    break;
                }
            }
            else
                PathFinder.getSingleton().findRoute(follower, leader.getPosition().getLastStep()[0], leader.getPosition().getLastStep()[1], true, Combat.getInstance().getDistanceForCombatType(follower), Combat.getInstance().getDistanceForCombatType(follower));

        }
        else if (follower.isNpc())
        {

            PathFinder.getSingleton().findRoute(follower, leader.getPosition().getLastStep()[0], leader.getPosition().getLastStep()[1], true, Combat.getInstance().getDistanceForCombatType(follower), Combat.getInstance().getDistanceForCombatType(follower));

            /*
             * int srcX = follower.getPosition().getX(); int srcY =
             * follower.getPosition().getY(); int dstX =
             * leader.getPosition().getX(); int dstY =
             * leader.getPosition().getY(); int z =
             * follower.getPosition().getZ(); int stepX = 0, stepY = 0; //WEST,
             * should check western on this tile and eastern on dest if (srcX >
             * dstX && !RegionClipping.blockedWest(follower, new Position(srcX,
             * srcY, z)) && !RegionClipping.blockedEast(follower, new
             * Position(dstX, dstY, z))) { stepX = -1; //EAST, should check
             * eastern on this tile and western on dest } else if (srcX < dstX
             * && !RegionClipping.blockedEast(follower, new Position(srcX, srcY,
             * z)) && !RegionClipping.blockedEast(follower, new Position(dstX,
             * dstY, z))) { stepX = 1; } //SOUTH, should check southern on this
             * and northern on dest if (srcY > dstY &&
             * !RegionClipping.blockedSouth(follower, new Position(srcX, srcY,
             * z))) stepY = -1; else if (srcY > dstY &&
             * !RegionClipping.blockedNorth(follower, new Position(dstX, dstY,
             * z))) { stepY = -1; //NORTH, should check northern on this and
             * southern on dest } else if (srcY < dstY &&
             * !RegionClipping.blockedNorth(follower, new Position(srcX, srcY,
             * z)) && !RegionClipping.blockedSouth(follower, new Position(dstX,
             * dstY, z))) { stepY = 1; } if (stepX != 0 || stepY != 0) {
             * follower.getMovementHandler().walk( new
             * Position(follower.getPosition().getX() + stepX,
             * follower.getPosition().getY() + stepY)); }
             */

            /*
             * Position[] npcTiles = TileControl.getHoveringTiles(follower);
             * Position npcLocation = follower.getPosition(); boolean[] canMove
             * = getMovementFlags(follower); int dir = -1; int distance =
             * TileControl.calculateDistance(follower, leader); if (distance >
             * 1) { for (Position tile : npcTiles) { if (tile.getX() ==
             * leader.getPosition().getX()) { canMove[EAST] = false;
             * canMove[WEST] = false; } else if (tile.getY() ==
             * leader.getPosition().getY()) { canMove[NORTH] = false;
             * canMove[SOUTH] = false; } } boolean[] blocked = new boolean[3];
             * if (canMove[NORTH] && canMove[EAST]) { for (Position tiles :
             * npcTiles) { if (RegionClipping.blockedNorth(follower, tiles)) {
             * blocked[0] = true; } if (RegionClipping.blockedEast(follower,
             * tiles)) { blocked[1] = true; } if
             * (RegionClipping.blockedNorthEast(follower, tiles)) { blocked[2] =
             * true; } } if (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX(), npcLocation.getY() + 1, 0)),
             * follower)) { blocked[0] = true; } if
             * (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX() + 1, npcLocation.getY(), 0)),
             * follower)) { blocked[1] = true; } if
             * (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX() + 1, npcLocation.getY() + 1, 0)),
             * follower)) { blocked[2] = true; } if (!blocked[2] && !blocked[0]
             * && !blocked[1]) { dir = 2; } else if (!blocked[0]) { dir = 0; }
             * else if (!blocked[1]) { dir = 4; } } else if (canMove[NORTH] &&
             * canMove[WEST]) { for (Position tiles : npcTiles) { if
             * (RegionClipping.blockedNorth(follower, tiles)) { blocked[0] =
             * true; } if (RegionClipping.blockedWest(follower, tiles)) {
             * blocked[1] = true; } if
             * (RegionClipping.blockedNorthWest(follower, tiles)) { blocked[2] =
             * true; } } if (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX(), npcLocation.getY() + 1, 0)),
             * follower)) { blocked[0] = true; } if
             * (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX() - 1, npcLocation.getY(), 0)),
             * follower)) { blocked[1] = true; } if
             * (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX() - 1, npcLocation.getY() + 1, 0)),
             * follower)) { blocked[2] = true; } if (!blocked[2] && !blocked[0]
             * && !blocked[1]) { dir = 14; } else if (!blocked[0]) { dir = 0; }
             * else if (!blocked[1]) { dir = 12; } } else if (canMove[SOUTH] &&
             * canMove[EAST]) { for (Position tiles : npcTiles) { if
             * (RegionClipping.blockedSouth(follower, tiles)) { blocked[0] =
             * true; } if (RegionClipping.blockedEast(follower, tiles)) {
             * blocked[1] = true; } if
             * (RegionClipping.blockedSouthEast(follower, tiles)) { blocked[2] =
             * true; } } if (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX(), npcLocation.getY() - 1, 0)),
             * follower)) { blocked[0] = true; } if
             * (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX() + 1, npcLocation.getY(), 0)),
             * follower)) { blocked[1] = true; } if
             * (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX() + 1, npcLocation.getY() - 1, 0)),
             * follower)) { blocked[2] = true; } if (!blocked[2] && !blocked[0]
             * && !blocked[1]) { dir = 6; } else if (!blocked[0]) { dir = 8; }
             * else if (!blocked[1]) { dir = 4; } } else if (canMove[SOUTH] &&
             * canMove[WEST]) { for (Position tiles : npcTiles) { if
             * (RegionClipping.blockedSouth(follower, tiles)) { blocked[0] =
             * true; } if (RegionClipping.blockedWest(follower, tiles)) {
             * blocked[1] = true; } if
             * (RegionClipping.blockedSouthWest(follower, tiles)) { blocked[2] =
             * true; } } if (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX(), npcLocation.getY() - 1, 0)),
             * follower)) { blocked[0] = true; } if
             * (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX() - 1, npcLocation.getY(), 0)),
             * follower)) { blocked[1] = true; } if
             * (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX() - 1, npcLocation.getY() - 1, 0)),
             * follower)) { blocked[2] = true; } if (!blocked[2] && !blocked[0]
             * && !blocked[1]) { dir = 10; } else if (!blocked[0]) { dir = 8; }
             * else if (!blocked[1]) { dir = 12; } } else if (canMove[NORTH]) {
             * dir = 0; for (Position tiles : npcTiles) { if
             * (RegionClipping.blockedNorth(follower, tiles)) { dir = -1; } } if
             * (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX(), npcLocation.getY() + 1, 0)),
             * follower)) { dir = -1; } } else if (canMove[EAST]) { dir = 4; for
             * (Position tiles : npcTiles) { if
             * (RegionClipping.blockedEast(follower, tiles)) { dir = -1; } } if
             * (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX() + 1, npcLocation.getY(), 0)),
             * follower)) { dir = -1; } } else if (canMove[SOUTH]) { dir = 8;
             * for (Position tiles : npcTiles) { if
             * (RegionClipping.blockedSouth(follower, tiles)) { dir = -1; } } if
             * (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX(), npcLocation.getY() - 1, 0)),
             * follower)) { dir = -1; } } else if (canMove[WEST]) { dir = 12;
             * for (Position tiles : npcTiles) { if
             * (RegionClipping.blockedWest(follower, tiles)) { dir = -1; } } if
             * (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX() - 1, npcLocation.getY(), 0)),
             * follower)) { dir = -1; } } } else if (distance == 0) { for (int i
             * = 0; i < canMove.length; i++) { canMove[i] = true; } for
             * (Position tiles : npcTiles) { if
             * (RegionClipping.blockedNorth(follower, tiles)) { canMove[NORTH] =
             * false; } if (RegionClipping.blockedEast(follower, tiles)) {
             * canMove[EAST] = false; } if
             * (RegionClipping.blockedSouth(follower, tiles)) { canMove[SOUTH] =
             * false; } if (RegionClipping.blockedWest(follower, tiles)) {
             * canMove[WEST] = false; } } int randomSelection = Misc.random(3);
             * if (canMove[randomSelection]) { dir = randomSelection * 4; switch
             * (dir) { case 0: if (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX(), npcLocation.getY() + 1, 0)),
             * follower)) { dir = -1; } break; case 4: if
             * (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX() + 1, npcLocation.getY(), 0)),
             * follower)) { dir = -1; } break; case 8: if
             * (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX(), npcLocation.getY() - 1, 0)),
             * follower)) { dir = -1; } break; case 12: if
             * (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX() - 1, npcLocation.getY(), 0)),
             * follower)) { dir = -1; } break; } } if (canMove[NORTH] && dir ==
             * -1) { dir = 0; if (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX(), npcLocation.getY() + 1, 0)),
             * follower)) { dir = -1; } } else if (canMove[EAST] && dir == -1) {
             * dir = 4; if (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX() + 1, npcLocation.getY(), 0)),
             * follower)) { dir = -1; } } else if (canMove[SOUTH] && dir == -1)
             * { dir = 8; if (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX(), npcLocation.getY() - 1, 0)),
             * follower)) { dir = -1; } } else if (canMove[WEST] && dir == -1) {
             * dir = 12; if (TileControl.getSingleton().locationOccupied(
             * TileControl.getHoveringTiles(follower, new
             * Position(npcLocation.getX() - 1, npcLocation.getY(), 0)),
             * follower)) { dir = -1; } } } if (dir == -1) return; dir >>= 1; if
             * (dir < 0) return;
             */
            /*
             * follower.getMovementHandler().walk( new
             * Position(follower.getPosition().getX() +
             * Misc.DIRECTION_DELTA_X[dir], follower.getPosition().getY() +
             * Misc.DIRECTION_DELTA_Y[dir]));
             */
        }
    }

    /**
     * @param mob The starting point
     * @return Possible movement flags to get from the mob to
     *         mob.getInteractingEntity()
     */
    public static boolean[] getMovementFlags(Entity mob)
    {
        boolean[] flags = new boolean[4];

        Entity target = mob.getFollowingEntity();

        if (target == null)
            return flags;

        if (mob.getPosition().getX() > target.getPosition().getX())
        {
            flags[WEST] = true;
        }
        else if (mob.getPosition().getX() < target.getPosition().getX())
        {
            flags[EAST] = true;
        }
        if (mob.getPosition().getY() > target.getPosition().getY())
        {
            flags[SOUTH] = true;
        }
        else if (mob.getPosition().getY() < target.getPosition().getY())
        {
            flags[NORTH] = true;
        }

        return flags;
    }
}
