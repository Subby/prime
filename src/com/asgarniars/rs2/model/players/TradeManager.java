package com.asgarniars.rs2.model.players;

import java.text.NumberFormat;
import java.util.Locale;

import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.model.players.Player.TradeStage;
import com.asgarniars.rs2.util.NameUtility;

/**
 * #########ADD A CHECK IF PLAYER IS BUSY (SHOPPING/TELEING/BANKING) #########
 * 
 * 
 * #### DONE #### Add support for xlogging and shit <-- #### DONE ###### Double
 * check everything, must get this bug free!
 * 
 * @author AkZu
 * 
 */

public class TradeManager
{

    public void refresh(Player player, Player otherPlayer)
    {
        player.getActionSender().sendUpdateItems(3322, player.getInventory().getItemContainer().toArray());
        otherPlayer.getActionSender().sendUpdateItems(3322, otherPlayer.getInventory().getItemContainer().toArray());

        player.getInventory().refresh();
        otherPlayer.getInventory().refresh();

        player.getActionSender().sendUpdateItems(3415, player.getTrade().toArray());
        player.getActionSender().sendUpdateItems(3416, otherPlayer.getTrade().toArray());

        otherPlayer.getActionSender().sendUpdateItems(3415, otherPlayer.getTrade().toArray());
        otherPlayer.getActionSender().sendUpdateItems(3416, player.getTrade().toArray());

        player.getActionSender().sendString("", 3431);
        otherPlayer.getActionSender().sendString("", 3431);

        player.getActionSender().sendString("@lre@Trading With: " + otherPlayer.getUsername(), 3417);
        player.getActionSender().sendString(otherPlayer.getUsername(), 8482);
        player.getActionSender().sendString("has " + otherPlayer.getInventory().getItemContainer().freeSlots() + " free", 8483);
        player.getActionSender().sendString("inventory slots", 8484);

        otherPlayer.getActionSender().sendString("@lre@Trading With: " + player.getUsername(), 3417);
        otherPlayer.getActionSender().sendString(player.getUsername(), 8482);
        otherPlayer.getActionSender().sendString("has " + player.getInventory().getItemContainer().freeSlots() + " free", 8483);
        otherPlayer.getActionSender().sendString("inventory slots", 8484);
    }

    public void handleTradeRequest(final Player player, final Player otherPlayer)
    {

        if (player == null || !player.isLoggedIn() || otherPlayer == null || !otherPlayer.isLoggedIn())
        {
            return;
        }

        if (otherPlayer.getIndex() == player.getIndex())
            return;

        if (otherPlayer.getTradeStage().equals(TradeStage.WAITING))
        {
            player.getActionSender().sendMessage("Sending trade offer...");
            otherPlayer.getActionSender().sendMessage(NameUtility.uppercaseFirstLetter(player.getUsername()) + ":tradereq:");
            player.setTradeStage(TradeStage.SEND_REQUEST);
        }
        else if (otherPlayer.getTradeStage().equals(TradeStage.SEND_REQUEST) && otherPlayer.getClickId() == player.getIndex())
        {
            player.setTradeStage(TradeStage.SEND_REQUEST_ACCEPT);
            otherPlayer.setTradeStage(TradeStage.SEND_REQUEST_ACCEPT);
            sendTrade(player, otherPlayer);
        }
        else
        {
            player.getActionSender().sendMessage("Sending trade offer...");
            otherPlayer.getActionSender().sendMessage(NameUtility.uppercaseFirstLetter(player.getUsername()) + ":tradereq:");
            player.setTradeStage(TradeStage.WAITING);
        }
    }

    private void sendTrade(Player player, Player otherPlayer)
    {
        refresh(player, otherPlayer);

        player.getActionSender().sendItfInvOverlay(3323, 3321);
        otherPlayer.getActionSender().sendItfInvOverlay(3323, 3321);
    }

    public void declineTrade(Player player)
    {

        if (!(player.getTradeStage().equals(TradeStage.ACCEPT) || player.getTradeStage().equals(TradeStage.SECOND_TRADE_WINDOW) || player.getTradeStage().equals(TradeStage.SEND_REQUEST_ACCEPT)))
            return;

        Player otherPlayer = World.getPlayer(player.getClickId());

        if (otherPlayer == null)
            return;

        if (otherPlayer.getClickId() != player.getIndex())
            return;

        if (otherPlayer.getTradeStage().equals(TradeStage.ACCEPT) || otherPlayer.getTradeStage().equals(TradeStage.SEND_REQUEST_ACCEPT) || otherPlayer.getTradeStage().equals(TradeStage.SECOND_TRADE_WINDOW))
        {
            otherPlayer.getActionSender().sendMessage("Other player has declined the trade.");
            player.getActionSender().sendMessage("You decline the trade.");
        }

        player.getActionSender().removeInterfaces();
        otherPlayer.getActionSender().removeInterfaces();

        player.setTradeStage(TradeStage.WAITING);
        otherPlayer.setTradeStage(TradeStage.WAITING);

        giveBackItems(player);

        player.setClickId(-1);
    }

    public void giveBackItems(Player player)
    {
        Player otherPlayer = World.getPlayer(player.getClickId());
        for (int i = 0; i < Inventory.SIZE; i++)
        {
            if (player.getTrade().get(i) != null)
            {
                Item item = player.getTrade().get(i);
                if (item != null)
                {
                    player.getTrade().remove(item);
                    player.getInventory().addItem(item);
                }
            }
        }
        for (int i = 0; i < Inventory.SIZE; i++)
        {
            if (otherPlayer.getTrade().get(i) != null)
            {
                Item item = otherPlayer.getTrade().get(i);
                if (item != null)
                {
                    otherPlayer.getTrade().remove(item);
                    otherPlayer.getInventory().addItem(item);
                }
            }
        }
    }

    public void offerItem(Player player, int slot, int tradeItem, int amount)
    {
        Player otherPlayer = World.getPlayer(player.getClickId());

        if (otherPlayer.getClickId() != player.getIndex())
            return;

        if (player.getTradeStage().equals(TradeStage.SECOND_TRADE_WINDOW))
        {
            return;
        }
        if (player == null || !player.isLoggedIn() || otherPlayer == null || !otherPlayer.isLoggedIn())
        {
            return;
        }
        if (tradeItem == -1)
        {
            return;
        }
        Item inv = player.getInventory().getItemContainer().get(slot);
        int invAmount = player.getInventory().getItemContainer().getCount(tradeItem);
        if (inv == null)
            return;
        if (inv.getId() != tradeItem)
        {
            return;
        }
        if (inv.getId() <= 0 || tradeItem <= 0 || inv.getCount() < 1 || amount < 1)
        {
            return;
        }

        if (ItemManager.getInstance().isUntradeable(tradeItem))
        {
            player.getActionSender().sendMessage("You can't trade that item.");
            return;
        }

        if (invAmount > amount)
        {
            invAmount = amount;
        }
        if (inv.getDefinition().isStackable())
        {
            player.getInventory().removeItemSlot(slot);
        }
        else
        {
            for (int i = 0; i < invAmount; i++)
            {
                if (invAmount == 1)
                    player.getInventory().removeItemSlot(slot);
                else
                    player.getInventory().removeItem(new Item(tradeItem, 1));
            }
        }
        int tradeAmount = player.getTrade().getCount(tradeItem);
        if (tradeAmount > 0 && inv.getDefinition().isStackable())
        {
            player.getTrade().set(player.getTrade().getSlotById(inv.getId()), new Item(tradeItem, tradeAmount + invAmount));
        }
        else
        {
            player.getTrade().add(new Item(inv.getId(), invAmount));
        }
        refresh(player, otherPlayer);
        player.setTradeStage(TradeStage.SEND_REQUEST_ACCEPT);
        otherPlayer.setTradeStage(TradeStage.SEND_REQUEST_ACCEPT);
    }

    public void removeTradeItem(Player player, int slot, int tradeItem, int amount)
    {
        Player otherPlayer = World.getPlayer(player.getClickId());

        if (otherPlayer.getClickId() != player.getIndex())
            return;

        if (player.getTradeStage().equals(TradeStage.SECOND_TRADE_WINDOW))
        {
            return;
        }
        if (player == null || !player.isLoggedIn() || otherPlayer == null || !otherPlayer.isLoggedIn())
        {
            return;
        }
        if (tradeItem == -1 || amount == 0)
        {
            return;
        }
        Item itemOnScreen = player.getTrade().get(slot);
        int itemOnScreenAmount = player.getTrade().getCount(tradeItem);
        if (itemOnScreen == null || itemOnScreen.getId() <= 0 || itemOnScreen.getId() != tradeItem)
        {
            return;
        }
        if (itemOnScreenAmount > amount)
        {
            itemOnScreenAmount = amount;
        }
        player.getInventory().addItem(new Item(itemOnScreen.getId(), itemOnScreenAmount));
        player.getTrade().remove(new Item(tradeItem, itemOnScreenAmount), slot);
        refresh(player, otherPlayer);
        player.setTradeStage(TradeStage.SEND_REQUEST_ACCEPT);
        otherPlayer.setTradeStage(TradeStage.SEND_REQUEST_ACCEPT);
    }

    public void acceptStageOne(Player player)
    {
        Player otherPlayer = World.getPlayer(player.getClickId());

        if (otherPlayer.getClickId() != player.getIndex())
            return;

        if (player == null || !player.isLoggedIn() || otherPlayer == null || !otherPlayer.isLoggedIn())
        {
            return;
        }

        if (otherPlayer.getInventory().getItemContainer().freeSlots() < player.getTrade().size())
        {
            player.getActionSender().sendMessage("Other player doesn't have enough inventory space for this trade.");
            return;
        }
        if (player.getInventory().getItemContainer().freeSlots() < otherPlayer.getTrade().size())
        {
            player.getActionSender().sendMessage("You don't have enough inventory space for this trade.");
            return;
        }

        player.setTradeStage(TradeStage.ACCEPT);

        if (!otherPlayer.getTradeStage().equals(TradeStage.ACCEPT))
        {
            player.getActionSender().sendString("Waiting for other player...", 3431);
            otherPlayer.getActionSender().sendString("Other player accepted.", 3431);
        }
        else
        {
            sendSecondScreen(player);
            sendSecondScreen(otherPlayer);
        }
    }

    public void acceptStageTwo(Player player)
    {
        if (!player.getTradeStage().equals(TradeStage.SECOND_TRADE_WINDOW))
        {
            return;
        }
        Player otherPlayer = World.getPlayer(player.getClickId());

        if (player == null || !player.isLoggedIn() || otherPlayer == null || !otherPlayer.isLoggedIn())
        {
            return;
        }

        if (otherPlayer.getClickId() != player.getIndex())
            return;

        player.setTradeStage(TradeStage.ACCEPT);

        if (!otherPlayer.getTradeStage().equals(TradeStage.ACCEPT))
        {
            player.getActionSender().sendString("@cya@Waiting for other player...", 3535);
            otherPlayer.getActionSender().sendString("@cya@Other player accepted.", 3535);
        }
        else
        {
            for (int i = 0; i < Inventory.SIZE; i++)
            {
                Item newItems = player.getTrade().get(i);
                if (newItems == null)
                {
                    continue;
                }
                player.getTrade().remove(newItems);
                otherPlayer.getInventory().addItem(newItems);
            }
            for (int i = 0; i < Inventory.SIZE; i++)
            {
                Item newItems = otherPlayer.getTrade().get(i);
                if (newItems == null)
                {
                    continue;
                }
                otherPlayer.getTrade().remove(newItems);
                player.getInventory().addItem(newItems);
            }

            player.setTradeStage(TradeStage.WAITING);
            otherPlayer.setTradeStage(TradeStage.WAITING);

            player.getActionSender().sendMessage("You accept the trade.");
            otherPlayer.getActionSender().sendMessage("You accept the trade.");

            player.getActionSender().removeInterfaces();
            otherPlayer.getActionSender().removeInterfaces();

        }
    }

    private void sendSecondScreen(Player player)
    {
        Player otherPlayer = World.getPlayer(player.getClickId());
        StringBuilder trade = new StringBuilder();
        boolean empty = true;

        for (int i = 0; i < Inventory.SIZE; i++)
        {
            Item item = player.getTrade().get(i);
            String prefix = "";
            Locale locale = Locale.US;
            String s = null;
            if (item != null)
            {
                empty = false;
                if (item.getCount() >= 1000 && item.getCount() < 10000000)
                {
                    s = NumberFormat.getNumberInstance(locale).format(item.getCount());
                    prefix = "@cya@" + NumberFormat.getNumberInstance(locale).format((item.getCount() / 1000)) + "K @whi@(" + s + ")";
                }
                else if (item.getCount() >= 10000000)
                {
                    s = NumberFormat.getNumberInstance(locale).format(item.getCount());
                    prefix = "@mon@" + (item.getCount() / 1000000) + "M (" + s + ")";
                }
                else
                {
                    s = NumberFormat.getNumberInstance(locale).format(item.getCount());
                    prefix = s;
                }
                trade.append(item.getDefinition().getName());
                if (item.getCount() > 1)
                {
                    trade.append(" x ");
                    trade.append(prefix);
                }
                trade.append("\\n");
            }
        }

        if (empty)
            trade.append("Absolutely nothing!");

        player.getActionSender().sendString(trade.toString(), 3557);
        trade = new StringBuilder();
        Locale locale = Locale.US;
        String s = null;
        empty = true;
        for (int i = 0; i < Inventory.SIZE; i++)
        {
            Item item = otherPlayer.getTrade().get(i);
            String prefix = "";
            if (item != null)
            {
                empty = false;
                if (item.getCount() >= 1000 && item.getCount() < 10000000)
                {
                    s = NumberFormat.getNumberInstance(locale).format(item.getCount());
                    prefix = "@cya@" + NumberFormat.getNumberInstance(locale).format((item.getCount() / 1000)) + "K @whi@(" + s + ")";
                }
                else if (item.getCount() >= 10000000)
                {
                    s = NumberFormat.getNumberInstance(locale).format(item.getCount());
                    prefix = "@mon@" + (item.getCount() / 1000000) + "M (" + s + ")";
                }
                else
                {
                    s = NumberFormat.getNumberInstance(locale).format(item.getCount());
                    prefix = s;
                }
                trade.append(item.getDefinition().getName());
                if (item.getCount() > 1)
                {
                    trade.append(" x ");
                    trade.append(prefix);
                }
                trade.append("\\n");
            }
        }

        if (empty)
            trade.append("Absolutely nothing!");

        player.getActionSender().sendString(trade.toString(), 3558);

        player.getActionSender().sendString("Trading with:\\n" + otherPlayer.getUsername(), 2422);
        otherPlayer.getActionSender().sendString("Trading with:\\n" + player.getUsername(), 2422);

        refresh(player, otherPlayer);

        player.getActionSender().sendItfInvOverlay(3443, 3213);
        otherPlayer.getActionSender().sendItfInvOverlay(3443, 3213);

        player.setTradeStage(TradeStage.SECOND_TRADE_WINDOW);
        otherPlayer.setTradeStage(TradeStage.SECOND_TRADE_WINDOW);

        player.getActionSender().sendString("Are you sure you want to accept this trade?", 3535);
        otherPlayer.getActionSender().sendString("Are you sure you want to accept this trade?", 3535);
    }

    public void handleDisconnect(Player player)
    {
        Player otherPlayer = World.getPlayer(player.getClickId());

        if (otherPlayer.getClickId() != player.getIndex())
            return;

        if (otherPlayer.getTradeStage().equals(TradeStage.ACCEPT) || otherPlayer.getTradeStage().equals(TradeStage.SEND_REQUEST_ACCEPT) || otherPlayer.getTradeStage().equals(TradeStage.SECOND_TRADE_WINDOW))
        {
            otherPlayer.getActionSender().sendMessage("Other player has declined the trade.");
            player.getActionSender().sendMessage("You decline the trade.");
        }

        player.getActionSender().removeInterfaces();
        otherPlayer.getActionSender().removeInterfaces();

        player.setTradeStage(TradeStage.WAITING);
        otherPlayer.setTradeStage(TradeStage.WAITING);

        for (int i = 0; i < Inventory.SIZE; i++)
        {
            if (player.getTrade().get(i) != null)
            {
                Item item = player.getTrade().get(i);
                if (item != null)
                {
                    player.getTrade().remove(item);
                    player.getInventory().getItemContainer().add(item);
                }
            }
        }
        for (int i = 0; i < Inventory.SIZE; i++)
        {
            if (otherPlayer.getTrade().get(i) != null)
            {
                Item item = otherPlayer.getTrade().get(i);
                if (item != null)
                {
                    otherPlayer.getTrade().remove(item);
                    otherPlayer.getInventory().getItemContainer().add(item);
                }
            }
        }
        player.getInventory().refresh();
        otherPlayer.getInventory().refresh();
        player.setClickId(-1);
    }
}
