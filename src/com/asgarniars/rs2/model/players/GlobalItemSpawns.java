package com.asgarniars.rs2.model.players;

/**
 * Handles loading for respawnable grounditems.
 * ### NEEDS SOME TWEAKING ###
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.io.XStreamController;
import com.asgarniars.rs2.model.Position;

public class GlobalItemSpawns
{

    private static GlobalItemSpawns instance = null;

    public static GlobalItemSpawns getInstance()
    {
        if (instance == null)
        {
            instance = new GlobalItemSpawns();
        }
        return instance;
    }

    private int id;
    private int amount;
    private int totalSpawns;
    private int time;
    private Position position;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public Position getPosition()
    {
        return position;
    }

    public void setPosition(Position pos)
    {
        this.position = pos;
    }

    public void setTotalSpawns(int count)
    {
        this.totalSpawns = count;
    }

    public int getTotalSpawns()
    {
        return totalSpawns;
    }

    public int getTime()
    {
        return time;
    }

    public void setTime(int time)
    {
        this.time = time;
    }

    public int getAmount()
    {
        return amount;
    }

    public void setAmount(int amount)
    {
        this.amount = amount;
    }

    public static class GlobalItemLoader
    {

        private static Map<Integer, GlobalItemSpawns> items;

        public static GlobalItemSpawns getItemSpawn(int index)
        {
            return items.get(index);
        }

        @SuppressWarnings("unchecked")
        public static void loadItemSpawns() throws FileNotFoundException
        {
            items = new HashMap<Integer, GlobalItemSpawns>();
            XStreamController.getInstance();

            int count = 0;

            List<GlobalItemSpawns> loaded = (ArrayList<GlobalItemSpawns>) XStreamController.getXStream().fromXML(new FileInputStream(Constants.CONFIG_DIRECTORY + "content/itemspawns.xml"));
            for (GlobalItemSpawns item : loaded)
            {
                items.put(count, item);
                GlobalItemSpawns.getInstance().setId(item.getId());
                GlobalItemSpawns.getInstance().setAmount(item.getAmount());
                GlobalItemSpawns.getInstance().setTime(item.getTime());
                GlobalItemSpawns.getInstance().setPosition(item.getPosition());
                count++;
                GlobalItemSpawns.getInstance().setTotalSpawns(count);
            }
        }
    }
}
