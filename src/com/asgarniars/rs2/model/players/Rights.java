package com.asgarniars.rs2.model.players;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public enum Rights
{

    PLAYER, // The standard player of the game.
    MODERATOR(""), // The player moderators.
    LOCAL_MODERATOR(""), // The server administrators.
    ADMINISTRATOR("Denver", "Dominic"); // The owners of the server.

    /* the members of the group */
    private String[] members;

    private Rights(String... members)
    {
        this.members = members;

    }

    /**
     * Gets the rights level based of the rank of order.
     * 
     * @param rank The rank in order of positions.
     * @return rights The rights for the member.
     */
    public static Rights forRank(int rank)
    {
        for (Rights rights : Rights.values())
            if (rank == rights.ordinal())
                return rights;
        /* return the standard rank if no other is found */
        return PLAYER;
    }

    public String[] getMembers()
    {
        return members;
    }

}
