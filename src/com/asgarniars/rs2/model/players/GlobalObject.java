package com.asgarniars.rs2.model.players;

import com.asgarniars.rs2.model.Position;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class GlobalObject
{

    private int id;
    private Position position;
    private int face;
    private int type;

    /**
     * 
     * @param id The ID of the object.
     * @param position The position it will be placed upon.
     * @param face The direction it should face.
     * @param type The object type. If unknown, 10 is a typical game object.
     */
    public GlobalObject(int id, Position position, int face, int type)
    {
        this.id = id;
        this.position = position;
        this.face = face;
        this.type = type;
    }

    /**
     * Sets a new object id.
     * 
     * @param id
     */
    public void setId(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    /**
     * Sets a new position.
     * 
     * @param position
     */
    public void setPosition(Position position)
    {
        this.position = position;
    }

    public Position getPosition()
    {
        return position;
    }

    /**
     * Sets a new face direction.
     * 
     * @param face
     */
    public void setFace(int face)
    {
        this.face = face;
    }

    public int getFace()
    {
        return face;
    }

    /**
     * Sets a new type.
     * 
     * @param type
     */
    public void setType(int type)
    {
        this.type = type;
    }

    public int getType()
    {
        return type;
    }
}