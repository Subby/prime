package com.asgarniars.rs2.model.players;

import java.util.logging.Logger;

import com.asgarniars.rs2.content.Climbable;
import com.asgarniars.rs2.content.Door;
import com.asgarniars.rs2.content.WildernessDitch;
import com.asgarniars.rs2.content.dialogue.ChatDialogue;
import com.asgarniars.rs2.content.dialogue.Dialogue;
import com.asgarniars.rs2.content.minigame.barrows.Barrows;
import com.asgarniars.rs2.content.minigame.impl.FightCaves;
import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.task.Task;
import com.asgarniars.rs2.util.Misc;
import com.asgarniars.rs2.util.ScriptManager;
import com.asgarniars.rs2.util.clip.ObjectDefinition;
import com.asgarniars.rs2.util.clip.RSObject;
import com.asgarniars.rs2.util.clip.RegionClipping;

/**
 * Actions that happen after the player walks within distance.
 * 
 * @author AkZu
 * @author Joshua Barry
 * 
 */
public class WalkToActions
{

    private static final Logger logger = Logger.getAnonymousLogger();

    private static Actions actions = Actions.OBJECT_FIRST_CLICK;

    public static void doActions(Player player, Object... objects)
    {
        switch (actions)
        {
        case OBJECT_FIRST_CLICK:
            logger.info("first object click");
            initiateFirstObjectResponce(player);
            break;
        case OBJECT_SECOND_CLICK:
            logger.info("second object click");
            doObjectSecondClick(player);
            break;
        case OBJECT_THIRD_CLICK:
            logger.info("third object click");
            doObjectThirdClick(player);
            break;
        case NPC_FIRST_CLICK:
            logger.info("first NPC click");
            invokeFirstNpcInteraction(player);
            break;
        case NPC_SECOND_CLICK:
            logger.info("second NPC click");
            doNpcSecondClick(player);
            break;
        case NPC_THIRD_CLICK:
            logger.info("third NPC click");
            doNpcThirdClick(player);
            break;
        case ITEM_ON_OBJECT:
            logger.info("item on object");
            doItemOnObject(player, (Item) objects[0]);
            break;
        }
    }

    public static void initiateFirstObjectResponce(final Player player)
    {
        final int id = player.getClickId();
        final int x = player.getClickX();
        final int y = player.getClickY();
        final ObjectDefinition def = ObjectDefinition.forId(id);

        player.getUpdateFlags().sendFaceToDirection(new Position(x, y, player.getPosition().getZ()));

        // TODO: IMPORTANT
        // 6774
        // barrow chest coords: 3552 9694
        // barrow chest real ID == 6774, what client sends == 10284 (because it
        // takes id somehow of the model??)

        final int orig_distance = Misc.getDistance(player.getPosition(), new Position(x, y));
        final RSObject object = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ());
        final Door door = new Door(object.getId(), object.getX(), object.getY(), object.getHeight(), object.getType(), object.getFace());

        player.setWalkToAction(new Task(1, true)
        {

            @Override
            protected void execute()
            {
                /*
                 * Swim the river ! Sneaky- waterfall
                 */
                if (id == 10283 && x == 2512 && y == 3475)
                {
                    if (!player.canWalk())
                    {
                        actions = null;
                        this.stop();
                        return;
                    }

                    if (player.getPosition() != new Position(2512, 3476, 0))
                        player.getMovementHandler().walk(new Position(2512, 3476, 0));

                    player.setAttribute("tempWalkAnim", 772);
                    player.setAttribute("tempRunAnim", 772);
                    player.setAppearanceUpdateRequired(true);
                    player.getActionSender().sendMessage("It looks like a long distance, but you swim out into the water.");
                    player.getMovementHandler().addToPath(new Position(2512, 3472, 0));

                    World.submit(new Task(4)
                    {

                        @Override
                        protected void execute()
                        {
                            player.removeAttribute("tempWalkAnim");
                            player.removeAttribute("tempRunAnim");
                            player.setAppearanceUpdateRequired(true);
                            player.getUpdateFlags().sendAnimation(765);
                            World.submit(new Task(2)
                            {

                                @Override
                                protected void execute()
                                {
                                    player.clipTeleport(new Position(2528, 3415, 0), 2);
                                    player.hit(2, 1, false);
                                    player.getActionSender().sendMessage("You are washed downstream but feel lucky to be alive.");

                                    this.stop();

                                }
                            });
                            this.stop();
                        }
                    });
                    // finalize the walk to action.
                    this.stop();
                    return;
                }
                else if (!Misc.objectExists(player, id, x, y, player.getPosition().getZ()))
                {
                    if (player.isDebugging())
                        player.getActionSender().sendMessage("non existant object");
                    actions = null;
                    this.stop();
                    return;
                }
                else if (id == 23271)
                {
                    if (WildernessDitch.handleDitch(player, x))
                    {
                        actions = null;
                        this.stop();
                        return;
                    }
                    else
                    {
                        return;
                    }
                    // -- Wait it to walk near the object..
                }
                else if (id == 1733)
                {
                    String action = def.actions[0].toLowerCase();
                    Position position = new Position(3057, 9777, 0);
                    if (action.contains("down"))
                        Climbable.climb(player, position, 1);
                    this.stop();
                    return;

                }
                else if (!player.getMovementHandler().walkToAction(new Position(x, y, player.getPosition().getZ()), getObjectSize(id)))
                {
                    return;
                }

                World.submit(new Task(orig_distance == 1 ? 1 : 2)
                {

                    @Override
                    protected void execute()
                    {
                        final String objName = (def.getName() == null ? "" : def.getName().toLowerCase());
                        player.getUpdateFlags().sendFaceToDirection(new Position(x, y, player.getPosition().getZ()));
                        switch (id)
                        {

                        case 9356:
                            Npc npc = new Npc(2617);
                            FightCaves caves = new FightCaves(player, npc);
                            caves.start();
                            break;

                        case 190:
                        case 1516:
                        case 1519:
                        case 1530:
                        case 1531:
                        case 1533:
                        case 1534:
                        case 1536:
                        case 1596:
                        case 1597:
                        case 1967:
                        case 1968:
                        case 2398:
                        case 2399:
                        case 9523:
                        case 11616:
                        case 11617:
                        case 11624:
                        case 11625:
                        case 11707:
                        case 11708:
                        case 11712:
                        case 11714:
                        case 11716:
                        case 11721:
                        case 12348:
                        case 12349:
                        case 12350:
                        case 13001:
                        case 15535:
                        case 15536:
                        case 24375:
                        case 24376:
                        case 24378:
                        case 24379:
                        case 24381:
                        case 24384:
                        case 23489:
                        case 26808:
                        case 26906:
                        case 26908:
                        case 26910:
                        case 26913:
                        case 26916:
                            door.open(player);
                            break;
                        case 10284:
                            RSObject chest = new RSObject(6775, object.getPosition(), object.getType(), object.getFace());
                            GlobalObjectHandler.createGlobalObject(player, chest);
                            Barrows.openChest(player);
                            break;
                        default:
                            String scriptName = "objectOptionOne_" + objName.replaceAll(" ", "_").replaceAll("'", "");
                            if (!ScriptManager.getSingleton().invokeWithFailTest(scriptName, player))
                            {
                                player.getActionSender().sendMessage("Nothing interesting happens.");
                            }
                            if (player.isDebugging())
                            {
                                logger.info("Attempted to call script " + scriptName);
                            }
                        }
                        this.stop();
                        actions = null;
                    }
                });
                this.stop();
            }
        });
    }

    /**
     * 
     * @param player
     */
    public static void doObjectSecondClick(final Player player)
    {
        final int id = player.getClickId();
        final int x = player.getClickX();
        final int y = player.getClickY();

        final ObjectDefinition def = ObjectDefinition.forId(id);
        final int orig_distance = Misc.getDistance(player.getPosition(), new Position(x, y));

        player.getUpdateFlags().sendFaceToDirection(new Position(x, y, player.getPosition().getZ()));

        player.setWalkToAction(new Task(1, true)
        {

            @Override
            protected void execute()
            {

                if (!player.getMovementHandler().walkToAction(new Position(x, y, player.getPosition().getZ()), getObjectSize(player.getClickId())))
                {
                    return;
                }

                final String objName = (def.getName() == null ? Integer.toString(id) : def.getName().toLowerCase());

                World.submit(new Task(orig_distance == 1 ? 1 : 2)
                {

                    @Override
                    protected void execute()
                    {

                        // TODO: AKZU
                        if (def.actions != null && def.actions.length > 1 && def.actions[1].startsWith("Climb-up"))
                            System.err.println("Climb-up");
                        else if (def.actions != null && def.actions.length > 1 && def.actions[1].startsWith("Climb-down"))
                            System.err.println("Climb-down");

                        if (id == 3193 || objName.toLowerCase().startsWith("bank") && !objName.toLowerCase().contains("deposit"))
                        {
                            BankManager.openBank(player);
                        }
                        else
                        {
                            switch (id)
                            {

                            case 8151:

                                player.getActionSender().sendMessage("plz code me - 8151");

                                break;

                            default:
                                String scriptName = "objectOptionTwo_" + objName.replaceAll(" ", "_").replaceAll("'", "");
                                System.out.println("Attempting to call script " + scriptName);
                                if (!ScriptManager.getSingleton().invokeWithFailTest(scriptName, player))
                                {
                                    player.getActionSender().sendMessage("Nothing interesting happens.");
                                }
                                break;

                            }
                        }
                        actions = null;
                        this.stop();
                    }
                });
                this.stop();
            }
        });
    }

    public static void doObjectThirdClick(final Player player)
    {
        final int id = player.getClickId();
        final int x = player.getClickX();
        final int y = player.getClickY();

        final int orig_distance = Misc.getDistance(player.getPosition(), new Position(x, y));
        final ObjectDefinition def = ObjectDefinition.forId(id);

        player.getUpdateFlags().sendFaceToDirection(new Position(x, y, player.getPosition().getZ()));

        player.setWalkToAction(new Task(1, true)
        {

            @Override
            protected void execute()
            {

                if (!player.getMovementHandler().walkToAction(new Position(x, y, player.getPosition().getZ()), getObjectSize(player.getClickId())))
                {
                    return;
                }

                final String objName = (def.getName() == null ? "" : def.getName().toLowerCase());

                World.submit(new Task(orig_distance == 1 ? 1 : 2)
                {

                    @Override
                    protected void execute()
                    {

                        String scriptName = "objectOptionThree_" + objName.replaceAll(" ", "_");
                        System.out.println("Attempting to call script " + scriptName);
                        if (!ScriptManager.getSingleton().invokeWithFailTest(scriptName, player))
                        {
                            player.getActionSender().sendMessage("Nothing interesting happens.");
                        }

                        actions = null;
                        this.stop();
                    }
                });
                this.stop();
            }
        });
    }

    private static void invokeFirstNpcInteraction(final Player player)
    {
        final int x = player.getClickX();
        final int y = player.getClickY();

        final Npc npc = World.getNpc(player.getNpcClickIndex());

        // -- We're already interacting with this npc.
        if (player.getInteractingEntity() != null && player.getInteractingEntity() == npc)
            return;

        player.setWalkToAction(new Task(1, true)
        {

            @Override
            protected void execute()
            {

                int offset = npc.getDefinition().getSize();

                switch (npc.getDefinition().getId())
                {
                case 305:
                    offset += 10;
                    break;
                }

                if (!player.getMovementHandler().walkToAction(new Position(x, y, player.getPosition().getZ()), offset))
                {
                    return;
                }

                World.submit(new Task(1)
                {
                    @Override
                    protected void execute()
                    {

                        player.setInteractingEntity(npc);
                        npc.setInteractingEntity(player);
                        npc.getUpdateFlags().sendFaceToDirection(player.getPosition());

                        npc.setAttribute("IS_BUSY", (byte) 1);
                        switch (player.getClickId())
                        {
                        default:
                            String scriptName = "npcInteractOptionOne_".concat((String) (npc.getDefinition() != null ? npc.getDefinition().getName().replace(" ", "_").replace("-", "_") : npc.getIndex()));
                            if (player.isDebugging())
                            {
                                logger.info("attempting to call " + scriptName + ".");
                            }
                            if (!ScriptManager.getSingleton().invokeWithFailTest(scriptName, player, npc))
                            {
                                String[][] strings =
                                {
                                { "Hello there, lovely day, isn't it?" },
                                { "Welcome to Asgarnia, please enjoy your stay." },
                                { "Howdy! Were you looking for help?", "I heard The Wise Old Man in Falador city offers", "a great deal of knowledge and assistance." } };
                                int idx = Misc.random(strings.length);
                                String[] message = strings[idx];
                                Dialogue dialogue = new ChatDialogue(npc, null, message);
                                player.open(dialogue);
                            }
                            break;
                        }
                        actions = null;
                        this.stop();
                    }
                });
                this.stop();
            }
        });
    }

    /**
     * 
     * @param player
     */
    private static void doNpcSecondClick(final Player player)
    {
        final int x = player.getClickX();
        final int y = player.getClickY();

        final Npc npc = World.getNpc(player.getNpcClickIndex());

        // -- We're already interacting with this npc.
        if (player.getInteractingEntity() != null && player.getInteractingEntity() == npc)
            return;

        player.setWalkToAction(new Task(1, true)
        {

            @Override
            protected void execute()
            {

                int offset = npc.getDefinition().getSize();

                switch (npc.getDefinition().getId())
                {
                case 494:
                    offset += 0;
                    break;
                }

                System.out.println("offset : " + offset + " npc : " + npc.getDefinition().getId());

                if (!player.getMovementHandler().walkToAction(new Position(x, y, player.getPosition().getZ()), offset))
                {
                    return;
                }

                World.submit(new Task(1)
                {
                    @Override
                    protected void execute()
                    {

                        player.setInteractingEntity(npc);
                        npc.setInteractingEntity(player);

                        if (player.isDebugging())
                        {
                            logger.info("Click ID : " + player.getClickId() + " NPC : " + npc.getDefinition().getName() + " " + npc.getNpcId());
                        }

                        npc.getUpdateFlags().sendFaceToDirection(player.getPosition());

                        switch (player.getClickId())
                        {
                        default:
                            String suffix = (String) (npc.getDefinition() != null ? npc.getDefinition().getName().replace(".", "").replace(" ", "_") : npc.getIndex());
                            String scriptName = "npcInteractOptionTwo_".concat(suffix);
                            if (player.isDebugging())
                            {
                                logger.info("attempted to call script " + scriptName + ".");

                            }
                            if (!ScriptManager.getSingleton().invokeWithFailTest(scriptName, player, npc))
                            {
                                player.getActionSender().sendMessage((npc.getDefinition() != null ? npc.getDefinition().getName() : "This mob") + " is too busy to trade right now.");
                            }

                        }
                        actions = null;
                        this.stop();
                    }
                });
                this.stop();
            }
        });
    }

    private static void doNpcThirdClick(final Player player)
    {
        final int x = player.getClickX();
        final int y = player.getClickY();

        final Npc npc = World.getNpc(player.getNpcClickIndex());

        // -- We're already interacting with this npc.
        if (player.getInteractingEntity() != null && player.getInteractingEntity() == npc)
            return;

        player.setWalkToAction(new Task(1, true)
        {

            @Override
            protected void execute()
            {
                final Npc npc = World.getNpc(player.getNpcClickIndex());

                if (!player.getMovementHandler().walkToAction(new Position(x, y, player.getPosition().getZ()), npc.getDefinition().getSize()))
                {
                    return;
                }

                World.submit(new Task(1)
                {
                    @Override
                    protected void execute()
                    {

                        player.setInteractingEntity(npc);
                        npc.setInteractingEntity(player);

                        if (player.isDebugging())
                            logger.info("Click ID : " + player.getClickId() + " NPC : " + npc.getDefinition().getName() + " " + npc.getNpcId());

                        npc.getUpdateFlags().sendFaceToDirection(player.getPosition());

                        switch (player.getClickId())
                        {
                        // Here
                        default:
                        }
                        actions = null;
                        this.stop();
                    }
                });
                this.stop();
            }
        });
    }

    private static void doItemOnObject(final Player player, final Item item)
    {
        // final int x = player.getClickX();
        // final int y = player.getClickY();
        player.setWalkToAction(new Task(1, true)
        {

            @Override
            protected void execute()
            {
                /*
                 * if (!player.getMovementHandler().walkToAction( new
                 * Position(x, y, player.getPosition().getZ()),
                 * getObjectSize(player.getClickId()))) {
                 * logger.severe("CANNOT PREFORM ITEM ON OBJECT!!!"); return; }
                 */
                /*
                 * //TODO: TEST if (player.getPosition().getX() - x < 0) {
                 * PathFinder.getSingleton().moveTo(player, x + 1, y); return; }
                 * else if (player.getPosition().getX() - x > 0) {
                 * PathFinder.getSingleton().moveTo(player, x - 1, y); return; }
                 */
                switch (player.getClickId())
                {
                case 2638:
                    player.getActionSender().sendMessage("You dip your amulet into the fountain...");
                    player.getUpdateFlags().sendAnimation(827, 0);
                    for (int i = 0; i < Inventory.SIZE; i++)
                    {
                        int[] glorys =
                        { 1704, 1706, 1708, 1710, 1712 };
                        for (int glory : glorys)
                        {
                            if (player.getInventory().getItemContainer().contains(glory))
                            {
                                player.getInventory().addItemToSlot(new Item(1712, 1), player.getInventory().getItemContainer().getSlotById(glory));
                            }
                        }
                    }
                    break;
                default:
                    String scriptName = "itemOnObject_" + player.getClickId();
                    boolean success = ScriptManager.getSingleton().invokeWithFailTest(scriptName, player, item);
                    if (!success)
                    {
                        logger.severe("unhandled item on object for object : " + player.getClickId());
                    }
                    if (player.isDebugging())
                    {
                        logger.info("Attempted to call script : " + scriptName + " successfull ? : " + success);
                    }
                }
                actions = null;
                this.stop();
            }
        });
    }

    public static int getObjectSize(int objectId)
    {
        return ObjectDefinition.forId(objectId).width;
    }

    public static void setActions(Actions actions)
    {
        WalkToActions.actions = actions;
    }

    public static Actions getActions()
    {
        return actions;
    }

    /**
     * 
     * @author Joshua Barry
     * 
     */
    public static enum Actions
    {

        OBJECT_FIRST_CLICK, OBJECT_SECOND_CLICK, OBJECT_THIRD_CLICK, ITEM_ON_OBJECT,

        NPC_FIRST_CLICK, NPC_SECOND_CLICK, NPC_THIRD_CLICK
    }

}
