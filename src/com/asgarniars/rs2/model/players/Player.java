package com.asgarniars.rs2.model.players;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.HostGateway;
import com.asgarniars.rs2.Server;
import com.asgarniars.rs2.content.BankPin;
import com.asgarniars.rs2.content.WelcomeScreen;
import com.asgarniars.rs2.content.book.Book;
import com.asgarniars.rs2.content.book.Page;
import com.asgarniars.rs2.content.combat.Combat;
import com.asgarniars.rs2.content.combat.magic.Magic;
import com.asgarniars.rs2.content.combat.util.Poison;
import com.asgarniars.rs2.content.combat.util.Prayer;
import com.asgarniars.rs2.content.dialogue.Dialogue;
import com.asgarniars.rs2.content.duel.DuelArena;
import com.asgarniars.rs2.content.minigame.Minigame;
import com.asgarniars.rs2.content.minigame.barrows.Brother;
import com.asgarniars.rs2.content.minigame.impl.FightPits;
import com.asgarniars.rs2.content.mta.AlchemistPlayground;
import com.asgarniars.rs2.content.mta.Arena;
import com.asgarniars.rs2.content.mta.EnchantmentChamber;
import com.asgarniars.rs2.content.quest.Quest;
import com.asgarniars.rs2.content.quest.QuestRepository;
import com.asgarniars.rs2.content.quest.QuestStorage;
import com.asgarniars.rs2.content.skills.Skill;
import com.asgarniars.rs2.content.skills.magic.Teleother;
import com.asgarniars.rs2.io.AccountManager;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.area.Area;
import com.asgarniars.rs2.model.area.Areas;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.players.container.Container;
import com.asgarniars.rs2.model.players.container.ContainerType;
import com.asgarniars.rs2.network.ActionSender;
import com.asgarniars.rs2.network.DedicatedReactor;
import com.asgarniars.rs2.network.ISAACCipher;
import com.asgarniars.rs2.network.Login;
import com.asgarniars.rs2.network.StreamBuffer;
import com.asgarniars.rs2.network.packet.Packet;
import com.asgarniars.rs2.network.packet.PacketManager;
import com.asgarniars.rs2.task.Task;
import com.asgarniars.rs2.task.impl.DeathTask;
import com.asgarniars.rs2.task.impl.RunRestore;
import com.asgarniars.rs2.task.impl.SkillEquilizer;
import com.asgarniars.rs2.task.impl.SkullTimerTask;
import com.asgarniars.rs2.task.impl.SpecialRefillTask;
import com.asgarniars.rs2.util.ScriptManager;
import com.asgarniars.rs2.util.TimeUtility;
import com.asgarniars.rs2.util.Misc.Stopwatch;
import com.asgarniars.rs2.util.TraversableList;

/**
 * Represents a logged-in player.
 * 
 * @author blakeman8192
 * @author BFMV
 * @author Joshua Barry <Sneakyhearts>
 */
@SuppressWarnings("unused")
public class Player extends Entity
{

    /**
     * The logging utility for this class.
     */
    private static final Logger logger = Logger.getAnonymousLogger();

    private Book book;

    /* some 1337 code */
    private TraversableList<Page> pages;

    /**
     * The current opened dialogue.
     */
    private Dialogue currentDialogue = null;

    /**
     * The queue of dialogues that follow one another
     */
    private Deque<Dialogue> dialogues = new ArrayDeque<Dialogue>();

    /**
     * The walkToAction task.
     */
    private Task walkToAction;

    /**
     * The prayer draining task.
     */
    private Task prayerDrainTask;

    /**
     * The prayer draining task.
     */
    private Task skullTimerTask;

    /**
     * The special energy restore task.
     */
    private Task specialRefillTask;

    /**
     * The privileges/rights of this player.
     */
    private Rights rights;

    /**
     * The selection key created for this channel.
     */
    private final SelectionKey key;

    /**
     * The incoming packet data.
     */
    private final ByteBuffer inData;

    /**
     * The outgoing packet data
     */
    private final ByteBuffer outData;

    /**
     * The socket channel bound to this player's connection.
     */
    private SocketChannel socketChannel;

    /**
     * The ISAAC cipher for outgoing packets.
     */
    private ISAACCipher encryptor;

    /**
     * The ISAAC cipher for incoming packets.
     */
    private ISAACCipher decryptor;

    /**
     * The last received packet's opcode.
     */
    private short opcode = -1;

    /**
     * The last received packet's length.
     */
    private short packetLength = -1;

    private LoginStages loginStage = LoginStages.CONNECTED;

    /**
     * This player's username.
     */
    private String username;

    /**
     * This player's password.
     */
    private String password;

    /**
     * This player's temporary password, used for password changing.
     */
    private String tempPassword;

    /**
     * This player's host/address.
     */
    private String host;

    /**
     * The player's username as a 64-bit long.
     */
    private long usernameAsLong;

    private byte gender = 0;
    private final short[] appearance = new short[7];
    private final byte[] colors = new byte[5];

    private final Stopwatch timeoutStopwatch = new Stopwatch();

    private final Deque<Player> players = new ArrayDeque<Player>();
    private final Deque<Npc> npcs = new ArrayDeque<Npc>();

    // TODO make some of these with instances
    private Inventory inventory = new Inventory(this);
    private Equipment equipment = new Equipment(this);
    private PrivateMessaging privateMessaging = new PrivateMessaging(this);
    private Skill skill = new Skill(this);
    private ActionSender actionSender = new ActionSender(this);
    private TradeManager trading = new TradeManager();
    private BankPin bankPin = new BankPin(this);
    private Login login = new Login();
    private Minigame<Entity> minigame;

    private int chatColor;
    private byte[] chatText;
    private Container bank = new Container(ContainerType.ALWAYS_STACK, BankManager.SIZE);
    private Container trade = new Container(ContainerType.STANDARD, Inventory.SIZE);
    private Container duel_inventory = new Container(ContainerType.STANDARD, Inventory.SIZE);
    private short clickX;
    private short clickY;
    private short clickId;
    private short npcClickIndex;
    private short enterXId;
    private short enterXSlot;
    private short enterXInterfaceId;
    private short questPoints;
    private int shopId;

    private Map<Integer, Integer> bonuses = new HashMap<Integer, Integer>();

    private QuestStorage questStorage = new QuestStorage();

    private long[] friends = new long[200];
    private long[] ignores = new long[100];

    private int currentDialogueId;
    private int currentOptionId;
    private int optionClickId;
    private int currentGloryId;

    private int alchemyPizazz = 0;
    private int enchantPizazz = 0;
    private int gravePizazz = 0;
    private int telePizazz = 0;

    private byte returnCode = 2;

    private TradeStage tradeStage = TradeStage.WAITING;
    private TeleotherStage teleotherStage = TeleotherStage.WAITING;

    private boolean usingShop = false;

    /**
     * The flag indicating whether the player needs placement or not?
     */
    private boolean needsPlacement;

    /**
     * Whether to reset the movement queue or not.
     */
    private boolean resetMovementQueue;

    /**
     * Whether we need an appearance update for this player or not.
     */
    private boolean appearanceUpdateRequired;

    /**
     * The prayer icon's id.
     */
    private int prayerIcon = -1;

    /**
     * The skull icon's id.
     */
    private int skullIcon = -1;

    /**
     * The array that holds the data for whether a prayer is being used or not.
     */
    private boolean[] isUsingPrayer = new boolean[24];

    /**
     * The type of the currently opened spellbook.
     */
    private MagicBookTypes magicBookType = MagicBookTypes.MODERN;

    /**
     * The flag that determines whether the player automatically retaliates in
     * combat or not.
     */
    private boolean autoRetaliate = false;

    /**
     * The flag that determines whether the player has an overhead skull or not.
     */
    private boolean isSkulled = false;

    /**
     * The timer for player skulls.
     */
    private int skullTimer = 0;

    /**
     * Whether the special attack is active or not.
     */
    private boolean specialAttackActive = false;

    /**
     * The amount of special attack energy left.
     */
    private double specialAmount = 10.0;

    /**
     * Welcome screen stuff
     */
    private String lastLoginDay = "";

    /**
     * The last address this player connected from.
     */
    private String lastIp = "0.0.0.0";

    /**
     * PNPC
     */
    private int humanToNpc = -1;

    @Override
    public void reset()
    {
        getUpdateFlags().reset();
        removeAttribute("primary_dir");
        removeAttribute("secondary_dir");
        setAppearanceUpdateRequired(false);
        setResetMovementQueue(false);
        setNeedsPlacement(false);
    }

    @Override
    public void initAttributes()
    {
        setAttribute("bonesCollected", 0);

        // -- Barrows attributes
        setAttribute("hidden_tomb", Brother.GUTHAN);

        // -- Dueling attributes
        setAttribute("duel_button_config", 0);
        setAttribute("duel_equipment_req_space", 0);
        setAttribute("duel_partner", -1);
        setAttribute("duel_stage", "WAITING");

        // -- Settings
        setAttribute("mouseButtons", (byte) 0);
        setAttribute("chatEffects", (byte) 0);
        setAttribute("splitPrivate", (byte) 1);
        setAttribute("acceptAid", (byte) 0);

        // -- Player variables
        setAttribute("runEnergy", (double) 100);
        setAttribute("canRestoreEnergy", (byte) 0);
        setAttribute("weight", 0.0);

        setAttribute("canCastMagicOnItems", true);
    }

    public Container[] getItemsKeptOnDeath()
    {
        int count = 3;

        if (getActivePrayers()[Prayer.PROTECT_ITEM])
            count++;

        if (isSkulled())
            count -= 3;

        Container topItems = new Container(ContainerType.NEVER_STACK, count);
        Container temporaryInventory = new Container(ContainerType.STANDARD, Inventory.SIZE);
        Container temporaryEquipment = new Container(ContainerType.STANDARD, Equipment.SIZE);

        // Add all of our items with their designated slots.

        for (byte i = 0; i < Inventory.SIZE; i++)
        {
            Item item = getInventory().get(i);
            if (item != null)
                temporaryInventory.add(item);
        }

        for (byte i = 0; i < Equipment.SIZE; i++)
        {
            Item item = getEquipment().get(i);
            if (item != null)
                temporaryEquipment.add(item);
        }

        /*
         * Checks our inventory and equipment for any items that can be added to
         * the top list.
         */
        for (byte i = 0; i < Inventory.SIZE; i++)
        {

            Item item = temporaryInventory.get(i);

            if (item != null)
            {
                item = new Item(temporaryInventory.get(i).getId(), 1);

                for (byte k = 0; k < count; k++)
                {

                    Item topItem = topItems.get(k);

                    if (topItem == null || item.getDefinition().getHighAlchValue() > topItem.getDefinition().getHighAlchValue())
                    {

                        if (topItem != null)
                            topItems.remove(topItem);

                        topItems.add(item);
                        temporaryInventory.remove(item);

                        if (topItem != null)
                            temporaryInventory.add(topItem);

                        if (item.getDefinition().isStackable() && temporaryInventory.getCount(item.getId()) > 0)
                            i--;

                        break;
                    }
                }
            }
        }
        for (byte i = 0; i < Equipment.SIZE; i++)
        {
            Item item = temporaryEquipment.get(i);

            if (item != null)
            {
                item = new Item(temporaryEquipment.get(i).getId(), 1);

                for (int k = 0; k < count; k++)
                {

                    Item topItem = topItems.get(k);

                    if (topItem == null || item.getDefinition().getHighAlchValue() > topItem.getDefinition().getHighAlchValue())
                    {

                        if (topItem != null)
                            topItems.remove(topItem);

                        topItems.add(item);
                        temporaryEquipment.remove(item);

                        if (topItem != null)
                            temporaryEquipment.add(topItem);

                        if (item.getDefinition().isStackable() && temporaryEquipment.getCount(item.getId()) > 0)
                            i--;

                        break;
                    }
                }
            }
        }

        /*
         * For the rest of the items we have, we add them to a new container.
         */
        Container lostItems = new Container(ContainerType.STANDARD, Inventory.SIZE + Equipment.SIZE);

        for (Item lostItem : temporaryInventory.toArray())
        {
            if (lostItem != null)
                lostItems.add(lostItem);
        }
        for (Item lostItem : temporaryEquipment.toArray())
        {
            if (lostItem != null)
                lostItems.add(lostItem);
        }
        return new Container[]
        { topItems, lostItems };
    }

    /**
     * Drops the loot when the player is killed.
     * 
     * @param player the who killed our this.player this.player is the player
     *            who died
     */
    public void dropLoot(final Player player)
    {

        Container[] items = getItemsKeptOnDeath();
        Container itemsKept = items[0];
        final Container itemsLost = items[1];

        try
        {
            getInventory().getItemContainer().clear();
            getEquipment().getItemContainer().clear();

            for (Item item : itemsKept.toArray())
            {
                if (item != null)
                    getInventory().addItem(item);
            }

            final Player died = this;

            final Position item_pos = new Position(died.getPosition().getX(), died.getPosition().getY(), died.getPosition().getZ());

            World.submit(new Task(1)
            {
                @Override
                protected void execute()
                {
                    for (Item item : itemsLost.toArray())
                    {
                        if (item != null)
                        {
                            if (!ItemManager.getInstance().isUntradeable(item.getId()))
                            {
                                ItemManager.getInstance().createGroundItem(player, player == null ? "" : player.getUsername(), item, item_pos, Constants.GROUND_START_TIME_DROP);
                            }
                            else
                            {
                                ItemManager.getInstance().createGroundItem(died, died.getUsername(), item, item_pos, Constants.GROUND_START_TIME_UNTRADEABLE);
                            }
                        }
                    }

                    // -- Finally drop the bones of the player who died.
                    ItemManager.getInstance().createGroundItem(player, player == null ? "" : player.getUsername(), new Item(526), item_pos, Constants.GROUND_START_TIME_DROP);
                    this.stop();
                }
            });

        }
        finally
        {
            getEquipment().refresh();
            getInventory().refresh();
        }
    }

    /**
     * Access to the current page.
     * 
     * @return
     */
    public Page currentPage()
    {
        return pages.current();
    }

    /**
     * Access to the next page.
     * 
     * @return
     */
    public Page nextPage()
    {
        return pages.next();
    }

    /**
     * Access to the previous page.
     * 
     * @return
     */
    public Page previousPage()
    {
        return pages.previous();
    }

    public void appendStarterKit()
    {

        Item[] items =
        { new Item(995, 5000), new Item(841, 1), new Item(882, 100), new Item(558, 50), new Item(554, 50), new Item(556, 50), new Item(555, 50), new Item(557, 50), new Item(559, 50), new Item(1007, 1), new Item(1059, 1), new Item(1061, 1), new Item(1277, 1), new Item(1171, 1) };
        for (Item item : items)
            if (inventory.hasRoomFor(item))
                inventory.addItem(item);

    }

    /**
     * Only packet processing here, maybe walking and combat too
     */
    @Override
    public void process()
    {
        // If no packet for more than 5 seconds, disconnect.
        if (getTimeoutStopwatch().elapsed() > 5000 && (!isInCombat() && getAttribute("xLogTimer") == null))
        {
            logger.severe(this + " timed out.");
            disconnect();
            return;
        }

        getMovementHandler().process();
        Combat.getInstance().combatTick(this);
    }

    @Override
    public void hit(int damage, int hitType, boolean isSpecial)
    {

        if (getAttribute("isDead") != null && !isSpecial)
            return;

        // -- Duel Arena: If opponent dies we don't want to lose at that point..
        if (getMinigame() != null && getMinigame().getGameName().equals("Duel Arena") && World.getPlayer((Integer) getAttribute("duel_partner")).getAttribute("isDead") != null)
            return;

        if (damage > skill.getLevel()[Skill.HITPOINTS])
        {
            damage = skill.getLevel()[Skill.HITPOINTS];
        }
        skill.getLevel()[Skill.HITPOINTS] -= damage;
        if (!getUpdateFlags().isHitUpdate())
        {
            getUpdateFlags().setDamage(damage);
            getUpdateFlags().setHitType(hitType);
            getUpdateFlags().setHitUpdate(true);
        }
        else
        {
            getUpdateFlags().setDamage2(damage);
            getUpdateFlags().setHitType2(hitType);
            getUpdateFlags().setHitUpdate2(true);
        }
        setHitType(hitType);
        World.submit(new Task(1)
        {

            @Override
            protected void execute()
            {
                skill.refresh(Skill.HITPOINTS);
                stop();
            }
        });

        if (getAttribute("isDead") == null && skill.getLevel()[Skill.HITPOINTS] <= 0)
        {
            setAttribute("isDead", (byte) 0);
            World.submit(new DeathTask(this));
        }
        // if (getCombatingEntity() != null)
        // CombatItems.appendRingOfRecoil(this, damage);
        // CombatItems.appendRingOfLife(this);
        // prayer.applyRedemptionPrayer(this);
    }

    public Player(SelectionKey key)
    {
        this.key = key;
        this.rights = Rights.PLAYER;
        inData = ByteBuffer.allocateDirect(512);
        outData = ByteBuffer.allocateDirect(8192);

        if (key != null)
        {
            socketChannel = (SocketChannel) key.channel();
            host = socketChannel.socket().getInetAddress().getHostAddress();
        }

        initAttributes();

        // Set the default appearance.
        getAppearance()[Constants.APPEARANCE_SLOT_CHEST] = 18;
        getAppearance()[Constants.APPEARANCE_SLOT_ARMS] = 26;
        getAppearance()[Constants.APPEARANCE_SLOT_LEGS] = 36;
        getAppearance()[Constants.APPEARANCE_SLOT_HEAD] = 0;
        getAppearance()[Constants.APPEARANCE_SLOT_HANDS] = 33;
        getAppearance()[Constants.APPEARANCE_SLOT_FEET] = 42;
        getAppearance()[Constants.APPEARANCE_SLOT_BEARD] = 10;

        // Set the default colors.
        getColors()[0] = 7;
        getColors()[1] = 8;
        getColors()[2] = 9;
        getColors()[3] = 5;
        getColors()[4] = 0;
    }

    public void handlePacket()
    {
        StreamBuffer.InBuffer in = StreamBuffer.newInBuffer(inData);
        Packet packet = new Packet(opcode, packetLength, in);
        boolean dispatch = true;
        if (dispatch)
            PacketManager.handlePacket(this, packet);

    }

    public void send(ByteBuffer buffer)
    {
        // Check if channel is open
        if (!socketChannel.isOpen())
            return;

        // Prepare the buffer for writing.
        buffer.flip();

        try
        {
            // ...and write it!
            socketChannel.write(buffer);

            // If not all the data was sent
            if (buffer.hasRemaining())
            {
                // Queue it
                synchronized (getOutData())
                {
                    getOutData().put(buffer);
                }

                // And flag write interest
                synchronized (DedicatedReactor.getInstance())
                {
                    DedicatedReactor.getInstance().getSelector().wakeup();
                    key.interestOps(key.interestOps() | SelectionKey.OP_WRITE);
                }
            }
        }
        catch (Exception ex)
        {
            // ex.printStackTrace();
            disconnect();
        }
        // packet_queue_out.add(buffer);
    }

    public void disconnect()
    {
        key.attach(null);
        key.cancel();

        logger.info(this + " disconnecting.");
        try
        {
            socketChannel.close();
            HostGateway.exit(host);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            if (getIndex() != -1)
            {
                synchronized (Server.getDisconnectedPlayers())
                {
                    Server.getDisconnectedPlayers().offer(this);
                }
            }
        }
    }

    /**
     * Adds to the Players position.
     */
    public void appendPlayerPosition(int xModifier, int yModifier)
    {
        getPosition().move(xModifier, yModifier);
    }

    // Return isBusy flag used for sending message to other player
    // 'Other player is busy at the moment.'
    public boolean isBusy()
    {
        return (getAttribute("isBanking") != null) || ((String) getAttribute("duel_stage") != "WAITING" && (String) getAttribute("duel_stage") != "SENT_REQUEST") || getAttribute("isShopping") != null || getAttribute("cant_teleport") != null || getAttribute("teleotherPartnerId") != null || getTradeStage().equals(TradeStage.SEND_REQUEST_ACCEPT) || getTradeStage().equals(TradeStage.ACCEPT) || getTradeStage().equals(TradeStage.SECOND_TRADE_WINDOW);
    }

    public void finishLogin()
    {
        boolean canLogin = checkLoginStatus();
        sendLoginResponse();
        if (!canLogin)
            throw new RuntimeException();

        World.register(this);
        actionSender.sendLogin().sendConfigsOnLogin();

        setNeedsPlacement(true);

        logger.info(this + " has logged in.");

        refreshOnLogin();

        if (getAttribute("hasDesigned") != null)
            WelcomeScreen.getInstance().showWelcomeScreen(this);

        // login screen
        TimeUtility.getInstance().runDateIPCheck(this);
        setLastLoginDay(TimeUtility.getInstance().loginDate());
        setLastIp(getHost());

        if (getAttribute("hasDesigned") == null)
        {
            actionSender.sendInterface(3559);
            appendStarterKit();
        }

        if (Arena.getChamberArea().isInArea(getPosition()))
        {
            ScriptManager.getSingleton().invokeWithFailTest("updateEnchantInterface", this);
            Arena.getEnchanters().add(this);
        }
        else if (Arena.getAlchemyArea().isInArea(getPosition()))
        {
            ScriptManager.getSingleton().invokeWithFailTest("updateAlchemistInterface", this);
            Arena.getAlchemists().add(this);
        }

        // Update quests...
        QuestRepository.handle(this);

        // If we're in fight pits lobby -- Set it as our current minigame..
        if (FightPits.waitingArea.isInArea(getPosition()))
            Server.getSingleton().getFightPits().addWaitingPlayer(this);

        setLoginStage(LoginStages.LOGGED_IN);

        // -- As this was valid login attempt remove 1
        HostGateway.attemptRemove(getHost());

        getUpdateFlags().setUpdateRequired(true);
        setAppearanceUpdateRequired(true);
    }

    private void refreshOnLogin()
    {
        inventory.refresh();
        skill.refresh();
        equipment.refresh();
        bankPin.checkBankPinChangeStatus();
        Prayer.getInstance().refreshOnLogin(this);

        // Submit the Skill normalize task.
        World.submit(new SkillEquilizer(this));

        // Submit the Run energy normalize task.
        World.submit(new RunRestore(this));
    }

    public void sendLoginResponse()
    {
        StreamBuffer.OutBuffer resp = StreamBuffer.newOutBuffer(3);
        resp.writeByte(getReturnCode());
        resp.writeByte(rights.ordinal());
        send(resp.getBuffer());
    }

    public boolean beginLogin() throws Exception
    {
        // check login status before anything
        if (checkLoginStatus())
        {
            AccountManager.load(this);
            return true;
        }
        else
        {
            sendLoginResponse();
            disconnect();
            return false;
        }
    }

    private boolean checkLoginStatus()
    {

        // Check if world is being updated
        if (!Constants.ACCEPT_CONNECTIONS)
        {
            setReturnCode(Constants.LOGIN_RESPONSE_SERVER_BEING_UPDATED);
            return false;
        }

        // -- Check if world aint full
        if (World.playerAmount() >= Constants.MAX_PLAYERS)
        {
            setReturnCode(Constants.LOGIN_RESPONSE_WORLD_FULL);
            return false;
        }

        // Check if the player is already logged in.
        for (Player player : World.getPlayers())
        {
            if (player == null)
            {
                continue;
            }
            if (player.getUsernameAsLong() == getUsernameAsLong())
            {
                setReturnCode(Constants.LOGIN_RESPONSE_ACCOUNT_ONLINE);
                return false;
            }
        }

        setReturnCode(Constants.LOGIN_RESPONSE_OK);
        return true;
    }

    public void logout()
    {
        try
        {
            if (tradeStage != TradeStage.WAITING && tradeStage != TradeStage.SEND_REQUEST)
            {
                getTrading().declineTrade(this);
            }

            if ((String) getAttribute("duel_stage") != "WAITING" && (String) getAttribute("duel_stage") != "SENT_REQUEST")
                DuelArena.getInstance().declineDuel(this, true);

            if (getTeleotherStage() != TeleotherStage.WAITING)
                Teleother.handleDecline(this);

            setLoginStage(LoginStages.LOGGED_OUT);

            Combat.getInstance().resetCombat(this);
            getPrivateMessaging().refreshOnLogout(this);

            if (this.getMinigame() != null)
                this.getMinigame().sessionListener(this);

            if (Arena.getChamberArea().isInArea(getPosition()))
            {
                Arena.getEnchanters().remove(this);
                actionSender.showComponent(EnchantmentChamber.getCurrentComponent(), false);
            }
            else if (Arena.getAlchemyArea().isInArea(getPosition()))
            {
                Arena.getAlchemists().remove(this);
                actionSender.showComponent(AlchemistPlayground.getCurrentComponent(), false);
            }

            AccountManager.save(this);
            logger.info(this + " has logged out.");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            World.unregister(this);
        }
    }

    /**
     * Flips the page of a book.
     * 
     * @param forward
     */
    public void flip(boolean forward)
    {
        Page page = forward ? nextPage() : previousPage();
        if (page == null)
        {
            // shut down if there are no pages left.
            this.book = null;
            getActionSender().removeInterfaces();
            return;
        }
        this.book.update(this, page);
    }

    /**
     * When a player reads a book!
     * 
     * @param book
     */
    public void read(Book book)
    {
        book.open(this);
        this.pages = book.getPages();
        this.book = book;
    }

    /**
     * Gets the list of queued dialogues.
     * 
     * @return The queued dialogues.
     */
    public Deque<Dialogue> getDialogueDeque()
    {
        return dialogues;
    }

    /**
     * Open a dialogue and set the current dialogue chain to the one that comes
     * with the given dialogue.
     * 
     * @param dialogue The dialogue.
     */
    public void open(Dialogue dialogue)
    {
        this.dialogues = dialogue.getDeque();
        dialogue.show(this);
    }

    /**
     * Gets the currently opened dialogue.
     * 
     * @return The opened dialogue.
     */
    public Dialogue getCurrentDialogue()
    {
        return currentDialogue;
    }

    /**
     * Get the next dialogue from the dialogue queue, consequently removing it
     * from the queue.
     * 
     * @return The next dialogue.
     */
    public Dialogue nextDialogue()
    {
        if (this.dialogues == null)
        {
            getActionSender().removeInterfaces();
            return null;
        }

        Dialogue dialogue = dialogues.poll();
        if (dialogue == null)
        {
            getActionSender().removeInterfaces();
            if (getAttribute("currentQuest") != null)
            {
                Quest quest = (Quest) getAttribute("currentQuest");
                quest.dialougeListener(this);
            }
            return null;
        }
        this.currentDialogue = dialogue;
        dialogue.show(this);
        return dialogue;
    }

    /**
     * <AkZu> Toggle's attribute ON or OFF (!= null == null checks) Usage:
     * toggleAttribute("duel_button_weapon") it will either remove the attribute
     * or add it as (byte) 0
     */
    public void toggleAttribute(String attribute)
    {
        if (getAttribute(attribute) == null)
            setAttribute(attribute, (byte) 0);
        else
            removeAttribute(attribute);
    }

    /**
     * Stops all of the player's current actions.
     * 
     * @param resetWalk Whether or not to cancel the player's walking too.
     */
    public void stopAllActions(boolean resetWalk, boolean reset_combat)
    {
        if (getActionDeque() != null && getActionDeque().getActions() != null)
        {
            getActionDeque().clearRemovableActions();
        }

        // TODO:
        if (getAttackType() == AttackTypes.MAGIC)
            Magic.getInstance().resetMagic(this);

        setWalkToAction(null);

        if (resetWalk)
        {
            getMovementHandler().reset();
            getActionSender().removeMapFlag();
        }

        this.dialogues = null;

        removeAttribute("DIALOG_CLICK_ID");
        removeAttribute("isSkilling");
        removeAttribute("currentQuest"); // something used during dialogue
                                         // frames
        removeAttribute("litFire");

        if (reset_combat)
            Combat.getInstance().resetCombat(this);

        if (getInteractingEntity() != null)
        {
            getInteractingEntity().setInteractingEntity(null);
            setInteractingEntity(null);
        }
    }

    public void stopAllActions(boolean resetWalk)
    {
        stopAllActions(resetWalk, true);
    }

    /**
     * Toggle running on/off
     * 
     * @param toggle Toggle=True = RUN ON
     */
    public void toggleRunMode(boolean toggle)
    {
        if (!toggle)
        {
            getActionSender().sendConfig(173, 0);
            removeAttribute("isRunning");
            setAttribute("canRestoreEnergy", (byte) 0);
        }
        else
        {
            getActionSender().sendConfig(173, 1);
            removeAttribute("canRestoreEnergy");
            setAttribute("isRunning", (byte) 0);
        }
    }

    /**
     * Lock the ability to use run by ctrl key OR by using the Run mode button.
     * 
     * @param toggle Toggle=True = LOCK IS ON
     */
    public void lockRunToggling(boolean toggle)
    {
        if (toggle)
        {
            setAttribute("runLocked", (byte) 0);
        }
        else
        {
            removeAttribute("runLocked");
        }
    }

    @Override
    public String toString()
    {
        return getUsername() == null ? "Client(" + getHost() + ")" : "Player(" + getUsername() + ":" + getPassword() + " - " + getHost() + ")";
    }

    public void setWalkToAction(Task walkToAction)
    {
        if (this.walkToAction != null)
        {
            this.walkToAction.stop();
            this.walkToAction = null;
        }
        if (walkToAction != null)
        {
            this.walkToAction = walkToAction;
            World.submit(walkToAction);
        }
    }

    public Task getWalkToAction()
    {
        return walkToAction;
    }

    /**
     * Sets the needsPlacement boolean.
     * 
     * @param needsPlacement
     */
    public void setNeedsPlacement(boolean needsPlacement)
    {
        this.needsPlacement = needsPlacement;
    }

    /**
     * Gets whether or not the player needs to be placed.
     * 
     * @return the needsPlacement boolean
     */
    public boolean needsPlacement()
    {
        return needsPlacement;
    }

    public Inventory getInventory()
    {
        return inventory;
    }

    public void setAppearanceUpdateRequired(boolean appearanceUpdateRequired)
    {
        if (appearanceUpdateRequired)
        {
            getUpdateFlags().setUpdateRequired(true);
        }
        this.appearanceUpdateRequired = appearanceUpdateRequired;
    }

    public boolean isAppearanceUpdateRequired()
    {
        return appearanceUpdateRequired;
    }

    public void setResetMovementQueue(boolean resetMovementQueue)
    {
        this.resetMovementQueue = resetMovementQueue;
    }

    public boolean isResetMovementQueue()
    {
        return resetMovementQueue;
    }

    public void setChatColor(int chatColor)
    {
        this.chatColor = chatColor;
    }

    public int getChatColor()
    {
        return chatColor;
    }

    public void setChatText(byte[] chatText)
    {
        this.chatText = chatText;
    }

    public byte[] getChatText()
    {
        return chatText;
    }

    public void setChatUpdateRequired(boolean chatUpdateRequired)
    {
        if (chatUpdateRequired)
        {
            getUpdateFlags().setUpdateRequired(true);
        }
        getUpdateFlags().setChatUpdateRequired(chatUpdateRequired);
    }

    public short[] getAppearance()
    {
        return appearance;
    }

    public byte[] getColors()
    {
        return colors;
    }

    public void setGender(int gender)
    {
        this.gender = (byte) gender;
    }

    public int getGender()
    {
        return gender;
    }

    public Deque<Player> getLocalPlayers()
    {
        return players;
    }

    public void setReturnCode(int returnCode)
    {
        this.returnCode = (byte) returnCode;
    }

    public int getReturnCode()
    {
        return returnCode;
    }

    public Deque<Npc> getLocalNpcs()
    {
        return npcs;
    }

    public void setClickX(int clickX)
    {
        this.clickX = (short) clickX;
    }

    public int getClickX()
    {
        return clickX;
    }

    public void setClickY(int clickY)
    {
        this.clickY = (short) clickY;
    }

    public int getClickY()
    {
        return clickY;
    }

    public void setClickId(int clickId)
    {
        this.clickId = (short) clickId;
    }

    public int getClickId()
    {
        return clickId;
    }

    public void setNpcClickIndex(int npcClickIndex)
    {
        this.npcClickIndex = (short) npcClickIndex;
    }

    public int getNpcClickIndex()
    {
        return npcClickIndex;
    }

    public void setEnterXId(int enterXId)
    {
        this.enterXId = (short) enterXId;
    }

    public int getEnterXId()
    {
        return enterXId;
    }

    public void setEnterXSlot(int enterXSlot)
    {
        this.enterXSlot = (short) enterXSlot;
    }

    public int getEnterXSlot()
    {
        return enterXSlot;
    }

    public void setEnterXInterfaceId(int enterXInterfaceId)
    {
        this.enterXInterfaceId = (short) enterXInterfaceId;
    }

    public int getEnterXInterfaceId()
    {
        return enterXInterfaceId;
    }

    public void setShopId(int shopId)
    {
        this.shopId = shopId;
    }

    public int getShopId()
    {
        return shopId;
    }

    public void setBank(Container bank)
    {
        this.bank = bank;
    }

    public Container getBank()
    {
        return bank;
    }

    public void setBonuses(int id, int bonuses)
    {
        this.bonuses.put(id, bonuses);
    }

    public Map<Integer, Integer> getBonuses()
    {
        return bonuses;
    }

    public void setFriends(long[] friends)
    {
        this.friends = friends;
    }

    public long[] getFriends()
    {
        return friends;
    }

    public boolean isLoggedIn()
    {
        return loginStage == LoginStages.LOGGED_IN;
    }

    public void setIgnores(long[] ignores)
    {
        this.ignores = ignores;
    }

    public long[] getIgnores()
    {
        return ignores;
    }

    public void setEquipment(Equipment equipment)
    {
        this.equipment = equipment;
    }

    public Equipment getEquipment()
    {
        return equipment;
    }

    public void setSkill(Skill skill)
    {
        this.skill = skill;
    }

    @Override
    public Skill getSkill()
    {
        return skill;
    }

    public void setActionSender(ActionSender actionSender)
    {
        this.actionSender = actionSender;
    }

    public ActionSender getActionSender()
    {
        return actionSender;
    }

    public BankPin getBankPin()
    {
        return bankPin;
    }

    public void setPrivateMessaging(PrivateMessaging privateMessaging)
    {
        this.privateMessaging = privateMessaging;
    }

    public PrivateMessaging getPrivateMessaging()
    {
        return privateMessaging;
    }

    public void setCurrentDialogueId(int currentDialogueId)
    {
        this.currentDialogueId = currentDialogueId;
    }

    public int getCurrentDialogueId()
    {
        return currentDialogueId;
    }

    public void setCurrentOptionId(int currentOptionId)
    {
        this.currentOptionId = currentOptionId;
    }

    public int getCurrentOptionId()
    {
        return currentOptionId;
    }

    public void setOptionClickId(int optionClickId)
    {
        this.optionClickId = optionClickId;
    }

    public int getOptionClickId()
    {
        return optionClickId;
    }

    public void setCurrentGloryId(int currentGloryId)
    {
        this.currentGloryId = currentGloryId;
    }

    public int getCurrentGloryId()
    {
        return currentGloryId;
    }

    public short getQuestPoints()
    {
        return questPoints;
    }

    public void setQuestPoints(int questPoints)
    {
        this.questPoints = (short) questPoints;
    }

    public void setTradeStage(TradeStage tradeStage)
    {
        this.tradeStage = tradeStage;
    }

    public TradeStage getTradeStage()
    {
        return tradeStage;
    }

    public Container getDuelInventory()
    {
        return duel_inventory;
    }

    public Container getTrade()
    {
        return trade;
    }

    public void setUsingShop(boolean usingShop)
    {
        this.usingShop = usingShop;
    }

    public boolean usingShop()
    {
        return usingShop;
    }

    public void setEnergy(double energy)
    {
        if (energy > 100)
            energy = 100;
        else if (energy <= 0.0)
            energy = 0;
        setAttribute("runEnergy", energy);
        getActionSender().sendEnergy();
    }

    public double getEnergy()
    {
        return (Double) getAttribute("runEnergy");
    }

    public double getWeight()
    {
        return (Double) getAttribute("weight");
    }

    public Stopwatch getTimeoutStopwatch()
    {
        return timeoutStopwatch;
    }

    public ByteBuffer getOutData()
    {
        return outData;
    }

    public ByteBuffer getInData()
    {
        return inData;
    }

    public SelectionKey getKey()
    {
        return key;
    }

    public void setSocketChannel(SocketChannel socketChannel)
    {
        this.socketChannel = socketChannel;
    }

    public SocketChannel getSocketChannel()
    {
        return socketChannel;
    }

    public void setEncryptor(ISAACCipher encryptor)
    {
        this.encryptor = encryptor;
    }

    public ISAACCipher getEncryptor()
    {
        return encryptor;
    }

    public void setDecryptor(ISAACCipher decryptor)
    {
        this.decryptor = decryptor;
    }

    public ISAACCipher getDecryptor()
    {
        return decryptor;
    }

    public void setLoginStage(LoginStages loginStage)
    {
        this.loginStage = loginStage;
    }

    public LoginStages getLoginStage()
    {
        return loginStage;
    }

    public void setLogin(Login login)
    {
        this.login = login;
    }

    public Login getLogin()
    {
        return login;
    }

    public void setOpcode(int opcode)
    {
        this.opcode = (short) opcode;
    }

    public int getOpcode()
    {
        return opcode;
    }

    public int getPacketLength()
    {
        return packetLength;
    }

    public void setPacketLength(int packetLength)
    {
        this.packetLength = (short) packetLength;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getUsername()
    {
        return username;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPrayerIcon(int prayerIcon)
    {
        this.prayerIcon = prayerIcon;
    }

    public int getPrayerIcon()
    {
        return prayerIcon;
    }

    public void setSkulled(boolean isSkulled)
    {
        if (isSkulled)
        {
            setSkullIcon(0);
            setSkullTimer(2000);
            if (getSkullTimerTask() == null)
            {
                setSkullTimerTask(new SkullTimerTask(this));
                World.submit(getSkullTimerTask());
            }
        }
        else
        {
            setSkullIcon(-1);
            if (getSkullTimerTask() != null)
            {
                getSkullTimerTask().stop();
                setSkullTimerTask(null);
                setSkullTimer(0);
            }
        }
        this.isSkulled = isSkulled;
        setAppearanceUpdateRequired(true);
    }

    public boolean isSkulled()
    {
        return isSkulled;
    }

    public void setSkullIcon(int skullIcon)
    {
        this.skullIcon = skullIcon;
    }

    public int getSkullIcon()
    {
        return skullIcon;
    }

    public void setIsUsingPrayer(boolean[] isUsingPrayer)
    {
        this.isUsingPrayer = isUsingPrayer;
    }

    public boolean[] getActivePrayers()
    {
        return isUsingPrayer;
    }

    public boolean shouldAutoRetaliate()
    {
        return autoRetaliate;
    }

    public void setAutoRetaliate(boolean autoRetaliate)
    {
        this.autoRetaliate = autoRetaliate;
    }

    public void setMagicBookType(MagicBookTypes magicBookType)
    {
        this.magicBookType = magicBookType;
    }

    public MagicBookTypes getMagicBookType()
    {
        return magicBookType;
    }

    public void setSpecialAmount(double specialAmount)
    {
        if (specialAmount < this.specialAmount && specialAmount < 10.0)
        {
            if (getSpecialRefillTask() == null)
            {
                setSpecialRefillTask(new SpecialRefillTask(this));
                World.submit(getSpecialRefillTask());
            }
        }
        else if (specialAmount > 9.9)
        {
            if (getSpecialRefillTask() != null)
            {
                getSpecialRefillTask().stop();
                setSpecialRefillTask(null);
            }
        }
        this.specialAmount = specialAmount;
        if (isLoggedIn())
            getActionSender().updateSpecialBar();
    }

    public double getSpecialAmount()
    {
        return specialAmount;
    }

    public boolean isSpecialAttackActive()
    {
        return specialAttackActive;
    }

    public void setSpecialAttackActive(boolean specialAttackActive)
    {
        this.specialAttackActive = specialAttackActive;
    }

    public void setTrading(TradeManager trading)
    {
        this.trading = trading;
    }

    public TradeManager getTrading()
    {
        return trading;
    }

    public enum MagicBookTypes
    {
        MODERN, ANCIENT, LUNAR
    }

    public enum TradeStage
    {
        WAITING, SEND_REQUEST, ACCEPT, SEND_REQUEST_ACCEPT, SECOND_TRADE_WINDOW
    }

    public enum TeleotherStage
    {
        WAITING, SEND_REQUEST, RECEIVED_REQUEST
    }

    public enum LoginStages
    {
        CONNECTED, LOGGING_IN, AWAITING_LOGIN_COMPLETE, LOGGED_IN, LOGGING_OUT, LOGGED_OUT
    }

    public Task getSpecialRefillTask()
    {
        return specialRefillTask;
    }

    public void setSpecialRefillTask(Task specialRefillTask)
    {
        this.specialRefillTask = specialRefillTask;
    }

    /**
     * @return the prayerUpdateTick
     */
    public Task getPrayerDrainTask()
    {
        return prayerDrainTask;
    }

    /**
     * @param prayerUpdateTick the prayerUpdateTick to set
     */
    public void setPrayerDrainTask(Task prayerDrainTask)
    {
        this.prayerDrainTask = prayerDrainTask;
    }

    public void setSkullTimerTask(Task skullTimerTask)
    {
        this.skullTimerTask = skullTimerTask;
    }

    public Task getSkullTimerTask()
    {
        return skullTimerTask;
    }

    public void setSkullTimer(int skullTimer)
    {
        this.skullTimer = skullTimer;
    }

    public int getSkullTimer()
    {
        return skullTimer;
    }

    public int getHumanToNpc()
    {
        return humanToNpc;
    }

    public void setHumanToNpc(int humanToNpc)
    {
        this.humanToNpc = humanToNpc;
    }

    public String getHost()
    {
        return host;
    }

    public long getUsernameAsLong()
    {
        return usernameAsLong;
    }

    public void setUsernameAsLong(long usernameAsLong)
    {
        this.usernameAsLong = usernameAsLong;
    }

    public void setTempPassword(String tempPassword)
    {
        this.tempPassword = tempPassword;
    }

    public String getTempPassword()
    {
        return tempPassword;
    }

    public TeleotherStage getTeleotherStage()
    {
        return teleotherStage;
    }

    public void setTeleotherStage(TeleotherStage teleotherStage)
    {
        this.teleotherStage = teleotherStage;
    }

    public void setLastLoginDay(String lastLoginDay)
    {
        this.lastLoginDay = lastLoginDay;
    }

    public String getLastLoginDay()
    {
        return lastLoginDay;
    }

    public void setLastIp(String lastIp)
    {
        this.lastIp = lastIp;
    }

    public String getLastIp()
    {
        return lastIp;
    }

    public QuestStorage getQuestStorage()
    {
        return questStorage;
    }

    /**
     * Used for testing purposes - <Ares>
     * 
     * @return
     */
    public boolean isDebugging()
    {
        return ((Boolean) getAttribute("isDebugging") != null);
    }

    /**
     * @return the minigame
     */
    public Minigame<Entity> getMinigame()
    {
        return minigame;
    }

    /**
     * @param minigame the minigame to set
     */
    public void setMinigame(Minigame<Entity> minigame)
    {
        this.minigame = minigame;
    }

    /**
     * @return the pizazz
     */
    public int getAlchemyPizazz()
    {
        return alchemyPizazz;
    }

    /**
     * @param pizazz the pizazz to set
     */
    public void setAlchemyPizazz(int pizazz)
    {
        this.alchemyPizazz = pizazz;
    }

    public int getEnchantPizazz()
    {
        return enchantPizazz;
    }

    public void setEnchantPizazz(int enchantPizazz)
    {
        this.enchantPizazz = enchantPizazz;
    }

    public int getGravePizazz()
    {
        return gravePizazz;
    }

    public void setGravePizazz(int gravePizazz)
    {
        this.gravePizazz = gravePizazz;
    }

    public int getTelePizazz()
    {
        return telePizazz;
    }

    public void setTelePizazz(int telePizazz)
    {
        this.telePizazz = telePizazz;
    }

    public void setRights(Rights rights)
    {
        this.rights = rights;
    }

    public int getRights()
    {
        return rights.ordinal();
    }
}
