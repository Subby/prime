package com.asgarniars.rs2.model.players;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.io.XStreamController;
import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.util.clip.RSObject;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class GlobalObjectHandler
{

    private static List<GlobalObject> objects = new ArrayList<GlobalObject>();

    private static final Logger logger = Logger.getLogger(GlobalObjectHandler.class.getName());

    @SuppressWarnings("unchecked")
    public static void loadObjects() throws FileNotFoundException
    {
        List<GlobalObject> list = (List<GlobalObject>) XStreamController.getXStream().fromXML(new FileInputStream(Constants.CONFIG_DIRECTORY + "content/objects.xml"));
        for (GlobalObject object : list)
        {
            objects.add(object);
        }
        logger.info("Loaded " + list.size() + " objects.");
    }

    /**
     * Creates an object for everyone online.
     * 
     * @param player
     */
    public static void createGlobalObject(Player player)
    {
        for (Player players : World.getPlayers())
        {
            if (players == null)
            {
                continue;
            }
            for (GlobalObject object : objects)
            {
                if (object.getPosition().isWithinDistance(player.getPosition()))
                {
                    players.getActionSender().sendObject(object);
                    // logger.info("New object created at " +
                    // object.getPosition() + " by " + player.getUsername());
                }
            }
        }
    }

    /**
     * 
     * @param player
     * @param object
     */
    public static void createGlobalObject(Player player, GlobalObject object)
    {
        for (Player players : World.getPlayers())
        {
            if (players == null)
            {
                continue;
            }
            if (object.getPosition().isWithinDistance(player.getPosition()))
            {
                players.getActionSender().sendObject(object);
                // logger.info("New object created at " + object.getPosition() +
                // " by " + player.getUsername());
            }
        }

    }

    /**
     * 
     * @param player
     * @param object
     */
    public static void createGlobalObject(Player player, RSObject object)
    {
        for (Player players : World.getPlayers())
        {
            if (players == null)
            {
                continue;
            }
            if (object.getPosition().isWithinDistance(player.getPosition()))
            {
                players.getActionSender().sendObject(object);
                // logger.info("New object created at " + object.getPosition() +
                // " by " + player.getUsername());
            }
        }

    }

    /**
     * Creates an object just for yourself. Specified the object id, position,
     * face, and type
     * 
     * @param player
     */
    public static void createObject(Player player, int id, Position position, int face, int type)
    {
        player.getActionSender().sendObject(new GlobalObject(id, position, face, type));
    }

    public static void setObjects(List<GlobalObject> objects)
    {
        GlobalObjectHandler.objects = objects;
    }

    public static List<GlobalObject> getObjects()
    {
        return objects;
    }

}