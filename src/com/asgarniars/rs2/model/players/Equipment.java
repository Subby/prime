package com.asgarniars.rs2.model.players;

import java.util.Arrays;
import java.util.logging.Logger;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.content.combat.Combat;
import com.asgarniars.rs2.content.combat.magic.Magic;
import com.asgarniars.rs2.content.combat.util.SpecialAttack;
import com.asgarniars.rs2.content.combat.util.Weapons;
import com.asgarniars.rs2.content.combat.util.Bonuses.BonusDefinition;
import com.asgarniars.rs2.model.items.Item;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.model.players.container.Container;
import com.asgarniars.rs2.model.players.container.ContainerType;
import com.asgarniars.rs2.util.Language;

/**
 * EQUIPMENT SLOTS MUST BE FILLED IN ORDER FROM 0 TO HIGHEST ID, OTHERWISE IT
 * WILL FUCK UP AND DON'T FIND THE SLOT
 * 
 * @author AkZu
 * 
 */
public class Equipment
{

    private static final Logger logger = Logger.getLogger(Equipment.class.getName());

    public static final int EQUIPMENT_INTERFACE = 1688;
    public static final int SIZE = 14;
    private Player player;
    /**
     * Our container system, fixed by Barry
     */
    private Container itemContainer = new Container(ContainerType.STANDARD, SIZE);

    public Equipment(Player player)
    {
        this.player = player;
    }

    public static final int[] CAPES =
    { 0, 1007, 1019, 1021, 1023, 1027, 1029, 1031, 1052, 2412, 2413, 2414, 3759, 3761, 3763, 3765, 3777, 3779, 3781, 3783, 3785, 3787, 3789, 4285, 4304, 4315, 4317, 4319, 4321, 4323, 4325, 4327, 4329, 4331, 4333, 4335, 4337, 4339, 4341, 4343, 4345, 4347, 4349, 4351, 4353, 4355, 4357, 4359, 4361, 4363, 4365, 4367, 4369, 4371, 4373, 4375, 4377, 4379, 4381, 4383, 4385, 4387, 4389, 4391, 4393, 4395, 4397, 4399, 4401, 4403, 4405, 4407, 4409, 4411, 4413, 4514, 4516, 5607, 6070, 6111, 6568, 6570, 6959, 7535, 7918, 9074, 9101, 9747, 9748, 9750, 9751, 9753, 9754, 9756, 9757, 9759, 9760, 9762, 9763, 9765, 9766, 9768, 9769, 9771, 9772, 9774, 9775, 9777, 9778, 9780, 9781, 9783, 9784, 9786, 9787, 9789, 9790, 9792, 9793, 9795, 9796, 9798, 9799, 9801, 9802, 9804, 9805, 9807, 9808, 9810, 9811, 9813, 9948, 9949, 10069, 10071, 10073, 10074, 10171, 10494, 10499, 10556, 10558, 10559, 10566 };
    public static final int[] BOOTS =
    { 0, 88, 89, 626, 628, 630, 632, 634, 1061, 1837, 1846, 2577, 2579, 2894, 2904, 2914, 2924, 2934, 3061, 3105, 3107, 3393, 3791, 4097, 4107, 4117, 4119, 4121, 4123, 4125, 4127, 4119, 4121, 4123, 4125, 4127, 4129, 4131, 4119, 4121, 4123, 4125, 4127, 4129, 4131, 4310, 5064, 5345, 5557, 6069, 6106, 6143, 6145, 6147, 6328, 6349, 6357, 6367, 6377, 6619, 6666, 6790, 6893, 6920, 7114, 7159, 7596, 9005, 9006, 9073, 9100, 9638, 9644, 9921, 10839, 10552, 10607, 10865, 10933, 10958, 11732 };
    public static final int[] GLOVES =
    { 0, 775, 776, 777, 778, 1059, 1063, 1065, 1495, 1580, 2487, 2489, 2491, 2902, 2912, 2922, 2932, 2942, 3060, 3391, 3799, 4095, 4105, 4115, 4308, 5556, 6068, 6110, 6149, 6151, 6153, 6330, 6347, 6359, 6369, 6379, 6629, 6720, 6922, 7453, 7454, 7455, 7456, 7457, 7458, 7459, 7460, 7461, 7462, 7595, 7671, 7673, 8842, 8929, 9072, 9099, 9922, 10023, 10024, 10075, 10077, 10079, 10081, 10083, 10085, 10336, 10376, 10384, 10368, 10553, 10554, 10725 };
    public static final int[] SHIELDS =
    { 0, 1171, 1173, 1175, 1177, 1179, 1181, 1183, 1185, 1187, 1189, 1191, 1193, 1195, 1197, 1187, 1189, 1191, 1193, 1195, 1197, 1199, 1201, 1540, 2589, 2597, 2603, 2611, 2621, 2629, 2659, 2667, 2675, 2890, 3122, 3488, 3758, 3839, 3840, 3841, 3842, 3843, 3844, 4072, 4156, 4224, 4225, 4226, 4227, 4228, 4229, 4230, 4231, 4232, 4233, 4234, 4507, 4512, 5609, 6215, 6217, 6219, 6221, 6223, 6225, 6227, 6229, 6231, 6233, 6235, 6237, 6239, 6241, 6243, 6245, 6247, 6249, 6251, 6253, 6255, 6257, 6259, 6261, 6263, 6265, 6267, 6269, 6271, 6273, 6275, 6277, 6279, 6524, 6631, 6633, 6889, 6894, 7051, 7053, 7332, 7334, 7336, 7338, 7340, 7342, 7344, 7346, 7348, 7350, 7352, 7344, 7346, 7348, 7350, 7352, 7354, 7356, 7358, 7360, 7676, 8714, 8716, 8718, 8720, 8722, 8724, 8726, 8728, 8730, 8732, 8734, 8736, 8738, 8740, 8742, 8744, 8746, 8748, 8750, 8752, 8754, 8756, 8758, 8760, 8762, 8764, 8766, 8768, 8770, 8772, 8774, 8776, 8844, 8845, 8846, 8847, 8848, 8849, 8850, 8856, 9704, 9731, 10352, 11283, 11284 };
    public static final int[] HATS =
    { 0, 579, 656, 658, 660, 662, 664, 740, 1017, 1025, 1037, 1038, 1040, 1042, 1044, 1046, 1048, 1050, 1949, 2422, 2581, 2631, 2633, 2635, 2637, 2639, 2641, 2643, 2645, 2647, 2649, 2651, 2900, 2910, 2920, 2930, 2940, 2978, 2979, 2980, 2981, 2982, 2983, 2984, 2985, 2986, 2987, 2988, 2989, 2990, 2991, 2992, 2993, 2994, 2995, 3327, 3329, 3331, 3333, 3335, 3337, 3339, 3341, 3343, 3385, 3797, 4089, 4099, 4109, 4166, 4168, 5013, 5014, 5525, 5527, 5529, 5531, 5533, 5535, 5537, 5539, 5541, 5543, 5545, 5547, 5549, 5551, 6182, 6326, 6335, 6336, 6337, 6338, 6339, 6340, 6345, 6355, 6365, 6375, 6382, 6547, 6548, 6656, 6665, 6856, 6858, 6860, 6862, 6885, 6886, 6887, 6918, 7319, 7321, 7323, 7325, 7327, 7319, 7321, 7323, 7325, 7327, 7394, 7396, 7917, 8901, 8903, 8905, 8907, 8909, 8911, 8913, 8915, 8917, 8919, 8921, 8928, 8949, 8950, 8959, 8960, 8961, 8962, 8963, 8964, 8965, 8928, 9069, 9106, 9472, 9729, 9733, 10172, 10547, 10548, 10549, 10550, 10608, 10836, 10862, 10883 };
    public static final int[] AMULETS =
    { 0, 86, 87, 295, 421, 552, 589, 616, 774, 1009, 1478, 1575, 1654, 1656, 1658, 1660, 1662, 1664, 1692, 1694, 1696, 1698, 1700, 1702, 1704, 1706, 1708, 1710, 1712, 1716, 1718, 1722, 1724, 1725, 1727, 1729, 1731, 1796, 1797, 2406, 3208, 3853, 3855, 3857, 3859, 3861, 3863, 3865, 3867, 4021, 4035, 4081, 4250, 4306, 4677, 5521, 6040, 6041, 6208, 6544, 6577, 6581, 6585, 6707, 6857, 6859, 6861, 6863, 7803, 8932, 9083, 9102, 9470, 10132, 10344, 10354, 10356, 10358, 10360, 10362, 10364, 10366, 10500, 10588, 11128 };
    public static final int[] ARROWS =
    { 0, 78, 598, 877, 878, 879, 880, 881, 882, 883, 884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 881, 942, 2532, 2533, 2534, 2535, 2536, 2537, 2538, 2539, 2540, 2541, 2866, 4160, 4172, 4173, 4174, 4175, 4740, 4773, 4778, 4783, 4788, 4793, 4798, 4803, 5616, 5617, 5618, 5619, 5620, 5621, 5622, 5623, 5624, 5625, 5626, 5627, 6061, 6062, 8882, 9139, 9140, 9141, 9142, 9143, 9144, 9145, 9236, 9237, 9238, 9239, 9240, 9241, 9242, 9243, 9244, 9245, 9286, 9287, 9288, 9289, 9290, 9291, 9292, 9293, 9294, 9295, 9296, 9297, 9298, 9299, 9300, 9301, 9302, 9303, 9304, 9305, 9306, 9335, 9336, 9337, 9338, 9339, 9340, 9341, 9342, 9419, 9706, 10142, 10143, 10144, 10145, 10158, 10159, 11212, 11227, 11228, 11229 };
    public static final int[] RINGS =
    { 0, 773, 1635, 1637, 1639, 1641, 1643, 1645, 2550, 2552, 2554, 2556, 2558, 2560, 2562, 2564, 2566, 2568, 2570, 2572, 4202, 4657, 6465, 6575, 6583, 6731, 6733, 6735, 6737, 7927, 9104 };
    public static final int[] BODY =
    { 0, 1005, 1101, 1103, 1105, 1107, 1109, 1111, 1113, 1129, 1131, 1133, 1135, 1757, 2499, 2501, 2503, 6615, 7110, 7122, 7128, 7134, 7362, 7364, 7370, 7372, 7374, 7376, 7592 };
    public static final int[] LEGS =
    { 0, 285, 428, 538, 542, 548, 646, 648, 650, 652, 654, 1011, 1013, 1015, 1033, 1067, 1069, 1071, 1073, 1075, 1077, 1079, 1081, 1083, 1085, 1087, 1089, 1091, 1093, 1095, 1097, 1099, 1835, 1845, 2493, 2495, 2497, 2585, 2593, 2601, 2609, 2617, 2625, 2655, 2663, 2671, 2898, 2908, 2918, 2928, 2938, 3059, 3389, 3472, 3473, 3474, 3475, 3476, 3477, 3478, 3479, 3480, 3483, 3485, 3472, 3473, 3474, 3475, 3476, 3477, 3478, 3479, 3480, 3485, 3795, 4070, 4087, 4093, 4103, 4113, 4180, 4300, 4505, 4510, 4585, 4714, 4722, 4730, 4738, 4751, 4759, 4874, 4875, 4876, 4877, 4898, 4899, 4900, 4901, 4922, 4923, 4924, 4925, 4946, 4947, 4948, 4949, 4970, 4971, 4972, 4973, 4994, 4995, 4996, 4997, 5036, 5038, 5040, 5042, 5044, 5046, 5048, 5050, 5052, 5555, 5576, 6108, 6067, 6130, 6135, 6141, 6181, 6185, 6187, 6324, 6343, 6353, 6363, 6373, 6386, 6390, 6396, 6398, 6404, 6406, 6625, 6627, 6655, 6752, 6787, 6809, 6924, 7116, 7126, 7132, 7138, 7366, 7368, 7378, 7380, 7382, 7384, 7386, 7388, 7398, 7593, 8840, 8991, 8992, 8993, 8994, 8995, 8996, 8997, 9071, 9098, 9636, 9642, 9676, 9678, 9923, 10035, 10041, 10047, 10055, 10059, 10063, 10067, 10332, 10340, 10346, 10372, 10380, 10388, 10394, 10396, 10402, 10406, 10410, 10414, 10418, 10422, 10426, 10430, 10434, 10438, 10464, 10466, 10468, 10555, 10838, 10864, 10940, 11722, 11726 };
    public static final int[] PLATEBODY =
    { 0, 75, 284, 426, 430, 540, 544, 546, 577, 581, 636, 638, 640, 642, 644, 1035, 1115, 1117, 1119, 1121, 1123, 1125, 1127, 1833, 1844, 2405, 2583, 2591, 2599, 2607, 2615, 2623, 2653, 2661, 2669, 2896, 2906, 2916, 2926, 2936, 3058, 3140, 3387, 3481, 3767, 3769, 3771, 3773, 3775, 3793, 4069, 4091, 4101, 4111, 4298, 4504, 4509, 4712, 4720, 4728, 4736, 4749, 4757, 4868, 4869, 4870, 4871, 4892, 4893, 4894, 4895, 4916, 4917, 4918, 4919, 4940, 4941, 4942, 4943, 4964, 4965, 4966, 4967, 4988, 4989, 4990, 4991, 5024, 5026, 5028, 5030, 5032, 5034, 5553, 5575, 6065, 6107, 6129, 6133, 6139, 6180, 6184, 6186, 6322, 6341, 6351, 6361, 6371, 6384, 6388, 6394, 6402, 6617, 6654, 6750, 6786, 6916, 7390, 7392, 7399, 8839, 8952, 8953, 8954, 8955, 8956, 8957, 8958, 9070, 9097, 9634, 9640, 9674, 9924, 9944, 10037, 10043, 10049, 10053, 10057, 10061, 10065, 10316, 10318, 10320, 10322, 10324, 10330, 10338, 10348, 10370, 10378, 10386, 10400, 10404, 10408, 10412, 10416, 10420, 10424, 10428, 10432, 10436, 10458, 10460, 10462, 10551, 10564, 10605, 10683, 10837, 10939, 10956, 10945, 10954, 11720, 11724 };
    public static int[] HELM_NO_HAIR =
    { 0, 74, 1137, 1139, 1141, 1143, 1145, 1147, 1149, 1151, 1167, 1169, 3748, 3749, 3751, 3753, 3755, 4041, 4042, 4071, 4302, 4502, 4506, 4511, 4513, 4515, 4567, 4708, 4716, 4856, 4857, 4858, 4859, 4880, 4881, 4882, 4883, 6109, 6128, 6131, 6137, 6392, 6400, 6621, 7003, 7112, 7124, 7130, 7136, 7400, 7539, 8924, 8926, 8927, 9749, 9752, 9755, 9758, 9761, 9764, 9767, 9770, 9773, 9776, 9779, 9782, 9785, 9788, 9791, 9794, 9797, 9800, 9803, 9806, 9809, 9812, 9814, 9925, 9945, 9946, 9950, 10045, 10051, 10286, 10288, 10290, 10292, 10294, 10296, 10298, 10300, 10302, 10304, 10306, 10308, 10310, 10312, 10314, 10334, 10342, 10350, 10374, 10382, 10390, 10392, 10398, 10452, 10454, 10456, 10507, 10589, 10828, 10941, 11663, 11664, 11665 };
    public static final int[] HELM_NO_BEARD =
    { 0, 3057, 4164 };
    public static final int[] FULL_MASK =
    { 0, 1053, 1055, 1057, 1153, 1155, 1157, 1159, 1161, 1163, 1165, 1506, 2587, 2595, 2605, 2613, 2619, 2627, 2657, 2665, 2673, 3486, 4551, 4611, 4724, 4732, 4745, 4753, 4904, 4905, 4906, 4907, 4928, 4929, 4930, 4931, 4952, 4953, 4954, 4955, 4976, 4977, 4978, 4979, 5554, 5574, 6188, 6623, 7534, 7594, 8464, 8466, 8468, 8470, 8472, 8474, 8476, 8478, 8480, 8482, 8484, 8486, 8488, 8490, 8492, 8494, 8682, 8684, 8686, 8688, 8690, 8692, 8694, 8696, 8698, 8700, 8702, 8704, 8706, 8708, 8710, 8712, 9096, 9629, 9672, 9920, 10039, 10609, 11718 };

    public static final int[] UNTRADEABLE_ITEMS =
    { 0, 1037, 1419, 2412, 2413, 2414, 2415, 2416, 2417, 4079, 4566, 6570, 6722, 6856, 6857, 6858, 6859, 6860, 6861, 6862, 6863, 6864, 6865, 6866, 6867, 6868, 6869, 6870, 6871, 6872, 6873, 6874, 6875, 6876, 6877, 6878, 6879, 6880, 6881, 6882, 7453, 7454, 7455, 7456, 7457, 7458, 7459, 7460, 7461, 7462, 7927, 8844, 8845, 8846, 8847, 8848, 8849, 8850, 9747, 9748, 9749, 9750, 9751, 9752, 9753, 9754, 9755, 9756, 9757, 9758, 9759, 9760, 9761, 9762, 9763, 9764, 9765, 9766, 9767, 9768, 9769, 9770, 9771, 9772, 9773, 9774, 9775, 9776, 9777, 9778, 9779, 9780, 9781, 9782, 9783, 9784, 9785, 9786, 9787, 9788, 9789, 9790, 9791, 9792, 9793, 9794, 9795, 9796, 9797, 9798, 9799, 9800, 9801, 9802, 9803, 9804, 9805, 9806, 9807, 9808, 9809, 9810, 9811, 9812, 9813, 9814, 9920, 9921, 9922, 9923, 9924, 9925, 10507, 10508, 10551 };
    /**
     * Array containing all the destroyable items in the 459 engine.cache.
     */
    public static final int[] UNDROPPABLE_ITEMS =
    { 0, 1, 3, 456, 457, 458, 459, 460, 461, 462, 463, 616, 671, 672, 673, 763, 765, 769, 775, 776, 777, 778, 1458, 2372, 2373, 2374, 2375, 2376, 2377, 2378, 2379, 2380, 2381, 2382, 2383, 2384, 2389, 2390, 2393, 2399, 2400, 2401, 3698, 3722, 3723, 3724, 3725, 3726, 3727, 3728, 3729, 3730, 3731, 3732, 3733, 5545, 6112, 6113, 6118, 6453, 6454, 6455, 6456, 6457, 6458, 6459, 6460, 6461, 6462, 6463, 6464, 6465, 6478, 6479, 6541, 6542, 6543, 6544, 6545, 6546, 6547, 6548, 6635, 6639, 6640, 6641, 6642, 6643, 6644, 6645, 6646, 6647, 6648, 6653, 6754, 6755, 6756, 6757, 6758, 6759, 6769, 6770, 6817, 6818, 6819, 6820, 6821, 6854, 6855, 6856, 6857, 6858, 6859, 6860, 6861, 6862, 6863, 6865, 6866, 6867, 6885, 6886, 6887, 6891, 6945, 6946, 6947, 6948, 6949, 6950, 6951, 6952, 6953, 6954, 6955, 6956, 6957, 6958, 6985, 6986, 6987, 6988, 6989, 6990, 6991, 6992, 6993, 6994, 6995, 6996, 6997, 6998, 6999, 7000, 7001, 7002, 7408, 7409, 7410, 7411, 7470, 7471, 7473, 7474, 7475, 7476, 7477, 7478, 7479, 7498, 7542, 7543, 7544, 7545, 7546, 7628, 7629, 7630, 7632, 7633, 7634, 7635, 7649, 7774, 7775, 7776, 7958, 7959, 7966, 7968, 9005, 9006, 9013, 9025, 9076, 9077, 9083, 9084, 9085, 9086, 9087, 9088, 9089, 9090, 9091, 9092, 9093, 9096, 9097, 9098, 9099, 9100, 9101, 9102, 9103, 9104, 9433, 9474, 9589, 9625, 9626, 9633, 9646, 9647, 9648, 9649, 9651, 9652, 9653, 9654, 9655, 9656, 9657, 9658, 9659, 9660, 9662, 9682, 9684, 9685, 9686, 9687, 9688, 9689, 9690, 9691, 9692, 9693, 9694, 9695, 9696, 9697, 9698, 9699, 9702, 9703, 9704, 9705, 9706, 9901, 9902, 9903, 9906, 9907, 9908, 9909, 9910, 9911, 9912, 9913, 9914, 9915, 9916, 9917, 9918, 9919, 9920, 9921, 9922, 9923, 9924, 9925, 9932, 9944, 9945, 9947, 10174, 10175, 10176, 10177, 10487, 10488, 10489, 10490, 10491, 10492, 10493, 10494, 10495, 10498, 10499, 10500, 10507, 10508, 10512, 10513, 10514, 10515, 10531, 10532, 10533, 10539, 10540, 10541, 10542, 10543, 10544, 10545, 10546, 10547, 10548, 10549, 10550, 10551, 10552, 10553, 10554, 10555, 10562, 10581, 10582, 10583, 10584, 10585, 10586, 10587, 10595, 10596, 10609, 10722, 10723, 10724, 10725, 10726, 10727, 10728, 10730, 10829, 10830, 10831, 10832, 10833, 10834, 10835, 10836, 10837, 10838, 10839, 10842, 10856, 10857, 10858, 10862, 10863, 10864, 10865, 10883, 10887, 10889, 10890, 10934, 10935, 10936, 10942, 10943, 10944, 10983, 10984, 10985, 10986, 10987, 10988, 10989, 10990, 10991, 10992, 10993, 10994, 11003, 11006, 11007, 11008, 11009, 11010, 11012, 11013, 11014, 11019, 11020, 11021, 11022, 11023, 11024, 11027, 11028, 11029, 11030, 11031, 11032, 11033, 11034, 11035, 11036, 11039, 11040, 11041, 11042, 11043, 11045, 11136, 11137, 11138, 11139, 11140, 11141, 11151, 11152, 11153, 11154, 11155, 11156, 11157, 11158, 11159, 11173, 11174, 11175, 11185, 11186, 11187, 11188, 11189, 11196, 11197, 11198, 11199, 11202, 11203, 11204, 11210, 11211, 11238, 11240, 11242, 11244, 11246, 11248, 11250, 11252, 11254, 11256, 11258, 11259, 11273, 11279 };

    public void refresh()
    {
        Item[] items = itemContainer.toArray();
        player.getActionSender().sendUpdateItems(EQUIPMENT_INTERFACE, items);
        setBonus();
        sendWeaponInterface();
    }

    public void refresh(int slot, Item item)
    {
        player.getActionSender().sendUpdateItem(slot, EQUIPMENT_INTERFACE, item);
    }

    public void equip(int slot)
    {

        // The item we're trying to equip.
        Item itemToEquip = player.getInventory().get(slot);

        // If item not found, return.
        if (itemToEquip == null)
            return;

        /*
         * if (BonusDefinition.getBonus(item.getId()) == null) { try {
         * RuneWiki.getBonuses(item.getId(), false); } catch (Exception e) { }
         * player.getActionSender().sendMessage( "There's no bonus for the " +
         * ItemManager.getItemDefinitions()[item.getId()].getName() +
         * " you just equipped."); player.getActionSender().sendMessage(
         * "Trying to dump item data from rune wiki..."); }
         */

        // TODO
        /*
         * if (!(Boolean) player.getAttribute("canTeleport")) { return; }
         */
        // check level reqs, TODO make more efficiently
        // boolean canWear = true;
        // int skillId = 0, skillId2 = 0, levelReq = 0, levelReq2 = 0;
        /*
         * if (LevelRequirementLoader.getRequirement(item.getId()) != null) {
         * for (int index = 0; index <
         * LevelRequirementLoader.getRequirement(item
         * .getId()).getLevelId().length; index++) { if
         * (player.getSkill().getLevelForXP(
         * player.getSkill().getExp()[LevelRequirementLoader
         * .getRequirement(item.getId()) .getLevelId()[index]]) <
         * LevelRequirementLoader.getRequirement(item.getId())
         * .getRequiredLevel()[index]) { if (player.getSkill().getLevelForXP(
         * player
         * .getSkill().getExp()[LevelRequirementLoader.getRequirement(item.
         * getId()) .getLevelId()[0]]) <
         * LevelRequirementLoader.getRequirement(item.getId())
         * .getRequiredLevel()[0]) { skillId =
         * LevelRequirementLoader.getRequirement(item.getId()).getLevelId()[0];
         * levelReq =
         * LevelRequirementLoader.getRequirement(item.getId()).getRequiredLevel
         * ()[0]; } if
         * (LevelRequirementLoader.getRequirement(item.getId()).getLevelId
         * ().length == 2 && player.getSkill().getLevelForXP(
         * player.getSkill().getExp()[LevelRequirementLoader.getRequirement(
         * item.getId()).getLevelId()[1]]) < LevelRequirementLoader
         * .getRequirement(item.getId()).getRequiredLevel()[1]) { skillId2 =
         * LevelRequirementLoader.getRequirement(item.getId()).getLevelId()[1];
         * levelReq2 = LevelRequirementLoader.getRequirement(item.getId())
         * .getRequiredLevel()[1]; } canWear = false; } } } if (!canWear) { if
         * (levelReq != 0) player.getActionSender().sendMessage(
         * "You need to have a " + Skill.SKILL_NAME[skillId] + " level of " +
         * levelReq + (levelReq2 == 0 ? "." : " and a " +
         * Skill.SKILL_NAME[skillId2] + " level of " + levelReq2)); else
         * player.getActionSender().sendMessage( "You need to have a " +
         * Skill.SKILL_NAME[skillId2] + " level of " + levelReq2 + "."); return;
         * }
         */

        // Define the slot type
        int slotType = getEquipmentSlot(itemToEquip.getId());

        if (player.getAttribute("duel_button_helmet") != null && slotType == 0)
        {
            player.getActionSender().sendMessage("You're not allowed to use head gear in this duel session.");
            return;
        }
        if (player.getAttribute("duel_button_cape") != null && slotType == 1)
        {
            player.getActionSender().sendMessage("You're not allowed to use the cape slot in this duel session.");
            return;
        }
        if (player.getAttribute("duel_button_amulet") != null && slotType == 2)
        {
            player.getActionSender().sendMessage("You're not allowed to use the amulet slot in this duel session.");
            return;
        }
        if (player.getAttribute("duel_button_arrows") != null && slotType == 13)
        {
            player.getActionSender().sendMessage("You're not allowed to use the quiver slot in this duel session.");
            return;
        }
        if (player.getAttribute("duel_button_weapon") != null && slotType == 3)
        {
            player.getActionSender().sendMessage("You're not allowed to use weapons in this duel session.");
            return;
        }
        if (player.getAttribute("duel_button_chest") != null && slotType == 4)
        {
            player.getActionSender().sendMessage("You're not allowed to use body equipment in this duel session.");
            return;
        }
        if (player.getAttribute("duel_button_shield") != null && slotType == 5)
        {
            player.getActionSender().sendMessage("You're not allowed to use shields in this duel session.");
            return;
        }
        if (player.getAttribute("duel_button_legs") != null && slotType == 7)
        {
            player.getActionSender().sendMessage("You're not allowed to use leg equipment in this duel session.");
            return;
        }
        if (player.getAttribute("duel_button_gloves") != null && slotType == 9)
        {
            player.getActionSender().sendMessage("You're not allowed to use gloves in this duel session.");
            return;
        }
        if (player.getAttribute("duel_button_boots") != null && slotType == 10)
        {
            player.getActionSender().sendMessage("You're not allowed to use boots in this duel session.");
            return;
        }
        if (player.getAttribute("duel_button_ring") != null && slotType == 12)
        {
            player.getActionSender().sendMessage("You're not allowed to use rings in this duel session.");
            return;
        }

        // The item wich could be equiped, if it exists in the inventory
        // container too (mainly used for stacking checks).
        Item invItem = (get(slotType) != null ? (player.getInventory().getItemContainer().getById(get(slotType).getId()) != null && player.getInventory().getItemContainer().getById(get(slotType).getId()).getDefinition().isStackable() ? player.getInventory().getItemContainer().getById(get(slotType).getId()) : null) : null);

        // The item we possibly have equiped
        Item equipedItem = get(slotType);

        // The amount of the item we equip (only applies to stackables)
        int amount = itemToEquip.getCount();

        // Stack amount checking for same items such as Rune darts
        if (equipedItem != null && equipedItem.getId() == itemToEquip.getId() && equipedItem.getDefinition().isStackable())
        {

            amount = Constants.MAX_ITEM_COUNT - equipedItem.getCount();

            if (amount == 0)
            {
                player.getActionSender().sendMessage("You don't have enough space to equip that.");
                return;
            }

            if (amount > itemToEquip.getCount())
                amount = itemToEquip.getCount();

            // And finally do these
            player.getInventory().removeItem(new Item(itemToEquip.getId(), amount));
            itemContainer.set(slotType, new Item(itemToEquip.getId(), equipedItem.getCount() + amount));

            doExtraThings();
            refresh(slotType, get(slotType));
            setBonus();
            return;
        }

        // 2h Weapon support.
        if (slotType == 3 && Weapons.isTwoHanded(itemToEquip.getId()) && get(5) != null)
        {

            if (get(3) != null && player.getInventory().getItemContainer().freeSlots() < 1 && invItem == null)
            {
                player.getActionSender().sendMessage("You don't have enough space to equip that.");
                return;
            }

            // Check stacking, for example if user has max amount of rune darts
            // equiped
            // And has 5 in inventory too, and trying to equip a whip it should
            // not allow that.
            if (equipedItem != null && get(3) != null && equipedItem.getDefinition().isStackable() && invItem != null)
            {
                amount = equipedItem.getCount() + invItem.getCount();
                if (amount < 1)
                {
                    player.getActionSender().sendMessage("You don't have enough space to equip that.");
                    return;
                }
            }

            final Item shield = get(5);

            // And finally do these
            itemContainer.remove(shield);
            player.getInventory().removeItemSlot(slot);
            if (equipedItem != null)
            {
                if (equipedItem.getDefinition().isStackable() && invItem != null)
                {
                    player.getInventory().addItem(equipedItem);
                }
                else
                {
                    player.getInventory().addItemToSlot(equipedItem, slot);
                }
            }
            player.getInventory().addItemToSlot(shield, (get(3) == null ? slot : player.getInventory().getItemContainer().freeSlot()));
            itemContainer.set(slotType, itemToEquip);

            doExtraThings();
            refresh(3, get(3));
            Magic.getInstance().resetAll(player, false);
            refresh(5, null);
            setBonus();
            sendWeaponInterface();
            SpecialAttack.getInstance().addSpecialBar(player);
            Combat.getInstance().fixAttackStyles(player, itemToEquip);
            return;
        }

        // 2h Weapon support continuin.
        if (slotType == 5 && get(3) != null && Weapons.isTwoHanded(get(3).getId()))
        {

            if (get(5) != null && player.getInventory().getItemContainer().freeSlots() < 1 && invItem == null)
            {
                player.getActionSender().sendMessage("You don't have enough space to equip that.");
                return;
            }

            // Check stacking, for example if user has max amount of rune darts
            // equiped
            // And has 5 in inventory too, and trying to equip a whip it should
            // not allow that.
            // TODO Shouldnt be any items that are 2h and stackable.
            if (equipedItem != null && get(5) != null && equipedItem.getDefinition().isStackable() && invItem != null)
            {
                amount = equipedItem.getCount() + invItem.getCount();
                if (amount < 1)
                {
                    player.getActionSender().sendMessage("You don't have enough space to equip that.");
                    return;
                }
            }

            final Item weapon = get(3);

            // And finally do these
            itemContainer.remove(weapon);
            player.getInventory().removeItemSlot(slot);
            if (equipedItem != null)
            {
                if (equipedItem.getDefinition().isStackable() && invItem != null)
                {
                    player.getInventory().addItem(equipedItem);
                }
                else
                {
                    player.getInventory().addItemToSlot(equipedItem, slot);
                }
            }
            player.getInventory().addItemToSlot(weapon, (get(5) == null ? slot : player.getInventory().getItemContainer().freeSlot()));
            itemContainer.set(slotType, itemToEquip);

            doExtraThings();
            refresh(5, get(5));
            Magic.getInstance().resetAll(player, false);
            refresh(3, null);
            setBonus();
            sendWeaponInterface();
            Combat.getInstance().fixAttackStyles(player, null);
            return;
        }

        // Check stacking, for example if user has max amount of rune darts
        // equiped
        // And has 5 in inventory too, and trying to equip a whip it should not
        // allow that.
        if (equipedItem != null && equipedItem.getDefinition().isStackable() && invItem != null)
        {

            amount = equipedItem.getCount() + invItem.getCount();

            if (amount < 1)
            {
                player.getActionSender().sendMessage("You don't have enough space to equip that.");
                return;
            }
        }

        // Default actions.
        player.getInventory().removeItemSlot(slot);
        itemContainer.set(slotType, itemToEquip);

        if (equipedItem != null)
        {
            if (equipedItem.getDefinition().isStackable() && invItem != null)
            {
                player.getInventory().addItem(equipedItem);
            }
            else
            {
                player.getInventory().addItemToSlot(equipedItem, slot);
            }
        }

        doExtraThings();
        refresh(slotType, get(slotType));
        setBonus();
        if (slotType == 3)
        {
            sendWeaponInterface();
            Magic.getInstance().resetAll(player, false);
            Combat.getInstance().fixAttackStyles(player, itemToEquip);
            SpecialAttack.getInstance().addSpecialBar(player);
        }

    }

    private void doExtraThings()
    {
        player.setSpecialAttackActive(false);
        player.setAppearanceUpdateRequired(true);
        player.stopAllActions(false);
    }

    public void unequip(int slot)
    {

        // If it's null, return.
        if (get(slot) == null)
            return;

        Item equipedItem = get(slot);

        // Space check
        if (player.getInventory().getItemContainer().freeSlot() == -1)
        {
            if (!player.getInventory().getItemContainer().contains(equipedItem.getId()) && (!(player.getInventory().getItemContainer().getById(equipedItem.getId()) != null && player.getInventory().getItemContainer().getById(equipedItem.getId()).getDefinition().isStackable())))
            {
                player.getActionSender().sendMessage(Language.NO_SPACE);
                return;
            }
        }

        // Amount we have equiped
        int equipAmount = equipedItem.getCount();

        // Checking stacked items..
        if (player.getInventory().getItemContainer().contains(equipedItem.getId()) && player.getInventory().getItemContainer().getById(equipedItem.getId()).getDefinition().isStackable() && !player.getInventory().hasRoomFor(equipedItem))
        {
            player.getActionSender().sendMessage(Language.NO_SPACE);
            return;
        }

        // Remove from equipment container
        itemContainer.remove(new Item(equipedItem.getId(), equipAmount), slot);

        // Add to inventory
        player.getInventory().addItem(new Item(equipedItem.getId(), equipAmount));

        // Refresh slot.
        refresh(slot, null);

        // Calculate bonuses
        setBonus();

        doExtraThings();

        // If it was a weapon, correct weapon interface
        if (slot == 3)
        {
            sendWeaponInterface();
            Combat.getInstance().fixAttackStyles(player, null);
        }
    }

    /**
     * Basically used for degraded equipment swapping. WARNING: Doesn't flag
     * player appreance change! ANOTHER WARNING: When setting item inside
     * container, if you want it to be empty just make it null!
     */
    public void forceEquip(Item item, int slot)
    {
        // First, fix the attack styles
        if (item == null)
            Combat.getInstance().fixAttackStyles(player, null);

        getItemContainer().set(slot, item);
        refresh(slot, item);
        setBonus();
        if (slot == 3)
            sendWeaponInterface();
    }

    public void setBonus()
    {

        // Reset the bonuses
        for (int i = 0; i < 12; i++)
            player.setBonuses(i, 0);

        for (int i = 0; i < 14; i++)
        {

            // If the item is null, just skip the calculation of bonuses for
            // that item.
            if (get(i) == null)
                continue;

            for (int bonus = 0; bonus < 12; bonus++)
            {
                if (BonusDefinition.getBonus(get(i).getId()) != null)
                    player.setBonuses(bonus, player.getBonuses().get(bonus) + BonusDefinition.getBonus(get(i).getId()).getBonus(bonus));
            }
        }
    }

    public void sendWeaponInterface()
    {

        // If the weapon is null, send default.
        if (get(3) == null)
        {
            player.getActionSender().sendSidebarInterface(0, 5855);
            player.getActionSender().sendString("Unarmed", 5857);
            return;
        }

        int interfaceId = Weapons.getWeaponInterface(player);
        player.getActionSender().sendSidebarInterface(0, interfaceId);
        player.getActionSender().sendString(get(3).getDefinition().getName(), interfaceId + (interfaceId == 6103 ? 29 : (interfaceId == 5855 ? 2 : 3)));
    }

    /**
     * Gets the slot that the item belongs to in the player equipment. If it has
     * no slot definition, the default slot is the weapon slot. This method is a
     * lot faster than the traditional winterlove server approach, because it
     * uses binary searching of the sorted equipment slot arrays instead of
     * straight looping.
     * 
     * @param itemID the item ID
     * @return the slot
     */
    public static int getEquipmentSlot(int itemID)
    {
        if (Arrays.binarySearch(PLATEBODY, itemID) > 0)
        {
            return Constants.EQUIPMENT_SLOT_CHEST;
        }
        if (Arrays.binarySearch(HELM_NO_HAIR, itemID) > 0)
        {
            return Constants.EQUIPMENT_SLOT_HEAD;
        }
        if (Arrays.binarySearch(HELM_NO_BEARD, itemID) > 0)
        {
            return Constants.EQUIPMENT_SLOT_HEAD;
        }
        if (Arrays.binarySearch(FULL_MASK, itemID) > 0)
        {
            return Constants.EQUIPMENT_SLOT_HEAD;
        }
        if (Arrays.binarySearch(BODY, itemID) > 0)
        {
            return Constants.EQUIPMENT_SLOT_CHEST;
        }
        if (Arrays.binarySearch(LEGS, itemID) > 0)
        {
            return Constants.EQUIPMENT_SLOT_LEGS;
        }
        if (Arrays.binarySearch(CAPES, itemID) > 0)
        {
            return Constants.EQUIPMENT_SLOT_CAPE;
        }
        if (Arrays.binarySearch(BOOTS, itemID) > 0)
        {
            return Constants.EQUIPMENT_SLOT_FEET;
        }
        if (Arrays.binarySearch(GLOVES, itemID) > 0)
        {
            return Constants.EQUIPMENT_SLOT_HANDS;
        }
        if (Arrays.binarySearch(SHIELDS, itemID) > 0)
        {
            return Constants.EQUIPMENT_SLOT_SHIELD;
        }
        if (Arrays.binarySearch(HATS, itemID) > 0)
        {
            return Constants.EQUIPMENT_SLOT_HEAD;
        }
        if (Arrays.binarySearch(AMULETS, itemID) > 0)
        {
            return Constants.EQUIPMENT_SLOT_AMULET;
        }
        if (Arrays.binarySearch(ARROWS, itemID) > 0)
        {
            return Constants.EQUIPMENT_SLOT_ARROWS;
        }
        if (Arrays.binarySearch(RINGS, itemID) > 0)
        {
            return Constants.EQUIPMENT_SLOT_RING;
        }
        logger.info("There's no slot for " + ItemManager.getInstance().getItemName(itemID) + "[" + itemID + "].");
        return Constants.EQUIPMENT_SLOT_WEAPON;
    }

    public static boolean isMedHelm(int id)
    {
        return Arrays.binarySearch(HELM_NO_HAIR, id) > 0;
    }

    public static boolean isFaceMask(int id)
    {
        return Arrays.binarySearch(HELM_NO_BEARD, id) > 0;
    }

    public static boolean isFullMask(int id)
    {
        return Arrays.binarySearch(FULL_MASK, id) > 0;
    }

    public static boolean isPlatebody(int id)
    {
        return Arrays.binarySearch(PLATEBODY, id) > 0;
    }

    public int getStandAnim(Item item)
    {
        if (item == null)
        {
            return 0x328;
        }
        return Weapons.getStandAnimation(player);
    }

    public int getWalkAnim(Item item)
    {
        if (item == null)
        {
            return 0x333;
        }
        return Weapons.getWalkOrRunAnimation(player, false);
    }

    public int getRunAnim(Item item)
    {
        if (item == null)
        {
            return 0x338;
        }
        return Weapons.getWalkOrRunAnimation(player, true);
    }

    public static void sortEquipmentSlotDefinitions()
    {
        Arrays.sort(CAPES);
        Arrays.sort(BOOTS);
        Arrays.sort(GLOVES);
        Arrays.sort(HATS);
        Arrays.sort(AMULETS);
        Arrays.sort(ARROWS);
        Arrays.sort(RINGS);
        Arrays.sort(BODY);
        Arrays.sort(LEGS);
        Arrays.sort(PLATEBODY);
        Arrays.sort(HELM_NO_HAIR);
        Arrays.sort(HELM_NO_BEARD);
        Arrays.sort(FULL_MASK);
        Arrays.sort(UNTRADEABLE_ITEMS);
        Arrays.sort(UNDROPPABLE_ITEMS);
    }

    /**
     * Gets the item from equipment container
     * 
     * @param index The index e.g 3-Weapon
     * @return The item, or <code>null</code> if it could not be found.
     */
    public Item get(int index)
    {
        return itemContainer.get(index);
    }

    public Container getItemContainer()
    {
        return itemContainer;
    }
}
