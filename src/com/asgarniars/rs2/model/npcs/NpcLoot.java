package com.asgarniars.rs2.model.npcs;

import com.asgarniars.rs2.model.items.Item;

/**
 * @author Joe.melsha@live.com (Killer 99)f
 */
public class NpcLoot
{

    /**
     * The chance out of 1.0.
     */
    private double frequency;

    /**
     * The item dropped.
     */
    private Item[] items;

    // -- Constructor for random drops..
    public NpcLoot(double frequency, Item... item)
    {
        this.frequency = frequency;
        this.items = item;
    }

    // -- Constructor for constant drops..
    public NpcLoot(Item... item)
    {
        this.items = item;
    }

    /**
     * @return the frequency
     */
    public double getFrequency()
    {
        return frequency;
    }

    /**
     * @return the item
     */
    public Item[] getItems()
    {
        return items;
    }

}
