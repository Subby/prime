package com.asgarniars.rs2.model.npcs;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.content.mta.Arena;
import com.asgarniars.rs2.io.XStreamController;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.npcs.Npc.WalkType;

/**
 * Having anything to do with any type of npc data loading.
 * 
 * @author BFMV, AkZu, and Jacob.
 */
public class NpcLoader
{

    private static final Logger logger = Logger.getLogger(NpcLoader.class.getName());

    @SuppressWarnings("unchecked")
    public static void loadSpawns() throws FileNotFoundException
    {
        List<Npc> list = (List<Npc>) XStreamController.getXStream().fromXML(new FileInputStream(Constants.CONFIG_DIRECTORY + "npcs/npcspawns.xml"));

        // Spawn and register the npc's which are loaded..
        for (Npc spawn : list)
        {

            // Create a new npc with given npc id
            Npc npc = new Npc(spawn.getNpcId());

            // Set the position where the npc should spawn
            npc.setSpawnPosition(spawn.getSpawnPosition());

            if (spawn.getMaxWalkingArea() != null)
                npc.setWalkType(WalkType.WALK);

            if (npc.getWalkType() != WalkType.STAND)
                npc.setFaceType(spawn.getFacingDirection());

            if (spawn.getMaxWalkingArea() != null)
                npc.setMaxWalkingArea(spawn.getMaxWalkingArea());

            // -- Finally, register the npc
            World.register(npc);

            if (spawn.getNpcId() == 3099)
                Arena.setAlchemyGuardian(npc);
        }

        logger.info("Loaded " + list.size() + " npc spawns");
    }

    private static Map<Integer, NpcDefinition> npcdef;

    public static NpcDefinition getDefinition(int id)
    {
        return npcdef.get(id);
    }

    @SuppressWarnings("unchecked")
    public static void loadDefinitions() throws FileNotFoundException
    {
        npcdef = new HashMap<Integer, NpcDefinition>();
        XStreamController.getInstance();

        List<NpcDefinition> loaded = (ArrayList<NpcDefinition>) XStreamController.getXStream().fromXML(new FileInputStream(Constants.CONFIG_DIRECTORY + "npcs/npcdef.xml"));
        for (NpcDefinition npc : loaded)
        {
            npcdef.put(npc.getId(), npc);
        }
        logger.info("Loaded " + npcdef.size() + " npc definitions.");
    }
}
