package com.asgarniars.rs2.model.npcs;

import com.asgarniars.rs2.model.Entity.AttackTypes;

public class NpcDefinition
{

	public Npc npc;
    private int id;
    private String name;
    private int[] combatLevels;
    private int[] bonuses; // Combat bonuses (defensive only)
    private int spell_id;
    private int attackAnim;
    private int atkAnimDelay;
    private int defenceAnim;
    private int defAnimDelay;
    private boolean isAttackable;
    private AttackTypes attackType;
    private int deathAnimation;
    private int deathAnimDelay;
    private int respawnTimer; // In tasks
    private int hiddenTimer; // ^
    private int attackSpeed;
    private int maxAtkDistance; // Maximum distance npc is allowed to follow
                                // player, untill remove interest and walk back
                                // via algorithm.
    private int size;
    private NpcLoot constantDrops; // The loot you can always get
    private NpcLoot[] randomDrops; // The loot you can possibly get when you
                                   // kill this npc
    private boolean immunePoison; // Set when npc is immune to poison
    private boolean poisonDmg; // Set when npc is poisoneus
    private boolean aggressive; // Set when npc is aggressiveness no matter what
    // combat level is victim (player)
    private boolean isThievable;
    private int maxHit;

    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public int getCombatLevel(int skillId)
    {
        if (combatLevels == null)
            return 1;
        if (combatLevels[3] == 0)
            return 1;
        return combatLevels[skillId];
    }

    public int getAttackAnim()
    {
        return attackAnim;
    }

    public int getDefenceAnim()
    {
        return defenceAnim;
    }

    public boolean isAttackable()
    {
        return isAttackable;
    }

    public int getDeathAnimation()
    {
        return deathAnimation;
    }

    public int getRespawnTimer()
    {
        return respawnTimer;
    }

    public void setRespawnTimer(int time)
    {
        respawnTimer = time;
    }

    public int getHiddenTimer()
    {
        return hiddenTimer;
    }

    public int getAttackSpeed()
    {
        return attackSpeed;
    }

    @SuppressWarnings("static-access")
	public AttackTypes getAttackType()
    {
    	 //if NPC's attackType is not defined we have to give it one!
    	if(attackType == null){
    		return attackType.MELEE;
    	}
    	
        return attackType;
    }

    public int getSize()
    {
        // -- Correcting the default value
        if (size == 0)
            size = 1;
        return size;
    }

    public int getMaxHit()
    {
        return maxHit;
    }

    /**
     * This is basic system that allows npc to cast spells which player can cast
     * too, more complex spells will be utilized with AttackType.SPECIAL and own
     * class for this npc. Note that this is not the spell id should be the spell 
     * slot and not the action button.
     */
    public int getSpellId()
    {
        return spell_id;
    }

    public boolean isThievable()
    {
        return isThievable;
    }

    public boolean isImmuneToPoison()
    {
        return immunePoison;
    }

    /**
     * This will be a basic system to poison kinda like how player will apply
     * poison with DDS++ (6 dmg), on special npcs the poison effect can be made
     * differently.
     */
    public boolean getPoisonDamage()
    {
        return poisonDmg;
    }

    public boolean isAggressive()
    {
        return aggressive;
    }

    public NpcLoot getConstantDrops()
    {
        return constantDrops;
    }

    public NpcLoot[] getRandomDrops()
    {
        return randomDrops;
    }

    public int getDeathAnimDelay()
    {
        return deathAnimDelay;
    }

    public int getDefAnimDelay()
    {
        return defAnimDelay;
    }

    public int getAtkAnimDelay()
    {
        return atkAnimDelay;
    }

    public int getMaxAtkDistance()
    {
        return maxAtkDistance;
    }

    public int[] getBonuses()
    {
        return bonuses;
    }
}
