package com.asgarniars.rs2.model;

/*
 * This file is part of RuneSource.
 *
 * RuneSource is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RuneSource is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RuneSource.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayDeque;
import java.util.Deque;

import com.asgarniars.rs2.content.duel.DuelArena;
import com.asgarniars.rs2.content.skills.magic.Teleother;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.model.players.Player.TeleotherStage;
import com.asgarniars.rs2.model.players.Player.TradeStage;
import com.asgarniars.rs2.util.Misc;

public class MovementHandler
{

    private final Entity entity;
    private Deque<Point> waypoints = new ArrayDeque<Point>();
    private boolean runPath = false;

    public MovementHandler(Entity entity)
    {
        this.entity = entity;
    }

    public void process()
    {

        if (entity.getAttribute("isDead") != null || entity.getAttribute("isFrozen") != null)
            return;

        if (waypoints.isEmpty())
            return;

        if (!entity.canWalk())
            return;

        Point walkPoint = null;
        Point runPoint = null;

        // Handle the movement.
        walkPoint = waypoints.poll();

        if (entity.isPlayer())
        {
            Player player = (Player) entity;

            if (player.getAttribute("isRunning") != null && player.getAttribute("runLocked") == null || isRunPath())
            {
                if (player.getEnergy() >= 1.0)
                {
                    runPoint = waypoints.poll();
                    if (runPoint != null)
                    {
                        player.setEnergy(player.getEnergy() - 1);
                        player.removeAttribute("canRestoreEnergy");
                    }
                    else
                    {
                        player.setAttribute("canRestoreEnergy", (byte) 0);
                    }
                }
                else
                {
                    setRunPath(false);
                    player.removeAttribute("isRunning");
                    player.getActionSender().sendConfig(173, 0);
                    player.setAttribute("canRestoreEnergy", (byte) 0);
                }
            }
        }

        if (walkPoint != null || runPoint != null)
        {

            if (walkPoint != null && walkPoint.getDirection() != -1)
            {
                entity.getPosition().move(Misc.DIRECTION_DELTA_X[walkPoint.getDirection()], Misc.DIRECTION_DELTA_Y[walkPoint.getDirection()]);
                entity.setAttribute("primary_dir", walkPoint.getDirection());
            }

            if (runPoint != null && runPoint.getDirection() != -1)
            {
                entity.getPosition().move(Misc.DIRECTION_DELTA_X[runPoint.getDirection()], Misc.DIRECTION_DELTA_Y[runPoint.getDirection()]);
                entity.setAttribute("secondary_dir", runPoint.getDirection());
            }

            if (entity.isPlayer())
            {

                // Check for region changes.
                final int deltaX = entity.getPosition().getX() - ((Player) entity).getLastKnownRegion().getRegionX() * 8;
                final int deltaY = entity.getPosition().getY() - ((Player) entity).getLastKnownRegion().getRegionY() * 8;
                if ((deltaX < 16 || deltaX >= 88 || deltaY < 16 || deltaY > 88) && entity.getAttribute("2nd_map_region_mode") == null)
                    ((Player) entity).getActionSender().sendMapRegion();

                if (((Player) entity).getMinigame() != null)
                {
                    ((Player) entity).getMinigame().movementListener((Player) entity);
                }
            }
        }
    }

    public boolean isEmpty()
    {
        return waypoints.size() == 0;
    }

    /**
     * Resets the walking queue.
     */
    public void reset()
    {
        setRunPath(false);
        waypoints.clear();

        // Set the base point as this position.
        Position p = entity.getPosition();
        waypoints.add(new Point(p.getX(), p.getY(), -1));
    }

    /**
     * Finishes the current path.
     */
    public void finish()
    {
        waypoints.removeFirst();
    }

    /**
     * Walk somewhere. WARNING: NOT CLIPPED WALKING!
     */
    public void walk(Position pos)
    {
        reset();
        addToPath(pos);
        finish();
    }

    /**
     * Internal add pos to path. WARNING: Don't use this for normal walking. Use
     * walk(Position pos) instead!
     */
    public void addToPath(Position position)
    {
        if (waypoints.size() == 0)
        {
            reset();
        }
        Point last = waypoints.peekLast();
        int deltaX = position.getX() - last.getX();
        int deltaY = position.getY() - last.getY();

        int max = Math.max(Math.abs(deltaX), Math.abs(deltaY));
        for (int i = 0; i < max; i++)
        {
            if (deltaX < 0)
            {
                deltaX++;
            }
            else if (deltaX > 0)
            {
                deltaX--;
            }
            if (deltaY < 0)
            {
                deltaY++;
            }
            else if (deltaY > 0)
            {
                deltaY--;
            }
            addStep(position.getX() - deltaX, position.getY() - deltaY);
        }
    }

    /**
     * Adds a step.
     * 
     * @param x the X coordinate
     * @param y the Y coordinate
     */
    private void addStep(int x, int y)
    {
        if (waypoints.size() >= 50)
        {
            return;
        }
        Point last = waypoints.peekLast();
        int deltaX = x - last.getX();
        int deltaY = y - last.getY();
        int direction = Misc.direction(deltaX, deltaY);
        if (direction > -1)
        {
            waypoints.add(new Point(x, y, direction));
        }
    }

    /**
     * Toggles running for the current path only.
     * 
     * @param runPath the flag
     */
    public void setRunPath(boolean runPath)
    {
        this.runPath = runPath;
    }

    /**
     * Gets whether or not we're running for the current path.
     * 
     * @return running
     */
    public boolean isRunPath()
    {
        return runPath;
    }

    public boolean walkToAction(Position position, int i)
    {
        boolean distance = Misc.getDistance(entity.getPosition(), position) <= i;
        return distance && isEmpty();
    }

    public void resetActionsOnMovement()
    {

        if (entity.isPlayer())
        {

            Player player = (Player) entity;

            if (player.getTradeStage() != TradeStage.WAITING && player.getTradeStage() != TradeStage.SEND_REQUEST)
                player.getTrading().declineTrade(player);

            if ((String) player.getAttribute("duel_stage") != "WAITING" && (String) player.getAttribute("duel_stage") != "INGAME" && (String) player.getAttribute("duel_stage") != "VICTORY" && (String) player.getAttribute("duel_stage") != "SENT_REQUEST")
                DuelArena.getInstance().declineDuel(player, false);

            player.stopAllActions(false, true);
            player.getActionSender().removeInterfaces();

            if (player.getTeleotherStage() != TeleotherStage.WAITING)
                Teleother.handleDecline(player);
        }
    }

    /**
     * An internal Position type class with support for direction.
     * 
     * @author blakeman8192
     */
    private class Point extends Position
    {

        private int direction;

        /**
         * Creates a new Point.
         * 
         * @param x the X coordinate
         * @param y the Y coordinate
         * @param direction the direction to this point
         */
        public Point(int x, int y, int direction)
        {
            super(x, y);
            setDirection(direction);
        }

        /**
         * Sets the direction.
         * 
         * @param direction the direction
         */
        public void setDirection(int direction)
        {
            this.direction = direction;
        }

        /**
         * Gets the direction.
         * 
         * @return the direction
         */
        public int getDirection()
        {
            return direction;
        }

    }

}