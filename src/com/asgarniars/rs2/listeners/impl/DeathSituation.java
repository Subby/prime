package com.asgarniars.rs2.listeners.impl;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.content.combat.Combat;
import com.asgarniars.rs2.content.combat.util.Poison;
import com.asgarniars.rs2.content.combat.util.Prayer;
import com.asgarniars.rs2.content.minigame.ItemSafety;
import com.asgarniars.rs2.content.minigame.barrows.Barrows;
import com.asgarniars.rs2.content.minigame.barrows.Brother;
import com.asgarniars.rs2.listeners.DeathListener;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.World;
import com.asgarniars.rs2.model.items.ItemManager;
import com.asgarniars.rs2.model.npcs.Npc;
import com.asgarniars.rs2.model.npcs.Npc.WalkType;
import com.asgarniars.rs2.model.npcs.NpcLoot;
import com.asgarniars.rs2.model.players.Player;
import com.asgarniars.rs2.task.Task;
import com.asgarniars.rs2.util.Misc;

/**
 * This is a skeletal death situation, it is the back bone to all other death
 * situations, taking the combined data and producing the result.
 * 
 * @author Joshua Barry
 * 
 */
public abstract class DeathSituation implements DeathListener
{
	
    protected abstract Entity getEntity();

    protected abstract Object[] getRespawnPosition();

    protected abstract String getDeadMessage();

    protected abstract ItemSafety getItemSafety();
    

    @Override
    public void die()
    {

        // -- The killer who killed this entity.
        final Entity killer = getEntity().getAttribute("combat_opponent");

        // -- TODO: Npcs death.....
        if (getEntity().isNpc())
        {

            final Npc npc = (Npc) getEntity();
            
            // -- Npc doesn't respawn anymore --- teleport and kill it. 
            if (npc.getDefinition().getRespawnTimer() == 0)
            {
                npc.teleport(new Position(0, 0, 0)); //Could this affect efficiency?
                if(killer.isPlayer()) {
                	for(Brother brother : Brother.values()) {
                		if(npc.getNpcId() == brother.getNpcId()) {
                			Barrows.defeat((Player) killer);
                			System.out.println("we tried to append kill count");
                		}
                	}
                }
                World.unregister(npc);
                return;
            }

            // Wait untill hidden timer is complete
            World.submit(new Task(npc.getDefinition().getHiddenTimer())
            {
                @Override
                protected void execute()
                {
                    // -- Drop the loot..
                    if (killer != null && killer.isPlayer())
                    {

                        // -- First remove the invalid entries from players who
                        // has hit
                        // this npc, so only latest hits with most damage
                        // dealt will
                        // get loot.
                        npc.getCombatState().getDamageMap().removeInvalidEntries();

                        final Player looter = (Player) ((npc.getCombatState().getDamageMap().highestDamage() != null) ? npc.getCombatState().getDamageMap().highestDamage() : null);

                        if (looter != null)
                        {
                            final NpcLoot constantDrops = npc.getDefinition().getConstantDrops();
                            final NpcLoot[] randomDrops = npc.getDefinition().getRandomDrops();
                            final Position loot_position = new Position(npc.getPosition().getX(), npc.getPosition().getY(), npc.getPosition().getZ());

                            // -- Drop the items that should always be dropped
                            if (constantDrops != null)
                            {
                                World.submit(new Task(1)
                                {
                                    @Override
                                    protected void execute()
                                    {
                                        for (int i = 0; i < constantDrops.getItems().length; i++)
                                            ItemManager.getInstance().createGroundItem(looter, looter.getUsername(), constantDrops.getItems()[i], loot_position, Constants.GROUND_START_TIME_DROP);
                                        this.stop();
                                    }
                                });
                            }

                            // -- Drop item(s) randomly from the drop table..
                            if (randomDrops != null)
                            {
                                if (randomDrops.length > 0)
                                {
                                    NpcLoot drop = null;

                                    while (drop == null)
                                    {
                                        NpcLoot randomDrop = randomDrops[Misc.random.nextInt(randomDrops.length)];
                                        if (Misc.random.nextDouble() < randomDrop.getFrequency())
                                            drop = randomDrop;
                                    }

                                    final NpcLoot final_drop = drop;

                                    World.submit(new Task(1)
                                    {
                                        @Override
                                        protected void execute()
                                        {
                                            for (int i = 0; i < final_drop.getItems().length; i++)
                                                ItemManager.getInstance().createGroundItem(looter, looter.getUsername(), final_drop.getItems()[i], loot_position, Constants.GROUND_START_TIME_DROP);
                                            this.stop();
                                        }
                                    });
                                }
                            }
                        }
                    }



                    // -- Teleport the npc away from sight for a while << TODO:
                    // If
                    // re-spawnable
                    npc.teleport(new Position(0, 0, 0));

                    // Remove some more attributes
                    npc.getCombatState().getDamageMap().reset(); // Clear the
                                                                 // damage map.
                    npc.removeAttribute("isFrozen");
                    npc.removeAttribute("cantBeFrozen");
                    Poison.appendPoison(npc, false, 0);
                    npc.setCombatTimer(0);
                    npc.getUpdateFlags().sendAnimation(65535, 0);
                    npc.setCanWalk(true);

                    // -- Reset combat
                    Combat.getInstance().resetCombat(npc);

                    // Restore skills TODO
                    npc.setAttribute("current_hp", npc.getDefinition().getCombatLevel(3));

                    // Respawn the npc
                    World.submit(new Task(npc.getDefinition().getRespawnTimer())
                    {
                        @Override
                        protected void execute()
                        {
                            npc.removeAttribute("isDead");

                            if (npc.getWalkType() == WalkType.WALK)
                                npc.clipTeleport(new Position(npc.getSpawnPosition().getX(), npc.getSpawnPosition().getY(), npc.getSpawnPosition().getZ()), 0, npc.getMaxWalkingArea().getX(), npc.getMaxWalkingArea().getY());
                            else
                                npc.teleport(npc.getSpawnPosition());

                            // -- Finally...
                            npc.removeAttribute("combat_opponent");
                            npc.actions_after_spawn();
                            this.stop();
                        }
                    });
                    this.stop();
                }
            });
        }

        // TODO: Players death...
        if (getEntity().isPlayer())
        {

            final Player player = (Player) getEntity();

            if (player.getMinigame() != null && killer != null && killer.isPlayer())
                player.getMinigame().defeatListener(player, killer);

            // Send kill messages if you're not inside minigame
            if (player.getMinigame() == null && killer != null && killer.isPlayer())
            {
                Player otherPlayer = (Player) killer;

                String[] randomKillMessages =
                { "You have defeated " + player.getUsername() + "!", player.getUsername() + " won't cross your path again!", "Good fight, " + player.getUsername() + ".", player.getUsername() + " will feel that in Lumbridge.", "Can anyone defeat you? Certainly not " + player.getUsername() + ".", player.getUsername() + " falls before your might.", "A humiliating defeat for " + player.getUsername() + ".", "You were clearly a better fighter than " + player.getUsername() + ".", player.getUsername() + " has won a free ticket to Lumbridge.", "It's all over for " + player.getUsername() + ".", "With a crushing blow you finish " + player.getUsername() + ".", player.getUsername() + " regrets the day they met you in combat.", player.getUsername() + " didn't stand a chance against you." };

                otherPlayer.getActionSender().sendMessage(randomKillMessages[Misc.random(randomKillMessages.length)]);
                Combat.getInstance().resetCombat(otherPlayer);
            }

            // Drop loot if it's allowed
            if (getItemSafety() == ItemSafety.UNSAFE)
            {

                // -- First remove the invalid entries from players who has hit
                // this player, so only latest hits with most damage dealt will
                // get loot.
                player.getCombatState().getDamageMap().removeInvalidEntries();

                final Entity looter = (Entity) (player.getCombatState().getDamageMap().highestDamage() != null ? player.getCombatState().getDamageMap().highestDamage() : player);

                player.dropLoot(looter.isNpc() ? null : ((Player) looter));
            }

            Combat.getInstance().fixAttackStyles(player, player.getEquipment().get(3));
            player.getActionSender().sendMessage(getDeadMessage());
            player.getCombatState().getDamageMap().reset(); // Clear the damage
                                                            // map.
            Combat.getInstance().resetCombat(player);

            player.setSpecialAttackActive(false);
            player.setSpecialAmount(10);
            player.setCombatTimer(0);
            player.setCanWalk(true);
            player.removeAttribute("isFrozen");
            player.removeAttribute("teleBlocked");
            player.removeAttribute("charge");
            player.removeAttribute("cant_teleport");
            player.removeAttribute("chargeCoolDown");
            player.setSkulled(false);
            Poison.appendPoison(player, false, 0);
            Prayer.getInstance().resetAll(player);
            player.getUpdateFlags().sendAnimation(65535, 0);
            player.getActionSender().sendWalkableInterface(-1);
            player.getSkill().normalize();
            player.removeAttribute("isDead");

            // -- Teleport the player
            player.clipTeleport((Position) getRespawnPosition()[0], (Integer) getRespawnPosition()[1], (Integer) getRespawnPosition()[2], (Integer) getRespawnPosition()[3]);

            // Null the player's minigame (if any) as he died!!
            if (player.getMinigame() != null && !player.getMinigame().getGameName().equals("Fight Pits"))
                player.setMinigame(null);

            // Null the combating entity, that's used for death method thingies
            player.removeAttribute("combat_opponent");
        }
    }
}
