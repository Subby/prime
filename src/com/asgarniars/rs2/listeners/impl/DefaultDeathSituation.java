package com.asgarniars.rs2.listeners.impl;

import com.asgarniars.rs2.Constants;
import com.asgarniars.rs2.content.minigame.ItemSafety;
import com.asgarniars.rs2.model.Entity;
import com.asgarniars.rs2.model.Position;
import com.asgarniars.rs2.model.players.Player;

/**
 * This is your default death situation, determining what occurs when you die
 * normally.
 * 
 * @author Joshua Barry
 * 
 */
public class DefaultDeathSituation extends DeathSituation
{

    private Entity entity;

    public DefaultDeathSituation(Entity entity)
    {
        this.entity = entity;
    }

    @Override
    protected Entity getEntity()
    {
        return entity;
    }

    @Override
    protected Object[] getRespawnPosition()
    {
        // -- Use minigame specific
        if (((Player) entity).getMinigame() != null)
        {
            return new Object[]
            { ((Player) entity).getMinigame().getEndPosition()[0], ((Player) entity).getMinigame().getEndPosition()[1], ((Player) entity).getMinigame().getEndPosition()[2], ((Player) entity).getMinigame().getEndPosition()[3] };
        }
        else
            return new Object[]
            { new Position(Constants.START_X, Constants.START_Y, Constants.START_Z), 0, 2, 2 };
    }

    @Override
    protected ItemSafety getItemSafety()
    {
        return (entity.isPlayer() && ((Player) entity).getMinigame() != null ? ((Player) entity).getMinigame().getItemSafety() : ItemSafety.UNSAFE);
    }

    @Override
    protected String getDeadMessage()
    {
        return (entity.isPlayer() && ((Player) entity).getMinigame() != null && ((Player) entity).getMinigame().getDeadMessage() != null ? (((Player) entity).getMinigame().getDeadMessage()) : "Oh dear, you have died!");
    }

}
