package com.asgarniars.rs2.listeners;

/**
 * A basic listener !
 * 
 * @author Joshua Barry
 * 
 */
public interface Listener<T>
{

    public abstract boolean listen();

}
