# All object interactions <3

from com.asgarniars.rs2 import Server
from com.asgarniars.rs2.model import Position, World

from com.asgarniars.rs2.content.dialogue import Option, StatementDialogue, StatementType
from com.asgarniars.rs2.content.dialogue.impl import SecurityGate
from com.asgarniars.rs2.content.minigame.impl.CastleWars import Team
from com.asgarniars.rs2.content.minigame.barrows import Brother, Barrows
from com.asgarniars.rs2.content.mta import Arena, AlchemistPlayground, EnchantmentChamber
from com.asgarniars.rs2.content.quest import QuestRepository
from com.asgarniars.rs2.content import Climbable, Crates, NiggerJob
from com.asgarniars.rs2.content.Climbable import Direction
from com.asgarniars.rs2.content.skills.agility import Obstacle, TackleObstacle
from com.asgarniars.rs2.content.skills.craft import Spinnable, WheelSpinning
from com.asgarniars.rs2.content.skills.mine import Mining
from com.asgarniars.rs2.content.skills.thief import Shoplift
from com.asgarniars.rs2.content.skills.runecraft import Rune, Runecraft
from com.asgarniars.rs2.content.skills.woodcut import Felling
from com.asgarniars.rs2.model.items import Item, ItemManager
from com.asgarniars.rs2.model.players import BankManager, GlobalObject, GlobalObjectHandler
from com.asgarniars.rs2.model.npcs import Npc
from com.asgarniars.rs2.content.duel import DuelArena
from com.asgarniars.rs2.task import Task
from com.asgarniars.rs2.util import Misc
from com.asgarniars.rs2.util.clip import ObjectDefinition, RegionClipping, RSObject

def objectOptionOne_sarcophagus(player):
    tomb = player.getClickId()
    brother = Brother.forTomb(tomb)
    if brother != None:
        if brother == player.getAttribute('hidden_tomb'):
            return Barrows.requestHiddenTomb(player)
        Barrows.awaken(brother, player)

def objectOptionOne_cave_entrance(player):
    if player.getClickId() == 9357:
        minigame = player.getAttribute('soloMinigame')
        if minigame != None:
            minigame.quit(player)
        player.clipTeleport(2439, 5170, 0, 2)
	
def objectOptionOne_entrance(player):
    x = player.getRegion().getCoordinates().getX()
    y = player.getRegion().getCoordinates().getY()
    print x, y
    if x == 96 and y == 106:
        player.clipTeleport(Position(1860, 5244, 0), 2)

def objectOptionOne_gate_of_war(player):
    npc = Npc(4377)
    dialogue = SecurityGate(player, [npc])
    dialogue.initiate()

def objectOptionOne_rickety_door(player):
    npc = Npc(4378)
    dialogue = SecurityGate(player, [npc])
    dialogue.initiate()

def objectOptionOne_oozing_barrier(player):
    npc = Npc(4379)
    dialogue = SecurityGate(player, [npc])
    dialogue.initiate()

def objectOptionOne_portal_of_death(player):
    npc = Npc(4380)
    dialogue = SecurityGate(player, [npc])
    dialogue.initiate()

def objectOptionOne_gift_of_peace(player):
    claimed = 'You have already claimed your reward from this level.'
    msg = 'The box hinges creak and appear to be forming audible words...'
    item = Item(995, 10000)
    dialogue = StatementDialogue(StatementType.NORMAL, [msg])
    dialogue.add(StatementType.NORMAL, ['...congratulations adventurer, you have been deemed worthy of this', 'reward. You have also unlocked the Flap emote!'])
    player.open(dialogue)
    player.getInventory().addItem(item)
    player.getActionSender().sendConfig(753, 1)

def objectOptionOne_grain_of_plenty(player):
    claimed = 'You have already claimed your reward from this level.'
    msg = 'The grain shifts in the sack, sighing audible words...'
    item = Item(995, 15000)
    dialogue = StatementDialogue(StatementType.NORMAL, [msg])
    dialogue.add(StatementType.NORMAL, ['...congratulations adventurer, you have been deemed worthy of this', 'reward. You have also unlocked the Slap Head emote!'])
    player.open(dialogue)
    player.getInventory().addItem(item)
    player.getActionSender().sendConfig(754, 1)

def objectOptionOne_box_of_health(player):
    claimed = 'You have already claimed your reward from this level.'
    msg = 'The box hinges creak and appear to be forming audible words...'
    item = Item(995, 25000)
    dialogue = StatementDialogue(StatementType.NORMAL, [msg])
    dialogue.add(StatementType.NORMAL, ['...congratulations adventurer, you have been deemed worthy of this', 'reward. You have also unlocked the Idea emote!'])
    player.open(dialogue)
    player.getInventory().addItem(item)
    player.getActionSender().sendConfig(751, 1).sendMessage('You feel refreshed and renewed.')
    player.getSkill().normalize()

def objectOptionOne_cradle_of_life(player):
    if player.getInventory().getItemContainer().contains(9005) or player.getInventory().getItemContainer().contains(9006) or player.getEquipment().getItemContainer().contains(9005) or player.getEquipment().getItemContainer().contains(9006) or player.getBank().contains(9005) or player.getBank().contains(9006):
         dialogue = StatementDialogue(StatementType.NORMAL, ['You have already claimed your reward from this level.'])
         return player.open(dialogue)
    
    msgs = ['As your hand touches the cradle, you hear a voice in your head of a', 'million dead adventurers...']
    
    dialogue = StatementDialogue(StatementType.NORMAL, msgs)
    dialogue.add(StatementType.NORMAL, ['....welcome adventurer... you have a choice....'])
    dialogue.add(StatementType.NORMAL, ['They will both protect your feet exactly the same, however they look', 'very different. You can always come back and get another pair if', 'you lose them, or even swap them for the other style!'])
    fancy = Option('Fancy Boots', FancyBoots(player))
    fight = Option('Fighting Boots', FighterBoots(player))
    dialogue.add('Select an Option', fancy, fight)
    player.open(dialogue)
    
def objectOptionOne_bone_chain(player):
    Climbable.climb(player, Position(3081, 3421, 0), 0)
    
def objectOptionOne_rope(player):
    if player.getClick() == 2017 and player.getClickY() == 5210:
        Climbable.climb(player, Position(3081, 3421, 0), 0)

def objectOptionOne_boney_ladder(player):
    Climbable.climb(player, Position(2145, 5284, 0), 2)

def objectOptionOne_ledge(player):
    quest = QuestRepository.get('WaterfallQuest')
    if player.getQuestStorage().hasStarted(quest):
        item = Item(295, 1)
        if (player.getInventory().getItemContainer().contains(item) or player.getEquipment().getItemContainer().contains(item)):
            player.getActionSender().sendMessage('The door begins to open.')
            World.submit(EnterBaxtorianFalls(player))
        else:
            player.hit(5, 1, False)
            player.getActionSender().sendMessage('You can not enter.')

def objectOptionOne_glarials_tomb(player):
    quest = QuestRepository.get('WaterfallQuest')
    if player.getClickId() == 1993:
        if player.getClickX() == 2542 and player.getClickY() == 9811:
            player.getActionSender().sendMessage('You search the coffin.')
            if player.getQuestStorage().hasStarted(quest):
                World.submit(GlarialsTomb(player))

def objectOptionOne_closed_bank_chest(player):
    x = player.getClickX()
    y = player.getClickY()
    z = player.getPosition().getZ()
    obj = RegionClipping.getRSObject(player, x, y, z)
    new_obj = RSObject(obj.getId() +1, x, y, z, obj.getType(), obj.getFace())
    GlobalObjectHandler.createGlobalObject(player, new_obj)

def objectOptionOne_closed_chest(player):
    # todo make this chest open first rofl.
    if player.getClickId() == 1994:
        if player.getClickX() == 2530 and player.getClickY() == 9844:
            obj = GlobalObject(379, Position(player.getClickX(), player.getClickY(), player.getPosition().getZ()), 1, 10)
            GlobalObjectHandler.createGlobalObject(player, obj)

def objectOptionTwo_open_chest(player):
    if player.getClickId() == 379:
         if player.getClickX() == 2530 and player.getClickY() == 9844:
             quest = QuestRepository.get('WaterfallQuest')
             if player.getQuestStorage().hasStarted(quest):
                 item = Item(295, 1)
                 if player.getInventory().getItemContainer().hasRoomFor(item):
                     if player.getInventory().addItem(item):
                         player.getActionSender().sendMessage('You search the chest and find a small amulet.')


def objectOptionOne_bookcase(player):
    if player.getClickId() == 1989 and player.getPosition().getZ() == 1:
        quest = QuestRepository.get('WaterfallQuest')
        if player.getQuestStorage().hasStarted(quest):
            book = Item(292, 1)
            if player.getInventory().getItemContainer().contains(book.getId()) or player.getBank().contains(book.getId()):
                return player.getActionSender().sendMessage('You already have a copy of the %s.' % ItemManager.getInstance().getItemName(book.getId()).lower())
            if player.getInventory().getItemContainer().hasRoomFor(book):
                player.getInventory().addItem(book)
        else:
            player.getActionSender().sendMessage('You search the bookcase, but find nothing of interest.')

def objectOptionOne_log_raft(player):
    quest = QuestRepository.get('WaterfallQuest')
    if player.getQuestStorage().hasStarted(quest):
        player.getActionSender().sendMessage('You board the small raft.')
        World.submit(RaftMessage(player))

def objectOptionOne_dead_tree(player):
    player.getActionSender().sendMessage('You slip and tumble over the water fall.')
    World.submit(TreeFall(player))


def objectOptionOne_barrel(player):
    player.getActionSender().sendMessage('You climb in the barrel and start rocking.')
    player.getActionSender().sendMessage('The barrel falls off the ledge.')
    World.submit(WaterfallBarrel(player))

def objectOptionOne_ladder(player):

    objdef = ObjectDefinition.forId(player.getClickId())

    dir = None

    if objdef.actions == None:
        return

    if len(objdef.actions) > 0:
        if 'up' in objdef.actions[0].lower():
            dir = Direction.UP
        elif 'down' in objdef.actions[0].lower():
            dir = Direction.DOWN

    Climbable.climb(player, dir)
    
def objectOptionOne_goo_covered_vine(player):
    Climbable.climb(player, Position(3081, 3421, 0), 0)
    
def objectOptionOne_dripping_vine(player):
    
    objdef = ObjectDefinition.forId(player.getClickId())
    
    dir = None
    
    if objdef.actions == None:
        return

    if len(objdef.actions) > 0:
        if 'up' in objdef.actions[0].lower():
            dir = Direction.UP
        elif 'down' in objdef.actions[0].lower():
            dir = Direction.DOWN
    
    Climbable.climb(player, dir)

# Mage Training Arena.
def objectOptionOne_doorway(player):
    if player.getPosition().getX() == 3363:
        if player.getPosition().getY() == 3298:
            y = int(player.getPosition().getY() + 2)
        elif player.getPosition().getY() == 3300:
            y = int(player.getPosition().getY() - 2)
        player.teleport(Position(player.getPosition().getX(), y, 0))

def objectOptionOne_exit_teleport(player):
    inChamber = Arena.getChamberArea().isInArea(player.getPosition())
    inAlchemy = Arena.getAlchemyArea().isInArea(player.getPosition())
    player.clipTeleport(3363, 3318, 0, 1)

    if inChamber:
        Arena.getEnchanters().remove(player)
        player.getActionSender().showComponent(EnchantmentChamber.getCurrentComponent(), False)

    elif inAlchemy:
        Arena.getAlchemists().remove(player)
        player.getActionSender().showComponent(AlchemistPlayground.getCurrentComponent(), False)

    else:
        amt = 0 if player.getAttribute('foodDeposits') == None else player.getAttribute('foodDeposits')
        Arena.getGraveyardMembers().remove(player)
        dialogue = StatementDialogue(StatementType.NORMAL, ['In this trip you gave %d fruit and you earned %d Graveyard Pizazz' % (amt, player.getGravePizazz()), 'Points!'])
        player.open(dialogue)
        player.removeAttribute('foodDeposits')

def objectOptionOne_alchemists_teleport(player):
    player.clipTeleport(3366, 9623, 2, 1)
    Arena.getAlchemists().add(player)
    updateAlchemistInterface(player)
    player.getActionSender().sendMessage("You've entered the Alchemists' Playground.")

def objectOptionOne_enchanters_teleport(player):
    player.clipTeleport(3363, 9649, 0, 1)
    Arena.getEnchanters().add(player)
    updateEnchantInterface(player)
    player.getActionSender().sendMessage("You've entered the Enchantment Chamber.")

def objectOptionOne_graveyard_teleport(player):
    player.clipTeleport(3364, 9640, 1, 1)
    Arena.getGraveyardMembers().add(player)
    updateGraveyardInterface(player)
    player.getActionSender().sendMessage("You've entered the Creature Graveyard.")


def objectOptionOne_bones(player):
    collected = player.getAttribute('bonesCollected')
    if collected < 4 or collected >= 16:
        if collected >= 16:
            collected = 0
        item = Item(6904, 1)
    elif collected >= 4 and collected < 8:
        item = Item(6905, 1)
    elif collected >= 8 and collected < 12:
        item = Item(6906, 1)
    elif collected >= 12 and collected < 16:
        item = Item(6907, 1)
    if player.getInventory().hasRoomFor(item):
        if player.getInventory().addItem(item):
            player.setAttribute('canTake', 0)
            World.submit(CollectionDelay(player))
            player.getUpdateFlags().sendAnimation(827)
            player.setAttribute('bonesCollected', collected + 1)


def objectOptionOne_food_chute(player):
    banana_count = player.getInventory().getItemContainer().getCount(1963)
    peach_count = player.getInventory().getItemContainer().getCount(6883)

    if banana_count > 0:
        item = player.getInventory().getItemContainer().getById(1963)
        for x in range(0, banana_count):
            player.getInventory().getItemContainer().remove(item)
        player.getInventory().refresh()

    if peach_count > 0:
        item = player.getInventory().getItemContainer().getById(6883)
        for x in range(0, peach_count):
            player.getInventory().getItemContainer().remove(item)
        player.getInventory().refresh()

    pre = player.getAttribute('foodChute')
    pre_deposit = 0 if pre == None else pre
    total = pre_deposit + banana_count + peach_count
    if total >= 16:
        points = total / 16
        player.setGravePizazz(player.getGravePizazz() + points)
        updateGraveyardInterface(player)
        if pre != None:
            player.removeAttribute('foodChute')

        if total > 16:
            extra = total - (16 * points)
            player.setAttribute('foodChute', extra)
    else:
        if total == 0:
            return
        player.setAttribute('foodChute', total)
    # store overall attribute
    deps = player.getAttribute('foodDeposits')
    amt = banana_count + peach_count if deps == None else banana_count + peach_count + deps
    player.setAttribute('foodDeposits', amt);


def objectOptionOne_cube_pile(player):
    item = Item(6899, 1)
    if player.getAttribute('canTake') == None:
        if player.getInventory().hasRoomFor(item):
            if player.getInventory().addItem(item):
                player.setAttribute('canTake', 0)
                World.submit(CollectionDelay(player))
                player.getUpdateFlags().sendAnimation(827)

def objectOptionOne_cylinder_pile(player):
    item = Item(6898, 1)
    if player.getAttribute('canTake') == None:
        if player.getInventory().hasRoomFor(item):
            if player.getInventory().addItem(item):
                player.setAttribute('canTake', 0)
                World.submit(CollectionDelay(player))
                player.getUpdateFlags().sendAnimation(827)

def objectOptionOne_icosahedron_pile(player):
    item = Item(6900, 1)
    if player.getAttribute('canTake') == None:
        if player.getInventory().hasRoomFor(item):
            if player.getInventory().addItem(item):
                player.setAttribute('canTake', 0)
                World.submit(CollectionDelay(player))
                player.getUpdateFlags().sendAnimation(827)

def objectOptionOne_pentamid_pile(player):
    item = Item(6901, 1)
    if player.getAttribute('canTake') == None:
        if player.getInventory().hasRoomFor(item):
            if player.getInventory().addItem(item):
                player.setAttribute('canTake', 0)
                World.submit(CollectionDelay(player))
                player.getUpdateFlags().sendAnimation(827)

def objectOptionOne_hole(player):
    if Arena.getChamberArea().isInArea(player.getPosition()):
        id = 6902
        if player.getInventory().getItemContainer().contains(id):
            item = player.getInventory().getItemContainer().getById(id)
            amt = player.getInventory().getItemContainer().getCount(id)
            for i in range(0, amt):
                player.getInventory().getItemContainer().remove(item)
            player.getInventory().refresh()
            if amt >= 20:
                rewards = [560, 564, 565]
                reward = Item(Misc.generatedValue(rewards), 3)
                if player.getInventory().addItem(reward):
                    dialogue = StatementDialogue(StatementType.NORMAL,
                                                 ["Congratulations! You've been rewarded with an item for your efforts"])
                    player.open(dialogue)
                    player.getActionSender().sendMessage("Congratulations! You've been rewarded with %d %ss for your efforts." % 
                                                         (reward.getCount(), ItemManager.getInstance().getItemName(reward.getId()).lower()))


def objectOptionOne_cupboard(player):
    content = AlchemistPlayground.getCupboardItems().get(player.getClickId())
    print content
    if content == None:
        player.getActionSender().sendMessage('The cupboard is empty.')
        return
    item = Item(AlchemistPlayground.getCupboardItems().get(player.getClickId()), 1)
    if player.getAttribute('canTake') == None:
        if player.getInventory().hasRoomFor(item):
            if player.getInventory().addItem(item):
                player.getActionSender().sendMessage('You found: %s.' % 
                                                     ItemManager.getInstance().getItemName(item.getId()))
                player.setAttribute('canTake', 0)
                World.submit(CollectionDelay(player))


def objectOptionOne_coin_collector(player):
    '''
    For every 100 coins deposited in the coin collector,
    the player will gain one pizazz point.
    The player also gains two magic experience per coin deposited,
    and ten coins (deposited straight to the player's bank) for every 100 deposited.
    '''
    if player.getInventory().getItemContainer().contains(8890):
        item = player.getInventory().getItemContainer().getById(8890)
        if item.getCount() >= 12000:
            if player.getInventory().removeItem(item):
                dialogue = StatementDialogue(StatementType.NORMAL,
                                             ['Your deposit exceeds the maximum amount allowed, as a',
                                              'result, you receive no reward.'])
                player.open(dialogue)
            return
        points = int(item.getCount() * 0.01)
        exp = int(item.getCount() * 2)
        gp = int(item.getCount() * 0.1)  # 10%
        if player.getInventory().removeItem(item):
            dialogue = StatementDialogue(StatementType.NORMAL,
                                         ("You've just deposited %d coins, earning you %d Alchemist Pizazz" % (item.getCount(), points),
                                         "Points and %d magic XP. So far you're taking %d coins as a reward" % (exp, gp),
                                         "when you leave!"))
            player.open(dialogue)
            points = player.getAlchemyPizazz() + points
            player.setAlchemyPizazz(points)
            updateAlchemistInterface(player)
            player.getBank().add(Item(995, gp))
            
def objectOptionOne_table(player):
    item = None
    obj = player.getClickId()
    if obj == 4458:
        item = Item(4049)
    elif obj == 4459:
        item = Item(4051)
    elif obj == 4460:
        item = Item(4043)
    elif obj == 4461:
        item = Item(4053)
    elif obj == 4462:
        item = Item(4047)
    elif obj == 4463:
        item = Item(4045)
    elif obj == 4464:
        item = Item(1265)
    player.getInventory().addItem(item)
    

def objectOptionOne_energy_barrier(player):
    barrier_x = player.getClickX()
    barrier_y = player.getClickY()
    player_x = player.getPosition().getX()
    player_y = player.getPosition().getY()
    plane = player.getPosition().getZ()
    
    obj = RegionClipping.getRSObject(player, barrier_x, barrier_y, plane)
    face = obj.getFace()
    
    
    print face, 'is the energy face dir'
    
    if face == 2:
        if (barrier_x < player_x):
            player_x -= 1
        else:
            player_x += 1
    elif face == 3:
        if (barrier_y <= player_y):
            player_y -= 1
        else:
            player_y += 1
    
    player.getMovementHandler().walk(Position(player_x, player_y, plane))
            
            
def objectOptionOne_cauldron(player):
    if player.getClickX() == 2967 and player.getClickY() == 3205:
        quest = QuestRepository.get('WitchsPotion')
        if player.getQuestStorage().hasStarted(quest):
            if player.getQuestStorage().getState(quest) == 3:
                player.setAttribute('currentQuest', quest)
                dialogue = StatementDialogue(StatementType.NORMAL, ("You drink from the cauldron, it tastes horrible! You feel yourself", "imbued with power."))
                player.getQuestStorage().setState(quest, 4)
                player.open(dialogue)

def objectOptionOne_bank_booth(player):
    BankManager.openBank(player)

def objectOptionOne_trapdoor(player):
    if player.getClickId() == 3203:
        if player.getAttribute("duel_button_forfeit") != None:
            return player.getActionSender().sendMessage("Forfeiting has been disabled for this duel session.")
        if player.getAttribute("duel_countdown") != None:
            return player.getActionSender().sendMessage("You can't forfeit the duel before the duel countdown is over.")
        player.getActionSender().sendMessage("You forfeit the duel.")
        DuelArena.getInstance().declineDuel(player, True)

# Castle wars objects
'''
def objectOptionOne_bank_chest(player):
    BankManager.openBank(player)
'''
def objectOptionOne_scoreboard(player):
    DuelArena.getInstance().open_scoreboard(player)
'''
def objectOptionOne_saradomin_portal(player):
    hood = player.getEquipment().getItemContainer().get(0)
    cape = player.getEquipment().getItemContainer().get(1)
    if hood or cape != None:
        return player.getActionSender().sendMessage("You can't wear hats, capes or helms in the arena.")
    Server.getSingleton().getCastleWars().join(player, Team.SARADOMIN)
    x = Misc.randomMinMax(0, 100)
    if x > 85:
        npcs = [708, 709, 43]
        id = Misc.generatedValue(npcs)
        Server.getSingleton().getCastleWars().transform(player, id)

def objectOptionOne_guthix_portal(player):
    sp = Server.getSingleton().getCastleWars().getSaradominPlayers().size()
    zp = Server.getSingleton().getCastleWars().getZamorakPlayers().size()
    if sp > zp:
        objectOptionOne_zamorak_portal(player)
    else:
        if sp == zp:
            x = Misc.randomMinMax(1, 3)
            if x > 1:
                objectOptionOne_zamorak_portal(player)
            else:
                objectOptionOne_saradomin_portal(player)
        else:
            objectOptionOne_saradomin_portal(player)

def objectOptionOne_zamorak_portal(player):
    hood = player.getEquipment().getItemContainer().get(0)
    cape = player.getEquipment().getItemContainer().get(1)
    if hood or cape != None:
        return player.getActionSender().sendMessage("You can't wear hats, capes or helms in the arena.")
    Server.getSingleton().getCastleWars().join(player, Team.ZAMORAK)
    x = Misc.randomMinMax(0, 100)
    if x > 85:
        npcs = [708, 709, 43]
        id = Misc.generatedValue(npcs)
        Server.getSingleton().getCastleWars().transform(player, id)

def objectOptionOne_portal(player):
    cw = Server.getSingleton().getCastleWars()
    team = None
    if cw.getSaradominWaiters().contains(player) or cw.getSaradominPlayers().contains(player):
        team = Team.SARADOMIN
    elif cw.getZamorakWaiters().contains(player) or cw.getZamorakPlayers().contains(player):
        team = Team.ZAMORAK
    if team == None:
        return
    print 'wutututu'
    cw.leave(player, team)
'''
'''
def objectOptionOne_saradomin_standard(player):

    pole = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ())
    print pole, 'is le pole'
    obj = RSObject(4377, pole.getPosition(), pole.getType(), pole.getFace())
    print obj, 'is le obj'
    GlobalObjectHandler.createGlobalObject(player, obj)
    banner = Item(4037, 1)
    player.getEquipment().getItemContainer().set(3, banner)
    player.getEquipment().refresh(3, banner)

    player.setAppearanceUpdateRequired(True)
    player.getEquipment().setBonus()

def objectOptionOne_zamorak_standard(player):
    
    print 'hi'

    pole = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ())
    obj = RSObject(4378, pole.getPosition(), pole.getType(), pole.getFace())

    banner = Item(4039, 1)
    player.getEquipment().getItemContainer().set(3, banner)
    player.getEquipment().refresh(3, banner)
    player.setAppearanceUpdateRequired(True)
    player.getEquipment().setBonus()

    GlobalObjectHandler.createGlobalObject(player, obj)
'''
def objectOptionOne_crates(player):
    Crates.search(player)

def objectOptionOne_crate(player):
    msg = 'You search the crate but find nothing.'
    if player.getClickId() == 1990 or player.getClickId() == 1999:
        player.getActionSender().sendMessage('You search the crate...')
        if player.getClickX() == 2548 and player.getClickY() == 9565 or player.getClickX() == 2589 and player.getClickY() == 9888:
            quest = QuestRepository.get('WaterfallQuest')
            if player.getQuestStorage().hasStarted(quest):
                World.submit(SearchCrate(player))
            else:
                player.getActionSender().sendMessage(msg)
    else:
        player.getActionSender().sendMessage(msg)

def objectOptionOne_door(player):
    print player.getClickId(), 'is the door ID. at ', player.getClickX(), '-', player.getClickY()
    if player.getClickId() == 1991:
        quest = QuestRepository.get('WaterfallQuest')
        if player.getQuestStorage().hasStarted(quest):
            if player.getInventory().getItemContainer().contains(2840):
                player.clipTeleport(player.getPosition().getX(), (player.getPosition().getY() + 3), player.getPosition().getZ(), 2)

def objectOptionOne_staircase(player):
    objdef = ObjectDefinition.forId(player.getClickId())

    dir = None

    if objdef.actions == None:
        return

    if len(objdef.actions) > 0:
        if 'up' in objdef.actions[0].lower():
            dir = Direction.UP
        elif 'down' in objdef.actions[0].lower():
            dir = Direction.DOWN

    Climbable.climb(player, dir)

def objectOptionOne_hot_vent_door(player):
    x = player.getClickX()
    y = player.getClickY()
    z = player.getPosition().getZ()
    if x == 2399 and y == 5176:
        if player.getPosition().getY() > y:
            Server.getSingleton().getFightPits().addWaitingPlayer(player)
            player.teleport(Position(2399, 5175, 0))
        else:
            player.teleport(Position(2399, 5177, 0))
            Server.getSingleton().getFightPits().quit(player)
    elif x == 2399 and y == 5168:
        if player.getPosition().getY() < y:
            Server.getSingleton().getFightPits().quit(player)

# Agility
def objectOptionOne_log_balance(player):
    handleObstacleForceWalk(player, 8,
        'You walk carefully across the slippery log...', '...You make it safely to the other side.')

def objectOptionOne_obstacle_net(player):
    pos = None
    if player.getPosition().getX() > 2480:
        obstacle = Obstacle.forObjectId(player.getClickId())
        pos = Position(player.getPosition().getX(),
                       obstacle.getPosition().getY(), player.getPosition().getZ())
    handleObstacleTeleport(player, 2, pos, 'You climb the netting...')

def objectOptionOne_tree_branch(player):
    if player.getPosition().getZ() == 1:
        msgs = ['You climb the tree...', '...To the platform above.']
        handleObstacleTeleport(player, 2, None, 'You climb the tree...', '...To the platform above.')
    elif player.getPosition().getZ() == 2:
        handleObstacleTeleport(player, 2, None, 'You climb the tree...', 'You land on the ground.')

def objectOptionOne_balancing_rope(player):
    handleObstacleForceWalk(player, 8, 'You carefully cross the tightrope.')

def objectOptionOne_obstacle_pipe(player):
    handleObstacleForceMove(player, 1, 20, 40, Misc.EAST, 'You squeeze through the pipe...')
    TackleObstacle.obstaclePipeExit(player, 3, 0, 5)
# (:
def objectOptionOne_tree(player):
    player.getActionDeque().clearAllActions()
    player.getActionDeque().addAction(
    Felling(player, RegionClipping.getRSObject(player,
    player.getClickX(), player.getClickY(), player.getPosition().getZ())))

def objectOptionOne_evergreen(player):
    player.getActionDeque().clearAllActions()
    player.getActionDeque().addAction(
    Felling(player, RegionClipping.getRSObject(player,
    player.getClickX(), player.getClickY(), player.getPosition().getZ())))

def objectOptionOne_oak(player):
    player.getActionDeque().clearAllActions()
    player.getActionDeque().addAction(
    Felling(player, RegionClipping.getRSObject(player,
    player.getClickX(), player.getClickY(), player.getPosition().getZ())))

def objectOptionOne_willow(player):
    player.getActionDeque().clearAllActions()
    player.getActionDeque().addAction(
    Felling(player, RegionClipping.getRSObject(player,
    player.getClickX(), player.getClickY(), player.getPosition().getZ())))

def objectOptionOne_maple_tree(player):
    player.getActionDeque().clearAllActions()
    player.getActionDeque().addAction(
    Felling(player, RegionClipping.getRSObject(player,
    player.getClickX(), player.getClickY(), player.getPosition().getZ())))

def objectOptionOne_yew(player):
    player.getActionDeque().clearAllActions()
    player.getActionDeque().addAction(
    Felling(player, RegionClipping.getRSObject(player,
    player.getClickX(), player.getClickY(), player.getPosition().getZ())))

def objectOptionOne_magic_tree(player):
    player.getActionDeque().clearAllActions()
    player.getActionDeque().addAction(
    Felling(player, RegionClipping.getRSObject(player,
    player.getClickX(), player.getClickY(), player.getPosition().getZ())))

def objectOptionOne_rune_essence(player):  # essence mining
    player.getActionDeque().clearAllActions()
    player.getActionDeque().addAction(
    Mining(player, RegionClipping.getRSObject(player,
    player.getClickX(), player.getClickY(), player.getPosition().getZ())))

def objectOptionOne_rocks(player):  # mining
    player.getActionDeque().clearAllActions()
    player.getActionDeque().addAction(
    Mining(player, RegionClipping.getRSObject(player,
    player.getClickX(), player.getClickY(), player.getPosition().getZ())))

def objectOptionOne_altar(player):  # prayer / runecrafting  
    if player.getClickId() == 2478:
        rune = Rune.forId(556)
    elif player.getClickId() == 2479:
        rune = Rune.forId(558)
    elif player.getClickId() == 2480:
        rune = Rune.forId(555)
    elif player.getClickId() == 2481:
        rune = Rune.forId(557)
    elif player.getClickId() == 2482:
        rune = Rune.forId(554)
    elif player.getClickId() == 2483:
        rune = Rune.forId(559)
    elif player.getClickId() == 2484:
        rune = Rune.forId(564)
    elif player.getClickId() == 2485:
        rune = Rune.forId(563)
    elif player.getClickId() == 2486:
        rune = Rune.forId(561)
    elif player.getClickId() == 2487:
        rune = Rune.forId(562)
    elif player.getClickId() == 2488:
        rune = Rune.forId(560)
    player.getActionDeque().addAction(Runecrafting(player, rune))
    



def objectOptionTwo_furnace(player):
    player.getActionSender().sendItemOnInterface(2406, 150, 2351)
    player.getActionSender().sendItemOnInterface(2407, 150, 2355)
    player.getActionSender().sendItemOnInterface(2409, 150, 2353)
    player.getActionSender().sendItemOnInterface(2410, 150, 2357)
    player.getActionSender().sendItemOnInterface(2411, 150, 2359)
    player.getActionSender().sendItemOnInterface(2412, 150, 2361)
    player.getActionSender().sendItemOnInterface(2413, 150, 2363)

    player.getActionSender().sendChatInterface(2400)

def objectOptionTwo_Spinning_wheel(player):  # testing.
    spin = Spinnable.forId(1737)  # noob.
    player.getActionDeque().addAction(WheelSpinning(player, 1, spin))

def objectOptionTwo_gem_stall(player):
    obj = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ())
    player.getActionDeque().addAction(Shoplift(player, obj, 0))

def objectOptionTwo_silver_stall(player):
    obj = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ())
    player.getActionDeque().addAction(Shoplift(player, obj, 0))

def objectOptionTwo_bakers_stall(player):
    obj = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ())
    player.getActionDeque().addAction(Shoplift(player, obj, 0))

def objectOptionTwo_silk_stall(player):
    obj = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ())
    player.getActionDeque().addAction(Shoplift(player, obj, 0))

def objectOptionTwo_fur_stall(player):
    obj = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ())
    player.getActionDeque().addAction(Shoplift(player, obj, 0))

def objectOptionTwo_spice_stall(player):
    obj = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ())
    player.getActionDeque().addAction(Shoplift(player, obj, 0))

def objectOptionTwo_general_stall(player):
    obj = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ())
    player.getActionDeque().addAction(Shoplift(player, obj, 0))

def objectOptionTwo_magic_stall(player):
    obj = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ())
    player.getActionDeque().addAction(Shoplift(player, obj, 0))

def objectOptionTwo_scimitar_stall(player):
    obj = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ())
    player.getActionDeque().addAction(Shoplift(player, obj, 0))

def objectOptionTwo_food_stall(player):
    obj = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ())
    player.getActionDeque().addAction(Shoplift(player, obj, 0))

def objectOptionTwo_crafting_stall(player):
    obj = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ())
    player.getActionDeque().addAction(Shoplift(player, obj, 0))

def objectOptionTwo_veg_stall(player):
    obj = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ())
    player.getActionDeque().addAction(Shoplift(player, obj, 0))

def objectOptionTwo_fish_stall(player):
    obj = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ())
    player.getActionDeque().addAction(Shoplift(player, obj, 0))

def objectOptionTwo_market_stall(player):
    obj = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ())
    player.getActionDeque().addAction(Shoplift(player, obj, 0))

def objectOptionTwo_seed_stall(player):
    obj = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ())
    player.getActionDeque().addAction(Shoplift(player, obj, 0))

def objectOptionTwo_tea_stall(player):
    obj = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ())
    player.getActionDeque().addAction(Shoplift(player, obj, 0))

def objectOptionTwo_counter(player):
    obj = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ())
    player.getActionDeque().addAction(Shoplift(player, obj, 0))
    
def objectOptionTwo_flax(player):
    player.getActionDeque().addAction(NiggerJob(player, 0))
    

def handleObstacleForceMove(player, ticks, speed, altspeed, dir, *msgs):
    obstacle = Obstacle.forObjectId(player.getClickId())
    TackleObstacle.obstacleForceMovement(player, obstacle, ticks, speed, altspeed, dir, msgs)
    TackleObstacle.updateCourseState(player, obstacle, 'gnomeAgilityCourseState')

def handleObstacleForceWalk(player, ticks, *msgs):
    obstacle = Obstacle.forObjectId(player.getClickId())
    TackleObstacle.obstacleForceWalk(player, obstacle, ticks, msgs)
    TackleObstacle.updateCourseState(player, obstacle, 'gnomeAgilityCourseState')

def handleObstacleTeleport(player, ticks, pos, *msgs):
    obstacle = Obstacle.forObjectId(player.getClickId())
    TackleObstacle.obstacleTeleport(player, obstacle, ticks, pos, msgs)
    TackleObstacle.updateCourseState(player, obstacle, 'gnomeAgilityCourseState')

def updateAlchemistInterface(player):
    player.getActionSender().sendString(str(player.getAlchemyPizazz()), 15896)
    player.getActionSender().sendString(str(AlchemistPlayground.getRewardPrices().get(6893)), 15902)  # boots
    player.getActionSender().sendString(str(AlchemistPlayground.getRewardPrices().get(6894)), 15903)  # kite
    player.getActionSender().sendString(str(AlchemistPlayground.getRewardPrices().get(6895)), 15904)  # helm
    player.getActionSender().sendString(str(AlchemistPlayground.getRewardPrices().get(6896)), 15905)  # emerald
    player.getActionSender().sendString(str(AlchemistPlayground.getRewardPrices().get(6897)), 15906)  # sword
    player.getActionSender().showComponent(AlchemistPlayground.getCurrentComponent(), True)

def updateEnchantInterface(player):
    player.getActionSender().showComponent(EnchantmentChamber.getCurrentComponent(), True)
    player.getActionSender().sendString(str(player.getEnchantPizazz()), 15921)

def updateGraveyardInterface(player):
    player.getActionSender().sendString(str(player.getGravePizazz()), 15935)


class CollectionDelay(Task):
    '''
    The delay between item collections.
    '''

    def __init__(self, player):
        super(CollectionDelay, self).__init__(2)
        self.player = player

    def execute(self):
        self.player.removeAttribute('canTake')
        self.stop()
        
class FancyBoots(Task):
    
    def __init__(self, player):
        super(FancyBoots, self).__init__(1, True)
        self.player = player
        self.boots = Item(9005)
        
    def execute(self):
        self.player.getInventory().addItem(self.boots)
        self.stop()
        
class FighterBoots(Task):
    
    def __init__(self, player):
        super(FighterBoots, self).__init__(1, True)
        self.player = player
        self.boots = Item(9006)
        
    def execute(self):
        self.player.getInventory().addItem(self.boots)
        self.stop()
