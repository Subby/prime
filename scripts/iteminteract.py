# Item Interaction Script
# Created by Joshua Barry <Ares>
# January 20 2013 : 2:00PM

from com.asgarniars.rs2.model import Position, World
from com.asgarniars.rs2.action.impl import ItemInteraction
from com.asgarniars.rs2.model.area import Area
from com.asgarniars.rs2.model.items import Item, ItemManager
from com.asgarniars.rs2.content.clue import Clue, Map, Riddle, Type
from com.asgarniars.rs2.content.dialogue.impl import ProgressHat
from com.asgarniars.rs2.content.book.impl import Baxtorian
from com.asgarniars.rs2.content.skills.cook import Cookable, Cooking
from com.asgarniars.rs2.content.skills.craft import Glass, GlassMelting
from com.asgarniars.rs2.content.skills.prayer import BoneType, BurialAction

from com.asgarniars.rs2.task import Task


def itemInteractOptionOne_bandages(player, item):
    if player.getInventory().removeItem(item):
        player.getSkill().increaseLevelOnce(3, 5)

def itemInteractOptionOne_clue_scroll(player, item):

    scrollId = item.getId()
    clue = None

    if item.getId() >= 10180:
        map = Map.forId(scrollId)
        player.setAttribute('clue_map', map.getScrollId())
        clue = Clue(map.getDifficulty(), Type.MAPS)

    else:
        riddle = Riddle.forId(scrollId)
        player.setAttribute('clue_riddle', riddle.getScrollId())
        clue = Clue(riddle.getDifficulty(), Type.RIDDLE)

    if clue != None:
        clue.read(player)

def itemInteractOptionOne_book_on_baxtorian(player, item):
    session = Baxtorian(player)
    player.read(session.evaluate())
    player.getActionSender().sendMessage('The book is old with many pages missing,')
    player.getActionSender().sendMessage('a few are translated from elven into common tongue.')

def itemInteractOptionOne_progress_hat(player, item):
    dialogue = ProgressHat(player, None)
    player.open(dialogue.evaluate())

def itemInteractOptionOne_bones(player, item):
    player.getActionSender().sendMessage("You dig a hole in the ground and bury the bones.")
    player.getActionDeque().addAction(BurialAction(player, BoneType.forId(item.getId())))
    
def itemInteractOptionOne_wolf_bones(player, item):
    player.getActionSender().sendMessage("You dig a hole in the ground and bury the bones.")
    player.getActionDeque().addAction(BurialAction(player, BoneType.forId(item.getId())))
    
def itemInteractOptionOne_burnt_bones(player, item):
    player.getActionSender().sendMessage("You dig a hole in the ground and bury the bones.")
    player.getActionDeque().addAction(BurialAction(player, BoneType.forId(item.getId())))
    
def itemInteractOptionOne_monkey_bones(player, item):
    player.getActionSender().sendMessage("You dig a hole in the ground and bury the bones.")
    player.getActionDeque().addAction(BurialAction(player, BoneType.forId(item.getId())))
    
def itemInteractOptionOne_bat_bones(player, item):
    player.getActionSender().sendMessage("You dig a hole in the ground and bury the bones.")
    player.getActionDeque().addAction(BurialAction(player, BoneType.forId(item.getId())))
    
def itemInteractOptionOne_big_bones(player, item):
    player.getActionSender().sendMessage("You dig a hole in the ground and bury the bones.")
    player.getActionDeque().addAction(BurialAction(player, BoneType.forId(item.getId())))
    
def itemInteractOptionOne_jogre_bones(player, item):
    player.getActionSender().sendMessage("You dig a hole in the ground and bury the bones.")
    player.getActionDeque().addAction(BurialAction(player, BoneType.forId(item.getId())))
    
def itemInteractOptionOne_zogre_bones(player, item):
    player.getActionSender().sendMessage("You dig a hole in the ground and bury the bones.")
    player.getActionDeque().addAction(BurialAction(player, BoneType.forId(item.getId())))
    
def itemInteractOptionOne_baby_dragon_bones(player, item):
    player.getActionSender().sendMessage("You dig a hole in the ground and bury the bones.")
    player.getActionDeque().addAction(BurialAction(player, BoneType.forId(item.getId())))
    
def itemInteractOptionOne_wvyern_bones(player, item):
    player.getActionSender().sendMessage("You dig a hole in the ground and bury the bones.")
    player.getActionDeque().addAction(BurialAction(player, BoneType.forId(item.getId())))
    
def itemInteractOptionOne_dragon_bones(player, item):
    player.getActionSender().sendMessage("You dig a hole in the ground and bury the bones.")
    player.getActionDeque().addAction(BurialAction(player, BoneType.forId(item.getId())))
    
def itemInteractOptionOne_dagannoth_bones(player, item):
    player.getActionSender().sendMessage("You dig a hole in the ground and bury the bones.")
    player.getActionDeque().addAction(BurialAction(player, BoneType.forId(item.getId())))
    
def itemInteractOptionOne_ourg_bones(player, item):
    player.getActionSender().sendMessage("You dig a hole in the ground and bury the bones.")
    player.getActionDeque().addAction(BurialAction(player, BoneType.forId(item.getId())))

def itemInteractOptionOne_toy_horsey(player, item):
    player.getActionDeque().addAction(ItemInteraction(player, item, 0))

def itemInteractOptionOne_spade(player, item):
    player.getUpdateFlags().sendAnimation(831)
    World.submit(Dig(player))

def itemInteractOptionOne_Newcomer_map(player, item):
    player.getActionSender().sendInterface(5392)
    player.getActionSender().sendMessage("You unfold the map and take a look at it's contents.")

def itemInteractOptionTwo_Armadyl_godsword(player, item):
    player.getActionSender().sendMessage("You can't dismantle this weapon yet!")

def itemOnObject_2783(player, item):
    player.getActionSender().sendConfig(211, 20)  # bars
    player.getActionSender().sendConfig(210, 99)  # level
    player.getActionSender().sendInterface(994)  # interface
    #player.getActionSender().sendConfig(262, 1)  # oil
    #player.getActionSender().sendConfig(261, 1)  # dart tips apparently.

def itemOnObject_2020(player, item):
    player.getActionSender().sendMessage('You tie the old rope to the tree.')
    World.submit(ClimbTree(player))

# Cauldron of Thunder
def itemOnObject_2142(player, item):

    def swapItem(item, id):
        player.getInventory().removeItem(item)
        player.getInventory().addItem(Item(id, item.getCount()))

    itemId = item.getId()
    meats = {2132:522, 2134:523, 2136:524, 2138:525}
    if itemId in meats.keys():
        swapItem(item, meats.get(itemId))
        player.getActionSender().sendMessage('You dip the %s in the cauldron.'
                                            % ItemManager.getInstance().getItemName(itemId).lower())

# Range
def itemOnObject_114(player, item):
    player.getActionDeque().addAction(Cooking(player, Cookable.forUncooked(item.getId())))

def itemOnObject_2728(player, item):
    player.getActionDeque().addAction(Cooking(player, Cookable.forUncooked(item.getId())))

def itemOnObject_2859(player, item):
    player.getActionDeque().addAction(Cooking(player, Cookable.forUncooked(item.getId())))

# Furnace
def itemOnObject_11666(player, item):
    player.getActionDeque().addAction(GlassMelting(player, 1, Glass.forReward(1775)))

def itemOnObject_10803(player, item):
    if Arena.getChamberArea().isInArea(player.getPosition()):
        if player.getInventory().getItemContainer().contains(6902):
            item = player.getInventory().getItemContainer().getById(6902)
            if player.getInventory().removeItem(item):
                print 'lelele'

def itemOnObject_1992(player, item):
    player.getActionSender().sendMessage("You place the pebble in the gravestone's small indent.")
    World.submit(InsertPebble(player))

def itemOnObject_1996(player, item):
    if item.getId() == 954:
        player.getUpdateFlags().sendAnimation(774)
        player.getUpdateFlags().sendGraphic(67)
        player.setAttribute("tempWalkAnim", 776)
        player.setAppearanceUpdateRequired(True)
        World.submit(LassoBoulder(player))

def  itemOnObject_2004(player, item):
    print 'HANDLE ME PLZ NEEDS PROPER X COORDS q_q'

class LassoBoulder(Task):

    def __init__(self, player):
        super(LassoBoulder, self).__init__(2)
        self.player = player

    def execute(self):
        self.player.getMovementHandler().addToPath(Position(2512, 3469, 0))
        self.stop()

class InsertPebble(Task):

    def __init__(self, player):
        super(InsertPebble, self).__init__(2)
        self.player = player

    def execute(self):
        self.player.getActionSender().sendMessage('It fits perfectly.')
        World.submit(TombCreak(self.player))
        self.stop()

class TombCreak(Task):

    def __init__(self, player):
        super(TombCreak, self).__init__(1)
        self.player = player

    def execute(self):
        self.player.getActionSender().sendMessage('You hear a loud creak.')
        World.submit(SlabSlide(self.player))
        self.stop()

class SlabSlide(Task):

    def __init__(self, player):
        super(SlabSlide, self).__init__(1)
        self.player = player

    def execute(self):
        self.player.getActionSender().sendMessage('The stone slab slides back revealing a ladder down.')
        World.submit(EnterGlarialsTomb(self.player))
        self.stop()

class EnterGlarialsTomb(Task):

    def __init__(self, player):
        super(EnterGlarialsTomb, self).__init__(1)
        self.player = player

    def execute(self):
        self.player.clipTeleport(Position(2554, 9844, 0), 2)
        self.stop()

class ClimbTree(Task):
    '''
    When the player climbs the tree in the waterfall quest.
    '''

    def __init__(self, player):
        self.player = player

    def execute(self):
        self.player.teleport(Position(2511, 3463, 0))
        self.player.getActionSender().sendMessage('and let your self down on to the ledge.')
        self.stop()

class Dig(Task):

    '''
    When a player digs with a spade
    '''

    def __init__(self, player):
        super(Dig, self).__init__(2)
        self.player = player

    def execute(self):
        self.player.getUpdateFlags().sendAnimation(-1)
        
        tombs = {Area(Position(3562, 3286, 0), Position(3567, 3291, 0)) : Position(3557, 9703, 3), #ahrim
                 Area(Position(3574, 3279, 0), Position(3579, 3286, 0)) : Position(3535, 9704, 3), # guthan
                 Area(Position(3572, 3295, 0), Position(3577, 3301, 0)) : Position(3556, 9718, 3), # D'harok
                 Area(Position(3563, 3272, 0), Position(3568, 3279, 0)) : Position(3546, 9684, 3), #Karil
                 Area(Position(3550, 3279, 0), Position(3556, 3286, 0)) : Position(3568, 9683, 3), #Torag
                 Area(Position(3553, 3294, 0), Position(3560, 3301, 0)) : Position(3578, 9706, 3) # verac
                 }
        
        for tomb in tombs.items():
            if tomb[0].isInArea(self.player.getPosition()):
                self.player.teleport(tomb[1])
                self.player.getActionSender().sendMessage("You've broken into a crypt!")
                self.player.getActionSender().sendMinimapBlackout(2)
        
        for map in Map.values():
            if map.getArea().isInArea(self.player.getPosition()):
                if self.player.getInventory().getItemContainer().contains(map.getScrollId()):
                    scroll = Item(map.getScrollId())
                    if self.player.getInventory().removeItem(scroll):
                        clue = Clue(map.getDifficulty(), Type.MAPS)
                        clue.complete(self.player)
        self.stop()
