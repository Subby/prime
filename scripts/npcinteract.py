from com.asgarniars.rs2.content import SheepShearer
from com.asgarniars.rs2.content.dialogue import ChatDialogue
from com.asgarniars.rs2.content.dialogue.impl import Almera, Aubury, Doric, \
	DukeHoracio, FredTheFarmer, Golrie, Kaqemeex, Sanfew, Sedridor, FatherAereck, \
	FatherUrhney, Hetty, Cook, Hudon, Hadley, WiseOldMan
from com.asgarniars.rs2.content.shop import ShopManager
from com.asgarniars.rs2.content.skills.fish import Fishable, Tools, \
	Fishing
from com.asgarniars.rs2.content.skills.thief import Pickpocketing
from com.asgarniars.rs2.model.players import BankManager
from com.asgarniars.rs2.util import Misc


# Option 1 below

def npcInteractOptionOne_Makeover_Mage(player, npc):
	player.getActionSender().sendInterface(3559)

def npcInteractOptionOne_Wise_Old_Man(player, *npc):
	session = WiseOldMan(player, npc)
	session.initiate()

def npcInteractOptionOne_Golrie(player, *npc):
	session = Golrie(player, npc)
	session.initiate()

def npcInteractOptionOne_Sheep(player, npc):
	SheepShearer.getSingleton().shear(player, npc)

def npcInteractOptionOne_Man(player, npc):
	message = getRandomMessage(["Sorry, were you speaking to me? I was daydreaming.",
						"Hello, anyway."],
					["Hello, I'm glad to see an adventurer about.",
						"There's an increase in goblins hanging around the area."],
					[ "Hello to you too, adventurer."],
					['Welcome to Lumbridge.'],
					["Hey, do you like my clothes? They're new."],
					["Don't come near me, I have a cold!"])
	dialogue = ChatDialogue(npc, None, message)
	player.open(dialogue)

def npcInteractOptionOne_Charmed_Warrior(player, npc):
	dialogue = ChatDialogue(player, None, ['Is anybody there?'])
	dialogue.add(npc, None, ['What do you think?'])
	player.open(dialogue)

def npcInteractOptionOne_Almera(player, *npc):
	dialogue = Almera(player, npc)
	dialogue.initiate()

def npcInteractOptionOne_Aubury(player, *npc):
	dialogue = Aubury(player, npc)
	dialogue.initiate()

def npcInteractOptionOne_Doric(player, *npc):
    dialogue = Doric(player, npc)
    dialogue.initiate()

def npcInteractOptionOne_Duke_Horacio(player, *npc):
	dialogue = DukeHoracio(player, npc)
	dialogue.initiate()

def npcInteractOptionOne_Sedridor(player, *npc):
	dialogue = Sedridor(player, npc)
	dialogue.initiate()

def npcInteractOptionOne_Father_Aereck(player, *npc):
	dialogue = FatherAereck(player, npc)
	dialogue.initiate()

def npcInteractOptionOne_Father_Urhney(player, *npc):
	dialogue = FatherUrhney(player, npc)
	dialogue.initiate()

def npcInteractOptionOne_Hetty(player, *npc):
	dialogue = Hetty(player, npc)
	dialogue.initiate()

def npcInteractOptionOne_Cook(player, *npc):
	dialogue = Cook(player, npc)
	dialogue.initiate()

def npcInteractOptionOne_Hudon(player, *npc):
	dialogue = Hudon(player, npc)
	dialogue.initiate()

def npcInteractOptionOne_Hadley(player, *npc):
	dialogue = Hadley(player, npc)
	dialogue.initiate()

def npcInteractOptionOne_Fred_the_Farmer(player, *npc):
	dialogue = FredTheFarmer(player, npc)
	dialogue.initiate()

def npcInteractOptionOne_Kaqemeex(player, *npc):
	dialogue = Kaqemeex(player, npc)
	dialogue.initiate()

def npcInteractOptionOne_Sanfew(player, *npc):
	dialogue = Sanfew(player, npc)
	dialogue.initiate()

def npcInteractOptionOne_Woman(player, npc):
	message = getRandomMessage(["I can't believe that Lachtopher boy.",
						"He tried to borrow money from me again."],
					["You're not from around here, are you?", "I can see it in your eyes."],
					[ "Hello to you too, adventurer."],
					['Welcome to Lumbridge.'],
					["Hey, do you like my clothes? They're new."],
					["You've chosen a lovely day to visit us. Welcome."])
	dialogue = ChatDialogue(npc, None, message)
	player.open(dialogue)

def npcInteractOptionOne_Fishing_spot(player, npc):  # Fishing spot
	if npc.getNpcId() == 316:  # Small net fishing
		tool = Tools.forId(303)
		fish = Fishable.forId(int(tool.getOutcomes()[0]))
		player.getActionDeque().addAction(
		Fishing(player, fish, tool))
	elif npc.getNpcId() == 324:  # Lobster fishing
		tool = Tools.forId(301)
		fish = Fishable.forId(int(tool.getOutcomes()[0]))
		player.getActionDeque().addAction(
		Fishing(player, fish, tool))
	elif npc.getNpcId() == 334:  # Big net fishing
		tool = Tools.forId(305)
		fish = Fishable.forId(int(tool.getOutcomes()[0]))
		player.getActionDeque().addAction(
		Fishing(player, fish, tool))


# Option 2 below

def npcInteractOptionTwo_Banker(player, npc):
	BankManager().openBank(player)

#Shops

def npcInteractOptionTwo_Shopkeeper(player, npc):
	if npc.getDefinition().getId() == 520:
		ShopManager.open(player, 104) #Lumbridge Shopkeeper
	elif npc.getDefinition().getId() == 522:
		ShopManager.open(player, 151) #Varrock Shopkeeper
	if npc.getDefinition().getId() == 524:
		ShopManager.open(player, 5) #Al Kharid Shopkeeper
	elif npc.getDefinition().getId() == 526:
		ShopManager.open(player, 56) #Falador Shopkeeper
	

def npcInteractOptionTwo_Shop_assistant(player, npc):
	if npc.getDefinition().getId() == 521:
		ShopManager.open(player, 104) #Lumbridge Shop Assistant
	elif npc.getDefinition().getId() == 523:
		ShopManager.open(player, 151) #Varrock Shop Assistant
	elif npc.getDefinition().getId() == 525:
		ShopManager.open(player, 5) #Al Kharid Shop Assistant
	elif npc.getDefinition().getId() == 527:
		ShopManager.open(player, 56) #Falador Shop Assistant

def npcInteractOptionTwo_Urbi(player, npc):
	ShopManager.open(player, 0)	#Urbi's pking shop

def npcInteractOptionTwo_Aubury(player, npc):
	ShopManager.open(player, 1)	#Aubury's runes shop

def npcInteractOptionTwo_Dommik(player, npc):
	ShopManager.open(player, 2)	#Dommik's skilling shop	

def npcInteractOptionTwo_Gaius(player, npc):
	ShopManager.open(player, 3)	#Gaius' weapon shop		

def npcInteractOptionTwo_Harry(player, npc):
	ShopManager.open(player, 4)	#Harry's fishing shop



# Delete these shops
'''
def npcInteractOptionTwo_Charley(player, npc):
	ShopManager.open(player, 0)
	
def npcInteractOptionTwo_Aaron(player, npc):
	ShopManager.open(player, 1)
	
def npcInteractOptionTwo_Aemad(player, npc):
	ShopManager.open(player, 2)
	
def npcInteractOptionTwo_Agmundi(player, npc):
	ShopManager.open(player, 3)
	
def npcInteractOptionTwo_Ak_Haranu(player, npc):
	ShopManager.open(player, 4)
	
def npcInteractOptionTwo_Ali_the_Carter(player, npc):
	ShopManager.open(player, 6)
	
def npcInteractOptionTwo_Ali_the_Kebab_seller(player, npc):
	ShopManager.open(player, 7)
	
def npcInteractOptionTwo_Alice(player, npc):
	ShopManager.open(player, 9)
	
def npcInteractOptionTwo_Apothecary(player, npc):
	ShopManager.open(player, 10)
	
def npcInteractOptionTwo_Baker(player, npc):
	ShopManager.open(player, 11)
	
def npcInteractOptionTwo_Fur_trader(player, npc):
	ShopManager.open(player, 12)
	
def npcInteractOptionTwo_Gem_merchant(player, npc):
	ShopManager.open(player, 13)
		
def npcInteractOptionTwo_Silver_merchant(player, npc):
	ShopManager.open(player, 14)
	
def npcInteractOptionTwo_Spice_seller(player, npc):
	ShopManager.open(player, 15)
	
def npcInteractOptionTwo_Arhein(player, npc):
	ShopManager.open(player, 16)
	
def npcInteractOptionTwo_Arnold(player, npc):
	ShopManager.open(player, 17) #and 18.
	
def npcInteractOptionTwo_Aubury(player, npc):
	ShopManager.open(player, 19)
	
def npcInteractOptionTwo_Aurel(player, npc):
	ShopManager.open(player, 20)
	
def npcInteractOptionTwo_Baraek(player, npc):
	ShopManager.open(player, 25)
	
def npcInteractOptionTwo_Betty(player, npc):
	ShopManager.open(player, 28)
	
def npcInteractOptionTwo_Bob(player, npc):
	ShopManager.open(player, 30)
	
def npcInteractOptionTwo_Brian(player, npc):
	if npc.getDefinition().getId() == 1860:
		ShopManager.open(player, 33)
	elif npc.getDefinition().getId() == 559:
		ShopManager.open(player, 34)
		
def npcInteractOptionTwo_Cassie(player, npc):
	ShopManager.open(player, 38)
	
def npcInteractOptionTwo_Flynn(player, npc):
	ShopManager.open(player, 61)
	
def npcInteractOptionTwo_Gerrant(player, npc):
	ShopManager.open(player, 73)
	
def npcInteractOptionTwo_Peksa(player, npc):
	ShopManager.open(player, 83)
	
def npcInteractOptionTwo_Herquin(player, npc):
	ShopManager.open(player, 84)

def npcInteractOptionTwo_Horvik(player, npc):
	ShopManager.open(player, 87)
	
def npcInteractOptionTwo_Lowe(player, npc):
	ShopManager.open(player, 103)	
	
def npcInteractOptionTwo_Rommik(player, npc):
	ShopManager.open(player, 128)
	
def npcInteractOptionTwo_Thessalia(player, npc):
	ShopManager.open(player, 142)
	
def npcInteractOptionTwo_Wayne(player, npc):
	ShopManager.open(player, 154)
	
def npcInteractOptionTwo_Zaff(player, npc):
	ShopManager.open(player, 158)
	
#End
'''	
#End

def npcInteractOptionTwo_Man(thief, victim):
	thief.getActionDeque().addAction(Pickpocketing(thief, victim, 0))

def npcInteractOptionTwo_Woman(thief, victim):
	thief.getActionDeque().addAction(Pickpocketing(thief, victim, 0))

def npcInteractOptionTwo_Farmer(thief, victim):
	thief.getActionDeque().addAction(Pickpocketing(thief, victim, 0))

def npcInteractOptionTwo_HAM_Member(thief, victim):
	thief.getActionDeque().addAction(Pickpocketing(thief, victim, 0))

def npcInteractOptionTwo_Warrior_woman(thief, victim):
	thief.getActionDeque().addAction(Pickpocketing(thief, victim, 0))

def npcInteractOptionTwo_Rogue(thief, victim):
	thief.getActionDeque().addAction(Pickpocketing(thief, victim, 0))

def npcInteractOptionTwo_Cave_goblin(thief, victim):
	thief.getActionDeque().addAction(Pickpocketing(thief, victim, 0))

def npcInteractOptionTwo_Guard(thief, victim):
	thief.getActionDeque().addAction(Pickpocketing(thief, victim, 0))
	
def npcInteractOptionOne_Master_farmer(thief, victim):
	thief.getActionDeque().addAction(Pickpocketing(thief, victim, 0))
	
def npcInteractOptionTwo_Hero(thief, victim):
	theif.getActionDeque().addAction(Pickpocketing(thief, victim, 0))
	
def npcInteractOptionTwo_Gnome(thief, victim):
	thief.getActionDeque().addAction(Pickpocketing(thief, victim, 0))


def npcInteractOptionTwo_Ellis(player, npc):  # Leather tanning
	player.getActionSender().sendString("Soft leather", 14777)
	player.getActionSender().sendString('@gre@ 1 Coin', 14785)
	player.getActionSender().sendString("Hard leather", 14778)
	player.getActionSender().sendString('@gre@ 3 Coins', 14786)
	player.getActionSender().sendString("Snake skin", 14779)
	player.getActionSender().sendString('@gre@ 15 Coins', 14787)
	player.getActionSender().sendString("Snake skin", 14780)
	player.getActionSender().sendString('@gre@ 20 Coins', 14788)
	player.getActionSender().sendString("Green D'hide", 14781)
	player.getActionSender().sendString('@gre@ 20 Coins', 14789)
	player.getActionSender().sendString("Blue D'hide", 14782)
	player.getActionSender().sendString('@gre@ 20 Coins', 14790)
	player.getActionSender().sendString("Red D'hide", 14783)
	player.getActionSender().sendString('@gre@ 20 Coins', 14791)
	player.getActionSender().sendString("Black D'hide", 14784)
	player.getActionSender().sendString('@gre@ 20 Coins', 14792)

	player.getActionSender().sendItemOnInterface(14769, 220, 1739)
	player.getActionSender().sendItemOnInterface(14770, 220, 1739)
	player.getActionSender().sendItemOnInterface(14771, 220, 6287)
	player.getActionSender().sendItemOnInterface(14772, 220, 7801)
	player.getActionSender().sendItemOnInterface(14773, 220, 1753)
	player.getActionSender().sendItemOnInterface(14774, 220, 1751)
	player.getActionSender().sendItemOnInterface(14775, 220, 1749)
	player.getActionSender().sendItemOnInterface(14776, 220, 1747)
	player.getActionSender().sendInterface(14670)


def npcInteractOptionTwo_Sbott(player, npc):  # Leather tanning
	player.getActionSender().sendString("Soft leather", 14777)
	player.getActionSender().sendString('@gre@ 2 Coins', 14785)
	player.getActionSender().sendString("Hard leather", 14778)
	player.getActionSender().sendString('@gre@ 5 Coins', 14786)
	player.getActionSender().sendString("Snake skin", 14779)
	player.getActionSender().sendString('@gre@ 25 Coins', 14787)
	player.getActionSender().sendString("Snake skin", 14780)
	player.getActionSender().sendString('@gre@ 45 Coins', 14788)
	player.getActionSender().sendString("Green D'hide", 14781)
	player.getActionSender().sendString('@gre@ 45 Coins', 14789)
	player.getActionSender().sendString("Blue D'hide", 14782)
	player.getActionSender().sendString('@gre@ 45 Coins', 14790)
	player.getActionSender().sendString("Red D'hide", 14783)
	player.getActionSender().sendString('@gre@ 45 Coins', 14791)
	player.getActionSender().sendString("Black D'hide", 14784)
	player.getActionSender().sendString('@gre@ 45 Coins', 14792)
	player.getActionSender().sendItemOnInterface(14769, 220, 1739)
	player.getActionSender().sendItemOnInterface(14770, 220, 1739)
	player.getActionSender().sendItemOnInterface(14771, 220, 6287)
	player.getActionSender().sendItemOnInterface(14772, 220, 7801)
	player.getActionSender().sendItemOnInterface(14773, 220, 1753)
	player.getActionSender().sendItemOnInterface(14774, 220, 1751)
	player.getActionSender().sendItemOnInterface(14775, 220, 1749)
	player.getActionSender().sendItemOnInterface(14776, 220, 1747)
	player.getActionSender().sendInterface(14670)

def npcInteractOptionTwo_Tanner(player, npc):  # Leather tanning
	# 2824 - models : 14769-14776
	player.getActionSender().sendString("Soft leather", 14777)
	player.getActionSender().sendString('@gre@ 1 Coin', 14785)
	player.getActionSender().sendString("Hard leather", 14778)
	player.getActionSender().sendString('@gre@ 3 Coins', 14786)
	player.getActionSender().sendString("Snake skin", 14779)
	player.getActionSender().sendString('@gre@ 15 Coins', 14787)
	player.getActionSender().sendString("Snake skin", 14780)
	player.getActionSender().sendString('@gre@ 20 Coins', 14788)
	player.getActionSender().sendString("Green D'hide", 14781)
	player.getActionSender().sendString('@gre@ 20 Coins', 14789)
	player.getActionSender().sendString("Blue D'hide", 14782)
	player.getActionSender().sendString('@gre@ 20 Coins', 14790)
	player.getActionSender().sendString("Red D'hide", 14783)
	player.getActionSender().sendString('@gre@ 20 Coins', 14791)
	player.getActionSender().sendString("Black D'hide", 14784)
	player.getActionSender().sendString('@gre@ 20 Coins', 14792)
	player.getActionSender().sendItemOnInterface(14769, 220, 1739)
	player.getActionSender().sendItemOnInterface(14770, 220, 1739)
	player.getActionSender().sendItemOnInterface(14771, 220, 6287)
	player.getActionSender().sendItemOnInterface(14772, 220, 7801)
	player.getActionSender().sendItemOnInterface(14773, 220, 1753)
	player.getActionSender().sendItemOnInterface(14774, 220, 1751)
	player.getActionSender().sendItemOnInterface(14775, 220, 1749)
	player.getActionSender().sendItemOnInterface(14776, 220, 1747)
	player.getActionSender().sendInterface(14670)

def npcInteractOptionTwo_Fishing_spot(player, npc):
	if npc.getNpcId() in [313, 324, 334]:  # Harpoon fishing
		tool = Tools.forId(311)
		fish = Fishable.forId(Misc.generatedValue(tool.getOutcomes()))
		player.getActionDeque().addAction(
		Fishing(player, fish, tool))
	elif npc.getNpcId() == 316:  # Bait Fishing [rod]
		tool = Tools.forId(307)
		fish = Fishable.forId(Misc.generatedValue(tool.getOutcomes()))
		player.getActionDeque().addAction(
		Fishing(player, fish, tool))

def npcInteractOptionTwo_Fadli(player, npc):
	BankManager.openBank(player)
	
def npcInteractOptionTwo_Surgeon_General_Tafani(player, npc):
	print player.getInteractingEntity()

def getRandomMessage(*msgs):
	messages = {}
	for x, y in enumerate(msgs):
		messages.update({x:y})
	index = Misc.random(len(messages.values()))
	message = messages.values()[index]
	return message

