# Created by Joshua Barry <Ares> February 14 2013


from com.asgarniars.rs2.model.items import ItemManager

from com.asgarniars.rs2.content.quest import QuestRepository

from com.asgarniars.rs2.content.skills.craft import Hide, Glass, Craftable, \
	ArmourCreation, LeatherTanning, GlassBlowing

from com.asgarniars.rs2.content.skills.smith import Bar, Smelting
from com.asgarniars.rs2.util import Misc

def actionButton12711(player):
	player.flip(False)

def actionButton12713(player):
	player.flip(True)

def actionButton3987(player):
	bar = Bar.BRONZE
	smeltBar(player, bar)
	
def actionButton3991(player):
	bar = Bar.IRON
	smeltBar(player, bar)
	
def actionButton3999(player):
	bar = Bar.STEEL
	smeltBar(player, bar)
	
def actionButton7441(player):
	bar = Bar.MITHRIL
	smeltBar(player, bar)
	
def actionButton7444(player):
	bar = Bar.ADAMANT
	smeltBar(player, bar, 5)
	
def actionButton7446(player):
	bar = Bar.ADAMANT
	smeltBar(player, bar)
	
def actionButton7450(player):
	bar = Bar.RUNITE
	smeltBar(player, bar)
	
def actionButton7344(player):
	q = QuestRepository.get(7344)
	q.start(player)

def actionButton7350(player):
	q = QuestRepository.get(7350)
	q.start(player)

def actionButton8633(player):  # make 10 leather body 2415 - 2420 string ids
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 10, Craftable.forReward(1129)))
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')

def actionButton8634(player):  # make 5 leather body
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 5, Craftable.forReward(1129)))
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')

def actionButton8635(player):  # make leather body
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 1, Craftable.forReward(1129)))
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')
	
def actionButton8636(player):  # make 10 leather gloves
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 10, Craftable.forReward(1059)))
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')
	
def actionButton8637(player):  # make 5 leather gloves
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 5, Craftable.forReward(1059)))
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')	

def actionButton8638(player):  # make leather gloves
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 1, Craftable.forReward(1059)))
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')

def actionButton8639(player):  # make 10 leather boots
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 10, Craftable.forReward(1061)))
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')
	
def actionButton8640(player):  # make 5 leather boots
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 5, Craftable.forReward(1061)))
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')
	
def actionButton8641(player):  # make leather boots
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 1, Craftable.forReward(1061)))
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')	

def actionButton8642(player):  # make 10 leather vambraces
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 10, Craftable.forReward(1063)))
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')

def actionButton8643(player):  # make 5 leather vambraces
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 5, Craftable.forReward(1063)))
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')

def actionButton8644(player):  # make leather vambraces
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 1, Craftable.forReward(1063)))
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')

def actionButton8645(player):  # make 10 leather chaps
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 10, Craftable.forReward(1095)))
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')
	
def actionButton8646(player):  # make 5 leather chaps
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 5, Craftable.forReward(1095)))
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')			

def actionButton8647(player):  # make leather chaps
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 1, Craftable.forReward(1095)))
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')

def actionButton8648(player):  # make 10 leather coifs
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 10, Craftable.forReward(1169)))
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')

def actionButton8649(player):  # make 5 leather coifs
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 5, Craftable.forReward(1169)))
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')	

def actionButton8650(player):  # make leather coif
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 1, Craftable.forReward(1169)))
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')
	
def actionButton8651(player):  # make 10 leather cowl
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 10, Craftable.forReward(1167)))
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')

def actionButton8652(player):  # make 5 leather cowl
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 5, Craftable.forReward(1167)))
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')	
	
def actionButton8653(player):  # make leather cowl
	player.getActionSender().removeInterfaces()
	player.getActionDeque().addAction(ArmourCreation(player, 1, Craftable.forReward(1167)))	
	player.removeAttribute('craftingType')
	player.removeAttribute('craftingHide')
	
def actionButton6201(player):  # make 10 fish bowl
	player.getActionDeque().addAction(
	GlassBlowing(player, 10, Glass.forReward(6667)))
	player.getActionSender().removeInterfaces()
	
def actionButton6202(player):  # make 5 fish bowl
	player.getActionDeque().addAction(
	GlassBlowing(player, 5, Glass.forReward(6667)))
	player.getActionSender().removeInterfaces()								

def actionButton6203(player):  # make fish bowl
	player.getActionDeque().addAction(
	GlassBlowing(player, 1, Glass.forReward(6667)))
	player.getActionSender().removeInterfaces()

def actionButton11472(player):  # make 10 vials
	player.getActionDeque().addAction(
	GlassBlowing(player, 10, Glass.forReward(229)))
	player.getActionSender().removeInterfaces()

def actionButton11473(player):  # make 5 vials
	player.getActionDeque().addAction(
	GlassBlowing(player, 5, Glass.forReward(229)))
	player.getActionSender().removeInterfaces()

def actionButton11474(player):  # make vial
	player.getActionDeque().addAction(
	GlassBlowing(player, 1, Glass.forReward(229)))
	player.getActionSender().removeInterfaces()

def actionButton12394(player):  # make 10 uncharged orb
	player.getActionDeque().addAction(
	GlassBlowing(player, 10, Glass.forReward(567)))
	player.getActionSender().removeInterfaces()	

def actionButton12395(player):  # make 5 uncharged orb
	player.getActionDeque().addAction(
	GlassBlowing(player, 5, Glass.forReward(567)))
	player.getActionSender().removeInterfaces()		
	
def actionButton12396(player):  # make uncharged orb
	player.getActionDeque().addAction(
	GlassBlowing(player, 1, Glass.forReward(567)))
	player.getActionSender().removeInterfaces()	

def actionButton12398(player):  # make  10 beer glass
	player.getActionDeque().addAction(
	GlassBlowing(player, 10, Glass.forReward(1919)))
	player.getActionSender().removeInterfaces()
	
def actionButton12399(player):  # make 5 beer glass
	player.getActionDeque().addAction(
	GlassBlowing(player, 5, Glass.forReward(1919)))
	player.getActionSender().removeInterfaces()	

def actionButton12400(player):  # make beer glass
	player.getActionDeque().addAction(
	GlassBlowing(player, 1, Glass.forReward(1919)))
	player.getActionSender().removeInterfaces()	

def actionButton12402(player):  # make 10 candle lantern
	player.getActionDeque().addAction(
	GlassBlowing(player, 10, Glass.forReward(4527)))
	player.getActionSender().removeInterfaces()	

def actionButton12403(player):  # make 5 candle lantern
	player.getActionDeque().addAction(
	GlassBlowing(player, 5, Glass.forReward(4527)))
	player.getActionSender().removeInterfaces()		

def actionButton12404(player):  # make candle lantern
	player.getActionDeque().addAction(
	GlassBlowing(player, 1, Glass.forReward(4527)))
	player.getActionSender().removeInterfaces()	

def actionButton12406(player):  # make 10 oil lamp
	player.getActionDeque().addAction(
	GlassBlowing(player, 10, Glass.forReward(4522)))
	player.getActionSender().removeInterfaces()
	
def actionButton12407(player):  # make  5 oil lamp
	player.getActionDeque().addAction(
	GlassBlowing(player, 5, Glass.forReward(4522)))
	player.getActionSender().removeInterfaces()	

def actionButton12408(player):  # make oil lamp
	player.getActionDeque().addAction(
	GlassBlowing(player, 1, Glass.forReward(4522)))
	player.getActionSender().removeInterfaces()

def actionButton12410(player):  # make 10 lantern lens
	player.getActionDeque().addAction(
	GlassBlowing(player, 10, Glass.forReward(4542)))
	player.getActionSender().removeInterfaces()

def actionButton12411(player):  # make 5 lantern lens
	player.getActionDeque().addAction(
	GlassBlowing(player, 5, Glass.forReward(4542)))
	player.getActionSender().removeInterfaces()	

def actionButton12412(player):  # make lantern lens
	player.getActionDeque().addAction(
	GlassBlowing(player, 1, Glass.forReward(4542)))
	player.getActionSender().removeInterfaces()		
	
def actionButton14793(player):  # tan all cowhide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, player.getInventory().getItemContainer().getCount(1739), Hide.forReward(1741)))
			
def actionButton14794(player):  # tan all cowhide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, player.getInventory().getItemContainer().getCount(1739), Hide.forReward(1743)))

def actionButton14795(player):  # tan all snake hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, player.getInventory().getItemContainer().getCount(6287), Hide.forId(6287)))

def actionButton14796(player):  # tan all snake hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, player.getInventory().getItemContainer().getCount(7801), Hide.forId(7801)))

def actionButton14797(player):  # tan all green d'hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, player.getInventory().getItemContainer().getCount(1753), Hide.forId(1753)))

def actionButton14798(player):  # tan all blue d'hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, player.getInventory().getItemContainer().getCount(1751), Hide.forId(1751)))

def actionButton14799(player):  # tan all red d'hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, player.getInventory().getItemContainer().getCount(1749), Hide.forId(1749)))

def actionButton14800(player):  # tan all black d'hide
	if player.getInteractingEntity().isNpc():
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, player.getInventory().getItemContainer().getCount(1747), Hide.forId(1747)))

def actionButton14801(player):  # tan 'x' cowhide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.setEnterXInterfaceId(14801)
			player.getActionSender().sendEnterX()
		
def actionButton14802(player):  # tan 'x' cowd'hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.setEnterXInterfaceId(14802)
			player.getActionSender().sendEnterX()

def actionButton14803(player):  # tan 'x' snake hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.setEnterXInterfaceId(14803)
			player.getActionSender().sendEnterX()

def actionButton14804(player):  # tan 'x' snake hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.setEnterXInterfaceId(14804)
			player.getActionSender().sendEnterX()

def actionButton14805(player):  # tan 'x' green d'hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.setEnterXInterfaceId(14805)
			player.getActionSender().sendEnterX()
		

def actionButton14806(player):  # tan 'x' blue d'hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.setEnterXInterfaceId(14806)
			player.getActionSender().sendEnterX()

def actionButton14807(player):  # tan 'x' red d'hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.setEnterXInterfaceId(14807)
			player.getActionSender().sendEnterX()
		

def actionButton14808(player):  # tan 'x' black d'hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.setEnterXInterfaceId(14808)
			player.getActionSender().sendEnterX()
		
def actionButton14809(player):  # tan 5 cowhide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, 5, Hide.forReward(1741)))
			
def actionButton14810(player):  # tan 5 cowhide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, 5, Hide.forReward(1743)))
			
def actionButton14811(player):  # tan 5 snake hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, 5, Hide.forId(6287)))

def actionButton14812(player):  # tan 5 snake hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, 5, Hide.forId(7801)))

def actionButton14813(player):  # tan 5 green d'hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, 5, Hide.forId(1753)))
		
def actionButton14814(player):  # tan 5 blue d'hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, 5, Hide.forId(1751)))

def actionButton14815(player):  # tan 5 red d'hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, 5, Hide.forId(1749)))

def actionButton14816(player):  # tan 5 black d'hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, 5, Hide.forId(1747)))

def actionButton14817(player):  # tan cowhide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, 1, Hide.forReward(1741)))
		
def actionButton14818(player):  # tan cowhide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, 1, Hide.forReward(1743)))

def actionButton14819(player):  # tan snake hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, 1, Hide.forId(6287)))
		
def actionButton14820(player):  # tan snake hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, 1, Hide.forId(7801)))
			
def actionButton14821(player):  # tan green d'hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, 1, Hide.forId(1753)))

def actionButton14822(player):  # tan blue d'hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, 1, Hide.forId(1751)))



def actionButton14823(player):  # tan red d'hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, 1, Hide.forId(1749)))


def actionButton14824(player):  # tan black d'hide
	if player.getInteractingEntity().isNpc():
		name = player.getInteractingEntity().getDefinition().getName().lower()
		names = ['ellis', 'sbott', 'tanner']
		if name in names:
			player.getActionDeque().addAction(
			LeatherTanning(player, 1, Hide.forId(1747)))

def smeltBar(player, bar, amt=1):
	barName = ItemManager.getInstance().getItemName(bar.getId()).lower()
	
	firstItem = bar.getOre(0)
	firstItemName = firstItem.getDefinition().getName().lower()
	
	if player.getInventory().getItemContainer().getCount(firstItem.getId()) < firstItem.getCount():
		prefix = Misc.getArticle(barName)
		player.getActionSender().sendMessage('You do not have enough %s to smelt %s %s.' % 
											(firstItemName, prefix, barName))
		return player.getActionSender().removeInterfaces()
	
	ores = len(bar.getOres())
	msg = ' '
	
	if ores > 1:
		secondItem = bar.getOre(1)
		secondItemName = secondItem.getDefinition().getName().lower()
		
		amt = secondItem.getCount()
		term = 'heaps' if amt > 1 else 'heap'
		msg = ' and %d %s of %s ' % (amt, term, secondItemName)
		
		if player.getInventory().getItemContainer().getCount(secondItem.getId()) < secondItem.getCount():
			prefix = Misc.getArticle(barName)
			player.getActionSender().sendMessage('You do not have enough %s to smelt %s %s.' % 
												(secondItemName, prefix, barName))
			return player.getActionSender().removeInterfaces()
		
	prefix = 'You place the %s' % firstItemName
	suffix = 'in the furnace...'
	player.getActionSender().sendMessage('%s%s%s' % (prefix, msg, suffix))
	player.getActionDeque().addAction(Smelting(player, bar, amt))
	player.getActionSender().removeInterfaces()
								
