from com.asgarniars.rs2.task import Task

'''
Code associated with the waterfall quest!
'''

class SearchCrate(Task):
    '''
    Searching a crate for a specialty item.
    '''

    def __init__(self, player):
        super(SearchCrate, self).__init__(2)
        self.player = player

    def execute(self):
        id = 2840 if self.player.getClickId() == 1990 else 2838
        key = Item(id, 1)
        if self.player.getInventory().getItemContainer().hasRoomFor(key):
            if self.player.getInventory().addItem(key):
                self.player.getActionSender().sendMessage('and find a large key.')
        self.stop()


class RopeSwim(Task):
    '''
    The event when you swim across to the rock for the waterfall.
    '''

    def __init__(self, player):
        super(RopeSwim, self).__init__(5)
        self.player = player

    def execute(self):
        self.player.removeAttribute('tempWalkAnim')
        self.player.setAppearanceUpdateRequired(True)
        self.stop()

class RaftMessage(Task):
    '''
    The inital message telling you you've taken off on the raft.
    '''

    def __init__(self, player):
        super(RaftMessage, self).__init__(2)
        self.player = player

    def execute(self):
        self.player.getActionSender().sendMessage('and push off down stream.')
        World.submit(WaterfallRaft(self.player))
        self.stop()

class WaterfallRaft(Task):
    '''
    The event when the player takes the raft over the falls and crashes.
    '''

    def __init__(self, player):
        super(WaterfallRaft, self).__init__(2)
        self.player = player

    def execute(self):
        self.player.getActionSender().sendMessage('The raft is pulled down stream by strong currents.')
        self.player.teleport(Position(2512, 3480, 0))
        self.stop()

class WaterfallBarrel(Task):
    '''
    The event when the player goes over the waterfall in the barrel.
    '''

    def __init__(self, player):
        super(WaterfallBarrel, self).__init__(3)
        self.player = player

    def execute(self):
        self.player.clipTeleport(Position(2528, 3415, 0), 2)
        self.stop()

class GlarialsTomb(Task):

    def __init__(self, player):
        super(GlarialsTomb, self).__init__(2)
        self.player = player

    def execute(self):
        self.player.getActionSender().sendMessage('Inside you find an urn full of ashes.')
        World.submit(RetrieveUrn(self.player))
        self.stop()

class RetrieveUrn(Task):

    def __init__(self, player):
        super(RetrieveUrn, self).__init__(2)
        self.player = player

    def execute(self):
        urn = Item(296, 1)
        if self.player.getInventory().getItemContainer().hasRoomFor(urn):
            if self.player.getInventory().addItem(urn):
                self.player.getActionSender().sendMessage('You take the urn and close the coffin.')
        self.stop()

class EnterBaxtorianFalls(Task):

    def __init__(self, player):
        super(EnterBaxtorianFalls, self).__init__(2)
        self.player = player

    def execute(self):
        self.player.clipTeleport(Position(2574, 9864, 0), 2)
        self.player.getActionSender().sendMessage('You walk through the door.')
        self.stop()

class TreeFall(Task):

    def __init__(self, player):
         super(TreeFall, self).__init__(2)
         self.player = player

    def execute(self):
        self.player.clipTeleport(Position(2528, 3415, 0), 2)
        self.player.hit(8, 1, False)
        self.player.getUpdateFlags().sendForceMessage('Ouch!')
        self.player.getActionSender().sendMessage('You are washed up by the river side.')
        self.stop()
