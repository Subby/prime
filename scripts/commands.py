'''
Scripted commands written by Sneakyhearts
'''

from com.asgarniars.rs2 import Constants
from com.asgarniars.rs2.content import Judgement
from com.asgarniars.rs2.content.combat.magic import Magic
from com.asgarniars.rs2.content.quest import QuestRepository
from com.asgarniars.rs2.content.shop import Shop, ShopManager
from com.asgarniars.rs2.model import Camera, Position, World
from com.asgarniars.rs2.model.items import Item
from com.asgarniars.rs2.model.players import BankManager, GlobalObjectHandler, Player, Rights
from com.asgarniars.rs2.model.players.Player import MagicBookTypes
from com.asgarniars.rs2.model.npcs import Npc
from com.asgarniars.rs2.task import Task
from com.asgarniars.rs2.util import Misc

from java.lang import Runtime

def command_yell(player, args):

	rights = player.getRights()

	if rights == 1:
		pre = '[<col=001EFF>Prime</col>]<img=0><col=FF0000>'
	elif rights > 1 and rights < 4:
		pre = '[<col=001EFF>Prime</col>]<img=1><col=FF0000>'

	if rights > 0:
		pre += ''
		post = '</col>'
	else:
		pre = '[<col=001EFF>Prime</col>]'
		post = '</col>'

	msg = '%s%s%s:' % (pre, player.getUsername(), post)

	for arg in args:
		if arg != args[0]:
			msg += ' %s' % arg

	for x in World.getPlayers():

		if x != None:
			x.getActionSender().sendMessage('%s' % msg)


def command_pos(player, args):
	player.getActionSender().sendMessage(str(player.getPosition()))
	if player.isDebugging():
		print player.getPosition()


def command_char(player, args):
	player.getActionSender().sendInterface(3559)

def command_players(player, args):
	line = 8147
	count = World.playerAmount()
	word = 'players' if count > 1 else 'player'
	player.getActionSender().sendString('Prime - Players Online', 8144)
	player.getActionSender().sendString('There is currently total of %d %s online' % (count, word), 8145)
	for i in range(8146, 8196):
		player.getActionSender().sendString('', i)
	for i in range(12174, 12224):
		player.getActionSender().sendString('', i)
	for x in World.getPlayers():
		if x != None:
			player.getActionSender().sendString(x.getUsername(), line)
			line += 1

	player.getActionSender().sendInterface(8134)



def command_pass(player, args):
	newPass = str(args[1])
	player.setAttribute('oldPass', player.getPassword())
	player.setPassword(newPass)
	player.getActionSender().sendMessage('Your password has been changed from <col=ff0033>' + player.getAttribute('oldPass') + '</col> to ' + newPass + '.')
	player.removeAttribute('oldPass')

def mod_command_teleto(player, args):
	name = args[1].lower().replace('-', ' ')
	p2 = World.getInstance().getPlayerByName(name)
	if p2 is not None:
		player.teleport(p2.getPosition())

def mod_command_teletome(player, args):
	name = args[1].lower().replace('-', ' ')
	p2 = World.getInstance().getPlayerByName(name)
	if p2 is not None:
		p2.teleport(player.getPosition())
	if player.isDebugging():
		player.getActionSender().sendMessage("You have summoned : " + str(p2))
		
def mod_command_mute(player, args):
	if len(args) < 2:
		return
	mins = 5 if len(args) < 3 else int(args[2])
	name = args[1].lower().replace('-', ' ')
	p2 = World.getInstance().getPlayerByName(name)
	if p2 is not None:
		Judgement.silence(p2, mins)
	
def mod_command_ban(player, args):
	reason = 'N/A' if len(args) < 2 else args[1]
	Judgement.ban(player, reason)

def admin_command_sound(player, args):
	id = int(args[1])
	player.getActionSender().sendSound(id, 100, 0)

def admin_command_confuse(player, args):
	name = args[1].lower().replace('-', ' ')
	p2 = World.getInstance().getPlayerByName(name)
	if p2 is not None:
		p2.getActionSender().sendMessage('You have been stricken by confusion!')
		p2.getActionSender().sendWalkableInterface(13583)
		p2.setCanWalk(False)
		World.submit(RegainClarity(p2))


def admin_command_debug(player, args):
	if player.getAttribute('isDebugging') is None:
		player.setAttribute('isDebugging', True)
		player.getActionSender().sendMessage('You are now in <col=008000>DEBUG</col> mode')
	else:
		player.removeAttribute('isDebugging')
		player.getActionSender().sendMessage('You have cancelled <col=ff0000>DEBUG</col> mode')


def admin_command_empty(player, args):
	player.getInventory().getItemContainer().clear()
	player.getInventory().refresh()

def admin_command_shop(player, args):
	ShopManager.open(player, int(args[1]))

def admin_command_spells(player, args):
	if len(args) < 2:
		return
	spellbook = int(args[1])
	if spellbook == 0:
		Magic.getInstance().changeSpellbook(player, MagicBookTypes.MODERN)
	if spellbook == 1:
		Magic.getInstance().changeSpellbook(player, MagicBookTypes.ANCIENT)
	if spellbook == 2:
	   Magic.getInstance().changeSpellbook(player, MagicBookTypes.LUNAR)


def admin_command_tele(player, args):
	x = int(args[1])
	y = int(args[2])
	z = 0
	if len(args) > 3:
		z = int(args[3])
	player.teleport(Position(x, y, z))

def admin_name(player, args):
	newName = str(args[1])
	player.setAttribute('oldName', player.getUsername())
	player.setUsername(newName)
	player.getActionSender().sendMessage('Your username has been changed from <col=ff0033>' + player.getAttribute('oldName') + '</col> to ' + newName + '.')
	player.removeAttribute('oldName')
	
def admin_command_gems(player, args):
	gems = [1617, 1619, 1621, 1623, 1625, 1627, 1629, 1631]
	for gem in gems:
		player.getInventory().addItem(Item(gem, 1))


def admin_command_song(player, args):
	player.getActionSender().sendSong(int(args[1]))


def admin_command_anim(player, args):
	player.getUpdateFlags().sendAnimation(int(args[1]), 0)


def admin_command_bank(player, args):
	BankManager.openBank(player)


def admin_command_spec(player, args):
	player.setSpecialAmount(10.0)


def admin_command_gfx(player, args):
	gfxId = int(args[1])
	player.getUpdateFlags().sendGraphic(gfxId, 0, 100)


def admin_command_energy(player, args):
	player.setEnergy(1000)


def admin_command_ie(player, args):
	print player.getInteractingEntity()


def admin_command_inter(player, args):
	inter = args[1]
	player.getActionSender().sendInterface(int(inter))


def admin_command_winter(player, args):
	inter = args[1]
	player.getActionSender().sendWalkableInterface(int(inter))


def admin_command_clear(player, args):
	player.getActionSender().removeInterfaces()


def admin_command_config(player, args):
	if len(args) < 3:
		return
	print 'wut'
	player.getActionSender().sendConfig(int(args[1]), int(args[2]))


def admin_command_sstring(player, args):
	player.getActionSender().sendString(args[1].replace('~', ' '), int(args[2]))


def admin_command_jewellery(player, args):
	rings = [Item(1635)]
	necklaces = [Item(1654)]
	amulets = [Item(1673)]
	if player.getInventory().getItemContainer().contains(1592):
		player.getActionSender().sendString("", 4230)
	if player.getInventory().getItemContainer().contains(1597):
		player.getActionSender().sendString("", 4236)
	if player.getInventory().getItemContainer().contains(1595):
		player.getActionSender().sendString("", 4242)
	player.getActionSender().sendUpdateItems(4233, rings)
	player.getActionSender().sendUpdateItems(4239, necklaces)
	player.getActionSender().sendUpdateItems(4245, amulets)
	player.getActionSender().sendInterface(4161)


def admin_command_ess(player, args):  # fills the inventory with pure essence
	player.getInventory().addItem(
	Item(7936, player.getInventory().getItemContainer().freeSlots()))


def admin_command_tabs(player, args):
	tabs = [8006 < 8022]
	for tab in tabs:
		item = Item(tab, 1)
		if player.getInventory().hasRoomFor(item):
			player.getInventory().addItem(item)

def admin_command_announce(player, args):

	msg = '[PRIME]<col=ff0000>'

	for arg in args:
		if arg != args[0]:
			msg += ' %s' % arg

	for player in World.getPlayers():
		if player != None:

			player.getActionSender().sendMessage(msg)


def admin_command_noclip(player, args):
	if player.getAttribute('noclip') == None:
		player.setAttribute('noclip', 0)
		player.getActionSender().sendMessage('You have enabled <col=008000>NOCLIP</col>.')
	else:
		player.removeAttribute('noclip')
		player.getActionSender().sendMessage('You have disabled <col=ff0000>NOCLIP</col>.')


def admin_command_camstill(player, args):
	camera = Camera(player)
	camera.move(int(args[1]), int(args[2]), int(args[3]))
	camera.rotate(int(args[4]))
	camera.ascend(int(args[5]))
	camera.update()


def admin_command_camreset(player, args):
	player.getActionSender().sendCameraReset()


def admin_command_camspin(player, args):
	player.getActionSender().sendCameraSpin(int(args[1]), int(args[2]),
											int(args[3]), int(args[4]), int(args[5]))

# This one is for the Winterlove fans
def admin_command_hail(player, args):
	for other in World.getPlayers():
		if isinstance(other, Player): # if they are not null, and in fact a player.
			if other is not player: # if they are not the hail caster.
				other.getUpdateFlags().sendFaceToDirection(player.getPosition())
				other.getUpdateFlags().sendAnimation(1651)
				other.getUpdateFlags().sendForceMessage('ALL HAIL %s!' % player.getUsername().upper())


def admin_command_pnpc(player, args):
	id = int(args[1])
	player.setHumanToNpc(id)
	player.setAppearanceUpdateRequired(True)


def admin_command_att(player, args):
	for att in player.getAttributes():
		player.getActionSender().sendMessage(str(att))


def admin_command_vpsload(player, args):
	player.getActionSender().sendMessage("<trans=150><shad><col=ccff00>[VPS LOAD] [CPU_COUNT=" + str(Runtime.getRuntime().availableProcessors()) + "] [RAM_HEAP (free/total): " + str(Runtime.getRuntime().freeMemory() / 1000000) + "mb/" + str(Runtime.getRuntime().maxMemory() / 1000000) + "mb]")


def admin_command_xnpc(player, args):
	id = args[1] if len(args) > 1 else '1'
	mode = 'STAND'
	x = str(player.getPosition().getX())
	y = str(player.getPosition().getY())
	z = str(player.getPosition().getZ())
	if len(args) > 2:
		mode = 'STAND' if '0' in args[2] else 'WALK'
	block = '\t<npc>\n\t\t<npcId>%s</npcId>\n\t\t<spawnPosition>\n\t\t\t<x>%s</x>\n\t\t\t<y>%s</y>\n\t\t\t<z>%s</z>\n\t\t</spawnPosition>\n\t\t<walkType>%s</walkType>'% (id, x, y, z, mode)
	if mode == 'WALK':
		if len(args) > 4:
			max_x = args[3]
			max_y = args[4]
		else:
			max_x = '1'
			max_y = '0'
		block += '\n\t\t<maxWalk>\n\t\t\t<x>%s</x>\n\t\t\t<y>%s</y>\n\t\t</maxWalk>' % (max_x, max_y)
	block += '\n\t</npc>'
	with open('spawnednpcs.dat', 'a+') as f:
		f.write('[START SPAWNED NPC (%s)]\n' % player.getUsername())
		f.write(block)
		f.write('\n[END SPAWNED NPC (%s)]\n' % player.getUsername())
	player.getActionSender().sendMessage('Successfully submitted npc spawn. ID : %s - %s' % (id, str(player.getPosition())))

def admin_command_rank(player, args):
	'''
	Updates the players rights, highly open to exploit.
	'''
	rights = Rights.forRank(int(args[1]))
	player.setRights(rights)
	player.getActionSender().sendMessage('Updated rights level : %d - %s' % (rights.ordinal(), str(rights)))

# ADMIN TYPE COMMANDS THAT PLAYERS CAN STILL EXECUTE, MUST BE HIGHLY MONITORED

def command_lvl(player, args):
	skill = int(args[1])
	lvl = int(args[2])
	player.getSkill().getLevel()[skill] = lvl
	player.getSkill().getExp()[skill] = player.getSkill().getXPForLevel(lvl)
	if skill is player.getSkill().PRAYER:
		player.getSkill().setPrayerPoints(lvl)
	player.getSkill().refresh()

def command_xlvl(player, args):
	lvl = int(args[1])
	for skill in range(0, len(player.getSkill().getLevel())):
		player.getSkill().getLevel()[skill] = lvl
		player.getSkill().getExp()[skill] = player.getSkill().getXPForLevel(lvl)
		player.getSkill().setPrayerPoints(lvl)
	player.getSkill().refresh()


def command_max(player, args):
	max = 99
	for skill in range(0, len(player.getSkill().getLevel())):
		player.getSkill().getLevel()[skill] = max
		player.getSkill().getExp()[skill] = player.getSkill().getXPForLevel(max)
		player.getSkill().setPrayerPoints(max)
	player.getSkill().refresh()

def command_runes(player, args):
	for runes in range(554, 567):
		rune = Item(runes, 100999)
		player.getInventory().addItem(rune)

def command_item(player, args):
	itemId = int(args[1])
	if len(args) > 2:
		amt = int(args[2])
		item = Item(itemId, amt)
		if not player.getInventory().hasRoomFor(item) and not item.getDefinition().isStackable():
			amt = player.getInventory().getItemContainer().freeSlots()
	else:
		amt = 1
	item = Item(itemId, amt)
	if player.getInventory().addItem(item):
		word = Misc.getArticle(item.getDefinition().getName())
		prefix = str(amt) if (amt > 1) else word
		suffix = "'s." if (amt > 1) else '.'
		player.getActionSender().sendMessage('You have successfully spawned ' + prefix + ' ' + item.getDefinition().getName() + suffix)

def command_home(player, args):
	player.getActionSender().sendMessage('Please use a Home Teleport Tab')

def command_pots(player, args):
	''' 
	Adds noted potions into the inventory of the player
	'''
	pots = [2441,2437, 2443, 2431, 6686]
	for pot in pots:
		addition = Item(pot, 1000)
		player.getInventory().addItem(addition)

# Tasks used for various commands.

class RegainClarity(Task):

	'''
	Used for a silly command that confuses the player with the
	inability to walk or see anything (black screen), then
	they are permitted their visibility and freedom to walk.
	'''

	def __init__(self, player):
		super(RegainClarity, self).__init__(5)
		self.player = player

	def execute(self):
		self.player.setCanWalk(True)
		self.player.getActionSender().sendWalkableInterface(-1)
		self.player.getActionSender().sendMessage('You have regained clarity.')
		self.stop()
