# Speciality Item Interaction Script
# Created by Joshua Barry <Sneaky>
# January 20 2013 : 3:00PM
from com.asgarniars.rs2.util import Misc

def toy_horsey(entity, item):
	sayings = ['Come on Dobbin, we can win the race!', 'Hi-ho Silver, and away!', 'Giddy-up horsey!']
	message = Misc.generatedString(sayings)
	entity.getUpdateFlags().sendForceMessage(message)
	if item.getId() == 2520:
		entity.getUpdateFlags().sendAnimation(918)
	elif item.getId() == 2522:
		entity.getUpdateFlags().sendAnimation(919)
	elif item.getId() == 2524:
		entity.getUpdateFlags().sendAnimation(920)
	elif item.getId() == 2526:
		entity.getUpdateFlags().sendAnimation(921)