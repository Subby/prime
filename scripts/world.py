'''
Created by Joshua Barry <Sneakyhearts> Sunday, September 15 2013 @ 10:55PM
'''

from com.asgarniars.rs2.model import Position, World
from com.asgarniars.rs2.model.area import Area
from com.asgarniars.rs2.model.players import GroundItem

from com.asgarniars.rs2.model.items import Item, ItemManager

from com.asgarniars.rs2.task import Task

from com.asgarniars.rs2.util import Misc

def distribute(time):
	min_x = 3185
	min_y = 3198
	max_x = 3240
	max_y = 3253
	plane = 0

	area = Area(Position(min_x, min_y, plane), Position(max_x, max_y, plane))

	World.submit(RareDistributor(time, area))

class RareDistributor(Task):

	'''
	A task that will distribute rare items to the world.
	'''

	def __init__(self, time, *areas):
		super(RareDistributor, self).__init__(time)
		self.areas = areas
		self.count = 0

	def execute(self):
		print self.areas

		area = self.areas[0]

		x = Misc.randomMinMax(area.getMinimumPosition().getX(), area.getMaximumPosition().getX())
		y = Misc.randomMinMax(area.getMinimumPosition().getY(), area.getMaximumPosition().getY())
		z = area.getMinimumPosition().getZ()

		id = Misc.generatedValue([1050, 1053, 1055, 1057])

		print x,y,z, 'id:', id

		for player in World.getPlayers():
			if player != None:
				ItemManager.getInstance().handle_rares(GroundItem(None, Item(id), Position(x, y, z), False))

		if self.count >= 10:
			self.stop()
		else:
			self.count += 1
